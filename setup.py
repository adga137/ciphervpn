import os
import versioneer

from setuptools import setup, find_packages

# import build_ui
try:
    from pyqt_distutils.build_ui import build_ui
    cmdclass = {'build_ui': build_ui}
except ImportError:
    build_ui = None  # user won't have pyqt_distutils when deploying
    cmdclass = {}

requirements = []
with open('pkg/requirements.pip') as requirements_file:
    for line in requirements_file:
        if line.strip() == '':
            continue
        if line.startswith('#'):
            continue
        requirements.append(line)

cmdclass.update(versioneer.get_cmdclass())
with open('README.rst', 'r') as readme_file:
    setup(
        name='netsplice',
        version=versioneer.get_version(),
        cmdclass=cmdclass,
        author='Netsplice',
        author_email='support@ipredator.se',
        description=(
            'Netsplice is a multi-platform desktop VPN client'),
        license='GPLv3',
        keywords='OpenVPN GUI',
        url='https://ipredator.se/',
        packages=find_packages(where='src'),
        package_dir={'': 'src'},
        entry_points = {
            'console_scripts': ['netsplice=netsplice.NetspliceGuiApp:run_gui'],
        },
        long_description=readme_file.read(),
        classifiers=[
            'Development Status :: 3 - Alpha',
            'Topic :: Utilities',
            'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)'
            'Programming Language :: Python :: 2.7',
            'Intended Audience :: End Users/Desktop',
            'Operating System :: Microsoft :: Windows',
            'Operating System :: MacOS :: MacOS X',
            'Operating System :: POSIX :: Linux',
            'Topic :: System :: Networking',
            'Topic :: Utilities'
        ],
        install_requires=requirements,
        test_suite='nose.collector',
        tests_require=['nose'],
        include_package_data=True,
        zip_safe=True,
    )
