#!/bin/bash
#
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# VERY simple script to set the dns infos to systemd
#
# This is inspired by https://github.com/jonathanio/update-systemd-resolved
#
# and reduced to a minimum.
cd "$(dirname "$0")" || exit_fatal "Cannot change directory to $0."
source .subroutines.inc.sh

DBUS_DEST="org.freedesktop.resolve1"
DBUS_NODE="/org/freedesktop/resolve1"
DHCP_DOMAIN=($(set | grep 'dhcp-option DOMAIN' \
  | sed "s|.*DOMAIN \([^']*\)'|\1|")) #'
DHCP_NAMESERVER=($(set | grep 'dhcp-option DNS' \
  | sed "s|.*DNS \([^']*\)'|\1|")) #'
IP_LINK_SHOW=$(ip link show dev "${openvpn_dev}" 2>/dev/null)
IF_INDEX=${IP_LINK_SHOW%%:*}

# Enforces RFC 5952:
#   1. Don't shorten a single 0 field to '::'
#   2. Only longest run of zeros should be compressed
#   3. If there are multiple longest runs, the leftmost should be compressed
#   4. Address must be maximally compressed, so no all-zero runs next to '::'
parse_ipv6()
{
    local raw_address="$1"

    if [[ "$raw_address" == *::*::* ]]; then
        error_log "address cannot contain more than one '::'"
        return 1
    elif [[ "$raw_address" =~ :0+:: ]] || [[ "$raw_address" =~ ::0+: ]]; then
        error_log "address contains a 0-group adjacent to '::' and is not maximally shortened"
        return 1
    fi

    local -i length=8
    local -a raw_segments=()

    IFS=$':' read -r -a raw_segments <<<"$raw_address"

    local -i raw_length="${#raw_segments[@]}"

    if (( raw_length > length )); then
        error_log "expected ${length} segments, got ${raw_length}"
        return 1
    fi

    # Store zero-runs keyed to their sizes, storing all non-zero segments prefixed
    # with a token marking them as such.
    local nonzero_prefix=$'!'
    local -i zero_run_i=0 compressed_i=0
    local -a tokenized_segments=()
    local decimal_segment='' next_decimal_segment=''

    for (( i = 0 ; i < raw_length ; i++ )); do
        raw_segment="${raw_segments[i]}"

        printf -v decimal_segment -- '%d' "0x${raw_segment:-0}"

        # We're in the compressed group.  The length of this run should be
        # enough to bring the total number of segments to 8.
        if [[ -z "$raw_segment" ]]; then
            (( compressed_i = zero_run_i ))

            # `+ 1' because the length of the current segment is counted in
            # `raw_length'.
            (( tokenized_segments[zero_run_i] = ((length - raw_length) + 1) ))

            # If we have an address like `::1', skip processing the next group to
            # avoid double-counting the zero-run, and increment the number of
            # 0-groups to add since the second empty group is counted in
            # `raw_length'.
            if [[ -z "${raw_segments[i + 1]}" ]]; then
                (( i++ ))
                (( tokenized_segments[zero_run_i]++ ))
            fi

            (( zero_run_i++ ))
        elif (( decimal_segment == 0 )); then
            (( tokenized_segments[zero_run_i]++ ))

            # The run is over if the next segment is not 0, so increment the
            # tracking index.
            printf -v next_decimal_segment -- '%d' "0x${raw_segments[i + 1]}"

            (( next_decimal_segment != 0 )) && (( zero_run_i++ ))
        else
            # Prefix the raw segment with `nonzero_prefix' to mark this as a
            # non-zero field.
            tokenized_segments[zero_run_i]="${nonzero_prefix}${decimal_segment}"
            (( zero_run_i++ ))
        fi
    done

    if [[ "$raw_address" == *::* ]]; then
        if (( ${#tokenized_segments[*]} == length )); then
            error_log "single '0' fields should not be compressed"
            return 1
        else
            local -i largest_run_i=0 largest_run=0

            for (( i = 0 ; i < ${#tokenized_segments[@]}; i ++ )); do
                # Skip groups that aren't zero-runs
                [[ "${tokenized_segments[i]:0:1}" == "$nonzero_prefix" ]] && continue

                if (( tokenized_segments[i] > largest_run )); then
                    (( largest_run_i = i ))
                    largest_run="${tokenized_segments[i]}"
                fi
            done

            local -i compressed_run="${tokenized_segments[compressed_i]}"

            if (( largest_run > compressed_run )); then
                error_log "the compressed run of all-zero fields is smaller than the largest such run"
                return 1
            elif (( largest_run == compressed_run )) && (( largest_run_i < compressed_i )); then
                error_log "only the leftmost largest run of all-zero fields should be compressed"
                return 1
            fi
        fi
    fi

    for segment in "${tokenized_segments[@]}"; do
        if [[ "${segment:0:1}" == "$nonzero_prefix" ]]; then
            printf -- '%04x\n' "${segment#${nonzero_prefix}}"
        else
            for (( n = 0 ; n < segment ; n++ )); do
                echo 0000
            done
        fi
    done
}

if [ -n "${IF_INDEX}" ] \
        && [ -n "${DHCP_DOMAIN}" ] \
        && [ -n "${DHCP_NAMESERVER}" ]; then
    COUNT_DOMAIN="${#DHCP_DOMAIN[@]}"
    DBUS_CTL_PARAMS=("${IF_INDEX}" "${COUNT_DOMAIN}" "${DHCP_DOMAIN}")
    dbusctl_call "${DBUS_DEST}" "${DBUS_NODE}" \
        SetLinkDomains 'ia(sb)' "${DBUS_CTL_PARAMS[@]}"

    COUNT_NAMESERVER="${#DHCP_NAMESERVER[@]}"
    declare -a DBUS_NAMESERVER
    declare -a IPV6_SEGMENTS=()
    for DHCP_NAMESERVER in "${DHCP_NAMESERVER[@]}"; do
        if looks_like_ipv4 "${DHCP_NAMESERVER}"; then
            DBUS_NAMESERVER+=(2 4 ${DHCP_NAMESERVER//./ })
        elif looks_like_ipv6 "${DHCP_NAMESERVER}"; then
            IPV6_SEGMENTS=($(parse_ipv6 "${DHCP_NAMESERVER}")) || continue

            # Add AF_INET6 and byte count
            DBUS_NAMESERVER+=(10 16)
            for segment in "${IPV6_SEGMENTS[@]}"; do
                DBUS_NAMESERVER+=("$((16#${segment:0:2}))" "$((16#${segment:2:2}))")
            done
        else
            error_fatal "Bad Nameserver Address"
        fi
    done
    DBUS_CTL_PARAMS=("${IF_INDEX}" "${COUNT_NAMESERVER}" ${DBUS_NAMESERVER[@]})
    dbusctl_call "${DBUS_DEST}" "${DBUS_NODE}" \
        SetLinkDNS 'ia(iay)' "${DBUS_CTL_PARAMS[@]}"
else
    error_log "Not enough information in environment." \
        "Need dhcp-options and dev."
fi
exit_success
