::
:: Copyright (C) 2017 Netsplice
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <http://www.gnu.org/licenses/>.

:: https://www.windows-security.org/b2940a7409e7c087bbc14c69d47e7aa3/turn-off-multicast-name-resolution

:: Specifies that link local multicast name resolution (LLMNR) is disabled
:: on client computers.LLMNR is a secondary name resolution protocol. With
:: LLMNR queries are sent using multicast over a local network link on a
:: single subnet from a client computer to another client computer on the
:: same subnet that also has LLMNR enabled. LLMNR does not require a DNS
:: server or DNS client configuration and provides name resolution in
:: scenarios in which conventional DNS name resolution is not possible.If
:: you enable this policy setting LLMNR will be disabled on all available
:: network adapters on the client computer.If you disable this policy
:: setting or you do not configure this policy setting LLMNR will be enabled
:: on all available network adapters.

:: 1: Enable multihome resolution
:: 0: Disable multihome resolution
reg add "HKLM\Software\Policies\Microsoft\Windows NT\DNSClient" /v "EnableMulticast" /t REG_DWORD /d "1" /f
