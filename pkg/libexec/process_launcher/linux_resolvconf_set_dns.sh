#!/bin/bash
#
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Set the /etc/resolv.conf domain and nameserver from the environment of the
# openvpn process.
# Stores the original /etc/resolv.conf for linux_restore_resolvconf.sh
# Also setup a disconnect rule that calls linux_restore_resolvconf.sh
# Using this script may break modern dns resolver features. Read the
# documentation of your distribution to learn if this is the correct
# method to set the DNS servers.
cd "$(dirname "$0")" || exit_fatal "Cannot change directory to $0."
source .subroutines.inc.sh

declare RESOLVCONF=${RESOLVCONF:-/etc/resolv.conf}
declare ACCOUNT_ID=${netsplice_account_id}
declare RESOLVCONF_BACKUP="${RESOLVCONF}.${ACCOUNT_ID}"
declare -a DHCP_DOMAIN
declare -a DHCP_NAMESERVER

DHCP_DOMAIN=($(set | grep 'dhcp-option DOMAIN' \
    | sed "s|.*DOMAIN \([^']*\)'|domain \1|"))
DHCP_NAMESERVER=($(set | grep 'dhcp-option DNS' \
    | sed "s|.*DNS \([^']*\)'|\1|"))

if [ -n "${DHCP_DOMAIN[*]}" ] && [ -n "${DHCP_NAMESERVER[*]}" ]; then
    if [ -f "${RESOLVCONF_BACKUP}" ]; then
        run_cmd_coe \
            rm "${RESOLVCONF_BACKUP}"
    fi
    run_cmd_coe \
        mv "${RESOLVCONF}" "${RESOLVCONF_BACKUP}"
    header_note "#" > "${RESOLVCONF}"
    echo "${DHCP_DOMAIN[@]}" >> "${RESOLVCONF}"
    for NAMESERVER in "${DHCP_NAMESERVER[@]}"; do
        echo "nameserver ${NAMESERVER}" >> "${RESOLVCONF}"
    done
else
    error_fatal "Did not find the domain or nameserver options" \
      "required in the environment"
fi

exit_success
