#!/bin/bash
#
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Set the nameservers in osx usingthe networksetup command
# from the environment of the openvpn process.
# Stores the original settings for osx_networksetup_restore_dns.sh
# Also setup a disconnect rule that calls osx_networksetup_restore_dns.sh
# Using this script may break modern dns resolver features. Read the
# documentation of your distribution to learn if this is the correct
# method to set the DNS servers.
cd "$(dirname "$0")" || exit_fatal "Cannot change directory to $0."
source .subroutines.inc.sh

declare -a DHCP_DOMAIN
declare -a DHCP_NAMESERVER

DHCP_DOMAIN=($(set | grep 'dhcp-option DOMAIN' \
    | sed "s|.*DOMAIN \(.*\)|\1|;s|'||g"))
DHCP_NAMESERVER=($(set | grep 'dhcp-option DNS' \
    | sed "s|.*DNS \([^']*\)'|\1|"))

if [ -n "${DHCP_DOMAIN[*]}" ] && [ -n "${DHCP_NAMESERVER[*]}" ]; then
    NETWORKDEVICES=$(networksetup -listallnetworkservices \
        | grep -v 'An asterisk')
    XIFS="${IFS}"
    IFS='
'
    for DEVICE in ${NETWORKDEVICES[@]}; do
        echo "Set DNS for '${DEVICE}' with ${DHCP_NAMESERVER[@]} and ${DHCP_DOMAIN}."
        run_cmd_coe \
            networksetup -setdnsservers "${DEVICE}" "${DHCP_NAMESERVER[@]}"
        run_cmd_coe \
            networksetup -setsearchdomains "${DEVICE}" "${DHCP_DOMAIN}"
    done
    IFS="${XIFS}"
else
    error_fatal "Did not find the domain or nameserver options" \
      "required in the environment"
fi

exit_success
