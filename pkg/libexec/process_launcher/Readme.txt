Privileged Launcher Rules
=========================

This directory contains scripts that are executed by NetsplicePrivilegedApp
when the user has configured it.

The scripts work without parameters and use environment variables to evaluate
what they have to do.

The environment variables are provider dependent. It is possible to see them
when a account connects in the logviewer.

Please use this scripts responsibly and send us useful examples that we can
include in a future release of the application.

Use return-codes to communicate back to the application:

0: OK.
32: Failure, Disconnect the account.
64: Failure, Show error message.

A .subroutines.inc.sh is provided that contains the error-codes and some useful
methods to output errors.
