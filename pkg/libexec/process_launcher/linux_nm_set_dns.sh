#!/bin/bash
#
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# VERY simple script to set the dns infos to all connections managed
# by the NetworkManager infrastructure.

cd "$(dirname "$0")" || exit_fatal "Cannot change directory to $0."
source .subroutines.inc.sh

DHCP_DOMAIN=($(set | grep 'dhcp-option DOMAIN' \
  | sed "s|.*DOMAIN \([^']*\)'|\1|")) #'
DHCP_NAMESERVER=($(set | grep 'dhcp-option DNS' \
  | sed "s|.*DNS \([^']*\)'|\1|")) #'

declare -a CONNECTIONS
org_IFS=${IFS}
IFS="
"
CONNECTIONS=($(
    nmcli device show \
    | grep 'GENERAL.CONNECTION' \
    | awk -F':' '{print $2}' \
    | sed 's|^\s*||' \
    | grep -v '^--$'
))
IFS=${org_IFS}

for DHCP_NAMESERVER in "${DHCP_NAMESERVER[@]}"; do
    for CONNECTION in "${CONNECTIONS[@]}"; do
        run_cmd_coe \
            nmcli c modify "${CONNECTION}" +ipv4.dns "${DHCP_NAMESERVER}"
    done
done

for CONNECTION in "${CONNECTIONS[@]}"; do
    run_cmd_coe \
        nmcli c modify "${CONNECTION}" ipv4.ignore-auto-dns yes
    run_cmd_coe \
        nmcli c up "${CONNECTION}"
done

exit_success
