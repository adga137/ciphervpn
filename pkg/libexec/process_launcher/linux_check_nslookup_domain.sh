#!/bin/bash
#
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Check that the DHCP nameservers from openvpn are used to resolve DNS
# requests by resolving the domain using nlookup.
# Request Netsplice to disconnect the account when the resolve comes from a
# different server

cd "$(dirname "$0")" || exit_fatal "Cannot change directory to $0."
source .subroutines.inc.sh

declare -a DHCP_DOMAIN
declare -a DHCP_NAMESERVER
declare DHCP_VERIFY_LOOKUP_DOMAIN
declare LOOKUP_SERVER
declare IS_FROM_NAMESERVER

DHCP_DOMAIN=($(set | grep 'dhcp-option DOMAIN' \
    | sed "s|.*DOMAIN \([^']*\)'|domain \1|"))
DHCP_NAMESERVER=($(set | grep 'dhcp-option DNS' \
    | sed "s|.*DNS \([^']*\)'|nameserver \1|"))
DHCP_VERIFY_LOOKUP_DOMAIN=$(echo "${DHCP_DOMAIN[@]}" | head -n 1 \
    | sed "s|domain ||")
if [ ! -n "${DHCP_DOMAIN[*]}" ] || [ ! -n "${DHCP_NAMESERVER[*]}" ]; then
    error_fatal "Did not find the domain or nameserver options" \
      "required in the environment"
fi
LOOKUP_SERVER=$(nslookup "${DHCP_VERIFY_LOOKUP_DOMAIN}" \
    | grep '^Server' \
    | sed 's|Server:\s*||')
IS_FROM_NAMESERVER=$(echo "${DHCP_NAMESERVER[@]}" \
    | grep "${LOOKUP_SERVER}")

if [ "${IS_FROM_NAMESERVER}" == "" ]; then
    error_fatal_code "${ERROR_CODE_DISCONNECT}" \
        "Nameserver not configured correctly." \
        " The request to ${DHCP_VERIFY_LOOKUP_DOMAIN} was returned by" \
        " ${LOOKUP_SERVER} instead of " \
        "$(printf "%s " "${DHCP_NAMESERVER[*]}")"
fi

exit_success
