#!/bin/bash
#
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Restore a previously overwritten networksetup

# Using this script may break modern dns resolver features. Read the
# documentation of your distribution to learn if this is the correct
# method to set the DNS servers.
cd "$(dirname "$0")" || exit_fatal "Cannot change directory to $0."
source .subroutines.inc.sh

declare -a NETWORKDEVICES=$(networksetup -listallnetworkservices \
  | grep -v 'An asterisk')
XIFS="${IFS}"
IFS='
'
for DEVICE in ${NETWORKDEVICES[@]}; do
    echo "Restore DNS for '${DEVICE}'"
    run_cmd_coe \
        networksetup -setdnsservers "${DEVICE}" Empty
    run_cmd_coe \
        networksetup -setsearchdomains "${DEVICE}" Empty
done
IFS="${XIFS}"