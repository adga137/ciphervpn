#!/bin/bash
#
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# VERY simple script to restore the dns infos to Network Manager
#
cd "$(dirname "$0")" || exit_fatal "Cannot change directory to $0."
source .subroutines.inc.sh

declare -a CONNECTIONS
org_IFS=${IFS}
IFS="
"
CONNECTIONS=($(
    nmcli device show \
    | grep 'GENERAL.CONNECTION' \
    | awk -F':' '{print $2}' \
    | sed 's|^\s*||' \
    | grep -v '^--$'
))
IFS=${org_IFS}


HAS_DNS=1
while [ "${HAS_DNS}" == "1" ]; do
    HAS_DNS=0
    for CONNECTION in "${CONNECTIONS[@]}"; do
        nmcli c modify "${CONNECTION}" -ipv4.dns 0 2> /dev/null \
        && HAS_DNS=1
    done
done

for CONNECTION in "${CONNECTIONS[@]}"; do
    run_cmd_coe \
	nmcli c modify "${CONNECTION}" ipv4.ignore-auto-dns no
    run_cmd_coe \
	nmcli c up "${CONNECTION}"
done

exit_success
