#!/bin/bash
#
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# VERY simple script to restore the dns infos to systemd
#
# This is inspired by https://github.com/jonathanio/update-systemd-resolved
#
# and reduced to a minimum.
cd "$(dirname "$0")" || exit_fatal "Cannot change directory to $0."
source .subroutines.inc.sh

DBUS_DEST="org.freedesktop.resolve1"
DBUS_NODE="/org/freedesktop/resolve1"
IP_LINK_SHOW=$(ip link show dev "${openvpn_dev}" 2>/dev/null)
IF_INDEX=${IP_LINK_SHOW%%:*}

if [ ! -n "${IF_INDEX}" ]; then
    error_fatal "Not enough information in environment." \
        "Need a 'dev' environment variable."
fi

dbusctl_call "${DBUS_DEST}" "${DBUS_NODE}" RevertLink i "${IF_INDEX}"
exit_success
