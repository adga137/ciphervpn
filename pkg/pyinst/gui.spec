# -*- mode: python -*-# -*- mode: python -*-
import sys
import os
sys.modules['FixTk'] = None

block_cipher = None
exe_name = 'NetspliceGuiApp'
debug = False

a = Analysis(
    ['../../src/netsplice/%s.py' % (exe_name,)],
    hiddenimports=[
        'PySide.QtCore', 'PySide.QtGui', 'timeago.locales.en'
    ],
    binaries=None,
    datas=None,
    # The path is strangely relative to slave/${system}
    hookspath=['../../../pyinst/hooks'],
    runtime_hooks=None,
    excludes=['FixTk', 'tcl', 'tk', '_tkinter', 'tkinter', 'Tkinter'],
    win_no_prefer_redirects=None,
    win_private_assemblies=None,
    cipher=block_cipher)

pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

# Binary files you need to include in the form of:
# (<destination>, <source>, '<TYPE>')

# Data files you want to include, in the form of:
# (<destination>, <source>, '<TYPE>')
data = [
  ('qt.conf', 'qt.conf', 'DATA')
]
exe = EXE(
    pyz,
    a.scripts,
    exclude_binaries=True,
    name=exe_name,
    debug=debug,
    strip=None,
    console=debug,
    upx=False,
    icon='../../res/images/netsplice.ico')

coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas,
    strip=None,
    debug=debug,
    upx=False,
    name=exe_name)

if sys.platform.startswith('darwin'):
    app = BUNDLE(
        coll,
        name=os.path.join('dist', '%s.app' % (exe_name,)),
        appname=exe_name,
        icon='../../res/images/netsplice.icns',
        bundle_identifier='netsplice-')
