#!/usr/bin/env bash

# Sign downloads for netsplice update
# This script finds the latest files in the HTTPDIR/downloads directory
# and compiles a version.json from the files by signing all files with
# the OPENSSL_SIGN_KEY

UPDATE_DATE="$(date --rfc-3339=date)T00:00:00Z"
UPDATE_FILE=update.json
DOWNLOAD_ROOT="https://ipredator.se/static/downloads/netsplice"

if [ -z ${OPENSSL_SIGN_KEY} ]; then
    echo 'Please export OPENSSL_SIGN_KEY=/path/to/private_rsa_key.pem.'
    echo 'You get this key by exporting the RSA from the gpg key:'
    echo '$ gpg --list-secret-keys --with-keygrip'
    echo '$ gpgsm --gen-key -o cert-gpg-keygrip.cert'
    echo '> choose 2 (Existing Key) and create a self signed certificate.'
    echo '$ gpgsm --import cert-gpg-keygrip.cert'
    echo '$ gpgsm --list-keys'
    echo '$ gpgsm -o cert.p12 --export-secret-key-p12 ${GPGSM_KEY_ID}'
    echo '$ openssl pkcs12 -in cert.p12 -nocerts -out priv-gpg-rsa.key'
    echo '$ export OPENSSL_SIGN_KEY=$(pwd)/priv-gpg-rsa.key'
    echo 'please do not forget to cleanup afterwards!'
    exit 1
fi

# win32 must be the last in the list, otherwise invalid json is created
declare -a download_os_list=(
    archlinux
    debian8
    debian9
    fedora24
    fedora25
    fedora27
    osx
    source
    ubuntu16.04
    ubuntu17.10
    win32
)

# Compile the latest version from the latest source file and extract the
# contents of the changelog.
pushd source > /dev/null
latest_source=$(ls -1 | sort -V | grep 'tar.bz2' | tail -n 1)
latest_version=$(echo "${latest_source}" \
    | sed 's|netsplice-\(.*\).tar.bz2|\1|;') #'
latest_changelog=$(cat "netsplice-${latest_version}.changelog" 2>/dev/null \
    || echo "no documented changes")
latest_changelog=${latest_changelog//$'\n'/\\n}
# source
popd > /dev/null

# Build the JSON with all registered os's and create a signature for each file
echo '{' > "${UPDATE_FILE}"
echo '"version":"'"${latest_version}"'",' >> "${UPDATE_FILE}"
echo '"date":"'"${UPDATE_DATE}"'",' >> "${UPDATE_FILE}"
echo '"changelog":"'"${latest_changelog}"'",' >> "${UPDATE_FILE}"
echo '"update_url_list":[' >> "${UPDATE_FILE}"
for os in ${download_os_list[@]}; do
    # create the value for a update_url_item
    # os, url, signature
    if [ ! -d "${os}" ]; then
        echo "Skipping ${os}"
        continue
    fi
    echo "Signing ${os}"
    pushd "${os}" > /dev/null
    os_file=$(ls -1 | grep "${latest_version}" \
        | grep -v 'dsc\|sha256\|changes\|tar.gz\|changelog')
    url="${DOWNLOAD_ROOT}/${os}/${os_file}"
    b64sig=$(openssl dgst -sha1 \
        -sign "${OPENSSL_SIGN_KEY}" \
        "${os_file}" | base64 -w0)
    sha1=$(sha1sum "${os_file}" | awk '{print $1}')
    sha256=$(sha256sum "${os_file}" | awk '{print $1}')
    # ${os}
    popd > /dev/null
    # write out the JSON
    echo -n \
        '{"os":"'"${os}"'","url":"'"${url}"'"' >> "${UPDATE_FILE}"
    echo -n ',"signature":"'"${b64sig}"'"' >> "${UPDATE_FILE}"
    echo -n ',"sha1sum":"'"${sha1}"'"' >> "${UPDATE_FILE}"
    echo -n ',"sha256sum":"'"${sha256}"'"' >> "${UPDATE_FILE}"
    if [ "${os}" != "win32" ]; then
        echo '},' >> "${UPDATE_FILE}"
    else
        echo '}' >> "${UPDATE_FILE}"
    fi
done
echo ']' >> "${UPDATE_FILE}"
echo '}' >> "${UPDATE_FILE}"

echo "Signing update file"
# Sign the JSON file
openssl dgst -sha1 \
    -sign "${OPENSSL_SIGN_KEY}" \
    "${UPDATE_FILE}" \
    | base64 -w0 \
    > "${UPDATE_FILE}.sig"
