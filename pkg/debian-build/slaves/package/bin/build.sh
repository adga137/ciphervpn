#!/bin/bash

source /usr/local/bin/build.sh.inc
set -x
# takes the current source of /var/src
# copy files to /var/tmp/product-version

mkdir -p \
  ${BUILDDIR} \
  ${TMPDIR}/clean_copy \
  ${TMPDIR}/pack_copy/debian \
  ${TMPDIR}/compile_copy/debian \
  || die 'cannot create directories'

cp -r ${SOURCEDIR}/doc \
  ${SOURCEDIR}/src \
  ${SOURCEDIR}/res \
  ${SOURCEDIR}/pkg \
  ${SOURCEDIR}/.git \
  ${TMPDIR}/clean_copy \
  || die 'cannot copy directories'

cp ${SOURCEDIR}/.gitattributes \
  ${SOURCEDIR}/.gitignore \
  ${SOURCEDIR}/CONTRIBUTING.rst \
  ${SOURCEDIR}/HISTORY.rst \
  ${SOURCEDIR}/LICENSE \
  ${SOURCEDIR}/MANIFEST.in \
  ${SOURCEDIR}/Makefile \
  ${SOURCEDIR}/README.rst \
  ${SOURCEDIR}/setup.cfg \
  ${SOURCEDIR}/setup.py \
  ${SOURCEDIR}/versioneer.py \
  ${TMPDIR}/clean_copy \
  || die 'cannot create files'

pushd ${TMPDIR}/clean_copy
make clean > /dev/null || die 'make clean failed'
popd

rsync -a ${TMPDIR}/clean_copy/ ${TMPDIR}/compile_copy \
  || die 'cannot duplicate clean_copy for compile/modify processing'
rsync -a ${TMPDIR}/clean_copy/ ${TMPDIR}/pack_copy \
  || die 'cannot duplicate clean_copy for package processing'

rm -r ${TMPDIR}/pack_copy/.git

# for now only satisfy buildchain
touch ${TMPDIR}/pack_copy/NEWS
touch ${TMPDIR}/pack_copy/README

cp ${TMPDIR}/clean_copy/pkg/debian-build/slaves/sysdebian9/config/*\
   ${TMPDIR}/compile_copy/debian \
  || die 'cannot copy debian build rules'

pushd ${TMPDIR}/compile_copy
# cleanup compile copy's root directory to avoid 'dirty' versions
# keep dirty files in any subdir
git status --short | grep -v '/' | sed 's|^.. ||' | xargs -iX git checkout X
popd
# create changelog from git

# copy packaging scripts for different platforms

pushd ${TMPDIR}/compile_copy > /dev/null
python setup.py build > /dev/null || die 'setup.py failed to run'
make gui  > /dev/null || die 'make gui failed'
make codedoc > ${TMPDIR}/pack_copy/doc/developer/autocode.rst
cp ${TMPDIR}/compile_copy/build/lib.linux-x86_64-2.7/netsplice/_version.py \
   ${TMPDIR}/pack_copy/src/netsplice/_version.py \
   || die 'versioneer version.py cloud not be added'

make version-nsh > ${TMPDIR}/pack_copy/pkg/version.nsh \
  || die 'version.nsh could not be added'

find . -type f | grep '_ui\.py$\|_rc\.py$' \
  | xargs -iX sed -i 's|# Created:.*|# Created:|' X

find . -type f | grep '_ui\.py$\|_rc\.py$' \
  | sed 's|^\./||' \
  | xargs -iX echo "cp ${TMPDIR}/compile_copy/X ${TMPDIR}/pack_copy/X" \
  | bash

popd > /dev/null

pushd ${TMPDIR}/compile_copy/debian > /dev/null
git_changelog.sh \
  || die 'changelog could not be created from git'

cp changelog ${TMPDIR}/pack_copy/CHANGES
cp * ${TMPDIR}/pack_copy/debian
popd > /dev/null

# prepare the sourcecode to be complete (eg uic generated)
# this runs in the copy to avoid that version 'build' has modified the sources
# make gui will create new files only
pushd ${TMPDIR}/pack_copy > /dev/null

popd > /dev/null

pushd ${TMPDIR}/pack_copy/pkg > /dev/null
ls -1 pyinst | grep 'spec.in' | sed 's|\.spec\.in$||' \
  | xargs -iX echo 'bash ./update_hidden_controller.sh pyinst/X.spec.in > pyinst/X.spec' \
  | bash \
  || die 'failed to create specs'
rm ./update_hidden_controller.sh
rm ./pyinst/*.spec.in
popd > /dev/null

SOURCE_DATE=$(dpkg-parsechangelog --show-field=Date -l${TMPDIR}/pack_copy/CHANGES)
find ${TMPDIR}/pack_copy -newermt "${SOURCE_DATE}" -print0 \
  | xargs -0r touch --no-dereference --date="${SOURCE_DATE}" \
  || die 'date could not be set'

rsync -a ${TMPDIR}/pack_copy/ ${BUILDDIR}

# checksum sourcecode

pushd ${BUILDDIR} > /dev/null
find . -type f -exec sha256sum {} \; \
  > ${INSTALLABLESDIR}/${PRODUCT}-${VERSION}-packaged-sources.sha256 \
  || die 'checksum failed'
popd > /dev/null


# pack sources with version-path
pushd ${BUILDDIR} > /dev/null
cd ..
tar cjf ${INSTALLABLESDIR}/${PRODUCT}-${VERSION}.tar.bz2 ${PRODUCT}-${VERSION} \
  || die 'tar packaging failed'
popd > /dev/null

ls -la ${INSTALLABLESDIR}/${PRODUCT}-${VERSION}*
