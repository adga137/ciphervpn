#!/bin/bash

# Create a changelog from the git history
# Creates a separator for each git tag
# The latest version

last_release_tag=
version=$(cd /var/src && make version)
clversion=$(echo ${version} | sed 's|\+\([0-9]*\).*$|-\1|')
release=$(echo ${version} | sed 's|.*\+\([0-9]*\).*$|\1|')
user_id="autobuild <build@ipredator.se>"

function get_log() {
  start_tag=$1
  end_tag=$2
  if [ "${start_tag}" == "" ]; then
    git log --pretty=oneline --no-merges ${end_tag} \
      > taglog
  else
    git log --pretty=oneline --no-merges ${start_tag}..${end_tag} \
      > taglog
  fi
  log_date=$(cat taglog | head -n 1 | awk '{print $1}' | xargs -iX echo 'git show X --date=iso| head -n 4 | grep Date:|sed "s|Date:||"' | bash)
  sign_date=$(date +"%a, %d %b %Y %T %z" --date "${log_date}")

  # Remove sha-id's and cut long lines
  cat taglog | sed 's|^[a-z0-9]* |  * |; s|^\(....................................................................................\).*|\1|g' \
    > filteredlog
  if [ 0 == $(cat filteredlog | wc | awk '{print $1}') ]; then
    echo '  * [dev] No commits yet'
  else
    cat filteredlog
  fi
  echo
  echo " -- ${user_id}  ${sign_date}"
  echo
}

release_tags=($(git tag | sort -V -r ))
for index in "${!release_tags[@]}";
do
  release_tag=${release_tags[$index]}
  prev_tag=
  if [ $index -lt ${#release_tags[@]} ]; then
    prev_tag=${release_tags[$((index+1))]}
  fi
  if [ ${index} -eq 0 ]; then
    if [ -z ${release} ]; then
      echo "netsplice (${clversion}) unstable; urgency=low" >> newlog
      get_log "${release_tag}" '' >> newlog
    fi
  fi
  echo "netsplice (${release_tag}) stable; urgency=low" >> newlog
  get_log "${prev_tag}" "${release_tag}" >> newlog
done

mv newlog changelog
