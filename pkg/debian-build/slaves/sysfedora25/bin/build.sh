#!/bin/bash

source /usr/local/bin/build.sh.inc

unpack

create_virtualenv

# make required steps to build the installer binaries
cd ${BUILDDIR}/pkg/debian-build/slaves/sysfedora25 \
  || die 'could not change to debian pkg directory'
make all \
  || die 'could not build binaries'
