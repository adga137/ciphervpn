#!/bin/bash

# ci script to build from current package
# executed on osx, so the following script
# may not be portable
# usage:
#  default (usually by ci using build.sh): sh build-slave.sh
#  pkg-debug: VERSION=9.9.9 SOURCE=YES sh build-slave.sh
set -x
cd $(dirname $0)
source ./slave-config

function die() {
  echo $@
  exit 1
}
PRODUCT=netsplice

if [ -z ${VERSION} ]; then
  # ci-usage
  # pull repository to fetch latest sources
  # and create version from that sources
  pushd ${root_repo}
  git fetch --all
  git checkout master
  git pull
  export VERSION=$(make version)
  export USE_CI_PACKAGE=YES
else
  if [ "${SOURCE}" == "YES" ]; then
    USE_CI_PACKAGE=NO
  else
    USE_CI_PACKAGE=YES
  fi
fi
export PRODUCTVERSION=${PRODUCT}-${VERSION}


mkdir -p ${root_rw}
if [ "${USE_CI_PACKAGE}" == "YES" ]; then
  # download a package like all the other slaves do
  # this package is prepared with all files required
  # the git should be synced with that command, producing the same version-ids
  ssh -p ${installable_host_port} ${installable_user}@${installable_host} \
    "cd ${installable_volume}/../../pkg/debian-build && pwd && git pull && git tag -l && docker-compose run --rm slavepackage"

  if [ ! -f ${root_rw}/${PRODUCTVERSION}.tar.bz2 ]; then
    scp -P ${installable_host_port} \
      ${installable_user}@${installable_host}:${installable_volume}/source/${PRODUCTVERSION}.tar.bz2 \
      ${root_rw} \
      || die 'could not download latest source package'
  fi
  if [ ! -f ${root_rw}/openvpn-build.tar.bz2 ]; then
    scp -P ${installable_host_port} \
      ${installable_user}@${installable_host}:${distfiles_volume}/openvpn-build.tar.bz2 \
      ${root_rw} \
      || die 'could not download latest openvpn distfiles'
  fi
  if [ ! -f ${root_rw}/tor-build.tar.bz2 ]; then
    scp -P ${installable_host_port} \
      ${installable_user}@${installable_host}:${distfiles_volume}/tor-build.tar.bz2 \
      ${root_rw} \
      || die 'could not download latest tor distfiles'
  fi
  scp -P ${installable_host_port} \
    ${installable_user}@${installable_host}:${distfiles_volume}/netsplice*.tar.gz \
    ${root_rw} \
    || die 'could not download latest netsplice subpackages distfiles'

  pushd ${root_rw}
    tar xjf ${PRODUCTVERSION}.tar.bz2 \
      || die 'could not unpack latest source package'
  popd
else
  # use a local repository
  LOCAL_PATH=/data/develop/projects/netsplice
  rm -rf ${root_rw}/pkg ${root_rw}/doc ${root_rw}/res ${root_rw}/src
  rsync -r ${LOCAL_PATH}/* ${root_rw}
fi

pushd ${root_rw}

if [ "${USE_CI_PACKAGE}" == "YES" ]; then
  pushd ${PRODUCTVERSION}
fi

  # requires brew install python, otherwise: no TLS1.2
  virtualenv --python=/usr/local/bin/python2 --system-site-package venv/pkg \
    || die 'virtualenv could not be setup'
  source venv/pkg/bin/activate \
    || die 'virtualenv could not be activated'
  pip install -r pkg/requirements.pip \
    || die 'requirements could not be installed'
  pip install -r pkg/requirements-pkg.pip \
    || die 'requirements-pkg could not be installed'
  pip install --find-links ${root_rw} -r pkg/requirements-netsplice.pip \
    || die 'requirements-netsplice could not be installed'
  touch venv/pkg/lib/python2.7/site-packages/evasion/__init__.py
  # pyside-rcc or uic makes magic on OSX to display the menu correctly
  # re-run make gui to ensure correct results
  cp /usr/local/lib/python2.7/site-packages/PySide/pyside-rcc venv/pkg/bin
  # Install M2Crypto
  # requires brew install swig
  env LDFLAGS="-L$(brew --prefix openssl)/lib" \
    CFLAGS="-I$(brew --prefix openssl)/include" \
    SWIG_FEATURES="-cpperraswarn -includeall -I$(brew --prefix openssl)/include" \
    pip install m2crypto || die "install M2Crypto failed"

  make clean # || die 'make clean failed'
  make gui || die 'make gui failed'

if [ "${USE_CI_PACKAGE}" == "NO" ]; then
  # when running multiple times from a git-checkout, create the spec-files
  # and gui files to get correct binaries - essentially mocking slaves/package
  pushd pkg
  ls -1 pyinst | grep 'spec.in' | sed 's|\.spec\.in$||' \
    | xargs -IX echo 'bash ./update_hidden_controller.sh pyinst/X.spec.in > pyinst/X.spec' \
    | bash \
    || die 'update_hidden_controller is required for source-tree builds'
  popd
  # copy a _version.py to avoid issues with code that is dependent
  fake_version=$(echo '
import json
version_json = '"'"'{"date": "2017-01-31T17:40:26+0100","dirty": true,"error": null,"full-revisionid": "9999","version": "'${VERSION}'"}'"'"'
def get_versions():
    return json.loads(version_json)
')
  echo "${fake_version}" > src/netsplice/_version.py
  # copy pyside-rcc, otherwise make gui will fail
  cp var/venv/ns-pkg/bin/pyside-rcc venv/pkg/bin
  make gui || die 'make gui failed'
fi

# This hands over the control to the makefile that builds the executables
# compiles the app directory and the associated binaries.
# This command requires to be run in a virtualenv in a prepared source-tree
pushd pkg/debian-build/slaves/sysosx
  make all PATH=$PATH DISTFILESDIR=${root_rw}\
    || die 'make all failed'
  deactivate
popd # pkg/debian-build/slaves/sysosx
popd # ${root_rw}

if [ "${USE_CI_PACKAGE}" == "YES" ]; then
  # Upload the result to the service
  scp -P ${installable_host_port} \
    ${root_rw}/installables/${PRODUCTVERSION}.dmg \
    ${installable_user}@${installable_host}:${installable_volume}/osx \
    || die 'could not upload compiled package'
fi
