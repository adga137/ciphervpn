#!/bin/bash

source /usr/local/bin/build.sh.inc

export VIRTUALENV_EXECUTABLE=virtualenv2

unpack

create_virtualenv

# make required steps to build the installer binaries
cd ${TMPDIR}/${PRODUCT}-${VERSION}/pkg/debian-build/slaves/sysarchlinux \
  || die 'could not change to archlinux pkg directory'

# clean a previous build to allow rebuilds, on clean machines this will fail
# with USE_GIT_SOURCE it will ensure that the pkg is always built
rm ${TMPDIR}/bin/${PRODUCT}-${VERSION}-1-x86_64.pkg.tar.xz 2>/dev/null

make all \
  || die 'could not build binaries'

# export the results to the shared storage so jenkins can access it again
cp ${TMPDIR}/bin/${PRODUCT}-${VERSION}-1-x86_64.pkg.tar.xz \
   ${INSTALLABLESDIR} \
   || die 'archlinux package could not be copied'
