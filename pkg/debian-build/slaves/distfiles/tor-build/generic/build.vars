# -*- mode: sh; -*-

# Configuration for the build script
# contains variables that may be preset in the environment

BUILD_VERSION="001"

OPENSSL_VERSION="${OPENSSL_VERSION:-1.0.2o}"
LIBRESSL_VERSION="${LIBRESSL_VERSION:-2.6.4}"

LIBEVENT_VERSION="${LIBEVENT_VERSION:-2.1.8-stable}"
ZLIB_VERSION="${ZLIB_VERSION:-1.2.11}"

TOR_VERSION="${TOR_VERSION:-0.3.2.10}"

OPENSSL_URL="${OPENSSL_URL:-https://www.openssl.org/source/openssl-${OPENSSL_VERSION}.tar.gz}"
OPENSSL_SIG_URL="${OPENSSL_SIG_URL:-https://www.openssl.org/source/openssl-${OPENSSL_VERSION}.tar.gz.asc}"

LIBRESSL_URL="${LIBRESSL_URL:-https://ftp.spline.de/pub/OpenBSD/LibreSSL/libressl-${LIBRESSL_VERSION}.tar.gz}"
LIBRESSL_SIG_URL="${LIBRESSL_SIG_URL:-https://ftp.spline.de/pub/OpenBSD/LibreSSL/libressl-${LIBRESSL_VERSION}.tar.gz.asc}"

LIBEVENT_URL="${LIBEVENT_URL:-https://github.com/libevent/libevent/releases/download/release-${LIBEVENT_VERSION}/libevent-${LIBEVENT_VERSION}.tar.gz}"
LIBEVENT_SIG_URL="${LIBEVENT_SIG_URL:-https://github.com/libevent/libevent/releases/download/release-${LIBEVENT_VERSION}/libevent-${LIBEVENT_VERSION}.tar.gz.asc}"

ZLIB_URL="${ZLIB_URL:-https://zlib.net/zlib-${ZLIB_VERSION}.tar.gz}"
ZLIB_SIG_URL="${ZLIB_SIG_URL:-https://zlib.net/zlib-1.2.11.tar.gz.asc}"

TOR_URL="${TOR_URL:-https://www.torproject.org/dist/tor-${TOR_VERSION}.tar.gz}"
TOR_SIG_URL="${TOR_SIG_URL:-https://www.torproject.org/dist/tor-${TOR_VERSION}.tar.gz.asc}"

LIBEVENT_KEY_ID=B86086848EF8686D
LIBRESSL_KEY_ID=4B708F96
OPENSSL_KEY_ID=0E604491
TOR_KEY_ID_NM=6AFEE6D49E92B601
TOR_KEY_ID_RD1=19F78451
TOR_KEY_ID_RD2=28988BF5
ZLIB_KEY_ID=58BCAFBA

#CHOST
#CTARGET
#CBUILD
#IMAGEROOT
#BUILDROOT
#SOURCESROOT

WGET="${WGET:-wget}"
CURL="${CURL:-curl}"
MAKE="${MAKE:-make}"

#WGET_OPTS
CURL_OPTS="${CURL_OPTS:---progress-bar --verbose --remote-name -L}"
MAKEOPTS="${MAKEOPTS:--j5}"
#DO_NO_STRIP
#DO_STATIC=
if [ -n "${DO_REALLY_STATIC}" ]; then
	DO_STATIC=1
	export LDFLAGS="-Xcompiler -static"
fi
USE_OPENSSL="${USE_OPENSSL:-1}"
if [ -n "${USE_LIBRESSL}" ]; then
  unset USE_OPENSSL
else
  USE_OPENSSL=1
fi

EXTRA_OPENSSL_CONFIG="${EXTRA_OPENSSL_CONFIG:--static-libgcc}" # uncomment if tor.exe fails to start with missing libgcc_s_sjlj-1.dll (win32)
EXTRA_LIBRESSL_CONFIG="${EXTRA_LIBRESSL_CONFIG:-}"
#EXTRA_LIBEVENT_CONFIG
#EXTRA_ZLIB_CONFIG
#EXTRA_TOR_CONFIG

# EXTRA_TARGET_CFLAGS="-Wl,--dynamicbase,--nxcompat"
EXTRA_TARGET_CFLAGS=""

# You can enable --high-entropy-va for 64-bit builds if you have recent enough
# mingw-w64
#EXTRA_TARGET_CFLAGS="-Wl,--dynamicbase,--nxcompat,--high-entropy-va"

TARGET_ROOT="${TARGET_ROOT:-/}"
