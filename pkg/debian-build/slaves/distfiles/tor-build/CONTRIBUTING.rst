CONTRIBUTING TO TOR-BUILD
=========================

The suggested way to contribute is to fork the repo and issue a pull request on 
GitHub. Alternatively you can send your patch to tor-devel mailing list:

- <https://lists.torproject.org/cgi-bin/mailman/listinfo>

The subject line should look like this:

  [PATCH: tor-build] summary of the patch

To avoid merging issues patches should be created with git-format-patch or sent 
using git-send-email. The easiest way to add the subject line prefix is to use 
this option:

  --subject-prefix='PATCH: tor-build'

Please try to split large patches into small, atomic pieces to make reviews 
easier.

If you want quick feedback on a patch, you can visit the #tor-dev channel 
on OFTC.