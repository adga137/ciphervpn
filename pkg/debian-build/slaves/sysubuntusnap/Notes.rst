Evaluation
==========

Plus: package description by convention. expects a pip-able package

Minus: alot of network traffic during build (2 updates: first before, does not
assume a apt-clean environment, then during after evaluating the required
packages). Amount of downloads can be minimized by making the container more
featured.

Snaps not easy to install. Terminal access required. +73mb download on live system.