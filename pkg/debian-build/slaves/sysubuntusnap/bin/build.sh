#!/bin/bash

source /usr/local/bin/build.sh.inc

unpack


# prepare builddir
pushd ${BUILDDIR}

cat ${TMPDIR}/snapcraft.yaml.in \
  | sed "s|DEBCLVERSION|${DEBCLVERSION}|" \
  | sed "s|PRODUCTVERSION|${PRODUCTVERSION}|" \
  > snapcraft.yaml

# snapcraft needs a up to date apt-cache
apt-get update \
  || die 'ubuntu apt-cache could not be built'

# then it takes a while to build the installer
# ~3min snapping only
# >30min for clean build
snapcraft \
  || die 'snap could not be created'
mv ${PRODUCT}_${DEBCLVERSION}_amd64.snap ${INSTALLABLESDIR} \
  || die 'installer could not be moved'
