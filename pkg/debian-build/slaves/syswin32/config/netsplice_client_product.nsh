!define APPNAME "Netsplice"
!define COMPANYNAME "IPredator.se"
!define DESCRIPTION "Netsplice is a multiplatform desktop client for VPN services."
# These will be displayed by the "Click here for support information" link in "Add/Remove Programs"
# It is possible to use "mailto:" links in here to open the email client
!define HELPURL "https://ipredator.se/help" # "Support Information" link
!define UPDATEURL "https://ipredator.se/netsplice" # "Product Updates" link
!define ABOUTURL "https://ipredator.se/" # "Publisher" link