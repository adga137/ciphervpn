# pwd is a ro-mounted source-tree that had all dependencies build into
# package-name directories
!define PKGNAMEPATH /var/tmp/executables
# These are generated for each build by build.sh and are mapped into the source directory
!include /var/src/pkg/version.nsh
!include .\netsplice_client_product.nsh
!include ${PKGNAMEPATH}\netsplice_install_files_size.nsh

!define OPENVPN_CONFIG_EXT_WIN "ovpn"
!define OPENVPN_CONFIG_EXT_UNIX "conf"

RequestExecutionLevel admin ;Require admin rights on NT6+ (When UAC is turned on)

InstallDir "$PROGRAMFILES\${APPNAME}"

LicenseData "license.rtf"
Name "${COMPANYNAME} - ${APPNAME}"
Icon "/var/src/res/images/netsplice.ico"

# /var/installables is a rw mounted volume
outFile "/var/installables/${PKGNAME}-${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}-${VERSIONRELEASE}.exe"

!include LogicLib.nsh


# Just three pages - license agreement, install location, and installation
page license
page directory
page instfiles

!macro VerifyUserIsAdmin
UserInfo::GetAccountType
pop $0
${If} $0 != "admin" ;Require admin rights on NT4+
        messageBox mb_iconstop "Administrator rights required!"
        setErrorLevel 740 ;ERROR_ELEVATION_REQUIRED
        quit
${EndIf}
!macroend

function StrRep

    ;Written by dirtydingus 2003-02-20 04:30:09
    ; USAGE
    ;Push String to do replacement in (haystack)
    ;Push String to replace (needle)
    ;Push Replacement
    ;Call StrRep
    ;Pop $R0 result

        Exch $R4 ; $R4 = Replacement String
        Exch
        Exch $R3 ; $R3 = String to replace (needle)
        Exch 2
        Exch $R1 ; $R1 = String to do replacement in (haystack)
        Push $R2 ; Replaced haystack
        Push $R5 ; Len (needle)
        Push $R6 ; len (haystack)
        Push $R7 ; Scratch reg
        StrCpy $R2 ""
        StrLen $R5 $R3
        StrLen $R6 $R1
    loop:
        StrCpy $R7 $R1 $R5
        StrCmp $R7 $R3 found
        StrCpy $R7 $R1 1 ; - optimization can be removed if U know len needle=1
        StrCpy $R2 "$R2$R7"
        StrCpy $R1 $R1 $R6 1
        StrCmp $R1 "" done loop
    found:
        StrCpy $R2 "$R2$R4"
        StrCpy $R1 $R1 $R6 $R5
        StrCmp $R1 "" done loop
    done:
        StrCpy $R3 $R2
        Pop $R7
        Pop $R6
        Pop $R5
        Pop $R2
        Pop $R1
        Pop $R4
        Exch $R3

functionEnd

!macro ReplaceSubStr OLD_STR SUB_STR REPLACEMENT_STR
    Push $R0

    Push "${OLD_STR}" ;String to do replacement in (haystack)
    Push "${SUB_STR}" ;String to replace (needle)
    Push "${REPLACEMENT_STR}" ; Replacement
    Call StrRep
    Pop $R0 ;result
    StrCpy ${OLD_STR} $R0

    Pop $R0
!macroend

function .onInit
    setShellVarContext all
    !insertmacro VerifyUserIsAdmin
functionEnd

section "TAP Virtual Ethernet Adapter" SecTAP
    SetOverwrite on
    SetOutPath "$TEMP"
    File /oname=tap-windows.exe "/var/tmp/executables/openvpn/tap-windows.exe"

    DetailPrint "Installing TAP (may need confirmation)..."
    nsExec::ExecToLog '"$TEMP\tap-windows.exe" /S /SELECT_UTILITIES=1'
    Pop $R0 # return value/error/timeout

    # Create additional ethernet adapters
    nsExec::ExecToLog '"$SMPROGRAMS\TAP-Windows\Add a new TAP virtual ethernet adapter"'
    Pop $R0 # return value/error/timeout

    # Get the install location of the TAP-Windows driver and binaries
    ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\TAP-Windows" "UninstallString"
    var /GLOBAL TAP_PATH
    ${If} $R0 != ""
        DetailPrint "Installing additional TAP devices"
        StrCpy $TAP_PATH $R0
        Pop $R0
    ${Else}
        # on x64 windows the uninstall location needs to be accessed using WOW
        SetRegView 64
        ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\TAP-Windows" "UninstallString"
        SetRegView 32
        ${If} $R0 != ""
            StrCpy $TAP_PATH $R0
            Pop $R0
        ${EndIf}
    ${EndIf}
    !insertmacro ReplaceSubStr $TAP_PATH "\Uninstall.exe" ""

    # Add 3 additional tap devices.
    # Each device takes ~2s to install
    DetailPrint "To have more than 3 active connections, create more devices manually in the StartMenu."
    ${For} $R1 1 3
        DetailPrint "Adding TAP0$R1 network interface using $TAP_PATH\bin\tapinstall.exe"
        nsExec::ExecToLog '"$TAP_PATH\bin\tapinstall.exe" "install" "$TAP_PATH\driver\OEMVista.inf" "tap0901"'
        Pop $R0
    ${Next}

    Delete "$TEMP\tap-windows.exe"
    WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "tap" "installed"
sectionEnd

Section /o "${COMPANYNAME} ${APPNAME}File Associations" SecFileAssociation
    WriteRegStr HKCR ".${OPENVPN_CONFIG_EXT_WIN}" "" "OpenVPN Config File"
    WriteRegStr HKCR ".${OPENVPN_CONFIG_EXT_UNIX}" "" "OpenVPN Config File"
    WriteRegStr HKCR "${COMPANYNAME} ${APPNAME}File" "" "OpenVPN Config File"
    WriteRegStr HKCR "${COMPANYNAME} ${APPNAME}File\shell" "" "open"
    WriteRegStr HKCR "${COMPANYNAME} ${APPNAME}File\DefaultIcon" "" "$INSTDIR\icon.ico,0"
    WriteRegStr HKCR "${COMPANYNAME} ${APPNAME}File\shell\open\command" "" '"$INSTDIR\bin\NetspliceGuiApp.exe" --import "%1"'
SectionEnd

section "install"
    setOutPath $INSTDIR

    file "/var/src/res/images/netsplice.ico"
    !include ${PKGNAMEPATH}/netsplice_install_files.nsh

    # DetailPrint "Installing Service..."
    # nsExec::ExecToLog '"$INSTDIR\NetspliceBackendApp.exe" --install'

    # Uninstaller - See function un.onInit and section "uninstall" for configuration
    writeUninstaller "$INSTDIR\uninstall.exe"

    # Start Menu
    # ensure that the app working directory is ${INSTDIR}
    setOutPath $INSTDIR
    createDirectory "$SMPROGRAMS\${COMPANYNAME}"
    createShortCut "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}.lnk" "$INSTDIR\NetspliceGuiApp.exe" "" "$INSTDIR\netsplice.ico"

    !include netsplice_client_registry_install.nsh
sectionEnd

# Uninstaller

function un.onInit
    SetShellVarContext all
    !insertmacro VerifyUserIsAdmin
functionEnd

section "uninstall"

    delete "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}.lnk"
    # Try to remove the Start Menu folder - this will only happen if it is empty
    rmDir "$SMPROGRAMS\${COMPANYNAME}"

    # Remove files
    delete $INSTDIR\netsplice.ico
    !include ${PKGNAMEPATH}/netsplice_uninstall_files.nsh

    # Remove TAP Drivers
    ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "tap"
    ${If} $R0 == "installed"
        DetailPrint "Uninstalling TAP as we installed it. This removes the additional TAP Adapters as well..."
        ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\TAP-Windows" "UninstallString"
        ${If} $R0 != ""
            DetailPrint "Uninstalling TAP..."
            nsExec::ExecToLog '"$R0" /S'
            Pop $R0 # return value/error/timeout
        ${Else}
            # on x64 windows the uninstall location needs to be accessed using WOW
            SetRegView 64
            ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\TAP-Windows" "UninstallString"
            SetRegView 32
            ${If} $R0 != ""
                DetailPrint "Uninstalling TAP 64..."
                nsExec::ExecToLog '"$R0" /S'
                Pop $R0 # return value/error/timeout
            ${EndIf}
        ${EndIf}
        DeleteRegValue HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "tap"
    ${EndIf}

    # Always delete uninstaller as the last action
    delete $INSTDIR\uninstall.exe

    # Try to remove the install directory - this will only happen if it is empty
    rmDir $INSTDIR

    # Remove uninstaller information from the registry
    DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}"
sectionEnd
