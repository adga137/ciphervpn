#!/bin/bash

# Render Tor prepared for installer
# ================================================
#
# Requires:
#  - a Linux host with mingw installed
#  - a rw directory mounted to /var/build
# Returns nonzero exit code when failed.
#
# clone tor-build repository
# runs cross-compile build
# - downloads Tor dependencies
# - compiles
# Copy files to executables so they can be installed
# Cleans up (remove read-write copy)
# The location where the Tor binaries are placed.

set -x
set -o pipefail
export VERSION=not_required
source /usr/local/bin/build.sh.inc
# OpenSSL 1.1 causes compile errors (2018-03)
export OPENSSL_VERSION=${OPENSSL_LEGACY_VERSION}

absolute_executable_path=${TMPDIR}/executables
temporary_build_path=${TMPDIR}/tor

# Cleanup the temporary build path for subsequent executes.
function cleanup() {
  rm -r ${temporary_build_path} 2>/dev/null
}

# Build Tor source.
function buildSource() {
  export TOR_VERSION=$1
  TOR_VERSION_ONLY=$(echo ${TOR_VERSION} | sed 's|\([^-]*\)-.*|\1|')
  TOR_SPECIAL_ONLY=$(echo ${TOR_VERSION} | sed 's|[^-]*-\(.*\)|\1|')
  pushd ${temporary_build_path}/tor-build/generic
  echo "Build Version: ${TOR_VERSION}"
  CHOST=i686-w64-mingw32 \
  CBUILD=i686-pc-linux-gnu \
  TOR_VERSION=${TOR_VERSION_ONLY} \
  USE_LIBRESSL=$(echo ${OPENVPN_SPECIAL_ONLY} | grep 'libressl') \
  USE_WIN32=1 \
  ./build 2>&1 | tee build-${TOR_VERSION}.log \
    || die "build tor ${TOR_VERSION} from source failed"
  mkdir -p ${absolute_executable_path}/libexec/tor/v${TOR_VERSION}/tor \
  && cp -r image/tor/* \
    ${absolute_executable_path}/libexec/tor/v${TOR_VERSION}/tor \
  && cp build-${TOR_VERSION}.log \
    ${absolute_executable_path}/libexec/tor/v${TOR_VERSION}/tor \
  && cp /usr/lib/gcc/i686-w64-mingw32/4.9-win32/libgcc_s_sjlj-1.dll \
    ${absolute_executable_path}/libexec/tor/v${TOR_VERSION}/tor/bin \
  && cp /usr/lib/gcc/i686-w64-mingw32/4.9-win32/libssp-0.dll \
    ${absolute_executable_path}/libexec/tor/v${TOR_VERSION}/tor/bin \
  || die "copy of compiled files failed"
  # add shasums of downloaded / used sources in build log
  sha256sum sources
  popd
  # remove files that are not required for distribution
  pushd ${absolute_executable_path}/libexec/tor/v${TOR_VERSION}/tor/bin
  rm c_rehash event_rpcgen.py openssl openssl.exe tor-gencert.exe tor-resolve.exe
  popd
}

# Prepare read-write copy.
function prepareBuildPath() {
  export TOR_VERSION=$1
  TOR_VERSION_ONLY=$(echo ${TOR_VERSION} | sed 's|\([^-]*\)-.*|\1|')
  cleanup
  mkdir -p ${temporary_build_path}
  pushd ${temporary_build_path}
  cp "${DISTFILESDIR}/tor-build.tar.bz2" . || die 'no tor build package available'
  tar xjf tor-build.tar.bz2 || die 'cannot unpack tor-build sources'

  cp ${SOURCEDIR}/pkg/libexec/tor/v${TOR_VERSION}/* \
    tor-build/generic/patches \
    || echo "building tor without patches"
  popd
}

# Display failure message and emit non-zero exit code.
function die() {
  echo "die:" $@
  exit 1
}

function main() {
  prepareBuildPath ${BUILD_TOR_VERSION}
  fetchTapWindows
  buildSource ${BUILD_TOR_VERSION}
  cleanup
}

main $@
