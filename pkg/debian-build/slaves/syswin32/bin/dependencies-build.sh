#!/bin/bash

# Render dependencies into separate subdirectories
# ================================================
#
# Requires
#  - a linux host with wine, wine with python and mingw installed
#  - the sourcecode mounted to /var/src/
#  - a rw directory mounted to /var/build
# Returns nonzero exit code when pinstaller failed.
#
# Prepares a read-write copy of the sourcecode.
# Executes qt-uic and qt-rcc for gui dialogs.
# Installs dependencies from pkg/dependencies-windows.pip.
# Runs pyinstaller.
# Cleans up (remove wine-dlls, remove read-write copy).
# Creates nsis install/uninstall scripts for the files for each package.

source /usr/local/bin/build.sh.inc

product=${PRODUCT}
# The location where the pyinstaller results are placed
absolute_executable_path=${TMPDIR}/executables
# The location of the nsis installer nis files dictates the path of the files
relative_executable_path=${TMPDIR}/executables
win_executable_path=$(echo "Z:/${relative_executable_path}" | sed 's|/|\\\\|g')
win_work_path=$(echo "Z:/${TMPDIR}" | sed 's|/|\\\\|g')
win_path=$(echo "Z:/${WINBUILDDIR}" | sed 's|/|\\\\|g')
win_distfiles=$(echo "Z:/${DISTFILESDIR}"  | sed 's|/|\\\\|g')
source_ro_path=${WINBUILDDIR}
temporary_build_path=${TMPDIR}/dependencies

setups=($(ls -1 ${source_ro_path}/pkg/pyinst \
    | grep '.spec$' | sed 's|.spec$||'))

# Cleanup the temporary build path for subsequent executes
function cleanup() {
  rm -r ${temporary_build_path} 2>/dev/null
}

# Create installable binaries with dlls
function createInstallables() {
  #rm -r ${absolute_executable_path}
  mkdir -p ${absolute_executable_path}
  pushd ${source_ro_path}/pkg/pyinst
  # Build install directories (contains multiple files with pyd,dll, some of
  # them look like windows WS_32.dll but are from wine).
  for setup in ${setups[@]}
  do
    echo ${setup}
    exe_name=$(cat ${source_ro_path}/pkg/pyinst/${setup}.spec | grep '^exe_name = ' \
      | sed "s|.*'\([^']*\)'.*|\1|") #'
    cat ${source_ro_path}/pkg/pyinst/${setup}.spec \
      | sed "s|'\.\./\.\./\.\./pyinst/hooks|'${WINBUILDDIR}/pkg/pyinst/hooks|g" \
      | sed "s|='\.\./\.\./res|='${WINBUILDDIR}/res|g" \
      > ${WINBUILDDIR}/pkg/pyinst/${setup}.win.spec
    cat ${WINBUILDDIR}/pkg/pyinst/${setup}.win.spec
    # --clean do not cache anything and overwrite everything --noconfirm
    # --distpath to place on correct location
    # --debug to see what may be wrong with the result
    # --paths=c:\python\lib\site-packages;c:\python27\lib\site-packages
    PYTHONHASHSEED=1 \
    wine pyinstaller \
      --clean \
      --noconfirm \
      --distpath=${win_executable_path} \
      --workpath=${win_work_path} \
      --paths=${win_path} \
      --log-level DEBUG \
      ${WINBUILDDIR}/pkg/pyinst/${setup}.win.spec \
    || die "pyinstaller failed for ${setup}"
    RemoveWineDlls ${absolute_executable_path}/${exe_name}
  done

  # Plugins are also needed for onefile executables unless pyinstaller cannot
  # put them to the pyside users we have to do this here cp -r
  # ${absolute_executable_path}/onedir/qt4_plugins
  # ${absolute_executable_path}/onefile
  popd
}

# Generate nsis file references for installer for single directory
# appends File and Remove to files that are later included by makensis
# separate files for install and uninstall statements
#
# directory_root: The tree root that is currently generated.
# subdir: Any directory in the tree.
# setup_name: The name of the setup this nsh entries are generated for.
function generateDirectoryNSISStatements() {
  directory_root=$1
  subdir=$2
  setup_name=$3
  find ${subdir} -maxdepth 1 -type f \
    -exec echo 'File "'${relative_executable_path}'/{}"' \; \
    >> ${setup_name}_install_files.nsh
  find ${subdir} -maxdepth 1 -type f \
    -exec echo 'Delete "$INSTDIR/{}"' \; \
    >> ${setup_name}_uninstall_files.nsh
}

# Generate a tree of files into nsis installer definitions.
# directory_root: The tree root that is currently generated
# setup_name: The name of the setup this nsh entries are generated for.
function generateDirectoryNSISStatementsTree() {
  directory_root=$1
  setup_name=$2
  subdirs=$(find ${directory_root} -type d | sort)
  for subdir in ${subdirs[@]}
  do
    if [ "${directory_root}" != "${subdir}" ]; then
      echo 'SetOutPath "$INSTDIR/'${subdir}'"' \
        >> ${setup_name}_install_files.nsh
    fi
    generateDirectoryNSISStatements ${directory_root} ${subdir} ${setup_name}
  done
  # again to remove emptied directories on uninstall so reverse
  subdirs=$(find ${directory_root} -type d | sort | tac)
  for subdir in ${subdirs[@]}
  do
    if [ "${directory_root}" != "${subdir}" ]; then
      echo 'RMDir "$INSTDIR/'${subdir}'"' >> ${setup_name}_uninstall_files.nsh
    fi
  done
}

# Generate installer files for the available setups.
# Those files include install and uninstall statements and are
# modified (backslashes/source_path) to generate a sane target
# structure.
function generateNSISStatements() {
  pushd ${absolute_executable_path}
  setup=${product}
  echo "setup:" ${setup}
  echo "# auto generated by $0 please do not modify" \
    > ${setup}_install_files.nsh
  echo "# auto generated by $9 please do not modify" \
    > ${setup}_uninstall_files.nsh
  setup_source_path=${setup}
  generateDirectoryNSISStatementsTree ${setup_source_path} ${setup}
  # remove the setup_source_path from the nsh files
  sed -i "s|INSTDIR/${setup_source_path}/|INSTDIR/|" ${setup}_install_files.nsh
  sed -i "s|/${setup_source_path}/|/|" ${setup}_uninstall_files.nsh
  # make backslashes
  sed -i "s|/|\\\\|g" ${setup}_install_files.nsh ${setup}_uninstall_files.nsh
  # make install size
  installed_size=$(du -s --block-size=1000 ${setup} | awk '{print $1}')
  echo "!define INSTALLSIZE ${installed_size}" > ${setup}_install_files_size.nsh
  popd
}

# Install (windows)dependencies of project.
function installProjectDependencies() {
  pushd ${source_ro_path} > /dev/null
  unsupported_packages="dirspec"
  pip_flags=""
  pip_flags="${pip_flags} -r"

  # install dependencies
  mkdir -p ${temporary_build_path}/wheels
  wine pip install ${pip_flags} pkg/requirements-pkg.pip \
    || die "install of requirements-pkg.pip failed"

  wine pip install ${pip_flags} pkg/requirements.pip \
    || die "install of requirements.pip failed"

  wine pip install ${pip_flags} pkg/requirements-windows.pip \
    || die "install of requirements-windows.pip failed"

  wine pip install --find-links ${win_distfiles} -r pkg/requirements-netsplice.pip \
    || die "install of requirements-netsplice.pip failed"
  # Hack: evasion site-package does not contain __init__.py
  # pyinstaller will not find the package unless we inject this file.
  touch /root/.wine/drive_c/Python27/Lib/site-packages/evasion/__init__.py
  popd
}

# Build the directory that will be installed usin nsis.
# Copy all required files to the correct location.
# Subsequent calls to the script will aggregate the binaries.
function mergeInstallables() {
  pushd ${absolute_executable_path}
  mkdir -p ${product}

  # openssl build - use version that was used to build eggs/wheels
  mkdir -p ${product}/openssl
  cp /root/.wine/drive_c/openssl/bin/* ${product}/openssl
  cp /root/.wine/drive_c/openssl/ssl/openssl.cnf ${product}/openssl
  cp /usr/lib/gcc/i686-w64-mingw32/4.9-win32/libgcc_s_sjlj-1.dll ${product}

  # OpenVPN build - Duplicates the openssl binary
  # as privileged/openvpn is strictly speaking a different module
  # than backend/../utils/certificates.py they should be different
  # TODO: define multiple OpenVPN backends, they should use specific
  # OpenSSL?
  for version in ${OPENVPN_VERSIONS[@]}; do
    mkdir -p ${product}/libexec/openvpn/v${version}/openvpn/sbin
    cp libexec/openvpn/v${version}/openvpn/bin/* \
      ${product}/libexec/openvpn/v${version}/openvpn/sbin
  done
  for version in ${TOR_VERSIONS[@]}; do
    mkdir -p ${product}/libexec/tor/v${version}/tor/bin
    cp libexec/tor/v${version}/tor/bin/* \
      ${product}/libexec/tor/v${version}/tor/bin
    mkdir -p ${product}/libexec/tor/v${version}/tor/share/tor
    cp libexec/tor/v${version}/tor/share/tor/* \
      ${product}/libexec/tor/v${version}/tor/share/tor
  done
  mkdir -p ${product}/libexec/process_launcher
  cp ${source_ro_path}/pkg/libexec/process_launcher/win32_* \
    ${product}/libexec/process_launcher/

  for setup in "${setups[@]}"
  do
    exe_name=$(cat ${source_ro_path}/pkg/pyinst/${setup}.spec \
      | grep '^exe_name = ' \
      | sed "s|.*'\([^']*\)'.*|\1|") #'
    echo "setup:" ${setup} ${exe_name}
    setup_source_path=${exe_name}
    cp -r ${setup_source_path}/* ${product} \
      || die "${setup_source_path} could not be copied"
  done
  popd
}

# Prepare read-write copy.
function prepareBuildPath() {
  cleanup
  mkdir -p ${temporary_build_path}

  # re-initialize wine with path
  wine regedit /root/.wine/drive_c/path.reg
}

# Remove wine dlls that should not be in the installer.
# root: Path that should be cleaned from dlls.
function RemoveWineDlls() {
  root=$1
  declare -a wine_dlls=(\
    advapi32.dll \
    comctl32.dll \
    comdlg32.dll \
    gdi32.dll \
    imm32.dll \
    iphlpapi.dll \
    ktmw32.dll \
    msvcp90.dll \
    msvcrt.dll \
    mswsock.dll \
    mpr.dll \
    netapi32.dll \
    ole32.dll \
    oleaut32.dll \
    opengl32.dll \
    psapi.dll \
    rpcrt4.dll \
    shell32.dll \
    user32.dll \
    version.dll \
    winmm.dll \
    winspool.drv \
    ws2_32.dll \
    wtsapi32.dll \
    )
  for wine_dll in "${wine_dlls[@]}"
  do
    # Not all of the listed dlls are in all directories.
    rm ${root}/${wine_dll} 2>/dev/null
  done
}

# Display failure message and emit non-zero exit code.
function die() {
  echo "die:" $@
  exit 1
}

function main() {
  if [ ! -z ${BUILD_NAME} ]; then
      setups=(${BUILD_NAME})
      echo "using ${setups}[@] as setup"
      installProjectDependencies
      prepareBuildPath
      createInstallables
      mergeInstallables
      generateNSISStatements
      cleanup
  else
      prepareBuildPath
      installProjectDependencies
      createInstallables
      mergeInstallables
      generateNSISStatements
      cleanup
  fi
}
main $@
