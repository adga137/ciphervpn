#!/bin/bash

source /usr/local/bin/build.sh.inc

unpack

if [ "${WINBUILDDIR}" != "${BUILDDIR}" ]; then
  rm -r ${WINBUILDDIR}
  cp -R ${BUILDDIR} ${WINBUILDDIR}
fi
pushd ${WINBUILDDIR}/pkg/debian-build/slaves/syswin32
make || die 'failed to build windows package'
popd

# cleanup required because of double-mapped paths, tmp is a volume
rm -r ${TMPDIR}/*
