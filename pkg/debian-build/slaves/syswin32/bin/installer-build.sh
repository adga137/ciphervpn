#!/bin/bash

# build installer
# ===============
#
# builds several installers from previously compiled binaries

source /usr/local/bin/build.sh.inc

pushd ${WINBUILDDIR}/pkg/debian-build/slaves/syswin32/config
for install_script in *.nis
do
makensis ${install_script}
done
popd