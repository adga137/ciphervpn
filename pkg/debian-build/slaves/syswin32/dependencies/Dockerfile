FROM debian:jessie
MAINTAINER support@ipredator.se

# Attention. when changing any values, the container will be rebuilt without cache.
# To update a version always update the SHA1/SHA256 sums as well.
ENV PYTHON_VERSION=2.7.14 \
    PYTHON_SHA1=a84cb11bdae3e1cb76cf45aa96838d80b9dcd160 \
    PYTHON_SHA256=450bde0540341d4f7a6ad2bb66639fd3fac1c53087e9844dc34ddf88057a17ca \
    PYINSTALLER_VERSION=3.1.1 \
    OPENSSL_VERSION=1.0.2o \
    OPENSSL_SHA1=a47faaca57b47a0d9d5fb085545857cc92062691 \
    OPENSSL_SHA256=ec3f5c9714ba0fd45cb4e087301eb1336c317e0d20b575a125050470e8089e4d \
    CURL_VERSION=7.58.0 \
    CURL_SHA256=1cb081f97807c01e3ed747b6e1c9fee7a01cb10048f1cd0b5f56cfe0209de731 \
    CURL_SHA1=42fff9f33ad4ed9e797ee1e7c0a3cda7f8dff7ff \
    MINGW_VERSION=0.6.2-beta-20131004-1 \
    MINGW_BIN_VERSION=0.6.2-mingw32-beta-20131004-1-bin \
    MINGW_SHA1=fbd2ecc84af18cd5f72929db6a2c464ccc03aea6 \
    MINGW_SHA256=2e0e9688d42adc68c5611759947e064156e169ff871816cae52d33ee0655826d \
    ZLIB_VERSION=1.2.11 \
    ZLIB_SHA1=e6d119755acdf9104d7ba236b1242696940ed6dd \
    ZLIB_SHA256=c3e5e9fdd5004dcb542feda5ee4f0ff0744628baf8ed2dd5d66f8ca1197cb1a1 \
    WINEDEBUG=fixme-all \
    PINNED_DOMAINS='dl.winehq.org www.python.org www.openssl.org' \
    PINNED_FPS_SHA1="SHA1 Fingerprint=35:5D:F1:73:15:8A:B1:55:0F:35:E0:6A:7A:6F:E4:43:B5:E9:5B:29\nSHA1 Fingerprint=37:2D:53:B5:BC:93:74:4C:99:51:77:0B:5A:40:15:BB:C4:EE:54:72\nSHA1 Fingerprint=BD:0B:0E:39:DF:A1:2D:B7:B0:3C:7D:69:B0:39:38:F5:FB:EA:9C:8E" \
    PINNED_FPS_SHA256="SHA256 Fingerprint=E2:32:59:FC:F4:29:BC:0D:10:51:F2:78:BB:BC:82:49:12:64:76:5E:1B:25:DC:03:CF:74:55:24:7E:AE:9B:DE\nSHA256 Fingerprint=1E:3D:A4:2B:20:98:B9:75:CA:FF:52:FA:2B:40:8B:4C:16:BD:D9:79:30:27:7E:96:86:9E:50:AB:85:CD:8D:9B\nSHA256 Fingerprint=C2:91:C5:C4:54:2F:6F:46:0E:4A:06:D0:1D:C0:DB:33:87:9D:C0:38:94:C8:4B:A7:2F:28:68:D6:A1:27:D6:A8"

######
# install packages required to build
# https-transport: winehq deb
# winbind: pip install keyring (requirements.pip) needs this somehow
RUN apt-get update && apt-get -y install \
    unzip curl apt-transport-https \
    winbind \
    build-essential autoconf bison gperf flex libtool mingw-w64 \
    pkg-config \
    git-core libgnutls28-dev

# Download, verify, compile and install curl version that supports
# --pinnedpubkey
RUN mkdir -p /var/src \
 && cd /var/src \
 && curl https://curl.haxx.se/download/curl-${CURL_VERSION}.tar.bz2 \
  > curl-${CURL_VERSION}.tar.bz2 \
 && echo "${CURL_SHA256}  curl-${CURL_VERSION}.tar.bz2" > sha256sum-signed \
 && echo "${CURL_SHA1}  curl-${CURL_VERSION}.tar.bz2" > sha1sum-signed \
 && sha1sum curl-${CURL_VERSION}.tar.bz2 \
 && sha256sum curl-${CURL_VERSION}.tar.bz2 \
 && sha1sum -c sha1sum-signed \
 && sha256sum -c sha256sum-signed \
 && tar xjf curl-${CURL_VERSION}.tar.bz2 \
 && cd /var/src/curl-${CURL_VERSION} \
 && ./configure --prefix=/usr/local --with-gnutls \
 && make -j 5 \
 && make install \
 && rm -r /var/src/curl-${CURL_VERSION}.tar.bz2 \
 && rm -r /var/src/curl-${CURL_VERSION}

# Acquire Certificates for domains that host files with https.
# The certificate fingerprints are matched to those listed in the ENV
# generate the public keys that are used for pinnedcertificate
RUN mkdir -p /pin \
 && mkdir -p /pfp \
 && for DOMAIN in ${PINNED_DOMAINS}; do \
    echo -n | openssl s_client -connect ${DOMAIN}:443 \
        -CAfile /usr/share/ca-certificates/mozilla/DigiCert_Assured_ID_Root_CA.crt \
        | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' \
        > /pin/${DOMAIN}.crt.pem \
    && openssl x509 -in /pin/${DOMAIN}.crt.pem -pubkey -noout > /pin/${DOMAIN}.pubkey.pem \
    ;done \
 && cd /pin \
 && ls -1 | grep 'crt.pem' | xargs -IX openssl x509 -noout -in X -fingerprint -sha1 \
    > /pfp/sha1 \
 && ls -1 | grep 'crt.pem' | xargs -IX openssl x509 -noout -in X -fingerprint -sha256 \
    > /pfp/sha256  \
 && ls -1 | grep 'crt.pem' \
    | xargs -IX echo 'echo -n "X:"; openssl x509 -noout -in X -fingerprint -sha1' | bash \
 && cat /pfp/sha1 /pfp/sha256 \
 && echo "${PINNED_FPS_SHA256}" > /pfp/sha256-signed \
 && echo "${PINNED_FPS_SHA1}" > /pfp/sha1-signed \
 && echo 'signed' && cat /pfp/sha1-signed /pfp/sha256-signed \
 && diff /pfp/sha1 /pfp/sha1-signed \
 && diff /pfp/sha256 /pfp/sha256-signed

# Download Python for Windows
RUN LD_LIBRARY_PATH=/usr/local/lib:${LD_LIBRARY_PATH} \
    /usr/local/bin/curl --pinnedpubkey /pin/www.python.org.pubkey.pem \
    https://www.python.org/ftp/python/${PYTHON_VERSION}/python-${PYTHON_VERSION}.msi \
    > /tmp/python-${PYTHON_VERSION}.msi \
 && echo "${PYTHON_SHA1}  /tmp/python-${PYTHON_VERSION}.msi" \
    > /tmp/python-${PYTHON_VERSION}.sha1sum-signed \
 && echo "${PYTHON_SHA256}  /tmp/python-${PYTHON_VERSION}.msi" \
    > /tmp/python-${PYTHON_VERSION}.sha256sum-signed \
 && cat /tmp/python-${PYTHON_VERSION}.sha1sum-signed /tmp/python-${PYTHON_VERSION}.sha256sum-signed \
 && sha1sum /tmp/python-${PYTHON_VERSION}.msi \
 && sha256sum /tmp/python-${PYTHON_VERSION}.msi \
 && sha1sum -c /tmp/python-${PYTHON_VERSION}.sha1sum-signed \
 && sha256sum -c /tmp/python-${PYTHON_VERSION}.sha256sum-signed

# Download and unpack MINGW installer
RUN LD_LIBRARY_PATH=/usr/local/lib:${LD_LIBRARY_PATH} \
    /usr/local/bin/curl -L \
    https://sourceforge.net/projects/mingw/files/Installer/mingw-get/mingw-get-${MINGW_VERSION}/mingw-get-${MINGW_BIN_VERSION}.zip/download \
    > /tmp/mingw-get-${MINGW_BIN_VERSION}.zip \
 && echo "${MINGW_SHA1}  /tmp/mingw-get-${MINGW_BIN_VERSION}.zip" \
    > /tmp/mingw-get-${MINGW_BIN_VERSION}.zip.sha1sum-signed \
 && echo "${MINGW_SHA256}  /tmp/mingw-get-${MINGW_BIN_VERSION}.zip" \
    > /tmp/mingw-get-${MINGW_BIN_VERSION}.zip.sha256sum-signed \
 && cat /tmp/mingw-get-${MINGW_BIN_VERSION}.zip.sha1sum-signed /tmp/mingw-get-${MINGW_BIN_VERSION}.zip.sha256sum-signed \
 && sha1sum /tmp/mingw-get-${MINGW_BIN_VERSION}.zip \
 && sha256sum /tmp/mingw-get-${MINGW_BIN_VERSION}.zip \
 && sha1sum -c /tmp/mingw-get-${MINGW_BIN_VERSION}.zip.sha1sum-signed \
 && sha256sum -c /tmp/mingw-get-${MINGW_BIN_VERSION}.zip.sha256sum-signed \
 && mkdir -p  /root/.wine/drive_c/mingw \
 && unzip -d /root/.wine/drive_c/mingw /tmp/mingw-get-${MINGW_BIN_VERSION}.zip \
 && rm /tmp/mingw-get-${MINGW_BIN_VERSION}.zip

# Download OpenSSL
RUN LD_LIBRARY_PATH=/usr/local/lib:${LD_LIBRARY_PATH} \
    /usr/local/bin/curl --pinnedpubkey /pin/www.openssl.org.pubkey.pem \
    https://www.openssl.org/source/openssl-${OPENSSL_VERSION}.tar.gz \
    > /tmp/openssl-${OPENSSL_VERSION}.tar.gz \
 && echo "${OPENSSL_SHA1}  /tmp/openssl-${OPENSSL_VERSION}.tar.gz" \
    > /tmp/openssl-${OPENSSL_VERSION}.tar.gz.sha1sum-signed \
 && echo "${OPENSSL_SHA256}  /tmp/openssl-${OPENSSL_VERSION}.tar.gz" \
    > /tmp/openssl-${OPENSSL_VERSION}.tar.gz.sha256sum-signed \
 && cat /tmp/openssl-${OPENSSL_VERSION}.tar.gz.sha1sum-signed /tmp/openssl-${OPENSSL_VERSION}.tar.gz.sha256sum-signed \
 && sha1sum /tmp/openssl-${OPENSSL_VERSION}.tar.gz \
 && sha256sum /tmp/openssl-${OPENSSL_VERSION}.tar.gz \
 && sha1sum -c /tmp/openssl-${OPENSSL_VERSION}.tar.gz.sha1sum-signed \
 && sha256sum -c /tmp/openssl-${OPENSSL_VERSION}.tar.gz.sha256sum-signed

# Download ZLIB
RUN LD_LIBRARY_PATH=/usr/local/lib:${LD_LIBRARY_PATH} \
    /usr/local/bin/curl -L http://zlib.net/zlib-${ZLIB_VERSION}.tar.gz \
    > /tmp/zlib-${ZLIB_VERSION}.tar.gz \
 && echo "${ZLIB_SHA1}  /tmp/zlib-${ZLIB_VERSION}.tar.gz" \
    > /tmp/zlib-${ZLIB_VERSION}.tar.gz.sha1sum-signed \
 && echo "${ZLIB_SHA256}  /tmp/zlib-${ZLIB_VERSION}.tar.gz" \
    > /tmp/zlib-${ZLIB_VERSION}.tar.gz.sha256sum-signed \
 && cat /tmp/zlib-${ZLIB_VERSION}.tar.gz.sha1sum-signed /tmp/zlib-${ZLIB_VERSION}.tar.gz.sha256sum-signed \
 && sha1sum /tmp/zlib-${ZLIB_VERSION}.tar.gz \
 && sha256sum /tmp/zlib-${ZLIB_VERSION}.tar.gz \
 && sha1sum -c /tmp/zlib-${ZLIB_VERSION}.tar.gz.sha1sum-signed \
 && sha256sum -c /tmp/zlib-${ZLIB_VERSION}.tar.gz.sha256sum-signed

# install wine > 1.6.2 (debian:jessie version fails with pip)
RUN dpkg --add-architecture i386 \
 && LD_LIBRARY_PATH=/usr/local/lib:${LD_LIBRARY_PATH} \
    /usr/local/bin/curl --pinnedpubkey /pin/dl.winehq.org.pubkey.pem \
    https://dl.winehq.org/wine-builds/Release.key \
    | apt-key add - \
 && echo 'deb https://dl.winehq.org/wine-builds/debian/ jessie main' \
    >> /etc/apt/sources.list.d/wine.list \
 && apt-get update \
 && apt-get install -y winehq-staging

#######
# Build python dependency
# using the 'host' (linux) xcompiler instead of fiddeling in wine
#
# openssl - needs approx every quarter a new update
RUN mkdir -p /root/.wine/drive_c/openssl/src \
 && mv /tmp/openssl-${OPENSSL_VERSION}.tar.gz /root/.wine/drive_c/openssl/src \
 && cd /root/.wine/drive_c/openssl/src \
 && tar xzf openssl-${OPENSSL_VERSION}.tar.gz \
 && cd openssl-${OPENSSL_VERSION} \
 && RC=/usr/bin/i686-w64-mingw32-windres \
    ./Configure shared mingw \
      --prefix=/root/.wine/drive_c/openssl/ \
 && make -j5 \
    CC=i686-w64-mingw32-gcc \
    CROSS_COMPILE=i686-w64-mingw32- \
 && make install \
    CC=i686-w64-mingw32-gcc \
    CROSS_COMPILE=i686-w64-mingw32- \
 && rm -r /root/.wine/drive_c/openssl/src

# zlib - needs a update every 5 years
# adds a patch that makes a shared lib - default is static

ADD zlib-mingw-shared.patch /zlib-mingw-shared.patch

RUN mkdir -p /root/.wine/drive_c/zlib/src \
 && mv /tmp/zlib-${ZLIB_VERSION}.tar.gz /root/.wine/drive_c/zlib/src \
 && cd /root/.wine/drive_c/zlib/src \
 && tar xzf zlib-${ZLIB_VERSION}.tar.gz \
 && cd zlib-${ZLIB_VERSION} \
 && patch -p0 < /zlib-mingw-shared.patch \
 && make -f win32/Makefile.gcc PREFIX=/usr/bin/i686-w64-mingw32- \
 && make -f win32/Makefile.gcc INCLUDE_PATH=/root/.wine/drive_c/zlib/include LIBRARY_PATH=/root/.wine/drive_c/zlib/lib BINARY_PATH=/root/.wine/drive_c/zlib/bin  install

######
# install gcc/g++ for most pip builds
RUN wine msiexec -i /tmp/python-${PYTHON_VERSION}.msi -q \
 && wine c:/mingw/bin/mingw-get.exe install gcc g++ mingw32-make \
 && rm -r /tmp/.wine-0

####
# pip configuration
# set wine mingw compiler to be used by "python setup build"
# set default include dirs, libraries and library paths
# the libraries=crypto is added because srp will only link using -lssl but links to BN_* (libcrypto) code
# 'install' zlib to mingw so python may find its dlls
# pyside-rcc fix (https://srinikom.github.io/pyside-bz-archive/670.html)
RUN printf "[build]\ncompiler=mingw32\n\n[build_ext]\ninclude_dirs=c:\\openssl\\include;c:\\zlib\\include\nlibraries=crypto\nlibrary_dirs=c:\\openssl\\lib;c:\\openssl\\bin;c:\\zlib\\lib;c:\\zlib\\bin" > /root/.wine/drive_c/Python27/Lib/distutils/distutils.cfg \
 && printf 'REGEDIT4\n\n[HKEY_CURRENT_USER\\Environment]\n"PATH"="C:\\\\python27;C:\\\\python27\\\\Scripts;C:\\\\python27\\\\Lib\\\\site-packages\\\\PySide;C:\\\\mingw\\\\bin;c:\\\\windows;c:\\\\windows\\\\system"' > /root/.wine/drive_c/path.reg \
 && cp /root/.wine/drive_c/zlib/bin/zlib1.dll /root/.wine/drive_c/mingw/bin \
 && cp /root/.wine/drive_c/zlib/lib/libz.dll.a /root/.wine/drive_c/mingw/lib

####
# prepare the environment with all python dependencies installed
#
RUN wine regedit /root/.wine/drive_c/path.reg \
 && wine pip install virtualenv pyinstaller==${PYINSTALLER_VERSION} \
 && wine pip install wheel \
 && wine pip install -U setuptools-scm \
 && wine pip install -U setuptools_scm \
 && wine pip install -U keyring \
 && wine pip install -U pyside python-qt \
 && wine pip install -U urllib3 \
 && wine pip install -U m2cryptowin32 \
 && rm -r /tmp/.wine-0

# When you need to debug the container content it is useful to have
# some tools around. Those are not required.
RUN apt-get -y install \
    mc

ENTRYPOINT ["/usr/local/bin/dependencies-build.sh"]
