#!/bin/bash

source /usr/local/bin/build.sh.inc

unpack

create_virtualenv

# make required steps to build the installer binaries
cd ${TMPDIR}/${PRODUCT}-${VERSION}/pkg/debian-build/slaves/sysdebian8 \
  || die 'could not change to debian pkg directory'
make all \
  || die 'could not build binaries'

# export the results to the shared storage so jenkins can access it again
cd ${TMPDIR}
cp ${PRODUCT}_${DEBCLVERSION}.dsc \
   ${INSTALLABLESDIR} \
   || die 'debian dsc could not be copied'
cp ${PRODUCT}_${DEBCLVERSION}.tar.gz \
   ${INSTALLABLESDIR} \
   || die 'debian dsc could not be copied'
cp ${PRODUCT}_${DEBCLVERSION}_amd64.changes \
   ${INSTALLABLESDIR} \
   || die 'debian changes file could not be copied'
cp ${PRODUCT}_${DEBCLVERSION}_amd64.deb \
   ${INSTALLABLESDIR} \
   || die 'debian deb package could not be copied'
