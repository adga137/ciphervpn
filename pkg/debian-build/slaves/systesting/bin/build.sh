#!/bin/bash

source /usr/local/bin/build.sh.inc

unpack

create_virtualenv

cd ${BUILDDIR}

pip install -r pkg/requirements-testing.pip \
  || die 'requirements-testing could not be installed'

# start fake xserver for gui tests

Xvfb :1 -screen 0 1024x768x16 &
export DISPLAY=:1.0

nosetests \
  || die 'at least one test failed'
