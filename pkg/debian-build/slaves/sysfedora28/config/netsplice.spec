Name:           netsplice
Version:        VERSION
Release:        RELEASE%{?dist}
Summary:        Netsplice is a multiplatform desktop client for VPN services.

License:        GPLv3

URL:            https://ipredator.se/netsplice
# The libexec binaries break rpm...
AutoReqProv:    no
%define _build_id_links none

%description
Netsplice is a multiplatform desktop client for VPN services.

#This is a comment (just as example)

%changelog
* Fri Apr 27 2018 autobuild <build@ipredator.se> 0.28-1.fc28
- initial fedora28 support

* Thu Dec 14 2017 autobuild <build@ipredator.se> 0.24-3.fc28
- disable build_id_links to avoid conflicts from files copied by pyinstaller

* Mon Mar 10 2017 autobuild <build@ipredator.se> 0.2-1.fc28
- introduce requires to allow libexec binaries

* Thu May 26 2016 autobuild <build@ipredator.se> 0.1-1.fc28
- initial build
