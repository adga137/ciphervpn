OPENVPN_BUILD_PREFIXES=$(OPENVPN_VERSIONS:%=$(BIN_DIR)/openvpn-%)
# Executables that will be packaged by rpmbuild/debpkg etc
OPENVPN_INSTALL_TARGETS=$(OPENVPN_VERSIONS:%=$(BIN_DIR)/install/opt/$(PRODUCT)/lib/libexec/openvpn/v%/openvpn/sbin/openvpn)
# Location of executables in the users system (prefix/RPATH during build)
OPENVPN_BUILD_TARGETS=$(OPENVPN_VERSIONS:%=/opt/${PRODUCT}/lib/libexec/openvpn/v%/openvpn/sbin/openvpn)
# Location of executables after build, cleaned of development files
OPENVPN_CLEAN_INSTALL_TARGETS=$(OPENVPN_VERSIONS:%=$(BIN_DIR)/libexec/openvpn/v%/openvpn/sbin/openvpn)

EXTRA_OPENVPN_CONFIG=
EXTRA_OPENSSL_CONFIG=
OPENVPN_INSTALL_ROOT=
OPENVPN_IMAGEROOT=

ifeq ($(BUILD_PLATFORM),Linux)
  EXTRA_OPENVPN_CONFIG=--enable-iproute2
  OPENVPN_INSTALL_ROOT=/opt/$(PRODUCT)/lib/libexec/openvpn/v$*
  OPENVPN_IMAGEROOT=/opt/${PRODUCT}/lib/libexec/openvpn/v$*
endif

ifeq ($(BUILD_PLATFORM),Darwin)
  EXTRA_OPENSSL_CONFIG=--libdir=/lib
endif

REPRODUCIBLE_MESSAGE="There is no simple recipe to build openvpn." \
	"\nThe binaries in this directory are produced in a clean environment" \
	"\nwith the sources that checksum'd as below." \
	"\nWhen you want to reproduce the build, please take a look at the" \
	"\npkg subdirectories in the netsplice-sources. It contains all the" \
	"\nnenvironment definitions (Dockerfile) and build rules (Makefile)" \
	"\nwe use to build the binaries, enhancements are welcome!\n\n"
$(OPENVPN_INSTALL_PREFIXES):
	mkdir -p $(OPENVPN_INSTALL_PREFIXES)


# OPENVPN_BUILD_TARGETS
# Produce a openvpn binary for each version configured in pkg/libexec/openvpn
# with the provided patches added.
# Tries to use a cache of downloaded installers to speed up the build time
# when the cache is not available, the files are downloaded using https urls
/opt/${PRODUCT}/lib/libexec/openvpn/v%/openvpn/sbin/openvpn:
	rm -rf $(BIN_DIR)/openvpn-build || true
	mkdir -p ${BIN_DIR}
	# This package is created by the distfiles container to minimize the downloads
	# and stabilize the packaging (platforms x openvpn-versions downloads otherwise)
	# this is a (patched) source-package, not the binaries
	cp ${DISTFILESDIR}/openvpn-build.tar.bz2 ${BIN_DIR}
	cd ${BIN_DIR} \
	  && tar xjf openvpn-build.tar.bz2

	# see all the configure options in the build.log
	sed -iorg \
		's|^die()|set -x;die()|' \
		$(BIN_DIR)/openvpn-build/generic/build

	cp $(BUILD_DIR)/pkg/libexec/openvpn/v$*/* \
	  $(BIN_DIR)/openvpn-build/generic/patches || echo "no patches on openvpn for $*"

	# remove other openvpn versions to prevent the build script picking up the wrong (or multiple)
	cd $(BIN_DIR)/openvpn-build/generic/sources \
	  && mv openvpn-$(shell echo $* | sed 's|\([^-]*\)-.*|\1|').tar.gz .. \
	  && mv openvpn-gui-* ..
	cd $(BIN_DIR)/openvpn-build/generic/sources \
	  && rm openvpn* \
	  || true
	cd $(BIN_DIR)/openvpn-build/generic/sources \
	  && mv ../openvpn-$(shell echo $* | sed 's|\([^-]*\)-.*|\1|').tar.gz . \
	  && mv ../openvpn-gui-* .

	# build the version and set its RPATH to the desired install location.
	# this way the makedebpkg/makerpm scripts do not add dependencies to the
	# package and the correct binaries are used on the users computer
	cd $(BIN_DIR)/openvpn-build/generic \
	  && OPENVPN_VERSION=$(shell echo $* | sed 's|\([^-]*\)-.*|\1|') \
	     OPENVPN_URL="https://build.openvpn.net/downloads/releases/openvpn-$(shell echo $* | sed 's|\([^-]*\)-.*|\1|').tar.gz" \
	     EXTRA_OPENVPN_CONFIG=$(EXTRA_OPENVPN_CONFIG) \
	     EXTRA_OPENSSL_CONFIG=$(EXTRA_OPENSSL_CONFIG) \
	     EXTRA_LIBRESSL_CONFIG=$(EXTRA_LIBRESSL_CONFIG) \
	     EXTRA_TARGET_CFLAGS=" " \
	     SPECIAL_BUILD=netsplice-$(shell echo $* | sed 's|[^-]*-\(.*\)|\1|') \
	     INSTALL_ROOT=$(OPENVPN_INSTALL_ROOT) \
	     IMAGEROOT=$(OPENVPN_IMAGEROOT) \
	     USE_LIBRESSL=$(shell echo $* | sed 's|[^-]*-\(.*\)|\1|' | grep 'libressl') \
	    ./build
	# "
	# add a checksum of the binaries that were used to create the
	# openvpn binaries to the logfile
ifeq ($(BUILD_PLATFORM),Darwin)
	mkdir -p $(BIN_DIR)/libexec/openvpn/v$*/openvpn/sbin
	echo $(REPRODUCIBLE_MESSAGE) \
	  > $(BIN_DIR)/libexec/openvpn/v$*/build-sources
	shasum -a 256 -p $(BIN_DIR)/openvpn-build/generic/sources/* \
	  | tee -a $(BIN_DIR)/libexec/openvpn/v$*/build-sources
	shasum -a 256 -p $(BIN_DIR)/openvpn-build/generic/patches/* \
	  | tee -a $(BIN_DIR)/libexec/openvpn/v$*/build-sources
	shasum -a 256 -p $(BIN_DIR)/openvpn-build/generic/* \
	  | tee -a $(BIN_DIR)/libexec/openvpn/v$*/build-sources
	cd $(BIN_DIR)/openvpn-build/generic/image/openvpn \
	  && cp sbin/openvpn \
	    $(BIN_DIR)/libexec/openvpn/v$*/openvpn/sbin \
	  && test -d lib/engines \
	    && chmod +w lib/engines/* \
	    || true \
	  && cp -R lib \
	    $(BIN_DIR)/libexec/openvpn/v$*/openvpn \
	  && test -d lib/engines \
	    && chmod -w $(BIN_DIR)/libexec/openvpn/v$*/openvpn/lib/engines/* \
	    || true \
	  && cp -R share \
	    $(BIN_DIR)/libexec/openvpn/v$*/openvpn

	# cleanup lib directory - nobody should link to the created libraries
	rm $(BIN_DIR)/libexec/openvpn/v$*/openvpn/lib/*.a \
	  $(BIN_DIR)/libexec/openvpn/v$*/openvpn/lib/*.la \
	  $(BIN_DIR)/libexec/openvpn/v$*/openvpn/lib/openvpn/plugins/*.la \
	  || true
	rm -rf $(BIN_DIR)/libexec/openvpn/v$*/openvpn/lib/pkgconfig \
	  $(BIN_DIR)/openvpn-build
	rm -rf $(BIN_DIR)/libexec/openvpn/v$*/openvpn/share
else
	echo $(REPRODUCIBLE_MESSAGE) \
	  > /opt/${PRODUCT}/lib/libexec/openvpn/v$*/build-sources
	sha256sum $(BIN_DIR)/openvpn-build/generic/sources/* \
	  | tee -a /opt/${PRODUCT}/lib/libexec/openvpn/v$*/build-sources
	sha256sum $(BIN_DIR)/openvpn-build/generic/patches/* \
	  | tee -a /opt/${PRODUCT}/lib/libexec/openvpn/v$*/build-sources
	sha256sum $(BIN_DIR)/openvpn-build/generic/* \
	  | tee -a /opt/${PRODUCT}/lib/libexec/openvpn/v$*/build-sources
endif


# OPENVPN_CLEAN_INSTALL_TARGETS
# $(BIN_DIR)/libexec/openvpn/v%/openvpn/sbin/openvpn
# This copies the binaries from openvpn-build to a location where the
# install-target can handle them.

$(BIN_DIR)/libexec/openvpn/v%/openvpn/sbin/openvpn: \
		$(OPENVPN_INSTALL_PREFIXES) \
		$(OPENVPN_BUILD_TARGETS)
	mkdir -p $(BIN_DIR)/libexec/openvpn/v$*/openvpn/sbin
	# only copy what is required to run openvpn:
	# openvpn binary and its dynamic libraries
	cp /opt/${PRODUCT}/lib/libexec/openvpn/v$*/openvpn/sbin/openvpn \
	  $(BIN_DIR)/libexec/openvpn/v$*/openvpn/sbin
	cp -R /opt/${PRODUCT}/lib/libexec/openvpn/v$*/openvpn/lib \
	  $(BIN_DIR)/libexec/openvpn/v$*/openvpn
	cp -R /opt/${PRODUCT}/lib/libexec/openvpn/v$*/build-sources \
	  $(BIN_DIR)/libexec/openvpn/v$*/build-sources

	# cleanup lib directory - nobody should link to the created libraries
	rm $(BIN_DIR)/libexec/openvpn/v$*/openvpn/lib/*.a \
	  $(BIN_DIR)/libexec/openvpn/v$*/openvpn/lib/*.la \
	  $(BIN_DIR)/libexec/openvpn/v$*/openvpn/lib/openvpn/plugins/*.la \
	  || true
	rm -rf $(BIN_DIR)/libexec/openvpn/v$*/openvpn/lib/pkgconfig \
	  $(BIN_DIR)/openvpn-build

	# In production, this ensures that a minimum of space is used during build
	# during development, it is useful to comment this out to avoid
	# recompiling N openvpn versions just to create a installer.
	# rm -rf /opt/${PRODUCT}/lib/libexec/openvpn/v$*

# OPENVPN_INSTALL_TARGETS used in Makefile.install:bin/install
# Place the binaries that should have correct RPATHs and only a minimum of
# files (eg no include) in the packaging directory
$(BIN_DIR)/install/opt/$(PRODUCT)/lib/libexec/openvpn/v%/openvpn/sbin/openvpn: \
		$(OPENVPN_CLEAN_INSTALL_TARGETS)
	mkdir -p $(BIN_DIR)/install/opt/$(PRODUCT)/lib/libexec/openvpn/v$*/openvpn/sbin
	mkdir -p $(BIN_DIR)/install/opt/$(PRODUCT)/lib/libexec/openvpn/v$*/openvpn/lib

	cp -rn $(BIN_DIR)/libexec/openvpn/v$*/openvpn/sbin/* \
	   $(BIN_DIR)/install/opt/$(PRODUCT)/lib/libexec/openvpn/v$*/openvpn/sbin
	cp -rn $(BIN_DIR)/libexec/openvpn/v$*/openvpn/lib/* \
	   $(BIN_DIR)/install/opt/$(PRODUCT)/lib/libexec/openvpn/v$*/openvpn/lib
	cp -rn $(BIN_DIR)/libexec/openvpn/v$*/build-sources \
	   $(BIN_DIR)/install/opt/$(PRODUCT)/lib/libexec/openvpn/v$*/build-sources
