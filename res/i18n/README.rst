res/i18n
========

Expect finished translations (i.e., those downloaded from ``transifex``) to live here. 

Translator object will pick them from here.

(Actually, from the embedded locale_rc)
