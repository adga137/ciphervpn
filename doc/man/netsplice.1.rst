.. _`man-netsplice`:

=========
Netsplice
=========

------------------------------------
graphical client to control OpenVPN.
------------------------------------

:Author: support@ipredator.se
:Date:   2017-02-23
:Copyright: GPLv3+
:Version: 0.14.2
:Manual section: 1
:Manual group: General Commands Manual

SYNOPSIS
========

netsplice [--help|-h] [--version|-V] [--debug|-d] [--log=<file>|-l <file>] [--openvpn-verbosity=<verbosity>]

DESCRIPTION
===========

*netsplice* is a graphical client to control OpenVPN.

When launched, it places an icon in the system tray from where the
connections can be controlled.


OPTIONS
=======

General options
---------------

**-h, --help**                  Print a help message and exit.

**-l, --log=<file>**            Write log to file. 


**-V, --version**               Print version and exit.


OpenVPN options
---------------

**--openvpn-verbosity** [0-5]   Verbosity level for OpenVPN logs.

Debug options
-------------

**-d, --debug**                 Launch in debug mode, write debug info to stdout or logfile.


WARNING
=======

This software is still in its early phases of testing. So don't trust your life to it! 


FILES
=====


/usr/share/polkit-1/actions/org.netsplice.netsplice-privileged.policy
---------------------------------------------------------------------

PolicyKit policy file, used for granting access to netsplice-privileged without the need of entering a password each time.

/usr/sbin/netsplice-privileged
------------------------------

Helper to launch and stop VPN clients.

~/.config/Netsplice/
--------------------

Main config folder.

~/.config/Netsplice/Netsplice.conf
----------------------------------

GUI options

BUGS
====

Please report any bugs to https://ipredator.se/page/contact.
