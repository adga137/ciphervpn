.. _`man-netsplice-privileged`:

====================
Netsplice Privileged
====================

--------------------------------------
privileged backend to control OpenVPN.
--------------------------------------

:Author: XXX
:Date:   2016-02-18
:Copyright: GPLv3+
:Version: 0.0.1
:Manual section: 1
:Manual group: General Commands Manual

SYNOPSIS
========

netsplice-privileged --key=<connectionkey> [--port=5151] [--host=localhost] [--log=<file>|-l <file>] [--debug|-d] [--version|-V]

DESCRIPTION
===========

*netsplice-privileged* is a privileged helper for Netsplice (netsplice).

It is used to start or stop OpenVPN . To operate, it
needs to be executed with root privileges.


OPTIONS
=======

general options
---------------

**-h, --help**                  Print a help message and exit.

**-l, --log=<file>**            Write log to file. 


**-V, --version**               Print version and exit.

mandatory options
-----------------

**-k, --key=<connectionkey>**   Allow only commands signed by clients with the same key

server options
--------------

**-p, --port=<portnumber>**     Listen on the specified port. Defaults to 5151

**-h, --host=<hostname>**       Listen on the specified host/ip. Defaults to localhost

debug options
-------------

**-d, --debug**                 Launch in debug mode, write debug info to stdout or logfile.


BUGS
====

Please report any bugs to https://XXX
