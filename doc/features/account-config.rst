.. _`features-account-config`:

Account configuration
=====================

.. begin-summary

To connect to a VPN provider a relatively complex configuration is
required. The Account Configuration tries to hide the complexity to avoid
overwhelming the user. Advanced users can see the configured values in a
table view and edit the plain config files.
The configuration dialog can be canceled, the current configuration may
be checked and the changes in the dialog can be saved.

.. image:: /images/account-config.png

.. end-summary

The first pane in the configuration is used to query the user for a name
that is displayed in the account list and a type of the account. Each type
loads a different set of forms to the dialog that should consist of
multiple levels.

.. _`features-account-config-openvpn`:

OpenVPN configuration
=====================

The OpenVPN configuration allows the user to enable and disable the account
and exposes the autostart option, that tells Netsplice to connect the
account when the application starts.
The Import Button lets the user choose a previously downloaded config
that has to contain all certificates.

Advanced OpenVPN configuration
==============================

The Advanced Configuration allows to remove, modify or introduce lines
properties of the Account. A Key-Value table and a syntax highlighted plaintext
edit allow to change any aspect of the account. The Key-Value configuration view
is similar to what the user knows from firefox's about:config.

Unmodified lines are displayed as normal lines. Lines that have been
inserted show italic. Lines that have been modified by the user are displayed in bold. The
dialog provides two actions Reset to defaults - that resets all user
modifications (no more bold entries). and reset to a line to its
imported value.

.. image:: /images/account-config-advanced.png

For advanced users the configuration options are presented in a table
view with documentation hints and indicators changed, inserted values.
The rows in the table can be reset to the imported defaults and the
complete configuration may be reset to the imported defaults.

.. image:: /images/account-config-advanced-plaintext.png

A advanced user can also edit the configuration in plaintext in a
syntax highlighted editor with tooltips on the OpenVPN keywords.
To influence the highlights in the table view, the imported values may
also be edited in the advanced mode.

More details can be found in the
:ref:`OpenVPN Plugin Description<plugins-openvpn>`.
