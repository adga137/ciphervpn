.. _`features-connection-statemachine`:

Connection State Machine
========================

.. begin-summary

Connections are implemented as state machines. This ensures that each event
can be processed by a finite number handlers and all transitions are defined.

.. end-summary

Connection
----------

The connection state machine interfaces between the plugins that implement
asynchronous methods for the transition of the states.

The states of a connection are:

* Aborted - The processing was disconnected with an error
* Created - - The object has been created
* Setting_Up - - All information to connect is made available to the process.
* Initialized - The object has been created and all information to connect
  is available in the process. This state is required from all accounts in a
  chain before the first connect is executed.
* Connecting - The connection has been queued for connection.
* Connecting Process - The plugin has initialized the (external) process that
  is responsible to handle the protocol. The process (OpenVPN/Tor) starts and
  loads the provided configuration.
* Connected - The plugin has completed the handshakes and a interface or
  socket is setup and ready to use.
* Disconnecting - The connection has been queued for disconnection
* Disconnecting Failed - The failed connection has been queued for
  disconnection because a failure occurred during connect or operation that
  may be recovered by reconnecting the parent.
* Disconnected - The connection has been terminated by the plugin, the
  interface or socket has been removed, the allocated process resources are
  free.
* Disconnected Failed - The connection has been terminated because of a error.
* Reconnecting - The connection has been queued for reconnection.
* Reconnecting Process - The plugin has triggered the reconnect sequence in
  the external process.
* Reconnected - The plugin has completed the sequence to restore the
  connection, the interface or socket is setup and ready to use.

.. graphviz::

    digraph G {
      Created -> Setting_Up
      Setting_Up -> Initialized
      Setting_Up -> Disconnected_Failure
      Initialized -> Connecting
      Connecting -> Connecting_Process
      Connecting_Process -> Connected
      Connecting_Process -> Disconnecting_Failed

      Connected -> Reconnecting
      Connected -> Disconnecting
      Connected -> Disconnecting_Failed

      Reconnecting -> Reconnecting_Process
      Reconnecting -> Disconnecting_Failed
      Reconnecting_Process -> Reconnected
      Reconnecting_Process -> Disconnecting_Failed

      Reconnected -> Disconnecting
      Reconnected -> Reconnecting
      Reconnected -> Disconnecting_Failed

      Disconnecting -> Disconnecting_Process-> Disconnected
      Disconnecting_Failed -> Disconnecting_Failure_Process -> Disconnected_Failure
    }

Connection Broker
-----------------

The connection broker interfaces between Connection and Account. The broker
ensures that connection chains are established and translates the connection
events into account events.

The connection broker holds multiple lists. A connection list that tracks
all connection objects. A connection queue that holds connections that need
to be notified and a connection chain that holds the relation between the
connection objects.

A user usually requests an *account* object to connect. This request is send
to the connection broker that queues the account for connection (setup the
connection object).

The broker acts on the events *connected*, *disconnected*,
*disconnected_failure* and *reconnected* from the connection.

The states of the broker are *aborted*, *active* and *inactive*.

When the broker arrives (with the RESUME trigger) at a queued account entry it
evaluates the *account chain* preference and creates additional connection
objects depending on the defined chain. Those objects are then send into
different state transition path's that are requested
(connect/disconnect/reconnect api), unconditional (disconnect all children of a
disconnected) or preference controlled (reconnect parent).


.. graphviz::


    digraph G {
        subgraph cluster_connection_broker {
            style=filled;
            color=lightgrey;
            label="Connection Broker";
            CMTRESUME[shape=box];
            CMTCANCEL[shape=box];
            CMTDONE[shape=box];

            CMTRESUME -> CMNextQueue
            CMNextQueue -> CTConnect[label="next"];
            CMNextQueue -> CTReconnect[label="next"];
            CMNextQueue -> CTDisconnect[label="next"];
            CMNextQueue -> CTSetup[label="next"];
            CMNextQueue -> CMTDONE[label="none in queue"];
            CMTCANCEL -> CMNextQueue[label="modify queue"]
        }
        subgraph cluster_connection {
            style=filled;
            color=lightgrey;
            label="Connection";

            CTConnect[shape=box];
            CConnecting;
            CConnected;
            CTConnect -> CConnecting -> CConnected;

            CTSetup[shape=box];
            CSetting_Up;
            CInitialized;
            CTSetup -> CSetting_Up -> CInitialized;

            CTDisconnect[shape=box];
            CTDisconnect -> CDisconnecting -> CDisconnected;

            CTReconnect[shape=box];
            CTReconnect -> CReconnecting -> CReconnected;
        }
        CInitialized ->CMTRESUME;
        CConnected -> CMTRESUME;
        CDisconnected -> CMTRESUME;
        CReconnected -> CMTRESUME;
    }

Connection Plugins
------------------

Every connection plugin will implement its process as asynchronous code. State
changes are not async, this means the connection plugin methods are called in
the next ioloop. Something triggered in the state machine they cause all
state machine events to be processed in the current loop/thread.
