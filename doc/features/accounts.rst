.. _`features-accounts`:

Accounts
========

.. begin-summary

Netsplice supports managing one or multiple accounts and allows to launch them
in a predefined order. Accounts are displayed in the :ref:`Connection Status
View <features-connection-status>` in the main window and in the systemtray
popup menu.

.. image:: /images/accounts-view.png

.. end-summary

Each account has a name that is shown in the :ref:`Connection Status View
<features-connection-status>`. When an account is connected, Netsplice creates
an internal connection object that references tha account by id, so its type
and name can be received. This allows the GUI to handle displaying useful
information without loading sensitive information from the backend. Accounts
are preference objects that are stored in the user configuration directory
along with the user's grouping and order. The type of the account determines
which dialog will be used to edit the account, which icon is displayed on the
account-list, and what method is used to setup a connection from the account.
The configuration itself is stored as a simple text. All accounts have a
``enabled``, ``autostart``, ``default_route`` and ``persist`` flag.

Context actions for the account in the
:ref:`Connection Status View <features-connection-status>` are:

* Connect
* Connect All
* Disconnect
* Reconnect
* Reset
* Add/Create
* Remove
* Show Log
* Settings
* Group
* Ungroup (only if a parent group exists)
* Move Up
* Move Down
* Move Left
* Move Right

A double click on the account will trigger the connect action.

Below the :ref:`Connection Status View <features-connection-status>` is a
button to connect groups or create a new account.

Actions for the account in the systemtray are connect and disconnect. Those
are dependent of the state of the account.

Accounts have small icons that hint their configuration in the account list.

.. image:: /images/chain_info.png

The account with this icon is chained to a parent.

.. image:: /images/chain_info_parallel.png

The account has chained accounts and they are connected in parallel.

.. image:: /images/chain_info_sequence.png

The account has chained accounts and they are connected in sequence.

.. image:: /images/chain_info_default_route.png

The account provides a default route when connected.

Read more about the
:ref:`Connection Statemachine<features-connection-statemachine>` that is used
to setup, connect and shutdown configured accounts.


Account Order
-------------

The user may order the accounts so they are started in a predefined order.
Depending on an accounts routing configuration the order can have a
significant impact on performance. Improper use of account ordering can make
communication impossible or prevent connecting a VPN.

A good rule of thumb is to start the account that sets a new default route
first. Then start accounts which only get host routes pushed.

.. _`features-accounts-group`:

Account Group
-------------

The user may order the accounts in a hierarchy so they are started in a
predefined order. The hierarchies can also make the accounts dependent but do
not allow the accounts to interact. A group may define a connect mode
*sequence* or *parallel* how all accounts in the group are connected. When
an account in the list of to-connect accounts needs credentials, they are
requested before the list- connect starts.
Groups are always aggregated in leaf to root direction.

Actions for the group in the Account-List-Treeview are:

* Connect All Accounts
* Disconnect All Accounts
* Reconnect All Accounts
* Create Group
* Rename Group
* Delete Group
* Collapse

When hierarchies of groups are defined, the groups are aggregated to a list
before they connect::

    Global
        SubgroupA
            SubsubgroupAA
                AccountAA1
                AccountAA2
            SubsubgroupAB
                AccountAB1
                AccountAB2
            AccountA1
            AccountA2
        SubgroupB
            SubsubgroupBA
                AccountBA1
                AccountBA2
            SubsubgroupBB
                AccountBB1
                AccountBB2
            AccountB1
            AccountB2

*Connect All* on **Global** will connect the accounts in the following order::

    AccountAA1
    AccountAA2
    AccountAB1
    AccountAB2
    AccountA1
    AccountA2
    AccountBA1
    AccountBA2
    AccountBB1
    AccountBB2
    AccountB1
    AccountB2

.. _`features-accounts-chain`:

Account Chain
-------------

The user may define chains of accounts to control how they are related
and how they interact. In contrast to the *Account Order* this allows better
control for complex routing setups.

When chains of accounts are defined, the accounts are aggregated to
connect in a parent-child order::

    Global (Group)
        AccountA
            AccountA1
                AccountA2
                AccountA3
        AccountB
            AccountB1
                AccountB2

*Connect All* on **Global** will connect the accounts in the following order::

    AccountA
    AccountA1
    AccountA2
    AccountA3
    AccountB
    AccountB1
    AccountB2

*Connect* on **A1** will connect the accounts in the following order::

    AccountA
    AccountA1

*Connect all* on **A1** will connect the accounts in the following order::

    AccountA
    AccountA1
    AccountA2
    AccountA3

The modes that are affecting in which order the accounts of a chain connect
are *Connect Modes* and *Failure Modes*.

Read more about the
:ref:`Connection Statemachine<features-connection-statemachine>` that controls
the transitions.


Connect Modes
-------------

*Account Groups* and *Account Chains* can select from various modes to
configure how they connect all accounts in that group or chain.

None
~~~~

When **None** is selected, no account in the group or chain will be connected.
The accounts can only be connected manually.

**None** is activated when multiple accounts in aggregated accounts have the
default route flag enabled.

Parallel
~~~~~~~~

When the mode **Parallel** is selected, all accounts on the same level of a
group or chain are connected at the same time.

This mode requires that the accounts in the list do not define conflicting
routes. When they do, the results are undefined as the order cannot be
predicted.

*Beware that depending on your system connecting lots of accounts at the same
time can cause hard to debug issues.*

Use this mode to manage multiple different independent networks.

.. graphviz::

    digraph G {
        Host -> backend_vpn[label="10.1.1.0/24)"]
        Host -> frontend_vpn[label="(10.1.2.0/24)"]
        Host -> database_vpn[label="(10.1.3.0/24)"]
        Host -> payment_vpn[label="(10.1.4.0/24)"]
    }

Sequence
~~~~~~~~

When sequence is selected, all accounts are connected from top to bottom.
Netsplice waits until each connection is up, this mode takes longer than the
parallel connect.

The wait time between each connection can be defined with the global
preference sequence_wait_after_connect. The default value is 1 second. Waiting
a bit gives the system time to process the network changes without causing
funny side effects.

This mode is useful if you connect to a many different VPN endpoints that set
a bunch of routes.

.. graphviz::

    digraph G {
        Host -> firewall_internet[label="(10.1.0.0/24)"]
        firewall_internet -> firewall_backend[label="(10.1.1.0/24)"]
        firewall_backend -> vpn_database[label="(10.1.2.1/24)"]
    }

Failure Mode
------------

**Connect Failure Mode** configures how Netsplice should act when an account
fails to connect or when a account of a chain disconnects.

Ignore
~~~~~~

The *Ignore* failure mode is the default setting for chains and
will cause all other accounts that are in the chain to continue
connecting.

Disconnect
~~~~~~~~~~

The *Disconnect* failure mode will cause all other accounts in the chain to
disconnect when the account fails to connect or reconnect.

Reconnect
~~~~~~~~~

The *Reconnect* failure mode will cause the owning account to reconnect. The
account that causes the failure will also reconnect. Other accounts in the
parent chain are not reconnected, but all dependents will.

Examples
~~~~~~~~

Create a VPN connection that tunnels over TOR::

    Account "Tor 5960"
        Chain [Reconnect]
        Advanced: SocksPort 5960

        Account "IPredator v4 pool"
            Chain [Reconnect]
            Advanced: TCP only, socks-proxy=localhost 5960
            ProcessLauncher: create/privileged host route to guard node

Ensure no Tor traffic is leaked to the attached network::

    Account "IPredator v4 pool"
        Chain [Reconnect]
        Advanced: UDP/443
        Optional ProcessLauncher: create/privileged iptables rules

        Account "Tor 5960"
            Chain [Reconnect]
            Advanced: SocksPort 5960
    Manual Network Settings: SOCKS Host localhost Port 5960 and RTFM!

Create SSH socks proxy to a node behind a hidden service::

    Account "Tor 5960"
        Chain [Reconnect]
        Advanced: SocksPort 5960

        Account "SSH myh1dd3n5ervice1.onion"
            Chain [Reconnect, Parallel]
            Advanced: -D 5961
                      -o ProxyCommand ... socksport=5960

            Account "SSH Firewall Navigation"
                Chain [Reconnect]
                Advanced: -D 5962
                          -o ProxyCommand ... socksport=5961

            Account "SSH Firewall Students"
                Chain [Reconnect]
                Advanced: -D 5963
                          -o ProxyCommand ... socksport=5962
    Configure ~/.ssh/config Host/ProxyCommand.

    # ProxyCommand /usr/bin/socat STDIO SOCKS4A:localhost:%h:%p,socksport=11000
    # ProxyCommand /usr/bin/nc -x localhost:11001

    # socat: 1.7.3.2
    # nc: openbsd-netcat 1.105-r1
    # Other parameters are needed for different versions of nc.
