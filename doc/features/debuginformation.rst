.. _`features-debuginformation`:

Debug Information
=================

.. begin-summary

In certain cases the user needs provider support and the provider needs
information to debug the problem. Then the provider usualy requests
information like the interface list, routing-table, firewall configuration,
software versions, log output and more infos that only advanced users can
easily provide. Those infos usualy contain sensitive information (the clients
real ip or local network information) that should be reviewed before sending
it to the provider. The Debug Information view collects all the available
infos, lets the user review and edit or comment the details and compiles a
report that can be sent to the provider as via a well defined REST API.

.. image:: /images/debuginformation.png

.. end-summary

Log Contents
------------

The user may choose to exclude information from the log by unchecking the
select boxes

The available information should include

* ifconfig/ipconfig
* route
* traceroute to gateway
* log

Create Report
-------------

To create the report from the selected data sources the action CreateReport
has to be clicked. This Action requests the privileged service to gather the
information and return the report

The result is displayed in a read-write text box that is accompanied by two
actions: Copy to Clipboard (for paste in a e-mail) and Save as encrypted
archive.

NOTE: Actually one of the biggest issues is that VPN clients accumulate
sensitive data all over the place. The basic rule of thumb here is to never
store cleartext data that users can shoot themselves with. This support
funtion can only work with encryption by default.

Encrypt Report
--------------

The report can be encrypted and sent to the Provider of the active accounts
when they provide a support REST interface with a public key.
