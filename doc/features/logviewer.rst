.. _`features-logviewer`:

Log viewer
==========

.. begin-summary

Connecting to VPN providers causes certain log messages from OpenVPN and from
the application that are needed to be viewed in order to ensure correct
function. Those logs need to be colored and filtered for debug, info, warning
and error messages. The log messages are collected during the runtime of
Netsplice and give the user and the provider's support essential information
about what is going on.

.. image:: /images/logviewer.png

.. end-summary

The log viewer can be accessed from the main menu and displays a window with
actions to filter and sort the output. The default is not to filter any
messages and display all messages in reverse order. The messages are
pre-processed to demote or promote certain messages as some messages come in
as 'info' while they should be highlighted as error while others state 'error'
but should in fact only be information.
The log viewer will show when the backend requests the GUI to display for example
when a account failed to connect or when the backend crashed.
New log messages are appended to the list and when the user has scrolled to the
end, the list continues to keep the end visible.

Log entry colors
----------------

The lines of each log entry are highlighted by color:

* Bold red: Critical
* Red: Error
* Orange: Warning
* White: Info, Debug

Filter of messages
------------------

On the top it is possible to filter the messages by their message level.
Multiple selections are possible.

Available filters are:

* Debug
* Info
* Warning
* Error
* Critical

When a text is entered to the filter, only messages containing the string are
displayed. The text filter can be toggled case-sensitive.
The clear filter action can be used to reset all filters and display all
available log messages.

Log actions
-----------

The log viewer provides two actions: copy to clipboard and send to support.
The lines in the log viewer may be selected and copied to the clipboard using
the context menu.
