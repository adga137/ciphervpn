.. _`features-mainmenu`:

Main Menu
---------

The Window Menu is available to access more features. In OSX or ubuntu-unity
the menu is displayed at the screen-top, in Windows and other window managers
it is displayed in the window.

* File

  * :ref:`Add Connection <features-account-config>`
  * Connect All
  * Disconnect All
  * Separator
  * Preferences
  * Separator
  * Quit
* Edit

  * :ref:`Preferences <features-preferences>`
  * :ref:`Accounts <features-accounts>`
* Debug

  * :ref:`Logs <features-logviewer>`
  * Interfaces
  * Routes
  * Shaping Test
  * Upload Logs
* Help

  * Help
  * :ref:`Logs <features-logviewer>`
  * Separator
  * About
