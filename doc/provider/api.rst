.. _`provider-api`:

Provider API
============

.. begin-summary

A VPN provider may expose its capabilities by publishing some
machine-readable documents that can be accessed by the application.

.. end-summary


capabilities.json
------------------

The capabilities.json defines in machine-readable form what the provider
allows the user to customize.  This file should be published on
``https://provider.host/capabilities.json``.  Publishing it on other
location requires the user to enter the full URI in the :ref:`Add Provider
<features-account-autoconfig-add-provider>` Dialog

Structure
^^^^^^^^^

The capabilities.json should look like this::

    {
      "name": "Provider Name",
      "banner": "base64 encoded png",
      "description": "plain text",
      "domain": "provider.domain",
      "languages": ["en", "de", "se"],
      "default_language": 0,
      "contacts": {
        "support": "URI",
        "abuse": "URI"
      },
      "openvpn": {
        "template": "URI",
        "string_option": {
          "type": "string",
          "label": "label for option",
          "description": "description for option",
          "default": ""
        },
        "select_option": {
          "type": "select",
          "label": "label for select",
          "description": "description for select",
          "default": "value-from-values",
          "values": [
            {
              "value":"literal-config-value",
              "label":"display label in selectbox",
              "description":"description when value is selected"
            },
            {
              "value":"literal-config-value",
              "label":"display label in selectbox",
              "description":"description when value is selected"
            }
          ]
        }
      }
    }

Details
-------

The above example already details some of the important constructs.
Required is the name and description of the provider.  This is for good user
experience and should provide all available details.

String values
^^^^^^^^^^^^^

String values are useful for advanced users only. They allow the override of
names like the tap-device. XXX detail possible uses. String values are
declared with the type string::

    {
      "user": {
        "type": "string",
        "label": "input label",
        "description": "input tooltip / below input text",
        "default": "text used when reset to default",
        "mandatory": true,
        "empty": true
      }
    }

Boolean values
^^^^^^^^^^^^^^

Boolean values are used to toggle configuration options.  They display a
checkbox with a label and a description.  When the user enables the
checkbox, the configuration option is active.  when disabling the checkbox
the configuration option is removed.  Boolean values are declared with the
type boolean::

    {
      "comp-lzo": {
        "type": "boolean",
        "label": "checkbox label",
        "description": "checkbox description",
        "description_true": "description displayed when true",
        "description_false": "description displayed when false",
        "default": true
      }
    }

Select values
^^^^^^^^^^^^^

Select values are used to allow the user to select from a list of values.
They display a selectbox that displays the current selected value and the
possible options.  Selectboxes that have a 'empty' property can be
completely masked.  Most of the capabilities will be described in
selectboxes as they are the most convienient way to allow the user to choose
from the vast possibilities.  Select values are declared with the type
"select"::

    {
      "proto": {
        "type": "select",
        "default": "udp",
        "label": "",
        "description": "",
        "values": [
          {
            "label": "UDP",
            "description": "Uses Packages without receive confirmation",
            "value": "udp"
          },
          {
            "label": "TCP",
            "description": "Uses Packages with receive confirmation",
            "value": "tcp"
          }
        ]
      }
    }

Range Values
^^^^^^^^^^^^

Range values are used when numeric selection is possible but the description
of each value would introduce redundant descriptions.  Range values are
declared with the type range::

  {
    "%%PORT%%": {
      "type": "range",
      "label": "",
      "description": "",
      "default": 1194,
      "from": 1,
      "to": 65534
    }
  }


Description
^^^^^^^^^^^

"name" (should be less than 64 characters), "description" (should be between
100 and 1000 characters) and "domain" are mandatory properties.  "banner"
may be a XXX 100x20 PNG that is displayed on the accounts page and is
optional.  "contacts" allows the application to display hyperlinks to the
providers support and abuse contacts.  Languages is a iso631 language code
that hints the application about translations for the capabilities.  The
translations are fetched separately from a ``translations.json``.  the
default_language is a index (zero based) of the languages array.  If no
Languages are provided, the labels and descriptions from this file are
displayed in the application.  otherwise the translations from the
``translations.json`` will be used.

OpenVPN
^^^^^^^

the openvpn object has the mandatory 'template' property that contains a URI
to a default configuration.  The client may redownload the template before
each connection to ensure up-to-date configuration options from the
provider.  All other properties of the VPN object are possible user
overrides for the template configuration.  Those are accompanied by
user-interface texts and hints and will be displayed in the order as they
are in the JSON file.  Properties that start with '%%' and end with '%%' are
templates which will be replaced by the values the user selects.

To allow the user to change the TCP/UDP mode of the connection the minimal
configuration could be::

    {
      "openvpn": {
        "template": "https://provider.host/default.ovpn",
        "proto": {
          "type": "select",
          "default": "udp",
          "values": [
            {
              "label": "UDP",
              "description": "Uses Packages without receive confirmation",
              "value": "udp"
            },
            {
              "label": "TCP",
              "description": "Uses Packages with receive confirmation",
              "value": "tcp"
            }
          ]
        }
      }
    }

To allow the user to change the Server to connect to the minimal
configuration could be::

    {
      "openvpn": {
        "template": "https://provider.host/default.ovpn",
        "%%HOST%%": {
          "type": "select",
          "default": "public.provider.host",
          "values": [
            {
              "label": "Public VPN",
              "description": "Crowded, Slow but secure",
              "value": "public.provider.host"
            },
            {
              "label": "China",
              "description": "Routed through the firewall to experience the net as if in China",
              "value": "china.provider.host"
            },
            {
              "label": "US",
              "description": "Experience the net as us-citizen",
              "value": "us.provider.host"
            }
          ]
        }
      }
    }

With default.ovpn containing this line::

    remote %%HOST%% 1194
