
Provider Guide
--------------

Docs related to the service provider.

*This is a planned feature that needs more discussion and evaluation*

.. toctree::
   :maxdepth: 1

   api
   config
