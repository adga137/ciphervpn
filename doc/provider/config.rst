.. _`provider-config`:

Provider Config
===============

.. begin-summary

A provider may expose multiple configurations to the client. In order to allow
Netsplice to access the configurations the files have to be provided in an
index and need a checksum and signature to validate their contents once
downloaded.

.. end-summary

Index
-----

The index for configuration files is a JSON file that defines an array of
objects with the following structure:::

    {
      "name": "User readable name",
      "description": "User readable description",
      "uri": "uri://where/to/download/the/file",
      "sha1sum": "207ca3716b29bf598cb0fa3842c431cf56d5ba97",
      "sha256sum": "865f0dd1b71b37fbe0e6853c6e086de137a2c6ba7aec708d7db99bf359b8087b"
    }

The complete index for two configurations would read:::

    [
      {
        "name": "IPv4",
        "description": "Configuration that uses only IPv4, suitable for old-fashioned providers",
        "uri": "https://provider.net/config/ipv4.conf",
        "sha1sum": "207ca3716b29bf598cb0fa3842c431cf56d5ba97",
        "sha256sum": "865f0dd1b71b37fbe0e6853c6e086de137a2c6ba7aec708d7db99bf359b8087b"
      },
      {
        "name": "IPv6",
        "description": "Configuration that uses only IPv6, suitable for bleeding edge providers",
        "uri": "https://provider.net/config/ipv6.conf",
        "sha1sum": "207ca3716b29bf598cb0fa3842c431cf56d5ba97",
        "sha256sum": "865f0dd1b71b37fbe0e6853c6e086de137a2c6ba7aec708d7db99bf359b8087b"
      }
    ]

