.. Packager Index

Packager Guide
--------------

Netsplice is build using docker containers. They are used for
documentation of the build process, allowing anyone to fork the project
and build customized packages. We encourage you to do that using the
various config options and still push and merge the code. This may be
useful to provide defaults for your customers or bundle even more
functionality.

One question that always comes up is: Why docker?

Look at the alternative that every package is built by a human on a more
or less clean copy of the operating system. Besides of the resources,
virtual or physical that are required to run that systems they tend to be
difficult to reproduce for others. Docker allows that reproducibility
from a verified and well tested base image. It makes it simple to share
storage locations and caching certain steps. docker's docker-compose
collects those locations and the Dockerfiles define what the clean environment
should be. The product build only needs to fetch the pip-requirements and can
start 'compiling' the binaries.

Packaging Requirements
----------------------

To build packages a linux server is required. It has to be capable to run
docker and docker-compose, so they need to be the only installed tools.
The first build of the packages will be very long, as docker-compose will
detect that there are no images for the slaves defined. Building them
takes about 5GB of traffic and depending on the hardware several hours.

Once the slave images are build, they need to be rebuild frequently.
Subscribe to the docker feeds to get notified when the base image get
updates. Subscribe to openssl and OpenVPN, when they change Netsplice
changes too.

For the packages it still takes some traffic but mostly compile-time for
the pyinstaller binaries and OpenVPN.

After all packages have been built, they need to be transfered to a
hosting location.

.. _`pkg-index-debugging`:

Package Debugging
-----------------

When a package fails to build or behaves unexpected on a given platform
it is inconvienient to build all packages just to fix that one. The
cleanest way is to run the package and slave container without arguments.
This creates a source package with all versioning and compiles the
installer for the platform::

.. code-block:: console

    docker-compose run --rm slavepackage
    docker-compose run --rm slavebrokensystem

To fix certain steps it is useful to enter the container and run the
statements manually::

.. code-block:: console

    docker-compose run --rm --entrypoint /bin/bash slavebrokensystem
    # build.sh # (to do it over and over, fixing the content of the package)
    # source /usr/local/bin/build.sh.inc
    # cd ${BUILDDIR}/pkg/debian-build/slaves/slavebrokensystem && make all

Be aware that the version changes from 1.0.0 (the release tag) to
1.0.0+abcdef123-dirty. So always check that the right version is build.
Especially after you commit changes.

In case the application is broken only on a specific system, the
developer may use a modified docker-compose.yml to map the real
sourcecode to the build and configure the output on a shared location so
the turn-around times will be shorter.

Changes
-------

There is always a better way to do things. The packages were created with
some basic knowledge of the environments. This can lead to requirements
that install more packages or run more code. Some of the constructs are
required to make it work for all worlds (eg a product name without
capital letters and spaces). Build all possible versions::

.. code-block:: console

    git tag '99.99.99'
    make all # netsplice-99.99.99.tar.bz2
    echo '#' >> Makefile # make tag dirty
    make all # netsplice-99.99.99+dirty.tar.bz2
    git add Makefile
    git commit -m 'comitted/clean'
    make all # netsplice-99.99.99+1.tar.bz2
    echo '#' >> Makefile # make working tree dirty
    make all # netsplice-99.99.99+1.dirty.tar.bz2

Cleanup
-------

Most of the cleanup is done automatically using the docker-compose --rm
switch. This instructs docker to purge all /var/tmp data and whatever
tools modified. Some volumes (see docker-compose.yml) can be written by
the containers, notably the installable path. When doing automatic builds
during development the directory will contain many versions that never
should be exported from the system. Some slaves depend on shared volumes
(osx) that should be cleaned up by the build.sh script.

Platforms
---------

To learn more about the different platforms, how to install and how the
packages are build on that platform select from this list::

.. toctree::
   :maxdepth: 1
   :includehidden:

   ../pkg/win
   ../pkg/osx
   ../pkg/debian
   ../pkg/documentation
   ../pkg/ubuntu
   ../pkg/source
   ../pkg/build
