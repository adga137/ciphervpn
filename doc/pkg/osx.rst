.. _pkg-osx:

OS X
=====


Installation
------------

.. begin-user-installation

* Download Installer for OSX.
* Double-click the Installer
* Accept the License Agreement
* Drag Netsplice Icon to Applications

The installation is finished. Netsplice can now be launched from the
Applications directory. As Netsplice is currently not a signed application, OSX
will refuse to start the first time. To start the application press the Shift
Key and click open in the Context menu. After the first confirmation OSX will
remember the choice until Netsplice is updated.

.. end-user-installation


Package
-------

The OSX package cannot be build in containers, at least for now. Therefore
the container named sysosx is a jump-container that connects to a prepared
osx host and triggers the build on that machine.

When the package is build `pkg/debian-build/slaves/sysosx/bin/build.sh`
is executed. This script loads the config from `sysosx/config/slave-config`
and ssh's into the remote system. On the remote system a prepared environment
is required (pyside installed, pip contaminated the system, homebrew, read the
`Readme.rst` in the sysosx directory). In that environment the build downloads
the current package, unpacks it, builds the openvpn binaries, runs pyinstaller
and compiles a app package in a dmg. When completed, the results are uploaded
from the system into the installable directory.
