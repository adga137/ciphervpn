.. _gpl3:

Licenses
--------

This document displays the license of the project.

Full License
============


Netsplice is released under the terms of the `GNU GPL version 3`_.

::

    Netsplice is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Netsplice is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Netsplice.  If not, see http://www.gnu.org/licenses/.

.. _`GNU GPL version 3`: http://www.gnu.org/licenses/gpl.txt

.. _`PySide`: http://qt-project.org/wiki/PySide

Netsplice uses icons from:::

    Blueberry Basic By IcoJam

    Author Website: http://www.icojam.com
    License: Creative Commons Public Domain Mark 1.0

    Icons iPhone Icons  in this pack: 105 icons

    Designer: IconShock
    Author Website: https://www.iconshock.com/
    License: Creative Commons Attribution (by)

GPLv3 License
=============

.. image:: gpl.png

.. include:: ../LICENSE
  :literal:
