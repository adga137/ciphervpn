.. Netsplice documentation master file

Netsplice
=========

Release v \ |version|.

.. include:: user/general.rst
  :start-after: begin-general
  :end-before: end-general

User Guide
----------

.. toctree::
  :maxdepth: 3

  user/index
  features/index
  developer/index


License
-------

License for Netsplice

.. toctree::
  :includehidden:

  gpl
