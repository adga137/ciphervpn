.. _developer-api-backend:

==============
API-Backend
==============

This document describes the components of the backend application.

Every backend request url has the following elements:

========= ====================================================================
Path      Description
========= ====================================================================
module    The module that is responsible for handling the request. Modules are
          "event", "gui", "log", "net", "priv", "report".
action    The action that is called in the module. Those are module dependent.
          "connect", "update", "shutdown". Actions are verbs
model     A model that is requested or should be modified. The exposed models
          are module dependent, not all models are exposed.
========= ====================================================================

Events
------

All events that are received from the subprocesses are signaled to the backend /
connected process using a REST request from the sub process to the
backend.

**Dispatcher Event Structure**::

    POST /module/priv/event
    {
        "id" : "uuid-for-event",
        "index": 0,
        "date": 0,
        "name": "name_of_the_event",
        "type": "event-type",
        "data": {
            "optional": "arguments"
        }
    }

============ ===============================================================
Attribute    Description
============ ===============================================================
**index**    Always-increasing number to ensure correct order in logs and
             prevent out-of-order event execution.
**date**     Milliseconds since 1970-01-01T00:00:00Z
**args**     The events that can be dispatched may need to send more complex
             structured data to the backend. Therefore the structure is not
             defined here.
============ ===============================================================

All events that are sent (eg gui/events) use the same structure.


Log Messages
------------

All log-messages that are captured from the sub processes are signaled to
the backend / connected process using a REST request from the sub process
to the backend.

**Log Message Structure**::

    POST /module/priv/log
    {
        "id" : "uuid-for-event",
        "index": 0,
        "date": 0,
        "level": "debug | info | warning | error | critical",
        "module": "openvpn",
        "context": "/file/under/exection.py:L123"
        "message": "message that needs to be logged"
    }

The backend stores the log messages in memory and may decide to write them on
disk or dispose of old items.


============ ============================================================
Attribute    Description
============ ============================================================
**index**    Always-increasing number to ensure correct order in logs and
             prevent out-of-order event execution.
**date**     Milliseconds since 1970-01-01T00:00:00Z
**level**    Loglevel that can be filtered in the User Interface
**context**  Location where the log message was triggered
**module**   The origin of the log message
============ ============================================================

Modules
-------

The backend API exposes the modules that are registered. The modules do
interfere and interconnect.

Event
+++++

The following actions are supported by the Event Module.


Not yet implemented.

GUI
+++

The API for the GUI is described in a separated document.
:ref:`Backend GUI API<developer-api-backend-gui>`

Log
+++

The following actions are supported by the Log Module.


Net
+++

The following actions are supported by the Net Module.


Module
~~~~~~

The "module" Request allows receiving the current state of all network requests.

XXX not defined yet

**REST Request**::

    GET /module/net

**REST Response**::

    {

    }

Preferences
+++++++++++

The API for the preferences is described in a separated document.
:ref:`Backend Preferences API<developer-api-backend-preferences>`

Privileged
++++++++++

The following actions are supported by the Privileged Module.


Module
~~~~~~

The "module" model request is used to expose the expected privileged state.

XXX not defined

**REST Request**::

    GET /module/priv

**REST Response**::

    {}

Log
~~~

The "log" request is used to receive logging information from the privileged
process.

**REST Request**::

    POST /module/priv/log
    {
        "id" : "uuid-for-event",
        "index": 0,
        "date": 0,
        "level": "debug | info | warning | error | critical",
        "origin": "openvpn",
        "message": "message that needs to be logged"
    }

**REST Response**::

    No Response

**Errors**::

* 400: Validation Error, Value Error (bad JSON)


Event
~~~~~

The "event" request is used to receive event information from the privileged
process. Events trigger dispatcher that may fetch details from the privileged
process.


**REST Request**::

    POST /module/priv/event
    {
        "index": 0,
        "date": 0,
        "origin": "openvpn",
        "type": "type of event",
        "name": "name of event"
    }

**REST Response**::

    {}

**Errors**::

* 400: Validation Error, Value Error (bad JSON)


Report
++++++

The following actions are supported by the Report Module.


Debug
~~~~~

To create a connection using "curl" first disable HTTPS and CHECK_SIGNATURE in
netsplice.config.ipc, then start the privileged App (as root) and invoke the
endpoints. The following code assumes that you have multiple terminals open,
root access and bash as shell:

Setup Server:::

    sed -i '|HTTPS = True|HTTPS = False' \
        src/netsplice/config/ipc.py
    sed -i '|CHECK_SIGNATURE = True|CHECK_SIGNATURE = False' \
        src/netsplice/config/ipc.py
    python src/netsplice/NetsplicePrivilegedApp.py \
        --host localhost --port 10000
    sed -i '|HTTPS = False|HTTPS = True' \
        src/netsplice/config/ipc.py
    sed -i '|CHECK_SIGNATURE = True|CHECK_SIGNATURE = True' \
        src/netsplice/config/ipc.py

List all Accounts:::

    curl localhost:10000/module/preferences/accounts

Add Account with minimal configuration:::

    curl localhost:10000/module/preferences/accounts -X POST \
        --data '{"name":"name",' \
        '"enabled":true,"autostart":false,"configuration":"remote localhost",' \
        '"import_configuration":"remote localhost"}'

Delete Account:::

    curl localhost:10000/module/preferences/account/1 -X DELETE

The sourcecode contains a test_api_backend.sh that contains useful tests for
the api with some basic checks for the correct HTTP code and content.
