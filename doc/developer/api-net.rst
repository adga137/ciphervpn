.. _developer-api-net:

API-Net
=======

This document describes the components of the net application.

Download
########

Downloads are remote files stored to a local location. They are required to be
https encrypted and the TLS fingerprint of the remote certificate is always
checked before the transfer. Optionally a expected signature and its public
key to verify the contents of the downloaded file. The remote file will be
downloaded and after successful verification of the signature.

The network backend will notify the owning backend when the download
progresses or runs in a error state using the event module.

**REST Request**::

    POST /module/download
    {
        "url": "url to download",
        "fingerprint": "TLS fingerprint of remote",
        "signature": "optional base64 encoded signature",
        "sign_key": "optional public key to verify signature",
        "destination": "local filesystem location for storage",
        "sha1sum": "optional sha1 checksum",
        "sha256sum": "optional sha256 checksum"
    }

**REST Response**::

    {
        "id": "id of download"
    }

Cancel a active download by id or remove the reference for the download in the
network backend.

**REST Request**::

    DELETE /module/download/{id}

**REST Response**::

    {
    }

**Errors**::

* 400: Validation Error, Value Error (bad JSON)
* 404: Remote URL does not return
* 502: Remote Request Error (Bad Gateway)
* 504: Gateway timeout


Get the status of a download that was created with the POST request.

**REST Request**::

    GET /module/download/{id}

**REST Response**::

    {
        "url": "url of download",
        "start_date": "YYYY-MM-DDTHH:MM:SSZ",
        "complete": numeric_value(0-100),
        "bps": numeric_value,
        "errors": true/false,
        "error_message": null/"message from backend",
        "signature_ok": null/true/false,
        "checksum_ok": null/true/false
    }

**Errors**::

* 400: Validation Error, Value Error (bad JSON)
* 404: Remote URL does not return
* 502: Remote Request Error (Bad Gateway)
* 504: Gateway timeout
