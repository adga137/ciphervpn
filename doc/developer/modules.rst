.. _modules:

Modules
=======

Netsplice should be extensible and with this requirement its core parts are
already modularized to define the pattern for future extensions. Each
application consists of multiple modules that register REST endpoints, define
module-models and implement dispatchers that do the module-specific work. A
modularized structure looks like this:::

    src
     |- module1
     |   |- model
     |   |   |- request
     |   |   |   |- requestable_value_type1.py
     |   |   |   \- requestable_value_type2.py
     |   |   |- response
     |   |   |   |- value_type_for_return1.py
     |   |   |   \- value_type_for_return2.py
     |   |   |- validator
     |   |   |   \- validator_for_requestable_value.py
     |   |   |- __init__.py
     |   |   |- intern_module_model1.py
     |   |   \- intern_module_model2.py
     |   |- views
     |   |- __init__.py
     |   |- config.py
     |   |- intern_model_controller.py
     |   \- intern_model_implementation.py
     |- module2
     \- app.py

As you can see this cleanly separates the classes that are required to create a
module that acts as a REST endpoint.

The module1/__init__.py will instanciate a model, endpoints and name value that
can be accessed by users of the module. The app.py registers the module before
it starts the REST ManagementServer.
It is required to define endpoints and model:::

    from netsplice.util.ipc.route import get_module_route
    from netsplice.backend.gui.model import model as module_model

    name = "gui"

    endpoints = get_module_route(
        'netsplice.backend',
        [
            (r'/module/gui', 'module'),
            (r'/module/gui/log', 'log'),
            (r'/module/gui/account', 'account'),
            (r'/module/gui/account/check', 'account_check'),
            (r'/module/gui/action/connect', 'connect_action'),
            (r'/module/gui/action/update', 'update_action'),
            (r'/module/gui/action/shutdown', 'shutdown_action'),
            (r'/module/gui/events', 'events'),
        ])

    model = module_model()


As a module is used by other components of the application the interface that is
accessed by those components should be very limited and should be implemented in
module controllers that are triggered by events.

Application Modules
-------------------

Netsplice is composed from multiple applications. Their layout is defined in the
same way as a module. They are responsible to register their modules before the
app is started:::

    app = application()

    # Add Modules

    app.add_module(mainwindow_module)
    app.add_module(preferences_module)
    app.add_module(logviewer_module)
    app.add_module(systray_module)

    app.start(get_route('netsplice.gui'))
    sys.exit(app.exec_())


Models, Dispatcher, Events
--------------------------

Most of the code looks like boilerplate for constructs that could be expressed
without creating so many files and functions. When implementing features inspect
the code of the sibling implementations to decide if the code that is added
handles generic passing of values or business logic. For example it is easy to
add a persistent model value before it is written to the REST response, but this
modification is actually a model-conversion that a dispatcher should decide.
This requires you to define an additional function, name it and then use it in
the controller.

Models
~~~~~~

There are persistent and transient models. The transient models are used for
data retrieval and transformation while the persistent models modify values.
This is not a hard requirement. There are some core application models that can
be inherited, e.g. ``logs`` and ``events`` and non-specific validators like
``boolean``.

The module-model defines the interface for the module components that could be
acted on. The module registers its event_loop on the model_changed/commit event
and runs dispatchers based on the change. The attributes of the module-model are
other models. Those definitions are separated from the models that can be
requested and that will be responded.


Request and Response Models
+++++++++++++++++++++++++++

To ensure that only specified information is transmitted between the processes a
series of models that define requests and responses has to be defined. This
allows the components to be developed independently and always specify the
interfaces they expect. On the other hand a developer needs to implement various
models of the same type in various locations to pass messages. A Request model
should only define fields that are allowed to be passed into the application
using JSON or request arguments. A Response Model should only define fields that
are allowed to be returned as the response of a request. A Controller should use
request and response models like this:::

    def post(self):
        request_model = username_password_model()
        request_model.from_json(self.request.body)
        # do something with request_model.username.get(), e.g. pass it to a module dispatcher
        account_id = self.application.get_module('register').create_account(
            request_model.username.get(), request_model.password.get())
        response_model = account_id_model()
        response_model.id.set(account_id)
        self.write(response_model.to_json())
        self.finish()


With a good documentation on both the request-model and the response model a
user of the API can easily understand what values should be passed, how they are
validated and what values the API will return.


Dispatcher
----------

Business Logic implementations that accept values and modify models based on the
values. Dispatchers connect multiple modules and orchestrate their usage. They
expect that all platform specific code is 'hidden' in functions, they trust the
values they are provided and the values that are generated in dispatchers are
considered 'safe' i.e. no type-checking and conversion, minimal error handling
(everything throws and is caught on an other level). Dispatcher functions are
considered async.

Events
------

Application components and Modules communicate using events. Those can be
subscribed by a dispatcher.
