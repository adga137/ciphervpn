.. _environment:

Setting up a development environment
====================================

This document describes how to setup a environment that allows debugging and
patching the sources of the project.

Cloning the repo
----------------
.. note::
   Stable releases are in *master* branch.
   Development code lives in *develop* branch.

::

    git clone https://XXX/netsplice.git netsplice
    cd netsplice
    git checkout master


Base Dependencies
------------------

Netsplice depends on these base libraries:

* `python 2.7`
* `qt4` libraries
* `openssl`
* `openvpn <http://openvpn.net/index.php/open-source/345-openvpn-project.html>`_

Debian
^^^^^^

In debian-based systems, install the following packages::

    $ apt-get install git python-dev python-setuptools \
    python-virtualenv \
    python-pip python-openssl build-essential openvpn make \
    pyside-tools python-pyside

.. _virtualenv:

Working with virtualenv
-----------------------

Intro
^^^^^

*Virtualenv* is the *Virtual Python Environment builder*.

It is a tool to create isolated Python environments.

The basic problem being addressed is one of dependencies and versions,
and indirectly permissions. Imagine the user has an application that needs
version 1 of LibFoo, but another application requires version 2. How can
the user use both these applications? When installing everything into
/usr/lib/python2.7/site-packages (or whatever the platform's standard
location is), it's easy to end up in a situation where applications are
unintentionally upgraded that shouldn't be upgraded.

Read more about it in the `project documentation page <http://pypi.python.org/pypi/virtualenv/>`_.


Create and activate environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Create a virtualenv in any directory::

    $ mkdir ~/Virtualenvs
    $ virtualenv --system-site-packages ~/Virtualenvs/netsplice
    $ source ~/Virtualenvs/netsplice/bin/activate
    (netsplice)$

Note the change in the prompt.

.. _pydepinstall:

Install python dependencies
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Install python dependencies with ``pip``. If this is executed inside the
working environment, they will be installed avoiding the need for
administrative permissions::

    (netsplice)$ pip install -r pkg/requirements.pip
    (netsplice)$ pip install -r pkg/requirements-pkg.pip

.. _makeresources:

Make resources
--------------

Compile the resource files::

    (netsplice)$ make all

This step is to be repeated each time  ``.ui`` translations or a ``.qrc`` file
is changed.

Debugging
---------

If everything went well, it should now be possible to run the application by
invoking ``make run``. When no window is displayed, or for more verbose
output, change the config.flags and config.log values. Sometimes it is useful
to see backtraces. ``GDB`` allows stepping when python is compiled with -ggdb
and not stripped. ``gdb attach PID`` to a running process and use the ``py-``
prefixed statements. When you are using eclipse for sourcecode editing the
integrated debugger is able to step through the code when the virtualenv
binaries are used.

Usually it is sufficient to write unit tests for the location you want to
investigate and ``print`` the values in the process.

Elevated binaries
-----------------

The application detects that it is running in source mode. This is required
because the production mode can run ```pksudo NetsplicePrivilegedApp``` and
register the app in the installation process so the user can securely elevate.
When you execute from source this would allow *all* python scripts running as
root. Therefore a developer is requested to build the openvpn binary from
source and place it in the config.LIBEXEC_BUILD location e.g
``var/build/openvpn/v2.4.0/sbin/openvpn`` and setuid root this executable.
This way the python scripts can be run as user while the elevated tools get
the required permissions.

Platform binaries
-----------------

Sometimes issues can only be reproduced on a $clean_platform but not on the
development machine. Use the ``pkg/debian-build`` makefiles to build
installers with debug-flags enabled or logger-statements added. When you need
to have short turnaround times, it is usually worth skipping the full clean build
by ``docker-compose run --rm --entrypoint /bin/bash slaveXXX`` and run only
parts of the ``/usr/local/bin/build.sh`` script or the makefiles.
To understand what is going on also read :ref:`Package Debugging<pkg-index-debugging>` and :ref:`Package Build<pkg-build>`

Logs
----

Logs are not written to the disk by default. During startup it is possible
that the logs are not displayed because the GUI cannot access the backend.
In this case it is useful to force the logger to write logs to the disk:
``mkdir -p ~/.config/Netsplice/logs/FORCE_LOG_FILES``.
