.. _developer-api-backend-preferences:

=======================
API-Backend-Preferences
=======================

This document describes the components of the backend application that cover the
preference retrieval and setting. This API is part of the
:ref:`Backend-API<developer-api-backend>`.


The "preferences" model request allows to receive and update application
settings.

**REST Request**::

    GET /module/preferences

**REST Response**::

    {
    "groups": [],
    "grouped_accounts": [],
    "accounts": [],
    "ui": {
        "start_hidden": "boolean",
        "start_on_boot": "boolean",
        "show_welcome_on_start": "boolean",
        "theme": "theme-name",
        "locale": "locale-name",
        "loglevel": "debug | info | warning | errors"
        }
    }


Events
######

Long polling events request to get notified when preferences change.

**REST Request**::

    GET /module/preferences/events

**REST Response**::

    No Response

Account
#######

The "account" model requests exposes the persistent accounts that Netsplice
manages.

Lists the available accounts or a specific account.

**REST Request**::

    GET /module/preferences/accounts[/account_id]

**REST Response** for /module/preferences/accounts::

    [
        {
            "id": "account-uuid",
            "name": "user-string",
            "type": "OpenVPN | Tor | Tinc | Wireguard",
            "configuration": "user-string",
            "import_configuration": "user-string",
            "enabled": "boolean",
            "autostart": "boolean"
        }
    ]

**REST Response** for /module/preferences/accounts/<account_id>::

    {
        "id": "account-uuid",
        "name": "user-string",
        "type": "OpenVPN | Tor | Tinc | Wireguard",
        "configuration": "user-string",
        "import_configuration": "user-string",
        "enabled": "boolean",
        "autostart": "boolean"
    }

Create an account with the given properties. The backend will persist the
account.

**REST Request**::

    POST /module/preferences/accounts
    {
        "name": "user-string",
        "type": "OpenVPN | Tor | Tinc | Wireguard",
        "configuration": "user-string",
        "import_configuration": "user-string",
        "enabled": "boolean",
        "autostart": "boolean"
    }

**REST Response**::

    {
        "id": "{new-account-uuid}"
    }

**Errors**:

* 400: Validation Error, Value Error (bad JSON)

Update an existing account with the given properties. The account must not be
connected.

**REST Request**::

    PUT /module/preferences/accounts
    {
        "id": "account-uuid"
        "name": "user-string",
        "configuration": "user-string",
        "import_configuration": "user-string",
        "enabled": "boolean",
        "autostart": "boolean"
    }

**REST Response**::

    {
        "id": "{account-uuid}"
    }

**Errors**:

* 400: Validation Error, Value Error (bad JSON)

Delete an existing account.

**REST Request**::

    DELETE /module/preferences/accounts/<account_id>

**REST Response**::

    {
        "id": "{deleted-account-uuid}"
    }

**Errors**:

* 400: Validation Error, Value Error (bad JSON)
* 404: NotFound Error: account_id not available
* 403: ConnectedError: Account is currently connected and cannot be deleted


Backend and UI
##############

Backend preferences are configurations that are influencing the behavior of the
backend. UI preferences are configurations that are considered of cosmetic value
like theme or confirmed dialogs.
Those preferences are accessed using the value and complex controllers to allow
simple generic extension of preference values.

**Receive**::

    GET /module/preferences/complex/backend
    GET /module/preferences/complex/ui

**Update**::

    PUT /module/preferences/complex/backend
    {
    "complete": "backend_structure"
    }
    PUT /module/preferences/complex/ui
    {
    "complete": "ui_structure"
    }
    PUT /module/preferences/value/backend/max_connect_time
    {
    "value": 30
    }
    PUT /module/preferences/value/ui/start_hidden
    {
    "value": true
    }


**Backend Structure**::

    {
    "max_connect_time": "integer",
    "max_shutdown_time": "integer",
    "ipc_certificate_valid_days": "integer",
    "default_openvpn_version": "v1.2.3",
    "graphical_sudo_application": "[/path/sudo/application]",
    "store_password_default": "never or keystore or session"
    }

**UI Structure**::

    {
    "shortcut_selected_rename": "F2",
    "show_log_on_connection_failure": true,
    "show_welcome_on_start": false,
    "locale": "None",
    "shortcut_show_logs": "ctrl+l",
    "close_to_systray": false,
    "logviewer_text_lines_per_item": 3,
    "start_on_boot": false,
    "statusbar_show_version": true,
    "theme": "default",
    "shortcut_selected_disconnect": "ctrl+d",
    "shortcut_connect_all": "ctrl+g",
    "statusbar_show_connections": false,
    "shortcut_selected_connect": "ctrl+c",
    "shortcut_help": "F1",
    "statusbar_show_heartbeat": true,
    "systray_animate_icon": true,
    "shortcut_selected_edit": "F4",
    "max_logviewer_items": 5000,
    "start_hidden": false,
    "shortcut_show_preferences": "ctrl+,",
    "start_drag_distance": -1,
    "logviewer_date": "timeago",
    "systray_show_notifications": true,
    "shortcut_create_connection": "ctrl+n",
    "shortcut_quit": "ctrl+q",
    "statusbar_show_product_owner": true
    }

Complex preference
##################

Sometimes a setting has a complex structure that needs the same access
method as the value types. The rest interface is slightly different to the value
model and requires to provide a valid structure of the JSON PUT.

**REST Request**::

    GET /module/preferences/complex/<collection>/<name>

**REST Response**::

    {
    "complex": ["structure"]
    }

**Errors**:

* 400: Validation Error, Value Error (bad JSON / wrong type for value)
* 404: NotFound Error: name is not a valid backend preference or name is not
  a marshalable.

Update a complex attribute.


**REST Request**::

    PUT /module/preferences/complex/<collection>/<name>
    {
    "complex": ["structure"]
    }

**REST Response**::

    {
    "complex": ["structure"]
    }


Groups
######

Accounts may be grouped and ordered in those groups. The groups define the
available groupings and grouped_accounts define how an account is associated
to the group.

Get the list of available groups

**REST Request**::

    GET /module/preferences/groups

**REST Response**::

    [
        {
        "id": "group-id",
        "name": "name of group",
        "parent": "parent-group-id",
        "weight": "positive-integer"
        }
    ]

Get the information on the given group_id.

**REST Request**::

    GET /module/preferences/groups/<group_id>

**REST Response**::

    {
    "id": "group-id",
    "name": "name of group",
    "parent": "parent-group-id",
    "weight": "positive-integer"
    }

**Errors**:

* 400: Validation Error: group_id is not valid.
* 404: NotFound Error: given group-id not found.

Create a new Group.

**REST Request**::

    POST /module/preferences/groups
    {
    "name": "name of group",
    "parent": "parent-group-id",
    "weight": "positive-integer"
    }

**REST Response**::

    {
    "id": "group-id"
    }


**Errors**:

* 400: Validation Error: a value is not valid.
* 404: NotFound Error: given parent-group-id not found.

Update the information on the given group_id.

**REST Request**::

    PUT /module/preferences/groups
    {
    "id": "group-id",
    "name": "name of group",
    "parent": "parent-group-id",
    "weight": "positive-integer",
    "collapsed": boolean
    }

**REST Response**::

    {
    "id": "group-id"
    }

**Errors**:

* 400: Validation Error: group_id is not valid.
* 403: NotAllowed Error: parent-group-id would cause circular or the group_id
  is immutable.
* 404: NotFound Error: given group-id or parent-group-id not found.

Delete the given Group

**REST Request**::

    DELETE /module/preferences/groups/<group_id>

**REST Response**::

    {
    "id": "group-id"
    }


**Errors**:

* 400: Validation Error: group_id is not valid.
* 403: NotAllowed Error: group has children and cannot be deleted or the group
  is immutable.
* 404: NotFound Error: given group-id not found.

Grouped Accounts
################

Grouped Accounts are the user-order of accounts in groups. Grouped accounts are
derived from the accounts collection. They are created automatically with a new
account and associated to the global group.

Get the list of available grouped_accounts.

**REST Request**::

    GET /module/preferences/grouped_accounts

**REST Response**::

    [
        {
        "id": "account-id",
        "name": "name of account",
        "group_id": "parent-group-id",
        "type": "account_type",
        "weight": "positive-integer"
        }
    ]

Get the a single grouped_account.

**REST Request**::

    GET /module/preferences/grouped_accounts/<account_id>

**REST Response**::

    {
    "id": "account-id",
    "name": "name of account",
    "group_id": "parent-group-id",
    "type": "account_type",
    "weight": "positive-integer"
    }

**Errors**:

* 400: Validation Error: account_id is not valid.
* 404: NotFound Error: given group-id or account_id not found.

Update a grouped account.

**REST Request**::

    PUT /module/preferences/grouped_accounts/<account_id>
    {
    "id": "account-id",
    "group_id": "parent-group-id",
    "weight": "positive-integer"
    }

**REST Response**::

    {
    "id": "account-id",
    "group_id": "parent-group-id",
    "weight": "positive-integer"
    }

**Errors**:

* 400: Validation Error: account_id is not valid.
* 404: NotFound Error: given group-id or account_id not found.


Overrides
#########

Override preferences are configurations that are considered before any
connection is setup, allowing the user to specify default encryption
expectations. Overrides are indexed by name & type and have a single value.

Get the value of a specific override value.

**REST Request**::

    GET /module/preferences/overrides/<name>/<type>

**REST Response**::

    {
    "value": "anything"
    }

**Errors**:

* 400: Validation Error, Value Error bad name & type
* 404: NotFound Error: name & type is not a valid override

Get the all override preferences.

**REST Request**::

    GET /module/preferences/overrides

**REST Response**::

    {
    "config": [{
        "name": "remote",
        "type": "OpenVPN",
        "value": "localhost 1111"
        }],
    "enabled": true
    }

Create the value of a specific override name & type.

**REST Request**::

    POST /module/preferences/override/<name>/<type>
    {
    "value": "new_value"
    }

**REST Response**::

    {
    "value": "new_value"
    }

**Errors**:

* 400: Validation Error, Value Error bad name & type
* 400: Not Unique Error name & type already existing
* 404: NotFound Error: name & type is not a valid override


Update the value of a specific override name & type.

**REST Request**::

    PUT /module/preferences/override/<name>/<type>
    {
    "value": "new_value"
    }

**REST Response**::

    {
    "value": "new_value"
    }

**Errors**:

* 400: Validation Error, Value Error bad name & type
* 404: NotFound Error: name & type is not a valid override

Remove the value of a specific override name & type combination.

**REST Request**::

    DELETE /module/preferences/override/<name>/<type>

**REST Response**::

    {
    "name": "<name>",
    "type": "<type>"
    }

**Errors**:

* 400: Validation Error, Value Error bad name & type
* 404: NotFound Error: name & type is not a valid override

Plugins
#######

Plugin Preferences allow the "external" components to register additional
fields that can be modified by the user and accessed in the components.

Get preferences for a plugin. This is a readonly endpoint. To change attributes
the attribute name needs to be provided in the url.

**REST Request**::

    GET /module/preferences/plugin/<plugin_name>

**REST Response**::

    {
    "name": "plugin_name",
    "field1": "field_value"
    }

**Errors**:

* 400: Validation Error, Value Error bad name
* 404: NotFound Error: the given plugin name did not register a preference


Get a preference value for a attribute in the plugin preferences.

**REST Request**::

    GET /module/preferences/plugin/<plugin_name>/attribute/<name>

**REST Response**::

    {
    "value": "current_value"
    }

**Errors**:

* 400: Validation Error, Value Error bad name & type

* 404: NotFound Error: the given plugin name did not register a preference or
    the attribute did not exist.

Update a preference value for a attribute in the plugin preferences.

**REST Request**::

    PUT /module/preferences/plugin/<plugin_name>/attribute/<name>
    {
    "value": "new_value"
    }

**REST Response**::

    {
    "value": "new_value"
    }

**Errors**:

* 400: Validation Error, Value Error bad name & type

* 404: NotFound Error: the given plugin name did not register a preference or
    the attribute did not exist.

Plugin Collection
#################

Plugins are able to register for collections. This allows plugins to provide
additional values for a collection instance. For example a account_item in the
accounts collection adds the field 'example' which has to store different values
for each account_item.

    tcurl 200 "v2." GET \
      http://localhost:10000/module/preferences/plugin/openvpn/collection/accounts/instance_id/${account_1_id}/attribute/openvpn_version
    tcurl 200 '"default_openvpn_version"' GET \
      http://localhost:10000/module/preferences/plugin/openvpn
    tcurl 200 '"openvpn_version"' GET \
      http://localhost:10000/module/preferences/plugin/openvpn/collection/accounts/instance_id/${account_1_id}
    tcurl 200 '"default_signal"' GET \
      http://localhost:10000/module/preferences/plugin/process_sniper
    tcurl 200 '"kill_list"' GET \
      http://localhost:10000/module/preferences/plugin/process_sniper/collection/accounts/instance_id/${account_1_id}

    tcurl 400 "" PUT \
      http://localhost:10000/module/preferences/plugin/openvpn/collection/accounts/instance_id/${account_1_id}/attribute/openvpn_version

    tcurl 200 "v3.0.0" PUT \
      http://localhost:10000/module/preferences/plugin/openvpn/collection/accounts/instance_id/${account_1_id}/attribute/openvpn_version \
      "v3.0.0"
    tcurl 200 "v4.0.0" PUT \
      http://localhost:10000/module/preferences/plugin/openvpn/collection/accounts/instance_id/${account_2_id}/attribute/openvpn_version \
      "v4.0.0"

    tcurl 200 "v3.0.0" GET \
      http://localhost:10000/module/preferences/plugin/openvpn/collection/accounts/instance_id/${account_1_id}/attribute/openvpn_version
    tcurl 200 "v4.0.0" GET \
      http://localhost:10000/module/preferences/plugin/openvpn/collection/accounts/instance_id/${account_2_id}/attribute/openvpn_version

    tcurl 200 '"kill_list"' PUT \
      http://localhost:10000/module/preferences/plugin/process_sniper/collection/accounts/instance_id/${account_1_id} \
      '{"name":"process_sniper","version":1,"kill_list":[{"event":"disconnect", "commandline": "firefox-beta", "signal":"SIGSTOP"}]}'
    tcurl 200 '"firefox-beta"' GET \
      http://localhost:10000/module/preferences/plugin/process_sniper/collection/accounts/instance_id/${account_1_id}

Get all preference values for a instance in the plugin collection preferences.

**REST Request**::

    GET /module/preferences/plugin/<plugin_name>/collection/<collection_name>/instance_id/<instance_id>

**REST Response**::

    {
    "value": "current_value"
    "name": "plugin_name"
    }

**Errors**:

* 400: Validation Error, Value Error bad plugin_name, collection_name or
  instance_id.
* 404: NotFound Error: the given plugin name did not register a preference or
  the instance did not exist, the collection does not exist.

Get a preference value for a attribute in the plugin collection preferences.

**REST Request**::

    GET /module/preferences/plugin/<plugin_name>/collection/<collection_name>/instance_id/<instance_id>/attribute/<name>

**REST Response**::

    {
    "value": "current_value"
    }

**Errors**:

* 400: Validation Error, Value Error bad plugin_name, collection_name or
  attribute_name
* 404: NotFound Error: the given plugin name did not register a preference or
  the attribute did not exist, the collection does not exist or the attribute
  does not exist.


Update all preference values for a instance in the plugin collection preferences.

**REST Request**::

    PUT /module/preferences/plugin/<plugin_name>/collection/<collection_name>/instance_id/<instance_id>
    {
    "value": "current_value"
    "name": "plugin_name"
    }

**REST Response**::

    {
    "value": "current_value"
    "name": "plugin_name"
    }

**Errors**:

* 400: Validation Error, Value Error bad plugin_name, collection_name,
  instance_id or the given json does not match the plugins structure.
* 404: NotFound Error: the given plugin name did not register a preference or
  the instance did not exist, the collection does not exist.

Update a preference value for a attribute in the plugin collection preferences.

**REST Request**::

    PUT /module/preferences/plugin/<plugin_name>/collection/<collection_name>/instance_id/<instance_id>/attribute/<name>
    {
    "value": "new_value"
    }

**REST Response**::

    {
    "value": "new_value"
    }

**Errors**:

* 400: Validation Error, Value Error bad plugin_name, collection_name,
  instance_id or the given json does not match the plugins structure.
* 404: NotFound Error: the given plugin name did not register a preference or
  the attribute did not exist, the collection does not exist or the attribute
  does not exist.
