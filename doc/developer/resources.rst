.. _resources:

PySide Resource files
=====================

Compiling resource/ui files
---------------------------

Refresh resource/ui files every time an image or a resource/ui (.ui / .qc) is
changed. From the root folder::

  % make gui

qrc and the qss/less files are defined as dependencies. That allows to make run
and automatically get the changes.

Theming with qss
----------------

With qss it is possible to change the colors and size of most of the properties
of the application. The basic theming contains widget backgrounds and general
colors for the following widgets:

* QToolTip
* QWidget
* QLineEdit
* QTextEdit
* QTableView
* QScrollBar
* QPushButton
* QListView
* QMainWindow
* QCheckBox
* QComboBox
* QMenuBar
* QMenu

Some widgets have been developed to suit our purposes and therefore contain
nonstandard selectors:

* list_item has the property active to indicate that it is selected
* account_list_item has the properties connected, connecting
* account_group_item is a list_item
* account_list
* QPushButton#action_indicator is a special button to collapse
  account_list_items

Read more about styling QT with QSS `Stylesheet Customizing`_
and `Stylesheet Reference`_.

.. _`Stylesheet Customizing`: https://doc.qt.io/qt-4.8/stylesheet-customizing.html
.. _`Stylesheet Reference`: https://doc.qt.io/qt-4.8/stylesheet-reference.html

Account List
------------

The Accountlist is a QScrollArea with nested account_group_items and
account_list_items. Their *tree* style display is achieved using the natural
widget nesting and the margins of that list.
Each Account list item should be considered with the properties 'connecting',
'connected' combined with 'active'.

.. image:: /images/theme-account-list.png

Account List Item
~~~~~~~~~~~~~~~~~

A account_list_item is child in a QVLayout and therefore displayed with
vertical spacing. The account_list_item should not have margins on the right
side. To modify the height of the account_list_item, a config value needs to
be adjusted as the layout cannot take the min-height infos from the qss.
The config is gui.account_list.config.MAX_ITEM_HEIGHT. In this config file the
icons for the account_types can configured with the animation icons for the
connecting state.

.. image:: /images/theme-account-list-item-dimensions.png

The widget margins define the tree-style display of the items. The spacing is
highlighted in this illustration:

.. image:: /images/theme-account-list-item-widget-spacing.png

QSS and LESS mixins
-------------------

QSS is a subset of CSS and supports most features known to web developers. As
advanced webdeveloper higher languages like LESS or SASS are useful to avoid
repetive typing of constants. This concept is unknown to QSS.
Therefore Netsplice adapted one of the most useful features, named mixins, to
the theming. It loads and evaluates a less file and replaces all mixins in the
QSS file.

default.less::

    @basecolor: red;
    @textcolor: white;

default.qss::

    QWidget {
      background-color: @basecolor;
      color: @textcolor;
    }
    QPushButton {
      border: 1px solid @textcolor;
    }

The above statements will be processed to qss before passing it to the QT style
engine::

    QWidget {
      background-color: red;
      color: white;
    }
    QPushButton {
      border: 1px solid white;
    }

Other features of LESS are not implemented.

Themes
------

With adding a qss resource to the res-themes directory it is possible to
completely alter the application. Please pull request your contribution.

Blue
----

A *blue*-theme has been created during development.

.. image:: /images/theme-blue.png
