.. _developer-api-backend-gui:

===============
API-Backend-GUI
===============

This document describes the components of the backend application that are
provided for a GUI. This API is part of the :ref:`Backend-API
<developer-api-backend>`.


The following actions are supported by the GUI Module.


Module
~~~~~~

The "module" model-request is used to get the current state of the backend
suitable for presentation.

**REST Request**::

    GET /module/gui

**REST Response**::

    {
        "connections": [],
        "credentials": [],
        "log": [],
        "check_connection": [],
        "events": []
    }

Events
######

Long-Polling endpoint that responds when the gui-model has changed or a event
has been queued by the backend.

**REST Request**::

    GET /module/gui/events

**REST Response**::

    No Response, use Module Request.


Module Actions
##############

Update starts an available update. Restarts the application and all connections
when the update is complete.
*TBD*

**REST Request**::

    GET /module/gui/actions/update

**REST Response**::

    No Response

**Errors**:

* 404 No Update Available


Shutdown the Application and its child processes. During shutdown all active
connections are disconnected in their required order. The backend will wait
for 1 second (see shutdown_action_controller.py) before it will close its
ioloop.


**REST Request**::

    POST /module/gui/actions/shutdown

**REST Response**::

    No Response

**Errors**:

* 403 Shutdown already in progress.

Log
~~~

The "log" request is used to fetch the complete or a filtered list of the logs
accessible to the backend.

**REST Request**::

    GET /module/gui/logs[?connection_id=uuid]

**REST Response**::

    [
        {
            "index": "number-unique",
            "date": "DATETYPE",
            "level": "string: debug|info|warning|error|critical",
            "origin": "string",
            "message": "unsafe-string",
            "extra": "account and connection id",
            "formated_message": "message with code location",
            "code_location": "file and line"
        }
    ]

The "log" request allows to filter the log by adding an optional
connection_id.

The "log" request allows to push log entries from the GUI to the backend where
the log-entry will be stored and can be processed and filtered.

**REST Request**::

    POST /module/gui/logs
        {
            "index": "number-unique",
            "date": "DATETYPE",
            "level": "string: debug|info|warning|error|critical",
            "origin": "string",
            "message": "unsafe-string",
            "extra": "account and connection id",
            "formated_message": "message with code location",
            "code_location": "file and line"
        }

**REST Response**::

    None

**Errors**::

* 400: Validation Error, Value Error (bad JSON)

To delete all log entries, the GUI can emit a DELETE request. This clears
all stored log entries in the backend.

**REST Request**::

    DELETE /module/gui/logs

**REST Response**::

    None


Accounts
~~~~~~~~

Accounts can be used in the gui module to act on previously configured
preferences.

Check account
#############

Check an account configuration without persisting. A connection will be setup
and the gui-model will update the check_connection instance.

**REST Request**::

    POST /module/gui/accounts/check
    {
        "type": "OpenVPN | SSH | Tor | Tinc | Wireguard",
        "configuration": "user-string",
    }

**REST Response**::

    Empty on Success

**Errors**::

* 400: Validation Error, Value Error (bad JSON)

To cancel an account-check the DELETE method is used on the same endpoint. The
check connection will be disconnected and removed from the privileged process
and the associated log messages will be cleared.


**REST Request**::

    DELETE /module/gui/accounts/check

**REST Response**::

    Empty on Success

**Errors**::

* 404: No check was active

Connect group
#############

Connect all accounts for the given group in a predefined order. The order is
evaluated and then executed. The backend will signal changes to the state of
the connections/accounts using the events interface.

**REST Request**::

    POST /module/gui/groups/<group_id>/connect
    {
    }

**REST Response**::

    Empty on Success

**Errors**:

    * 400 ValidationError
    * 404 NotFoundError group_id not found


Disconnect group
################

Disconnect all accounts for the given group in a predefined order. The order
is evaluated and then executed. The backend will signal changes to the state
of the connections/accounts using the events interface.

**REST Request**::

    POST /module/gui/groups/<group_id>/disconnect
    {
    }

**REST Response**::

    Empty on Success

**Errors**:

    * 400 ValidationError
    * 404 NotFoundError group_id not found

Connect
#######

Connect specific account. Accounts that the given account depends on will be
connected before the selected account. The state change of the
connection/account will be signaled using the events interface.

**REST Request**::

    POST /module/gui/accounts/<account_id>/connect

**REST Response**::

    Empty on Success

**Errors**:

* 400 ValidationError
* 403 AlreadyConnectedError
* 404 NotFoundError account_id not found

Disconnect
##########

Disconnect specific account. Accounts that depend on the given account will be
disconnected before disconnecting. *TBD*

**REST Request**::

    POST /module/gui/accounts/<account_id>/disconnect

**REST Response**::

    Empty on Success

**Errors**:

* 400 ValidationError
* 403 NotConnectedError
* 404 NotFoundError account_id not found

Reconnect
#########

Reconnect specific Account. The OpenVPN instance will be requested to reconnect
the connection.

**REST Request**::

    POST /module/gui/accounts/<account_id>/reconnect

**REST Response**::

    Empty on Success

**Errors**:

* 400 ValidationError
* 403 NotConnectedError
* 404 NotFoundError account_id not found

Reset
#####

Reset specific account. On connection failure, the account still has a
connection that contains the error reasons. To reset those reasons and remove
the connection instance reset is used.

**REST Request**::

    POST /module/gui/accounts/<account_id>/reset

**REST Response**::

    Empty on Success

**Errors**:

* 400 ValidationError, ValueError
* 403 ConnectedError
* 404 NotFoundError account_id not found

Credential
##########

When the backend requested a credential, this endpoint is used to transport the
users data. A POST request with a credential JSON is used to respond, a DELETE
request signals a user-cancelation that leads to the cancelation of the process
that requested the credential.

**REST Request**::

    POST /module/gui/credential
    {
        "account_id": "account-id-uuid",
        "username": "user-provided-string-value",
        "password": "user-provided-string-value",
        "store_password": "never|session|keystore|local"
    }

**REST Response**::

    Empty on Success

**Errors**:

* 400 ValidationError
* 404 NotFoundError account_id not found

When the backend requests a credential, the user might decide not to enter the
information. With the DELETE request the backend will be informed that the
current process (eg connect list) has to be canceled.

**REST Request**::

    DELETE /module/gui/credential

**REST Response**::

    Empty on Success
