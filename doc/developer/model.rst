.. _model:

Model
=====

.. graphviz::

    digraph G {
      subgraph cluster_persistent {
          style=filled;
          color=lightgrey;
          label="persistent";
          Account[shape="box",label="Account\n+id\n+name\n+type\n+configuration\n+import_configuration\n+enabled\n+autostart"];
          GroupedAccount[shape="box",label="Grouped Account\n+group_id\n+type\n+name\n+weight\n+id"];
          Group[shape="box",label="Group\n+parent\n+collapsed\n+name\n+weight\n+id"];
          PreferencesP[shape="box",label="Preferences\n+autostart\n+autoconnect\n+save_connection_state\n+ui_language\n+account_hierarchy"];
          Override[shape="box",label="Override\n+enabled\n+[{name,type}]"];
      }
      subgraph cluster_transient {
          style=filled;
          color=lightgrey;
          label="transient";
          Event[shape="box",label="Event\n+index\n+date\n+origin\n+type\n+name\n+data"];
          LogItem[shape="box",label="Log Item\n+index\n+level\n+origin\n+message\n+formatted_message\n+code_location\n+extra"];
          LogItemExtra[shape="box",label="Log Item Extra\n+connection_id\n+account_id"];
          Connection[shape="box",label="Connection\n+id\n+account_id\n+type\n+connected\n+connecting\n+connect_failed\n+status\n+errors"];
          Credential[shape="box",label="Credential\n+username\n+password"];
          StatusItem[shape="box",label="Status Item\n+message\n+state"];
          StatusItemList[shape="box",label="Status Item List"];
          Log[shape="box",label="Log\n+logitems"];
          Backend[shape="box",label="Backend\n+network_active\n+privileged_active\n+started_on\n+log"];
          Gui[shape="box",label="GUI\n+connections\n+log_index\n+check_connection\n+events\n+credentials"];
          PreferencesT[shape="box",label="Preferences not Stored\n+credentials"];
      }

      PreferencesP -> GroupedAccount[label="loads"];
      PreferencesP -> Group[label="loads"];
      PreferencesP -> Account[label="loads"];
      GroupedAccount -> Account[label="references"];
      GroupedAccount -> Group[label="references"];

      PreferencesT -> Credential[label="holds"];
      Connection -> Account[label="references"]
      Connection -> Credential[label="load"]
      Connection -> StatusItemList[label="owns\nstatus and errors"];
      StatusItemList -> StatusItem;

      Log -> LogItem[label="owns"];
      LogItem -> LogItemExtra[label="owns"];

      Backend -> Connection[label="owns\ntransient"];
      Backend -> Log[label="owns\ntransient"];
      Backend -> Event[label="owns\ntransient"];


      Backend -> Preferences[label="owns\nloads"];
      Backend -> Gui[label="owns\ntransient"]
      Gui -> Connection[label="references"];
    }


Account
-------

Accounts contain the information how to create a connection. They are part of
the preferences module. Accounts may be referenced using their id-field. Only
the GroupedAccount loader will try to match accounts by name when they are not
yet associated to a group. Accounts, Groups, GroupedAccounts are stored on
disk every time the user changes their values.

.. graphviz::

    digraph G {
      subgraph cluster_persistent {
          style=filled;
          color=lightgrey;
          label="Account";
          Account[shape="box",label="Account\n+id\n+name\n+type\n++configuration\n+import_configuration\n+enabled"];
          GroupedAccount[shape="box",label="Grouped Account\n+group_id\n+type\n+name\n+weight\n+id"];
          Group[shape="box",label="Group\n+parent\n+collapsed\n+name\n+weight\n+id"];
          Preferences[shape="box",label="Preferences\n+backend{}\n+ui{}"];
      }

      Preferences -> GroupedAccount[label="loads"];
      Preferences -> Group[label="loads"];
      Preferences -> Account[label="loads"];
      GroupedAccount -> Account[label="references"];
      GroupedAccount -> Group[label="references"];

    }

Connection
----------

Connections are the application information that tracks aggregated information
about the state. Connections are updated by log messages and can be used to
filter logs. Connections are not stored on disk. Connections have to reference
a account that was used to setup the connection.

.. graphviz::

    digraph G {
      subgraph cluster_transient {
          style=filled;
          color=lightgrey;
          label="Connection";
          Connection[shape="box",label="Connection\n+id\n+account_id\n+type\n+connected\n+connecting\n+connect_failed\n+status\n+errors"];
          StatusItem[shape="box",label="Status Item\n+message\n+state"];
          StatusItemList[shape="box",label="Status Item List"];
      }

      Connection -> Account[label="references"]
      Connection -> StatusItemList[label="owns status"];
      Connection -> StatusItemList[label="owns errors"];
      StatusItemList -> StatusItem;
    }

Credential
----------

Credentials are username password combinations that are stored for a account.
The storage can be controlled to Never (so they can only be used once by the
application and have to be rerequested every time) Session (so the application
does not rerequest once they have worked) or OS Keystore (persist them in
the keystore of the OS). Credentials are identified by the account-id.
Only some connections need credentials, therefore the specific connection
implementation will inherit the connection.credential dispatcher if required.

.. graphviz::

    digraph G {
      subgraph cluster_transient {
          style=filled;
          color=lightgrey;
          label="Transient";
          Credential[shape="box",label="Credential\n+username\n+password"];
          Preferences[shape="box",label="Preferences"];
      }
      subgraph cluster_persistent {
          style=filled;
          color=lightgrey;
          label="Persistent";
          Account[shape="box",label="Account\n+id\n...."];
      }

      Credential -> Account[label="references by id"];
      Connection -> Credential[label="loads"];
      Preferences -> Credential[label="holds"];
    }

Override
--------

Overrides are persistent overloads that force certain keywords to the
configuration. Overrides can be enabled and disabled and are identified by
type and name.