Marshaling
==========

The REST interface requires the processes to communicate using JSON and pass
more or less complex information. To generalize the method how those information
is described a set of classes is implemented that all models need to inherit
from. The marshaling base class implements methods to serialize and un-serialize
JSON strings with validation and cleaning. There are two different types that
are supported: Fields and Lists. Fields describe any basic types like integer,
strings, boolean and allow the developer to specify a default value, if the
field is required and multiple validators and cleaners. A List allows the
developer to add multiple items of the same type and get them serialized as JSON
array. A marshalable is also a field, so it is allowed to define nested
structures. Marshalable defines the functions from_json and to_json that are
used by controllers to create response messages. In addition a marshalable
implements a commit function that can be used to signal other functions that the
model has changed and can be evaluated.

A simple model can be defined like this:::

    class credential_item(marshalable):
        def __init__(self):
            marshalable.__init__(self)
            self.username = field()
            self.password = field()

This definition allows the user of the model to set and get values:::

    def model_creator():
        credential = credential_item()
        credential.username.set('john_doe')
        credential.password.set('pa55w0rd')
        credential.commit()

The credential instance can now be passed through the application and a consumer
might access its values using:::

    def model_user(credential_model_instance):
        username = credential_model_instance.username.get()
        password = credential_model_instance.password.get()

All this could be easily done using a python-dictionary and would be even easier
to write. So lets add a requirement to the username and the password:::

    class string_min_length_validator(validator):

        def __init__(self, min_length):
            validator(self)
            self.min_length = min_length

        def is_valid(self, value):
            if not isinstance(value, (basestring)):
                return False
            if len(value) < 8:
                return False
            return True


    class email_validator(validator):

        def __init__(self, min_length):
            validator(self)
            self.min_length = min_length

        def is_valid(self, value):
            if not isinstance(value, (basestring)):
                return False
            if '@' not in value:
                return False
            return True


    class not_too_simple_password_validator(validator):

        def __init__(self, min_length):
            validator(self)
            self.min_length = min_length

        def is_valid(self, value):
            if not isinstance(value, (basestring)):
                return False
            if 'qwertz' in value:
                return False
            return True


    class credential_item(marshalable):

        def __init__(self):
            marshalable.__init__(self)
            self.username = field(
                validators=[email_validator()])
            self.password = field(
                validators=[
                    string_min_length_validator(8),
                    not_too_simple_password_validator()])


Using this definitions the developer ensures, that only values that are valid
are stored in the model using set() and only values that are considered valid
can be extracted using get() all other values will throw a ValidationError.

When using the JSON serialization and un-serialization functions to_json and
from_json this rules also apply:::
    
    def signup_action(request):
        credential = credential_item()
        credential.from_json(request.body)
        db.create_user(credential.username.get(), credential.password.get())

now the valid POST will simply continue in the control-flow:::

    POST /signup
    {
       "username": "john@doe.com",
       "password": "asdfghij"
    }

while an invalid POST will raise an exception when the json is parsed:::

    POST /signup
    {
        "username": "fred",
        "password": "qwertz"
    }

Raises ValidationError('username is not valid') that needs to be caught in the
handler function and converted to a appropriate response code.
