.. _running:

Running
=======

This document covers how to launch Netsplice.

Launching Netsplice
-------------------

After a successful installation, there should be a launcher called `netsplice`
somewhere in the PATH::

  % netsplice

The first time the application is launched, it will show the first run wizard
that will guide through the essential settings.

Commandline Options
-------------------

To see all the available command line options::

  % netsplice --help
  % man netsplice

See Also the :ref:`Netsplice Manual Page<man-netsplice>`
