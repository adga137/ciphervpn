.. _introduction:

Introduction
============

Netsplice
--------------

.. include:: general.rst
  :start-after: begin-general
  :end-before: end-general

Use Cases
~~~~~~~~~

There are several scenarios where it is essential to use a VPN connection.
Most often the goal is to increase the security when using the Internet in general.

* Securing remote maintenance operations
* Securing data transmissions between a network of globally
  distributed parties
* Connecting Teleworkers to the corporate Network
* Make sure that the geographic detection systems used by certain tracking
  services can't see your real location.
* Appear in multiple locations at the same time
* Hide the origin of requests

Users
~~~~~

Netsplice aims to satisfy users with multiple levels of experience. As VPN is a
technical concept, not all users will understand what its implications are.

Level 1
^^^^^^^

No experience in VPN usage at all. The installation and configuration was done
by a friend (Level 2) and only one of the VPN use-cases is expected. The
software is navigated using the mouse. When the user starts the application the
desired effect is expected and its progress and needs 'translated' progress
information instead of technical messages. The major events (connecting, up,
down) of the VPN need to be communicated as toast/popup messages over any other
software (eg games or movies). The user will not be aware of the possible
negative effects when the VPN is not up and cannot see the difference to clear.
When something goes wrong and the user needs support, the software needs to
compile a high-level summary and a detailed attachment and preferably send it
without additional tools to the VPN provider.

Level 2
^^^^^^^

Experience using VPN as business / university. The installation is done by the
user and one of the VPN use-cases is expected. To navigate the software mouse
and basic keyboard shortcuts (copy&paste) are used. The user will start the
software for the first time to configure one account. All future starts of the
software are expected to execute that configuration. The major events
(connecting, up, down) of the VPN need to be communicated as toast/popup
messages. The user is aware of the possible negative effects when the VPN is not
up but cannot reliably find out when that happens. When something goes wrong and
the user needs support, the software needs to compile a high-level summary and a
detailed attachment and preferably send it without additional tools to the vpn
provider.

Level 3
^^^^^^^

Experience using VPN for various purposes. The installation is done by the user
and one or more VPN use-cases are expected. To navigate the sofware mouse and
keyboard shortcuts are used. The user will start the software for the first time
to configure one or more accounts. All future starts of the software allow the
user to decide which account to connect. The major events (connecting, up, down)
of the VPN need to be communicated as toast/popup messages. The user is aware of
the possible negative effects when the VPN is not up and can configure methods
to handle this events. When something goes wrong the user will investigate log
messages and try to fix the configuration. when the user still needs support,
the software needs to compile a high-level summary and a detailed attachment
with the relevant information that can be edited and send with third party
software.

Level 4
^^^^^^^

Experience using VPN for various purposes running them from various devices like
the router. The installation is done by the user and one or more VPN use-cases
are expected. During the installation the software components and certificates
are distributed to different machines. The software is used to monitor the
network state for more or less complicated setups. The setups are configured and
validated by the user using third party tools. When something goes wrong the
user will investigate log messages and fix the configuration or file a
bug-report for the faulty component.

Features
========

Netsplice allows to easily secure network connections. It allows to configure
multiple Accounts for different connection types. The integrated logviewer
allows to debug issues and report to support. Several plugins like kill switch,
ping or updates extend the core functionality. The platform independent code
base allows to use the same interface and configuration on the available
desktop-platforms.


Accounts
--------

.. include:: ../features/accounts.rst
  :start-after: begin-summary
  :end-before: end-summary

:ref:`Read More<features-accounts>`

Connection Status
-----------------

.. include:: ../features/connection-status.rst
  :start-after: begin-summary
  :end-before: end-summary

:ref:`Read More<features-connection-status>`

Systray
-------

.. include:: ../features/systemtray.rst
  :start-after: begin-summary
  :end-before: end-summary

:ref:`Read More<features-systemtray>`

Log Viewer
----------

.. include:: ../features/logviewer.rst
  :start-after: begin-summary
  :end-before: end-summary

:ref:`Read More<features-logviewer>`

Preferences
-----------

.. include:: ../features/preferences.rst
  :start-after: begin-summary
  :end-before: end-summary

:ref:`Read More<features-preferences>`

Plugins
-------

.. include:: ../features/plugins.rst
  :start-after: begin-summary
  :end-before: end-summary

:ref:`Read More<features-plugins>`
