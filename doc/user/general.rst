General Snippets
================

.. begin-general

**Netsplice** is a multiplatform desktop client for VPN services. It is
written in python using `Logbook`_, `OpenSSL`_, `OpenVPN`_, `PySide`_ (`Qt`_) and
`Tornadoweb`_ :ref:`licensed under the GPL3 <gpl3>`.

.. _`Logbook`: http://logbook.readthedocs.io/en/stable/index.html
.. _`OpenSSL`: https://openssl.org
.. _`OpenVPN`: https://openvpn.net
.. _`PySide`: http://qt-project.org/wiki/PySide
.. _`Qt`: https://qt.io
.. _`Tornadoweb`: https://tornadoweb.org


.. end-general
