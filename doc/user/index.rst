User Guide
----------

.. toctree::
  :maxdepth: 1

  intro
  install
  running

.. toctree::
  :hidden:

  general
