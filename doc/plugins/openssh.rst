.. _`plugins-openssh`:

OpenSSH
=======

.. begin-summary

OpenSSH is installed in most unix-like operatingsystems and can be used to
setup a tunnel or a socks proxy.

.. end-summary

.. image:: /images/plugins-openssh.png

Configuration
-------------

Similar to the :ref:`OpenVPN Plugin<plugins-openvpn>` the OpenSSH plugin is
configured by a key-value list or a plaintext editor. The configuration is
stored for the account and highlights changes made from the default
configuration.

The key-value list takes the possible parameters to the ``ssh`` binary and
supports two additional keys: ``host`` and ``command``.

Connection
----------

OpenSSH has integrated mechanisms to prompt during connection. The simplest
way to avoid that prompts during connect is to start netsplice in a shell
with the ssh-agent activated and the key to the remote host added to that
agent.
The implementation does not support accepting changed host-keys. The connection
will fail in this case and requires you to add the host key manually by
connecting from the commandline.

Privileges
----------

The ``ssh`` executable is executed without extra privileges.

Plugin State
------------

The OpenSSH plugin is in a early development state and may have issues on
systems with locales other than ``en_US``. It does not work in windows because
ssh is not available in the operating system.
