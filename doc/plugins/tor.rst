.. _`plugins-tor`:

Tor
===

.. begin-summary

Tor is free software for enabling anonymous
communication. The name is derived from an acronym for the original software
project name "The Onion Router".

The plugin supports setting up multiple Tor instances with different
options. You need to configure your client (browser) to use the socks proxy
that is created when connected.

.. end-summary

.. image:: /images/plugins-tor.png

Configuration
-------------

Similar to the :ref:`OpenVPN Plugin<plugins-openvpn>` the Tor plugin is
configured by a key-value list or a plaintext editor. The configuration is
stored for the account and highlights changes made from the default
configuration.

The key-value list takes the possible parameters to the ``tor`` binary.

With the :ref:`Process Manager Plugin<plugins-process-manager>` a browser
with a profile can be launched ``/usr/bin/firefox-bin --profile <path>`` and
this profile may use the the configured SOCKSPort.

The documentation on how to secure the browser in order to avoid leaks is
provided by the tor-project and should be followed.

Connection
----------

The tor process uses a ``DataDirectory`` to store keys and certificates. To run
multiple instances, the ``DataDirectory`` and the ``SOCKSPort`` needs to be
different for each instance.

Privileges
----------

The ``tor`` executable is executed without extra privileges.

Plugin State
------------

The Tor plugin is in a early development state and may have issues. It does work
on all operatingsystems that have the ``tor`` executable in the path.
