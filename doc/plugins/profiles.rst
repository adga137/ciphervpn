.. _`plugins-profiles`:

Profiles
========

.. begin-summary

Profiles are presets for the available connection types. They provide a
description for the user-interface and a default configuration that the user
can start from.

.. end-summary

Configuration
-------------

Like the import feature of OpenVPN, profiles only create presets. They may work
out of the box (OpenVPN provider profiles with username and password) or need
customization by the user (ssh reverse proxy).
To add, deactivate or activate profiles they need to be available in the
source-tree (src/netsplice/plugins) and configured in the plugin-config
(src/netsplice/config/plugins.py).

Privileges
----------

The profile plugins do not need any backend privileges. They are gui-plugins.

Plugin State
------------

The OpenVPN profile plugins are provided by IPRedator.se and reflect the current
online guides and configurations.
Tor and SSH profiles are provided for educational purposes and are not reviewed
toughly.

New Profiles
------------

To add new profiles, copy a existing profile and modify its contents. If the
new profile should be part of a future release, please contact us.
