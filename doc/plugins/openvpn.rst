.. _`plugins-openvpn`:

OpenVPN
=======

.. begin-summary

OpenVPN is an open-source software application that implements
virtual private network techniques for creating secure point-to-point or
site-to-site connections in routed or bridged configurations and remote
access facilities.

Presets simplify the process of connecting to a provider. In the advanced
tab it is possible to import existing configurations and in the plugin tab
connection timeouts and executable versions can be configured.

OpenVPN executables are shipped with the application.

.. end-summary


Configuration
-------------

OpenVPN plugin is configured by a key-value list or a plaintext editor. The
configuration is stored for the account and highlights changes made from the
default configuration.

The minimum configuration is a ``remote host port`` line, all other settings
are provider dependent.

Insert new lines py pressing the ``Insert`` Button and fill the key with a
option (see openvpn --help) with the value. Options that are part of the
shipped openvpn executables are identified with a Type and give a help tooltip.

The Plaintext Editor is the more advanced configuration. It allows pasting
existing configurations and modify them. The options that are part of the
shipped openvpn executables are highlighted and give a help tooltip.

When the Plaintext Editor is activated, a ``Import Editor`` button is available.
This allows to modify the values that are considered 'default' for this
configuration. When the values are missing or changed in the Plaintext, they are
highlighted in the key-value-table.

The Advanced Editor and the Plaintext Editor operate on the same data and update
when the 'other' editor changed the configuration.

Also read the :ref:`Account Config<features-account-config-openvpn>` for OpenVPN.

.. image:: /images/plugins-openvpn-advanced.png

Account Options
---------------

Each account can have options for the openvpn process. Those options are a
timeout that allows connecting to 'slow' remote systems and the openvpn version
to be used. The nice-value of the openvpn process may also be configured.

.. image:: /images/plugins-openvpn.png

Import File
-----------

It is possible to import a ovpn/conf file that has been provided by your
service-provider. The import function sets the contents of the file to the
``imported`` configuration and the ``current`` configuration. Files with
embedded ``ca``, ``cert``, ``key`` or ``tls-auth`` work out of the box, Files
that reference relative external files usually cause trouble, because the
connection is made with a ``/tmp/random.conf`` and the key-files are still on
the desktop. Convert the keys and certificates to PEM/base64 and embed them in
the configuration then.

Connection
----------

The configuration file is analyzed if it contains the ``auth-user-pass`` option
and prompts for the username/password. If it contains the ``askpass`` option
only a password for the private key is requested.
Some options are not allowed because they are a security issue or would cause
Netsplice to behave strange. Those include
``chroot``, ``daemon``, ``down``, ``inetd``, ``ipchange``, ``route-pre-down``,
``route-up``, ``tls-verify`` and ``up``. The up/down scripts can be simulated
with the :ref:`Process Manager Plugin<plugins-process-manager>`.

Privileges
----------

The OpenVPN executable is executed with root privileges because it needs to
setup network interfaces and routes.

Some operations (--version, --help) are executed in the unprivileged component.

Plugin State
------------

The OpenVPN plugin is a key component of netsplice and intensively tested with
the IPredator.se configurations and with several configuration with embedded
keys. It works on all platforms and ships with executables linked to openssl and
libressl.
