.. _`plugins-nmap`:

Network Port Scan
=================

.. begin-summary

Find ports in closed (eg Company, University) networks that allow outbound VPN
connections. This plugin uses the :ref:`Statistics Plugin<plugins-statistics>`

.. end-summary
