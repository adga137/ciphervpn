.. _`plugins-process-sniper`:

Process Sniper
==============

.. begin-summary

Send a signal (eg Kill) to all processes the unprivleged component can access.
It is possible to define the signal that is sent and to choose from the
commands that were started from the
:ref:`Process Launcher<plugins-process-launcher>`.

.. end-summary

VPNs are used to secure activity that a user would not execute in the open
internet.  An IRC chat client will hold as example here.  With the VPN the
chat client connects through a tunnel and the IRC server sees the
VPN providers IP connecting.  When the VPN connection is shut down by the
user or by the provider the chat client tries to reconnect.  As the VPN
connection is now down, the chat client will expose the ISP-IP to the
IRC server (and notifying all users of that IRC server about the new
IP).  In order to avoid this exposure the user has to shut down the IRC chat
client when the VPN connection goes down.  This can be done by killing or
suspending the IRC chat client.

The Process Sniper automates this behavior: It registers for the events on
certain connection profiles and signals the application when a event occurs. The
user can configure the name of the application with a signal to be sent when a
VPN connection is disconnected or the application shuts down.

.. image:: /images/plugins-process-sniper.png

Edit Connection Profile
-----------------------

Edit a Connection Profile reveals the sniping rules. Right-click on a
(disconnected) Connection and select the 'Plugin: Process Manager' tab. The
lower-section of the manager is used to define applications that are killed.

The rules may be ``active``. Active rules are executed when the connection state
changes, inactive rules are not.


Insert
------

Insert a rule for the sniper. Each Rule is associated with a connection event
(connected, disconnected, connecting) a signal (eg SIGINT or SIGSTOP) and a
pattern that is matched. Instead of the pattern the rule may reference a ::ref::`Process Launcher<plugins-process_launcher>` id.

With a click on the ``Insert`` button, the rule dialog is displayed with
defaults for a new rule.

The default view is the ``Process List`` that allows the user to define a
regular expression for processes. To kill a process that was launched from the
:ref:`Process Launcher<plugins-process-launcher>`, select the source to
``Process Launcher`` and see the rules that are defined for earlier events.

New rules are active by default and act on the ``disconnected`` event.


Edit
----

The sniper is able to process multiple rules for a connection. Each rule can be
edited using a double click or by pressing the 'Edit' button in the toolbar. A
new window appears that allows modifying the rule by choosing from combo boxes
and select the process from the list of currently running processes or the list
of process launcher items.

To update the available processes click on the 'Update Process' button. Only
processes that are owned by the user that is running netsplice will be displayed
as a regular expression that can be modified. Click a line to use it as filter
and edit the rule to your liking.

On systems with many processes for a user the Filter input may help finding the
commandline.

When Update is pressed, the rule is displayed in the Connection settings and
with confirming the Connection settings the rule is active when the connection
state changes.

The launcher rules that are available for selection are filtered by multiple criteria.
They never contain elevated launchers and only launchers that are possible in the
causality chain ```connecting``` -> ```connected``` -> ```disconnected```.

.. image:: /images/plugins-process-sniper-edit.png

Remove
------

Removes the selected Rule.

Test
----

Allows to test the rule by displaying the process(es) that will be matched. The
signal is not sent to the application.

.. image:: /images/plugins-process-sniper-test.png

To ensure that the sniper will have the desired effect on the selected
application, start the process that should receive the signal and connect or
disconnect the connection with the configured rule.

Check that the process receives and handles the signal before relying on the
sniper's accuracy. Note that sometimes with the update of tools, the rule may no
longer match after the update. This can be avoided by choosing a more general
expression to match but requires more knowledge on regular expressions.

Launcher Limitations
--------------------

When the rule is associated using the ``Process Launcher`` source, some
limitations apply for the availability of the Launcher Rules:

- Depending on the selected event, the Launcher Rules are filtered:

    - Connected: Only rules that act on ``Connecting`` are available.
    - Connecting: Only rules that act on ``Disconnected`` or ``Connected`` are
      available.
    - Disconnected: Only rules that act on ``Connected`` or ``Connecting`` are
      available.
- Rules in the ``Process Launcher`` that are ``elevated`` cannot be killed by
  the Process Sniper.

For killing applications that have been started by the Process Launcher a
system limitation exists. For now this document notes this even if there are
possible solutions.

In the situation that ``/usr/bin/firefox-bin`` is started with
``--profile <path>``, there is the possibility that the ``firefox-bin`` is only
a wrapper for ``/opt/firefox/firefox``. As a security measure the sniper will
only kill applications it created. Therefore it checks the commandline for the
pid that was stored when firefox was launched and sees that this is not the same
command.

This measure is required because the operatingsystem 'recycles' process ids.
When the user connects the VPN and firefox is started with the pid 1000. Now the
user closes this firefox, but keeps the VPN open. Some other process now may be
using the pid 1000. For now, the kill is averted when the other process has a
different argument list (eg [] vs ["--profile <path>"]). Without parameters
any process (eg the wordprocessor) would receive the signal.

This problem occurs for wrappers that do not use ``exec`` like ``firefox-bin``.
Wrappers that launch new processes with new pid's will make it impossible to
kill the correct application for the sniper. The result will be that the
application keeps running even if the connection state changed.

Make sure to test the behavior for the desired application and use the regular
expression instead of the launcher id in 'complicated' cases.

Events
------

The events that are supported by the process sniper are

account_connecting
~~~~~~~~~~~~~~~~~~

This event happens before the connection is established or when it reconnects
from a ping-timeout (eg suspended machine) or network connectivity issue (wifi
network change).

Use this event to ensure that applications cannot leak information while routes
or default gateways are not setup yet. For example SIGINT all chat clients to
avoid that they communicate with a unwanted network.

account_connected
~~~~~~~~~~~~~~~~~

This event happens when the connection has been established.

Use this event to ensure that applications do not leak information into a
specific network. For example SIGINT a gaming identity when the connection to a
work network has been established.

account_disconnected
~~~~~~~~~~~~~~~~~~~~

This event happens when the connection is shut down by the user or by system
events (eg openvpn executable was killed). When Netsplice is shutdown, all
active connections will receive a disconnect.

Use this event to ensure that applications only communicate while a connection
is up and with networks that ensure that during reconnect no data is leaked.
