.. _`plugins-process-manager`:

Process Manager
===============

.. begin-summary

OpenVPN has a up/down feature that is disabled in Netsplice because of security
concerns. This feature is missing in Tor and OpenSSH and may not be available
in other tunnel applications. Therefore the Process Manager Plugin implements
this feature to make it simple and secure for the user to start and stop
applications depending on a accounts connection state.

The Process Manager combines the Process Launcher and the Process Sniper
functionality and connects both plugins. It presents the properties on a single
page in the account_editor and allows the process_sniper to access the items
of the process_launcher.

.. end-summary

.. image:: /images/plugins-process-manager.png

Process Launcher
----------------

.. include:: process-launcher.rst
  :start-after: begin-summary
  :end-before: end-summary

:ref:`Read More<plugins-process-launcher>`.

Process Sniper
--------------

.. include:: process-sniper.rst
  :start-after: begin-summary
  :end-before: end-summary

:ref:`Read More<plugins-process-sniper>`.
