.. _Plugins-Index:

Plugins
-------

.. begin summary

Netsplice allows activating various plugins. Those extend the core
functionality.

.. end-summary

.. toctree::
  :titlesonly:

  openvpn
  process-launcher
  process-manager
  process-sniper
  openssh
  tor
  profiles
  ping
  update
  config-quality-check
  Statistics (not implemented) <statistics>
  Portscan/NMap/IPerf (not implemented) <portscan>
  api

