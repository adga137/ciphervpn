.. _`plugins-ping`:

Ping
====

.. begin-summary

Some services disconnect the client when a inactivity is detected. While this
may safe some resources for the service it is usually a inconvenience for the
user. To avoid the disconnect there are several hacks that request a remote
resource continuously only to prevent the inactivity detection. The simplest is
so ping well known IP-addresses and hook that into the up-scripts of the
process that establishes the connection.

The Ping Plugin automates the start and stop of a regular execution of a ICMP
ping and minimizes the impact on the network.

.. end-summary

The send of ICMP packets requires the ping process to have elevated rights.
Most operating systems allow users to execute the ping program by setting its
rights using setuid. Netsplice relies that the ping program can be found in one
of the directories specified by the PATH environment variable. When you can
start ping from the commandline everything is fine.


Configuration
-------------

The Account dialog has a *Ping* tab to change ping related options.


  * Period

      - Set a period for the ping process to be executed. The period is a
        number of seconds between 0 and 999. When it is set to 0, no ping will
        be executed.
  * Remote

      - With a IPv4 or IPv6 address a fixed remote may be selected. The dialog
        allows the user to specify a custom IP-address or select from a short
        list of common addresses.

      - With a ENV variable it is possible to ping anything that is pushed from
        the server during the connect process. This makes it possible to ping
        the pushed gateway and reduce the load of the internet or auto-select a
        DNS server - given that they are pushed as IP-addresses. The ENV
        variables differ between services (and sometimes even between connects)
        make sure that the selected value is available.

Workflow
--------

When a account has successfully connected, the Ping plugin receives the
connected or reconnected event and sets up a interval process in the network
backend. When the connection enters a disconnected state, the network backend
is requested to drop the active interval. The output of the ping program is
silently dropped, only the execution is counted. When ping could not be
executed, a error message in the log will indicate this.


UI
--

The Ping plugin is configured in the account configuration. The default is that
no remote is pinged when the connection is up.


Backend
-------

The backend maintains a list of pings that have been setup in the network
backend. This list is consulted on connection events to make sure that only one
ping interval is active for a connection and that the interval is stopped when
the connection goes down.


Network
-------

The network maintains a list of ping processes that it uses to schedule the
ping to multiple remotes at the given periods. The ping processes are
referenced with the connection_id.


To setup a ping the network backend is posted with a setup.

**REST Request**::

    POST /module/plugin/ping/
    {
        "connection_id": "uuid of a backend connection",
        "period": number_of_seconds_between_each_ping,
        "remote": "IPv4 or IPv6 address"
    }

**REST Response**::

    201 (no content) for success.

**Errors**::

* 400: Validation Error, Value Error (bad JSON)
* 403: A ping already exists for the connection

To get information about a active ping the network backend may be requested

**REST Request**::

    GET /module/plugin/ping/connections/${uuid_of_a_backend_connection}

**REST Response**::

    {
        "period": number_of_seconds_between_each_ping,
        "remote": "IPv4 or IPv6 address",
        "count": number_of_pings_sent
    }

**Errors**::

* 400: Validation Error, Value Error (bad JSON)
* 404: No process with connection_id exists.

**REST Request**::

    DELETE /module/plugin/ping/connections/${uuid_of_a_backend_connection}

**REST Response**::

    201 (no content)

**Errors**::

* 400: Validation Error, Value Error (bad JSON)
* 404: No process with connection_id exists.

