##################################
# Makefile for compiling resources
# files.
#
# TODO move to setup scripts
# and implement it in python
#
# http://die-offenbachs.homelinux.org:48888/hg/eric5/file/5072605ad4dd/compileUiFiles.py
###### EDIT ######################

NAMESPACE_SOURCE = src/netsplice
PYTHON_EXECUTABLE = $(shell which python)
PYSIDERCC=$(shell \
  which python | sed 's|/bin/python||')/lib/$(shell \
  readlink $$(which python))/site-packages/PySide/pyside-rcc

DEBVER = $(shell dpkg-parsechangelog | sed -ne 's,Version: ,,p')

ifndef EDITOR
	export EDITOR=vim
endif

ifndef RESOURCE_TIME
	export RESOURCE_TIME=10
endif

CURDIR = $(shell pwd)

# subdirs that define additional rules
MODULES = \
    $(NAMESPACE_SOURCE)/gui \
    $(NAMESPACE_SOURCE)/plugins

MODULE_DIRS = $(MODULES)

###########################################

.PHONY: all gui documentation clean \
	$(MODULE_DIRS) \
	do_cprofile view_cprofile \
	get_wheels gather_wheels gather_deps \
	version version-deb version-fedora version-nsh \
	run rund run_cgdb run_backend run_privileged \
	tests test_backend_api test_privileged_api test_backend_api_account_check \
	test_backend_account_check_api test tdd \
	codestat codetodo codeerrors

all : gui

UI_FILES=$(shell find . -type f | grep '\.ui$$')
RES_FILES=$(shell find . -type f | grep '\.qrc$$')

gui: .uic_files_built

pyuic.json: $(UI_FILES) $(RES_FILES)
	@echo $@
	rm pyuic.json 2> /dev/null || true
	pyuicfg -g --pyside

	find . -type f | grep ui$$ | grep '\./src/netsplice' \
	  | sed 's|^./src/netsplice/\(.*\)/views/\(.*\.ui\)|pyuicfg -a src/netsplice/\1/views/\2 src/netsplice/\1|' \
	  | bash > /dev/null
	find . -type f | grep qrc$$ | grep '\./res' \
	  | sed 's|^./res/\(.*\)$$|pyuicfg -a res/\1 src/netsplice/gui|' \
	  | bash > /dev/null
	find . -type f | grep qrc$$ | grep '\./src/netsplice/plugins' \
	  | sed 's|^./src/netsplice/plugins/\(.*\)/res/qtresources/\([^/]*\)\.qrc$$|pyuicfg -a src/netsplice/plugins/\1/res/qtresources/\2.qrc src/netsplice/plugins/\1/gui|' \
	  | bash
	sed -ibak 's|"pyrcc": ""|"pyrcc": "$(PYSIDERCC)"|' pyuic.json
	sed -ibak 's|"pyuic": ""|"pyuic": "pyside-uic"|' pyuic.json
	sed -ibak 's|"pyuic_options": "\-\-from-import"|"pyuic_options": "--from-imports"|' pyuic.json

	rm pyuic.jsonbak

.uic_files_built: pyuic.json
	${PYTHON_EXECUTABLE} setup.py build_ui > /dev/null
	touch .uic_files_built

clean :
	# remove all pyc files
	find $(NAMESPACE_SOURCE) -type f | grep .pyc\$$ | xargs -IX $(RM) X || true
	# remove any compiled resources
	find $(NAMESPACE_SOURCE) -type f | grep _ui.py | xargs -IX $(RM) X || true
	find $(NAMESPACE_SOURCE) -type f | grep _rc.py | xargs -IX $(RM) X || true
	rm -rf __pycache__ || true
	rm -rf Netsplice.egg-info || true
	rm .uic_files_built || true
	rm pyuic.json || true

%clean : %
	# run clean in all
	$(MAKE) -C $< clean

$(MODULE_DIRS):
	$(MAKE) -C $@ all

documentation:
	cd pkg/debian-build \
	  && docker-compose run --rm slavedocumentation


get_wheels:
	pip install --upgrade setuptools
	pip install --upgrade pip
	pip install wheel

gather_wheels:
	pip wheel --wheel-dir=var/tmp/wheelhouse

install_wheel:
	pip install --pre --use-wheel --no-index --find-links=var/tmp/wheelhouse -r pkg/requirements.pip

gather_deps:
	pipdeptree | pkg/scripts/filter-netsplice-deps

version:
	@PYTHONPATH=${PYTHONPATH}:$(CURDIR)/src/ \
		${PYTHON_EXECUTABLE} $(NAMESPACE_SOURCE)/VersionApp.py

version-deb:
	@PYTHONPATH=${PYTHONPATH}:$(CURDIR)/src/ \
		${PYTHON_EXECUTABLE} $(NAMESPACE_SOURCE)/VersionApp.py --deb

version-fedora:
	@PYTHONPATH=${PYTHONPATH}:$(CURDIR)/src/ \
		${PYTHON_EXECUTABLE} $(NAMESPACE_SOURCE)/VersionApp.py --fedora

version-nsh:
	@PYTHONPATH=${PYTHONPATH}:$(CURDIR)/src/ \
		${PYTHON_EXECUTABLE} $(NAMESPACE_SOURCE)/VersionApp.py --nsh

run_cgdb: gui
	PYTHONPATH=${PYTHONPATH}:$(CURDIR)/src/ \
		cgdb --args ${PYTHON_EXECUTABLE} $(NAMESPACE_SOURCE)/NetspliceGuiApp.py

run: gui
	PYTHONPATH=${PYTHONPATH}:$(CURDIR)/src/ \
		${PYTHON_EXECUTABLE} $(NAMESPACE_SOURCE)/NetspliceGuiApp.py \
			--port 4$(shell id -u)

rund: gui
	PYTHONPATH=${PYTHONPATH}:$(CURDIR)/src/ \
		${PYTHON_EXECUTABLE} $(NAMESPACE_SOURCE)/NetspliceGuiApp.py \
		--debug

run_backend:
	PYTHONPATH=${PYTHONPATH}:$(CURDIR)/src/ \
		${PYTHON_EXECUTABLE}  $(NAMESPACE_SOURCE)/NetspliceBackendApp.py \
			--host localhost \
			--port 5$(shell id -u) \
			--config_home /tmp/.config/
			--config_home /tmp/.config/

run_privileged:

	PYTHONPATH=${PYTHONPATH}:$(CURDIR)/src/ \
		${PYTHON_EXECUTABLE}  $(NAMESPACE_SOURCE)/NetsplicePrivilegedApp.py \
			--host localhost \
			--port 10002 \
			--config_home /tmp/.config

run_unprivileged:
	PYTHONPATH=${PYTHONPATH}:$(CURDIR)/src/ \
		${PYTHON_EXECUTABLE}  $(NAMESPACE_SOURCE)/NetspliceUnprivilegedApp.py \
			--host localhost \
			--port 10003 \
			--config_home /tmp/.config

run_network:
	PYTHONPATH=${PYTHONPATH}:$(CURDIR)/src/ \
		${PYTHON_EXECUTABLE}  $(NAMESPACE_SOURCE)/NetspliceNetApp.py \
			--host localhost \
			--port 10001 \
			--config_home /tmp/.config

test:
	nosetests --with-coverage --cover-package=netsplice --cover-html --cover-html-dir=/var/tmp/nose

tdd:
	echo 'set MODULE environment to limit tests'
	while [ true ]; do nosetests -d $(MODULE) && nosetests --with-coverage $(MODULE); read; done

do_cprofile:
	${PYTHON_EXECUTABLE} -m cProfile -o netsplice.cprofile src/netsplice/NetspliceApp.py --debug

view_cprofile:
	cprofilev netsplice.cprofile

test_backend_api:
	bash test_backend_api.sh

test_backend_api_account_check:
	# run with:
	# export CONFIG="{\"username\":\"uuuu\",\"password\":\"pppp\",\"configuration\":\"$(echo $(cat /tmp/randomname|grep -v '^#' | sed 's|"|\\"|g' | sed 's|^|SSS|;s|$|EEE|g') | sed 's|EEE SSS|\\n|g;s|EEE$||;s|^SSS||')\"}"
	# or
	# export CONFIG="{\"username\":\"uuuu\",\"password\":\"$(cat /tmp/randompassword)\",\"configuration\":\"$(echo $(cat /tmp/randomname|grep -v '^#' | sed 's|"|\\"|g' | sed 's|^|SSS|;s|$|EEE|g') | sed 's|EEE SSS|\\n|g;s|EEE$||;s|^SSS||')\"}"
	# make test_backend_api_account_check CONFIG="${CONFIG}"
	bash test_backend_account_check_api.sh

test_privileged_api:
	@echo 'this assumes that you disabled HTTPS and HMAC authentication'
	@echo 'and the backend is running on port 10002 on localhost'
	bash test_privileged_api.sh

codestat:
	@echo 'number of files'
	@find ./src -type f | grep .py\$$ | wc
	@echo 'lines of code (no comment, not empty, no indent)'
	@find ./src -type f | grep .py\$$ | xargs cat | grep -v '^ *#\|^\s*$$' | sed 's|^\s*||;s|[._]| |g;' \
	  | wc

codetodo:
	@echo ""
	@echo "TODO's with Context"
	@echo "-------------------"
	@echo ""
	@find ./src -type f | grep .py\$$ | xargs -IF echo "fgrep 'XXX ' -H -n -A 2 'F' || true" \
	  | bash \
	  | sed 's|\(.*[:][0-9]*\)\:\(.*\)|\nIn \1:::\nC\2|;s|\(.*[-][0-9]*\)-\(.*\)|C\2|' \
	  | sed 's|^C *|    |g'
	@echo ""
	@echo "This file is generated using the 'make codetodo > doc/codetodo.rst'"
	@echo "make-target and should be updated before releases."
	@find ./src -type f | grep .py\$$ | xargs -IF echo "fgrep 'XXX ' 'F' || true" \
	  | bash | wc | awk '{print "Total Number of XXX: ", $$1}'
	@find ./src -type f | grep .py\$$ | xargs -IF echo "fgrep 'TODO ' 'F' || true" \
	  | bash | wc | awk '{print "Total Number of TODO: ", $$1}'

codeerrors:
	@echo ""
	@echo "Error Codes with Context"
	@echo "------------------------"
	@echo ""
	@echo "This is a list of error codes with the file and the line before the set_error_code"
	@echo "is set in the code:"
	@echo ""
	@find ./src/netsplice -type f  \
	  | grep -v ./src/netsplice/util \
	  | grep .py\$$ \
	  | xargs -IF echo "fgrep 'set_error_code' -H -n -B 1 'F' || true" \
	  | bash \
	  | sed 's|[-:]    ||;s|^|    |;s|    .*set_error_code(\([0-9]*\).*|\n~~~~\n\1\n|g' \
	  | grep -v '^    --' \
	  | tac \
	  | sed 's|    \./\(.*\)-\([0-9]*\)    except \([^, ]*\).*|* Exception: \3\n* :file:`\1` Line \2\n|g'
	@echo ""
	@echo "Sorted Error Codes"
	@echo "------------------"
	@echo ""
	@echo "This is a list of error codes used in the application::"
	@echo ""
	@find ./src/netsplice -type f \
	  | grep -v ./src/netsplice/util \
	  | grep .py\$$ \
	  | xargs -IF echo "fgrep 'set_error_code' -H 'F' || true" \
	  | bash \
	  | sed 's|.*/\([^/]*/[^/]*/[^/]*\.py\):.*set_error_code(\([0-9]*\).*|    \2 \1|' \
	  | sort
	@echo ""
	@echo "Non-Unique Error Codes"
	@echo "----------------------"
	@echo ""
	@echo "This is a list of error codes over-used in the application::"
	@echo ""
	@find ./src/netsplice -type f \
	  | grep -v ./src/netsplice/util \
	  | grep .py\$$ \
	  | xargs -IF echo "fgrep 'set_error_code' -H 'F' || true" \
	  | bash \
	  | sed 's|.*/\([^/]*/[^/]*/[^/]*\.py\):.*set_error_code(\([0-9]*\).*|    \2|' \
	  | sort | uniq -d
	@echo "    end"
	@echo ""
	@echo "This file is generated using the 'make codeerrors > doc/errorcodes.rst'"
	@echo "make-target and should be updated before releases."

codedoc:
	@echo "Project Classes"
	@echo "==============="
	@echo ""
	@echo "Almost all classes that are found in the src/netsplice tree."
	@echo ""
	@# find all python files and remove those that have no relevant
	@# documentation embedded or that are no classes.
	@# generate autodoc rst with headlines and autoclass
	@find ./src/netsplice -type f \
	  | grep .py\$$ \
	  | grep -v __init__.py \
	  | grep -v test_ \
	  | grep -v _ui.py \
	  | grep -v _rc.py \
	  | grep -v _version.py \
	  | grep -v _appname.py \
	  | grep -v _none.py \
	  | grep -v config.py \
	  | grep -v endpoints.py \
	  | grep -v errors.py \
	  | grep -v factory.py \
	  | grep -v getppid.py \
	  | grep -v names.py \
	  | grep -v origins.py \
	  | grep -v replacement.py \
	  | grep -v ipc/route.py \
	  | grep -v ipc/status.py \
	  | grep -v util/files.py \
	  | grep -v util/path.py \
	  | grep -v util/commandline.py \
	  | grep -v /config \
	  | grep -v /messages.py \
	  | grep -v /state.py \
	  | grep -v /types.py \
	  | grep -v /network/ \
	  | grep -v App.py \
	  | sed 's|./src/||;s|/|.|g;' \
	  | sed 's|\(.*\.\)\([^\.]*\).py|\1\2\nUNDERLINE\1\2\n\n.. autoclass:: \1\2.\2|;' \
	  | sed 's|\(.. autoclass.*\)$$|\1\n  :members:\n  :undoc-members:\n|' \
	  | sed '/^UNDERLINE/ y|abcdefghijklmnopqrstuvwxyz._|----------------------------|;' \
	  | sed 's|^UNDERLINE||'
	@echo ""
	@echo "Project Modules"
	@echo "==============="
	@echo ""
	@echo "All Modules found in the src/netsplice tree."
	@echo ""
	@# find all python files and use only module __init__.py files
	@# generate autodoc rst with headlines and automodule
	@find ./src/netsplice -type f \
	  | grep __init__.py\$$ \
	  | grep -v test_ \
	  | sed 's|/__init__.py||' \
	  | sed 's|./src/||;s|/|.|g;' \
	  | sed 's|\(.*\.\)\([^\.]*\)|\1\2\nUNDERLINE\1\2\n\n.. automodule:: \1\2|;' \
	  | sed 's|\(.. automodule.*\)$$|\1\n  :members:\n  :undoc-members:\n|' \
	  | sed '/^UNDERLINE/ y|abcdefghijklmnopqrstuvwxyz._|----------------------------|;' \
	  | sed 's|^UNDERLINE||'
	@echo ""
	@echo "This file is generated using the 'make codedoc > doc/developer/autocode.rst'"
	@echo "make-target and should be updated before releases."
