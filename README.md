Cipher
=========

*VPN configuration made usable*


**Cipher** is a multiplatform desktop client for VPN services.

It is written in python using `PySide`_ and licensed under the GPL3.

.. _`PySide`: http://qt-project.org/wiki/PySide


Read the Docs!
------------------

The latest documentation is available at ?.


License
=======

Netsplice is released under the terms of the `GNU GPL version 3`_ or later.

.. _`GNU GPL version 3`: http://www.gnu.org/licenses/gpl.txt


Contributing
============

We welcome contributions! see  the `Contribution guidelines <CONTRIBUTING.rst>`_


Donate
============

you can donate:

- bitcoin wallet: 1GoxoTSkTHiVoBGtcoxW72tHc4qrVb9NEG
- litecoin wallet: LbWco8CTCT54Gky9eFt4Bjc1RjbmGMx1B9
- dogecoin wallet: DJc82ydXnfXcfsBa2DXX4dnEYh4fP3BrdF

