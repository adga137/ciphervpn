# -*- coding: utf-8 -*-
# connection_status.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for connection status.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.log_list import log_list as log_list_model
from netsplice.model.validator.connection_id import (
    connection_id as connection_id_validator
)
from netsplice.backend.unprivileged.model.validator.connection_type import (
    connection_type as connection_type_validator
)
from netsplice.model.validator.boolean import (
    boolean as boolean_validator
)


class connection_status(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.id = field(
            required=True,
            validators=[connection_id_validator()])

        self.active = field(
            default=False,
            required=True,
            validators=[boolean_validator()])

        self.type = field(
            required=True,
            validators=[connection_type_validator()])

        self.read_tap_bytes = field(required=False)

        self.write_tap_bytes = field(required=False)

        self.read_udp_bytes = field(required=False)

        self.write_udp_bytes = field(required=False)

        self.auth_read_bytes = field(required=False)

        self.pre_compress_bytes = field(required=False)

        self.post_compress_bytes = field(required=False)

        self.pre_decompress_bytes = field(required=False)

        self.post_decompress_bytes = field(required=False)
