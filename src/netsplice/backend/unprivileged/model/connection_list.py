# -*- coding: utf-8 -*-
# connection_list_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model.

Model for listing connections.
'''

from netsplice.util.errors import (ConnectedError, NotFoundError)
from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.backend.unprivileged.model.connection_list_item import (
    connection_list_item
)


class connection_list(marshalable_list):
    '''
    Marshalable model.

    Contain multiple items of connection_list_item.
    '''

    def __init__(self):
        '''
        Initialize Base.

        Initialize marshalable_list to store connection_list_items.
        '''
        marshalable_list.__init__(self, connection_list_item)

    def add_connection(self, origin, connection_model_instance):
        '''
        Add Connection.

        Add the given (module) connection_model_instance to the
        connection_list by creating a new instance, populate it with values
        and append it to the collection.
        '''
        connection = self.create_connection(
            connection_model_instance.id.get())
        connection.active.set(connection_model_instance.active.get())
        connection.type.set(connection_model_instance.type.get())
        connection.origin.set(origin)

        self.append(connection)

    def create_connection(self, connection_id):
        '''
        Create connection.

        Create a new connection with default values to be added to the
        connection list.
        '''
        connection = self.item_model_class()
        connection.id.set(connection_id)
        connection.active.set(False)
        return connection

    def delete_connection(self, connection_id):
        '''
        Delete Connection.

        Delete all connections with the given connection_id when the
        connection is not active.
        Raises ConnectedError when the connection is active.
        '''
        connection = self.find_by_id(connection_id)
        if connection.active.get() is True:
            raise ConnectedError()
        self.remove(connection)

    def find_by_id(self, connection_id):
        '''
        Find By Id.

        Find a connection by Id. Raise NotFoundError when connection is not
        found.
        '''
        for index, connection in enumerate(self):
            if connection.id.get() == connection_id:
                return connection
        raise NotFoundError(connection_id)
