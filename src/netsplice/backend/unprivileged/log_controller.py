# -*- coding: utf-8 -*-
# log_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Log Controller.

Handles log events from the unprivileged backend.
'''

from netsplice.backend.event import names
from netsplice.backend.event import origins
from netsplice.backend.event import types
from netsplice.model.log_item import log_item as log_item_model
from netsplice.model.event import event as event_model
from netsplice.util import get_logger
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.util.errors import NotFoundError

logger = get_logger()


class log_controller(middleware):
    '''
    Log Controller.

    Receives the log messages from the Unprivileged Backend.
    '''

    def __init__(self, request, response):
        '''
        Initialize Controller.

        Initialize middleware.
        '''
        middleware.__init__(self, request, response)

    def post(self):
        '''
        Receive Log Event.

        Receive log events from the unprivileged backend.
        '''
        request_model = log_item_model()
        try:
            request_model.from_json(self.request.body.decode('utf-8'))

            log_module = self.application.get_module('log')
            broker = self.application.get_module('connection').broker
            connection_id = request_model.extra.connection_id.get()
            account_id = None
            if connection_id is not None:
                try:
                    connection = broker.\
                        find_connection_by_unprivileged_connection_id(
                            connection_id)
                    account_id = connection.account.id.get()
                    request_model.extra.account_id.set(account_id)
                except NotFoundError:
                    pass
            request_model.origin.set(origins.UNPRIVILEGED)
            log_module.add_log_entry(request_model)
            self.application.get_module('event').notify(names.LOG_CHANGED)

            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2194, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2195, errors)
            self.set_status(400)
            logger.error(str(errors))
        self.finish()
