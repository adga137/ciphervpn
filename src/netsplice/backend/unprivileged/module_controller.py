# -*- coding: utf-8 -*-
# module_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.ipc.middleware import middleware


class module_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def post(self):
        # data = json.loads(self.request.body)
        # dispatch:
        # if data['state'] == 'disconnected':
        #    gui_model.buttons['connect_button']['enabled'] = True
        #    gui_model.commit()
        #    noop = true
        # if data['state'] == 'changed':
        #    gui_model.connections[0]['step'] = data['step']
        #    gui_model.commit()
        self.set_status(200)
        self.finish()

    def get(self):
        # self.write(json.dumps(privileged_model.__dict__))
        self.set_status(200)
        self.finish()
