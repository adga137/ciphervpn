# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
from netsplice.backend.log.model import model as module_model
import netsplice.config.log as config

name = 'log'

endpoints = []

model = module_model()


def add_log_entry(log_model_instance):
    '''
    Add a entry to the backend log.

    Adds the given log_model_instance to the model and fixing the model index
    fist. When the model's length is over the configured maximum of displayable
    items, the top of the log gets purged.
    Commit the model to notify about the change.
    '''
    insert_log_entry(log_model_instance)
    model.commit()


def insert_log_entry(log_model_instance):
    '''
    Insert a entry to the backend log.

    Adds the given log_model_instance to the model and fixing the model index
    fist. When the model's length is over the configured maximum of displayable
    items, the top of the log gets purged.
    Does not commit.
    '''
    log_model_instance.index.set(model.get_next_index())
    model.append(log_model_instance)
    if len(model) > config.MAX_LOGVIEWER_ITEMS:
        while len(model) > config.MAX_LOGVIEWER_ITEMS:
            del model[0]
