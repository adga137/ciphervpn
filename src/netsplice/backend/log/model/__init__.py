# -*- coding: utf-8 -*-
# model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for GUI process. Stores all information the GUI should know about
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.backend.log.model.log_item import log_item as log_item_model


class model(marshalable_list):
    def __init__(self):
        marshalable_list.__init__(self, log_item_model)
        self.log_index = 0

    def filter_by_extra(self, key, value, min_index=None):
        log = []
        for item in self:
            if item.extra is None:
                continue
            if key not in item.extra.__dict__:
                continue
            if item.extra.__dict__[key].get() != value:
                continue
            if min_index is not None and item.index.value < min_index:
                continue
            log.append(item)
        return log

    def get_next_index(self):
        '''
        Get Next Index.

        Increase the log_index and return the increased value.
        '''
        self.log_index += 1
        return self.log_index
