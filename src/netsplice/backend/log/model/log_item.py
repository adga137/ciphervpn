# -*- coding: utf-8 -*-
# log_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for GUI process. Stores all information the GUI should know about
'''

from netsplice.util.model.field import field
from netsplice.model.log_item import log_item as log_item_model


class log_item(log_item_model):
    def __init__(self):
        log_item_model.__init__(self)

    def matches_connection(self, connection_id):
        if connection_id is None:
            return True
        if self.extra is None:
            return False
        if self.extra.connection_id.get() == connection_id:
            return True
        return False
