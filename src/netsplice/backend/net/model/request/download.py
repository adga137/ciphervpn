# -*- coding: utf-8 -*-
# download.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for requesting downloads.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.validator.uri import (
    uri as uri_validator
)
from netsplice.model.validator.none import (
    none as none_validator
)
from netsplice.model.validator.path import (
    path as path_validator
)
from netsplice.model.validator.signature import (
    signature as signature_validator
)
from netsplice.model.validator.sign_key import (
    sign_key as sign_key_validator
)
from netsplice.model.validator.sha1sum import (
    sha1sum as sha1sum_validator
)
from netsplice.model.validator.sha256sum import (
    sha256sum as sha256sum_validator
)
from .fingerprint_list import fingerprint_list


class download(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.url = field(
            required=True,
            validators=[uri_validator()])

        self.fingerprints = fingerprint_list()

        self.signature = field(
            required=False,
            validators=[none_validator(exp_or=[signature_validator()])])

        self.sign_key = field(
            required=False,
            validators=[none_validator(exp_or=[sign_key_validator()])])

        self.sha1sum = field(
            required=False,
            validators=[none_validator(exp_or=[sha1sum_validator()])])

        self.sha256sum = field(
            required=False,
            validators=[none_validator(exp_or=[sha256sum_validator()])])

        self.destination = field(
            required=True,
            validators=[path_validator()])
