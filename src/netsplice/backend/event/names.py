# -*- coding: utf-8 -*-
# names.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''


ACCOUNT_CONNECTED = 'account_connected'
ACCOUNT_CONNECTING = 'account_connecting'
ACCOUNT_DISCONNECTED = 'account_disconnected'
ACCOUNT_RECONNECT = 'account_reconnect'
ACCOUNT_RECONNECTING = 'account_reconnecting'
ACCOUNT_RECONNECTED = 'account_reconnected'
ACCOUNT_REQUEST_DISCONNECT = 'account_request_disconnect'
ACCOUNT_CONNECT_FAILED = 'account_connect_failed'

BACKEND_READY = 'backend_ready'
CONNECTION_STATE_CHANGED = 'connection_state_changed'
CONNECTION_ERROR = 'connection_error'

CONNECTION_CONNECTED = 'connection_connected'
CONNECTION_CONNECTING = 'connection_connecting'
CONNECTION_DISCONNECTED = 'connection_disconnected'
CONNECTION_RECONNECT = 'connection_reconnect'
CONNECTION_RECONNECTING = 'connection_reconnecting'
CONNECTION_RECONNECTED = 'connection_reconnected'
CONNECTION_REQUEST_DISCONNECT = 'connection_request_disconnect'
CONNECTION_CONNECT_FAILED = 'connection_connect_failed'
CONNECTION_UP = 'connection_up'
CONNECTION_DOWN = 'connection_down'

ERROR = 'error'
LOG_CHANGED = 'log_changed'
PRIVATE_KEY_PASSWORD = 'private_key_password'
SUBPROCESS_STARTED = 'backend.event.subprocess_started'
SYSTEMINFO_MODEL = 'systeminfo.model'
DOWNLOAD_MODEL = 'download.model'
DOWNLOAD_NOTIFY = 'network.download.notify'
USERNAME_PASSWORD = 'username_password'


ACCOUNT_EVENTS = [
    ACCOUNT_CONNECT_FAILED,
    ACCOUNT_CONNECTED,
    ACCOUNT_CONNECTING,
    ACCOUNT_DISCONNECTED,
    ACCOUNT_RECONNECT,
    ACCOUNT_RECONNECTING,
    ACCOUNT_RECONNECTED
]

CONNECTION_EVENTS = [
    CONNECTION_STATE_CHANGED,
    CONNECTION_CONNECT_FAILED,
    CONNECTION_CONNECTED,
    CONNECTION_CONNECTING,
    CONNECTION_DISCONNECTED,
    CONNECTION_RECONNECT,
    CONNECTION_RECONNECTED,
    CONNECTION_RECONNECTING,
    CONNECTION_STATE_CHANGED,
    CONNECTION_UP,
    CONNECTION_DOWN
]

BACKEND_EVENTS = [
    BACKEND_READY,
]
