# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Event module used by the event loop.
'''
import sys
from netsplice.config import flags
from netsplice.backend.event.model import model as module_model
from netsplice.backend.event import origins as event_origins
from netsplice.backend.event import types as event_types
from netsplice.util.errors import NotRegisteredEventError
from netsplice.util import get_logger

logger = get_logger()

name = 'event'

endpoints = []

model = module_model()
this = sys.modules[__name__]
this.registered_origins = dict()
this.registered_destinations = dict()


def register_origin(origin_type, event_name_list):
    '''
    Register Origin.

    Register that the given origin may notify/add events specified in the
    event_name_list.
    Existing origins are appended.

    Arguments:
        origin_type (type or string): a type or string to be stored as key
        event_name_list (list<string>): list of event names
    '''
    try:
        for event_name in event_name_list:
            if event_name not in this.registered_origins[origin_type]:
                this.registered_origins[origin_type].append(event_name)
    except KeyError:
        this.registered_origins[origin_type] = event_name_list
    if flags.DEBUG:
        logger.debug(
            'Register origins: type: %s, list %s'
            % (str(origin_type), str(this.registered_origins[origin_type])))


def register_destination(origin_type, event_name_list):
    '''
    Register Destination

    Register that the given origin may receive events specified in the
    event_name_list.
    Existing registered events are appended.

    Arguments:
        origin_type (type or string): a type or string to be stored as key
        event_name_list (list<string>): list of event names
    '''
    try:
        for event_name in event_name_list:
            if event_name not in this.registered_destinations[origin_type]:
                this.registered_destinations[origin_type].append(event_name)
        this.registered_destinations[origin_type].extend(event_name_list)
    except KeyError:
        this.registered_destinations[origin_type] = event_name_list
    if flags.DEBUG:
        logger.debug(
            'Register destinations: type: %s list: %s'
            % (str(origin_type),
                str(this.registered_destinations[origin_type])))


def add_event(event_model_instance):
    '''
    Add Event.

    Add the given event_model_instance to the model and
    commit.
    Evaluate the call stack to find the origin of the adder in order to prevent
    that unregistered origins insert events.
    '''
    origin = event_origins.BACKEND
    try:
        origin = type(sys._getframe(1).f_locals['self'])
    except KeyError:
        pass
    if origin not in this.registered_origins:
        raise NotRegisteredEventError(
            event_model_instance.name.get(), origin, this.registered_origins)
    if event_model_instance.name.get() not in this.registered_origins[origin]:
        raise NotRegisteredEventError(
            event_model_instance.name.get(), origin,
            this.registered_origins[origin])
    event_model_instance.origin.set(origin)
    if flags.DEBUG:
        logger.debug('Add Event: %s %s %s' % (
            origin, event_model_instance.name.get(),
            event_model_instance.data.get()))
    _add_event(event_model_instance)


def _add_event(event_model_instance):
    '''
    Add Event.

    'private' that is used to queue the event and trigger the model_changed.

    Arguments:
        event_model_instance (model.event): instance of a event
    '''
    model.append(event_model_instance)
    model.commit()


def notify(event_name, data=None):
    '''
    Emit a Event.

    Convenience method to send a event in the backend so it can be picked
    up by plugins.
    Evaluate the call stack to find the origin of the adder in order to prevent
    that unregistered origins insert events.
    '''
    origin = event_origins.BACKEND
    try:
        origin = type(sys._getframe(1).f_locals['self'])
    except KeyError:
        pass
    if origin not in this.registered_origins:
        raise NotRegisteredEventError(
            event_name, origin, this.registered_origins)
    if event_name not in this.registered_origins[origin]:
        raise NotRegisteredEventError(
            event_name, origin, this.registered_origins[origin])
    if flags.VERBOSE > 1:
        logger.debug('Notify Event: %s %s %s' % (origin, event_name, data))
    event_model_instance = model.new_event(event_types.NOTIFY, event_name)
    event_model_instance.origin.set(str(origin))
    event_model_instance.data.set(data)

    _add_event(event_model_instance)
