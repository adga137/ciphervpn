# -*- coding: utf-8 -*-
# test___init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Event.

Checks Module functions.
'''
import mock
import netsplice.backend.event as event_module
from netsplice.util.errors import NotRegisteredEventError
from netsplice.model.event import event as event_model
from netsplice.config import flags


def test_register_origin_new_creates_event_name_list():
    before = len(event_module.registered_origins.keys())
    event_module.register_origin('test', ['a'])
    after = len(event_module.registered_origins.keys())
    assert(after == before + 1)


def test_register_origin_existing_extends_event_name_list():
    event_module.register_origin('test', ['a'])
    before = len(event_module.registered_origins.keys())
    event_module.register_origin('test', ['b'])
    after = len(event_module.registered_origins.keys())
    assert(after == before)


@mock.patch('netsplice.backend.event.logger')
def test_register_origin_logs_only_in_debug(mock_logger):
    debug_flag = flags.DEBUG
    flags.DEBUG = False
    event_module.register_origin('test', ['a'])
    mock_logger.debug.assert_not_called()
    flags.DEBUG = True
    event_module.register_origin('test', ['b'])
    mock_logger.debug.assert_called()
    flags.DEBUG = debug_flag


def test_register_destination_new_creates_event_name_list():
    before = len(event_module.registered_destinations.keys())
    event_module.register_destination('test', ['a'])
    after = len(event_module.registered_destinations.keys())
    assert(after == before + 1)


def test_register_destination_existing_extends_event_name_list():
    event_module.register_destination('test', ['a'])
    before = len(event_module.registered_destinations.keys())
    event_module.register_destination('test', ['b'])
    after = len(event_module.registered_destinations.keys())
    assert(after == before)


@mock.patch('netsplice.backend.event.logger')
def test_register_destination_logs_only_in_debug(mock_logger):
    debug_flag = flags.DEBUG
    flags.DEBUG = False
    event_module.register_destination('test', ['a'])
    mock_logger.debug.assert_not_called()
    flags.DEBUG = True
    event_module.register_destination('test', ['b'])
    mock_logger.debug.assert_called()
    flags.DEBUG = debug_flag


def test_add_event_not_registered_not_appends_event():
    before = len(event_module.model)
    test_event = event_model()
    test_event.name.set('test')
    try:
        event_module.add_event(test_event)
        assert(False)  # Expected error not raised
    except NotRegisteredEventError:
        assert(len(event_module.model) == before)
        assert(True)  # Error raised


def test_add_event_registered_appends_event():
    before = len(event_module.model)
    event_module.register_origin('backend', ['test_add_event'])
    test_event = event_model()
    test_event.name.set('test_add_event')
    event_module.add_event(test_event)
    assert(len(event_module.model) == before + 1)


def test_notify_not_registered_not_appends_event():
    before = len(event_module.model)
    try:
        event_module.notify('notify_test')
        assert(False)  # Expected error not raised
    except NotRegisteredEventError:
        assert(len(event_module.model) == before)
        assert(True)  # Error raised


def test_notify_registered_appends_event():
    before = len(event_module.model)
    event_module.register_origin('backend', ['test_notify'])
    event_module.notify('test_notify')
    assert(len(event_module.model) == before + 1)
