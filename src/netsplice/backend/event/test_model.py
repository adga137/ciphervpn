# -*- coding: utf-8 -*-
# test_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Event Model.

Checks Model functions.
'''

from netsplice.backend.event.model import model


def get_test_object():
    m = model()
    return m


def test_new_element_returns_initialized_instance():
    model = get_test_object()
    event = model.new_event('TYPE_NAME', 'NAME')
    assert(event.type.get() == 'TYPE_NAME')
    assert(event.name.get() == 'NAME')
