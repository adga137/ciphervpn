# -*- coding: utf-8 -*-
# override_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Override Controller to access the persistent config override instances.
'''

from netsplice.util import get_logger
from netsplice.util.errors import (
    NotFoundError, NotUniqueError
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.backend.preferences.model.request.name_type import (
    name_type as name_type_model
)
from netsplice.backend.preferences.model.response.preference_value import (
    preference_value as preference_value_model
)

logger = get_logger()


class override_controller(middleware):
    '''
    Account Controller to access the persistent Account instances.
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def delete(self, name, type):
        '''
        Delete an existing override.
        '''
        request_parameter_model = name_type_model()
        module = self.application.get_module('preferences')
        model = module.model.overrides
        try:
            request_parameter_model.name.set(name)
            request_parameter_model.type.set(type)

            model.remove(name, type)
            module.model.commit_collection('overrides')
            self.write(request_parameter_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2127, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2128, errors)
            self.set_status(400)
            logger.error(str(errors))
        except NotFoundError as errors:
            self.set_error_code(2129, errors)
            self.set_status(404)
            logger.error(str(errors))
        self.finish()

    def get(self, name=None, type=None):
        '''
        List the available persistent overrides.
        '''
        response_model = preference_value_model()
        request_parameter_model = name_type_model()
        module = self.application.get_module('preferences')
        model = module.model.overrides
        try:

            if name is None and type is None:
                # XXX response model
                self.write(model.to_json())
                self.set_status(200)
            else:
                request_parameter_model.name.set(name)
                request_parameter_model.type.set(type)
                if not model.find(name, type):
                    raise NotFoundError(
                        '%s is not a override preference' % (name,))
                response_model.value.set(
                    model.find(name, type).value.get())
                self.write(response_model.to_json())
                self.set_status(200)
        except NotFoundError as errors:
            self.set_error_code(2130, errors)
            self.set_status(404)
            logger.error(str(errors))
        self.finish()

    def post(self, name, type):
        '''
        Update an existing, override value.
        '''
        request_model = preference_value_model()
        request_parameter_model = name_type_model()
        module = self.application.get_module('preferences')
        model = module.model.overrides
        try:
            request_parameter_model.name.set(name)
            request_parameter_model.type.set(type)

            request_model.from_json(self.request.body.decode('utf-8'))
            new_item = model.create(name, type)
            new_item.value.set(
                request_model.value.get())
            model.append(new_item)
            module.model.commit_collection('overrides')
            self.write(request_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2131, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2132, errors)
            self.set_status(400)
            logger.error(str(errors))
        except NotFoundError as errors:
            self.set_error_code(2133, errors)
            self.set_status(404)
            logger.error(str(errors))
        except NotUniqueError as errors:
            self.set_error_code(2134, errors)
            self.set_status(400)
            logger.error(str(errors))
        self.finish()

    def put(self, name, type):
        '''
        Update an existing, override value.
        '''
        request_model = preference_value_model()
        request_parameter_model = name_type_model()
        module = self.application.get_module('preferences')
        model = module.model.overrides
        try:
            request_parameter_model.name.set(name)
            request_parameter_model.type.set(type)

            request_model.from_json(self.request.body.decode('utf-8'))
            if not model.find(name, type):
                raise NotFoundError(
                    '%s is not a override preference' % (name,))
            model.find(name, type).value.set(
                request_model.value.get())
            module.model.commit_collection('overrides')
            self.write(request_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2135, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2136, errors)
            self.set_status(400)
            logger.error(str(errors))
        except NotFoundError as errors:
            self.set_error_code(2137, errors)
            self.set_status(404)
            logger.error(str(errors))
        self.finish()
