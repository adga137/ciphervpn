# -*- coding: utf-8 -*-
# grouped_account_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Group Controller to access and modify the persistent Group instances.
'''


from netsplice.util import get_logger
from netsplice.util.errors import (
    NotFoundError, NotEmptyError
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError

from netsplice.backend.preferences.model.request.account_id import (
    account_id as account_id_model
)
from netsplice.backend.preferences.model.request.grouped_account_create \
    import (
        grouped_account_create as grouped_account_create_model
    )
from netsplice.backend.preferences.model.request.grouped_account_update \
    import (
        grouped_account_update as grouped_account_update_model
    )
from netsplice.backend.preferences.model.response.grouped_account_list \
    import (
        grouped_account_list as response_grouped_account_list_model
    )

logger = get_logger()


class grouped_account_controller(middleware):
    '''
    Account Controller to access the persistent Account instances.
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def get(self, grouped_account_id=None):
        '''
        List the available persistent grouped_accounts.
        '''
        module = self.application.get_module('preferences')
        response_model = response_grouped_account_list_model()
        try:
            if grouped_account_id is None:
                for grouped_account in module.model.grouped_accounts:
                    response_model.append(grouped_account)

                self.write(response_model.to_json())
            else:
                request_model = account_id_model()
                request_model.id.set(grouped_account_id)
                grouped_accounts_model = module.model.grouped_accounts
                grouped_account = grouped_accounts_model.find_by_id(
                    request_model.id.get())
                response_model.append(grouped_account)
                self.write(response_model[0].to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2226, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2227, errors)
            self.set_status(400)
            logger.error(str(errors))
        except NotFoundError as errors:
            self.set_error_code(2228, errors)
            self.set_status(404)
            logger.error(str(errors))
        self.finish()

    def put(self):
        '''
        Update an existing, not connected account.
        '''
        module = self.application.get_module('preferences')
        model = module.model.grouped_accounts
        accounts = module.model.accounts
        chains = module.model.chains
        request_model = grouped_account_update_model()
        try:
            request_model.from_json(self.request.body.decode('utf-8'))
            account = accounts.find_by_id(request_model.id.get())
            chain = chains.find_by_id(request_model.id.get())
            module.model.groups.find_by_id(request_model.group_id.get())
            if chain.parent_id.get() is not None:
                # move instances out of the chain
                chain.parent_id.set(None)
                chains.verify(accounts)
                model.verify(
                    module.model.accounts,
                    module.model.groups,
                    module.model.chains)
                module.model.commit_collection('chains')
            grouped_account = model.find_by_id(account.id.get())
            grouped_account.update(request_model.group_id.get(), account)
            grouped_account.weight.set(request_model.weight.get())
            model.verify(
                module.model.accounts,
                module.model.groups,
                module.model.chains)
            module.model.commit_collection('grouped_accounts')
            self.write(request_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2198, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2199, errors)
            self.set_status(400)
            logger.error(str(errors))
        except NotFoundError as errors:
            self.set_error_code(2200, errors)
            self.set_status(404)
            logger.error(str(errors))
        self.finish()
