# -*- coding: utf-8 -*-
# chain_controller.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
chain Controller to access and modify the persistent chain
instances.
'''


from netsplice.util import get_logger
from netsplice.util.errors import (
    NotFoundError, NotEmptyError
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError

from netsplice.backend.preferences.model.request.account_id import (
    account_id as account_id_model
)
from netsplice.backend.preferences.model.request.chain_create \
    import (
        chain_create as chain_create_model
    )
from netsplice.backend.preferences.model.request.chain_update \
    import (
        chain_update as chain_update_model
    )
from netsplice.backend.preferences.model.response.chain_list \
    import (
        chain_list as response_chain_list_model
    )
from netsplice.backend.preferences.model.response.chain_item \
    import (
        chain_item as response_chain_item_model
    )

logger = get_logger()


class chain_controller(middleware):
    '''
    Account Controller to access the persistent Account instances.
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def get(self, chain_id=None):
        '''
        List the available persistent chains.
        '''
        module = self.application.get_module('preferences')
        response_model = response_chain_list_model()
        try:
            if chain_id is None:
                for chain in module.model.chains:
                    response_model.append(chain)

                self.write(response_model.to_json())
            else:
                request_model = account_id_model()
                request_model.id.set(chain_id)
                chains = module.model.chains
                chain = chains.find_by_id(
                    request_model.id.get())
                response_model.append(chain)
                self.write(response_model[0].to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2280, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2281, errors)
            self.set_status(400)
            logger.error(str(errors))
        except NotFoundError as errors:
            self.set_error_code(2282, errors)
            self.set_status(404)
            logger.error(str(errors))
        self.finish()

    def post(self):
        '''
        Create a new chain.
        '''
        module = self.application.get_module('preferences')
        model = module.model.chains
        accounts = module.model.accounts
        request_model = chain_create_model()
        response_model = account_id_model()
        try:
            request_model.from_json(self.request.body.decode('utf-8'))
            accounts.find_by_id(request_model.parent_id.get())
            chain_account = accounts.find_by_id(
                request_model.account_id.get())
            model.add_account(
                request_model.parent_id.get(),
                chain_account)
            model.verify(accounts)
            module.model.commit_collection('chains')
            response_model.id.set(chain_account.id.get())
            self.write(response_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2274, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2275, errors)
            self.set_status(400)
            logger.error(str(errors))
        except NotFoundError as errors:
            self.set_error_code(2276, errors)
            self.set_status(404)
            logger.error(str(errors))
        self.finish()

    def put(self, chain_id):
        '''
        Update an existing chain.
        '''
        module = self.application.get_module('preferences')
        model = module.model.chains
        accounts = module.model.accounts
        grouped_accounts = module.model.grouped_accounts
        groups = module.model.groups
        request_model = chain_update_model()
        response_model = response_chain_item_model()
        try:
            request_model.from_json(self.request.body.decode('utf-8'))
            account = accounts.find_by_id(request_model.id.get())
            if request_model.parent_id.get() is not None:
                accounts.find_by_id(request_model.parent_id.get())
            chain = model.find_by_id(account.id.get())
            chain.update_parent(request_model.parent_id.get(), account, model)
            chain.update(request_model)
            model.verify(accounts)
            # parent of account may have been a group before
            grouped_accounts.verify(accounts, groups, model)
            module.model.commit_collection('chains')
            chain = model.find_by_id(account.id.get())
            response_model.from_json(chain.to_json())
            self.write(response_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2277, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2278, errors)
            self.set_status(400)
            logger.error(str(errors))
        except NotFoundError as errors:
            self.set_error_code(2279, errors)
            self.set_status(404)
            logger.error(str(errors))
        self.finish()
