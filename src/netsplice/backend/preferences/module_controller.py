# -*- coding: utf-8 -*-
# module_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Module controller for actions and requests that directly involve the state of
the frontend.
'''

from netsplice.util.ipc.middleware import middleware
from netsplice.backend.preferences.model.response import (
    model as response_model_model
)


class module_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def get(self):
        '''
        The "module" model-request is used to get the current state of the
        backend suitable for presentation.
        '''
        response_model = response_model_model()
        response_model.from_json(
            self.application.get_module('preferences').model.to_json())
        self.write(response_model.to_json())
        self.set_status(200)
        self.finish()
        # gui_model.set_etag(self._headers['Etag'])
