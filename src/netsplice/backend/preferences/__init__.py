# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
from netsplice.backend.preferences.model import model as module_model
from netsplice.util.ipc.route import get_module_route


name = 'preferences'

endpoints = get_module_route(
    'netsplice.backend',
    [
        (r'/module/preferences', 'module'),
        (r'/module/preferences/accounts', 'account'),
        (r'/module/preferences/accounts/(?P<account_id>[^\/]+)', 'account'),
        (r'/module/preferences/events', 'events'),
        (r'/module/preferences/chains', 'chain'),
        (r'/module/preferences/chains/'
            '(?P<chain_id>[^\/]+)',
            'chain'),
        (r'/module/preferences/groups', 'group'),
        (r'/module/preferences/groups/(?P<group_id>[^\/]+)', 'group'),
        (r'/module/preferences/grouped_accounts', 'grouped_account'),
        (r'/module/preferences/grouped_accounts/'
            '(?P<grouped_account_id>[^\/]+)',
            'grouped_account'),
        (r'/module/preferences/overrides', 'override'),
        (r'/module/preferences/overrides/(?P<name>[^\/]+)/(?P<type>[^\/]+)',
            'override'),
        (r'/module/preferences/value/'
            '(?P<collection_name>[^\/]+)/(?P<name>[^\/]+)',
            'value'),
        (r'/module/preferences/complex/'
            '(?P<collection_name>[^\/]+)/(?P<name>[^\/]+)',
            'complex'),
        (r'/module/preferences/complex/'
            '(?P<collection_name>[^\/]+)',
            'complex'),
        (r'/module/preferences/plugin/(?P<plugin_name>[^\/]+)/'
            'attribute/(?P<attribute_name>[^\/]+)',
            'plugin'),
        (r'/module/preferences/plugin/(?P<plugin_name>[^\/]+)/'
            'collection/(?P<collection_name>[^\/]+)/'
            'instance_id/(?P<instance_id>[^\/]+)',
            'plugin_collection'),
        (r'/module/preferences/plugin/(?P<plugin_name>[^\/]+)/'
            'collection/(?P<collection_name>[^\/]+)/'
            'instance_id/(?P<instance_id>[^\/]+)/'
            'attribute/(?P<attribute_name>[^\/]+)',
            'plugin_collection'),
        (r'/module/preferences/plugin/(?P<plugin_name>[^\/]+)',
            'plugin'),
    ])

model = module_model()

model.defaults()
