# -*- coding: utf-8 -*-
# account_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Account Controller to access the persistent Account instances.
'''

from netsplice.util import get_logger
from netsplice.util.errors import (
    NotFoundError, ConnectedError
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError

from netsplice.backend.preferences.model.request.account_id import (
    account_id as account_id_model
)
from netsplice.backend.preferences.model.request.account_create import (
    account_create as account_create_model
)
from netsplice.backend.preferences.model.request.account_update import (
    account_update as account_update_model
)
from netsplice.backend.preferences.model.response.account_list import (
    account_list as account_list_model
)
from netsplice.backend.preferences.model.response.account_detail_item import (
    account_detail_item as account_detail_item_model
)

logger = get_logger()


class account_controller(middleware):
    '''
    Account Controller to access the persistent Account instances.
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def delete(self, account_id=None):
        '''
        Delete an existing account.
        '''
        module = self.application.get_module('preferences')
        connection_module = self.application.get_module('connection')
        model = module.model.accounts
        request_model = account_id_model()
        try:
            request_model.id.set(account_id)
            model.delete_account(request_model.id.get(), connection_module)
            module.model.commit_account(request_model.id.get())
            module.model.chains.verify(model)
            module.model.grouped_accounts.verify(
                model, module.model.groups, module.model.chains)
            module.model.commit_collection('grouped_accounts')
            module.model.commit_collection('chains')
            self.write(request_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2008, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2009, errors)
            self.set_status(400)
            logger.error(str(errors))
        except NotFoundError as errors:
            self.set_error_code(2010, errors)
            self.set_status(404)
            logger.error(str(errors))
        except ConnectedError as errors:
            self.set_error_code(2011, errors)
            self.set_status(403)
            logger.error(str(errors))
        self.finish()

    def get(self, account_id=None):
        '''
        List the available persistent accounts.
        '''
        module = self.application.get_module('preferences')
        if account_id is None:
            '''
            List of accounts without credentials
            '''
            response_model = account_list_model()
            for account in module.model.accounts:
                response_model.add_account(account)

            self.write(response_model.to_json())
        else:
            '''
            A single account with all the details stored in the backend.
            '''
            response_model = account_detail_item_model()
            request_model = account_id_model()
            request_model.id.set(account_id)
            accounts_model = module.model.accounts
            account = accounts_model.find_by_id(request_model.id.get())
            response_model.from_json(account.to_json())
            self.write(response_model.to_json())
        self.set_status(200)
        self.finish()

    def post(self):
        '''
        Create a new persistent account.
        '''
        module = self.application.get_module('preferences')
        accounts = module.model.accounts
        groups = module.model.groups
        grouped_accounts = module.model.grouped_accounts
        chains = module.model.chains
        request_model = account_create_model()
        response_model = account_id_model()
        try:
            request_model.from_json(self.request.body.decode('utf-8'))
            new_account = accounts.new_account(request_model)
            accounts.add_account(new_account)
            chains.verify(accounts)
            grouped_accounts.verify(
                accounts, groups, chains)
            module.model.commit_account(new_account.id.get())
            module.model.commit_collection('groups')
            module.model.commit_collection('grouped_accounts')
            module.model.commit_collection('chains')
            response_model.id.set(new_account.id.get())
            self.write(response_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2004, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2005, errors)
            self.set_status(400)
            logger.error(str(errors))
        self.finish()

    def put(self):
        '''
        Update an existing, not connected account.
        '''
        module = self.application.get_module('preferences')
        connection_module = self.application.get_module('connection')
        model = module.model.accounts
        request_model = account_update_model()
        try:
            request_model.from_json(self.request.body.decode('utf-8'))
            model.update_account(request_model, connection_module)
            module.model.chains.verify(model)
            module.model.grouped_accounts.verify(
                model, module.model.groups, module.model.chains)
            module.model.commit_collection('grouped_accounts')
            module.model.commit_collection('chains')
            module.model.commit_account(request_model.id.get())
            self.write(request_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2090, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2091, errors)
            self.set_status(400)
            logger.error(str(errors))
        self.finish()
