# -*- coding: utf-8 -*-
# complex_controller.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Backend Controller to access the complex Preference values.
'''

from tornado.web import MissingArgumentError

from netsplice.util import get_logger
from netsplice.util.errors import (
    NotFoundError, ConnectedError, InvalidUsageError
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.util.model.marshalable import marshalable
from netsplice.backend.preferences.model.response.preference_value import (
    preference_value as preference_value_model
)
from netsplice.backend.preferences.model.response import (
    model as response_model_model
)
from netsplice.backend.preferences.model.request import (
    model as request_model_model
)
from netsplice.backend.preferences.model.request.attribute_name import (
    attribute_name as request_attribute_name_model
)
from netsplice.backend.preferences.model.request.collection_name import (
    collection_name as request_collection_name_model
)
from netsplice.backend.preferences.model.request.preference_value import (
    preference_value as request_preference_value_model
)

logger = get_logger()


class complex_controller(middleware):
    '''
    Controller to access complex (marshalable) Preferences.
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def get(self, collection_name, name=None):
        '''
        Get the values of a collection property in backend preferences.
        '''
        request_preferences_model = request_model_model()
        request_attribute_name = request_attribute_name_model()
        request_collection_name = request_collection_name_model()
        module = self.application.get_module('preferences')
        try:
            request_collection_name.name.set(collection_name)
            request_collection_model = request_preferences_model.\
                __dict__[collection_name]
            response_model_preferences = response_model_model()
            response_collection_model = response_model_preferences.\
                __dict__[collection_name]

            if name is not None:
                request_attribute_name.name.set(name)
                request_model = request_collection_model.__dict__[name]
                response_model = response_collection_model.__dict__[name]
                if not isinstance(response_model, (marshalable,)):
                    raise ValidationError(
                        '"%s"/"%s" is not a complex attribute'
                        % (collection_name, name,))
                response_model.from_json(
                    module.model.__dict__[collection_name].
                    __dict__[name].to_json())
            else:
                request_model = request_collection_model
                response_model = response_collection_model
                if not isinstance(response_model, (marshalable,)):
                    raise ValidationError(
                        '"%s" is not a complex attribute'
                        % (collection_name,))
                response_model.from_json(
                    module.model.__dict__[collection_name].to_json())
            self.write(response_model.to_json())
            self.set_status(200)
        except NotFoundError as errors:
            self.set_error_code(2203, errors)
            self.set_status(404)
            logger.error(str(errors))
        except InvalidUsageError as errors:
            self.set_error_code(2204, errors)
            self.set_status(400)
            logger.error(str(errors))
        except KeyError as errors:
            self.set_error_code(2212, errors)
            self.set_status(404)
            logger.error(str(errors))
        except ValidationError as errors:
            self.set_error_code(2213, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2214, errors)
            self.set_status(400)
            logger.error(str(errors))
        self.finish()

    def put(self, collection_name, name):
        '''
        Update complex preference value.
        '''
        request_preferences_model = request_model_model()
        request_value_model = request_preference_value_model()
        request_attribute_name = request_attribute_name_model()
        request_collection_name = request_collection_name_model()
        module = self.application.get_module('preferences')
        try:
            request_attribute_name.name.set(name)
            request_collection_name.name.set(collection_name)
            request_collection_model = request_preferences_model.\
                __dict__[collection_name]
            request_model = type(request_collection_model.__dict__[name])()
            if not isinstance(request_model, (marshalable,)):
                raise ValidationError(
                    '"%s"/"%s" is not a complex attribute'
                    % (collection_name, name,))
            request_value_model.from_json(self.request.body.decode('utf-8'))
            request_model.from_json(request_value_model.value.get())
            module.model.backend.__dict__[name].from_json(
                request_value_model.value.get())
            module.model.commit_collection(collection_name)
            self.write(request_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2207, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2208, errors)
            self.set_status(400)
            logger.error(str(errors))
        except InvalidUsageError as errors:
            self.set_error_code(2209, errors)
            self.set_status(400)
            logger.error(str(errors))
        except NotFoundError as errors:
            self.set_error_code(2210, errors)
            self.set_status(404)
            logger.error(str(errors))
        except KeyError as errors:
            self.set_error_code(2211, errors)
            self.set_status(404)
            logger.error(str(errors))
        except AttributeError as errors:
            self.set_error_code(2217, errors)
            self.set_status(404)
            logger.error(str(errors))
        self.finish()
