# -*- coding: utf-8 -*-
# group_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Group Controller to access and modify the persistent Group instances.
'''

from netsplice.util import get_logger
from netsplice.util.errors import (
    NotFoundError, NotEmptyError
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.util.errors import InvalidUsageError

from netsplice.backend.preferences.model.request.group_id import (
    group_id as group_id_model
)
from netsplice.backend.preferences.model.request.group_create import (
    group_create as group_create_model
)
from netsplice.backend.preferences.model.request.group_update import (
    group_update as group_update_model
)
from netsplice.backend.preferences.model.response.group_list import (
    group_list as response_group_list_model
)

logger = get_logger()


class group_controller(middleware):
    '''
    Account Controller to access the persistent Account instances.
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def delete(self, group_id):
        '''
        Delete an existing account.
        '''
        module = self.application.get_module('preferences')
        model = module.model.groups
        request_model = group_id_model()
        try:
            request_model.id.set(group_id)
            model.delete_group(request_model.id.get())
            module.model.grouped_accounts.verify(
                module.model.accounts,
                module.model.groups,
                module.model.chains)
            model.verify()
            module.model.commit_collection('groups')
            module.model.commit_collection('grouped_accounts')
            self.write(request_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2036, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2037, errors)
            self.set_status(400)
            logger.error(str(errors))
        except NotFoundError as errors:
            self.set_error_code(2038, errors)
            self.set_status(404)
            logger.error(str(errors))
        except NotEmptyError as errors:
            self.set_error_code(2039, errors)
            self.set_status(403)
            logger.error(str(errors))
        except InvalidUsageError as errors:
            self.set_error_code(2225, errors)
            self.set_status(403)
            logger.error(str(errors))
        self.finish()

    def get(self, group_id=None):
        '''
        List the available persistent groups.
        '''
        module = self.application.get_module('preferences')
        response_model = response_group_list_model()
        try:
            if group_id is None:
                for group in module.model.groups:
                    response_model.append(group)

                self.write(response_model.to_json())
            else:
                request_model = group_id_model()
                request_model.id.set(group_id)
                groups_model = module.model.groups
                group = groups_model.find_by_id(request_model.id.get())
                response_model.append(group)
                self.write(response_model[0].to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2220, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2221, errors)
            self.set_status(400)
            logger.error(str(errors))
        except NotFoundError as errors:
            self.set_error_code(2222, errors)
            self.set_status(404)
            logger.error(str(errors))
        self.finish()

    def post(self):
        '''
        Create a new persistent group.
        '''
        module = self.application.get_module('preferences')
        model = module.model.groups
        request_model = group_create_model()
        response_model = group_id_model()
        try:
            request_model.from_json(self.request.body.decode('utf-8'))
            module.model.groups.find_by_id(request_model.parent.get())
            new_group = model.initialize_group()
            new_group.update_new(request_model)
            model.append(new_group)
            module.model.grouped_accounts.verify(
                module.model.accounts,
                module.model.groups,
                module.model.chains)
            model.verify()
            module.model.commit_collection('groups')
            response_model.id.set(new_group.id.get())
            self.write(response_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2032, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2033, errors)
            self.set_status(400)
            logger.error(str(errors))
        except NotFoundError as errors:
            self.set_error_code(2223, errors)
            self.set_status(404)
            logger.error(str(errors))
        self.finish()

    def put(self):
        '''
        Update an existing, not connected account.
        '''
        module = self.application.get_module('preferences')
        model = module.model.groups
        request_model = group_update_model()
        try:
            request_model.from_json(self.request.body.decode('utf-8'))
            module.model.groups.find_by_id(request_model.parent.get())
            group = model.find_by_id(request_model.id.get())
            if group.immutable.get() is True:
                raise InvalidUsageError('The group must not be updated')
            group.update(request_model)
            module.model.grouped_accounts.verify(
                module.model.accounts,
                module.model.groups,
                module.model.chains)
            model.verify()
            module.model.commit_collection('groups')
            self.write(request_model.to_json())
            self.set_status(200)
        except NotFoundError as errors:
            self.set_error_code(2048, errors)
            self.set_status(404)
            logger.error(str(errors))
        except ValidationError as errors:
            self.set_error_code(2034, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2035, errors)
            self.set_status(400)
            logger.error(str(errors))
        except InvalidUsageError as errors:
            self.set_error_code(2224, errors)
            self.set_status(403)
            logger.error(str(errors))
        self.finish()
