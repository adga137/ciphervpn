# -*- coding: utf-8 -*-
# plugin_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Plugin Controller to access the persistent Plugin preference instances.
'''

from tornado.web import MissingArgumentError

from netsplice.util import get_logger
from netsplice.util.errors import (
    NotFoundError, ConnectedError
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.backend.preferences.model.response.preference_value import (
    preference_value as preference_value_model
)
from netsplice.backend.preferences.model.request.attribute_name import (
    attribute_name as request_attribute_name_model
)
from netsplice.backend.preferences.model.request.plugin_name import (
    plugin_name as request_plugin_name_model
)

logger = get_logger()


class plugin_controller(middleware):
    '''
    Plugin Controller to access the persistent Plugin preference instances.
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def get(self, plugin_name, attribute_name=None):
        '''
        List the preferences for the plugin, optionally by attribute_name.
        '''
        module = self.application.get_module('preferences')
        request_plugin_name = request_plugin_name_model()
        request_attribute_name = request_attribute_name_model()
        response_model = preference_value_model()
        try:
            request_plugin_name.name.set(plugin_name)
            plugin_model = module.model.plugins.find_by_name(plugin_name)
            if attribute_name is None:
                self.write(plugin_model.to_json())
                self.set_status(200)
            else:
                request_attribute_name.name.set(attribute_name)
                if attribute_name not in plugin_model.__dict__:
                    raise NotFoundError(
                        '%s is not a attribute_name in plugin %s'
                        % (attribute_name, plugin_name,))
                if isinstance(plugin_model.__dict__[attribute_name], (list,)):
                    response_model.value.set(
                        str(plugin_model.__dict__[attribute_name]))
                else:
                    response_model.value.set(
                        plugin_model.__dict__[attribute_name].get())
                self.write(response_model.to_json())
                self.set_status(200)
        except NotFoundError as errors:
            self.set_error_code(2119, errors)
            self.set_status(404)
            logger.error(str(errors))
        except ValidationError as errors:
            self.set_error_code(2241, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2242, errors)
            self.set_status(400)
            logger.error(str(errors))
        self.finish()

    def put(self, plugin_name, attribute_name):
        '''
        Update an existing plugin preference.
        '''
        request_model = preference_value_model()
        request_plugin_name = request_plugin_name_model()
        request_attribute_name = request_attribute_name_model()
        module = self.application.get_module('preferences')
        try:
            request_plugin_name.name.set(plugin_name)
            request_attribute_name.name.set(attribute_name)
            request_model.from_json(self.request.body.decode('utf-8'))
            plugin_model = module.model.plugins.find_by_name(plugin_name)
            if attribute_name == 'name':
                raise ValidationError('Name of plugin must not be changed')
            if attribute_name not in plugin_model.__dict__:
                raise NotFoundError(
                    '%s is not a attribute_name in plugin %s'
                    % (attribute_name, plugin_name,))
            if isinstance(plugin_model.__dict__[attribute_name], (list,)):
                plugin_model.__dict__[attribute_name].from_json(
                    request_model.value.get())
            else:
                plugin_model.__dict__[attribute_name].set(
                    request_model.value.get())
            plugin_model.commit_plugins(plugin_name)
            self.write(request_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2116, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2117, errors)
            self.set_status(400)
            logger.error(str(errors))
        except NotFoundError as errors:
            self.set_error_code(2118, errors)
            self.set_status(404)
            logger.error(str(errors))
        self.finish()
