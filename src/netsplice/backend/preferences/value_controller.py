# -*- coding: utf-8 -*-
# value_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Backend Controller to access the persistent Preference values.
'''

from tornado.web import MissingArgumentError

from netsplice.util import get_logger
from netsplice.util.errors import (
    NotFoundError, ConnectedError, InvalidUsageError
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.backend.preferences.model.request.attribute_name import (
    attribute_name as request_attribute_name_model
)
from netsplice.backend.preferences.model.request.collection_name import (
    collection_name as request_collection_name_model
)
from netsplice.backend.preferences.model.request import (
    model as request_model_model
)
from netsplice.backend.preferences.model.response.preference_value import (
    preference_value as preference_value_model
)
from netsplice.backend.preferences.model.response import (
    model as response_model_model
)

logger = get_logger()


class value_controller(middleware):
    '''
    Account Controller to access the persistent Backend Preferences.
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def get(self, collection_name, name=None):
        '''
        List and get the available backend preferences.
        '''
        request_preferences_model = request_model_model()
        request_attribute_name = request_attribute_name_model()
        request_collection_name = request_collection_name_model()
        module = self.application.get_module('preferences')
        try:
            request_collection_name.name.set(collection_name)
            request_collection_model = request_preferences_model.\
                __dict__[collection_name]
            response_model_preferences = response_model_model()
            response_collection_model = response_model_preferences.\
                __dict__[collection_name]
            if name is None:
                response_model = response_collection_model
                response_model.from_json(
                    module.model.__dict__[collection_name].to_json())
            else:
                request_attribute_name.name.set(name)

                response_model = preference_value_model()
                response_model.value.set(
                    module.model.__dict__[collection_name].get(name))
            self.write(response_model.to_json())
            self.set_status(200)
        except NotFoundError as errors:
            self.set_error_code(2026, errors)
            self.set_status(404)
            logger.error(str(errors))
        except KeyError as errors:
            self.set_error_code(2218, errors)
            self.set_status(404)
            logger.error(str(errors))
        except InvalidUsageError as errors:
            self.set_error_code(2202, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValidationError as errors:
            self.set_error_code(2215, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2216, errors)
            self.set_status(400)
            logger.error(str(errors))
        self.finish()

    def put(self, collection_name, name):
        '''
        Update backend preference value.
        '''
        request_preferences_model = request_model_model()
        request_attribute_name = request_attribute_name_model()
        request_collection_name = request_collection_name_model()
        module = self.application.get_module('preferences')
        try:
            request_collection_name.name.set(collection_name)
            request_collection_model = request_preferences_model.\
                __dict__[collection_name]
            request_model = preference_value_model()
            request_model.from_json(self.request.body.decode('utf-8'))
            module.model.__dict__[collection_name].set_named(
                name, request_model.value.get())
            module.model.commit_collection(collection_name)
            self.write(request_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2124, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2125, errors)
            self.set_status(400)
            logger.error(str(errors))
        except InvalidUsageError as errors:
            self.set_error_code(2206, errors)
            self.set_status(400)
            logger.error(str(errors))
        except NotFoundError as errors:
            self.set_error_code(2126, errors)
            self.set_status(404)
            logger.error(str(errors))
        except KeyError as errors:
            self.set_error_code(2219, errors)
            self.set_status(404)
            logger.error(str(errors))
        self.finish()
