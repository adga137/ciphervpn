# -*- coding: utf-8 -*-
# group_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Groups.
'''

import uuid
import netsplice.config.backend as config

from netsplice.util.errors import (
    NotFoundError, NotEmptyError, NotUniqueError, InvalidUsageError
)
from netsplice.util.model.marshalable_list_persistent import (
    marshalable_list_persistent
)
from netsplice.backend.preferences.model.group_item import (
    group_item as group_item_model
)


class group_list(marshalable_list_persistent):
    '''
    Groups List Model.
    '''
    def __init__(self, owner):
        marshalable_list_persistent.__init__(self, group_item_model, owner)

    def _get_weight(self, grouped_account_model_instance):
        '''
        Get Weight.

        Return weight for a group_account item for sorting.
        '''
        return grouped_account_model_instance.weight.get()

    def apply_values(self):
        '''
        Apply Values.

        Apply values from the preferences to the config so other
        components can use the values.
        '''
        pass

    def delete_group(self, group_id):
        '''
        Delete Group

        Delete the group with the given group_id when it is empty.
        '''
        group = self.find_by_id(group_id)
        if group.immutable.get() is True:
            raise InvalidUsageError('The group must not be deleted.')
        if self.has_child_groups(group):
            raise NotEmptyError()
        self.remove(group)

    def find_by_id(self, group_id):
        '''
        Find By Id.

        Return the group with the given id or raise a NotFoundError.
        '''
        for group in self:
            if group.id.get() == group_id:
                return group
        raise NotFoundError(
            'No group with id %s found.' % (group_id,))

    def find_global(self):
        '''
        Find Global.

        Return the group with the global name.
        '''
        for group in self:
            if group.immutable.get() is False:
                continue
            if group.name.get() == config.GLOBAL_GROUP_NAME:
                return group
        raise NotFoundError('No immutable global group found.')

    def find_root(self):
        '''
        Find Root.

        Return the group with no parent.
        '''
        for group in self:
            if group.immutable.get() is False:
                continue
            if group.parent.get() is None:
                return group
        raise NotFoundError('No immutable group with None parent found.')

    def get_child_groups(self, root_group):
        '''
        Get Child Groups.

        Return a list of sorted groups that are children of the given
        root_group.

        :param root_group backend.preferences.model.group_item
        '''
        groups = []
        for group in self:
            if group.parent.get() == root_group.id.get():
                groups.append(group)
        sorted_groups = sorted(
            groups, key=self._get_weight)
        return sorted_groups

    def get_parent_groups(self, root_group):
        '''
        Get Parent Groups.

        Return a list of groups that are parent to the given root_group.
        When a parent is listed multiple times, a NotUniqueError is raised.

        :param root_group backend.preferences.model.group_item
        '''
        groups = []
        parent_id = root_group.parent.get()
        while parent_id is not None:
            parent_group = self.find_by_id(parent_id)
            if parent_group in groups:
                raise NotUniqueError(
                    'Parents is duplicate: %s'
                    % (parent_id,))
            groups.append(parent_group)
            parent_id = parent_group.parent.get()
        return groups

    def has_child_groups(self, group):
        '''
        Has Child Groups.

        Return True when another group references the given group as parent.

        :param group backend.preferences.model.group_item
        '''
        for group_item in self:
            if group_item.parent.get() == group.id.get():
                return True
        return False

    def initialize_global(self):
        '''
        Initialize Global.

        Create a new global item.
        '''
        root_group = self.find_root()
        global_group = self.item_model_class(self.get_owner())
        global_group.id.set(str(uuid.uuid4()))
        global_group.parent.set(root_group.id.get())
        global_group.weight.set(0)
        global_group.name.set(config.GLOBAL_GROUP_NAME)
        global_group.immutable.set(True)
        self.append(global_group)

    def initialize_group(self):
        '''
        Initialize Group.

        Create a new group item.
        '''
        root_group = self.find_root()
        new_group = self.item_model_class(self.get_owner())
        new_group.id.set(str(uuid.uuid4()))
        new_group.parent.set(root_group.id.get())
        new_group.weight.set(0)
        new_group.name.set(config.UNSET_GROUP_NAME)
        new_group.collapsed.set(False)
        return new_group

    def initialize_root(self):
        '''
        Initialize root.

        Create a new root item.
        '''
        root_group = self.item_model_class(self.get_owner())
        root_group.id.set(str(uuid.uuid4()))
        root_group.parent.set(None)
        root_group.weight.set(0)
        root_group.name.set(config.ROOT_GROUP_NAME)
        root_group.immutable.set(True)
        self.append(root_group)

    def verify(self):
        '''
        Verify Groups.

        Check that the root-group is available, create the group if not.
        Check that the global-group is available, create the group if not.
        Check that all other groups have a valid parent-to-root hierarchy.
        When a parent is missing it is replaced with the root_group.
        '''
        root_group = None
        try:
            root_group = self.find_root()
        except:
            self.initialize_root()
            root_group = self.find_root()

        try:
            self.find_global()
        except NotFoundError:
            self.initialize_global()

        # Re-associate groups with invalid parent with root_group
        for group in self:
            if group is root_group:
                continue
            parent = group.parent.get()
            try:
                self.find_by_id(parent)
            except NotFoundError:
                group.parent.set(root_group.id.get())
            try:
                self.get_parent_groups(group)
            except NotUniqueError:
                group.parent.set(root_group.id.get())

        # Re-sort every groups children
        for group in self:
            group_groups = []
            for group_group in self:
                if group_group.parent.get() != group.id.get():
                    continue
                group_groups.append(group_group)
            sorted_groups = sorted(
                group_groups, key=self._get_weight)
            for (weight, group_group) in enumerate(sorted_groups):
                group_group.weight.set((weight + 1) * 10)
