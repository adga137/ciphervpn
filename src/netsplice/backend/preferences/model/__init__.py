# -*- coding: utf-8 -*-
# model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Preferences. Defines how the user input is persisted
'''

from functools import cmp_to_key

from netsplice.backend.preferences.model.account_list import (
    account_list as account_list_model
)
from netsplice.backend.preferences.model.chain_list import (
    chain_list as chain_list_model
)
from netsplice.backend.preferences.model.grouped_account_list import (
    grouped_account_list as grouped_account_list_model
)
from netsplice.backend.preferences.model.group_list import (
    group_list as group_list_model
)
from netsplice.backend.preferences.model.override_list import (
    override_list as override_list_model
)
from netsplice.backend.preferences.model.ui import (
    ui as ui_model
)
from netsplice.backend.preferences.model.backend import (
    backend as backend_model
)
from netsplice.backend.preferences.model.request import (
    model as request_model
)
from netsplice.backend.preferences.model.response import (
    model as response_model
)
from netsplice.model.credential_list import (
    credential_list as credential_list_model
)
from netsplice.model.plugins import (
    plugins as plugins_model
)
from netsplice.util import get_logger
from netsplice.util.errors import (
    AccessError, NotFoundError
)
from netsplice.util.model.errors import ValidationError
from netsplice.util.model.field import field
from netsplice.util.model.marshalable_persistent import marshalable_persistent
from netsplice.util.model.marshalable_list_persistent import (
    marshalable_list_persistent
)

logger = get_logger()


class model(marshalable_persistent):
    def __init__(self):
        marshalable_persistent.__init__(
            self, None)

        # Not persisted
        self.credentials = credential_list_model()

        # Persisted
        self.accounts = account_list_model(self)
        self.backend = backend_model(self)
        self.chains = chain_list_model(self)
        self.groups = group_list_model(self)
        self.grouped_accounts = grouped_account_list_model(self)
        self.overrides = override_list_model(self)
        self.ui = ui_model(self)

        self.plugins = plugins_model(self, 'plugins')
        self.request = request_model()
        self.response = response_model()

    def commit_account(self, account_id):
        try:
            self.accounts.find_by_id(account_id).commit()
        except NotFoundError:
            self.accounts.remove_by_id(account_id)

        marshalable_persistent.commit(self)

    def commit_collection(self, name):
        '''
        Commit Collection.

        Commit the values of the collection and execute apply_values on it.
        '''
        self.__dict__[name].apply_values()
        self.__dict__[name].commit()
        marshalable_persistent.commit(self)

    def defaults(self):
        '''
        Set Defaults.

        Sets defaults that are available without any setting of the user.
        '''
        pass

    def get_all_accounts(self, reverse=False):
        '''
        Get All Accounts.

        Return all accounts in the order they need to be connected. The
        List may be reverted with the reverse flag for disconnecting.
        '''
        accounts = self.grouped_accounts.connection_order(
            self.groups.find_root().id.get(),
            self.groups)
        if reverse:
            accounts.reverse()
        return accounts

    def get_autostart_list(self):
        '''
        Get Autostart Accounts.

        Create a list of accounts that have the autoconnect flag.
        Find the accounts from the chains, put them in order by their group.
        '''

        autostart_accounts = []

        # collect all accounts that have the flag
        unordered_autostart_accounts = self.accounts.get_autostart_accounts()

        # sort them by the grouped_account order
        autostart_accounts = self.grouped_accounts.order(
            unordered_autostart_accounts, self.groups, self.chains)

        # remove accounts that will be connected through the chains anyway
        autostart_accounts = self.chains.forget_implicit_accounts(
            autostart_accounts, self.chains)

        # order with same parent by weight
        autostart_accounts = sorted(
            autostart_accounts,
            key=cmp_to_key(self.chains.cmp_by_chain_parent_weight))

        return autostart_accounts

    def load(self):
        '''
        Load.

        Load persistent models from disk.
        '''
        loaded_types = (
            marshalable_persistent, marshalable_list_persistent)
        try:
            migrated = False
            for attr in self.__dict__:
                try:
                    if not isinstance(self.__dict__[attr], loaded_types):
                        continue
                    self.__dict__[attr].load()
                    if self.__dict__[attr].get_migrated():
                        migrated = True
                except ValidationError as errors:
                    # only for read_array, read_*_instances read all instances
                    # and validate separately
                    logger.critical(str(errors))
                    continue
            if migrated:
                # store all properties
                for attr in self.__dict__:
                    if not isinstance(self.__dict__[attr], loaded_types):
                        continue
                    self.__dict__[attr].commit()
                for account in self.accounts:
                    self.commit_account(account.id.get())
        except OSError as errors:
            logger.critical(str(errors))
            raise AccessError('Preferences could not be read.')

        self.groups.verify()
        self.chains.verify(self.accounts)
        self.grouped_accounts.verify(self.accounts, self.groups, self.chains)
        self.backend.apply_values()
