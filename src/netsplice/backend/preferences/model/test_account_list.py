# -*- coding: utf-8 -*-
# test_account_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Accounts.

Checks preferences accounts model functions.
'''
import mock

from netsplice.backend.preferences.model.account_list import account_list
from netsplice.backend.preferences.model.request.account_create import (
    account_create as account_create_model
)
from netsplice.backend.preferences.model.request.account_update import (
    account_update as account_update_model
)
from netsplice.backend import connection as connection_module
from netsplice.util.errors import (
    NotFoundError, ConnectedError
)


ACCOUNT_ID = '31c5aa5a-d1df-4ee8-9ede-86898effe163'
OTHER_ACCOUNT_ID = '31c5aa5a-d1df-4ee8-9ede-86898effe164'
CONNECTION_ID = '1115aa5a-d1df-4ee8-9ede-86898effe163'


def get_connection_module():
    return connection_module


def get_test_object():
    return account_list(None)


class mock_app(object):
    def __init__(self):
        pass

    def get_privileged(self):
        return None

    def get_unprivileged(self):
        return None


def test_add_account_appends():
    a = get_test_object()
    i = a.item_model_class(None)
    i.id.set(ACCOUNT_ID)
    i.type.set('OpenVPN')
    i.name.set('Account')
    i.configuration.set('remote host port')
    i.import_configuration.set('remote imported port')
    i.enabled.set(True)
    i.default_route.set(False)
    i.autostart.set(True)
    assert(len(a) == 0)
    a.add_account(i)
    assert(len(a) == 1)


def test_add_account_uses_values():
    a = get_test_object()
    i = a.item_model_class(None)
    i.id.set(ACCOUNT_ID)
    i.type.set('OpenVPN')
    i.name.set('Account')
    i.configuration.set('remote host port')
    i.import_configuration.set('remote imported port')
    i.enabled.set(True)
    i.default_route.set(True)
    i.autostart.set(True)
    a.add_account(i)
    result = a[0]
    assert(result.id.get() == ACCOUNT_ID)
    assert(result.type.get() == 'OpenVPN')
    assert(result.name.get() == 'Account')
    assert(result.configuration.get() == 'remote host port')
    assert(result.import_configuration.get() == 'remote imported port')
    assert(result.enabled.get() is True)
    assert(result.default_route.get() is True)
    assert(result.autostart.get() is True)


def test_delete_account_raises_when_not_found():
    a = get_test_object()
    try:
        a.delete_account(ACCOUNT_ID, None)
        assert(False)  # Exception expected
    except NotFoundError:
        assert(True)


@mock.patch(
    'netsplice.backend.preferences.model.account_item.'
    'account_item.is_connected')
def test_delete_account_raises_when_connected(mock_is_connected):
    a = get_test_object()
    mock_is_connected.return_value = True
    i = a.item_model_class(None)
    i.id.set(ACCOUNT_ID)
    i.type.set('OpenVPN')
    i.name.set('Account')
    i.configuration.set('remote host port')
    i.import_configuration.set('remote imported port')
    i.enabled.set(True)
    i.default_route.set(False)
    i.autostart.set(True)
    a.add_account(i)
    try:
        a.delete_account(ACCOUNT_ID, None)
        assert(False)  # Exception expected
    except ConnectedError:
        assert(True)


@mock.patch(
    'netsplice.backend.preferences.model.account_item.'
    'account_item.is_connected')
def test_delete_account_removes_account_when_not_connected(mock_is_connected):
    a = get_test_object()
    mock_is_connected.return_value = False
    i = a.item_model_class(None)
    i.id.set(ACCOUNT_ID)
    i.type.set('OpenVPN')
    i.name.set('Account')
    i.configuration.set('remote host port')
    i.import_configuration.set('remote imported port')
    i.enabled.set(True)
    i.default_route.set(False)
    i.autostart.set(True)
    a.add_account(i)
    assert(len(a) == 1)
    a.delete_account(ACCOUNT_ID, None)
    assert(len(a) == 0)


def test_find_by_id_raises_notfound():
    a = get_test_object()
    i = a.item_model_class(None)
    i.id.set(ACCOUNT_ID)
    i.type.set('OpenVPN')
    i.name.set('Account')
    i.configuration.set('remote host port')
    i.import_configuration.set('remote imported port')
    i.enabled.set(True)
    i.default_route.set(False)
    i.autostart.set(True)
    a.add_account(i)
    try:
        a.find_by_id(OTHER_ACCOUNT_ID)
        assert(False)  # NotFoundError expected
    except NotFoundError:
        assert(True)


def test_find_by_id_returns_when_found():
    a = get_test_object()
    i = a.item_model_class(None)
    i.id.set(ACCOUNT_ID)
    i.type.set('OpenVPN')
    i.name.set('Account')
    i.configuration.set('remote host port')
    i.import_configuration.set('remote imported port')
    i.enabled.set(True)
    i.default_route.set(False)
    i.autostart.set(True)
    a.add_account(i)
    result = a.find_by_id(ACCOUNT_ID)
    assert(result.name.get() == 'Account')


def test_get_autostart_accounts_returns_empty():
    a = get_test_object()
    i = a.item_model_class(None)
    i.id.set(ACCOUNT_ID)
    i.type.set('OpenVPN')
    i.name.set('Account')
    i.configuration.set('remote host port')
    i.import_configuration.set('remote imported port')
    i.enabled.set(True)
    i.default_route.set(False)
    i.autostart.set(False)
    a.add_account(i)
    result = a.get_autostart_accounts()
    assert(len(result) == 0)


def test_get_autostart_accounts_returns_one():
    a = get_test_object()
    i = a.item_model_class(None)
    i.id.set(ACCOUNT_ID)
    i.type.set('OpenVPN')
    i.name.set('Account')
    i.configuration.set('remote host port')
    i.import_configuration.set('remote imported port')
    i.enabled.set(True)
    i.default_route.set(False)
    i.autostart.set(True)
    a.add_account(i)
    result = a.get_autostart_accounts()
    assert(len(result) == 1)


def test_new_account_creates_new_uuid():
    a = get_test_object()
    cai = account_create_model()
    cai.name.set('Account')
    cai.type.set('Tor')
    cai.configuration.set('remote mock')
    cai.import_configuration.set('remote import mock')
    cai.enabled.set(True)
    cai.default_route.set(True)
    cai.autostart.set(True)
    result = a.new_account(cai)
    assert(result.id.get() is not None)
    assert(result.type.get() == 'Tor')
    assert(result.name.get() == 'Account')
    assert(result.configuration.get() == 'remote mock')
    assert(result.import_configuration.get() == 'remote import mock')
    assert(result.enabled.get() is True)
    assert(result.default_route.get() is True)
    assert(result.autostart.get() is True)


@mock.patch(
    'netsplice.backend.preferences.model.account_item.'
    'account_item.is_connected')
def test_update_account_not_raises_when_connected(mock_is_connected):
    a = get_test_object()
    mock_is_connected.return_value = True
    i = a.item_model_class(None)
    i.id.set(ACCOUNT_ID)
    i.type.set('OpenVPN')
    i.name.set('Account')
    i.configuration.set('remote host port')
    i.import_configuration.set('remote imported port')
    i.enabled.set(True)
    i.default_route.set(False)
    i.autostart.set(True)
    a.add_account(i)
    try:
        a.update_account(i, None)
        assert(True)  # No Exception expected
    except:
        assert(False)  # NoException expected


def test_update_raises_when_not_found():
    a = get_test_object()
    u = account_update_model()
    u.id.set(OTHER_ACCOUNT_ID)
    u.name.set('UpdatedAccount')
    u.type.set('UpdatedType')
    u.configuration.set('remote updated')
    u.import_configuration.set('remote import updated')
    u.enabled.set(False)
    u.default_route.set(False)
    u.autostart.set(False)
    i = a.item_model_class(None)
    i.id.set(ACCOUNT_ID)
    i.type.set('OpenVPN')
    i.name.set('Account')
    i.configuration.set('remote host port')
    i.import_configuration.set('remote imported port')
    i.enabled.set(True)
    i.default_route.set(False)
    i.autostart.set(True)
    a.add_account(i)
    try:
        a.update_account(u, None)
        assert(False)  # NotFoundError expected
    except NotFoundError:
        assert(True)


@mock.patch(
    'netsplice.backend.preferences.model.account_item.'
    'account_item.is_connected')
def test_update_account_updates(mock_is_connected):
    a = get_test_object()
    mock_is_connected.return_value = False
    u = account_update_model()
    u.id.set(ACCOUNT_ID)
    u.name.set('UpdatedAccount')
    u.type.set('UpdatedType')
    u.configuration.set('remote updated')
    u.import_configuration.set('remote import updated')
    u.enabled.set(False)
    u.default_route.set(False)
    u.autostart.set(False)
    i = a.item_model_class(None)
    i.id.set(ACCOUNT_ID)
    i.type.set('OpenVPN')
    i.name.set('Account')
    i.configuration.set('remote host port')
    i.import_configuration.set('remote imported port')
    i.enabled.set(True)
    i.default_route.set(True)
    i.autostart.set(True)
    a.add_account(i)
    a.update_account(u, None)
    result = a[0]
    assert(result.type.get() == 'UpdatedType')
    assert(result.name.get() == 'UpdatedAccount')
    assert(result.configuration.get() == 'remote updated')
    assert(result.import_configuration.get() == 'remote import updated')
    assert(result.enabled.get() is False)
    assert(result.default_route.get() is False)
    assert(result.autostart.get() is False)
