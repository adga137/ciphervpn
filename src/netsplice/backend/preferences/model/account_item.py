# -*- coding: utf-8 -*-
# account_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for GUI process. Stores all information the GUI should know about
'''

from netsplice.config import model as config_model
from netsplice.util.errors import NotFoundError
from netsplice.util.model.field import field
from netsplice.util.model.marshalable_persistent import marshalable_persistent
from netsplice.model.validator.account_id import (
    account_id as account_id_validator
)
from netsplice.backend.preferences.model.validator.account_name import (
    account_name as account_name_validator
)
from netsplice.backend.preferences.model.validator.account_type import (
    account_type as account_type_validator
)
from netsplice.backend.preferences.model.validator.configuration import (
    configuration as configuration_validator
)
from netsplice.model.validator.boolean import (
    boolean as boolean_validator
)
from netsplice.model.validator.version import (
    version as version_validator
)


class account_item(marshalable_persistent):
    def __init__(self, owner):
        marshalable_persistent.__init__(self, owner)

        self.id = field(
            required=True,
            validators=[account_id_validator()])

        self.name = field(
            required=True,
            validators=[account_name_validator()])

        self.type = field(
            required=True,
            validators=[account_type_validator()])

        self.configuration = field(
            required=True,
            validators=[configuration_validator()])

        self.import_configuration = field(
            required=True,
            validators=[configuration_validator()])

        self.enabled = field(
            required=True,
            validators=[boolean_validator()])

        self.autostart = field(
            required=True,
            validators=[boolean_validator()])

        self.default_route = field(
            required=True,
            validators=[boolean_validator()])

        self.version = field(
            required=True,
            default=config_model.VERSION,
            validators=[version_validator()])

    def from_account(self, account_model_instance):
        '''
        From Account.

        Set values from an account_model_instance.
        '''
        self.id.set(account_model_instance.id.get())
        self.type.set(account_model_instance.type.get())
        self.name.set(account_model_instance.name.get())
        self.configuration.set(
            account_model_instance.configuration.get())
        self.import_configuration.set(
            account_model_instance.import_configuration.get())
        self.enabled.set(account_model_instance.enabled.get())
        self.default_route.set(account_model_instance.default_route.get())
        self.autostart.set(account_model_instance.autostart.get())

    def from_create_account(self, create_account_instance):
        '''
        From Create Account.

        Set values from a create_account_instance.
        '''
        self.name.set(create_account_instance.name.get())
        self.type.set(create_account_instance.type.get())
        self.configuration.set(
            create_account_instance.configuration.get())
        self.import_configuration.set(
            create_account_instance.import_configuration.get())
        self.enabled.set(create_account_instance.enabled.get())
        self.default_route.set(create_account_instance.default_route.get())
        self.autostart.set(create_account_instance.autostart.get())

    def from_update_account(self, update_account_instance):
        '''
        From update account.

        Set values from an update_account_instance.
        '''
        self.name.set(update_account_instance.name.get())
        self.type.set(update_account_instance.type.get())
        self.configuration.set(update_account_instance.configuration.get())
        self.import_configuration.set(
            update_account_instance.import_configuration.get())
        self.enabled.set(update_account_instance.enabled.get())
        self.default_route.set(update_account_instance.default_route.get())
        self.autostart.set(update_account_instance.autostart.get())

    def is_connected(self, connection_module):
        '''
        Is Connected.

        Check if the current account_item is in the given connection_module
        and the state is connected.
        Returns False when the account is not in the connection_module or
        the connected state is not a connected state.
        '''
        try:
            broker = connection_module.broker
            connection = broker.find_connection_by_account_id(
                self.id.get())
            if connection.in_connected():
                return True
        except NotFoundError:
            pass
        return False
