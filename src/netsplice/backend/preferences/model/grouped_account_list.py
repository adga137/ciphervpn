# -*- coding: utf-8 -*-
# grouped_account_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for grouped accounts.
'''

import sys


from netsplice.backend.preferences.model.grouped_account_item import (
    grouped_account_item as grouped_account_item_model
)
from netsplice.util.errors import NotFoundError
from netsplice.util.model.marshalable_list_persistent import (
    marshalable_list_persistent
)


class grouped_account_list(marshalable_list_persistent):
    def __init__(self, owner):
        marshalable_list_persistent.__init__(
            self, grouped_account_item_model, owner)

    def _get_weight(self, grouped_account_model_instance):
        '''
        Get Weight.

        Return weight for a group_account item for sorting.
        '''
        return grouped_account_model_instance.weight.get()

    def add_account(self, group_id, account_model_instance):
        '''
        Add Account.

        Add a grouped_account_item with values from the account_model_instance
        to the grouped_account_list with the group_id given.
        Set the weight of the new grouped_account_item to maximum, so it will
        be at the end of the list.
        '''
        new_grouped_account = self.item_model_class(self.get_owner())
        new_grouped_account.update(group_id, account_model_instance)
        new_grouped_account.weight.set(sys.maxsize)
        self.append(new_grouped_account)

    def apply_values(self):
        '''
        Apply Values.

        Apply values from the preferences to the config so other
        components can use the values.
        '''
        pass

    def connection_order(self, group_id, groups):
        '''
        Build a list of accounts that is ordered by the weight. For
        hierarchical groups the 'root' group accounts will be connected before
        the child group accounts.
        '''
        group = groups.find_by_id(group_id)
        child_groups = groups.get_child_groups(group)
        accounts = []
        try:
            group_accounts = self.find_by_group_id(group.id.get())
            sorted_group_accounts = sorted(
                group_accounts, key=self._get_weight)
            for account in sorted_group_accounts:
                if account.enabled.get():
                    accounts.append(account)
        except NotFoundError:
            # allow empty root groups
            pass

        # evaluate all child groups
        for child_group in child_groups:
            child_group_accounts = self.connection_order(
                child_group.id.get(), groups)
            for account in child_group_accounts:
                if account.enabled.get():
                    accounts.append(account)

        return accounts

    def delete_account(self, account_id):
        '''
        Delete Account.

        Remove the grouped_account for the given account_id.
        '''
        grouped_account = self.find_by_id(account_id)
        self.remove(grouped_account)

    def find_by_id(self, account_id):
        '''
        Find By Id.

        Find a grouped account with the given account_id.
        '''
        for grouped_account in self:
            if grouped_account.id.get() == account_id:
                return grouped_account
        raise NotFoundError(
            'No grouped_account with account %s found.' % (account_id,))

    def find_by_group_id(self, group_id):
        '''
        Find by Group Id.

        Return a list of grouped_accounts that have the given group_id.
        When no account with the group_id is found, a NotFoundError is raised.
        The returned list is sorted by the weight.
        '''
        grouped_accounts = []
        for grouped_account in self:
            if grouped_account.group_id.get() == group_id:
                grouped_accounts.append(grouped_account)
        if len(grouped_accounts) is 0:
            raise NotFoundError(
                'No grouped_account with group %s found.' % (group_id,))
        sorted_grouped_accounts = sorted(
            grouped_accounts, key=self._get_weight)
        return sorted_grouped_accounts

    def get_account_list(self, grouped_accounts, accounts):
        '''
        Get Account List.

        Return a list of account_items that match the ids in the
        grouped_accounts. This method operates on a given grouped_accounts
        list to allow sorting and filtering before getting the accounts.
        '''
        account_list = []
        for grouped_account in grouped_accounts:
            account_list.append(accounts.find_by_id(grouped_account.id.get()))
        return account_list

    def order(self, accounts, groups, chains):
        '''
        Order.

        Order the accounts in the given list by the grouped_account_weight.

        Arguments:
            accounts (list): list of accounts
            groups (list): list of groups (contains root)
            chains (list): list of chain items (contains account)
        '''
        ordered_grouped_accounts = self.connection_order(
            groups.find_root().id.get(),
            groups)
        ordered_accounts = []
        account_ids = []
        for account in accounts:
            account_ids.append(account.id.get())
        for grouped_account in ordered_grouped_accounts:
            for account in accounts:
                account_parents = chains.get_parents_for_id(
                    account.id.get())
                root_id = account.id.get()
                if len(account_parents) > 0:
                    root_id = account_parents[-1].id.get()
                if root_id != grouped_account.id.get():
                    continue
                ordered_accounts.append(account)
        return ordered_accounts

    def verify(self, accounts, groups, chains):
        '''
        Verify.

        Check that all accounts are referenced in the grouped_accounts.
        When an account is not referenced, link it to the global-group.

        Logger is not available when this function is called during startup
        use print.
        '''
        global_group = groups.find_global()
        # Add accounts to global group when they are not associated.
        for account in accounts:
            try:
                group_item = self.find_by_id(account.id.get())
                if group_item.name.get() != account.name.get():
                    group_item.name.set(account.name.get())
                group_item.enabled.set(account.enabled.get())
                group_item.type.set(account.type.get())
                continue
            except NotFoundError:
                # add accounts not in any group only when they are not
                # part of a chain.
                try:
                    chain = chains.find_by_id(account.id.get())
                    if chain.parent_id.get() is None:
                        self.add_account(
                            global_group.id.get(), account)
                except NotFoundError:
                    self.add_account(
                        global_group.id.get(), account)

        # Clean grouped accounts that have no account
        rm_grouped_accounts = []
        for grouped_account in self:
            try:
                accounts.find_by_id(grouped_account.id.get())
            except NotFoundError:
                rm_grouped_accounts.append(grouped_account)
                continue

            # Re-associate accounts to global group when group is gone
            try:
                groups.find_by_id(grouped_account.group_id.get())
            except NotFoundError:
                grouped_account.group_id.set(global_group.id.get())

            try:
                chain_item = chains.find_by_id(grouped_account.id.get())
                if chain_item.parent_id.get() is not None:
                    # item has a chain parent, do not associate to group
                    rm_grouped_accounts.append(grouped_account)
                    continue
            except NotFoundError:
                # item is not in a chain, it should not be in a grouped account
                rm_grouped_accounts.append(grouped_account)
                continue
        for grouped_account in rm_grouped_accounts:
            self.remove(grouped_account)

        for group in groups:
            # Correct Weight in groups
            group_accounts = []
            for grouped_account in self:
                if grouped_account.group_id.get() != group.id.get():
                    continue
                group_accounts.append(grouped_account)
            if len(group_accounts) == 0:
                continue
            sorted_group_accounts = sorted(
                group_accounts, key=self._get_weight)
            child_groups = groups.get_child_groups(group)
            for (weight, grouped_account) in enumerate(sorted_group_accounts):
                new_weight = (len(child_groups) + weight + 1) * 10
                grouped_account.weight.set(new_weight)
