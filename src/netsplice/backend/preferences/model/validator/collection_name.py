# -*- coding: utf-8 -*-
# collection_name.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Validator for preferences collection_names
'''

import re
from netsplice.util.model.validator import validator
from netsplice.util import basestring
from netsplice.util import get_logger

logger = get_logger()


class collection_name(validator):
    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        '''
        Collection names are nonempty lowercase strings that may contain
        underscores and numbers.
        '''
        if value is None:
            logger.error('Value is None but should not.')
            return False
        if not isinstance(value, (basestring)):
            logger.error('Value is not a string')
            return False
        rx_valid = re.compile('^([a-z0-9_]*)$')
        if re.match(rx_valid, value):
            return True
        return False
