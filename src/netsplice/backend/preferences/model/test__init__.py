# -*- coding: utf-8 -*-
# test_chain_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Model.

Checks preferences model functions.
'''
import mock
from netsplice.backend.preferences.model import model as model
from netsplice.util.errors import (
    NotFoundError
)


ACCOUNT_ID = 'aac5aa5a-d1df-4ee8-9ede-86898effe163'
OTHER_ACCOUNT_ID = 'bbc5aa5a-d1df-4ee8-9ede-86898effe164'
CONNECTION_ID = 'cccccccc-d1df-4ee8-9ede-86898effe163'
CHAINED_ACCOUNT_ID_ROOT = '31c5aa5a-d1df-4ee8-9ed0-86898effe163'
CHAINED_ACCOUNT_ID_L1 = '31c5aa5a-d1df-4ee8-9ed1-86898effe163'
CHAINED_ACCOUNT_ID_L2 = '31c5aa5a-d1df-4ee8-9ed2-86898effe163'
CHAINED_ACCOUNT_ID_L3 = '31c5aa5a-d1df-4ee8-9ed3-86898effe163'
FIRST_ACCOUNT_ID = '11c5aa5a-d1df-4ee8-9ede-86898effe163'
SECOND_ACCOUNT_ID = '22c5aa5a-d1df-4ee8-9ede-86898effe163'
THIRD_ACCOUNT_ID = '33c5aa5a-d1df-4ee8-9ede-86898effe163'
FOURTH_ACCOUNT_ID = '44c5aa5a-d1df-4ee8-9ede-86898effe163'
FIFTH_ACCOUNT_ID = '55c5aa5a-d1df-4ee8-9ede-86898effe163'
SIXTH_ACCOUNT_ID = '66c5aa5a-d1df-4ee8-9ede-86898effe163'
SEVENTH_ACCOUNT_ID = '77c5aa5a-d1df-4ee8-9ede-86898effe163'
EIGHT_ACCOUNT_ID = '88c5aa5a-d1df-4ee8-9ede-86898effe163'


def get_test_object():
    model_instance = model()
    return model_instance


def get_test_objects():
    model_instance = model()
    a1 = model_instance.accounts.item_model_class(None)
    a1.id.set(ACCOUNT_ID)
    a1.name.set('a1')
    a1.type.set('OpenVPN')
    a1.autostart.set(False)
    a1.enabled.set(True)
    a2 = model_instance.accounts.item_model_class(None)
    a2.id.set(OTHER_ACCOUNT_ID)
    a2.name.set('a2')
    a2.type.set('OpenVPN')
    a2.autostart.set(False)
    a2.enabled.set(True)
    a3 = model_instance.accounts.item_model_class(None)
    a3.id.set(THIRD_ACCOUNT_ID)
    a3.name.set('a3')
    a3.type.set('OpenVPN')
    a3.autostart.set(False)
    a3.enabled.set(True)

    a4 = model_instance.accounts.item_model_class(None)
    a4.id.set(FOURTH_ACCOUNT_ID)
    a4.name.set('a4')
    a4.type.set('OpenVPN')
    a4.autostart.set(False)
    a4.enabled.set(True)

    a5 = model_instance.accounts.item_model_class(None)
    a5.id.set(FIFTH_ACCOUNT_ID)
    a5.name.set('a5')
    a5.type.set('OpenVPN')
    a5.autostart.set(False)
    a5.enabled.set(True)

    model_instance.accounts.append(a1)
    model_instance.accounts.append(a2)
    model_instance.accounts.append(a3)
    model_instance.accounts.append(a4)
    model_instance.accounts.append(a5)
    verify_model(model_instance)
    return model_instance


def verify_model(model_instance):
    model_instance.groups.verify()
    model_instance.chains.verify(
        model_instance.accounts)
    model_instance.grouped_accounts.verify(
        model_instance.accounts, model_instance.groups, model_instance.chains)
    return model_instance


@mock.patch(
    'netsplice.util.model.marshalable_persistent.'
    'marshalable_persistent.commit')
def test_commit_account_calls_commit(mock_commit):
    m = get_test_object()
    m.commit_account(None)
    mock_commit.assert_called()


@mock.patch(
    'netsplice.backend.preferences.model.account_list.'
    'account_list.find_by_id')
def test_commit_account_calls_find_by_id(mock_find_by_id):
    m = get_test_object()
    m.commit_account(None)
    mock_find_by_id.assert_called()


@mock.patch(
    'netsplice.backend.preferences.model.account_list.'
    'account_list.remove_by_id')
@mock.patch(
    'netsplice.backend.preferences.model.account_list.'
    'account_list.find_by_id')
def test_commit_account_with_not_fount_calls_remove_by_id(
        mock_remove_by_id, mock_find_by_id):
    m = get_test_object()
    m.commit_account(None)
    mock_find_by_id.side_effect = NotFoundError('mock')
    mock_remove_by_id.assert_called()


def test_get_autostart_list_no_autostart_returns_empty():
    m = get_test_objects()
    result = m.get_autostart_list()
    assert(len(result) == 0)


def test_get_autostart_list_one_autostart_returns_one():
    m = get_test_objects()
    m.accounts.find_by_id(ACCOUNT_ID).autostart.set(True)
    verify_model(m)
    result = m.get_autostart_list()
    assert(len(result) == 1)
    assert(result[0].id.get() == ACCOUNT_ID)


def test_get_autostart_list_two_autostart_returns_two():
    m = get_test_objects()
    m.accounts.find_by_id(ACCOUNT_ID).autostart.set(True)
    m.accounts.find_by_id(OTHER_ACCOUNT_ID).autostart.set(True)
    verify_model(m)
    result = m.get_autostart_list()
    assert(len(result) == 2)
    assert(result[0].id.get() == ACCOUNT_ID)
    assert(result[1].id.get() == OTHER_ACCOUNT_ID)


def test_get_autostart_list_two_autostart_returns_two_correct_weight():
    m = get_test_objects()
    m.accounts.find_by_id(ACCOUNT_ID).autostart.set(True)
    m.grouped_accounts.find_by_id(ACCOUNT_ID).weight.set(20)
    m.accounts.find_by_id(OTHER_ACCOUNT_ID).autostart.set(True)
    m.grouped_accounts.find_by_id(OTHER_ACCOUNT_ID).weight.set(10)
    verify_model(m)
    print(m.grouped_accounts.to_json())
    result = m.get_autostart_list()
    for account in result:
        print(account.name.get(), account.id.get())
    assert(len(result) == 2)
    assert(result[0].id.get() == OTHER_ACCOUNT_ID)
    assert(result[1].id.get() == ACCOUNT_ID)


def test_get_autostart_list_three_autostart_returns_three_correct_weight():
    m = get_test_objects()
    m.accounts.find_by_id(ACCOUNT_ID).autostart.set(True)
    m.grouped_accounts.find_by_id(ACCOUNT_ID).weight.set(20)
    m.accounts.find_by_id(OTHER_ACCOUNT_ID).autostart.set(True)
    m.grouped_accounts.find_by_id(OTHER_ACCOUNT_ID).weight.set(10)
    m.accounts.find_by_id(THIRD_ACCOUNT_ID).autostart.set(True)
    m.grouped_accounts.find_by_id(THIRD_ACCOUNT_ID).weight.set(30)
    verify_model(m)
    print(m.grouped_accounts.to_json())
    result = m.get_autostart_list()
    for account in result:
        print(account.name.get(), account.id.get())
    assert(len(result) == 3)
    assert(result[0].id.get() == OTHER_ACCOUNT_ID)
    assert(result[1].id.get() == ACCOUNT_ID)
    assert(result[2].id.get() == THIRD_ACCOUNT_ID)


def test_get_autostart_list_chained_autostart_returns_chained():
    m = get_test_objects()
    m.accounts.find_by_id(ACCOUNT_ID).autostart.set(True)
    m.accounts.find_by_id(OTHER_ACCOUNT_ID).autostart.set(True)
    m.accounts.find_by_id(THIRD_ACCOUNT_ID).autostart.set(True)
    del m.grouped_accounts[:]
    m.chains.find_by_id(OTHER_ACCOUNT_ID).parent_id.set(ACCOUNT_ID)
    verify_model(m)
    print(m.grouped_accounts.to_json())
    result = m.get_autostart_list()
    for account in result:
        print(account.name.get(), account.id.get())
    assert(len(result) == 2)
    assert(result[0].id.get() == OTHER_ACCOUNT_ID)
    assert(result[1].id.get() == THIRD_ACCOUNT_ID)


def test_get_autostart_list_chained_same_parent_autostart_returns_chained():
    '''
    a1*
      a2*
      a3*
    '''
    m = get_test_objects()
    m.accounts.find_by_id(ACCOUNT_ID).autostart.set(True)
    m.accounts.find_by_id(OTHER_ACCOUNT_ID).autostart.set(True)
    m.accounts.find_by_id(THIRD_ACCOUNT_ID).autostart.set(True)
    del m.grouped_accounts[:]
    m.chains.find_by_id(OTHER_ACCOUNT_ID).parent_id.set(ACCOUNT_ID)
    m.chains.find_by_id(OTHER_ACCOUNT_ID).weight.set(0)
    m.chains.find_by_id(THIRD_ACCOUNT_ID).parent_id.set(ACCOUNT_ID)
    m.chains.find_by_id(THIRD_ACCOUNT_ID).weight.set(10)
    verify_model(m)
    result = m.get_autostart_list()
    for account in result:
        print(account.name.get(), account.id.get())
    assert(len(result) == 2)
    assert(result[0].id.get() == OTHER_ACCOUNT_ID)
    assert(result[1].id.get() == THIRD_ACCOUNT_ID)


def test_get_autostart_list_chained_weighted_same_parent_autostart_returns():
    m = get_test_objects()
    m.accounts.find_by_id(ACCOUNT_ID).autostart.set(True)

    m.accounts.find_by_id(OTHER_ACCOUNT_ID).autostart.set(True)

    m.accounts.find_by_id(THIRD_ACCOUNT_ID).autostart.set(True)
    del m.grouped_accounts[:]
    m.chains.find_by_id(THIRD_ACCOUNT_ID).parent_id.set(ACCOUNT_ID)
    m.chains.find_by_id(THIRD_ACCOUNT_ID).weight.set(10)
    m.chains.find_by_id(OTHER_ACCOUNT_ID).parent_id.set(ACCOUNT_ID)
    m.chains.find_by_id(OTHER_ACCOUNT_ID).weight.set(20)
    verify_model(m)
    print(m.grouped_accounts.to_json())
    result = m.get_autostart_list()
    for account in result:
        print(account.name.get(), account.id.get())
    assert(len(result) == 2)
    assert(result[0].id.get() == THIRD_ACCOUNT_ID)
    assert(result[1].id.get() == OTHER_ACCOUNT_ID)


def test_get_autostart_list_chained_nested_autostart_returns_deep():
    '''
    a1*
    a2*
      a3
        a4
          a5*
    '''
    m = get_test_objects()
    m.accounts.find_by_id(ACCOUNT_ID).autostart.set(True)

    m.accounts.find_by_id(OTHER_ACCOUNT_ID).autostart.set(True)

    m.accounts.find_by_id(FIFTH_ACCOUNT_ID).autostart.set(True)
    del m.grouped_accounts[:]
    m.chains.find_by_id(ACCOUNT_ID).parent_id.set(None)
    m.chains.find_by_id(THIRD_ACCOUNT_ID).parent_id.set(OTHER_ACCOUNT_ID)
    m.chains.find_by_id(FOURTH_ACCOUNT_ID).parent_id.set(THIRD_ACCOUNT_ID)
    m.chains.find_by_id(FIFTH_ACCOUNT_ID).parent_id.set(FOURTH_ACCOUNT_ID)
    verify_model(m)
    print(m.grouped_accounts.to_json())
    result = m.get_autostart_list()
    for account in result:
        print(account.name.get(), account.id.get())
    assert(len(result) == 2)
    assert(result[0].id.get() == ACCOUNT_ID)
    assert(result[1].id.get() == FIFTH_ACCOUNT_ID)


def test_get_autostart_list_chained_nested_autostart_returns_ordered():
    '''
    a1*
    a2
      a3*
      a4
        a5*
    '''
    m = get_test_objects()
    m.accounts.find_by_id(ACCOUNT_ID).autostart.set(True)

    m.accounts.find_by_id(THIRD_ACCOUNT_ID).autostart.set(True)
    m.accounts.find_by_id(FIFTH_ACCOUNT_ID).autostart.set(True)
    del m.grouped_accounts[:]
    m.chains.find_by_id(OTHER_ACCOUNT_ID).parent_id.set(None)
    m.chains.find_by_id(THIRD_ACCOUNT_ID).parent_id.set(OTHER_ACCOUNT_ID)
    m.chains.find_by_id(THIRD_ACCOUNT_ID).weight.set(10)
    m.chains.find_by_id(FOURTH_ACCOUNT_ID).parent_id.set(OTHER_ACCOUNT_ID)
    m.chains.find_by_id(FOURTH_ACCOUNT_ID).weight.set(20)
    m.chains.find_by_id(FIFTH_ACCOUNT_ID).parent_id.set(FOURTH_ACCOUNT_ID)
    verify_model(m)
    print(m.grouped_accounts.to_json())

    result = m.get_autostart_list()
    for account in result:
        print(account.name.get(), account.id.get())
    assert(len(result) == 3)
    assert(result[0].id.get() == ACCOUNT_ID)
    assert(result[1].id.get() == THIRD_ACCOUNT_ID)
    assert(result[2].id.get() == FIFTH_ACCOUNT_ID)
