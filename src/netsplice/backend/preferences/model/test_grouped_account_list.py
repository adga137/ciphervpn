# -*- coding: utf-8 -*-
# test_grouped_account_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Grouped Account.

Checks preferences model methods.
'''

from netsplice.backend.preferences.model.grouped_account_list import (
    grouped_account_list
)
from netsplice.backend.preferences.model.account_item import (
    account_item as account_item_model
)
from netsplice.backend.preferences.model.account_list import (
    account_list as account_list_model
)
from netsplice.backend.preferences.model.chain_list import (
    chain_list as chain_list_model
)
from netsplice.backend.preferences.model.group_list import (
    group_list as group_list_model
)
from netsplice.util.errors import (
    NotFoundError
)


ACCOUNT_ID = '11c5aa5a-d1df-4ee8-9ede-86898effe163'
OTHER_ACCOUNT_ID = '11c5aa5a-d1df-4ee8-9ede-86898effe164'
OTHER_OTHER_ACCOUNT_ID = '11c5aa5a-d1df-4ee8-9ede-86898effe165'
GROUP_ID = '31c5aa5a-d1df-4ee8-9ede-86898effe163'
OTHER_GROUP_ID = '31c5aa5a-d1df-4ee8-9ede-86898effe164'


def get_test_object():
    return grouped_account_list(None)


def test_add_account_appends():
    a = get_test_object()
    i = account_item_model(None)
    i.id.set(ACCOUNT_ID)
    i.type.set('OpenVPN')
    i.name.set('Account')
    i.configuration.set('remote host port')
    i.import_configuration.set('remote imported port')
    i.enabled.set(True)
    i.default_route.set(False)
    i.autostart.set(True)
    assert(len(a) == 0)
    a.add_account(GROUP_ID, i)
    assert(len(a) == 1)


def test_connection_order_raises_notfound_when_groupid_not_exists():
    a = get_test_object()
    empty_groups = group_list_model(None)
    try:
        a.connection_order(GROUP_ID, empty_groups)
        assert(False)  # NotFoundError expected
    except NotFoundError:
        assert(True)


def test_connection_order_returns_empty_when_not_available():
    a = get_test_object()
    groups = group_list_model(None)
    gi = groups.item_model_class(None)
    gi.id.set(GROUP_ID)
    groups.append(gi)
    result = a.connection_order(GROUP_ID, groups)
    assert(len(result) == 0)


def test_connection_order_return_accounts_from_group():
    a = get_test_object()
    ai1 = a.item_model_class(None)
    ai1.id.set(ACCOUNT_ID)
    ai1.group_id.set(GROUP_ID)
    ai1.enabled.set(True)
    ai2 = a.item_model_class(None)
    ai2.id.set(OTHER_ACCOUNT_ID)
    ai2.group_id.set(GROUP_ID)
    ai2.enabled.set(True)
    a.append(ai1)
    a.append(ai2)
    groups = group_list_model(None)
    gi = groups.item_model_class(None)
    gi.id.set(GROUP_ID)
    groups.append(gi)
    result = a.connection_order(GROUP_ID, groups)
    assert(len(result) == 2)


def test_connection_order_return_only_enabled_accounts_from_group():
    a = get_test_object()
    ai1 = a.item_model_class(None)
    ai1.id.set(ACCOUNT_ID)
    ai1.group_id.set(GROUP_ID)
    ai1.enabled.set(True)
    ai2 = a.item_model_class(None)
    ai2.id.set(OTHER_ACCOUNT_ID)
    ai2.group_id.set(GROUP_ID)
    ai2.enabled.set(False)
    a.append(ai1)
    a.append(ai2)
    groups = group_list_model(None)
    gi = groups.item_model_class(None)
    gi.id.set(GROUP_ID)
    groups.append(gi)
    result = a.connection_order(GROUP_ID, groups)
    assert(len(result) == 1)
    assert(result[0].id.get() == ACCOUNT_ID)


def test_connection_order_return_accounts_from_child_group():
    a = get_test_object()
    ai1 = a.item_model_class(None)
    ai1.id.set(ACCOUNT_ID)
    ai1.group_id.set(OTHER_GROUP_ID)
    ai1.enabled.set(True)
    ai2 = a.item_model_class(None)
    ai2.id.set(OTHER_ACCOUNT_ID)
    ai2.group_id.set(OTHER_GROUP_ID)
    ai2.enabled.set(True)
    a.append(ai1)
    a.append(ai2)
    groups = group_list_model(None)
    gi = groups.item_model_class(None)
    gi.id.set(GROUP_ID)
    gc = groups.item_model_class(None)
    gc.id.set(OTHER_GROUP_ID)
    gc.parent.set(GROUP_ID)
    groups.append(gi)
    groups.append(gc)
    result = a.connection_order(GROUP_ID, groups)
    assert(len(result) == 2)


def test_connection_order_return_enabled_accounts_from_child_group():
    a = get_test_object()
    ai1 = a.item_model_class(None)
    ai1.id.set(ACCOUNT_ID)
    ai1.group_id.set(OTHER_GROUP_ID)
    ai1.enabled.set(True)
    ai2 = a.item_model_class(None)
    ai2.id.set(OTHER_ACCOUNT_ID)
    ai2.group_id.set(OTHER_GROUP_ID)
    ai2.enabled.set(False)
    a.append(ai1)
    a.append(ai2)
    groups = group_list_model(None)
    gi = groups.item_model_class(None)
    gi.id.set(GROUP_ID)
    gc = groups.item_model_class(None)
    gc.id.set(OTHER_GROUP_ID)
    gc.parent.set(GROUP_ID)
    groups.append(gi)
    groups.append(gc)
    result = a.connection_order(GROUP_ID, groups)
    assert(len(result) == 1)
    assert(result[0].id.get() == ACCOUNT_ID)


def test_delete_account_raises_notfound():
    a = get_test_object()
    ai1 = a.item_model_class(None)
    ai1.id.set(ACCOUNT_ID)
    ai1.group_id.set(OTHER_GROUP_ID)
    ai1.enabled.set(True)
    a.append(ai1)
    try:
        a.delete_account(OTHER_ACCOUNT_ID)
        assert(False)  # NotFoundError expected
    except NotFoundError:
        assert(True)


def test_delete_account_removes():
    a = get_test_object()
    ai1 = a.item_model_class(None)
    ai1.id.set(ACCOUNT_ID)
    ai1.group_id.set(OTHER_GROUP_ID)
    ai1.enabled.set(True)
    ai2 = a.item_model_class(None)
    ai2.id.set(OTHER_ACCOUNT_ID)
    ai2.group_id.set(OTHER_GROUP_ID)
    ai2.enabled.set(False)
    a.append(ai1)
    a.append(ai2)
    assert(len(a) == 2)
    a.delete_account(OTHER_ACCOUNT_ID)
    assert(len(a) == 1)
    assert(a[0].id.get() == ACCOUNT_ID)


def test_find_by_id_raises_notfound():
    a = get_test_object()
    ai1 = a.item_model_class(None)
    ai1.id.set(ACCOUNT_ID)
    ai1.group_id.set(OTHER_GROUP_ID)
    ai1.enabled.set(True)
    a.append(ai1)
    try:
        a.find_by_id(OTHER_ACCOUNT_ID)
        assert(False)  # NotFoundError expected
    except NotFoundError:
        assert(True)


def test_find_by_id_returns_grouped_account():
    a = get_test_object()
    ai1 = a.item_model_class(None)
    ai1.id.set(ACCOUNT_ID)
    ai1.group_id.set(OTHER_GROUP_ID)
    ai1.enabled.set(True)
    a.append(ai1)
    result = a.find_by_id(ACCOUNT_ID)
    assert(isinstance(result, (a.item_model_class,)))


def test_find_by_group_id_raises_notfound():
    a = get_test_object()
    ai1 = a.item_model_class(None)
    ai1.id.set(ACCOUNT_ID)
    ai1.group_id.set(GROUP_ID)
    ai1.enabled.set(True)
    a.append(ai1)
    try:
        a.find_by_group_id(OTHER_GROUP_ID)
        assert(False)  # NotFoundError expected
    except NotFoundError:
        assert(True)


def test_find_by_group_id_returns_list():
    a = get_test_object()
    ai1 = a.item_model_class(None)
    ai1.id.set(ACCOUNT_ID)
    ai1.group_id.set(GROUP_ID)
    ai1.enabled.set(True)
    a.append(ai1)
    result = a.find_by_group_id(GROUP_ID)
    assert(isinstance(result, (list,)))
    assert(len(result) == 1)


def test_find_by_group_id_returns_sorted_list():
    a = get_test_object()
    ai1 = a.item_model_class(None)
    ai1.id.set(ACCOUNT_ID)
    ai1.group_id.set(GROUP_ID)
    ai1.enabled.set(True)
    ai1.weight.set(20)
    a.append(ai1)
    ai2 = a.item_model_class(None)
    ai2.id.set(OTHER_ACCOUNT_ID)
    ai2.group_id.set(GROUP_ID)
    ai2.enabled.set(True)
    ai2.weight.set(30)
    a.append(ai2)
    ai3 = a.item_model_class(None)
    ai3.id.set(OTHER_OTHER_ACCOUNT_ID)
    ai3.group_id.set(GROUP_ID)
    ai3.enabled.set(True)
    ai3.weight.set(10)
    a.append(ai3)
    result = a.find_by_group_id(GROUP_ID)
    assert(isinstance(result, (list,)))
    assert(len(result) == 3)
    assert(result[0].weight.get() == 10)
    assert(result[1].weight.get() == 20)
    assert(result[2].weight.get() == 30)


def test_get_account_list_raises_when_account_list_out_of_sync():
    a = get_test_object()
    gal = []
    al = account_list_model(None)
    ai1 = al.item_model_class(None)
    ai1.id.set(ACCOUNT_ID)
    gai1 = a.item_model_class(None)
    gai1.id.set(ACCOUNT_ID)
    gal.append(gai1)
    gai2 = a.item_model_class(None)
    gai2.id.set(OTHER_ACCOUNT_ID)
    gal.append(gai2)
    try:
        a.get_account_list(gal, al)
        # al does not contain OTHER_ACCOUNT_ID
        assert(False)  # NotFoundError expected
    except NotFoundError:
        assert(True)


def test_get_account_list_returns_list_of_account_item():
    a = get_test_object()
    gal = []
    al = account_list_model(None)
    ai1 = al.item_model_class(None)
    ai1.id.set(ACCOUNT_ID)
    al.append(ai1)
    ai2 = al.item_model_class(None)
    ai2.id.set(OTHER_ACCOUNT_ID)
    al.append(ai2)
    gai1 = a.item_model_class(None)
    gai1.id.set(ACCOUNT_ID)
    gal.append(gai1)
    gai2 = a.item_model_class(None)
    gai2.id.set(OTHER_ACCOUNT_ID)
    gal.append(gai2)
    result = a.get_account_list(gal, al)
    assert(len(result) == 2)
    assert(isinstance(result[0], (al.item_model_class,)))
    assert(isinstance(result[1], (al.item_model_class,)))


def test_order_returns_basic():
    a = get_test_object()
    groups = group_list_model(None)
    groups.initialize_root()
    groups.initialize_global()
    chains = chain_list_model(None)
    accounts = []
    chains.verify(accounts)
    a.verify(accounts, groups, chains)
    a.order(accounts, groups, chains)
    assert(len(accounts) is 0)
    accounts = account_list_model(None)
    account = accounts.item_model_class(None)
    account.id.set(ACCOUNT_ID)
    account.name.set('account')
    account.type.set('OpenVPN')
    account.enabled.set(True)
    accounts.append(account)
    chains.verify(accounts)
    a.verify(accounts, groups, chains)
    result = a.order(accounts, groups, chains)
    assert(len(result) is 1)


def test_order_reorders():
    a = get_test_object()
    groups = group_list_model(None)
    groups.initialize_root()
    groups.initialize_global()
    chains = chain_list_model(None)
    accounts = [account_item_model(None), account_item_model(None)]
    accounts = account_list_model(None)
    account = accounts.item_model_class(None)
    account.id.set(ACCOUNT_ID)
    account.name.set('account1')
    account.type.set('OpenVPN')
    account.enabled.set(True)
    accounts.append(account)
    account = accounts.item_model_class(None)
    account.id.set(OTHER_ACCOUNT_ID)
    account.name.set('account2')
    account.type.set('OpenVPN')
    account.enabled.set(True)
    accounts.append(account)
    chains.verify(accounts)
    a.verify(accounts, groups, chains)
    a.find_by_id(ACCOUNT_ID).weight.set(10)
    a.find_by_id(OTHER_ACCOUNT_ID).weight.set(20)
    result = a.order(accounts, groups, chains)
    assert(len(result) is 2)


def test_verify_raises_without_global_group():
    a = get_test_object()
    al = account_list_model(None)
    g = group_list_model(None)
    c = chain_list_model(None)
    g.initialize_root()
    try:
        a.verify(al, g, c)
        assert(False)  # NotFoundError expected because no group is available
    except NotFoundError:
        assert(True)


def test_verify_accounts_are_added_to_global_group():
    a = get_test_object()
    al = account_list_model(None)
    c = chain_list_model(None)
    g = group_list_model(None)
    g.initialize_root()
    g.initialize_global()
    ai = account_item_model(None)
    ai.id.set(ACCOUNT_ID)
    ai.name.set('Account')
    ai.type.set('Mock')
    ai.enabled.set(True)
    al.append(ai)
    cai = c.item_model_class(None)
    cai.id.set(ACCOUNT_ID)
    cai.parent_id.set(None)
    c.append(cai)
    assert(len(a) == 0)
    a.verify(al, g, c)
    assert(len(a) == 1)
    assert(a[0].group_id.get() == g.find_global().id.get())


def test_verify_grouped_account_list_are_updated():
    a = get_test_object()
    al = account_list_model(None)
    c = chain_list_model(None)
    g = group_list_model(None)
    g.initialize_root()
    g.initialize_global()
    ai = account_item_model(None)
    ai.id.set(ACCOUNT_ID)
    ai.name.set('Account')
    ai.type.set('Mock')
    ai.enabled.set(True)
    al.append(ai)
    gai = a.item_model_class(None)
    gai.id.set(ACCOUNT_ID)
    gai.name.set('overwrite')
    gai.type.set('overwrite')
    gai.enabled.set(False)
    cai = c.item_model_class(None)
    cai.id.set(ACCOUNT_ID)
    cai.parent_id.set(None)
    c.append(cai)
    a.append(gai)
    a.verify(al, g, c)
    assert(len(a) == 1)
    assert(a[0].name.get() == 'Account')
    assert(a[0].enabled.get() is True)
    assert(a[0].type.get() == 'Mock')


def test_verify_grouped_account_list_are_removed_when_no_account():
    a = get_test_object()
    al = account_list_model(None)
    g = group_list_model(None)
    c = chain_list_model(None)
    g.initialize_root()
    g.initialize_global()
    gai = a.item_model_class(None)
    gai.id.set(ACCOUNT_ID)
    gai.name.set('remove')
    gai.type.set('remove')
    gai.enabled.set(False)
    a.append(gai)
    cai = c.item_model_class(None)
    cai.id.set(ACCOUNT_ID)
    cai.parent_id.set(None)
    c.append(cai)
    assert(len(a) == 1)
    a.verify(al, g, c)
    assert(len(a) == 0)


def test_verify_grouped_account_list_regrouped_when_no_group():
    a = get_test_object()
    al = account_list_model(None)
    ai = account_item_model(None)
    c = chain_list_model(None)
    ai.id.set(ACCOUNT_ID)
    ai.name.set('Account')
    ai.type.set('Mock')
    ai.enabled.set(True)
    al.append(ai)
    g = group_list_model(None)
    g.initialize_root()
    g.initialize_global()
    gai = a.item_model_class(None)
    gai.id.set(ACCOUNT_ID)
    gai.group_id.set(GROUP_ID)
    gai.name.set('ungrouped')
    gai.type.set('Mock')
    gai.enabled.set(False)
    cai = c.item_model_class(None)
    cai.id.set(ACCOUNT_ID)
    cai.parent_id.set(None)
    c.append(cai)
    a.append(gai)
    a.verify(al, g, c)
    assert(len(a) == 1)
    assert(a[0].group_id.get() != GROUP_ID)
    assert(a[0].group_id.get() == g.find_global().id.get())


def test_verify_grouped_account_list_removed_when_in_chain_with_parent():
    a = get_test_object()
    al = account_list_model(None)
    ai = account_item_model(None)
    c = chain_list_model(None)
    ai.id.set(ACCOUNT_ID)
    ai.name.set('Account')
    ai.type.set('Mock')
    ai.enabled.set(True)
    al.append(ai)
    g = group_list_model(None)
    g.initialize_root()
    g.initialize_global()

    gai = a.item_model_class(None)
    gai.id.set(ACCOUNT_ID)
    gai.group_id.set(GROUP_ID)
    gai.name.set('ungrouped')
    gai.type.set('Mock')
    gai.enabled.set(False)
    a.append(gai)
    cai = c.item_model_class(None)
    cai.id.set(ACCOUNT_ID)
    cai.parent_id.set(OTHER_ACCOUNT_ID)
    c.append(cai)
    assert(len(a) == 1)
    a.verify(al, g, c)
    assert(len(a) == 0)
