# -*- coding: utf-8 -*-
# test_chain_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Chain Lists.

Checks preferences chains model functions.
'''
import mock
from netsplice.backend.connection.chain import chain as connection_chain
from netsplice.backend.preferences.model.chain_list import chain_list
from netsplice.util.errors import (
    NotFoundError
)


ACCOUNT_ID = 'aac5aa5a-d1df-4ee8-9ede-86898effe163'
OTHER_ACCOUNT_ID = 'bbc5aa5a-d1df-4ee8-9ede-86898effe164'
CONNECTION_ID = 'cccccccc-d1df-4ee8-9ede-86898effe163'
CHAINED_ACCOUNT_ID_ROOT = '31c5aa5a-d1df-4ee8-9ed0-86898effe163'
CHAINED_ACCOUNT_ID_L1 = '31c5aa5a-d1df-4ee8-9ed1-86898effe163'
CHAINED_ACCOUNT_ID_L2 = '31c5aa5a-d1df-4ee8-9ed2-86898effe163'
CHAINED_ACCOUNT_ID_L3 = '31c5aa5a-d1df-4ee8-9ed3-86898effe163'
FIRST_ACCOUNT_ID = '11c5aa5a-d1df-4ee8-9ede-86898effe163'
SECOND_ACCOUNT_ID = '22c5aa5a-d1df-4ee8-9ede-86898effe163'
THIRD_ACCOUNT_ID = '33c5aa5a-d1df-4ee8-9ede-86898effe163'
FOURTH_ACCOUNT_ID = '44c5aa5a-d1df-4ee8-9ede-86898effe163'
FIFTH_ACCOUNT_ID = '55c5aa5a-d1df-4ee8-9ede-86898effe163'
SIXTH_ACCOUNT_ID = '66c5aa5a-d1df-4ee8-9ede-86898effe163'
SEVENTH_ACCOUNT_ID = '77c5aa5a-d1df-4ee8-9ede-86898effe163'
EIGHT_ACCOUNT_ID = '88c5aa5a-d1df-4ee8-9ede-86898effe163'


class mock_account_list(list):
    def __init__(self):
        pass

    def find_by_id(self, id):
        for item in self:
            if item.id.get() == id:
                return item
        raise NotFoundError('%s not found' % (id,))


def get_test_objects(account_id_list=[]):
    chain = chain_list(None)
    accounts = mock_account_list()
    for account_id in account_id_list:
        chain_item = chain.item_model_class(None)
        chain_item.id.set(account_id)
        chain.append(chain_item)
        accounts.append(chain_item)
    return (chain, accounts)


class mock_app(object):
    def __init__(self):
        pass

    def get_privileged(self):
        return None

    def get_unprivileged(self):
        return None


def test_add_account_appends():
    (a, accounts) = get_test_objects()
    p = a.item_model_class(None)
    i = a.item_model_class(None)
    p.id.set(OTHER_ACCOUNT_ID)
    i.id.set(ACCOUNT_ID)
    # i.enabled.set(True)
    # i.autostart.set(True)
    assert(len(a) == 0)
    a.add_account(None, p)
    a.add_account(OTHER_ACCOUNT_ID, i)
    assert(len(a) == 2)


@mock.patch(
    'netsplice.backend.preferences.model.chain_list.'
    'chain_list.find_by_id')
@mock.patch(
    'netsplice.backend.preferences.model.chain_list.'
    'chain_list.remove')
def test_delete_account_calls_methods(mock_find_by_id, mock_remove):
    (a, accounts) = get_test_objects()
    a.delete_account(OTHER_ACCOUNT_ID)
    mock_find_by_id.assert_called()
    mock_remove.assert_called()


def test_find_by_id_with_existing_returns_existing():
    (a, accounts) = get_test_objects([ACCOUNT_ID, OTHER_ACCOUNT_ID])

    result = a.find_by_id(ACCOUNT_ID)
    assert(result.id.get() == ACCOUNT_ID)


def test_find_by_parent_id_with_existing_returns_existing():
    (a, accounts) = get_test_objects([ACCOUNT_ID, OTHER_ACCOUNT_ID])
    a[0].parent_id.set(OTHER_ACCOUNT_ID)

    result = a.find_by_parent_id(OTHER_ACCOUNT_ID)
    assert(result[0].id.get() == ACCOUNT_ID)


def test_find_by_id_without_existing_throws_exception():
    (a, accounts) = get_test_objects([ACCOUNT_ID, OTHER_ACCOUNT_ID])
    a[0].parent_id.set(OTHER_ACCOUNT_ID)
    try:
        a.find_by_parent_id(ACCOUNT_ID)
        assert(False)  # NotFoundError expected
    except NotFoundError:
        assert(True)  # Expected error thrown


@mock.patch(
    'netsplice.backend.preferences.model.account_list.'
    'account_list')
def test_get_account_list_with_list_returns_instance_from_accounts(
        mock_account_list):
    (a, accounts) = get_test_objects([ACCOUNT_ID])
    al = mock_account_list
    al.find_by_id.return_value = 'instance'
    chain_accounts = [a[0]]
    result = a.get_account_list(chain_accounts, al)
    assert(result[0] == 'instance')


@mock.patch(
    'netsplice.backend.preferences.model.account_list.'
    'account_list')
def test_get_account_list_without_list_returns_empty(
        mock_account_list):
    (a, accounts) = get_test_objects()
    al = mock_account_list
    al.find_by_id.return_value = 'instance'
    chain_accounts = []
    result = a.get_account_list(chain_accounts, al)
    assert(result == [])


@mock.patch(
    'netsplice.backend.preferences.model.account_list.'
    'account_list')
def test_get_account_list_with_bad_chain_id_raises_NotFoundError(
        mock_account_list):
    (a, accounts) = get_test_objects([ACCOUNT_ID])
    al = mock_account_list
    al.find_by_id.side_effect = NotFoundError('test')
    chain_accounts = [a[0]]
    try:
        a.get_account_list(chain_accounts, al)
        assert(False)  # expect NotFoundError
    except NotFoundError:
        assert(True)  # NotFoundError expected


def test_get_children_for_id_without_parent_account_returns_one_item():
    (a, accounts) = get_test_objects([ACCOUNT_ID])
    result = a.get_children_for_id(ACCOUNT_ID)
    assert(len(result) is 0)


def test_get_children_for_id_with_child_account_returns_one_item():
    (a, accounts) = get_test_objects([FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID])
    a[1].parent_id.set(FIRST_ACCOUNT_ID)
    result = a.get_children_for_id(FIRST_ACCOUNT_ID)
    assert(len(result) is 1)
    assert(result[0].id.get() == SECOND_ACCOUNT_ID)


def test_get_children_for_id_with_children_account_returns_multiple_items():
    (a, accounts) = get_test_objects([
        FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID, THIRD_ACCOUNT_ID])
    a[1].parent_id.set(FIRST_ACCOUNT_ID)
    a[2].parent_id.set(SECOND_ACCOUNT_ID)
    result = a.get_children_for_id(FIRST_ACCOUNT_ID)
    assert(len(result) is 2)
    assert(result[0].id.get() == SECOND_ACCOUNT_ID)
    assert(result[1].id.get() == THIRD_ACCOUNT_ID)


def test_get_children_for_id_with_invalid_account_id_raises_NotFoundError():
    (a, accounts) = get_test_objects()
    try:
        a.get_children_for_id(ACCOUNT_ID)
        assert(False)  # Exception expected
    except NotFoundError:
        assert(True)


@mock.patch(
    'netsplice.backend.preferences.model.chain_list.'
    'chain_list.get_siblings_for_id')
def test_get_children_for_id_without_siblings_returns_empty_list(
        mock_get_siblings_for_id):
    (a, accounts) = get_test_objects([ACCOUNT_ID])
    mock_get_siblings_for_id.side_effect = NotFoundError('test')
    result = a.get_children_for_id(ACCOUNT_ID)
    assert(len(result) is 0)


@mock.patch(
    'netsplice.backend.preferences.model.chain_list.'
    'chain_list.get_parents_for_id')
def test_get_connection_chain_without_parents_returns_one_item_chain(
        mock_get_parents_for_id):
    (a, accounts) = get_test_objects([ACCOUNT_ID])
    mock_get_parents_for_id.return_value = []
    result = a.get_connection_chain(
        ACCOUNT_ID, accounts, connection_chain)
    # [ ci ]
    print('result:', result, a)
    assert(len(result) is 1)


def test_get_connection_chain_with_parents_returns_correct_chain_length():
    print('[ 0, 1 ]')
    (a, accounts) = get_test_objects([FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID])
    a[1].parent_id.set(FIRST_ACCOUNT_ID)

    result = a.get_connection_chain(
        SECOND_ACCOUNT_ID, accounts, connection_chain)
    print('0', a[0].id.get(), a[0].parent_id.get())
    print('1', a[1].id.get(), a[1].parent_id.get())
    print(result)
    assert(len(result) is 2)
    result = a.get_connection_chain(
        FIRST_ACCOUNT_ID, accounts, connection_chain)
    assert(len(result) is 2)

    print('[ 0, 1, 2 ]')
    (a, accounts) = get_test_objects([
        FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID, THIRD_ACCOUNT_ID])
    a[1].parent_id.set(FIRST_ACCOUNT_ID)
    a[2].parent_id.set(SECOND_ACCOUNT_ID)

    result = a.get_connection_chain(
        FIRST_ACCOUNT_ID, accounts, connection_chain)
    print('0', a[0].id.get(), a[0].parent_id.get())
    print('1', a[1].id.get(), a[1].parent_id.get())
    print('2', a[2].id.get(), a[2].parent_id.get())
    print(result)
    assert(len(result) is 3)
    result = a.get_connection_chain(
        SECOND_ACCOUNT_ID, accounts, connection_chain)
    assert(len(result) is 3)
    result = a.get_connection_chain(
        THIRD_ACCOUNT_ID, accounts, connection_chain)
    assert(len(result) is 3)

    print('[ 0 -> 1, 0 -> 2 ]')
    (a, accounts) = get_test_objects([
        FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID, THIRD_ACCOUNT_ID])
    a[1].parent_id.set(FIRST_ACCOUNT_ID)
    a[2].parent_id.set(FIRST_ACCOUNT_ID)

    result = a.get_connection_chain(
        FIRST_ACCOUNT_ID, accounts, connection_chain)
    print('0', a[0].id.get(), a[0].parent_id.get())
    print('1', a[1].id.get(), a[1].parent_id.get())
    print('2', a[2].id.get(), a[2].parent_id.get())
    print(result)
    assert(len(result) is 3)
    result = a.get_connection_chain(
        SECOND_ACCOUNT_ID, accounts, connection_chain)
    assert(len(result) is 3)
    result = a.get_connection_chain(
        THIRD_ACCOUNT_ID, accounts, connection_chain)
    assert(len(result) is 3)


def test_get_connection_chain_without_change_returns_same_chain():
    (a, accounts) = get_test_objects([FIRST_ACCOUNT_ID])
    result1 = a.get_connection_chain(
        FIRST_ACCOUNT_ID, accounts, connection_chain)
    result2 = a.get_connection_chain(
        FIRST_ACCOUNT_ID, accounts, connection_chain)
    # [ ci ]
    print('results:', result1, result2)
    assert(result1 is result2)

    print('[ 0 -> 1, 0 -> 2 ]')
    (a, accounts) = get_test_objects([
        FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID, THIRD_ACCOUNT_ID])
    a[1].parent_id.set(FIRST_ACCOUNT_ID)
    a[2].parent_id.set(FIRST_ACCOUNT_ID)

    result1 = a.get_connection_chain(
        FIRST_ACCOUNT_ID, accounts, connection_chain)
    print('0', a[0].id.get(), a[0].parent_id.get())
    print('1', a[1].id.get(), a[1].parent_id.get())
    print('2', a[2].id.get(), a[2].parent_id.get())
    result2 = a.get_connection_chain(
        SECOND_ACCOUNT_ID, accounts, connection_chain)
    result3 = a.get_connection_chain(
        THIRD_ACCOUNT_ID, accounts, connection_chain)
    assert(result1 is result2)
    assert(result1 is result3)


def test_get_connection_chain_with_change_returns_new_chain():
    (a, accounts) = get_test_objects([FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID])
    a[1].parent_id.set(FIRST_ACCOUNT_ID)
    result1 = a.get_connection_chain(
        SECOND_ACCOUNT_ID, accounts, connection_chain)
    a.delete_account(FIRST_ACCOUNT_ID)
    result2 = a.get_connection_chain(
        SECOND_ACCOUNT_ID, accounts, connection_chain)
    # [ ci ]
    print('results:', result1, result2)
    assert(result1 is not result2)

    print('[ 0 -> 1, 0 -> 2 ]')
    (a, accounts) = get_test_objects([
        FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID, THIRD_ACCOUNT_ID])
    a[1].parent_id.set(FIRST_ACCOUNT_ID)
    a[2].parent_id.set(FIRST_ACCOUNT_ID)

    result1 = a.get_connection_chain(
        FIRST_ACCOUNT_ID, accounts, connection_chain)
    print('0', a[0].id.get(), a[0].parent_id.get())
    print('1', a[1].id.get(), a[1].parent_id.get())
    print('2', a[2].id.get(), a[2].parent_id.get())
    a.modify_account(FIRST_ACCOUNT_ID)
    result2 = a.get_connection_chain(
        SECOND_ACCOUNT_ID, accounts, connection_chain)
    result3 = a.get_connection_chain(
        THIRD_ACCOUNT_ID, accounts, connection_chain)
    assert(result1 is not result2)
    assert(result1 is not result3)
    assert(result2 is result3)


def test_get_parents_for_id():
    (a, accounts) = get_test_objects([
        FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID, THIRD_ACCOUNT_ID])
    a[1].parent_id.set(FIRST_ACCOUNT_ID)
    a[2].parent_id.set(SECOND_ACCOUNT_ID)
    print('0', a[0].id.get(), a[0].parent_id.get())
    print('1', a[1].id.get(), a[1].parent_id.get())
    print('2', a[2].id.get(), a[2].parent_id.get())

    result = a.get_parents_for_id(FIRST_ACCOUNT_ID)
    assert(len(result) is 0)

    result = a.get_parents_for_id(SECOND_ACCOUNT_ID)
    print(result)
    assert(len(result) is 1)

    result = a.get_parents_for_id(THIRD_ACCOUNT_ID)
    print(result)
    assert(len(result) is 2)


@mock.patch(
    'netsplice.backend.preferences.model.chain_list.chain_list.find_by_id')
def test_get_parents_for_id_broken_chain_raises(mock_find_by_id):
    (a, accounts) = get_test_objects([
        FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID, THIRD_ACCOUNT_ID])
    a[1].parent_id.set(FIRST_ACCOUNT_ID)
    a[2].parent_id.set(SECOND_ACCOUNT_ID)
    mock_find_by_id.side_effect = NotFoundError('mock error')
    try:
        a.get_parents_for_id(THIRD_ACCOUNT_ID)
        assert(False)  # Exception not raised
    except NotFoundError:
        assert(True)  # Not Found error raised


def test_get_siblings_for_id():
    (a, accounts) = get_test_objects([
        FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID, THIRD_ACCOUNT_ID])
    a[1].parent_id.set(FIRST_ACCOUNT_ID)
    a[2].parent_id.set(SECOND_ACCOUNT_ID)
    print('0', a[0].id.get(), a[0].parent_id.get())
    print('1', a[1].id.get(), a[1].parent_id.get())
    print('2', a[2].id.get(), a[2].parent_id.get())

    result = a.get_siblings_for_id(FIRST_ACCOUNT_ID)
    assert(len(result) is 1)

    result = a.get_siblings_for_id(SECOND_ACCOUNT_ID)
    print(result)
    assert(len(result) is 1)

    result = a.get_siblings_for_id(THIRD_ACCOUNT_ID)
    print(result)
    assert(len(result) is 0)


def test_modify_account_no_cache_no_cache():
    (a, accounts) = get_test_objects([
        FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID, THIRD_ACCOUNT_ID])
    assert(len(a._cache.keys()) is 0)
    a.modify_account(FIRST_ACCOUNT_ID)
    assert(len(a._cache.keys()) is 0)


def test_modify_account_with_cache_removes_correct():
    (a, accounts) = get_test_objects([
        FIRST_ACCOUNT_ID, SECOND_ACCOUNT_ID, THIRD_ACCOUNT_ID])
    a._cache[FIRST_ACCOUNT_ID] = '1'
    a._cache[SECOND_ACCOUNT_ID] = '1'
    a._cache[THIRD_ACCOUNT_ID] = '2'
    assert(len(a._cache.keys()) is 3)
    a.modify_account(FIRST_ACCOUNT_ID)
    assert(len(a._cache.keys()) is 1)
