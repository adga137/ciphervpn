# -*- coding: utf-8 -*-
# backend.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for preferences concerning the Backend.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.arg_list import arg_list
from netsplice.model.validator.min import (
    min as min_validator
)
from netsplice.model.validator.max import (
    max as max_validator
)
from netsplice.model.validator.store_password import (
    store_password as store_password_validator
)


class backend(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.max_shutdown_time = field(
            validators=[min_validator(1), max_validator(300)])

        self.sequence_wait_after_connect = field(
            validators=[min_validator(1), max_validator(30)])

        self.ipc_certificate_valid_days = field(
            validators=[min_validator(1), max_validator(365)])

        self.graphical_sudo_application = arg_list()

        self.store_password_default = field(
            validators=[store_password_validator()])
