# -*- coding: utf-8 -*-
# collection_name.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for collection names used to validate requests.
This model is also implemented in the Backend.
'''

import uuid
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.backend.preferences.model.validator.collection_name import (
    collection_name as collection_name_validator
)


class collection_name(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.name = field(
            required=True,
            validators=[collection_name_validator()])
