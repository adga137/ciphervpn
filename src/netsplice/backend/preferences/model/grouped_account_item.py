# -*- coding: utf-8 -*-
# grouped_account_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for grouped account items.
'''

from netsplice.config import model as config_model
from netsplice.util.model.field import field
from netsplice.util.model.marshalable_persistent import marshalable_persistent
from netsplice.model.validator.group_id import (
    group_id as group_id_validator
)
from netsplice.model.validator.account_id import (
    account_id as account_id_validator
)
from netsplice.backend.preferences.model.validator.account_name import (
    account_name as account_name_validator
)
from netsplice.backend.preferences.model.validator.account_type import (
    account_type as account_type_validator
)
from netsplice.backend.preferences.model.validator.weight import (
    weight as weight_validator
)
from netsplice.model.validator.boolean import (
    boolean as boolean_validator
)
from netsplice.model.validator.version import (
    version as version_validator
)


class grouped_account_item(marshalable_persistent):
    def __init__(self, owner):
        marshalable_persistent.__init__(self, owner)

        self.id = field(
            required=True,
            validators=[account_id_validator()])

        self.group_id = field(
            required=True,
            validators=[group_id_validator()])

        self.weight = field(
            required=False,
            default=0,
            validators=[weight_validator()])

        # Derived fields, not modifiable by API.
        self.name = field(
            required=True,
            validators=[account_name_validator()])

        self.type = field(
            default='OpenVPN',
            required=True,
            validators=[account_type_validator()])

        self.enabled = field(
            required=True,
            validators=[boolean_validator()])

        self.version = field(
            required=True,
            default=config_model.VERSION,
            validators=[version_validator()])

    def update(self, group_id, account_model_instance):
        self.name.set(account_model_instance.name.get())
        self.type.set(account_model_instance.type.get())
        self.id.set(account_model_instance.id.get())
        self.group_id.set(group_id)
        self.enabled.set(account_model_instance.enabled.get())
