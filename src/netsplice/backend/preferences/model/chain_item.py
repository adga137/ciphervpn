# -*- coding: utf-8 -*-
# chain_item.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for chain items.
'''

from netsplice.config import model as config_model
from netsplice.config import backend as config_backend
from netsplice.util.model.errors import ValidationError
from netsplice.util.errors import NotFoundError
from netsplice.util.model.field import field
from netsplice.util.model.marshalable_persistent import marshalable_persistent
from netsplice.model.validator.account_id import (
    account_id as account_id_validator
)
from netsplice.model.validator.account_id_value import (
    account_id_value as account_id_value_validator
)
from netsplice.model.validator.enum import (
    enum as enum_validator
)
from netsplice.backend.preferences.model.validator.weight import (
    weight as weight_validator
)
from netsplice.model.validator.boolean import (
    boolean as boolean_validator
)
from netsplice.model.validator.version import (
    version as version_validator
)


class chain_item(marshalable_persistent):
    def __init__(self, owner):
        marshalable_persistent.__init__(self, owner)

        self.id = field(
            required=True,
            validators=[account_id_validator()])

        self.parent_id = field(
            required=True,
            default=None,
            validators=[account_id_value_validator()])

        self.connect_mode = field(
            required=False,
            default=config_backend.CONNECT_MODE_SEQUENCE,
            validators=[enum_validator(config_backend.CONNECT_MODES)])

        self.failure_mode = field(
            required=False,
            default=config_backend.FAILURE_MODE_BREAK,
            validators=[enum_validator(config_backend.FAILURE_MODES)])

        self.collapsed = field(
            required=False,
            default=False,
            validators=[boolean_validator()])

        self.weight = field(
            required=False,
            default=0,
            validators=[weight_validator()])

        self.version = field(
            required=True,
            default=config_model.VERSION,
            validators=[version_validator()])

    def is_none(self):
        '''
        Is None.

        Return true when the connect mode is none.

        Returns:
            bool -- none connect mode selected.
        '''
        return self.connect_mode.get() == config_backend.CONNECT_MODE_NONE

    def is_parallel(self):
        '''
        Is Parallel.

        Return true when the connect mode is parallel.

        Returns:
            bool -- parallel connect mode selected.
        '''
        return self.connect_mode.get() == config_backend.CONNECT_MODE_PARALLEL

    def is_sequence(self):
        '''
        Is Sequence.

        Return true when the connect mode is sequence.

        Returns:
            bool -- sequence connect mode selected.
        '''
        return self.connect_mode.get() == config_backend.CONNECT_MODE_SEQUENCE

    def update_parent(self, parent_account_id, account_model_instance, chains):
        '''
        Update Parent.

        Update the parent of the chain with the given parent_account_id

        Arguments:
            parent_account_id (string): account_id that should be parent
            account_model_instance (model.account_item): account associated \
                with chain.
            chains (model.chain_list): complete chain to check if a \
                recursion is defined.
        Raises:
            ValidationError - when the given parent would cause a recursion.
        '''
        if account_model_instance.id.get() == parent_account_id:
            raise ValidationError('Cannot move to self parent.')
        if parent_account_id is not None:
            try:
                children = chains.get_children_for_id(self.id.get())
                for child in children:
                    if child.id.get() == parent_account_id:
                        raise ValidationError('Cannot move to child.')
            except NotFoundError:
                pass
        self.id.set(account_model_instance.id.get())
        self.parent_id.set(parent_account_id)

    def update(self, update_chain_model):
        '''
        Update.

        Update the chain with the given request model.

        Arguments:
            update_chain_model (model.request.chain_update): update chain \
                model.
        '''
        self.collapsed.set(update_chain_model.collapsed.get())
        self.connect_mode.set(update_chain_model.connect_mode.get())
        self.failure_mode.set(update_chain_model.failure_mode.get())
        self.weight.set(update_chain_model.weight.get())
