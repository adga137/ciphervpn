# -*- coding: utf-8 -*-
# account_list_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for listing stored accounts via REST response.
'''

from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.backend.preferences.model.response.account_list_item import (
    account_list_item
)


class account_list(marshalable_list):
    '''
    Marshalable model that contains multiple items of
    account_list_item_model.
    '''
    def __init__(self):
        marshalable_list.__init__(self, account_list_item)

    def add_account(self, account_model_instance):
        '''
        Add the given (module)account_model_instance to the account_list by
        creating a new instance, populate it with values and append it to the
        collection.
        '''
        account = self.item_model_class()
        account.id.set(account_model_instance.id.get())
        account.name.set(account_model_instance.name.get())
        account.type.set(account_model_instance.type.get())
        account.enabled.set(account_model_instance.enabled.get())

        self.append(account)
