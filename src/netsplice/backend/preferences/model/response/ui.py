# -*- coding: utf-8 -*-
# ui.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for preferences concerning the UI.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.validator.boolean import (
    boolean as boolean_validator
)
from netsplice.model.validator.enum import (
    enum as enum_validator
)
from netsplice.model.validator.min import (
    min as min_validator
)
from netsplice.model.validator.max import (
    max as max_validator
)
from netsplice.model.validator.keyboard_shortcut import (
    keyboard_shortcut as keyboard_shortcut_validator
)
import netsplice.config.log as config_log
import netsplice.config.gui as config_gui


class ui(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.close_to_systray = field(
            default=config_gui.CLOSE_TO_SYSTRAY,
            validators=[boolean_validator()])

        self.start_hidden = field(
            default=False,
            validators=[boolean_validator()])

        self.start_on_boot = field(
            default=False,
            validators=[boolean_validator()])

        self.show_welcome_on_start = field(
            default=True,
            validators=[boolean_validator()])

        self.theme = field(
            default=config_gui.THEME,
            validators=[enum_validator(config_gui.THEMES_AVAILABLE)])

        self.locale = field(default='C')

        self.start_drag_distance = field(
            default=-1,
            validators=[min_validator(-1), max_validator(10)])

        self.max_logviewer_items = field(
            default=config_log.MAX_LOGVIEWER_ITEMS,
            validators=[min_validator(100), max_validator(10000)])

        self.logviewer_text_lines_per_item = field(
            default=config_log.TEXT_LINES_PER_ITEM,
            validators=[min_validator(1), max_validator(10)])

        self.logviewer_date = field(
            default=config_log.LOGVIEWER_DATE,
            validators=[enum_validator(['timeago', 'raw', 'time'])])

        self.statusbar_show_version = field(
            default=config_gui.STATUSBAR_SHOW_VERSION,
            validators=[boolean_validator()])

        self.statusbar_show_product_owner = field(
            default=config_gui.STATUSBAR_SHOW_PRODUCT_OWNER,
            validators=[boolean_validator()])

        self.statusbar_show_connections = field(
            default=config_gui.STATUSBAR_SHOW_CONNECTIONS,
            validators=[boolean_validator()])

        self.statusbar_show_heartbeat = field(
            default=config_gui.STATUSBAR_SHOW_HEARTBEAT,
            validators=[boolean_validator()])

        self.show_log_on_connection_failure = field(
            default=config_gui.SHOW_LOG_ON_CONNECTION_FAILURE,
            validators=[boolean_validator()])

        self.shortcut_connect_all = field(
            default=config_gui.SHORTCUT_CONNECT_ALL,
            validators=[keyboard_shortcut_validator()])

        self.shortcut_create_connection = field(
            default=config_gui.SHORTCUT_CREATE_CONNECTION,
            validators=[keyboard_shortcut_validator()])

        self.shortcut_help = field(
            default=config_gui.SHORTCUT_HELP,
            validators=[keyboard_shortcut_validator()])

        self.shortcut_selected_rename = field(
            default=config_gui.SHORTCUT_SELECTED_RENAME,
            validators=[keyboard_shortcut_validator()])

        self.shortcut_selected_edit = field(
            default=config_gui.SHORTCUT_SELECTED_EDIT,
            validators=[keyboard_shortcut_validator()])

        self.shortcut_selected_connect = field(
            default=config_gui.SHORTCUT_SELECTED_CONNECT,
            validators=[keyboard_shortcut_validator()])

        self.shortcut_selected_disconnect = field(
            default=config_gui.SHORTCUT_SELECTED_DISCONNECT,
            validators=[keyboard_shortcut_validator()])

        self.shortcut_show_logs = field(
            default=config_gui.SHORTCUT_SHOW_LOGS,
            validators=[keyboard_shortcut_validator()])

        self.shortcut_show_preferences = field(
            default=config_gui.SHORTCUT_SHOW_PREFERENCES,
            validators=[keyboard_shortcut_validator()])

        self.shortcut_quit = field(
            default=config_gui.SHORTCUT_QUIT,
            validators=[keyboard_shortcut_validator()])

        self.systray_animate_icon = field(
            default=config_gui.SYSTRAY_ANIMATE_ICON,
            validators=[boolean_validator()])

        self.systray_show_notifications = field(
            default=config_gui.SYSTRAY_SHOW_NOTIFICATIONS,
            validators=[boolean_validator()])
