# -*- coding: utf-8 -*-
# grouped_account_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for listing a single group configuration via REST response.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.validator.account_id import (
    account_id as account_id_validator
)
from netsplice.backend.preferences.model.validator.account_name import (
    account_name as account_name_validator
)
from netsplice.backend.preferences.model.validator.account_type import (
    account_type as account_type_validator
)
from netsplice.model.validator.group_id import (
    group_id as group_id_validator
)
from netsplice.backend.preferences.model.validator.weight import (
    weight as weight_validator
)
from netsplice.model.validator.boolean import (
    boolean as boolean_validator
)


class grouped_account_item(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.id = field(
            required=True,
            validators=[account_id_validator()])

        self.group_id = field(
            required=True,
            validators=[group_id_validator()])

        self.name = field(
            required=True,
            validators=[account_name_validator()])

        self.type = field(
            default='OpenVPN',
            required=True,
            validators=[account_type_validator()])

        self.weight = field(
            required=False,
            default=0,
            validators=[weight_validator()])

        self.enabled = field(
            required=True,
            validators=[boolean_validator()])
