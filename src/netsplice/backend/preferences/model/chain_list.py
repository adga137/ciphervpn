# -*- coding: utf-8 -*-
# chain_list.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Account Chain Preferences.
'''

import sys
from netsplice.util.model.marshalable_list_persistent import (
    marshalable_list_persistent
)
from netsplice.backend.preferences.model.chain_item import (
    chain_item as chain_item_model
)
from netsplice.backend.preferences.model.request import (
    model as request_model
)
from netsplice.backend.preferences.model.response import (
    model as response_model
)
from netsplice.config import backend as config_backend
from netsplice.util.errors import (
    NotFoundError, ConnectedError
)
from netsplice.util import get_logger

logger = get_logger()


class chain_list(marshalable_list_persistent):
    def __init__(self, owner):
        marshalable_list_persistent.__init__(
            self, chain_item_model, owner, 'chains')
        self.separate_instances = False

        self.request = request_model()
        self.response = response_model()
        self._cache = dict()

    def _get_weight(self, chain_model_instance):
        '''
        Get Weight.

        Return weight for a chain item for sorting.
        '''
        return chain_model_instance.weight.get()

    def _produce_connection_chain(
            self, chain_model_instance, accounts,
            connection_chain_constructor):
        '''
        Produce Connection Chain.

        Recursively walk the children of the given chain model instance
        and create chains with the connection_chain_constructor

        Arguments:
            chain_model_instance (chain_item): item with or without children
            accounts (list<accounts>): list of all accounts
            connection_chain_constructor ([type]): chain constructor

        Returns:
            connection_chain -- instance of a connection_chain with \
                registered links according to the preference structure.
        '''
        chain_item_children = self.get_siblings_for_id(
            chain_model_instance.id.get())
        account_model_instance = accounts.find_by_id(
            chain_model_instance.id.get())
        connection_chain = connection_chain_constructor(
            [],
            failure_mode=chain_model_instance.failure_mode.get(),
            connect_mode=chain_model_instance.connect_mode.get())

        if len(chain_item_children) == 0:
            # Recursion end. register the given instance and return the chain
            connection_chain.register([account_model_instance])
            return connection_chain

        # Children available. Sort the children by weight and evaluate their
        # subchains.
        sorted_children = sorted(
            chain_item_children, key=self._get_weight)
        child_chain = list()
        child_chain.append(account_model_instance)

        for child in sorted_children:
            links = self._produce_connection_chain(
                child, accounts, connection_chain_constructor)
            child_chain.append(links)
        connection_chain.register(child_chain)

        return connection_chain

    def add_account(self, parent_account_id, account_model_instance):
        '''
        Add Account.

        Add a chain_item with values from the account_model_instance
        to the chain_list with the parent_account_id given.
        Set the weight of the new chain_item to maximum, so it will
        be at the end of the list.
        '''
        new_chain = self.item_model_class(self.get_owner())
        new_chain.id.set(account_model_instance.id.get())
        new_chain.update_parent(
            parent_account_id, account_model_instance, self)
        new_chain.weight.set(sys.maxsize)
        self.append(new_chain)

    def apply_values(self):
        '''
        Apply Values.

        Apply values from the preferences to the config so other
        components can use the values.
        '''
        pass

    def cmp_by_chain_parent_weight(self, account_a, account_b):
        '''
        Compare by chain parent weight.

        Sorted cmp= method that evaluates the two accounts by their weight or
        their parents weight:

        .. code-block:: none

            root
              a1
              a2
            a2 > a1

            root
              a1
              a2
                a3
              a4
            a3 > a1

        Arguments:
            account_a (account): account to compare
            account_b (account): account to compare

        Returns:
            int -- 1,0,-1 for sorted
        '''
        a_parents = self.get_parents_for_id(account_a.id.get())
        b_parents = self.get_parents_for_id(account_b.id.get())
        if len(a_parents) == 0 or len(b_parents) == 0:
            return 0
        common_parent = None
        for a_parent in a_parents:
            for b_parent in b_parents:
                if a_parent.id.get() == b_parent.id.get():
                    common_parent = a_parent
                    break
            if common_parent is not None:
                break

        if common_parent is None:
            return 0

        a_chain = self.find_common_account(account_a, common_parent)
        b_chain = self.find_common_account(account_b, common_parent)

        if a_chain.weight.get() > b_chain.weight.get():
            return 1
        return -1

    def delete_account(self, account_id):
        '''
        Delete Account.

        Remove the chain for the given account_id.
        '''
        chain = self.find_by_id(account_id)
        chain_siblings = self.get_siblings_for_id(account_id)
        for sibling in chain_siblings:
            sibling.parent_id.set(chain.parent_id.get())
        self.modify_account(account_id)
        self.remove(chain)

    def find_by_id(self, account_id):
        '''
        Find By Id.

        Find a chain for an account with the given account_id.
        '''
        for chain in self:
            if chain.id.get() == account_id:
                return chain
        raise NotFoundError(
            'No chain for account %s found.' % (account_id,))

    def find_by_parent_id(self, parent_account_id):
        '''
        Find by Parent Id.

        Return a chain that has the given parent_account_id
        defined as parent. When no account with the parent_account_id is
        found, a NotFoundError is raised.
        '''
        chain_items = []
        for chain in self:
            if chain.parent_id.get() == parent_account_id:
                chain_items.append(chain)
        if len(chain_items) is 0:
            raise NotFoundError(
                'No parent chain for account %s found.'
                % (parent_account_id,))
        return chain_items

    def find_common_account(self, account, common_parent):
        '''
        Find Common Account.

        Find the account in the parents that is in the same level as the given
        account.

        Arguments:
            account (account): Account that has common_parent as one of its \
                parents.
            common_parent (account): Common Parent of account and a other \
                account

        Returns:
            [type] -- [description]
        '''
        parents = self.get_parents_for_id(account.id.get())
        common_account = self.find_by_id(account.id.get())
        for index in range(0, len(parents)):
            if parents[index].id.get() != common_parent.id.get():
                continue
            if index == 0:
                break
            try:
                common_account = self.find_by_id(parents[index - 1].id.get())
            except NotFoundError:
                break
            break
        return common_account

    def get_account_chain(self, account_id):
        '''
        Get Account Chain.

        Return the chain the given account_id exists in.
        '''
        # raise NotFoundError when account_id is not a chain
        account_chain_instance = self.find_by_id(account_id)
        account_parent_chain = self.get_parents_for_id(account_id)
        account_child_chain = self.get_children_for_id(account_id)
        account_chain = list()
        account_chain.extend(account_parent_chain)
        account_chain.append(account_chain_instance)
        account_chain.extend(account_child_chain)
        return account_chain

    def get_account_list(self, chain_accounts, accounts):
        '''
        Get Account List.

        Return a list of account_items that match the ids in the
        chain_accounts. This method operates on a given
        chain_accounts list to allow sorting and filtering before getting
        the accounts.
        '''
        account_list = []
        for chain in chain_accounts:
            account_list.append(accounts.find_by_id(chain.id.get()))
        return account_list

    def forget_implicit_accounts(self, accounts, chains):
        '''
        Get Account List vectors.

        Return accounts that resolve to unique chains, removing all accounts
        that are covered by their parent hierarchy

        Arguments:
            accounts ([type]): accounts that need cleanup.
            chains ([type]): chains that contain the accounts
        '''
        account_ids = []
        rm_accounts = []
        rm_account_ids = []
        clean_accounts = []

        for account in accounts:
            account_ids.append(account.id.get())
            clean_accounts.append(account)

        for account in accounts:
            chained_account_parents = chains.get_parents_for_id(
                account.id.get())
            for chained_account in chained_account_parents:
                if chained_account.id.get() not in account_ids:
                    continue
                if chained_account.id.get() in rm_account_ids:
                    continue
                rm_accounts.append(chained_account)

                rm_account_ids = []
                for account in rm_accounts:
                    rm_account_ids.append(account.id.get())

        for account in rm_accounts:
            for ca in clean_accounts:
                if ca.id.get() != account.id.get():
                    continue
                clean_accounts.remove(ca)
                break

        return clean_accounts

    def get_children_for_id(self, account_id):
        '''
        Get Chain for id

        Return the all accounts that are chained to the given account_id.

        [a -> b -> c -> d] for b returns [c, d]
        [a -> b -> c -> d] for a returns [b, c, d]

        '''
        chain_children = []
        siblings = []
        start_chain = self.find_by_id(account_id)
        try:
            siblings = self.get_siblings_for_id(start_chain.id.get())
        except NotFoundError:
            return chain_children

        for item in siblings:
            chain_children.append(item)
            try:
                chain_children.extend(
                    self.get_children_for_id(item.id.get()))
            except NotFoundError:
                pass
        return chain_children

    def get_connection_chain(
            self, account_id, accounts, connection_chain_constructor):
        '''
        Get Connection Chain

        Return the connection chain for the given account_id by evaluating its
        parents and producing a chain accordingly. When the chain was produced
        it is stored in a cache to ensure that a chain is reused.

        Arguments:
            account_id (uuid): id of the account in the chain
            accounts {list<account> -- list of all available accounts.
            connection_chain_constructor (backend.connection.chain): \
                constructor to build the chain with.
        '''
        if account_id in self._cache:
            return self._cache[account_id]

        parents = self.get_parents_for_id(account_id)
        root = None
        if len(parents) > 0:
            root = parents[-1]
        else:
            chain_model_instance = self.find_by_id(account_id)
            root = chain_model_instance
        connection_chain = self._produce_connection_chain(
            root, accounts, connection_chain_constructor)
        for link in connection_chain:
            self._cache[link.account.id.get()] = connection_chain
        return connection_chain

    def get_parents_for_id(self, account_id):
        '''
        Get Parents for id.

        Return the parent chain for the given account_id.

        [a -> b -> c -> d] for b returns [a]
        '''
        parents = []
        try:
            chain = self.find_by_id(account_id)
            while chain.parent_id.get() is not None:
                chain = self.find_by_id(chain.parent_id.get())
                if chain in parents:
                    logger.warn('Recursion in chain')
                    break
                parents.append(chain)
        except NotFoundError as errors:
            logger.error('Chain is broken at %s' % (str(errors,)))
            raise errors
        return parents

    def get_siblings_for_id(self, account_id):
        '''
        Get Siblings for id.

        Return the accounts that are chained directly to the given account_id.

        [a -> b -> c -> d] for b returns [c]
        [a -> b -> c -> d] for a returns [b]
        [a -> b, a -> c] for a returns [b, c]

        '''
        chain_siblings = []
        siblings = []
        start_chain = self.find_by_id(account_id)

        try:
            siblings = self.find_by_parent_id(start_chain.id.get())
        except NotFoundError:
            return chain_siblings

        for item in siblings:
            chain_siblings.append(item)

        return chain_siblings

    def modify_account(self, account_id):
        '''
        Modify Account.

        Remove any caches associated with the account chain

        Arguments:
            account_id (string): account_id that has been modified
        '''
        try:
            account_connection_chain = self._cache[account_id]
            rm_ids = list()
            for key, chain in self._cache.items():
                if chain is account_connection_chain:
                    rm_ids.append(key)
            for rm_id in rm_ids:
                del self._cache[rm_id]
        except KeyError:
            pass

    def verify(self, accounts):
        '''
        Verify.

        Check that all accounts referenced in the dependencies are available.
        When a account is not referenced, link it to the global-group.

        Logger is not available when this function is called during startup
        use print.
        '''
        self._cache.clear()
        rm_chains = []
        for chain in self:
            try:
                accounts.find_by_id(chain.id.get())
                if chain.parent_id.get() is not None:
                    accounts.find_by_id(chain.parent_id.get())
                    # ensure that no recursion is defined
                    parents = self.get_parents_for_id(chain.id.get())
                    for parent in parents:
                        if parent.parent_id.get() == chain.id.get():
                            rm_chains.append(chain)
                            break
            except NotFoundError:
                rm_chains.append(chain)

        for chain in rm_chains:
            self.remove(chain)

        # ensure that a chain exists for every account
        for account in accounts:
            try:
                self.find_by_id(account.id.get())
            except NotFoundError:
                new_chain = self.item_model_class(self.get_owner())
                new_chain.id.set(account.id.get())
                self.append(new_chain)

        # ensure consistent weight
        for chain in self:
            siblings = self.get_siblings_for_id(chain.id.get())
            sorted_siblings = sorted(siblings, key=self._get_weight)

            for (weight, sibling) in enumerate(sorted_siblings):
                new_weight = (len(sorted_siblings) + weight + 1) * 10
                sibling.weight.set(new_weight)

        # ensure only one default route per sibling list, change to
        # none queue otherwise
        for chain in self:
            try:
                if chain.parent_id.get() is None:
                    continue
                parent_chain = self.find_by_id(chain.parent_id.get())
                siblings = self.get_siblings_for_id(chain.parent_id.get())
                sibling_default_route = False
                for sibling in siblings:
                    sibling_account = accounts.find_by_id(
                        sibling.id.get())
                    if sibling_account.default_route.get():
                        if sibling_default_route:
                            # multiple siblings with default route
                            parent_chain.connect_mode.set(
                                config_backend.CONNECT_MODE_NONE)
                        sibling_default_route = True
            except NotFoundError:
                continue
