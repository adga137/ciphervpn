# -*- coding: utf-8 -*-
# test_chain_list_verify.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Chain Lists.

Checks preferences chains model functions.
'''
import mock
from netsplice.backend.connection.chain import chain as connection_chain
from netsplice.backend.preferences.model.account_list import account_list
from netsplice.backend.preferences.model.chain_list import chain_list
from netsplice.backend.preferences.model.request.chain_create import (
    chain_create as chain_create_model
)
from netsplice.backend.preferences.model.request.chain_update import (
    chain_update as chain_update_model
)
from netsplice.config import backend as config_backend
from netsplice.util.errors import (
    NotFoundError, ConnectedError
)


ACCOUNT_ID = 'aac5aa5a-d1df-4ee8-9ede-86898effe163'
OTHER_ACCOUNT_ID = 'bbc5aa5a-d1df-4ee8-9ede-86898effe164'
THIRD_ACCOUNT_ID = 'ccc5aa5a-d1df-4ee8-9ede-86898effe164'


def get_test_object(account_id_list=[]):
    chain = chain_list(None)
    for account_id in account_id_list:
        chain_item = chain.item_model_class(None)
        chain_item.id.set(account_id)
        chain.append(chain_item)
        print(len(chain))
    return chain


@mock.patch(
    'netsplice.backend.preferences.model.account_list.'
    'account_list')
@mock.patch(
    'netsplice.backend.preferences.model.chain_list.'
    'chain_list.remove')
def test_verify_removes_when_account_not_exists(
        mock_remove, mock_account_list):
    a = get_test_object([ACCOUNT_ID, OTHER_ACCOUNT_ID])
    a[1].parent_id.set(ACCOUNT_ID)
    mock_account_list.find_by_id.side_effect = NotFoundError('test')
    a.verify(mock_account_list)
    mock_remove.assert_called()


@mock.patch(
    'netsplice.backend.preferences.model.chain_list.'
    'chain_list.remove')
def test_verify_removes_when_parent_account_not_exists(
        mock_remove):
    a = get_test_object([ACCOUNT_ID, OTHER_ACCOUNT_ID])
    a[1].parent_id.set(ACCOUNT_ID)
    al = account_list(None)
    ala1 = al.item_model_class(None)
    ala1.id.set(ACCOUNT_ID)
    ala1.type.set('OpenVPN')
    ala1.name.set('test')
    ala1.configuration.set('remote host port')
    ala1.import_configuration.set('remote host port')
    ala1.enabled.set(True)
    ala1.default_route.set(False)
    ala1.autostart.set(True)
    al.add_account(ala1)
    a.verify(al)
    mock_remove.assert_called()


@mock.patch(
    'netsplice.backend.preferences.model.chain_list.'
    'chain_list.remove')
def test_verify_with_parent_account_exists_does_not_call_remove(
        mock_remove):
    a = get_test_object([ACCOUNT_ID, OTHER_ACCOUNT_ID])
    a[1].parent_id.set(ACCOUNT_ID)
    al = account_list(None)
    ala1 = al.item_model_class(None)
    ala1.id.set(ACCOUNT_ID)
    ala1.type.set('OpenVPN')
    ala1.name.set('test')
    ala1.configuration.set('remote host port')
    ala1.import_configuration.set('remote host port')
    ala1.enabled.set(True)
    ala1.default_route.set(False)
    ala1.autostart.set(True)

    ala2 = al.item_model_class(None)
    ala2.id.set(OTHER_ACCOUNT_ID)
    ala2.type.set('OpenVPN')
    ala2.name.set('test')
    ala2.configuration.set('remote host port')
    ala2.import_configuration.set('remote host port')
    ala2.enabled.set(True)
    ala2.default_route.set(False)
    ala2.autostart.set(True)

    al.add_account(ala1)
    al.add_account(ala2)
    a.verify(al)
    mock_remove.assert_not_called()
    print(len(a))
    assert(len(a) is 2)


def test_verify_with_unregistered_account_id_appends():
    a = get_test_object([ACCOUNT_ID, OTHER_ACCOUNT_ID])
    al = account_list(None)
    ala1 = al.item_model_class(None)
    ala1.id.set(ACCOUNT_ID)
    ala1.type.set('OpenVPN')
    ala1.name.set('test')
    ala1.configuration.set('remote host port')
    ala1.import_configuration.set('remote host port')
    ala1.enabled.set(True)
    ala1.default_route.set(False)
    ala1.autostart.set(True)

    ala2 = al.item_model_class(None)
    ala2.id.set(OTHER_ACCOUNT_ID)
    ala2.type.set('OpenVPN')
    ala2.name.set('test')
    ala2.configuration.set('remote host port')
    ala2.import_configuration.set('remote host port')
    ala2.enabled.set(True)
    ala2.default_route.set(False)
    ala2.autostart.set(True)

    al.add_account(ala1)
    al.add_account(ala2)
    a.verify(al)
    print(len(a))
    assert(len(a) is 2)


def test_verify_with_multiple_default_routes_changes_to_connectmode_none():
    a = get_test_object([ACCOUNT_ID, OTHER_ACCOUNT_ID, THIRD_ACCOUNT_ID])
    al = account_list(None)
    ala1 = al.item_model_class(None)
    ala1.id.set(ACCOUNT_ID)
    ala1.type.set('OpenVPN')
    ala1.name.set('test')
    ala1.configuration.set('remote host port')
    ala1.import_configuration.set('remote host port')
    ala1.enabled.set(True)
    ala1.default_route.set(True)
    ala1.autostart.set(True)

    ala2 = al.item_model_class(None)
    ala2.id.set(OTHER_ACCOUNT_ID)
    ala2.type.set('OpenVPN')
    ala2.name.set('test')
    ala2.configuration.set('remote host port')
    ala2.import_configuration.set('remote host port')
    ala2.enabled.set(True)
    ala2.default_route.set(True)
    ala2.autostart.set(True)

    ala3 = al.item_model_class(None)
    ala3.id.set(THIRD_ACCOUNT_ID)
    ala3.type.set('OpenVPN')
    ala3.name.set('test')
    ala3.configuration.set('remote host port')
    ala3.import_configuration.set('remote host port')
    ala3.enabled.set(True)
    ala3.default_route.set(True)
    ala3.autostart.set(True)

    al.add_account(ala1)
    al.add_account(ala2)
    al.add_account(ala3)

    a.verify(al)
    ci = a.find_by_id(ACCOUNT_ID)
    ci.connect_mode.set(config_backend.CONNECT_MODE_PARALLEL)

    ci = a.find_by_id(OTHER_ACCOUNT_ID)
    ci.parent_id.set(ACCOUNT_ID)
    ci = a.find_by_id(THIRD_ACCOUNT_ID)
    ci.parent_id.set(ACCOUNT_ID)
    a.verify(al)
    print(len(a))
    assert(len(a) is 3)
    ci = a.find_by_id(ACCOUNT_ID)
    print(ci.connect_mode.get())
    assert(ci.connect_mode.get() == config_backend.CONNECT_MODE_NONE)
