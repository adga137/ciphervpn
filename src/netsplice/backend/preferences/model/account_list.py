# -*- coding: utf-8 -*-
# account_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Account Preferences
'''

import uuid
from netsplice.util.model.marshalable_list_persistent import (
    marshalable_list_persistent
)
from netsplice.backend.preferences.model.account_item import (
    account_item as account_item_model
)
from netsplice.backend.preferences.model.request import (
    model as request_model
)
from netsplice.backend.preferences.model.response import (
    model as response_model
)
from netsplice.model.plugins import (
    plugins as plugins_model
)
from netsplice.util.errors import (
    NotFoundError, ConnectedError
)
from netsplice.util import get_logger

logger = get_logger()


class account_list(marshalable_list_persistent):
    def __init__(self, owner):
        marshalable_list_persistent.__init__(
            self, account_item_model, owner, 'accounts')
        self.separate_instances = True

        self.plugins = plugins_model(self)
        self.request = request_model()
        self.response = response_model()

    def add_account(self, account_model_instance):
        '''
        Add Account.

        Add the given (module) account_model_instance to the account_list by
        creating a new instance, populate it with values and append it to the
        collection.
        '''
        account = self.item_model_class(self)
        account.from_account(account_model_instance)
        self.append(account)

    def append(self, account_model_instance):
        '''
        Append.

        Override of default append that applies the plugin data to the new
        element.
        '''
        list.append(self, account_model_instance)
        for plugin_item in self.plugins:
            plugin_name = plugin_item.name.get()
            try:
                plugin_value = account_model_instance.__dict__[plugin_name]
            except KeyError:
                plugin_value = type(plugin_item)(self.get_owner())
                plugin_value.name.set(plugin_name)
                plugin_value.serialize_json = False

            account_model_instance.__dict__[plugin_name] = plugin_value

    def get_autostart_accounts(self):
        '''
        Get autostart accounts.

        Return a list of accounts that have the autostart attribute set to
        True.

        Returns:
            list -- List of accounts
        '''
        autostart_accounts = []
        for account in self:
            account = self.find_by_id(account.id.get())
            if account.autostart.get() is False:
                continue
            autostart_accounts.append(account)
        return autostart_accounts

    def delete_account(self, account_id, connection_module):
        '''
        Delete Account.

        Remove the account with the given account_id when the account is
        not connected.
        :param account_id basestring
        :param connection_list_model backend.connections
        '''
        account = self.find_by_id(account_id)
        if account.is_connected(connection_module):
            raise ConnectedError()
        self.remove(account)

    def find_by_id(self, account_id):
        '''
        Find By Id.

        Find the account with the given id or raise a NotFoundError.
        '''
        for index, account in enumerate(self):
            if account.id.get() == account_id:
                return account
        raise NotFoundError(account_id)

    def new_account(self, create_account_instance):
        '''
        New Account

        Populate a new account instance with the values given.
        :param update_account_instance backend.preferences.model.request\
                                       .account_create
        '''
        account = self.item_model_class(self)
        account.id.set(str(uuid.uuid4()))
        account.type.set('OpenVPN')
        account.from_create_account(create_account_instance)
        return account

    def update_account(self, update_account_instance, connection_module):
        '''
        Update Account.

        Update the account that matches the id of the update_account_instance.

        update_account_instance:
        backend.preferences.model.request.account_update
        connection_module: backend.connections
        '''
        account = self.find_by_id(update_account_instance.id.get())
        account.from_update_account(update_account_instance)
        self.commit()
