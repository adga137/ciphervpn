# -*- coding: utf-8 -*-
# plugin_collection_controller.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Account Plugin Controller to access the persistent Account instances.

Allow preferences to be stored for plugins. Those settings should not
interfere with other plugins. Each account can reference-use multiple
plugin preferences.
# doc: structure class
#      describe backend-api
# get: Return plugin preference if it exists. 404 otherwise
# put: update plugin preference, full structure using request model provided
       by plugin
'''

from netsplice.util import get_logger
from netsplice.util.errors import (
    NotFoundError, ConnectedError
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError

from netsplice.backend.preferences.model.request.account_id import (
    account_id as account_id_model
)
from netsplice.backend.preferences.model.request.account_create import (
    account_create as account_create_model
)
from netsplice.backend.preferences.model.request.account_update import (
    account_update as account_update_model
)
from netsplice.backend.preferences.model.request.collection_name import (
    collection_name as collection_name_model
)
from netsplice.backend.preferences.model.request.attribute_name import (
    attribute_name as attribute_name_model
)
from netsplice.backend.preferences.model.request.plugin_name import (
    plugin_name as plugin_name_model
)
from netsplice.backend.preferences.model.request.plugin_value import (
    plugin_value as plugin_value_model
)
from netsplice.backend.preferences.model.response.account_list import (
    account_list as account_list_model
)
from netsplice.model.plugin_collection_item import (
    plugin_collection_item as plugin_collection_item_model
)
from netsplice.backend.preferences.model.response.account_detail_item import (
    account_detail_item as account_detail_item_model
)

logger = get_logger()


class plugin_collection_controller(middleware):
    '''
    Account Controller to access the persistent Account instances.
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def get(self,
            plugin_name, collection_name, instance_id, attribute_name=None):
        '''
        Return plugin preferences for collection.
        '''
        module = self.application.get_module('preferences')
        '''
        A single instance with all the details stored in the backend.
        '''
        request_collection_name_model = collection_name_model()
        request_attribute_name_model = attribute_name_model()
        request_instance_id_model = account_id_model()
        request_plugin_name_model = plugin_name_model()
        try:
            request_collection_name_model.name.set(collection_name)
            request_instance_id_model.id.set(instance_id)
            request_plugin_name_model.name.set(plugin_name)
            collection_model = \
                module.model.__dict__[request_collection_name_model.name.get()]

            plugin_instance = collection_model.plugins.\
                get_collection_plugin_instance(
                    request_plugin_name_model.name.get(),
                    request_instance_id_model.id.get())
            if attribute_name is not None:
                request_attribute_name_model.name.set(attribute_name)
                response_model = plugin_value_model()
                response_model.value.set(plugin_instance.get(
                    request_attribute_name_model.name.get()))
            else:
                response_model = collection_model.response.plugins.\
                    find_by_name(
                        request_plugin_name_model.name.get())
                response_model.from_json(plugin_instance.to_json())
            self.write(response_model.to_json())
            self.set_status(200)
        except KeyError as errors:
            self.set_error_code(2237, errors)
            self.set_status(404)
            logger.error(str(errors))
        except NotFoundError as errors:
            self.set_error_code(2238, errors)
            self.set_status(404)
            logger.error(str(errors))
        except ValidationError as errors:
            self.set_error_code(2239, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2240, errors)
            self.set_status(400)
            logger.error(str(errors))
        self.finish()

    def put(self,
            plugin_name, collection_name, instance_id, attribute_name=None):
        '''
        Update an existing account plugin data.
        '''
        module = self.application.get_module('preferences')
        request_collection_name_model = collection_name_model()
        request_attribute_name_model = attribute_name_model()
        request_instance_id_model = account_id_model()
        request_plugin_name_model = plugin_name_model()
        try:
            request_collection_name_model.name.set(collection_name)
            request_instance_id_model.id.set(instance_id)
            request_plugin_name_model.name.set(plugin_name)

            collection_model = \
                module.model.__dict__[request_collection_name_model.name.get()]
            plugin_instance = collection_model.plugins.\
                get_collection_plugin_instance(
                    request_plugin_name_model.name.get(),
                    request_instance_id_model.id.get())
            if attribute_name == 'name':
                raise ValidationError('Name of a plugin cannot be changed')
            if attribute_name is not None:
                request_attribute_name_model.name.set(attribute_name)
                request_model = plugin_value_model()
                request_model.from_json(self.request.body.decode('utf-8'))
                plugin_instance.set_named(
                    request_attribute_name_model.name.get(),
                    request_model.value.get())
            else:
                request_model_type = type(
                    collection_model.request.plugins.find_by_name(
                        request_plugin_name_model.name.get()))
                request_model = request_model_type()
                request_model.from_json(self.request.body.decode('utf-8'))
                request_model.name.set(
                    request_plugin_name_model.name.get())
                plugin_instance.from_json(request_model.to_json())
            module.model.commit_account(request_instance_id_model.id.get())
            self.write(request_model.to_json())
            self.set_status(200)
        except NotFoundError as errors:
            self.set_error_code(2087, errors)
            self.set_status(404)
        except ValidationError as errors:
            self.set_error_code(2088, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2089, errors)
            self.set_status(400)
            logger.error(str(errors))
        self.finish()
