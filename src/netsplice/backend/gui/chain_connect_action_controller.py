# -*- coding: utf-8 -*-
# chain_connect action_controller.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Chain Controller
Controller implementation for connecting chained Account configurations.
'''
from tornado import gen, ioloop

from netsplice.backend.connection.account import account
from netsplice.backend.gui.model.request.account_id import (
    account_id as account_id_model)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.errors import (
    NotFoundError, ConnectedError)
from netsplice.util.model.errors import ValidationError


class chain_connect_action_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)
        self.account = account(self.application)

    @gen.coroutine
    def post(self, account_id):
        '''
        Find given account_id and evaluate the required accounts for the given
        account. connects all accounts in the correct order.
        '''
        request_model = account_id_model()
        try:
            request_model.id.set(account_id)
            ioloop.IOLoop.current().call_later(
                0, lambda: self.account.connect_all(request_model.id.get()))

            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2283, errors)
            self.account.validation_errors(str(errors))
            self.set_status(400)
        except ValueError as errors:
            self.set_error_code(2284, errors)
            self.set_status(400)
        except ConnectedError as errors:
            self.set_error_code(2285, errors)
            self.set_status(403)
        except NotFoundError as errors:
            self.set_error_code(2286, errors)
            self.set_status(404)
        self.finish()
