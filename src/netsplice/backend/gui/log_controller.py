# -*- coding: utf-8 -*-
# log_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Log Controller
Controller implementation for getting logs for connections.
'''

from netsplice.backend.event import names
from netsplice.backend.event import origins
from netsplice.backend.event import types
from netsplice.backend.log.model.log_list import log_list as log_list_model
from netsplice.backend.log.model.log_item import log_item as log_item_model
from netsplice.backend.gui.model.request.connection_id import (
    connection_id as connection_id_model
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.util import get_logger

logger = get_logger()


class log_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def delete(self):
        '''
        Delete Log.

        Delete all Log entries.
        '''
        event_module = self.application.get_module('event')
        event_model = event_module.model
        log_module = self.application.get_module('log')
        log_model = log_module.model

        del log_model[:]
        event = event_model.new_event(
            types.NOTIFY, names.LOG_CHANGED)
        event.origin.set(origins.GUI)
        event_module.add_event(event)

        self.set_status(200)
        self.finish()

    def get(self, offset=None):
        '''
        The "log" request is used to fetch the complete or a limited list of
        the logs accessible to the backend. With the given offset, only log
        items with an index larger than the given offset will be returned.
        '''
        response_model = log_list_model()

        log_module = self.application.get_module('log')
        log_model = log_module.model

        int_offset = 0
        if offset is not None:
            int_offset = int(offset)

        #
        # export the application log
        #
        for log_item in log_model:
            if log_item.index.get() < int_offset:
                continue
            response_item_model = log_item_model()
            response_item_model.from_json(log_item.to_json())
            response_model.append(response_item_model)

        self.write(response_model.to_json())
        self.set_status(200)
        self.finish()

    def post(self):
        '''
        Receive log events from the gui frontend.
        '''
        request_model = log_item_model()
        try:
            request_model.from_json(self.request.body.decode('utf-8'))

            event_module = self.application.get_module('event')
            event_model = event_module.model
            log_module = self.application.get_module('log')
            log_module.add_log_entry(request_model)

            event = event_model.new_event(
                types.NOTIFY, names.LOG_CHANGED)
            event.origin.set(origins.GUI)
            event_module.add_event(event)

            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2107, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2108, errors)
            self.set_status(400)
            logger.error(str(errors))
        self.finish()
