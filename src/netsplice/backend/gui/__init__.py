# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
GUI module exposes all information for displaying the current state of all
connections and trigger actions related to connections and accounts.
'''
from netsplice.backend.gui.model import model as module_model
from netsplice.util.ipc.route import get_module_route
from netsplice.backend.event import names as event_names
from netsplice.backend.gui.log_controller import (
    log_controller as log_controller
)

name = 'gui'

endpoints = get_module_route(
    'netsplice.backend',
    [
        (r'/module/gui', 'module'),
        (r'/module/gui/accounts/(?P<account_id>[^\/]+)/connect',
            'account_connect_action'),
        (r'/module/gui/accounts/(?P<account_id>[^\/]+)/disconnect',
            'account_disconnect_action'),
        (r'/module/gui/accounts/(?P<account_id>[^\/]+)/reconnect',
            'account_reconnect_action'),
        (r'/module/gui/accounts/(?P<account_id>[^\/]+)/reset',
            'account_reset_action'),
        (r'/module/gui/actions/shutdown', 'shutdown_action'),
        (r'/module/gui/chain/(?P<account_id>[^\/]+)/connect',
            'chain_connect_action'),
        (r'/module/gui/credential', 'credential_action'),
        (r'/module/gui/events', 'events'),
        (r'/module/gui/groups/(?P<group_id>[^\/]+)/connect',
            'group_connect_action'),
        (r'/module/gui/groups/(?P<group_id>[^\/]+)/disconnect',
            'group_disconnect_action'),
        (r'/module/gui/groups/(?P<group_id>[^\/]+)/reconnect',
            'group_reconnect_action'),
        (r'/module/gui/logs', 'log'),
        (r'/module/gui/logs/offset/(?P<offset>[^\/]+)', 'log'),
    ])

model = module_model()


def register_module_events(event_module):
    '''
    Register Module Events.

    Register Events send or received by this module and its components.

    Arguments:
        event_module (backend.event): event_module instance
    '''
    event_module.register_origin(
        log_controller, [event_names.LOG_CHANGED])
