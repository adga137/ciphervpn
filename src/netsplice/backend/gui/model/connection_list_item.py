# -*- coding: utf-8 -*-
# connection_list_item_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for listing connections.
'''

import time
from tornado import locks
from netsplice.util.model.field import field
import netsplice.config.backend as config
from netsplice.backend.connection.model.connection_item import (
    connection_item as connection_item_model
)
from netsplice.backend.gui.model.status_list import (
    status_list as status_list_model
)
from netsplice.model.named_value_list import (
    named_value_list as named_value_list_model
)
from netsplice.model.validator.max import (
    max as max_validator
)
from netsplice.model.validator.min import (
    min as min_validator
)
from netsplice.model.validator.byte import (
    byte as byte_validator
)
from netsplice.util import get_logger

logger = get_logger()


class connection_list_item(connection_item_model):
    def __init__(self):
        connection_item_model.__init__(self)

        self.read_tap_bytes = field(
            required=False,
            validators=[byte_validator()])

        self.write_tap_bytes = field(
            required=False,
            validators=[byte_validator()])

        self.read_udp_bytes = field(
            required=False,
            validators=[byte_validator()])

        self.write_udp_bytes = field(
            required=False,
            validators=[byte_validator()])

        self.auth_read_bytes = field(
            required=False,
            validators=[byte_validator()])

        self.pre_compress_bytes = field(
            required=False,
            validators=[byte_validator()])

        self.post_compress_bytes = field(
            required=False,
            validators=[byte_validator()])

        self.pre_decompress_bytes = field(
            required=False,
            validators=[byte_validator()])

        self.post_decompress_bytes = field(
            required=False,
            validators=[byte_validator()])

        # velocity of connection
        self.read_tap_velocity = field(
            required=False,
            validators=[byte_validator()])

        self.write_tap_velocity = field(
            required=False,
            validators=[byte_validator()])

        self.read_udp_velocity = field(
            required=False,
            validators=[byte_validator()])

        self.write_udp_velocity = field(
            required=False,
            validators=[byte_validator()])

        self.environment = named_value_list_model()

        # fields that have a _bytes and _velocity postfix
        self.velocity_fields = [
            'read_tap',
            'write_tap',
            'read_udp',
            'write_udp'
        ]

        self.state_condition = locks.Condition()
        self.last_velocity_time = None
        self.last_velocity = {}

    def reset(self):
        '''
        Reset.

        Reset the Connection list item to a defined default.
        '''
        connection_item_model.reset(self)
        self.status = status_list_model()
        self.errors = status_list_model()
        self.last_velocity = {}
        self.last_velocity_time = None

    def update_data(self, connection_model_instance):
        '''
        Update the data of the connection list item with the data from a
        privileged request.
        '''
        updated = False
        updated |= self.state.set(
            connection_model_instance.state.get())
        updated |= self.update_connection_status(connection_model_instance)
        new_environment = connection_model_instance.environment.to_json()
        current_environment = self.environment.to_json()
        if new_environment != current_environment:
            self.environment.from_json(
                new_environment)
            updated = True
        return updated

    def update_connection_statistic(self, connection_status_model_instance):
        '''
        Update Connection Statistic.

        Update the values of the connection with the values from the
        connection_status_model_instance.
        '''
        updated = False
        updated |= self.read_tap_bytes.set(
            connection_status_model_instance.read_tap_bytes.get())
        updated |= self.write_tap_bytes.set(
            connection_status_model_instance.write_tap_bytes.get())
        updated |= self.read_udp_bytes.set(
            connection_status_model_instance.read_udp_bytes.get())
        updated |= self.write_udp_bytes.set(
            connection_status_model_instance.write_udp_bytes.get())
        updated |= self.auth_read_bytes.set(
            connection_status_model_instance.auth_read_bytes.get())
        updated |= self.pre_compress_bytes.set(
            connection_status_model_instance.pre_compress_bytes.get())
        updated |= self.post_compress_bytes.set(
            connection_status_model_instance.post_compress_bytes.get())
        updated |= self.pre_decompress_bytes.set(
            connection_status_model_instance.pre_decompress_bytes.get())
        updated |= self.post_decompress_bytes.set(
            connection_status_model_instance.post_decompress_bytes.get())
        updated |= self.update_velocity()
        return updated

    def update_velocity(self):
        '''
        Update Velocity.

        Set _velocity fields from the values that have changed.
        '''
        updated = False
        now = int(round(time.time() * 1000))
        if self.last_velocity_time is None:
            self.last_velocity_time = now

        for vfield in self.velocity_fields:
            if vfield + '_bytes' not in self.__dict__:
                continue
            bytes_value = self.__dict__[vfield + '_bytes'].get()
            if bytes_value is None:
                continue
            bytes = int(bytes_value)
            if vfield not in self.last_velocity:
                self.last_velocity[vfield] = bytes
                continue

            bytes_per_period = (bytes - self.last_velocity[vfield])
            period_length = (now - self.last_velocity_time)
            if bytes_per_period is 0 and period_length < 1000:
                continue
            if bytes_per_period < 0 or period_length < 0:
                continue
            try:
                bytes_per_second = bytes_per_period * 1000 / period_length
                velocity_value = self.__dict__[vfield + '_velocity'].get()
                if velocity_value != bytes_per_second:
                    updated = True
                    self.__dict__[vfield + '_velocity'].set(bytes_per_second)
                self.last_velocity[vfield] = bytes
            except ZeroDivisionError:
                continue
        self.last_velocity_time = now

        return updated
