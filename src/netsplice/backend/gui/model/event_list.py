# -*- coding: utf-8 -*-
# event_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Model for listing event items.
'''
import sys

from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.util.errors import (
    NotRegisteredEventError
)
from netsplice.model.event import (
    event as event_model
)
import netsplice.backend.event.names as names
import netsplice.backend.event.origins as origins
import netsplice.backend.event.types as types

MAX_EVENT_ITEMS = 5


class event_list(marshalable_list):
    '''
    Marshalable model that contains multiple items of
    event_model.
    '''
    def __init__(self):
        marshalable_list.__init__(self, event_model)
        self._index = 0
        self._handled_errors = []

    def handle_error(self, error_code):
        '''
        Handle Error.

        Indicate that the error with the given code is handled in a gui
        component and does not need a raise of the log_window.

        Arguments:
            error_code -- code that should be ignored
        '''
        self._handled_errors.append(str(error_code))

    def append(self, instance):
        '''
        Return the event with the given key. Raises a
        NotFoundError if not found.
        '''
        super(marshalable_list, self).append(instance)
        if len(self) > MAX_EVENT_ITEMS:
            del self[0]

    def get_next_index(self):
        '''
        Get next index.

        Return the current index and increase it afterwards.
        '''
        index = self._index
        self._index += 1
        return index

    def notify_connection_error(self, connection_id):
        '''
        Notify connection error.

        Shorthand method to create an event for the GUI that indicates a
        connection error.
        '''
        event = self.item_model_class()
        event.index.set(self.get_next_index())
        event.origin.set(origins.BACKEND)
        event.name.set(names.CONNECTION_ERROR)
        event.type.set(types.NOTIFY)
        event.data.set(connection_id)
        self.append(event)

    def notify_error(self, error_code):
        '''
        Notify error.

        Shorthand method to create a event for the GUI that indicates an
        error with error_code.
        '''
        if str(error_code) in self._handled_errors:
            return
        event = self.item_model_class()
        event.index.set(self.get_next_index())
        event.origin.set(origins.BACKEND)
        event.name.set(names.ERROR)
        event.type.set(types.NOTIFY)
        event.data.set(error_code)
        self.append(event)

    def notify(self, application, event_name, event_data):
        '''
        Notify.

        Notify the gui about the given event_name with event_data with the
        correct origin.

        Arguments:
            event_name (string): Name of the event to notify.
            event_data ([type]): Data for the event
        '''
        origin = origins.BACKEND
        try:
            origin = type(sys._getframe(1).f_locals['self'])
        except KeyError:
            pass
        registered_origins = application.get_module(
            'event').registered_origins
        if origin not in registered_origins:
            raise NotRegisteredEventError(
                event_name, origin, registered_origins)
        event = self.item_model_class()
        event.index.set(self.get_next_index())
        event.origin.set(str(type(origin)))
        event.name.set(event_name)
        event.type.set(types.NOTIFY)
        event.data.set(event_data)
        self.append(event)
