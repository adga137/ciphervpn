# -*- coding: utf-8 -*-
# test_connection_list_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Connection List Item.

Checks Model functions.
'''

import mock
from netsplice.config import connection as config_connection
from netsplice.backend.gui.model.connection_list_item import (
    connection_list_item
)
from netsplice.backend.privileged.model.response.connection_status import (
    connection_status as connection_status_model
)


def get_test_object():
    item = connection_list_item()
    return item


def test_reset_sets_defaults():
    item = get_test_object()
    item.state.set(config_connection.CONNECTING)
    item.progress.append(1)
    item.errors.append(1)
    item.last_velocity = {'last': 'velocity'}
    item.last_velocity_time = 1
    item.reset()
    assert(item.state.get() is config_connection.INITIALIZED)
    assert(len(item.status) is 0)
    assert(len(item.errors) is 0)
    assert(item.last_velocity == {})
    assert(item.last_velocity_time is None)


def test_update_data_with_different_connected_state_returns_true():
    item = get_test_object()
    item.state.set(config_connection.INITIALIZED)
    update_item = connection_list_item()
    update_item.state.set(config_connection.CONNECTED)
    result = item.update_data(update_item)
    assert(result is True)


@mock.patch(
    'netsplice.backend.gui.model.connection_list_item.'
    'connection_list_item.update_connection_status')
def test_update_data_calls_update_connection_status(
        mock_update_connection_status):
    item = get_test_object()
    update_item = connection_list_item()
    item.update_data(update_item)
    mock_update_connection_status.assert_called()


@mock.patch(
    'netsplice.backend.gui.model.connection_list_item.'
    'connection_list_item.update_velocity')
def test_update_connection_statistic_calls_update_velocity(
        mock_update_velocity):
    item = get_test_object()
    connection_status_item = connection_status_model()
    item.update_connection_statistic(connection_status_item)
    mock_update_velocity.assert_called()


@mock.patch(
    'netsplice.backend.gui.model.connection_list_item.'
    'connection_list_item.update_velocity')
def test_update_connection_statistic_returns_true_when_changed(
        mock_update_velocity):
    item = get_test_object()
    connection_status_item = connection_status_model()
    mock_update_velocity.return_value = False
    item.read_udp_bytes.set(1000)
    connection_status_item.read_udp_bytes.set(2000)
    result = item.update_connection_statistic(connection_status_item)
    assert(result is True)


@mock.patch(
    'netsplice.backend.gui.model.connection_list_item.'
    'connection_list_item.update_velocity')
def test_update_connection_statistic_returns_false_when_unchanged(
        mock_update_velocity):
    item = get_test_object()
    connection_status_item = connection_status_model()
    mock_update_velocity.return_value = False
    item.read_udp_bytes.set(1000)
    connection_status_item.read_udp_bytes.set(1000)
    result = item.update_connection_statistic(connection_status_item)
    assert(result is False)


@mock.patch('time.time')
def test_update_velocity_first_time_returns_false(mock_time):
    item = get_test_object()
    mock_time.return_value = 1
    result = item.update_velocity()
    assert(result is False)


@mock.patch('time.time')
def test_update_velocity_second_time_no_updates_returns_false(mock_time):
    item = get_test_object()
    mock_time.return_value = 1
    item.update_velocity()
    mock_time.return_value = 2
    result = item.update_velocity()
    assert(result is False)


@mock.patch('time.time')
def test_update_velocity_with_updates_returns_true(mock_time):
    item = get_test_object()
    mock_time.return_value = 1
    item.read_udp_bytes.set(1000)
    item.update_velocity()
    item.read_udp_bytes.set(2000)
    mock_time.return_value = 2
    result = item.update_velocity()
    assert(result is True)


@mock.patch('time.time')
def test_update_velocity_invalid_field_name_still_returns_true(mock_time):
    item = get_test_object()
    item.velocity_fields.append('invalid_field')
    mock_time.return_value = 1
    item.read_udp_bytes.set(1000)
    item.update_velocity()
    item.read_udp_bytes.set(2000)
    mock_time.return_value = 2
    result = item.update_velocity()
    assert(result is True)


@mock.patch('time.time')
def test_update_velocity_with_updates_0_period_returns_false(mock_time):
    item = get_test_object()
    mock_time.return_value = 1
    item.read_udp_bytes.set(1000)
    item.update_velocity()
    item.read_udp_bytes.set(2000)
    mock_time.return_value = 1
    result = item.update_velocity()
    assert(result is False)


@mock.patch('time.time')
def test_update_velocity_without_updates_and_short_period_returns_false(
        mock_time):
    item = get_test_object()
    mock_time.return_value = 1
    item.read_udp_bytes.set(1000)
    item.update_velocity()
    item.read_udp_bytes.set(1000)
    mock_time.return_value = 1.1
    result = item.update_velocity()
    assert(result is False)


def test_update_connection_status_with_differnt_connected_state_returns_true():
    item = get_test_object()
    item.state.set(config_connection.INITIALIZED)
    update_item = connection_list_item()
    update_item.state.set(config_connection.CONNECTED)
    result = item.update_connection_status(update_item)
    assert(result is True)


def test_update_connection_status_with_same_connected_state_returns_false():
    item = get_test_object()
    item.state.set(config_connection.CONNECTED)
    update_item = connection_list_item()
    update_item.state.set(config_connection.CONNECTED)
    result = item.update_connection_status(update_item)
    assert(result is False)
