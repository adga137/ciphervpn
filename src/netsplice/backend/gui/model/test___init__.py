# -*- coding: utf-8 -*-
# test___init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Gui Model.

Checks Model functions.
'''

from netsplice.backend.gui.model import model

from netsplice.config import connection as config_connection
from netsplice.backend.connection.connection import (
    connection as connection
)
from netsplice.backend.preferences.model.account_item import (
    account_item as account_item
)
from netsplice.backend.gui.model.connection_list_item import (
    connection_list_item as connection_list_item_model
)

CONNECTION_ID = '1115aa5a-d1df-4ee8-9ede-86898effe163'
OTHER_CONNECTION_ID = '1115aa5a-d1df-4ee8-9ede-86898effe164'


class mock_model(object):
    def __init__(self):
        self.commit_called = False

    def commit(self):
        self.commit_called = True


def get_test_object():
    m = model()
    m.mock = mock_model()
    return m


def test_update_connections_does_nothing_if_same():
    m = get_test_object()
    cl = dict()
    ci_a = connection_list_item_model()
    ci_a.id.set(CONNECTION_ID)
    c_a = connection(None, account_item(None))
    c_a.id.set(CONNECTION_ID)
    c_a.model = ci_a
    ci_b = connection_list_item_model()
    ci_b.id.set(CONNECTION_ID)
    c_b = connection(None, account_item(None))
    c_b.id.set(CONNECTION_ID)
    c_b.model = ci_b
    cl['1'] = (c_a, None)
    cl['2'] = (c_b, None)
    m.connections.append(ci_b)
    assert(len(m.connections) == 1)
    m.update_connections(cl)
    assert(len(m.connections) == 1)


def test_update_connections_adds_new():
    m = get_test_object()
    cl = dict()
    ci_a = connection_list_item_model()
    ci_a.id.set(CONNECTION_ID)
    c_a = connection(None, account_item(None))
    c_a.id.set(CONNECTION_ID)
    c_a.model = ci_a
    ci_b = connection_list_item_model()
    ci_b.id.set(OTHER_CONNECTION_ID)
    c_b = connection(None, account_item(None))
    c_b.id.set(OTHER_CONNECTION_ID)
    c_b.model = ci_b
    cl['1'] = (c_a, None)
    cl['2'] = (c_b, None)

    m.connections.append(ci_a)
    assert(len(m.connections) == 1)
    m.update_connections(cl)
    assert(len(m.connections) == 2)


def test_update_connections_removes_abandoned():
    m = get_test_object()
    cl = dict()
    ci_a = connection_list_item_model()
    ci_a.id.set(CONNECTION_ID)
    c_a = connection(None, account_item(None))
    c_a.id.set(CONNECTION_ID)
    c_a.model = ci_a
    ci_b = connection_list_item_model()
    ci_b.id.set(OTHER_CONNECTION_ID)
    c_b = connection(None, account_item(None))
    c_b.id.set(OTHER_CONNECTION_ID)
    c_b.model = ci_b
    cl['1'] = (c_a, None)
    m.connections.append(ci_a)
    m.connections.append(ci_b)
    assert(len(m.connections) == 2)
    m.update_connections(cl)
    assert(len(m.connections) == 1)


def test_update_connections_moves_failed():
    m = get_test_object()

    cl = dict()
    ci_a = connection_list_item_model()
    ci_a.id.set(CONNECTION_ID)
    c_a = connection(None, account_item(None))
    c_a.id.set(CONNECTION_ID)
    c_a.model = ci_a
    ci_b = connection_list_item_model()
    ci_b.id.set(OTHER_CONNECTION_ID)
    c_b = connection(None, account_item(None))
    c_b.id.set(OTHER_CONNECTION_ID)
    c_b.state = config_connection.DISCONNECTED_FAILURE
    c_b.model = ci_b
    cl['1'] = (c_a, None)
    cl['2'] = (c_b, None)
    m.connections.append(ci_a)
    m.connections.append(ci_b)

    assert(len(m.connections) == 2)
    assert(len(m.failed_connections) == 0)
    m.update_connections(cl)
    assert(len(m.connections) == 1)
    assert(len(m.failed_connections) == 1)


def test_update_connections_moves_failed_once():
    m = get_test_object()

    cl = dict()
    ci_a = connection_list_item_model()
    ci_a.id.set(CONNECTION_ID)
    c_a = connection(None, account_item(None))
    c_a.id.set(CONNECTION_ID)
    c_a.model = ci_a
    ci_b = connection_list_item_model()
    ci_b.id.set(OTHER_CONNECTION_ID)
    c_b = connection(None, account_item(None))
    c_b.id.set(OTHER_CONNECTION_ID)
    c_b.state = config_connection.DISCONNECTED_FAILURE
    c_b.model = ci_b
    cl['1'] = (c_a, None)
    cl['2'] = (c_b, None)
    m.connections.append(ci_a)
    m.connections.append(ci_b)

    assert(len(m.connections) == 2)
    assert(len(m.failed_connections) == 0)
    m.update_connections(cl)
    m.update_connections(cl)
    assert(len(m.connections) == 1)
    assert(len(m.failed_connections) == 1)


def test_get_next_log_index_increases_index():
    m = get_test_object()
    m.log_index.set(99)
    result = m.get_next_log_index()
    assert(result is 100)
    assert(m.log_index.get() is 100)


def test_new_event_returns_instance_with_gui_origin():
    m = get_test_object()
    result = m.new_event()
    assert(result.origin.get() == 'gui')
    assert(result.index.get() >= 0)


def test_add_event_adds():
    m = get_test_object()
    e = m.new_event()
    assert(len(m.events) == 0)
    m.add_event(e)
    assert(len(m.events) == 1)


def test_add_event_commits():
    m = get_test_object()
    m.commit = m.mock.commit
    e = m.new_event()
    m.add_event(e)
    assert(m.mock.commit_called)
