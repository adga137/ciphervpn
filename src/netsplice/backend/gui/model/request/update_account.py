# -*- coding: utf-8 -*-
# update_account.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for connection id's used to validate requests.
This model is also implemented in the Backend.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.validator.boolean import (
    boolean as boolean_validator
)
from netsplice.backend.gui.model.validator.account_name import (
    account_name as account_name_validator
)
from netsplice.backend.gui.model.validator.configuration import (
    configuration as configuration_validator
)
from netsplice.model.validator.account_id import (
    account_id as account_id_validator
)


class update_account(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.id = field(
            required=True,
            validators=[account_id_validator()])

        self.name = field(
            required=True,
            validators=[account_name_validator()])

        self.configuration = field(
            required=True,
            validators=[configuration_validator()])
