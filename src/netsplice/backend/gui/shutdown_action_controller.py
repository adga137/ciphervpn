# -*- coding: utf-8 -*-
# shutdown_action_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Shutdown Controller
Controller implementation for shutting down the backend.
'''

from tornado import gen

from netsplice.util.ipc.middleware import middleware


class shutdown_action_controller(middleware):
    '''
    Shutdown Action Controller.
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    @gen.coroutine
    def post(self):
        '''
        Shutdown the Application and its child processes. During shutdown all
        active connections are disconnected in their required order. The
        backend will wait for 1 second (see shutdown_action_controller.py)
        before it will close its ioloop.
        '''
        if not self.application.shutdown:
            self.application.shutdown = True
            yield self.get_gui().shutdown(self)
            self.set_status(200)
        self.finish()
