# -*- coding: utf-8 -*-
# group_reconnect_action_controller.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Group Controller
Controller implementation for reconnecting all connected accounts
in the correct order.
'''
from tornado import gen, ioloop

from netsplice.backend.connection.group import group
from netsplice.backend.preferences.model.request.group_id import (
    group_id as group_id_model
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.errors import NotFoundError
from netsplice.util.model.errors import ValidationError


class group_reconnect_action_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)
        self.group = group(self.application)

    @gen.coroutine
    def post(self, group_id):
        '''
        Connect all Accounts that are defined by the user.
        '''
        request_model = group_id_model()
        try:
            request_model.id.set(group_id)
            ioloop.IOLoop.current().call_later(
                0, lambda: self.group.reconnect(request_model.id.get()))

            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2114, errors)
            self.set_status(400)
        except NotFoundError as errors:
            self.set_error_code(2115, errors)
            self.set_status(404)
        self.finish()
