# -*- coding: utf-8 -*-
# gui_application_dispatcher.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Dispatch stub for the GUI application part.
'''

from tornado import gen, ioloop

import netsplice.config.backend as config
from netsplice.backend.connection import broker as connection_broker
from netsplice.util import get_logger
from netsplice.util.process.reaper import reaper

logger = get_logger()


class gui_application_dispatcher(object):
    def __init__(self, application):
        self.network = application.network
        self.privileged = application.privileged
        self.unprivileged = application.unprivileged
        self.model = application.model

    def force_shutdown(self):
        try:
            self.network.get_subprocess().terminate()
        except:
            pass
        try:
            self.unprivileged.get_subprocess().terminate()
        except:
            pass
        try:
            self.privileged.get_subprocess().terminate()
        except:
            pass
        ioloop.IOLoop.current().stop()

        reaper_instance = reaper()
        if not reaper_instance.kill_children():
            reaper_instance.kill_self()

    @gen.coroutine
    def shutdown(self, middleware):

        try:
            broker = self.application.get_module('connection').broker
            broker.disconnect_all()
            yield broker.wait_for_done(config.MAX_SHUTDOWN_TIME)
        except Exception as errors:
            logger.warn(
                'Disconnect during shutdown raised a error: %s'
                % (str(errors),))
            pass

        ioloop.IOLoop.current().call_later(
            config.MAX_SHUTDOWN_TIME, lambda: self.shutdown_subprocesses())

    @gen.coroutine
    def shutdown_subprocesses(self):

        self.model.backend_running = False
        self.model.commit()

        logger.info('Shutdown Network.')
        try:
            yield self.network.request_shutdown()
        except Exception as errors:
            logger.warn(
                'Shutdown network raised a error: %s'
                % (str(errors),))

        logger.info('Shutdown Privileged.')
        try:
            yield self.privileged.shutdown()
        except Exception as errors:
            logger.warn(
                'Shutdown privileged raised a error: %s'
                % (str(errors),))

        logger.info('Shutdown Unprivileged.')
        try:
            yield self.unprivileged.shutdown()
        except Exception as errors:
            logger.warn(
                'Shutdown unprivileged raised a error: %s'
                % (str(errors),))

        logger.info('Shutdown Backend.')
        ioloop.IOLoop.current().call_later(
            config.MAX_SHUTDOWN_TIME, lambda: self.force_shutdown())
