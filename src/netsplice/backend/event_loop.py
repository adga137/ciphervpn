# -*- coding: utf-8 -*-
# event_loop.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Event loop for backend app.

Waits for model changes on event-module and
calls dispatcher functions that registered on those events.
'''

from tornado import gen, ioloop
import traceback

import netsplice.backend.event as event_module
import netsplice.backend.event.names as names
import netsplice.backend.event.origins as origins
import netsplice.backend.event.types as types
from netsplice.backend.net.event_controller import (
    event_controller as network_event_controller
)
from netsplice.backend.privileged.event_controller import (
    event_controller as privileged_event_controller
)
from netsplice.backend.unprivileged.event_controller import (
    event_controller as unprivileged_event_controller
)
from netsplice.backend.event.model import model as event_model
from netsplice.backend.log.model import model as log_model
from netsplice.config import flags
from netsplice.config import ipc as config_ipc
from netsplice.config import connection as config_connection
from netsplice.model.log_item import log_item as log_item_model
from netsplice.util.ipc.event_loop import event_loop as event_loop_base

from netsplice.util import get_logger_handler, get_logger

logger = get_logger()

LOOP_TIMER_LOG_MODEL_CHANGE = 0.01
LOOP_TIMER_MODULE_MODEL_CHANGE = 0.1


class event_loop(event_loop_base):
    '''
    Event loop for the backend.

    Requires modules registered and waits for model-change on that modules
    to process events.
    '''

    def __init__(self):
        '''
        Initialize members.

        Requires the user to set application and add modules.
        '''
        event_loop_base.__init__(self, 'Backend')

    @gen.coroutine
    def process_events(self):
        '''
        Process Events.

        Process all events in the event queue. Copy the event_queue before
        processing to avoid events messing up the for loop.

        Decorators:
            gen.coroutine

        Yields:
            self.process_event -- call for each event
        '''
        event_module = self.application.get_module('event')
        events = []
        events.extend(event_module.model)
        del event_module.model[:]
        for event_model_instance in events:
            yield self.process_event(event_model_instance)

    @gen.coroutine
    def process_gui_event(self, event_model_instance):
        '''
        Process Gui Event.

        Process single event that need to update the gui.

        Decorators:
            gen.continue

        Arguments:
            event_model_instance (model.event): event to be processed

        '''
        connection_module = self.application.get_module('connection')
        gui_module = self.application.get_module('gui')
        gui_model = gui_module.model
        gui_model.events_processed = self.events_processed
        event_name = event_model_instance.name.get()
        event_type = event_model_instance.type.get()

        if event_type == types.NOTIFY:
            if event_name == names.LOG_CHANGED:
                # Event for unpriv/priv backends when they posted a log
                log_list_model = self.application.get_module('log').model
                if len(log_list_model) > 0:
                    gui_model.log_index.set(log_list_model[-1].index.get())
                else:
                    gui_model.log_index.set(0)
                gui_model.commit()
            if event_name == names.ERROR:
                gui_model.events.notify_error('')
        gui_model.update_connections(
            connection_module.broker.connections)

    @gen.coroutine
    def process_plugin_event(self, event_model_instance):
        '''
        Process Plugin Event.

        Process single event in plugins that have registered for the event's
        origin.

        Decorators:
            gen.continue

        Arguments:
            event_model_instance (model.event): event to be processed

        Yields:
            gen.coroutine -- plugin coroutine to process event
        '''
        event_destinations = event_module.registered_destinations
        registered_event_destinations = event_destinations.keys()
        for plugin_module in self.plugin_modules:
            if type(plugin_module) not in registered_event_destinations:
                continue
            plugin_events = event_destinations[type(plugin_module)]
            if event_model_instance.name.get() not in plugin_events:
                continue

            try:
                yield plugin_module.process_event(event_model_instance)
            except Exception as errors:
                tb = ''.join(traceback.format_exc(errors))
                logger.warn(
                    'Exception during plugin event handling: %s %s. (%s)'
                    % (str(errors), type(errors), tb))

    @gen.coroutine
    def process_event(self, event_model_instance):
        '''
        Process the event in the event_model_instance.

        Evaluate the event-type, name and origin and call handler functions.
        '''
        self.events_processed += 1

        event_name = event_model_instance.name.get()
        event_type = event_model_instance.type.get()
        event_origin = event_model_instance.origin.get()

        if flags.VERBOSE > 1:
            logger.debug(
                'Event type: %s, origin: %s, name: %s'
                % (event_type, event_origin, event_name))

        if event_type == types.NOTIFY:
            if event_name == names.SUBPROCESS_STARTED:
                if event_origin == network_event_controller:
                    self.application.get_network().set_available()
                if event_origin == privileged_event_controller:
                    self.application.get_privileged().set_available()
                if event_origin == unprivileged_event_controller:
                    self.application.get_unprivileged().set_available()
            if event_name == names.BACKEND_READY:
                broker = self.application.get_module(
                    'connection').broker
                yield broker.autostart(self.application)

        yield self.process_gui_event(event_model_instance)
        yield self.process_plugin_event(event_model_instance)

    @gen.coroutine
    def schedule_wait_for_log_model_change(self):
        '''
        Schedule log_model_change wait.

        Avoid endless stacks and ensure that log model changes are processed.
        '''
        try:
            ioloop.IOLoop.current().call_later(
                config_ipc.LOOP_TIMER_LOG_MODEL_CHANGE,
                self.wait_for_log_model_change)
        except AttributeError:
            pass
        except AssertionError:
            pass

    @gen.coroutine
    def wait_for_log_model_change(self):
        '''
        Get Logs from logbook and store them in the log module.

        Takes any logger.* items and convert them to log items that can be
        displayed in the logviewer.
        '''
        state = yield self.application.model.log.model_changed()
        if state:
            logs = get_logger_handler().take_logs()
            critical = False
            for log_dict_item in logs:
                log_item = log_item_model()
                if isinstance(log_dict_item, (dict,)):
                    log_item.set_from_dict(log_dict_item)
                else:
                    log_item.from_json(log_dict_item.to_json())
                log_item.origin.set(origins.BACKEND)
                critical |= log_item.level.get() == 'CRITICAL'
                self.application.get_module('log').insert_log_entry(log_item)
            if len(logs):
                self.application.get_module('log').model.commit()
            if critical:
                self.application.get_module('gui').model.events.notify_error(
                    '')
                self.application.get_module('gui').model.commit()
        self.schedule_wait_for_log_model_change()

    @gen.coroutine
    def wait_for_module_model_change(self, module):
        '''
        Wait for module model changes.

        Checks the model of the module if it changed and process log and event
        instances in the model.
        '''
        state = yield module.model.model_changed()
        if isinstance(module.model, (event_model,)):
            yield self.process_events()

        if state:
            if isinstance(module.model, (log_model,)):
                if len(module.model) > 0:
                    log_index = module.model[-1].index.get()
                else:
                    log_index = 0
                gui_module_model = self.application.get_module('gui').model
                if log_index != gui_module_model.log_index.get():
                    gui_module_model.log_index.set(log_index)
                    gui_module_model.commit()
        self.schedule_wait_for_module_model_change(module)
