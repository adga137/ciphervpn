# -*- coding: utf-8 -*-
# event_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.model.event import event as event_model
from netsplice.util import get_logger
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.util.errors import NotRegisteredEventError

logger = get_logger()


class event_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def post(self):
        '''
        Add the given event from the privileged backend.
        '''
        request_model = event_model()
        try:
            request_model.from_json(self.request.body.decode('utf-8'))
            module = self.application.get_module('event')
            new_event = module.model.new_event(
                request_model.type.get(), request_model.name.get())
            new_event.data.set(request_model.data.get())
            module.add_event(new_event)
            self.set_status(200)
        except NotRegisteredEventError as errors:
            self.set_error_code(2268, errors)
            self.set_status(500)
            logger.error(str(errors))
        except ValidationError as errors:
            self.set_error_code(2028, errors)
            self.set_status(400)
            logger.error(str(errors))
        except ValueError as errors:
            self.set_error_code(2030, errors)
            self.set_status(400)
            logger.error(str(errors))
        self.finish()
