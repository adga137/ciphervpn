# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
from netsplice.util.ipc.route import get_module_route
from netsplice.backend.privileged_dispatcher import privileged_dispatcher
from netsplice.backend.privileged.model import model as module_model
from netsplice.backend.privileged.log_controller import log_controller
from netsplice.backend.privileged.event_controller import event_controller
from netsplice.backend.event import names as event_names

name = 'privileged'

endpoints = get_module_route(
    'netsplice.backend',
    [
        (r'/module/privileged', 'module'),
        (r'/module/privileged/event', 'event'),
        (r'/module/privileged/log', 'log'),
    ])

model = module_model()


def register_module_events(event_module):
    '''
    Register Module Events.

    Register Events send or received by this module and its components.

    Arguments:
        event_module (backend.event): event_module instance
    '''
    event_module.register_origin(
        log_controller, [
            event_names.LOG_CHANGED
        ]
    )
    event_module.register_origin(
        privileged_dispatcher, [
            event_names.ERROR
        ]
    )
    event_module.register_origin(
        event_controller, [
            event_names.LOG_CHANGED,
            event_names.SUBPROCESS_STARTED,
            event_names.SYSTEMINFO_MODEL
        ]
    )
