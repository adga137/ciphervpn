# -*- coding: utf-8 -*-
# test_connection_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Connection List.

Check model functions.
'''

from netsplice.backend.privileged.model.connection_list_item import (
    connection_list_item as connection_list_item
)
from netsplice.backend.privileged.model.connection_list import (
    connection_list as connection_list
)
from netsplice.util.errors import (ConnectedError, NotFoundError)


CONNECTION_ID = '00000000-0000-0000-0000-000000000001'
MISSING_ID = '00000000-0000-0000-0000-000000000002'
OTHER_CONNECTION_ID = '00000000-0000-0000-0000-000000000003'


def test_add_connection():
    l = connection_list()
    c = l.create_connection(CONNECTION_ID)
    c.type.set('OpenVPN')
    assert(len(l) == 0)
    l.add_connection('unknown', c)
    assert(len(l) == 1)
    # change the local connection, expect no change in the list
    c.id.set(OTHER_CONNECTION_ID)
    assert(len(l) == 1)
    try:
        l.find_by_id(CONNECTION_ID)
        assert(True)
    except NotFoundError:
        assert(False)  # connection id in list changed


def test_create_connection():
    l = connection_list()
    c = l.create_connection(CONNECTION_ID)
    assert(c.id.get() == CONNECTION_ID)
    assert(c.active.get() is False)


def test_delete_connection():
    l = connection_list()
    i = connection_list_item()
    i.id.set(CONNECTION_ID)
    l.append(i)
    assert(len(l) == 1)
    l.delete_connection(CONNECTION_ID)
    assert(len(l) == 0)


def test_delete_connection_on_connected_raises():
    l = connection_list()
    i = connection_list_item()
    i.id.set(CONNECTION_ID)
    i.active.set(True)
    l.append(i)
    try:
        l.delete_connection(CONNECTION_ID)
        assert(False)  # Connected Error Expected
    except ConnectedError:
        assert(True)


def test_delete_connection_on_missing_raises():
    l = connection_list()
    i = connection_list_item()
    i.id.set(CONNECTION_ID)
    l.append(i)
    try:
        l.delete_connection(MISSING_ID)
        assert(False)  # Connected Error Expected
    except NotFoundError:
        assert(True)


def test_find_by_id_with_existing_returns():
    l = connection_list()
    i = connection_list_item()
    i.id.set(CONNECTION_ID)
    l.append(i)
    result = l.find_by_id(CONNECTION_ID)
    assert(result is not None)
    assert(result.id.get() == CONNECTION_ID)


def test_find_by_id_with_nonexisting_raises():
    l = connection_list()
    i = connection_list_item()
    i.id.set(CONNECTION_ID)
    l.append(i)
    try:
        l.find_by_id(MISSING_ID)
        assert(False)  # result received for inexisten id
    except NotFoundError:
        assert(True)
