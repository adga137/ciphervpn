# -*- coding: utf-8 -*-
# route_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for a single route.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable


class route(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.destination = field(
            required=True)

        self.gateway = field(
            required=True)

        self.genmask = field(
            required=True)

        self.flags = field(
            required=True)

        self.metric = field(
            required=True)

        self.ref = field(
            required=True)

        self.use = field(
            required=True)

        self.interface = field(
            required=True)
