# -*- coding: utf-8 -*-
# test_connection_list_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Connection List Item.

Check model functions.
'''

from netsplice.backend.privileged.model.connection_list_item import (
    connection_list_item as connection_list_item
)


def test_update_data_with_same_not_changed():
    i = connection_list_item()
    u = connection_list_item()
    assert(i.update_data(u) is False)
    i.active.set(True)
    u.active.set(True)
    assert(i.update_data(u) is False)


def test_update_data_with_different_returns_changed():
    i = connection_list_item()
    u = connection_list_item()
    i.active.set(False)
    u.active.set(True)
    assert(i.update_data(u) is True)
    i.active.set(True)
    u.active.set(False)
    assert(i.update_data(u) is True)
