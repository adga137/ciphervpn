# -*- coding: utf-8 -*-
# connection_type.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Validator for privileged connection types.
'''
from netsplice.util import get_logger, basestring
from netsplice.util.model.validator import validator

logger = get_logger()


class connection_type(validator):
    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        '''
        Connection Types are nonempty strings.
        '''
        if value is None:
            logger.error('Value is None but should not.')
            return False
        if not isinstance(value, (basestring)):
            logger.error('Value is not a string')
            return False
        if value == '':
            logger.error('Value is empty string')
            return False
        return True
