# -*- coding: utf-8 -*-
# privileged_dispatcher.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Dispatcher for privileged actions.
'''

from socket import error as socket_error

from tornado import gen, httpclient

import netsplice.backend.dispatcher_endpoints as endpoints
from netsplice.backend.privileged.model.request.connection_id import (
    connection_id as connection_id_model
)
from netsplice.backend.privileged.model.response.connection_list import (
    connection_list as connection_list_model
)
from netsplice.backend.privileged.model.response.connection_status import (
    connection_status as connection_status_model
)

from netsplice.backend.privileged.model.response.interface_list import (
    interface_list as interface_list_model
)
from netsplice.backend.privileged.model.response.route_list import (
    route_list as route_list_model
)
from netsplice.model.process import (
    process as process_model
)
from netsplice.util import get_logger
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.ipc.service import service
from netsplice.util.path import get_path_prefix
from netsplice.util.process.dispatcher import dispatcher as process_dispatcher
from netsplice.util.process.errors import ProcessError
from netsplice.util.model.errors import ValidationError


logger = get_logger()


class privileged_dispatcher(service, process_dispatcher):

    def __init__(self, host, port, host_owner, port_owner):
        service.__init__(
            self, host, port, host_owner, port_owner,
            'privileged', 'backend')
        self.wait_for_service = True
        self.process_model = process_model()
        process_dispatcher.__init__(
            self, 'NetsplicePrivilegedApp', {
                '--host': self.host,
                '--port': self.port,
                '--host-owner': self.host_owner,
                '--port-owner': self.port_owner,
                '--config-home': get_path_prefix()
            },
            elevated=True,
            model=self.process_model,
            unique=True)

    @gen.coroutine
    def force_shutdown(self):
        '''
        Force Shutdown.

        Force the Dispatcher to shutdown by killing it elevated if required.
        '''
        try:
            self.force_stop()
        except ProcessError as errors:
            logger.critical('Failed to force shutdown %s' % (str(errors),))

    def start(self):
        '''
        Start.

        Start the service process and notify the gui in case it fails.
        '''
        try:
            self.start_subprocess()
        except ProcessError as errors:
            message = (
                'Cannot start subprocess: %s. The App %s is required to change'
                ' the network and routing preferences on this system. You need'
                ' to allow that it modifies your system.'
                % (str(errors), self._name))
            logger.critical(message)
            gui_model = self.application.get_module('gui').model
            gui_model.events.notify_error('Cannot start subprocess')

    @gen.coroutine
    def interfaces(self):
        try:
            response = yield self.post(
                endpoints.SYSINFO_INTERFACES,
                '{}'
            )

            response_model = interface_list_model()
            response_model.from_json(response.body)

            model = self.application.get_module('report').model
            model.set_interfaces(response_model)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.set_error_code(2067, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.set_error_code(2068, str(errors))
            raise errors
        except ValueError as errors:
            self.set_error_code(2069, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.set_error_code(2070, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def routes(self):
        try:
            response = yield self.post(
                endpoints.SYSINFO_ROUTES,
                '{}'
            )

            response_model = route_list_model()
            response_model.from_json(response.body)

            model = self.application.get_module('report').model
            model.set_routes(response_model)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.set_error_code(2055, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.set_error_code(2056, str(errors))
            raise errors
        except ValueError as errors:
            self.set_error_code(2057, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.set_error_code(2058, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def shutdown(self):
        try:
            response = yield self.post(endpoints.SHUTDOWN, '{}')
            raise gen.Return(response)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.set_error_code(2053, str(errors))
            yield self.force_shutdown()
            raise ServerConnectionError(str(errors))
        except httpclient.HTTPError as errors:
            yield self.force_shutdown()
            self.set_error_code(2054, str(errors))
            raise ServerConnectionError(str(errors))
