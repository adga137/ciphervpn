# -*- coding: utf-8 -*-
# test_integration.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Connection.

Integration tests for Connection
To see (logged) details from the connection/connection broker, add
'nosetests' to the STDERR_LOGGER_NAMES in netsplice/config/log.py
'''
import tornado
import mock
from tornado.testing import AsyncTestCase
from netsplice.util.errors import NotConnectedError, ConnectedError

from netsplice.config.backend import (
    FAILURE_MODE_BREAK, FAILURE_MODE_DISCONNECT, FAILURE_MODE_IGNORE,
    FAILURE_MODE_RECONNECT
)
from netsplice.config import connection as config_connection
from netsplice.config import backend as config_backend

from .test_chain_common import (
    get_app, get_logger, get_test_objects, get_bc_sequence_test_objects,
    get_complex_test_objects, get_sequence_test_objects,
    get_parallel_test_objects, get_single_test_object,
    mock_async_reconnect, mock_async_setup, mock_async_connect,
    mock_async_disconnect, mock_async_connecting,
    mock_async_disconnecting,
    mock_async_disconnect_failure, mock_async_connect_fail,
    mock_async_connect_abort, mock_async_setup_cancel, mock_async_setup_wait,
    mock_async_connect_fail_process,
    print_state, print_logs
)
logger = get_logger()


def module_teardown(module):
    app = get_app()
    app.remove_module('preferences')


class ConnectionIntegrationTests(AsyncTestCase):
    '''
    Test the various combinations how the connection_broker may act on
    events.
    All tests are async and simulate the expected state changes with
    the mock_async_* on the expected connections.
    As the mocks are simple, it is not likely that more/other state changes
    occur. The order of the mock_async_* matters.
    '''

    @tornado.testing.gen_test
    def test_connect_complex_first(self):
        '''
        Check that a connect on the 1st with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info(
            '------ test_connect_complex_first')
        logger.info('a<')
        logger.info('|')
        logger.info('b - c ')
        logger.info('\ - d - e ')
        logger.info('    | - f - g')
        logger.info('    \ - h')
        cb.connect(a, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CREATED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.CREATED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_connect_complex_second(self):
        '''
        Check that a connect on the 2nd with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info(
            '------ test_connect_complex_second')
        logger.info('a')
        logger.info('|')
        logger.info('b<- c ')
        logger.info('\ - d - e ')
        logger.info('    | - f - g')
        logger.info('    \ - h')
        cb.connect(b, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.CREATED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_connect_complex_third(self):
        '''
        Check that a connect on the 3rd with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info(
            '------ test_connect_complex_third')
        logger.info('a')
        logger.info('|')
        logger.info('b ->c<')
        logger.info('\ - d - e ')
        logger.info('    | - f - g')
        logger.info('    \ - h')
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.CONNECTED)
        assert(cb.account_state(d) == config_connection.CREATED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_connect_complex_fourth(self):
        '''
        Check that a connect on the 4th with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info(
            '------ test_connect_complex_fourth')
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ ->d<- e ')
        logger.info('    | - f - g')
        logger.info('    \ - h')
        cb.connect(d, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.CONNECTED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_connect_complex_fifth(self):
        '''
        Check that a connect on the 5th with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info(
            '------ test_connect_complex_fifth')
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ - d ->e<')
        logger.info('    | - f - g')
        logger.info('    \ - h')
        cb.connect(e, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_setup(cb.connections[e.id.get()][0])

        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])
        yield mock_async_connect(cb.connections[e.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.CONNECTED)
        assert(cb.account_state(e) == config_connection.CONNECTED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_connect_complex_sixth(self):
        '''
        Check that a connect on the 6th with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info(
            '------ test_connect_complex_sixth')
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ - d - e ')
        logger.info('    | ->f<- g')
        logger.info('    \ - h')
        cb.connect(f, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_setup(cb.connections[f.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])
        yield mock_async_connect(cb.connections[f.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.CONNECTED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CONNECTED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_connect_complex_seventh(self):
        '''
        Check that a connect on the 7th with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info(
            '------ test_connect_complex_seventh')
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ - d - e ')
        logger.info('    | - f ->g<')
        logger.info('    \ - h')
        cb.connect(g, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_setup(cb.connections[f.id.get()][0])
        yield mock_async_setup(cb.connections[g.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])
        yield mock_async_connect(cb.connections[f.id.get()][0])
        yield mock_async_connect(cb.connections[g.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.CONNECTED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CONNECTED)
        assert(cb.account_state(g) == config_connection.CONNECTED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_connect_complex_eigth(self):
        '''
        Check that a connect on the 8th with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info(
            '------ test_connect_complex_eigth')
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ - d - e ')
        logger.info('    | - f - g')
        logger.info('    \ ->h<')
        cb.connect(h, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_setup(cb.connections[h.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])
        yield mock_async_connect(cb.connections[h.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.CONNECTED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CONNECTED)
        self.stop()

    @tornado.testing.gen_test
    def test_connect_simple_parallel(self):
        '''
        Check that a connect on a sequence only all connecting.
        '''
        (cb, chain_instance, a, b, c) = \
            get_parallel_test_objects()
        logger.info(
            '------ test_connect_simple_parallel')
        logger.info('a - b')
        logger.info('\ - c')
        cb.connect_all(chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTING)
        assert(cb.account_state(c) == config_connection.CONNECTING)

    @tornado.testing.gen_test
    def test_connect_simple_linear_sequence(self):
        '''
        Check that a connect on a sequence only first connecting.
        '''
        (cb, chain_instance, a, b, c) = \
            get_sequence_test_objects()
        logger.info(
            '------ test_connect_simple_linear_sequence')
        logger.info('a - b - c')
        cb.connect_all(chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTING)
        assert(cb.account_state(c) == config_connection.INITIALIZED)

    @tornado.testing.gen_test
    def test_connect_simple_child_sequence(self):
        '''
        Check that a connect on a sequence only first connecting.
        '''
        (cb, chain_instance, a, b, c) = \
            get_bc_sequence_test_objects()
        logger.info(
            '------ test_connect_simple_child_sequence')
        logger.info('a - b')
        logger.info('  \ c')
        cb.connect_all(chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTING)
        assert(cb.account_state(c) == config_connection.INITIALIZED)

    #
    #
    # Complex Disconnect
    #
    #

    @tornado.testing.gen_test
    def test_disconnect_complex_first(self):
        '''
        Check that a connect on the 1st with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a<')
        logger.info('|')
        logger.info('b - c ')
        logger.info('\ - d - e ')
        logger.info('    | - f - g')
        logger.info('    \ - h')
        cb.connect(a, chain_instance)
        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])

        logger.info(
            '------ test_disconnect_complex_first')
        cb.disconnect(a, chain_instance)
        yield mock_async_disconnect(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.DISCONNECTED)
        assert(cb.account_state(b) == config_connection.CREATED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.CREATED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_disconnect_complex_second(self):
        '''
        Check that a connect on the 2nd with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b<- c ')
        logger.info('\ - d - e ')
        logger.info('    | - f - g')
        logger.info('    \ - h')
        cb.connect(b, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])

        logger.info(
            '------ test_disconnect_complex_second')
        cb.disconnect(a, chain_instance)
        yield mock_async_disconnect(cb.connections[b.id.get()][0])
        yield mock_async_disconnect(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.DISCONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.CREATED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_disconnect_complex_third(self):
        '''
        Check that a connect on the 3rd with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b ->c<')
        logger.info('\ - d - e ')
        logger.info('    | - f - g')
        logger.info('    \ - h')
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        logger.info(
            '------ test_disconnect_complex_third')
        cb.disconnect(c, chain_instance)
        yield mock_async_disconnect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.DISCONNECTED)
        assert(cb.account_state(d) == config_connection.CREATED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)

        cb.disconnect(a, chain_instance)
        yield mock_async_disconnect(cb.connections[b.id.get()][0])
        yield mock_async_disconnect(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.DISCONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED)
        assert(cb.account_state(c) == config_connection.DISCONNECTED)
        assert(cb.account_state(d) == config_connection.CREATED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_disconnect_complex_fourth(self):
        '''
        Check that a connect on the 4th with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ ->d<- e ')
        logger.info('    | - f - g')
        logger.info('    \ - h')
        cb.connect(d, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])

        logger.info(
            '------ test_disconnect_complex_fourth')
        cb.disconnect(d, chain_instance)
        yield mock_async_disconnect(cb.connections[d.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.DISCONNECTED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)

        cb.disconnect(a, chain_instance)
        yield mock_async_disconnect(cb.connections[b.id.get()][0])
        yield mock_async_disconnect(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.DISCONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.DISCONNECTED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_disconnect_complex_fifth(self):
        '''
        Check that a connect on the 5th with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ - d ->e<')
        logger.info('    | - f - g')
        logger.info('    \ - h')
        cb.connect(e, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_setup(cb.connections[e.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])
        yield mock_async_connect(cb.connections[e.id.get()][0])

        logger.info(
            '------ test_disconnect_complex_fifth')
        cb.disconnect(a, chain_instance)
        yield mock_async_disconnect(cb.connections[e.id.get()][0])
        yield mock_async_disconnect(cb.connections[d.id.get()][0])
        yield mock_async_disconnect(cb.connections[b.id.get()][0])
        yield mock_async_disconnect(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.DISCONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.DISCONNECTED)
        assert(cb.account_state(e) == config_connection.DISCONNECTED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_disconnect_complex_sixth(self):
        '''
        Check that a connect on the 6th with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ - d - e ')
        logger.info('    | ->f<- g')
        logger.info('    \ - h')
        cb.connect(f, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_setup(cb.connections[f.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])
        yield mock_async_connect(cb.connections[f.id.get()][0])

        logger.info(
            '------ test_disconnect_complex_sixth')
        cb.disconnect(b, chain_instance)
        yield mock_async_disconnect(cb.connections[f.id.get()][0])
        yield mock_async_disconnect(cb.connections[d.id.get()][0])
        yield mock_async_disconnect(cb.connections[b.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.DISCONNECTED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.DISCONNECTED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_disconnect_complex_seventh(self):
        '''
        Check that a connect on the 7th with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ - d - e ')
        logger.info('    | - f ->g<')
        logger.info('    \ - h')
        cb.connect(g, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_setup(cb.connections[f.id.get()][0])
        yield mock_async_setup(cb.connections[g.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])
        yield mock_async_connect(cb.connections[f.id.get()][0])
        yield mock_async_connect(cb.connections[g.id.get()][0])

        logger.info(
            '------ test_disconnect_complex_seventh')
        cb.disconnect(a, chain_instance)
        yield mock_async_disconnect(cb.connections[g.id.get()][0])
        yield mock_async_disconnect(cb.connections[f.id.get()][0])
        yield mock_async_disconnect(cb.connections[d.id.get()][0])
        yield mock_async_disconnect(cb.connections[b.id.get()][0])
        yield mock_async_disconnect(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.DISCONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.DISCONNECTED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.DISCONNECTED)
        assert(cb.account_state(g) == config_connection.DISCONNECTED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_disconnect_complex_eigth(self):
        '''
        Check that a disconnect on the 1th with a nested chain, disconnects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ - d - e ')
        logger.info('    | - f - g')
        logger.info('    \ ->h<')
        cb.connect(h, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_setup(cb.connections[h.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])
        yield mock_async_connect(cb.connections[h.id.get()][0])

        logger.info(
            '------ test_disconnect_complex_eigth')
        print_state(cb)
        print_logs()
        cb.disconnect(a, chain_instance)
        yield mock_async_disconnect(cb.connections[h.id.get()][0])
        yield mock_async_disconnect(cb.connections[d.id.get()][0])
        yield mock_async_disconnect(cb.connections[b.id.get()][0])
        yield mock_async_disconnect(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.DISCONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.DISCONNECTED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.DISCONNECTED)
        self.stop()

    #
    #
    # Complex Reconnect
    #
    #
    @tornado.testing.gen_test
    def test_reconnect_complex_first(self):
        '''
        Check that a connect on the 1st with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a<')
        logger.info('|')
        logger.info('b - c ')
        logger.info('\ - d - e ')
        logger.info('    | - f - g')
        logger.info('    \ - h')
        cb.connect(a, chain_instance)
        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])

        logger.info(
            '------ test_reconnect_complex_first')
        cb.reconnect(a, chain_instance)
        yield mock_async_reconnect(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.RECONNECTED)
        assert(cb.account_state(b) == config_connection.CREATED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.CREATED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_reconnect_complex_second(self):
        '''
        Check that a connect on the 2nd with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b<- c ')
        logger.info('\ - d - e ')
        logger.info('    | - f - g')
        logger.info('    \ - h')
        cb.connect(b, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])

        logger.info(
            '------ test_reconnect_complex_second')
        cb.reconnect(a, chain_instance)
        yield mock_async_reconnect(cb.connections[a.id.get()][0])
        yield mock_async_reconnect(cb.connections[b.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.RECONNECTED)
        assert(cb.account_state(b) == config_connection.RECONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.CREATED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_reconnect_complex_third(self):
        '''
        Check that a connect on the 3rd with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b ->c<')
        logger.info('\ - d - e ')
        logger.info('    | - f - g')
        logger.info('    \ - h')
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        logger.info(
            '------ test_reconnect_complex_third part 1')
        cb.reconnect(c, chain_instance)
        yield mock_async_reconnect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.RECONNECTED)
        assert(cb.account_state(d) == config_connection.CREATED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)

        logger.info(
            '------ test_reconnect_complex_third part 2')
        cb.reconnect(a, chain_instance)
        yield mock_async_reconnect(cb.connections[a.id.get()][0])
        yield mock_async_reconnect(cb.connections[b.id.get()][0])
        yield mock_async_reconnect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.RECONNECTED)
        assert(cb.account_state(b) == config_connection.RECONNECTED)
        assert(cb.account_state(c) == config_connection.RECONNECTED)
        assert(cb.account_state(d) == config_connection.CREATED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_reconnect_complex_fourth(self):
        '''
        Check that a connect on the 4th with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ ->d<- e ')
        logger.info('    | - f - g')
        logger.info('    \ - h')
        cb.connect(d, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])

        logger.info(
            '------ test_reconnect_complex_fourth')
        cb.reconnect(d, chain_instance)
        yield mock_async_reconnect(cb.connections[d.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.RECONNECTED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)

        cb.reconnect(a, chain_instance)
        yield mock_async_reconnect(cb.connections[a.id.get()][0])
        yield mock_async_reconnect(cb.connections[b.id.get()][0])
        yield mock_async_reconnect(cb.connections[d.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.RECONNECTED)
        assert(cb.account_state(b) == config_connection.RECONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.RECONNECTED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_reconnect_complex_fifth(self):
        '''
        Check that a connect on the 5th with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ - d ->e<')
        logger.info('    | - f - g')
        logger.info('    \ - h')
        cb.connect(e, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_setup(cb.connections[e.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])
        yield mock_async_connect(cb.connections[e.id.get()][0])

        logger.info(
            '------ test_reconnect_complex_fifth')
        cb.reconnect(a, chain_instance)
        yield mock_async_reconnect(cb.connections[a.id.get()][0])
        yield mock_async_reconnect(cb.connections[b.id.get()][0])
        yield mock_async_reconnect(cb.connections[d.id.get()][0])
        yield mock_async_reconnect(cb.connections[e.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.RECONNECTED)
        assert(cb.account_state(b) == config_connection.RECONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.RECONNECTED)
        assert(cb.account_state(e) == config_connection.RECONNECTED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_reconnect_complex_sixth(self):
        '''
        Check that a connect on the 6th with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ - d - e ')
        logger.info('    | ->f<- g')
        logger.info('    \ - h')
        cb.connect(f, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_setup(cb.connections[f.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])
        yield mock_async_connect(cb.connections[f.id.get()][0])

        logger.info(
            '------ test_reconnect_complex_sixth')
        cb.reconnect(b, chain_instance)
        yield mock_async_reconnect(cb.connections[b.id.get()][0])
        yield mock_async_reconnect(cb.connections[d.id.get()][0])
        yield mock_async_reconnect(cb.connections[f.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.RECONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.RECONNECTED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.RECONNECTED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_reconnect_complex_seventh(self):
        '''
        Check that a connect on the 7th with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ - d - e ')
        logger.info('    | - f ->g<')
        logger.info('    \ - h')
        cb.connect(g, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_setup(cb.connections[f.id.get()][0])
        yield mock_async_setup(cb.connections[g.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])
        yield mock_async_connect(cb.connections[f.id.get()][0])
        yield mock_async_connect(cb.connections[g.id.get()][0])

        logger.info(
            '------ test_reconnect_complex_seventh')
        cb.reconnect(a, chain_instance)
        yield mock_async_reconnect(cb.connections[a.id.get()][0])
        yield mock_async_reconnect(cb.connections[b.id.get()][0])
        yield mock_async_reconnect(cb.connections[d.id.get()][0])
        yield mock_async_reconnect(cb.connections[f.id.get()][0])
        yield mock_async_reconnect(cb.connections[g.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.RECONNECTED)
        assert(cb.account_state(b) == config_connection.RECONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.RECONNECTED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.RECONNECTED)
        assert(cb.account_state(g) == config_connection.RECONNECTED)
        assert(cb.account_state(h) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_reconnect_complex_eigth(self):
        '''
        Check that a connect on the 8th with a nested chain, connects the
        right instances.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ - d - e ')
        logger.info('    | - f - g')
        logger.info('    \ ->h<')
        cb.connect(h, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_setup(cb.connections[h.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])
        yield mock_async_connect(cb.connections[h.id.get()][0])

        logger.info(
            '------ test_reconnect_complex_eigth')
        cb.reconnect(a, chain_instance)
        yield mock_async_reconnect(cb.connections[a.id.get()][0])
        yield mock_async_reconnect(cb.connections[b.id.get()][0])
        yield mock_async_reconnect(cb.connections[d.id.get()][0])
        yield mock_async_reconnect(cb.connections[h.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.RECONNECTED)
        assert(cb.account_state(b) == config_connection.RECONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        assert(cb.account_state(d) == config_connection.RECONNECTED)
        assert(cb.account_state(e) == config_connection.CREATED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.RECONNECTED)
        self.stop()

    @tornado.testing.gen_test
    def test_connect_second_in_chain_leaves_last_initialized(self):
        '''
        Check that a connect on the second with a 3 account chain connects
        only the first and second and leaves the third initialized.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects()
        logger.info(
            '------ test_connect_second_in_chain_leaves_last_initialized')
        cb.connect(b, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_connect_second_in_chain_first_fails_in_backend(self):
        '''Check that a failed connect on the first leaves other initialized.

        Test that the user/backend cancels the connect before the backend
        process executes.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects()
        logger.info(
            '------ test_connect_second_in_chain_first_fails_in_backend')
        cb.connect(b, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_connect_fail(cb.connections[a.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[a.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[b.id.get()][0])
        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(b) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(c) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_connect_second_in_chain_first_fails_in_process(self):
        '''Check that a failed connect on the first leaves other initialized.

        Test that a backend error (network/remote auth) cancels the connect.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects()
        logger.info(
            '------ test_connect_second_in_chain_first_fails_in_process')
        cb.connect(b, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_connect_fail_process(cb.connections[a.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[a.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[b.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(b) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(c) == config_connection.CREATED)
        self.stop()

    @tornado.testing.gen_test
    def test_connect_second_in_chain_second_fails_in_process(
            self):
        '''Check that the first stays connected when second fails in break
        fail mode.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects()
        logger.info(
            '------ test_connect_second_in_chain_second_fails_in_process')
        cb.connect(b, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect_fail_process(cb.connections[b.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[b.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(c) == config_connection.CREATED)

    @tornado.testing.gen_test
    def test_connect_second_in_chain_second_fails_in_backend(
            self):
        '''Check that the first stays connected when second fails in break
        fail mode.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects()
        logger.info(
            '------ test_connect_second_in_chain_second_fails_in_backend')
        cb.connect(b, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect_fail(cb.connections[b.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[b.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(c) == config_connection.CREATED)

    @tornado.testing.gen_test
    def test_connect_first_in_chain_fails_in_setup(
            self):
        '''Check that the connect is canceled completely when the setup fails.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects()
        logger.info(
            '------ test_connect_first_in_chain_fails_in_setup')
        cb.connect(c, chain_instance)

        yield mock_async_setup_cancel(cb.connections[a.id.get()][0])
        # a receives the cancel, disconnects first, then in reverse order
        # of chain.
        yield mock_async_disconnect_failure(cb.connections[a.id.get()][0])

        yield mock_async_disconnect_failure(cb.connections[c.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[b.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(b) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(c) == config_connection.DISCONNECTED_FAILURE)

    @tornado.testing.gen_test
    def test_connect_second_in_disconnect_chain_second_fails_in_process(
            self):
        '''Check that the first and second are disconnected when the second
        fails in failure mode disconnect.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_DISCONNECT)
        logger.info(
            '------ test_connect_second_in_disconnect_chain_second_fails_in'
            '_process')
        cb.connect(b, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect_fail_process(cb.connections[b.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[b.id.get()][0])
        yield mock_async_disconnect(cb.connections[a.id.get()][0])
        try:
            connection_values = cb.connections.values()
        except AttributeError:
            connection_values = cb.connections.itervalues()
        for (ci, cc) in connection_values:
            print('  S', ci.account.id.get(), ci.state)
        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.DISCONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(c) == config_connection.CREATED)

    @tornado.testing.gen_test
    def test_connect_second_in_reconnect_chain_second_fails_in_process(
            self):
        '''Check that the first and second are reconnected when the second fails
        in failure mode reconnect.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_RECONNECT)
        logger.info(
            '------ test_connect_second_in_reconnect_chain_second_fails_in'
            '_process')
        cb.connect(b, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect_fail_process(cb.connections[b.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[b.id.get()][0])
        # a is connected, b needs to be setup
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_reconnect(cb.connections[a.id.get()][0])
        yield mock_async_reconnect(cb.connections[b.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.RECONNECTED)
        assert(cb.account_state(b) == config_connection.RECONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)

    @tornado.testing.gen_test
    def test_connect_second_in_reconnect_chain_first_aborts(
            self):
        '''Check the first and second are disconnected when the first aborts
        in failure mode reconnect.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_RECONNECT)
        logger.info(
            '------ test_connect_second_in_reconnect_chain_second_fails_in'
            '_process')
        cb.connect(b, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_connect_abort(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.ABORTED)
        assert(cb.account_state(b) == config_connection.ABORTED)
        assert(cb.account_state(c) == config_connection.CREATED)

    @tornado.testing.gen_test
    def test_connect_second_in_reconnect_chain_second_aborts(
            self):
        '''Check the second is disconnected when the second aborts
        in failure mode reconnect.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_RECONNECT)
        logger.info(
            '------ test_connect_second_in_reconnect_chain_second_fails_in'
            '_process')
        cb.connect(b, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect_abort(cb.connections[b.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.ABORTED)
        assert(cb.account_state(c) == config_connection.CREATED)

    @tornado.testing.gen_test
    def test_connect_second_in_reconnect_chain_first_cancels_setup(
            self):
        '''Check that reconnect chains can cancel the setup.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_RECONNECT)
        logger.info(
            '------ test_connect_second_in_reconnect_chain_second_fails_in'
            '_process')
        cb.connect(b, chain_instance)

        yield mock_async_setup_cancel(cb.connections[a.id.get()][0])
        # first the canceled connection disconnects, then the dependent
        yield mock_async_disconnect_failure(cb.connections[a.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[b.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(b) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(c) == config_connection.CREATED)

    @tornado.testing.gen_test
    def test_connect_second_in_reconnect_chain_second_cancels_setup(
            self):
        '''Check that reconnect chains can cancel the setup.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_RECONNECT)
        logger.info(
            '------ test_connect_second_in_reconnect_chain_second_fails_in'
            '_process')
        cb.connect(b, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup_cancel(cb.connections[b.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[b.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(b) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(c) == config_connection.CREATED)

    @tornado.testing.gen_test
    def test_connect_third_in_ignore_chain_second_fails_in_process(
            self):
        '''Check that the first and third stay connected when second fails in
        failure mode ignore.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_IGNORE)
        logger.info(
            '------ test_connect_third_in_ignore_chain_second_fails'
            ' in_process')
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect_fail_process(cb.connections[b.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(c) == config_connection.CONNECTED)

    @tornado.testing.gen_test
    def test_connect_third_in_ignore_chain_first_and_second_fail(
            self):
        '''Check that third gets connected when first second fails in ignore
        failure mode.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_IGNORE)
        logger.info(
            '------ test_connect_third_in_ignore_chain_first_and_second_fail'
            ' in_process')
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect_fail(cb.connections[a.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[a.id.get()][0])
        yield mock_async_connect_fail_process(cb.connections[b.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(b) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(c) == config_connection.CONNECTED)

    @tornado.testing.gen_test
    def test_connect_third_in_chain_third_fails_in_backend(
            self):
        '''Check that first and second stay connected when the third fails.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects()
        logger.info(
            '------ test_connect_third_in_chain_third_fails_in_backend')
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect_fail(cb.connections[c.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.DISCONNECTED_FAILURE)

    @tornado.testing.gen_test
    def test_connect_third_in_chain_third_fails_in_process(
            self):
        '''Check that first and second stay connected when the third fails.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects()
        logger.info(
            '------ test_connect_third_in_chain_third_fails_in_process')
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect_fail_process(cb.connections[c.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.DISCONNECTED_FAILURE)

    @tornado.testing.gen_test
    def test_connect_third_in_chain(
            self):
        '''Check connecting last in chain.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects()
        logger.info('------ test_connect_third_in_chain')
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.CONNECTED)

    @tornado.testing.gen_test
    def test_connect_third_in_chain_after_second(
            self):
        '''Check connecting a partially connected chain
        '''
        (cb, chain_instance, a, b, c) = get_test_objects()
        cb.connect(b, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])

        logger.info('------ test_connect_third_in_chain_after_second')
        cb.connect(c, chain_instance)
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.CONNECTED)

    #
    #
    # Disconnect
    #
    #
    @tornado.testing.gen_test
    def test_disconnect_third_in_chain_after_all_connected(
            self):
        '''Check that disconnecting the third disconnects third.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects()
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        logger.info(
            '------ test_disconnect_third_in_chain_after_all_connected')
        cb.disconnect(c, chain_instance)
        yield mock_async_disconnect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.DISCONNECTED)

    @tornado.testing.gen_test
    def test_disconnect_second_in_chain_after_all_connected(
            self):
        '''Check that disconnecting the second disconnects second and third
        '''
        (cb, chain_instance, a, b, c) = get_test_objects()
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        logger.info(
            '------ test_disconnect_second_in_chain_after_all_connected')
        cb.disconnect(b, chain_instance)

        yield mock_async_disconnect(cb.connections[c.id.get()][0])
        yield mock_async_disconnect(cb.connections[b.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED)
        assert(cb.account_state(c) == config_connection.DISCONNECTED)

    @tornado.testing.gen_test
    def test_disconnect_second_in_chain_after_second_disconnected(
            self):
        '''Check that the connection broker raises a exception.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects()
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        logger.info(
            '------ test_disconnect_second_in_chain_after_second_disconnected')
        cb.disconnect(b, chain_instance)
        yield mock_async_disconnect(cb.connections[c.id.get()][0])
        yield mock_async_disconnect(cb.connections[b.id.get()][0])

        try:
            cb.disconnect(b, chain_instance)
            print_state(cb)
            print_logs()
            assert(False)  # Should raise exception
        except NotConnectedError:
            print_state(cb)
            print_logs()
            assert(cb.account_state(a) == config_connection.CONNECTED)
            assert(cb.account_state(b) == config_connection.DISCONNECTED)
            assert(cb.account_state(c) == config_connection.DISCONNECTED)

    #
    #
    # Reconnect
    #
    #
    @tornado.testing.gen_test
    def test_reconnect_third_in_chain(
            self):
        '''Check that reconnect last only reconnects last.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects()
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        logger.info('------ test_reconnect_third_in_chain')
        cb.reconnect(c, chain_instance)
        yield mock_async_reconnect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.RECONNECTED)

    @tornado.testing.gen_test
    def test_reconnect_second_in_chain(
            self):
        '''Check that reconnect second reconnects second and last.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects()
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        logger.info('------ test_reconnect_second_in_chain')
        cb.reconnect(b, chain_instance)

        print_state(cb)
        print_logs()
        yield mock_async_reconnect(cb.connections[b.id.get()][0])
        yield mock_async_reconnect(cb.connections[c.id.get()][0])

        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.RECONNECTED)
        assert(cb.account_state(c) == config_connection.RECONNECTED)

    @tornado.testing.gen_test
    def test_reconnect_third_in_reconnect_chain(
            self):
        '''Check that reconnect last only reconnects last regardless of failure
        mode.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_RECONNECT)
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        logger.info('------ test_reconnect_third_in_reconnect_chain')
        cb.reconnect(c, chain_instance)
        yield mock_async_reconnect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.RECONNECTED)

    @tornado.testing.gen_test
    def test_reconnect_second_in_reconnect_chain(
            self):
        '''Check that reconnect last reconnects last and second regardless
        of failure mode.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_RECONNECT)
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        logger.info('------ test_reconnect_second_in_reconnect_chain')
        cb.reconnect(b, chain_instance)

        yield mock_async_reconnect(cb.connections[b.id.get()][0])
        yield mock_async_reconnect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.RECONNECTED)
        assert(cb.account_state(c) == config_connection.RECONNECTED)

    @tornado.testing.gen_test
    def test_reconnect_third_in_disconnect_chain(
            self):
        '''Check that reconnect last only reconnects last regardless of failure
        mode.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_DISCONNECT)
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        logger.info('------ test_reconnect_third_in_disconnect_chain')
        cb.reconnect(c, chain_instance)
        yield mock_async_reconnect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.RECONNECTED)

    @tornado.testing.gen_test
    def test_reconnect_second_in_disconnect_chain(
            self):
        '''Check that reconnect last reconnects last and second regardless
        of failure mode.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_DISCONNECT)
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        logger.info('------ test_reconnect_second_in_disconnect_chain')
        cb.reconnect(b, chain_instance)

        yield mock_async_reconnect(cb.connections[b.id.get()][0])
        yield mock_async_reconnect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.RECONNECTED)
        assert(cb.account_state(c) == config_connection.RECONNECTED)

    #
    #
    # Disconnect by plugin
    #
    #
    @tornado.testing.gen_test
    def test_disconnect_second_in_chain_by_plugin(
            self):
        '''Check that the chain is disconnected.

        When a connection in the chain disconnects and the chain has a break
        failure mode, then all children are disconnected.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_BREAK)
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])

        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        logger.info(
            '------ test_disconnect_second_in_chain_by_plugin')
        # simulate a trigger from the plugin (eg remote disconnected)
        broker_connection = cb.connections[b.id.get()][0]
        broker_connection.trigger(config_connection.DISCONNECT)

        yield mock_async_disconnect(cb.connections[b.id.get()][0])
        yield mock_async_disconnect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED)
        assert(cb.account_state(c) == config_connection.DISCONNECTED)

    @tornado.testing.gen_test
    def test_disconnect_second_in_reconnect_chain_by_plugin(
            self):
        '''Check that the chain is reconnected.

        When a connection in the chain disconnects and the chain has a
        reconnect failure mode, then all connections will reconnect.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_RECONNECT)
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        logger.info(
            '------ test_disconnect_second_in_reconnect_chain_by_plugin')
        # simulate a trigger from the plugin (eg remote disconnected)
        broker_connection = cb.connections[b.id.get()][0]
        broker_connection.trigger(config_connection.DISCONNECT)

        yield mock_async_disconnect(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])

        yield mock_async_reconnect(cb.connections[a.id.get()][0])
        yield mock_async_reconnect(cb.connections[b.id.get()][0])
        yield mock_async_reconnect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.RECONNECTED)
        assert(cb.account_state(b) == config_connection.RECONNECTED)
        assert(cb.account_state(c) == config_connection.RECONNECTED)

    @tornado.testing.gen_test
    def test_disconnect_second_in_disconnect_chain_by_plugin(
            self):
        '''Check that the chain is completely disconnected.

        When a connection in the chain disconnects and the chain as a
        disconnect failure mode, then all connections of the chain have to
        disconnect.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_DISCONNECT)
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        logger.info(
            '------ test_disconnect_second_in_disconnect_chain_by_plugin')
        # simulate a trigger from the plugin (eg remote disconnected)
        broker_connection = cb.connections[b.id.get()][0]
        broker_connection.trigger(config_connection.DISCONNECT)

        yield mock_async_disconnect(cb.connections[b.id.get()][0])
        yield mock_async_disconnect(cb.connections[c.id.get()][0])
        yield mock_async_disconnect(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.DISCONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED)
        assert(cb.account_state(c) == config_connection.DISCONNECTED)

    @tornado.testing.gen_test
    def test_disconnect_second_in_break_chain_by_plugin(
            self):
        '''Check that the child connection are disconnected.

        When a connection in the chain disconnects, and the chain has a break
        failure mode, then the child-connections have to be disconnected
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_BREAK)
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        logger.info(
            '------ test_disconnect_second_in_break_chain_by_plugin')
        # simulate a trigger from the plugin (eg remote disconnected)
        broker_connection = cb.connections[b.id.get()][0]
        broker_connection.trigger(config_connection.DISCONNECT)

        yield mock_async_disconnect(cb.connections[b.id.get()][0])
        yield mock_async_disconnect(cb.connections[c.id.get()][0])

        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED)
        assert(cb.account_state(c) == config_connection.DISCONNECTED)

    @tornado.testing.gen_test
    def test_disconnect_all_disconnects_all(
            self):
        '''Check that the connection connections are disconnected.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_BREAK)
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])

        logger.info(
            '------ test_disconnect_all_disconnects_all')

        print_state(cb)
        cb.disconnect_all()
        yield mock_async_disconnect(cb.connections[c.id.get()][0])
        yield mock_async_disconnect(cb.connections[b.id.get()][0])
        yield mock_async_disconnect(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()

        assert(cb.account_state(a) == config_connection.DISCONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED)
        assert(cb.account_state(c) == config_connection.DISCONNECTED)

    @tornado.testing.gen_test
    def test_disconnect_all_disconnects_all_connecting(
            self):
        '''Check that the connection connections are disconnected.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_BREAK)
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connecting(cb.connections[c.id.get()][0])

        logger.info(
            '------ test_disconnect_all_disconnects_all_connecting')

        print_state(cb)
        cb.disconnect_all()
        yield mock_async_disconnect(cb.connections[c.id.get()][0])
        yield mock_async_disconnect(cb.connections[b.id.get()][0])
        yield mock_async_disconnect(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()

        assert(cb.account_state(a) == config_connection.DISCONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED)
        assert(cb.account_state(c) == config_connection.DISCONNECTED)

    @tornado.testing.gen_test
    def test_disconnect_all_disconnects_all_during_connecting(
            self):
        '''Check that the connection connections are disconnected.

        disconnect_all is called when b is connecting and c is waiting.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_BREAK)
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])

        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connecting(cb.connections[b.id.get()][0])

        logger.info(
            '------ test_disconnect_all_disconnects_all_during_connecting')

        print_state(cb)

        cb.disconnect_all()

        yield mock_async_disconnect_failure(cb.connections[c.id.get()][0])
        yield mock_async_disconnect(cb.connections[b.id.get()][0])
        yield mock_async_disconnect(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()

        assert(cb.account_state(a) == config_connection.DISCONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED)
        assert(cb.account_state(c) == config_connection.DISCONNECTED_FAILURE)

    @tornado.testing.gen_test
    def test_disconnect_all_disconnects_all_during_setup(
            self):
        '''Check that the connection connections are disconnected.

        disconnect_all is called when b is connecting and c is waiting.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_BREAK)
        cb.connect(c, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup_wait(cb.connections[b.id.get()][0])

        logger.info(
            '------ test_disconnect_all_disconnects_all_during_connecting')

        print_state(cb)

        cb.disconnect_all()

        yield mock_async_disconnect_failure(cb.connections[c.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[b.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()

        assert(cb.account_state(a) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(b) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(c) == config_connection.DISCONNECTED_FAILURE)

    @tornado.testing.gen_test
    def test_disconnect_disconnects_during_setup_not_waiting(
            self):
        '''Check the connections are disconnected when without waiting.

        disconnect is called when a is connecting and b&c are waiting.
        '''
        (cb, chain_instance, a, b, c) = get_test_objects(
            failure_mode=FAILURE_MODE_BREAK)
        cb.connect(c, chain_instance)

        yield mock_async_setup_wait(cb.connections[a.id.get()][0])

        logger.info(
            '------ test_disconnect_disconnects_during_setup')

        print_state(cb)

        cb.disconnect(b, chain_instance)

        yield mock_async_disconnect_failure(cb.connections[c.id.get()][0])
        yield mock_async_disconnect_failure(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])

        print_state(cb)
        print_logs()

        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.DISCONNECTED_FAILURE)
        assert(cb.account_state(c) == config_connection.DISCONNECTED_FAILURE)

    @tornado.testing.gen_test
    def test_connect_other_chain_during_connect_creates_connections(
            self):
        '''Connections are created without waiting when chain is connecting.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ - d - e ')
        logger.info('    | - f - g')
        logger.info('    \ ->h<')
        cb.connect(h, chain_instance)

        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_setup(cb.connections[h.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])
        assert(len(cb.connections) == 4)
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(d) == config_connection.CONNECTED)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CONNECTING)

        # now in between, connect another chain
        cb.connect(g, chain_instance)
        # f & g are now created, the connection instances exist, the gui may
        # display the connecting state
        assert(len(cb.connections) == 6)
        assert(cb.account_state(f) == config_connection.CREATED)
        assert(cb.account_state(g) == config_connection.CREATED)
        assert(cb.account_state(h) == config_connection.CONNECTING)

        yield mock_async_connect(cb.connections[h.id.get()][0])
        # switch to queued chain
        yield mock_async_setup(cb.connections[f.id.get()][0])
        yield mock_async_setup(cb.connections[g.id.get()][0])
        yield mock_async_connect(cb.connections[f.id.get()][0])
        yield mock_async_connect(cb.connections[g.id.get()][0])

        print_state(cb)
        assert(cb.account_state(f) == config_connection.CONNECTED)
        assert(cb.account_state(g) == config_connection.CONNECTED)
        assert(cb.account_state(h) == config_connection.CONNECTED)

    @tornado.testing.gen_test
    def test_connect_all_does_not_connect_none_mode(
            self):
        '''Connections are created without waiting when chain is connecting.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ - d - e ')
        logger.info('    | - f - g')
        logger.info('    \ ->h<')
        for link in chain_instance:
            if link.account.id.get() == b.id.get():
                link.connect_mode = config_backend.CONNECT_MODE_NONE
        cb.connect_all(chain_instance)
        logger.info('-----------')
        print_state(cb)
        print_logs()

        assert(len(cb.connections.keys()) == 2)
        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTED)
        assert(cb.account_state(c) == config_connection.CREATED)

    @tornado.testing.gen_test
    def test_connect_disconnect_connect_does_queue_connect(
            self):
        '''Connections are fully disconnected before new connections are setup
        connect -> disconnect -> disconnecting -> disconnected -> connect
                               ^
                               new connect
        '''
        (cb, chain_instance, a) = \
            get_single_test_object()
        logger.info('a')
        cb.connect_all(chain_instance)
        logger.info('-----------')
        print_state(cb)
        print_logs()

        assert(len(cb.connections.keys()) == 1)
        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        cb.disconnect(a, chain_instance)
        yield mock_async_disconnecting(cb.connections[a.id.get()][0])
        assert(cb.account_state(a) == config_connection.DISCONNECTING_PROCESS)
        # still in disconnecting state, but the user clicks connect already
        print_state(cb)
        print_logs()
        try:
            cb.connect(a, chain_instance)
            assert(False)  # Expect Connected Error in this state
        except ConnectedError:
            pass
        # disconnect continues
        yield mock_async_disconnect(cb.connections[a.id.get()][0])
        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.DISCONNECTED)
        print('Connecting')
        # now disconnected, connect is not a issue
        cb.connect(a, chain_instance)
        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)

    @tornado.testing.gen_test
    def test_connect_in_same_chain_during_connect(
            self):
        '''Connections are created without waiting when chain is connecting.
        '''
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        logger.info('a')
        logger.info('|')
        logger.info('b - c')
        logger.info('\ - d - e ')
        logger.info('    | - f - g')
        logger.info('    \ ->h<')
        for link in chain_instance:
            if link.account.id.get() == b.id.get():
                link.connect_mode = config_backend.CONNECT_MODE_NONE
        cb.connect(c, chain_instance)
        logger.info('-----------')
        print_state(cb)
        print_logs()

        assert(len(cb.connections.keys()) == 3)
        yield mock_async_setup(cb.connections[a.id.get()][0])
        yield mock_async_setup(cb.connections[b.id.get()][0])
        yield mock_async_setup(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[a.id.get()][0])
        print_state(cb)
        print_logs()
        assert(cb.account_state(a) == config_connection.CONNECTED)
        assert(cb.account_state(b) == config_connection.CONNECTING)
        logger.info('-----------')
        cb.connect(d, chain_instance)
        print_state(cb)
        print_logs()
        assert(len(cb.connections.keys()) == 4)
        assert(cb.account_state(d) == config_connection.CREATED)
        yield mock_async_setup(cb.connections[d.id.get()][0])
        yield mock_async_connect(cb.connections[b.id.get()][0])
        yield mock_async_connect(cb.connections[c.id.get()][0])
        yield mock_async_connect(cb.connections[d.id.get()][0])
        print_state(cb)
        print_logs()
        assert(cb.account_state(c) == config_connection.CONNECTED)
        assert(cb.account_state(d) == config_connection.CONNECTED)
