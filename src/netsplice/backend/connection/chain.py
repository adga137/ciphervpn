# -*- coding: utf-8 -*-
# chain.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Chain from Preferences.
'''

from netsplice.backend.connection.link import link as link_type
from netsplice.config import backend as config_backend

from netsplice.util.errors import NotFoundError
from netsplice.util import get_logger

logger = get_logger()


class chain(list):
    '''
    Chain

    Chain holds a list of links that are connected.

    Extends:
        list
    '''
    def __init__(
            self, links,
            failure_mode=config_backend.FAILURE_MODE_BREAK,
            connect_mode=config_backend.CONNECT_MODE_SEQUENCE):
        '''
        Initialize Chain.

        Create a chain with the given accounts as links.

        Arguments:
            links (list): list of accounts or chains that are to be resolved
                into links in this chain.

        Keyword Arguments:
            failure_mode (enum): mode how to handle disconnects
                (default: {config_backend.FAILURE_MODE_BREAK})
            connect_mode (enum): mode how to handle connects of children
                (default: {config_backend.CONNECT_MODE_SEQUENCE})

        Raises:
            NotFoundError -- [description]
        '''

        self.failure_mode = failure_mode
        self.connect_mode = connect_mode
        self.register(links)

    def find_by_account_id(self, account_id):
        '''
        Find by Account Id.

        Find the link in the chain with the given account_id.

        Arguments:
            account_id (string): id of account

        Returns:
            (link): instance that holds the account with the

        Raises:
            NotFoundError -- no link with an account matching the id is in
                the chain.
        '''
        for link in self:
            if link.account is None:
                continue
            if link.account.id.get() != account_id:
                continue
            return link
        raise NotFoundError(
            'No Link responsible for %s in this chain.'
            % (account_id,))

    def register(self, link_chain_list):
        '''
        Register

        Register a list of accounts or chains in the current chain. Each
        account in the chain_list is wrapped as link and appended to the
        current chain. The before and after attributes of the link are updated
        appropriately. When a chain is a account in the chain list the before
        of the first element in that chain is linked to the current last and
        all links of the chain are copied to the current chain.

        Arguments:
            link_chain_list (list): list of links or chains
        '''
        before_link = None
        for link_chain_item in link_chain_list:
            if before_link is None and len(self) > 0:
                # use a common before for all links in the given
                # link_chain_list when the chain is empty, the first
                # link_chain_item_link will be used as before for all
                # subsequent links
                before_link = self[-1]

            if isinstance(link_chain_item, (chain,)):
                # copy links to current chain
                link_chain_item[0].register_before(before_link)
                before_link.register_after(link_chain_item[0])
                for link in link_chain_item:
                    link.chain = self
                    if link.get_parent() is None:
                        link.parent = before_link
                self.extend(link_chain_item)
            else:
                # create link in chain
                new_link = link_type(
                    link_chain_item, self,
                    self.failure_mode, self.connect_mode)
                if before_link is not None:
                    new_link.register_before(before_link)
                    new_link.parent = before_link
                    before_link.register_after(new_link)

                self.append(new_link)
