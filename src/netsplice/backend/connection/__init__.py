# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
from netsplice.backend.connection.model import model as module_model
from netsplice.backend.connection.connection_broker import (
    connection_broker as connection_broker
)
from netsplice.backend.connection.connection import (
    connection as connection
)
from netsplice.backend.event import names as event_names
from netsplice.backend.event import origins as event_origins
from netsplice.config import connection as config_connection

name = 'connection'

endpoints = []

model = module_model()
broker = connection_broker()


def register_module_events(event_module):
    '''
    Register Module Events.

    Register Events send or received by this module and its components.

    Arguments:
        event_module (backend.event): event_module instance
    '''
    event_module.register_origin(
        connection, config_connection.TRANSIENT_STATES)
