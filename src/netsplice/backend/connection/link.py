# -*- coding: utf-8 -*-
# link.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Link of a Chain.
'''

from netsplice.config import backend as config_backend

from netsplice.util import get_logger

logger = get_logger()


class link(object):
    '''
    Link

    Link in a Chain that supports before and after links.
    This is a in-memory model for the state machine to easily find the
    dependents and conditions for a account.
    '''
    def __init__(
            self, account_model_instance,
            chain,
            failure_mode=config_backend.FAILURE_MODE_BREAK,
            connect_mode=config_backend.CONNECT_MODE_SEQUENCE):
        '''
        Initialize Link.

        Create a link in the given chain.

        Arguments:
            account_model_instance (account): account referenced by the link
            chain (chain): chain owning the link

        Keyword Arguments:
            failure_mode (enum): mode how to handle disconnects \
                (default: {config_backend.FAILURE_MODE_BREAK})
            connect_mode (enum): mode how to handle connects of children \
                (default: {config_backend.CONNECT_MODE_SEQUENCE})
        '''
        self.account = account_model_instance
        self.after = []
        self.before = []
        self.parent = None
        self.chain = chain
        self.failure_mode = failure_mode
        self.connect_mode = connect_mode

    def depends(self, check_connect_mode=False):
        '''
        List of links depending on this instance.

        Compile a list of accounts that are dependent on this instance by its
        after links.

        Returns:
            (list): List of accounts.
        '''
        dependents = []
        for after_link in self.after:
            if check_connect_mode and \
                    after_link.get_parent() and \
                    after_link.get_parent().connect_mode == \
                    config_backend.CONNECT_MODE_NONE:
                break

            dependents.append(after_link.account)
            dependents.extend(after_link.depends(check_connect_mode))
        return dependents

    def get_parent(self):
        '''
        Get the parent from the link.
        '''
        return self.parent

    def prior(self):
        '''
        List of links that are 'more important'.

        Compile a list of accounts that are required to be processed before
        this instance by its before links.

        Returns:
            (list): List of accounts.
        '''
        prepends = []
        for before_link in self.before:
            prepends.append(before_link.account)
            prepends.extend(before_link.prior())
        return prepends

    def register_after(self, new_after):
        '''
        Register After.

        Register the given new_after link to this link.

        Arguments:
            new_after (link): Link to be a follower of this link
        '''
        self.after.append(new_after)

    def register_before(self, new_before):
        '''
        Register Before.

        Register the given new_before link to this link.

        Arguments:
            new_before (link): Link to be a previous of this link
        '''
        self.before.append(new_before)

    def __repr__(self):
        '''
        String Representation

        Debug string to understand how the link is positioned in the chain
        and what priors and depends exist.

        Returns:
            string -- formatted output of a link
        '''
        info = 'link: '
        for link in self.chain:
            if link == self:
                info += ' >'
            else:
                info += ' '
            if link.account is None:
                info += '??'
            else:
                info += link.account.id.get()[:2]
                if link.parent is not None:
                    info += (
                        '(p:%s%s)'
                        % (link.parent.account.id.get()[:2],
                            link.parent.connect_mode))
            if link == self:
                info += '<'
        info += '\n  prior: (' + str(len(self.prior())) + ')'
        for d in self.prior():
            info += ' ' + d.id.get()[:2]
        info += '\n  depends: (' + str(len(self.depends())) + ')'
        for d in self.depends():
            info += ' ' + d.id.get()[:2]
        return info
