# -*- coding: utf-8 -*-
# test_chain_common.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Shared code for the test_chain, test_integration and test_connection_broker.
'''
import sys
import netsplice.backend.log as log_module
import netsplice.backend.event as event_module
import netsplice.backend.preferences as preferences_module

from tornado import gen

from netsplice.backend.connection.connection_broker import connection_broker
from netsplice.backend.connection.connection import connection

from netsplice.util.ipc.application import application as application

from netsplice.config.backend import (
    FAILURE_MODE_BREAK,
    CONNECT_MODE_PARALLEL,
    CONNECT_MODE_SEQUENCE,
)
from netsplice.config import connection as config_connection

from netsplice.backend.preferences.model import model as preferences_model
from netsplice.backend.preferences.model.account_item import (
    account_item as account
)
from netsplice.backend.connection.chain import chain

from netsplice.util import get_logger, take_logs
logger = get_logger()

this = sys.modules[__name__]

this.app = None


def get_app():
    if this.app is not None:
        return this.app
    app = application()
    app.add_module(log_module)
    app.add_module(event_module)
    event_module.register_origin(
        connection, config_connection.TRANSIENT_STATES)
    this.app = app

    return app

FIRST_ACCOUNT_ID = '11c5aa5a-d1df-4ee8-9ede-86898effe163'
SECOND_ACCOUNT_ID = '22c5aa5a-d1df-4ee8-9ede-86898effe163'
THIRD_ACCOUNT_ID = '33c5aa5a-d1df-4ee8-9ede-86898effe163'
FOURTH_ACCOUNT_ID = '44c5aa5a-d1df-4ee8-9ede-86898effe163'
FIFTH_ACCOUNT_ID = '55c5aa5a-d1df-4ee8-9ede-86898effe163'
SIXTH_ACCOUNT_ID = '66c5aa5a-d1df-4ee8-9ede-86898effe163'
SEVENTH_ACCOUNT_ID = '77c5aa5a-d1df-4ee8-9ede-86898effe163'
EIGHT_ACCOUNT_ID = '88c5aa5a-d1df-4ee8-9ede-86898effe163'
OTHER_ACCOUNT_ID = '00c5aa5a-d1df-4ee8-9ede-86898effe163'


def print_logs():
    '''
    Print Logs

    Print the logs produced by classes in netsplice (logbook).
    '''
    logs = take_logs()
    for item in logs:
        print(item['formated_message'])


def print_state(broker):
    '''
    Print State

    Print the state in the given broker.
    '''

    print(broker, broker.state)
    print(
        'broker summary: %d'
        % (len(broker.connections),))
    active_chains = []
    try:
        connection_values = broker.connections.itervalues()
    except AttributeError:
        connection_values = broker.connections.values()
    for (connection_instance, broker_chain) in connection_values:
        if broker_chain not in active_chains:
            active_chains.append(broker_chain)
    for active_chain in active_chains:
        print('chain: %s' % (active_chain,))
        for (connection_instance, broker_chain) in connection_values:
            if broker_chain is not active_chain:
                continue
            key = connection_instance.account.id.get()
            name = connection_instance.account.name.get()

            print(
                ' connection %s %s: %s'
                % (name, key[:2], connection_instance.state))


@gen.coroutine
def mock_async_setup(connection_instance):
    '''mock setup

    This mock simulates a successful setup state change in the given
    connection.
    '''
    logger.info(
        '%s %s' % (connection_instance.id.get(), connection_instance.state))
    if connection_instance.state == config_connection.SETTING_UP:
        connection_instance.trigger(config_connection.SETUP_FINISH)
        yield mock_async_setup(connection_instance)
        raise gen.Return(True)
    if connection_instance.state == config_connection.CREATED:
        connection_instance.trigger(config_connection.SETUP)
        yield mock_async_setup(connection_instance)
        raise gen.Return(True)
    raise gen.Return(False)


@gen.coroutine
def mock_async_setup_wait(connection_instance):
    '''mock setup wait

    This mock simulates a setup that returns when it is waiting for testing
    events that occur while the user enters the password.
    '''
    logger.info(
        '%s %s' % (connection_instance.id.get(), connection_instance.state))
    if connection_instance.state == config_connection.CREATED:
        connection_instance.trigger(config_connection.SETUP)
        yield mock_async_setup_wait(connection_instance)
        raise gen.Return(True)
    raise gen.Return(False)


@gen.coroutine
def mock_async_setup_cancel(connection_instance):
    '''mock setup cancel

    This mock simulates a canceled setup state change in the given
    connection.
    '''
    logger.info(
        '%s %s' % (connection_instance.id.get(), connection_instance.state))
    if connection_instance.state == config_connection.SETTING_UP:
        connection_instance.trigger(config_connection.SETUP_CANCEL)
        yield mock_async_setup(connection_instance)
        raise gen.Return(True)
    if connection_instance.state == config_connection.CREATED:
        connection_instance.trigger(config_connection.SETUP)
        yield mock_async_setup(connection_instance)
        raise gen.Return(True)
    raise gen.Return(False)


@gen.coroutine
def mock_async_connect(connection_instance):
    '''mock connect

    This mock simulates a successful connect state change in the given
    connection.
    '''
    if connection_instance.state == config_connection.CONNECTING:
        connection_instance.trigger(config_connection.CONNECT_PROCESS)
        yield mock_async_connect(connection_instance)
        raise gen.Return(True)
    if connection_instance.state == config_connection.CONNECTING_PROCESS:
        connection_instance.trigger(config_connection.CONNECT_FINISH)
        yield mock_async_connect(connection_instance)
        raise gen.Return(True)
    raise gen.Return(False)


@gen.coroutine
def mock_async_connecting(connection_instance):
    '''mock connecting

    This mock simulates a successful connect state change in the given
    connection to the point that it is left connecting.
    '''
    if connection_instance.state == config_connection.CONNECTING:
        connection_instance.trigger(config_connection.CONNECT_PROCESS)
        yield mock_async_connecting(connection_instance)
        raise gen.Return(True)
    raise gen.Return(False)


@gen.coroutine
def mock_async_connect_fail(connection_instance):
    '''mock connect fail

    This mock simulates a failure in the backend during connect.
    '''
    if connection_instance.state == config_connection.CONNECTING:
        connection_instance.trigger(config_connection.DISCONNECT_FAILURE)
        yield mock_async_connect_fail(connection_instance)
        raise gen.Return(True)
    raise gen.Return(False)


@gen.coroutine
def mock_async_connect_abort(connection_instance):
    '''mock connect abort

    This mock simulates an abort failure in the backend during connect.
    '''
    if connection_instance.state == config_connection.CONNECTING:
        connection_instance.trigger(config_connection.DISCONNECT_ABORT)
        yield mock_async_connect_fail(connection_instance)
        raise gen.Return(True)
    raise gen.Return(False)


@gen.coroutine
def mock_async_connect_fail_process(connection_instance):
    '''mock connect fail process

    This mock simulates a failure in the process/remote during connect.
    '''
    if connection_instance.state == config_connection.CONNECTING:
        connection_instance.trigger(config_connection.CONNECT_PROCESS)
        yield mock_async_connect_fail_process(connection_instance)
        raise gen.Return(True)
    if connection_instance.state == config_connection.CONNECTING_PROCESS:
        connection_instance.trigger(config_connection.DISCONNECT_FAILURE)
        yield mock_async_connect_fail_process(connection_instance)
        raise gen.Return(True)
    raise gen.Return(False)


@gen.coroutine
def mock_async_disconnect(connection_instance):
    '''mock disconnect

    This mock simulates a disconnect.
    '''
    if connection_instance.state == config_connection.DISCONNECTING:
        connection_instance.trigger(config_connection.DISCONNECT_PROCESS)
        yield mock_async_disconnect(connection_instance)
        raise gen.Return(True)
    if connection_instance.state == config_connection.DISCONNECTING_PROCESS:
        connection_instance.trigger(config_connection.DISCONNECT_FINISH)
        yield mock_async_disconnect(connection_instance)
        raise gen.Return(True)
    raise gen.Return(False)


@gen.coroutine
def mock_async_disconnect_failure(connection_instance):
    '''mock disconnect failure

    This mock simulates a disconnect that was caused by a failure.
    '''
    if connection_instance.state == config_connection.DISCONNECTING_FAILURE:
        connection_instance.trigger(
            config_connection.DISCONNECT_FAILURE_PROCESS)
        yield mock_async_disconnect_failure(connection_instance)
        raise gen.Return(True)
    if connection_instance.state == \
            config_connection.DISCONNECTING_FAILURE_PROCESS:
        connection_instance.trigger(
            config_connection.DISCONNECT_FAILURE_FINISH)
        yield mock_async_disconnect_failure(connection_instance)
        raise gen.Return(True)
    raise gen.Return(False)


@gen.coroutine
def mock_async_disconnecting(connection_instance):
    '''mock disconnect

    This mock simulates a disconnect.
    '''
    if connection_instance.state == config_connection.DISCONNECTING:
        connection_instance.trigger(config_connection.DISCONNECT_PROCESS)
        yield mock_async_disconnecting(connection_instance)
        raise gen.Return(True)
    raise gen.Return(False)



@gen.coroutine
def mock_async_reconnect(connection_instance):
    '''mock reconnect

    This mock simulates a reconnect.
    '''
    if connection_instance.state == config_connection.RECONNECTING:
        connection_instance.trigger(config_connection.RECONNECT_PROCESS)
        yield mock_async_reconnect(connection_instance)
        raise gen.Return(True)
    if connection_instance.state == config_connection.RECONNECTING_PROCESS:
        connection_instance.trigger(config_connection.RECONNECT_FINISH)
        yield mock_async_reconnect(connection_instance)
        raise gen.Return(True)
    raise gen.Return(False)


def get_test_a_bc_chain(
        failure_mode=FAILURE_MODE_BREAK, connect_mode=CONNECT_MODE_SEQUENCE):
    '''create a test chain

    Return a chain instance and the accounts of the chain instance.
    a - b
      \ c
    '''
    a = account(None)
    a.id.set(FIRST_ACCOUNT_ID)
    a.name.set(FIRST_ACCOUNT_ID[:2])
    b = account(None)
    b.id.set(SECOND_ACCOUNT_ID)
    b.name.set(SECOND_ACCOUNT_ID[:2])
    c = account(None)
    c.id.set(THIRD_ACCOUNT_ID)
    c.name.set(THIRD_ACCOUNT_ID[:2])
    sc_b = chain(
        [b, c], failure_mode=failure_mode, connect_mode=connect_mode)

    chain_instance = chain(
        [a, sc_b], failure_mode=failure_mode, connect_mode=connect_mode)
    return (chain_instance, a, b, c)


def get_test_a_b_c_chain(
        failure_mode=FAILURE_MODE_BREAK, connect_mode=CONNECT_MODE_SEQUENCE):
    '''create a test chain

    Return a chain instance and the accounts of the chain instance.
    '''
    a = account(None)
    a.id.set(FIRST_ACCOUNT_ID)
    a.name.set(FIRST_ACCOUNT_ID[:2])
    b = account(None)
    b.id.set(SECOND_ACCOUNT_ID)
    b.name.set(SECOND_ACCOUNT_ID[:2])
    c = account(None)
    c.id.set(THIRD_ACCOUNT_ID)
    c.name.set(THIRD_ACCOUNT_ID[:2])
    sc_c = chain(
        [c], failure_mode=failure_mode, connect_mode=connect_mode)

    sc_b = chain(
        [b, sc_c], failure_mode=failure_mode, connect_mode=connect_mode)

    chain_instance = chain(
        [a, sc_b], failure_mode=failure_mode, connect_mode=connect_mode)
    return (chain_instance, a, b, c)


def get_test_single_chain(
        failure_mode=FAILURE_MODE_BREAK, connect_mode=CONNECT_MODE_SEQUENCE):
    '''create a test chain

    Return a chain instance and the accounts of the chain instance.
    '''
    a = account(None)
    a.id.set(FIRST_ACCOUNT_ID)
    a.name.set(FIRST_ACCOUNT_ID[:2])

    chain_instance = chain(
        [a], failure_mode=failure_mode, connect_mode=connect_mode)
    return (chain_instance, a)


def get_complex_test_chain(failure_mode=FAILURE_MODE_BREAK):
    '''Create a complex test chain

    Return a chain instance and the accounts of the chain instance.
    1
    |
    2 - 3
    \ - 4 - 5
        | - 6 - 7
        \ - 8
    a
    |
    b - c
    \ - d - e
        | - f - g
        \ - h

    a: 0 parent, parent for b
      [a, b]
    b: a parent, parent for c, parent for d
      [a, [b, c, d]]
    c: b parent, not parent for anyone
      [a, [b, [c], d]
    d: b parent, parent for e, parent for f, parent for h
      [a, [b, [c], [d, e, f h]]
    e: d parent, not parent for anyone
      [a, [b, [c], [d, [e], f, h]]
    f: d parent, parent for g
      [a, [b, [c], [d, [e], [f, g], h]]
    g: f parent, not parent for anyone
      [a, [b, [c], [d, [e], [f, [g]], h]]
    h: d parent, not parent for anyone
      [a, [b, [c], [d, [e], [f, [g]], [h]]]

    # all with no children: [item]
    # all with n children: [item, child1, childn]
    # item with no parent: [item,

    1 - 2 - 3
    1 - 2 - 4 - 5
    1 - 2 - 4 - 6 - 7
    1 - 2 - 4 - 6 - 8

    '''
    a = account(None)
    a.id.set(FIRST_ACCOUNT_ID)
    a.name.set(FIRST_ACCOUNT_ID[:2])
    b = account(None)
    b.id.set(SECOND_ACCOUNT_ID)
    b.name.set(SECOND_ACCOUNT_ID[:2])
    c = account(None)
    c.id.set(THIRD_ACCOUNT_ID)
    c.name.set(THIRD_ACCOUNT_ID[:2])
    d = account(None)
    d.id.set(FOURTH_ACCOUNT_ID)
    d.name.set(FOURTH_ACCOUNT_ID[:2])
    e = account(None)
    e.id.set(FIFTH_ACCOUNT_ID)
    e.name.set(FIFTH_ACCOUNT_ID[:2])
    f = account(None)
    f.id.set(SIXTH_ACCOUNT_ID)
    f.name.set(SIXTH_ACCOUNT_ID[:2])
    g = account(None)
    g.id.set(SEVENTH_ACCOUNT_ID)
    g.name.set(SEVENTH_ACCOUNT_ID[:2])
    h = account(None)
    h.id.set(EIGHT_ACCOUNT_ID)
    h.name.set(EIGHT_ACCOUNT_ID[:2])
    # [a, [b, [c], [d, [e], [f, g], [h]]]]
    sc_e = chain([e], failure_mode=FAILURE_MODE_BREAK)
    sc_fg = chain([f, g], failure_mode=FAILURE_MODE_BREAK)
    sc_h = chain([h], failure_mode=FAILURE_MODE_BREAK)
    sc_defgh = chain([d, sc_e, sc_fg, sc_h], failure_mode=FAILURE_MODE_BREAK)
    sc_c = chain([c], failure_mode=FAILURE_MODE_BREAK)
    sc_b = chain([b, sc_c, sc_defgh], failure_mode=FAILURE_MODE_BREAK)

    chain_instance = chain(
        [a, sc_b], failure_mode=failure_mode)

    return (chain_instance, a, b, c, d, e, f, g, h)


def get_test_objects(failure_mode=FAILURE_MODE_BREAK):
    '''get test objects

    Return a connection broker and a cahin with the requested failure mode.
    '''
    cb = connection_broker()
    cb.application = get_app()
    cb.wait_after_connect = lambda: False
    (chain_instance, a, b, c) = get_test_a_b_c_chain(failure_mode)
    return (cb, chain_instance, a, b, c)


def get_parallel_test_objects(failure_mode=FAILURE_MODE_BREAK):
    '''get test objects

    Return a connection broker and a chain with the requested failure mode.
    a - b
      \ c
    a in parallel mode
    '''
    cb = connection_broker()
    cb.application = get_app()
    cb.wait_after_connect = lambda: False
    (chain_instance, a, b, c) = get_test_a_bc_chain(
        failure_mode, CONNECT_MODE_PARALLEL)
    return (cb, chain_instance, a, b, c)


def get_sequence_test_objects(failure_mode=FAILURE_MODE_BREAK):
    '''get test objects

    Return a connection broker and a chain with the requested failure mode.
    a - b
      \ c
    a in sequence mode
    '''
    cb = connection_broker()
    cb.application = get_app()
    cb.wait_after_connect = lambda: False
    (chain_instance, a, b, c) = get_test_a_b_c_chain(failure_mode)

    return (cb, chain_instance, a, b, c)


def get_bc_sequence_test_objects(failure_mode=FAILURE_MODE_BREAK):
    '''get test objects

    Return a connection broker and a cahin with the requested failure mode.
    '''
    cb = connection_broker()
    cb.application = get_app()
    cb.wait_after_connect = lambda: False
    (chain_instance, a, b, c) = get_test_a_bc_chain(failure_mode)

    return (cb, chain_instance, a, b, c)


def get_complex_test_objects(failure_mode=FAILURE_MODE_BREAK):
    '''get test objects

    Return a connection broker and a chain with the requested failure mode.
    '''
    cb = connection_broker()
    cb.application = get_app()
    cb.wait_after_connect = lambda: False
    (chain_instance, a, b, c, d, e, f, g, h) = get_complex_test_chain(
        failure_mode)
    return (cb, chain_instance, a, b, c, d, e, f, g, h)


def get_single_test_object(failure_mode=FAILURE_MODE_BREAK):
    '''get single test object

    Return a connection broker and a chain with the requested failure mode.
    The chain contains only one connection.
    '''
    cb = connection_broker()
    cb.application = get_app()
    cb.wait_after_connect = lambda: False
    (chain_instance, a) = get_test_single_chain(
        failure_mode)
    return (cb, chain_instance, a)
