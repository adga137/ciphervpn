# -*- coding: utf-8 -*-
# connection_broker.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Connection Broker State Machine.
'''

from tornado import locks, gen, ioloop
from transitions import Machine, MachineError

from netsplice.config.backend import (
    FAILURE_MODE_BREAK, FAILURE_MODE_DISCONNECT, FAILURE_MODE_IGNORE,
    FAILURE_MODE_RECONNECT
)
from netsplice.config import connection_broker as config
from netsplice.config import connection as config_connection
from netsplice.config import backend as config_backend
from netsplice.backend.connection.connection import connection
from netsplice.backend.connection.chain import chain as connection_chain

from netsplice.util.errors import (
    NotFoundError, ConnectedError, NotConnectedError
)
from netsplice.util.ipc.errors import ModuleNotFoundError
from netsplice.util import get_logger

logger = get_logger()


class connection_broker(object):

    def __init__(self):
        self.application = None

        #: Connections is a dictionary with account_id as keys and a tuple \
        #    (connection_instance, chain) as value. They are the active \
        #    managed connections.
        self.connections = dict()

        #: Link Queue is a tuple list of (next_state, connection) that is \
        #    processed from the top on RESUME
        self.link_queue = list()

        #: Chain Queue is a tuple list of (next_state, account, chain) that \
        #    is processed from the top on RESUME when the link_queue is empty
        self.chain_queue = list()

        # Async lock done is a lock that can be used by async methods to
        # wait until the broker is finished.
        self._async_lock_done = locks.Condition()

        # Last connection trigger is a indicator on how to RESUME
        self._last_connection_trigger = None

        self.machine = Machine(
            model=self,
            states=config.STATES,
            initial=config.INACTIVE)

        self.machine.add_transition(
            trigger=config.CANCEL,
            source=[
                config.ACTIVE
            ],
            dest=config.ACTIVE,
            after='cancel_queue')

        self.machine.add_transition(
            trigger=config.DONE,
            source=[
                config.ACTIVE
            ],
            dest=config.INACTIVE,
            after='notify_done')

        self.machine.add_transition(
            trigger=config.RESUME,
            source=[
                config.INACTIVE,
                config.ACTIVE
            ],
            dest=config.ACTIVE,
            after='next_queue')

    def account_is_aborted(self, account_instance):
        '''
        Account is aborted.

        Return True when the connection to the given account_instance is
        currently in an aborted state.

        Arguments:
            account_instance (account): account to check.

        Returns:
            boolean -- state considered aborted
        '''
        account_state = self.account_state(account_instance)
        if account_state == config_connection.ABORTED:
            return True
        return False

    def account_is_connected(self, account_instance):
        '''
        Account is Connected.

        Return True when the connection to the given account_instance is
        currently in a connected state.

        Arguments:
            account_instance (account): account to check.

        Returns:
            boolean -- state considered connected
        '''
        account_state = self.account_state(account_instance)
        if account_state == config_connection.CONNECTED:
            return True
        if account_state == config_connection.RECONNECTED:
            return True
        return False

    def account_is_connecting(self, account_instance):
        '''
        Account is Connecting.

        Return True when the connection to the given account_instance is
        currently in a connecting state.

        Arguments:
            account_instance (account): account to check.

        Returns:
            boolean -- state considered connected
        '''
        account_state = self.account_state(account_instance)
        if account_state == config_connection.CONNECTING:
            return True
        if account_state == config_connection.CONNECTING_PROCESS:
            return True
        return False

    def account_is_created(self, account_instance):
        '''
        Account is Created.

        Return True when the connection to the given account_instance is
        currently in a created state.

        Arguments:
            account_instance (account): account to check.

        Returns:
            boolean -- state considered created
        '''
        account_state = self.account_state(account_instance)
        if account_state == config_connection.CREATED:
            return True
        return False

    def account_is_disconnected(self, account_instance):
        '''
        Account is Disconnected.

        Return True when the connection to the given account_instance is
        currently not in a connected state.

        Arguments:
            account_instance (account): account to check.

        Returns:
            boolean -- state considered disconnected
        '''
        account_state = self.account_state(account_instance)
        if account_state == config_connection.CREATED:
            return True
        if account_state == config_connection.INITIALIZED:
            return True
        if account_state == config_connection.DISCONNECTED:
            return True
        if account_state == config_connection.DISCONNECTED_FAILURE:
            return True
        if account_state == config_connection.ABORTED:
            return True
        return False

    def account_is_disconnected_removed(self, account_instance):
        '''
        Account is Disconnected and Removed.

        Return True when the connection to the given account_instance is
        currently not in a connected state but was connected earlier.

        Arguments:
            account_instance (account): account to check.

        Returns:
            boolean -- state considered disconnected
        '''
        account_state = self.account_state(account_instance)
        if account_state == config_connection.DISCONNECTED:
            return True
        if account_state == config_connection.DISCONNECTED_FAILURE:
            return True
        if account_state == config_connection.ABORTED:
            return True
        return False

    def account_is_disconnecting(self, account_instance):
        '''
        Account is Disconnecting.

        Return True when the connection to the given account_instance is
        currently in a disconnecting state.

        Arguments:
            account_instance (account): account to check.

        Returns:
            boolean -- state considered disconnecting
        '''
        account_state = self.account_state(account_instance)
        if account_state == config_connection.DISCONNECTING:
            return True
        if account_state == config_connection.DISCONNECTING_PROCESS:
            return True
        if account_state == config_connection.DISCONNECTING_FAILURE:
            return True
        if account_state == config_connection.DISCONNECTING_FAILURE_PROCESS:
            return True
        return False

    def account_is_initialized(self, account_instance):
        '''
        Account is Initialized.

        Return True when the connection to the given account_instance is
        currently in the initialized state.

        Arguments:
            account_instance (account): account to check.

        Returns:
            boolean -- state considered initialized
        '''
        account_state = self.account_state(account_instance)
        if account_state == config_connection.INITIALIZED:
            return True
        return False

    def account_is_reconnecting(self, account_instance):
        '''
        Account is Reconnecting.

        Return True when the connection to the given account_instance is
        currently in the reconnecting state.

        Arguments:
            account_instance (account): account to check.

        Returns:
            boolean -- state considered reconnecting
        '''
        account_state = self.account_state(account_instance)
        if account_state == config_connection.RECONNECTING:
            return True
        return False

    def account_is_reconnected(self, account_instance):
        '''
        Account is Initialized.

        Return True when the connection to the given account_instance is
        currently in the reconnected state.

        Arguments:
            account_instance (account): account to check.

        Returns:
            boolean -- state considered disconnected
        '''
        account_state = self.account_state(account_instance)
        if account_state == config_connection.RECONNECTED:
            return True
        return False

    def account_state(self, account_instance):
        '''
        Account State.

        Return the state of the connection associated with the given account
        instance. When the given account_instance has no registered connection
        return CREATED.

        Arguments:
            account_instance (preferences.model.account_item): \
                account instance

        Returns:
            [string] -- state of the connection
        '''
        if account_instance.id.get() not in self.connections.keys():
            logger.debug(
                'Account State %s %s.'
                % (account_instance.name.get(), config_connection.CREATED))
            return config_connection.CREATED
        account_connection = self.find_connection_by_account_id(
            account_instance.id.get())
        logger.debug(
            'Account State: %s %s.'
            % (account_instance.name.get(), account_connection.state))
        return account_connection.state

    def append_chain_queue(
            self, next_state, account_instance, connection_chain=None,
            front=False):
        '''
        Append Chain Queue

        Append a connection for the given account_instance to the chain queue.
        Reuses the connection instances if possible. Otherwise creates them.

        Arguments:
            next_state (string): state that the account should reach.
            account_instance (account_item): account for which the \
                connection is added.
            connection_chain (chain_list): chain the account is located in
            front -- switch between append/insert

        Returns:
            created (bool): True when a new connection is created that \
                needs to be setup.
        '''

        created = False

        account_list = list()
        connect_all = False
        if account_instance is None:
            connect_all = True
            account_instance = connection_chain[0].account
            account_list.append(account_instance)
            account_list.extend(connection_chain[0].depends(
                check_connect_mode=True))
        else:
            account_list = connection_chain.find_by_account_id(
                account_instance.id.get()).prior()
            account_list.reverse()
            account_list.append(account_instance)

        for account in account_list:
            if self.account_is_connected(account):
                continue
            if self.account_is_connecting(account):
                continue
            if account.enabled.get() is False:
                continue
            if account.id.get() in self.connections:
                logger.warn(
                    'Replacing connection for %s in the connection broker'
                    ' connection list: %s -> created'
                    % (account.name.get(), self.account_state(account)))
            connection_instance = connection(self, account)
            self.connections[account.id.get()] = (
                connection_instance, connection_chain)
            created = True

            if connect_all:
                # When connecting all accounts, the complete account_list is
                # relevant.
                continue
            if account_instance.id.get() == account.id.get():
                # do not add links that are in the chain after the given
                # account_id
                break

        if front:
            self.chain_queue.insert(
                0, (next_state, account_instance, connection_chain))
        else:
            self.chain_queue.append(
                (next_state, account_instance, connection_chain))
        if created:
            self.update_gui_connections()
        return created

    def append_link_queue(
            self, next_state, account_instance, connection_chain=None,
            front=False):
        '''
        Append Link Queue

        Append a connection for the given account_instance to the link queue.
        Reuses the connection instance if possible, creates it otherwise.

        Arguments:
            next_state (string): state that the account should reach.
            account_instance (account_item): account for which the \
                connection is added.
            connection_chain (chain_list): chain the account is located in
            front -- switch between append/insert

        Returns:
            created (bool): True when a new connection is created that \
                needs to be setup.
        '''
        created = False
        connection_instance = None
        try:
            connection_instance = self.find_connection_by_account_id(
                account_instance.id.get())
            is_disconnected = self.account_is_disconnected_removed(
                connection_instance.account)
            is_aborted = self.account_is_aborted(
                connection_instance.account)
            if is_disconnected or is_aborted:
                connection_instance = None
        except NotFoundError:
            pass
        if connection_chain is None:
            connection_chain = self.find_chain_by_account_id(
                account_instance.id.get())
        if connection_instance is None and connection_chain is not None:
            self.cleanup()
            connection_instance = connection(self, account_instance)
            self.connections[account_instance.id.get()] = (
                connection_instance, connection_chain)
            created = True
        if connection_instance is None:
            raise NotFoundError(
                'no connection for account %s'
                % (account_instance.id.get(),))

        if front:
            self.link_queue.insert(0, (next_state, connection_instance))
        else:
            self.link_queue.append((next_state, connection_instance))
        if created:
            self.update_gui_connections()
        return created

    @gen.coroutine
    def autostart(self, application):
        '''
        Autostart.

        Autostart accounts that are configured to start automatically. Request
        the preferences for a list of accounts and build the chains for these
        accounts.

        Decorators:
            gen.coroutine

        Arguments:
            application ([type]): [description]
        '''
        self.application = application
        autostart_accounts = self.application.get_module(
            'preferences').model.get_autostart_list()
        accounts = self.application.get_module(
            'preferences').model.accounts

        if len(autostart_accounts) is 0:
            logger.info('No accounts to autostart.')

        for autostart_account in autostart_accounts:
            logger.info(
                'Autostart account: %s' % (autostart_account.name.get(),))
            account_chain = self.application.get_module(
                'preferences').model.chains.get_connection_chain(
                    autostart_account.id.get(), accounts, connection_chain)
            ioloop.IOLoop.current().call_later(0, lambda: self.connect(
                autostart_account,
                account_chain))

    def cancel_queue(self, trigger_connection, trigger_state):
        '''
        Cancel Queue

        Cancel Queue when the chain is a break/disconnect chain. Break and
        reconnect chains set all queued connections to DISCONNECTED_FAILURE.
        Disconnect chains queue the disconnect of the parent.

        Arguments:
            trigger_connection (backend.connection.connection): \
                Connection that caused the cancel.
            trigger_state (string): state of the connection.
        '''
        logger.info(
            'Cancel Queue %s %s'
            % (trigger_connection.account.name.get(), trigger_state,))
        account = trigger_connection.account
        (broker_connection_instance, chain) = \
            self.connections[account.id.get()]
        if trigger_state == config_connection.ABORTED:
            for (next_state, link) in self.link_queue:
                link.state = config_connection.ABORTED
                link.notify_plugin_abort()
            del self.link_queue[:]
        elif self.is_break_chain(trigger_connection):
            self.requeue_disconnect(config_connection.DISCONNECT_FAILURE)
        elif self.is_disconnect_chain(trigger_connection):
            self.requeue_disconnect()
            parent_accounts = chain.find_by_account_id(
                account.id.get()).prior()
            for parent_account in parent_accounts:
                if not self.account_is_connected(parent_account):
                    continue
                self.append_link_queue(
                    config_connection.DISCONNECT, parent_account, chain)
        elif self.is_ignore_chain(trigger_connection):
            for (next_state, link) in self.link_queue:
                if link.id.get() == trigger_connection.id.get():
                    self.link_queue.remove(
                        (next_state, link))
                    break
        elif self.is_reconnect_parent_chain(trigger_connection):
            self.requeue_disconnect()
            self._last_connection_trigger = \
                config_connection.DISCONNECT

        self.trigger(config.RESUME)

    def cleanup(self):
        '''
        Cleanup

        Cleanup connections that are in disconnected state and should not
        be reused.
        '''
        rm_keys = list()
        for key, (connection_instance, chain) in self.connections.items():
            disconnected = connection_instance.is_disposable()
            if not disconnected:
                continue
            rm_keys.append(key)
        if len(rm_keys):
            for key in rm_keys:
                del self.connections[key]
            self.update_gui_connections()

    def connect_queued(self, account_instance, chain):
        '''
        Connect Queued.

        Queue the connection of the given account_instance in the given
        chain. When the account_instance is None, all accounts that are
        depended upon for a chain are connected.

        Arguments:
            account_instance (account): account model instance that is to \
                be connected. None to connect all chained accounts.
            chain (chain): chain that contains the account
        '''
        account_list = list()
        connect_all = False
        if account_instance is None:
            connect_all = True
            account_instance = chain[0].account
            account_list.append(account_instance)
            account_list.extend(chain[0].depends(
                check_connect_mode=True))
        else:
            account_list = chain.find_by_account_id(
                account_instance.id.get()).prior()
            account_list.reverse()
            account_list.append(account_instance)

        for account in account_list:
            if self.account_is_connected(account):
                continue
            if account.enabled.get() is False:
                continue

            self.append_link_queue(
                config_connection.CONNECT, account, chain)
            if connect_all:
                # When connecting all accounts, the complete account_list is
                # relevant.
                continue
            if account_instance.id.get() == account.id.get():
                # do not add links that are in the chain after the given
                # account_id
                break

        self.trigger(config.RESUME)

    def disconnect_queued(self, account_instance, chain):
        '''
        Disconnect Queued.

        Queue the connection of the given account_instance in the given chain.

        Arguments:
            account_instance (account): account model instance that is to \
                be disconnected.
            chain (chain): chain that contains the account

        Raises:
            NotConnectedError -- When the given account is not connected.
        '''
        # walk the chain reversed
        account_list = chain.find_by_account_id(
            account_instance.id.get()).depends(
                check_connect_mode=False)
        account_list.insert(0, account_instance)

        for account in account_list[::-1]:
            if self.account_is_created(account):
                continue
            if self.account_is_disconnected_removed(account):
                continue
            if self.account_is_disconnecting(account):
                continue
            self.append_link_queue(
                config_connection.DISCONNECT, account)
            if account_instance.id.get() == account.id.get():
                # do not add links that are in the chain after the given
                # account_id
                break

        self.trigger(config.RESUME)

    def disconnect_all(self):
        '''
        Disconnect All.

        Disconnect all currently connected accounts in an orderly fashion.
        '''
        del self.link_queue[:]
        ordered_disconnect_accounts = list()
        try:
            connection_values = self.connections.values()
        except AttributeError:
            connection_values = self.connections.itervalues()
        for (connection_instance, chain) in connection_values:
            disconnect_accounts = self.get_disconnect_accounts(
                connection_instance)
            for account in disconnect_accounts:
                if self.account_is_disconnected_removed(account):
                    continue
                ordered_disconnect_accounts.append(account)

        for account in ordered_disconnect_accounts:
            try:
                connection_instance = self.find_connection_by_account_id(
                    account.id.get())
                self.link_queue.append(
                    (config_connection.DISCONNECT, connection_instance))
            except NotFoundError:
                pass

        self.trigger(config.RESUME)

    def find_connection_by_account_id(self, account_id):
        '''
        Find connection by account_id.

        Return the broker managed connection that has associated the account
        with the given account_id.

        Arguments:
            account_id (string): account_id to find

        Returns:
            [connection] -- a connection

        Raises:
            NotFoundError -- The account was never subject to a state-change
        '''
        try:
            connection = self.connections[account_id][0]
            return connection
        except KeyError:
            raise NotFoundError('No connection for %s' % (account_id,))

    def find_connection_by_connection_id(self, connection_id):
        '''
        Find connection by connection_id.

        Return the broker managed connection that has associated the account
        with the given connection_id.

        Arguments:
            connection_id (string): connection_id to find

        Returns:
            [connection] -- a connection.

        Raises:
            NotFoundError -- There is no connection for the id
        '''
        try:
            connection_values = self.connections.values()
        except AttributeError:
            connection_values = self.connections.itervalues()
        for (connection_instance, chain) in connection_values:
            if connection_instance.id.get() == connection_id:
                return connection_instance
        raise NotFoundError('No connection with id %s' % (connection_id,))

    def find_connection_by_privileged_connection_id(self, connection_id):
        '''
        Find connection by privileged connection_id.

        Return the broker managed connection that has associated the account
        with the given privileged connection_id.

        Arguments:
            connection_id (string): connection_id to find

        Returns:
            [connection] -- a connection.

        Raises:
            NotFoundError -- There is no connection for the id
        '''
        try:
            connection_values = self.connections.values()
        except AttributeError:
            connection_values = self.connections.itervalues()
        for (connection_instance, chain) in connection_values:
            if connection_instance.model.privileged_id.get() == connection_id:
                return connection_instance
        raise NotFoundError(
            'No privileged connection with id %s' % (connection_id,))

    def find_connection_by_unprivileged_connection_id(self, connection_id):
        '''
        Find connection by unprivileged connection_id.

        Return the broker managed connection that has associated the account
        with the given unprivileged connection_id.

        Arguments:
            connection_id (string): connection_id to find

        Returns:
            [connection] -- a connection.

        Raises:
            NotFoundError -- There is no connection for the id
        '''
        try:
            connection_values = self.connections.values()
        except AttributeError:
            connection_values = self.connections.itervalues()
        for (connection_instance, chain) in connection_values:
            if connection_instance.model.unprivileged_id.get() == \
                    connection_id:
                return connection_instance
        raise NotFoundError(
            'No privileged connection with id %s' % (connection_id,))

    def find_chain_by_account_id(self, account_id):
        '''
        Find chain by account_id.

        Return the broker managed connection that has associated the account
        with the given account_id.

        Arguments:
            account_id (string): account_id to find

        Returns:
            chain -- an instance of backend.connection.chain

        Raises:
            NotFoundError -- The account was never subject to a state-change
        '''
        try:
            chain = self.connections[account_id][1]
            return chain
        except KeyError:
            pass
        try:
            connection_values = self.connections.values()
        except AttributeError:
            connection_values = self.connections.itervalues()
        for (connection_instance, chain) in connection_values:
            try:
                chain.find_by_account_id(account_id)
                return chain
            except NotFoundError:
                pass
        raise NotFoundError('No chain for %s' % (account_id,))

    def get_disconnect_accounts(self, connection_instance):
        '''
        Get Disconnect Accounts.

        Get a list of accounts that need to be disconnected when the given
        connection_instance is disconnected.
        This is used for failure_mode break.

        Arguments:
            connection_instance (connection): connection that triggers the \
                disconnect

        Returns:
            list -- list of accounts
        '''
        connection_chain = self.find_chain_by_account_id(
            connection_instance.account.id.get())
        link = connection_chain.find_by_account_id(
            connection_instance.account.id.get())
        account_list = link.depends(
            check_connect_mode=False)
        account_list.insert(0, link.account)
        account_list.reverse()
        return account_list

    def get_disconnect_all_accounts(self, connection_instance):
        '''
        Get Disconnect all Accounts.

        Return all accounts of a chain that need to be disconnected when the
        given connection_instance is disconnected.
        This is used for failure_mode disconnect.

        Arguments:
            connection_instance (connection): connection that triggers the \
                disconnect

        Returns:
            list -- list of accounts
        '''
        connection_chain = self.find_chain_by_account_id(
            connection_instance.account.id.get())
        link = connection_chain.find_by_account_id(
            connection_instance.account.id.get())
        account_list = link.prior()
        account_list.append(link.account)
        account_list.extend(link.depends(
            check_connect_mode=False))
        account_list.reverse()
        return account_list

    def get_reconnect_accounts(
            self, connection_instance, next_connection_instance):
        '''
        Get Reconnect accounts.

        Return all accounts that need to be reconnected when the given
        connection instance is reconnected.

        Arguments:
            connection_instance (connection): connection that triggers the \
                reconnect
            next_connection_instance (connection): connection that was in \
                the link queue

        Returns:
            [type] -- [description]
        '''
        reconnect_account_list = list()
        account_list = list()
        connection_chain = self.find_chain_by_account_id(
            connection_instance.account.id.get())
        link = connection_chain.find_by_account_id(
            connection_instance.account.id.get())
        prior_account_list = link.prior()
        dependent_account_list = link.depends(
            check_connect_mode=True)
        account_list = prior_account_list
        account_list.append(link.account)
        account_list.extend(dependent_account_list)

        if next_connection_instance is None:
            reconnect_account_list = account_list
            logger.info('Reconnect Parent, no next.')
        else:
            next_account = next_connection_instance.account
            for account in account_list:
                if account.id.get() == next_account.id.get():
                    break
                reconnect_account_list.append(account)
            logger.info(
                'Reconnect Parent: %s.' % (str(reconnect_account_list),))
        return reconnect_account_list

    def is_break_chain(self, connection_instance):
        '''
        Is Break Chain.

        Return True if the given connection_instance is in a chain that has
        the failure_mode break.

        Arguments:
            connection_instance (connection): registered connection to check

        Returns:
            boolean -- Match of failure mode
        '''
        connection_chain = self.find_chain_by_account_id(
            connection_instance.account.id.get())
        return connection_chain.failure_mode == FAILURE_MODE_BREAK

    def is_disconnect_chain(self, connection_instance):
        '''
        Is Disconnect Chain.

        Return True if the given connection_instance is in a chain
        that has the failure_mode disconnect.

        Arguments:
            connection_instance (connection): registered connection to check

        Returns:
            boolean -- Match of failure mode
        '''
        connection_chain = self.find_chain_by_account_id(
            connection_instance.account.id.get())
        return connection_chain.failure_mode == FAILURE_MODE_DISCONNECT

    def is_ignore_chain(self, connection_instance):
        '''
        Is Ignore Chain.

        Return True if the given connection_instance is in a chain
        that has the failure_mode ignore.

        Arguments:
            connection_instance (connection): registered connection to check

        Returns:
            boolean -- Match of failure mode
        '''
        connection_chain = self.find_chain_by_account_id(
            connection_instance.account.id.get())
        return connection_chain.failure_mode == FAILURE_MODE_IGNORE

    def is_in_link_queue(self, connection_instance):
        '''
        Is in Link Queue.

        Return True if the given connection_instance is in the current link
        queue.

        Arguments:
            connection_instance (connection): registered connection to check

        Returns:
            boolean -- Match of failure mode
        '''
        in_link_queue = False
        for (next_state, next_link) in self.link_queue:
            if next_link.id.get() != connection_instance.id.get():
                continue
            in_link_queue = True
            break
        return in_link_queue

    def is_in_parallel_chain(self, account_id):
        '''
        Is in parallel chain.

        Check if the given account_id is in a parallel chain.

        Arguments:
            account_id (string): Account id to be checked.
        '''
        chain = self.find_chain_by_account_id(account_id)
        for link in chain:
            if link.account.id.get() != account_id:
                continue
            parent = link.get_parent()
            if parent is None:
                return False
            if parent.connect_mode == config_backend.CONNECT_MODE_PARALLEL:
                return True
        return False

    def is_reconnect_parent_chain(self, connection_instance):
        '''
        Is Reconnect Parent Chain.

        Return true if the given connection_instance is in a chain
        that has the failure_mode reconnect parent.

        Arguments:
            connection_instance (connection): registered connection to check

        Returns:
            boolean -- Match of failure mode
        '''
        connection_chain = self.find_chain_by_account_id(
            connection_instance.account.id.get())
        return connection_chain.failure_mode == FAILURE_MODE_RECONNECT

    def next_connect_queue(self, connection_instance):
        '''
        Next Connect Queue.

        Continues in the current link queue if it is not empty, load the next
        chain when not empty.
        When all queued links are disconnected, the state is set to done.
        '''
        logger.debug(
            'next_connect_queue: Queue length: %d.' % (len(self.link_queue),))

        if self.next_setup_queue(connection_instance):
            self.link_queue.insert(
                0, (config_connection.CONNECT, connection_instance))
            return

        connection_instance.trigger(config_connection.CONNECT)
        self.next_parallel_connect(connection_instance)

    def next_disconnect_queue(self, connection_instance):
        '''
        Next Disconnect Queue.

        Handler that is executed when a disconnect has finished.
        Continues in the current link queue if it is not empty, load the next
        chain when not empty.
        When all queued links are disconnected, the state is set to done.
        '''
        logger.debug('next_disconnect_queue')

        if connection_instance.state == \
                config_connection.DISCONNECTED or \
                connection_instance.state == \
                config_connection.DISCONNECTED_FAILURE:
            self.trigger(config.RESUME)
            return
        if connection_instance.is_disconnected():
            connection_instance.trigger(config_connection.DISCONNECT_FAILURE)
            return

        connection_instance.trigger(config_connection.DISCONNECT)

    def next_disconnect_abort_queue(self, connection_instance):
        '''
        Next Disconnect Abort Queue.

        Handler that is executed when a disconnect has finished.
        Continues in the current link queue if it is not empty, load the next
        chain when not empty.
        When all queued links are disconnected, the state is set to done.
        '''
        logger.debug('next_disconnect_abort_queue')

        if connection_instance.state == \
                config_connection.DISCONNECTED or \
                connection_instance.state == \
                config_connection.DISCONNECTED_FAILURE:
            self.trigger(config.RESUME)
            return

        connection_instance.trigger(config_connection.DISCONNECT_FAILURE)

    def next_parallel_connect(self, connection_instance):
        '''
        Next Parallel Connect.

        Check if next in the link queue is available and if the given
        connection_instance is in a parallel chain with the following link.

        Arguments:
            connection_instance (backend.connection.connection): \
                connection that just got triggered to connect/reconnect.
        '''
        if len(self.link_queue) == 0:
            return
        (following_state, following_link) = self.link_queue[0]
        if following_state != config_connection.CONNECT and \
                following_state != config_connection.RECONNECT:
            return

        next_parallel = self.is_in_parallel_chain(
            connection_instance.account.id.get())
        next_parallel &= self.is_in_parallel_chain(
            following_link.account.id.get())

        if not next_parallel:
            return
        logger.info(
            'Resume connecting next queued because the'
            ' parent is a configured as parallel connect'
            ' mode.')
        self.trigger(config.RESUME)

    def next_reconnect_queue(self, connection_instance):
        '''
        Next Reconnect Queue

        Handler that is executed when a reconnect has finished.
        Continues in the current link queue if it is not empty, load the next
        chain when not empty.
        When all queued links are reconnected, the state is set to done.
        '''
        logger.debug('next_reconnect_queue')
        if self.next_setup_queue(connection_instance):
            self.link_queue.insert(
                0, (config_connection.RECONNECT, connection_instance))
            return

        connection_instance.trigger(config_connection.RECONNECT)
        self.next_parallel_connect(connection_instance)

    def next_queue(self, trigger_connection=None, trigger_state=None):
        '''
        Next Queue.

        After connection event handler that decides what will be done next.
        When a trigger_connection is provided with a trigger_state, the
        chain is checked for possible breaks.
        When there are links in the link_queue, they are processed to reach
        the queued state. When there are no links, the chain_queue is used
        to setup the next link_queue.
        '''
        logger.debug(
            'Next Link Queue %s [%d/%d].'
            % (str(trigger_state),
                len(self.link_queue), len(self.chain_queue)))
        if trigger_connection is not None:
            # check connection chain
            is_parallel_trigger = self.is_in_parallel_chain(
                trigger_connection.account.id.get())

            if trigger_state == config_connection.ABORTED:
                # clear all queued links
                self.cancel_queue(trigger_connection, trigger_state)
            elif trigger_state == config_connection.DISCONNECTED:
                self.restore_disconnected_chain(trigger_connection)
            elif trigger_state == config_connection.CONNECTED:
                # continue connect
                pass
            elif trigger_state == config_connection.RECONNECTED:
                # continue reconnect
                # ? reconnect chain children?
                pass
            elif trigger_state == config_connection.SETUP:
                # continue connect
                pass
            if not is_parallel_trigger:
                postpone_resume = trigger_state == \
                    config_connection.CONNECTED
                postpone_resume |= trigger_state == \
                    config_connection.RECONNECTED

                if postpone_resume and self.wait_after_connect():
                    # wait_after_connect has triggered a resume
                    return

        if len(self.link_queue) > 0:
            # Continue with the next link in the link_queue
            (next_state, next_link) = self.link_queue[0]
            logger.debug(
                'Next Link Queue [%d]/[%s -> %s] Account: %s'
                % (len(self.link_queue), next_link.state, next_state,
                    next_link.account.name.get(),))
            del self.link_queue[0]
            self._last_connection_trigger = next_state
            try:
                if next_state == config_connection.CONNECT:
                    self.next_connect_queue(next_link)
                elif next_state == config_connection.DISCONNECT:
                    self.next_disconnect_queue(next_link)
                elif next_state == config_connection.DISCONNECT_FAILURE:
                    self.next_disconnect_abort_queue(next_link)
                elif next_state == config_connection.RECONNECT:
                    self.next_reconnect_queue(next_link)
                else:
                    logger.warn(
                        'Unknown next state %s, doing nothing'
                        % (next_state,))
            except MachineError:
                logger.critical(
                    'Invalid state transition: %s to %s for %s'
                    % (next_link.state, next_state,
                        next_link.account.name.get()))
                self.cancel_queue(next_link, next_state)
            return

        if len(self.chain_queue) > 0:
            (next_state, next_account, next_chain) = self.chain_queue[0]
            logger.debug(
                'Next Chain Queue [%d]/[%s] %s'
                % (len(self.chain_queue), next_state,
                    next_chain,))
            del self.chain_queue[0]
            if next_state == config_connection.CONNECT:
                self.connect_queued(next_account, next_chain)
            elif next_state == config_connection.DISCONNECT:
                self.disconnect_queued(next_account, next_chain)
            elif next_state == config_connection.RECONNECT:
                self.reconnect_queued(next_account, next_chain)
            else:
                logger.warn(
                    'Unknown next state %s, doing nothing'
                    % (next_state,))
            return
        if self.state == config.ACTIVE:
            self.trigger(config.DONE)

    def next_setup_queue(self, setup_connection_instance):
        '''
        Next Setup Queue.

        Continues to setup connections.
        When all queued links are setup, the connect queue is started.
        Setup may involve a credential question that can be canceled.
        '''
        logger.debug(
            'next_setup_queue: Queue length: %d/%d.'
            % (len(self.link_queue), len(self.connections.keys())))

        setting_up = False
        if setup_connection_instance.state == config_connection.SETTING_UP:
            setting_up = True
            return setting_up
        if setup_connection_instance.state == config_connection.CREATED:
            setup_connection_instance.trigger(config_connection.SETUP)
            setting_up = True
            return setting_up

        try:
            connection_values = self.connections.values()
        except AttributeError:
            connection_values = self.connections.itervalues()
        for (connection_instance, chain) in connection_values:
            if not self.is_in_link_queue(connection_instance):
                continue
            if connection_instance.state == config_connection.CREATED:
                connection_instance.trigger(config_connection.SETUP)
                setting_up = True
                break
            if connection_instance.state == config_connection.SETTING_UP:
                setting_up = True
                break
            continue

        return setting_up

    def reconnect_queued(self, account_instance, chain):
        '''
        Reconnect Queued.

        Queue the connection of the given account_instance in the given chain.

        Arguments:
            account_instance (account): account model instance that is to \
                be disconnected.
            chain (chain): chain that contains the account

        Raises:
            NotConnectedError -- When the given account is not connected.
        '''
        # walk the chain reversed
        account_list = chain.find_by_account_id(
            account_instance.id.get()).depends(
                check_connect_mode=True)
        account_list.insert(0, account_instance)
        for account in account_list[::-1]:
            # the account list has to be evaluated from the end
            if self.account_is_created(account):
                continue
            if self.account_is_initialized(account):
                continue
            if self.account_is_reconnecting(account):
                continue

            self.append_link_queue(
                config_connection.RECONNECT, account, chain, front=True)

            if account_instance.id.get() == account.id.get():
                # do not add links that are in the chain after th
                # given account_id
                break
        # the reconnects have to be from top to bottom
        self.trigger(config.RESUME)

    def requeue_disconnect(self, trigger=config_connection.DISCONNECT):
        '''
        Requeue Disconnect.

        Disconnect all queued connections. This will setup a new queue that
        ensures that the plugins are called to free the resources.
        '''
        new_link_queue = list()
        for (next_state, next_link) in self.link_queue:
            new_link_queue.append(next_link)
        del self.link_queue[:]

        # reverse the order so the last queued gets disconnected first.
        for link in new_link_queue:
            self.link_queue.insert(
                0, (trigger, link))

    def restore_disconnected_chain(self, connection_instance):
        '''
        Restore disconnected chain.

        Restore the chain after a connection has disconnected. By default this
        method does nothing (handling requested disconnects, but when the
        _last_connection_trigger was not DISCONNECT, then the link queue needs
        to be adjusted according to the configured chain failure mode.

        Arguments:
            connection_instance (backend.connection.connection): \
                Connection that was disconnected and triggered this method.
        '''

        (broker_connection_instance, chain) = \
            self.connections[connection_instance.account.id.get()]
        # Check that child connections are canceled, reconnected or
        # ignored according to chain mode
        parent_accounts = chain.find_by_account_id(
            connection_instance.account.id.get()).prior()
        children_accounts = chain.find_by_account_id(
            connection_instance.account.id.get()).depends(
                check_connect_mode=True)

        if self.is_break_chain(connection_instance):
            # cancel queued connects of chain
            if self._last_connection_trigger == config_connection.CONNECT:
                self.requeue_disconnect()
            elif self._last_connection_trigger is None:
                for account in children_accounts:
                    if not self.account_is_connected(account):
                        continue
                    self.append_link_queue(
                        config_connection.DISCONNECT, account, chain)

        elif self.is_disconnect_chain(connection_instance):
            if self._last_connection_trigger == config_connection.DISCONNECT:
                pass
            elif self._last_connection_trigger == config_connection.CONNECT:
                # Cancel queued connects of chain
                self.requeue_disconnect()
                # Disconnect connected of chain
                for account in parent_accounts:
                    if not self.account_is_connected(account):
                        continue
                    self.append_link_queue(
                        config_connection.DISCONNECT, account, chain)
            elif self._last_connection_trigger is None:
                # Disconnect occurred while inactive
                # Disconnect connected of chain
                # (topmost ancestor = last disconnected)
                for account in parent_accounts:
                    if not self.account_is_connected(account):
                        continue
                    self.append_link_queue(
                        config_connection.DISCONNECT, account, chain)
                # Insert disconnect of children
                # (last children = first disconnected)
                for account in children_accounts:
                    if not self.account_is_connected(account):
                        continue
                    self.append_link_queue(
                        config_connection.DISCONNECT, account, chain,
                        front=True)
            pass
        elif self.is_ignore_chain(connection_instance):
            # Continue connect
            pass
        elif self.is_reconnect_parent_chain(connection_instance):
            if self._last_connection_trigger == config_connection.DISCONNECT:
                # broker (user) triggered disconnect
                pass
            else:
                # insert connect of connection.account in link queue
                self.append_link_queue(
                    config_connection.RECONNECT, connection_instance.account,
                    chain, front=True)
                # insert reconnect parent of connection in link queue
                if len(parent_accounts) > 0:
                    parent_account = parent_accounts[0]
                    self.append_link_queue(
                        config_connection.RECONNECT, parent_account, chain,
                        front=True)
                # Insert disconnect of children
                # (last children = first disconnected)
                for account in children_accounts:
                    if not self.account_is_connected(account):
                        continue
                    self.append_link_queue(
                        config_connection.RECONNECT, account, chain)

    #
    # Public Interface
    #

    def connect(self, account_instance, chain):
        '''
        Connect.

        Connect the given account instance in the given chain. The request is
        queued when the connection_broker is currently active.
        The consistency of the chain is maintained.
        All accounts that are dependent on the given account_instance are
        disconnected in the correct order:
        A - B - C - D
        Connect C calls connect on A, B then C.

        Arguments:
            account_instance (account): account model instance to be \
                connected.
            chain (chain): chain that contains the account.
        '''
        if self.account_is_connected(account_instance):
            raise ConnectedError()
        if self.account_is_disconnecting(account_instance):
            raise ConnectedError()
        logger.info('Connect %s.' % (account_instance.name.get(),))
        if self.state != config.INACTIVE:
            self.append_chain_queue(
                config_connection.CONNECT, account_instance, chain)
            return
        self.connect_queued(account_instance, chain)

    def connect_all(self, chain):
        '''
        Connect All.

        Connect the given chain. The request is queued when the
        connection_broker is currently active. The consistency of the chain is
        maintained. All accounts that are defined in the given chain are
        connected in the correct order:
        A - B - C - D Connect A calls connect on A, B, C then D.
        A - B,  A - C, A - D Connect A calls connect on A, B, C then D.
        A - B,  A - C - D, A - E Connect A calls connect on A, B, C, D then E.

        Arguments:
            chain (chain): chain that contains the accounts.
        '''
        logger.info('Connect all %s.' % (str(chain),))

        if self.state != config.INACTIVE:
            self.append_chain_queue(
                config_connection.CONNECT, None, chain)
            return
        self.connect_queued(None, chain)

    def disconnect(self, account_instance, chain):
        '''
        Disconnect.

        Disconnect the given account instance in the given chain. The request
        is queued when the connection_broker is currently active. The
        consistency of the chain is maintained. All accounts that are dependent
        on the given account_instance are disconnected in the correct order:

        A - B - C - D
        Disconnect B calls disconnect on D, C then B.

        Arguments:
            account_instance (account): account model instance to be \
                disconnected.
            chain (chain): chain that contains the account.
        '''
        logger.info('Disconnect %s.' % (account_instance.name.get(),))

        account_state = self.account_state(account_instance)
        if account_state == config_connection.DISCONNECTED_FAILURE or \
                account_state == config_connection.DISCONNECTED:
            raise NotConnectedError()

        # Check if the disconnect needs to be injected into the current link
        # queue.
        connection_instance = self.find_connection_by_account_id(
            account_instance.id.get())
        inject_queue = list()
        rm_queue = list()
        disconnect_accounts = self.get_disconnect_accounts(
            connection_instance)
        for account in disconnect_accounts:
            try:
                rm_connection = self.find_connection_by_account_id(
                    account.id.get())
                inject_queue.insert(0, rm_connection)
            except NotFoundError:
                continue

        if len(inject_queue) > 0:
            for (next_state, next_link) in self.link_queue:
                if next_link in inject_queue:
                    rm_queue.append(next_link)

            for next_link in rm_queue:
                self.remove_from_link_queue(next_link)

            for link in inject_queue:
                self.link_queue.insert(
                    0, (config_connection.DISCONNECT, link))
            self._last_connection_trigger = config_connection.DISCONNECT
            self.trigger(config.RESUME)
            return

        if self.state != config.INACTIVE:
            self.append_chain_queue(
                config_connection.DISCONNECT, account_instance, chain)
            return
        self.disconnect_queued(account_instance, chain)

    def remove_from_link_queue(self, connection_instance):
        '''
        Remove from Link Queue.

        Remove the given connection instance from the link queue. It iterates
        the list, finds the index and removes the list item with the index.

        Arguments:
            connection_instance (backend.connection.connection): \
                connection instance to be removed from link_queue.
        '''
        rm_index = None
        for (index, (next_state, next_link)) in enumerate(self.link_queue):
            if next_link.id.get() != connection_instance.id.get():
                continue
            rm_index = index
            break
        if rm_index is None:
            raise NotFoundError('Connection is not in the link queue')
        del self.link_queue[rm_index]

    def notify_done(self):
        '''
        Notify Done.

        Notify that the connection broker is currently inactive.
        '''
        logger.info('Connection Broker is finished.')
        self._last_connection_trigger = None
        self._async_lock_done.notify_all()

    def reconnect(self, account_instance, chain):
        '''
        Reconnect.

        Reconnect the given account instance in the given chain. The request is
        queued when the connection_broker is currently active.
        The consistency of the chain is maintained.
        All accounts that are dependent on the given account_instance are
        reconnected in the correct order:
        A - B - C - D
        Reconnect B reconnects B, C then D

        Arguments:
            account_instance (account): account model instance to be \
                reconnected.
            chain (chain): chain that contains the account.
        '''
        if self.account_is_disconnected(account_instance):
            raise NotConnectedError()
        logger.info('Reconnect %s.' % (account_instance.name.get(),))
        if self.state != config.INACTIVE:
            self.append_chain_queue(
                config_connection.RECONNECT, account_instance, chain)
            return
        self.reconnect_queued(account_instance, chain)

    def reset(self, account_instance):
        '''
        Reset.

        Reset the given account instance in the given chain. The request is
        queued when the connection_broker is currently active.
        Ignore when there is no connection for the account registered.

        Arguments:
            account_instance (account): account model instance to be \
                connected.
        '''
        logger.info('Reset %s.' % (account_instance.name.get(),))
        try:
            connection_instance = self.find_connection_by_account_id(
                account_instance.id.get())
            connection_instance.reset()
        except NotFoundError:
            pass

        try:
            del self.connections[account_instance.id.get()]
        except KeyError:
            pass
        self.update_gui_connections()

    def update_gui_connections(self):
        '''
        Update GUI connections.

        Run a update on the gui connections with the current connection list.
        This method should be called every time when the connection list is
        changed.
        '''
        try:
            gui_module = self.application.get_module('gui')
            gui_module.model.update_connections(self.connections)
        except ModuleNotFoundError:
            pass

    def wait_after_connect(self):
        '''
        Wait after connect.

        Inject an async resume for the configured event for sequence chains.

        Return True when the next queue was scheduled.
        '''
        if config_backend.MAX_SEQUENCE_WAIT_AFTER_CONNECT_TIME is 0:
            return False
        logger.info(
            'Waiting %d seconds before the next queued.'
            % (config_backend.MAX_SEQUENCE_WAIT_AFTER_CONNECT_TIME,))
        ioloop.IOLoop.current().call_later(
            config_backend.MAX_SEQUENCE_WAIT_AFTER_CONNECT_TIME,
            self.next_queue)
        return True

    @gen.coroutine
    def wait_for_done(self, max_wait_time=0):
        '''
        Wait for Done.

        Wait until the broker is in DONE state. This is useful to trigger
        in async coroutines and then wait for the connections to finish.

        Decorators:
            gen.coroutine

        Yields:
            locks.Condition -- condition that is unlocked when the done state \
                is reached.
        '''
        if self.state != config.INACTIVE:
            wait_timeout = None
            if max_wait_time > 0:
                wait_timeout = ioloop.IOLoop.current().time()
                wait_timeout += max_wait_time
            yield self._async_lock_done.wait(
                timeout=wait_timeout)
