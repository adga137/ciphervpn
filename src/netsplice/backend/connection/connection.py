# -*- coding: utf-8 -*-
# connection.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Connection State Machine.
'''

import uuid
from tornado import ioloop
from transitions import Machine

from netsplice.config import connection as config
from netsplice.config import connection_broker as config_connection_broker
from netsplice.backend.connection.connection_plugin import (
    factory as connection_plugin_factory
)
from netsplice.backend.connection.model.connection_item import (
    connection_item as connection_item_model
)
from netsplice.util.model.field import field

from netsplice.util.ipc.errors import ModuleNotFoundError
from netsplice.util import get_logger

logger = get_logger()


class connection(object):

    def __init__(self, broker, account_instance):
        self.broker = broker
        self.account = account_instance
        self.id = field()
        self.id.set(str(uuid.uuid4()))
        self.plugin = None
        self.model = connection_item_model()
        self.model.id.set(self.id.get())
        self._state_history = list()
        self._up = False
        logger.info(
            'connection: %s' % (self.id.get(),),
            extra={
                'connection_id': self.id.get(),
                'account_id': self.account.id.get()})

        if self.account.type.get() is not None:
            # None is useful for unit testing connection states.
            self.plugin = connection_plugin_factory(
                self.account.type.get(),
                self.broker.application,
                self)

        self.machine = Machine(
            model=self,
            states=config.STATES,
            initial=config.CREATED)

        self.machine.add_transition(
            trigger=config.SETUP,
            source=[
                config.CREATED],
            dest=config.SETTING_UP,
            after=['notify', 'notify_plugin_setup'])

        self.machine.add_transition(
            trigger=config.SETUP_FINISH,
            source=[
                config.SETTING_UP],
            dest=config.INITIALIZED,
            after=['notify', 'notify_broker_setup'])

        self.machine.add_transition(
            trigger=config.SETUP_CANCEL,
            source=[
                config.SETTING_UP],
            dest=config.DISCONNECTED_FAILURE,
            after=[
                'notify',
                'notify_broker_disconnected_cancel',
                'notify_down'])

        self.machine.add_transition(
            trigger=config.CONNECT,
            source=[
                config.INITIALIZED,
                config.DISCONNECTED,
                config.DISCONNECTED_FAILURE],
            dest=config.CONNECTING,
            after=['notify', 'notify_plugin_connect'])

        self.machine.add_transition(
            trigger=config.CONNECT_PROCESS,
            source=[config.CONNECTING],
            dest=config.CONNECTING_PROCESS,
            after='notify')

        self.machine.add_transition(
            trigger=config.CONNECT_FINISH,
            source=config.CONNECTING_PROCESS,
            dest=config.CONNECTED,
            after=['notify', 'notify_broker_connected', 'notify_up'])

        self.machine.add_transition(
            trigger=config.DISCONNECT,
            source=[
                config.CONNECTED,
                config.CONNECTING,
                config.CONNECTING_PROCESS,
                config.RECONNECTED,
                config.RECONNECTING_PROCESS,
                config.RECONNECTING],
            dest=config.DISCONNECTING,
            after=['notify', 'notify_plugin_disconnect', 'notify_down'])

        self.machine.add_transition(
            trigger=config.DISCONNECT_PROCESS,
            source=[config.DISCONNECTING],
            dest=config.DISCONNECTING_PROCESS,
            after='notify')

        self.machine.add_transition(
            trigger=config.DISCONNECT_FINISH,
            source=config.DISCONNECTING_PROCESS,
            dest=config.DISCONNECTED,
            after=['notify', 'notify_broker_disconnected'])

        self.machine.add_transition(
            trigger=config.DISCONNECT_FAILURE,
            source=[
                config.CONNECTED,
                config.CONNECTING,
                config.CONNECTING_PROCESS,
                config.DISCONNECTING,
                config.RECONNECTED,
                config.RECONNECTING,
                config.RECONNECTING_PROCESS,
                config.INITIALIZED,
                config.CREATED,
                config.SETTING_UP],
            dest=config.DISCONNECTING_FAILURE,
            after=['notify', 'notify_plugin_disconnect', 'notify_down'])

        self.machine.add_transition(
            trigger=config.DISCONNECT_ABORT,
            source=[
                config.CONNECTED,
                config.CONNECTING,
                config.CONNECTING_PROCESS,
                config.DISCONNECTING,
                config.DISCONNECTING_FAILURE,
                config.DISCONNECTING_FAILURE_PROCESS,
                config.DISCONNECTED,
                config.DISCONNECTED_FAILURE,
                config.RECONNECTED,
                config.RECONNECTING,
                config.RECONNECTING_PROCESS,
                config.INITIALIZED,
                config.CREATED,
                config.SETTING_UP],
            dest=config.ABORTED,
            after=[
                'notify',
                'notify_broker_aborted',
                'notify_plugin_abort',
                'notify_down'])

        self.machine.add_transition(
            trigger=config.DISCONNECT_FAILURE_PROCESS,
            source=[config.DISCONNECTING_FAILURE],
            dest=config.DISCONNECTING_FAILURE_PROCESS,
            after='notify')

        self.machine.add_transition(
            trigger=config.DISCONNECT_FAILURE_FINISH,
            source=config.DISCONNECTING_FAILURE_PROCESS,
            dest=config.DISCONNECTED_FAILURE,
            after=[
                'notify',
                'notify_broker_disconnected',
                'notify_failure_to_gui'])

        self.machine.add_transition(
            trigger=config.RECONNECT,
            source=[
                config.CONNECTED,
                config.INITIALIZED,  # from setup after reset
                config.DISCONNECTING,
                config.DISCONNECTING_FAILURE,
                config.DISCONNECTED,
                config.DISCONNECTED_FAILURE,
                config.RECONNECTING_PROCESS,
                config.RECONNECTED],
            dest=config.RECONNECTING,
            after=['notify', 'notify_plugin_reconnect', 'notify_down'])

        self.machine.add_transition(
            trigger=config.RECONNECT_PROCESS,
            source=[config.RECONNECTING],
            dest=config.RECONNECTING_PROCESS,
            after='notify')

        self.machine.add_transition(
            trigger=config.RECONNECT_FINISH,
            source=config.RECONNECTING_PROCESS,
            dest=config.RECONNECTED,
            after=['notify', 'notify_broker_reconnected', 'notify_up'])

        if self.broker:
            self.broker.application.get_module('event').notify(
                self.state, self.id.get())

    def in_active(self):
        '''
        Is Active

        Return True when the connection is in one of the active states and
        the associated account must not be modified in the meantime.

        Returns:
            bool -- Connection is active.
        '''
        return self.state in config.ACTIVE_STATES

    def in_connected(self):
        '''
        Is Connected

        Return True when the connection is in one of the connected states and
        the associated account must not be modified in the meantime.

        Returns:
            bool -- Connection is connected.
        '''
        return self.state in config.CONNECTED_STATES

    def in_disconnecting(self):
        '''
        Is Disconnecting

        Return True when the connection is in one of the disconnecting states
        and the associated account must not be modified in the meantime.

        Returns:
            bool -- Connection is connected.
        '''
        return self.state in config.DISCONNECTING_STATES

    def in_disconnecting_process(self):
        '''
        Is Disconnecting

        Return True when the connection is in one of the disconnecting states
        and the associated account must not be modified in the meantime.

        Returns:
            bool -- Connection is disconnecting.
        '''
        return self.state in config.DISCONNECTING_PROCESS_STATES

    def is_disconnected(self):
        '''
        Is Disconnected

        Return True when the connection is in one of the disconnected states
        and the connection should not be reused anymore.

        Returns:
            bool -- Connection is disconnected.
        '''
        return self.state in config.DISCONNECTED_STATES

    def is_disposable(self):
        '''
        Is Disposable

        Return True when the connection is in one of the disconnected states
        and the connection can be removed.

        Returns:
            bool -- Connection is disconnected.
        '''
        return self.state in config.DISPOSABLE_STATES

    def notify(self):
        '''
        Notify.

        Notify handler for all state changes. Maintains a history and
        notifies the event module.
        '''
        logger.info(
            'Connection notify: state: %s account: %s %s.'
            % (self.state, self.account.name.get(), self.id.get()),
            extra={
                'connection_id': self.id.get(),
                'account_id': self.account.id.get()})
        if self.state not in self._state_history:
            self._state_history.append(self.state)

        self.broker.application.get_module('event').notify(
            self.state, self.id.get())

    def notify_broker_connected(self):
        '''
        Notify Broker Connected.

        Notify the broker that the connection is now connected and the broker
        may continue to setup a chain.
        '''
        self.broker.trigger(
            config_connection_broker.RESUME,
            self,
            config.CONNECTED)

    def notify_broker_disconnected(self):
        '''
        Notify Broker Disconnected.

        Notify the broker that the connection is now disconnected and the
        broker may continue to disconnect the chain.
        '''
        self.broker.trigger(
            config_connection_broker.RESUME,
            self,
            config.DISCONNECTED)

    def notify_broker_aborted(self):
        '''
        Notify Broker Aborted.

        Notify the broker that the connection has aborted and the chain
        processing needs to stop. The broker may continue with the next queued
        chain.
        '''
        self.broker.trigger(
            config_connection_broker.RESUME,
            self,
            config.ABORTED)

    def notify_broker_disconnected_cancel(self):
        '''
        Notify Broker Disconnected Cancel.

        Notify the broker that the connection is now disconnected and the
        broker may not continue to connect the chain.
        '''
        self.broker.trigger(
            config_connection_broker.CANCEL,
            self,
            config.DISCONNECTED)

    def notify_broker_reconnected(self):
        '''
        Notify Broker Reconnected.

        Notify the broker that the connection is now reconnected an the
        broker may continue to reconnect the chain.
        '''
        self.broker.trigger(
            config_connection_broker.RESUME,
            self,
            config.RECONNECTED)

    def notify_broker_restore(self):
        '''
        Notify Broker Restore.

        Notify the broker that something with this connection happened that
        requires to restore the chain.
        '''
        self.broker.trigger(config_connection_broker.RESTORE_CHAIN, self)

    def notify_broker_setup(self):
        '''
        Notify Broker Setup.

        Notify the broker that the connection is setup.
        '''
        self.broker.trigger(
            config_connection_broker.RESUME,
            self,
            config.SETUP)

    def notify_down(self):
        '''
        Notify.

        Notify handler for down state changes. Only notifies when a previous
        up state was active.
        '''
        if self._up is False:
            return
        self._up = False

        logger.info(
            'Connection Down: account: %s %s.'
            % (self.account.name.get(), self.id.get()),
            extra={
                'connection_id': self.id.get(),
                'account_id': self.account.id.get()})

        self.broker.application.get_module('event').notify(
            config.DOWN, self.id.get())

    def notify_failure_to_gui(self):
        '''
        Notify Failure.

        Notify the gui that a failure occured. Request the gui to display
        the log.
        '''
        if config.CONNECTING_PROCESS not in self._state_history:
            return
        try:
            gui_module = self.broker.application.get_module('gui')
            gui_module.model.events.notify_connection_error(
                self.id.get())
        except ModuleNotFoundError:
            pass

    def notify_plugin_abort(self):
        '''
        Notify Plugin Abort

        Notify the plugin to call its async abort method.
        '''
        if self.plugin:
            ioloop.IOLoop.current().call_later(
                0, lambda: self.plugin.abort())

    def notify_plugin_connect(self):
        '''
        Notify Plugin Connect

        Notify the plugin to call its async connect method.
        '''
        if self.plugin:
            ioloop.IOLoop.current().call_later(
                0, lambda: self.plugin.connect())

    def notify_plugin_disconnect(self):
        '''
        Notify Plugin Disconnect

        Notify the plugin to call its async disconnect method.
        '''
        if self.plugin:
            ioloop.IOLoop.current().call_later(
                0, lambda: self.plugin.disconnect())

    def notify_plugin_reconnect(self):
        '''
        Notify Plugin Reconnect

        Notify the plugin to call its async reconnect method.
        '''
        if self.plugin:
            ioloop.IOLoop.current().call_later(
                0, lambda: self.plugin.reconnect())

    def notify_plugin_setup(self):
        '''
        Notify Plugin Setup

        Notify the plugin to call its async setup method.
        '''
        if self.plugin:
            ioloop.IOLoop.current().call_later(
                0, lambda: self.plugin.setup(self.model, self.account))

    def notify_up(self):
        '''
        Notify.

        Notify handler for all up state changes.
        '''
        logger.info(
            'Connection Up: account: %s %s.'
            % (self.account.name.get(), self.id.get()),
            extra={
                'connection_id': self.id.get(),
                'account_id': self.account.id.get()})
        self._up = True
        self.broker.application.get_module('event').notify(
            config.UP, self.id.get())

    def reset(self):
        '''
        Reset.

        Reset the connection state to initial, abort the plugin.
        '''
        if self.plugin:
            self.plugin.reset()
            ioloop.IOLoop.current().call_later(
                0, lambda: self.plugin.abort())

        self.state = config.CREATED
        self._state_history = list()
