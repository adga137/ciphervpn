# -*- coding: utf-8 -*-
# credential.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Credential Dispatcher.

Defines dispatcher functions that are called by connections that need
credentials.
'''
from tornado import gen
from netsplice.config import backend as config_backend
from netsplice.model.credential import (
    credential as credential_model
)
import netsplice.backend.event.names as event_names
import netsplice.backend.event.types as event_types
from netsplice.util import get_logger
from netsplice.util.errors import NotFoundError, NoCredentialsProvidedError


logger = get_logger()


class credential(object):
    '''
    Credential implementation.

    Provides implementations for the credential dispatcher that allow the
    software to handle credentials for connections.
    '''
    TYPE_USERNAME_PASSWORD = 0
    TYPE_PRIVATE_KEY_PASSWORD = 1

    def __init__(self, application, connection):
        '''
        Initialize module.

        Initialize the members.
        '''
        self.application = application
        self.connection = connection
        self.gui_model = self.application.get_module('gui').model
        self.credential_type = self.TYPE_USERNAME_PASSWORD

    def _store_credential(self, user_credential):
        '''
        Store Credential.

        Save the given user_credential to the associated store.
        '''
        if user_credential.store_password.get() == \
                config_backend.STORE_PASSWORD_KEYSTORE:
            # Store Keyring Credential
            try:
                import keyring
                account_id = user_credential.account_id.get()
                if self.credential_type == self.TYPE_USERNAME_PASSWORD:
                    keyring.set_password(
                        account_id, 'password', user_credential.password.get())
                    keyring.set_password(
                        account_id, 'username', user_credential.username.get())
                elif self.credential_type == self.TYPE_PRIVATE_KEY_PASSWORD:
                    keyring.set_password(
                        account_id, 'pkpassword',
                        user_credential.password.get())
            except RuntimeError as errors:
                logger.warn(
                    'OS Keystore not available: %s'
                    % (str(errors),))
            except Exception as errors:
                logger.warn(
                    'Key storage failed, not storing. %s %s'
                    % (str(type(errors)), str(errors)))

        if user_credential.store_password.get() != \
                config_backend.STORE_PASSWORD_NEVER:
            self.gui_model.credentials.add(user_credential)

    @gen.coroutine
    def get_credential(self, account_id):
        '''
        Get Credentials for an account.

        Return the Credentials for the account_id. Will return empty
        credentials when no credentials are stored.
        '''
        if self.gui_model.credentials.is_wrong(account_id):
            '''
            Handle wrong credential.

            Ask the user for credential when a previous use for this account
            led to an AUTH_FAILED message.
            '''
            user_credential = yield self.get_credential_from_user(account_id)
            self._store_credential(user_credential)
            raise gen.Return(user_credential)

        try:
            stored_credential = yield self.get_credential_from_store(
                account_id)
            raise gen.Return(stored_credential)
        except NotFoundError:
            pass

        user_credential = yield self.get_credential_from_user(account_id)
        self._store_credential(user_credential)
        raise gen.Return(user_credential)

    @gen.coroutine
    def get_credential_from_store(self, account_id):
        '''
        Get credential from store.

        Try to find the credential for the given account_id in the available
        stores.
        First the credential is searched in the gui-session, then in the
        OS keyring, then in the preference store.
        '''
        # Session Credential
        try:
            gui_credential = self.gui_model.credentials.find_by_account_id(
                account_id)
            logger.info('Using session credential', extra={
                'connection_id': self.connection.id.get()
            })
            raise gen.Return(gui_credential)
        except NotFoundError:
            pass

        # Keyring Credential
        try:
            import keyring
            username = None
            password = None
            if self.credential_type == self.TYPE_USERNAME_PASSWORD:
                password = keyring.get_password(account_id, 'password')
                username = keyring.get_password(account_id, 'username')
            elif self.credential_type == self.TYPE_PRIVATE_KEY_PASSWORD:
                password = keyring.get_password(account_id, 'pkpassword')
                username = ''
            if username is not None and password is not None:
                credential = credential_model()
                credential.username.set('')
                credential.password.set('')
                credential.username.set(username)
                credential.password.set(password)
                logger.info('Using keyring credential', extra={
                    'connection_id': self.connection.id.get()
                })
                raise gen.Return(credential)
        except gen.Return as return_value:
            raise return_value
        except RuntimeError as errors:
            logger.warn(
                'OS Keystore not available: %s'
                % (str(errors),))
        except Exception as errors:
            logger.warn(
                'Key storage failed, not storing. %s %s'
                % (str(type(errors)), str(errors)))

        raise NotFoundError(
            'A stored credential for account %s was not found.'
            % (account_id,))

    @gen.coroutine
    def get_credential_from_user(self, account_id):
        '''
        Get Credentials from User.

        Create an event for the GUI and wait for the GUI to send credentials
        or cancel the dialog.
        '''
        request_username_password_event = self.gui_model.new_event()
        request_username_password_event.type.set(event_types.QUERY)
        if self.credential_type == self.TYPE_USERNAME_PASSWORD:
            request_username_password_event.name.set(
                event_names.USERNAME_PASSWORD)
            logger.info(
                'Waiting for credentials (username and password) from user.',
                extra={
                    'account_id': account_id
                })
        elif self.credential_type == self.TYPE_PRIVATE_KEY_PASSWORD:
            request_username_password_event.name.set(
                event_names.PRIVATE_KEY_PASSWORD)
            logger.info(
                'Waiting for credentials (private key) from user.',
                extra={
                    'account_id': account_id
                })
        request_username_password_event.data.set(account_id)
        self.gui_model.add_event(request_username_password_event)
        user_credential = yield self.gui_model.credentials. \
            wait_for_account_id(
                account_id)
        if user_credential is None:
            logger.warn('User canceled credential input', extra={
                'account_id': account_id
            })
            raise NoCredentialsProvidedError(
                'No credential found for %s. User did not provide'
                % (account_id,))
        raise gen.Return(user_credential)
