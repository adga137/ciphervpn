# -*- coding: utf-8 -*-
# test_chain.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Connection Chain.

Unit tests for connection chains. Shares code with the test_integration and
test_connection_broker.
'''
import tornado
from tornado.testing import AsyncTestCase
from netsplice.util.errors import NotFoundError
from .test_chain_common import (
    get_complex_test_objects,
    FIRST_ACCOUNT_ID, EIGHT_ACCOUNT_ID, OTHER_ACCOUNT_ID,
    get_logger
)
logger = get_logger()


class ConnectionIntegrationTests(AsyncTestCase):
    '''
    Test the various combinations how the connection_broker may act on
    events.
    All tests are async and simulate the expected state changes with
    the mock_async_* on the expected connections.
    As the mocks are simple, it is not likely that more/other state changes
    occur. The order of the mock_async_* matters.
    '''
    @tornado.testing.gen_test
    def test_find_by_account_id_returns_instance(self):
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        result = chain_instance.find_by_account_id(FIRST_ACCOUNT_ID)
        assert(result.account.id.get() == FIRST_ACCOUNT_ID)

    @tornado.testing.gen_test
    def test_find_by_account_id_raises_not_found_error(self):
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        try:
            chain_instance.find_by_account_id(OTHER_ACCOUNT_ID)
            assert(False)  # No exception raised.
        except NotFoundError:
            assert(True)  # NotFoundError was raised.

    @tornado.testing.gen_test
    def test_find_by_account_id_ignores_None_account_ids(self):
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        for i in chain_instance:
            if i.account == h:
                continue
            # invalidate all accounts except 8/h
            i.account = None
        result = chain_instance.find_by_account_id(EIGHT_ACCOUNT_ID)
        assert(result.account.id.get() == EIGHT_ACCOUNT_ID)
