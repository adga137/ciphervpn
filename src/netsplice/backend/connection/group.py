# -*- coding: utf-8 -*-
# group.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
(Account) Group Dispatcher.

Dispatcher functions to be used in controllers concerning groups of accounts.
'''
from tornado import gen
from netsplice.util import get_logger
from netsplice.backend.connection.chain import (
    chain as connection_chain
)
from netsplice.util.errors import NotFoundError

logger = get_logger()


class group(object):
    '''
    Stateless Group dispatcher.

    Middleware creates instance to expose application and other modules to
    the functions.
    '''

    def __init__(self, application):
        '''
        Initialize Module.

        Setup members from application.
        '''
        self.application = application
        self.broker = self.application.get_module(
            'connection').broker
        self.event_module = self.application.get_module(
            'event')
        self.gui_model = self.application.get_module('gui').model
        self.preferences_model = self.application.get_module(
            'preferences').model

    def _get_chain(self, account_id):
        '''Get Chain.

        Return a connection_chain from the preferences

        Arguments:
            account_id (string): id of a account

        Returns:
            connection_chain -- instance with account_list, sequence and \
                failure mode set.
        '''

        accounts = self.preferences_model.accounts
        chains = self.preferences_model.chains
        account_chain = chains.get_connection_chain(
            account_id, accounts, connection_chain)
        return account_chain

    @gen.coroutine
    def connect(self, group_id):
        '''
        Connect.

        Connect Accounts that belong to the group. All accounts are being
        setup before their connect is executed.
        '''
        grouped_accounts = self.preferences_model.grouped_accounts
        groups = self.preferences_model.groups
        accounts = self.preferences_model.accounts
        account_list = grouped_accounts.get_account_list(
            grouped_accounts.connection_order(group_id, groups), accounts)

        # Setup all connections in the account_list, causing all credentials
        # to be queried.
        for account_model_instance in account_list:
            chain = self._get_chain(account_model_instance.id.get())
            self.broker.connect_all(chain)

        raise gen.Return(True)

    @gen.coroutine
    def disconnect_all(self):
        '''
        Disconnect all.

        Disconnect all Accounts in reverse connect order.
        '''
        self.broker.disconnect_all()

        raise gen.Return(True)

    @gen.coroutine
    def disconnect(self, group_id):
        '''
        Disconnect.

        Disconnect all accounts in a group.
        '''
        grouped_accounts = self.preferences_model.grouped_accounts
        groups = self.preferences_model.groups
        accounts = self.preferences_model.accounts
        account_list = grouped_accounts.get_account_list(
            grouped_accounts.connection_order(group_id, groups), accounts)
        account_list.reverse()
        for account_model_instance in account_list:
            account_id = account_model_instance.id.get()
            try:
                chain = self._get_chain(account_id)
                if self.broker.account_is_connected(account_model_instance):
                    self.broker.disconnect(account_model_instance, chain)
            except NotFoundError:
                pass
            self.gui_model.commit()

        raise gen.Return(True)

    @gen.coroutine
    def reconnect(self, group_id):
        '''
        Reconnect.

        Reconnect all accounts in a group.
        '''
        grouped_accounts = self.preferences_model.grouped_accounts
        groups = self.preferences_model.groups
        accounts = self.preferences_model.accounts
        account_list = grouped_accounts.get_account_list(
            grouped_accounts.connection_order(group_id, groups), accounts)
        for account_model_instance in account_list:
            account_id = account_model_instance.id.get()
            try:
                chain = self._get_chain(account_id)
                if self.broker.account_is_connected(account_model_instance):
                    self.broker.reconnect(account_model_instance, chain)
            except NotFoundError:
                pass
        self.gui_model.commit()

        raise gen.Return(True)
