# -*- coding: utf-8 -*-
# test_connection_broker.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Connection Broker.

Unittests for connection chains. Shares code with the test_integration and
test_connection_broker.
'''
import tornado
import mock
from tornado.testing import AsyncTestCase
from netsplice.config import connection as config_connection


from .test_chain_common import (
    get_complex_test_objects,
    FIRST_ACCOUNT_ID,
    get_logger
)

logger = get_logger()


class ConnectionIntegrationTests(AsyncTestCase):
    '''
    Test the various combinations how the connection_broker may act on
    events.
    All tests are async and simulate the expected state changes with
    the mock_async_* on the expected connections.
    As the mocks are simple, it is not likely that more/other state changes
    occur. The order of the mock_async_* matters.
    '''
    @mock.patch(
        'netsplice.backend.connection.'
        'connection_broker.account_state')
    @tornado.testing.gen_test
    def test_account_is_reconnecting(self, mock_account_state):
        (cb, chain_instance, a, b, c, d, e, f, g, h) = \
            get_complex_test_objects()
        mock_account_state.return_value = config_connection.RECONNECTING
        result = cb.account_is_reconnecting(FIRST_ACCOUNT_ID)
        assert(result is True)
