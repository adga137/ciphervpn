# -*- coding: utf-8 -*-
# download_item.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for a single download.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.validator.download_id import (
    download_id as download_id_validator
)
from netsplice.model.validator.max import (
    max as max_validator
)
from netsplice.model.validator.min import (
    min as min_validator
)
from netsplice.model.validator.uri import (
    uri as uri_validator
)
from netsplice.model.validator.date import (
    date as date_validator
)
from netsplice.model.validator.none import (
    none as none_validator
)
from netsplice.model.validator.path import (
    path as path_validator
)
from netsplice.model.validator.signature import (
    signature as signature_validator
)
from netsplice.model.validator.sign_key import (
    sign_key as sign_key_validator
)
from netsplice.model.validator.sha1sum import (
    sha1sum as sha1sum_validator
)
from netsplice.model.validator.sha256sum import (
    sha256sum as sha256sum_validator
)
from netsplice.model.validator.boolean import (
    boolean as boolean_validator
)
from netsplice.model.validator.message import (
    message as message_validator
)
from netsplice.util import get_logger
from .fingerprint_list import fingerprint_list

logger = get_logger()


class download_item(marshalable):
    def __init__(self):

        self.id = field(
            required=True,
            validators=[download_id_validator()])

        self.active = field(
            required=True,
            validators=[boolean_validator()])

        self.url = field(
            required=True,
            validators=[uri_validator()])

        self.fingerprints = fingerprint_list()

        self.signature = field(
            required=False,
            validators=[none_validator(exp_or=[signature_validator()])])

        self.sign_key = field(
            required=False,
            validators=[none_validator(exp_or=[sign_key_validator()])])

        self.sha1sum = field(
            required=False,
            validators=[none_validator(exp_or=[sha1sum_validator()])])

        self.sha256sum = field(
            required=False,
            validators=[none_validator(exp_or=[sha256sum_validator()])])

        self.destination = field(
            required=True,
            validators=[path_validator()])

        self.start_date = field(
            required=True,
            validators=[date_validator()])

        self.complete = field(
            required=True,
            validators=[min_validator(0), max_validator(100)])

        self.bps = field(
            required=True,
            validators=[min_validator(0)])

        self.errors = field(
            required=True,
            validators=[boolean_validator()])

        self.error_message = field(
            required=True,
            validators=[none_validator(exp_or=[message_validator()])])

        self.signature_ok = field(
            required=True,
            validators=[none_validator(exp_or=[boolean_validator()])])

        self.checksum_ok = field(
            required=True,
            validators=[none_validator(exp_or=[boolean_validator()])])

        self._process = None
