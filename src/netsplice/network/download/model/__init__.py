# -*- coding: utf-8 -*-
# model.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for network download process. Stores all downloads.
'''


from netsplice.util.model.marshalable import marshalable
from netsplice.network.download.model.download_list import (
    download_list as download_list_model
)


class model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.downloads = download_list_model()
