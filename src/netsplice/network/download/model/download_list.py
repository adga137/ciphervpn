# -*- coding: utf-8 -*-
# download_list_model.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for listing downloads.

Marshalable List of download_list_items.
'''
import uuid
from datetime import datetime

from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.network.download.model.download_item import (
    download_item as download_item_model
)
from netsplice.util.errors import NotFoundError


class download_list(marshalable_list):
    '''
    Download List.

    Marshalable model that contains multiple items of
    download_list_item_model.
    '''

    def __init__(self):
        '''
        Initialize Module.

        Initializes the marshalable_list.
        '''
        marshalable_list.__init__(self, download_item_model)

    def create(self, download_model_instance):
        '''
        Create.

        Create new download instance, fill it with values from the given
        instance and return the new instance.

        Arguments:
            download_model_instance (model): Instance with uri, fingerprint \
                etc
        '''
        download = self.item_model_class()
        now = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
        download.active.set(False)
        download.id.set(str(uuid.uuid4()))
        download.url.set(download_model_instance.url.get())
        download.fingerprints.set_list(
            download_model_instance.fingerprints.get_list())
        download.signature.set(download_model_instance.signature.get())
        download.sign_key.set(download_model_instance.sign_key.get())
        download.sha1sum.set(download_model_instance.sha1sum.get())
        download.sha256sum.set(download_model_instance.sha256sum.get())
        download.destination.set(download_model_instance.destination.get())
        download.start_date.set(now)
        download.complete.set(0)
        download.bps.set(0)
        download.errors.set(False)
        download.error_message.set(None)
        download.signature_ok.set(None)
        self.append(download)
        return download

    def find_by_id(self, download_id):
        '''
        Find By Id.

        Find the download with the given download_id or raise a
        NotFoundError.
        '''
        for index, download in enumerate(self):
            if download.id.get() == download_id:
                return download
        raise NotFoundError(download_id)
