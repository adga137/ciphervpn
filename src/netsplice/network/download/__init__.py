# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.ipc.route import get_module_route
from netsplice.network.download.model import model as module_model
from netsplice.network.download.download import download

name = 'download'

endpoints = get_module_route(
    'netsplice.network',
    [
        (r'/module/download', 'module'),
        (r'/module/download/(?P<download_id>[^\/]+)', 'module'),
    ])

model = module_model()


def find_download(download_id):
    '''
    Find Download.

    Find a existing download.

    Arguments:
        download_id (string): id of active download.
    '''
    download_model = model.find_by_id(download_id)
    return download_model._process
