# -*- coding: utf-8 -*-
# log_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for any Log Item
'''

import datetime
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.log_item_extra import (
    log_item_extra as log_item_extra_model
)


class log_item(marshalable):

    def __init__(self, log_instance=None):
        marshalable.__init__(self)
        self.index = field()
        self.date = field()
        self.level = field(default="debug")
        self.origin = field()
        self.message = field()
        self.formated_message = field(required=False)
        self.code_location = field(required=False)
        self.extra = log_item_extra_model()
        if log_instance is not None:
            self.index.set(log_instance.index.get())
            self.date.set(log_instance.date.get())
            self.level.set(log_instance.level.get())
            self.origin.set(log_instance.origin.get())
            self.message.set(log_instance.message.get())

    def prepare(self, level, origin, message):
        self.index.set(0)
        self.date.set(datetime.datetime.utcnow().isoformat())
        self.level.set(level)
        self.origin.set(origin)
        self.message.set(message)
        self.formated_message.set(message)

    def set_from_dict(self, log_dict_item):
        self.date.set(log_dict_item['date'])
        self.level.set(log_dict_item['level'])
        self.origin.set(log_dict_item['origin'])
        self.message.set(log_dict_item['message'])
        self.formated_message.set(log_dict_item['formated_message'])
        self.code_location.set(log_dict_item['code_location'])
        self.extra.set_from_dict(log_dict_item['extra'])
