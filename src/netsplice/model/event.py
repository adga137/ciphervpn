# -*- coding: utf-8 -*-
# event.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for any Event
'''
import time

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable


class event(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.index = field()
        self.date = field()
        self.origin = field(required=False)
        self.type = field()
        self.name = field()
        self.data = field(required=False)

    def prepare(self, type_name, name):
        self.type.set(type_name)
        self.name.set(name)
        self.index.set(-1)
        self.origin.set('')
        self.date.set(time.time())
        self.data.set('')
