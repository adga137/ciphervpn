# -*- coding: utf-8 -*-
# test_test_configuration_list.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Test for Configuration List.
'''
import mock

from .configuration_item import configuration_item
from .configuration_list import configuration_list
from netsplice.util.errors import NotFoundError

def get_testobject():
    return configuration_list()

def test_find_by_key_unknown_raises():
    l = get_testobject()
    try:
        l.find_by_key('unknown')
        assert(False)  # NotFoundError expected
    except NotFoundError:
        assert(True)


def test_find_by_key_known_returns():
    l = get_testobject()
    i = configuration_item(0)
    i.key.set('test')
    l.append(i)
    result = l.find_by_key('test')
    assert(isinstance(result, (configuration_item,)))
