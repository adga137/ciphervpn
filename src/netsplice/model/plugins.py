# -*- coding: utf-8 -*-
# plugins.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Plugin Preferences
'''
from netsplice.model.plugin_item import (
    plugin_item as plugin_item_model
)
from netsplice.util.model.marshalable_list_persistent import (
    marshalable_list_persistent
)
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.util import get_logger
from netsplice.util.errors import NotFoundError
logger = get_logger()


class plugins(marshalable_list_persistent):
    '''
    Plugins.

    Plugins for preferences. Add a plugins marshalable collection to allow
    registration for additional properties. Plugins are identified by name
    and can be collection or object plugins.
    Collection plugins add the properties to each item in the collection.
    Object plugins hold only one instance of properties.
    '''
    def __init__(self, owner, prefix=None):
        marshalable_list_persistent.__init__(
            self, plugin_item_model, owner, prefix)
        self.required = False
        self.serialize_json = False

    def commit(self):
        '''
        Commit.

        Commit the values stored for the plugin.
        '''
        for plugin in self:
            plugin.commit()
        marshalable.commit(self)

    def find_by_name(self, name):
        '''
        Find by name.

        Return the plugin_model for the given name or raise a NotFoundError
        when no plugin registered for that name.
        '''
        for plugin in self:
            if plugin.name.get() == name:
                return plugin
        raise NotFoundError(
            '%s is not a registered plugin.'
            % (name,))

    def get_collection_plugin_instance(self, plugin_name, instance_id):
        '''
        Get Plugin Instance.

        Return the plugin instance associated with the collection instance.
        '''
        try:
            owner = self.get_owner()
            if owner is None:
                raise NotFoundError(
                    'Owning collection for %s not found' % (str(self),))
            instance = owner.find_by_id(instance_id)
            return instance.__dict__[plugin_name]
        except KeyError:
            raise NotFoundError(
                'Collection Plugin %s could not be found'
                % (plugin_name,))
        except AttributeError:
            raise NotImplementedError(
                'The owning collection %s does not implement find_by_id'
                % (type(owner),))

    def register_collection_plugin(self, plugin_name, plugin_model_instance):
        '''
        Register abstract plugin.

        Make the given plugin_model_instance - 'type' - responsible for the
        given plugin name. Remove any plugins that have registered with that
        name.
        Collection plugins are used all instances of a marshalable_list.
        When items exist in the collection, they are initialized and loaded.
        '''
        assert(isinstance(plugin_model_instance, (marshalable,)))
        assert(callable(plugin_model_instance.migrate))
        try:
            stored_instance = self.find_by_name(plugin_name)
            self.remove(stored_instance)
        except NotFoundError:
            pass
        plugin_model_instance.name.set(plugin_name)
        plugin_model_instance.is_registered = True
        plugin_model_instance.is_collection = True
        self.append(plugin_model_instance)
        owner = self.get_owner()
        if isinstance(owner, (list,)):
            # apply to all items
            for item in owner:
                # initialize defaults
                item.__dict__[plugin_name] = type(plugin_model_instance)(owner)
                item.__dict__[plugin_name].name.set(plugin_name)
                item.__dict__[plugin_name].serialize_json = False
                item.__dict__[plugin_name].required = False
            try:
                # Load Instances in backend, other apps receive them with REST
                self.load_plugin_instances()
                if self.get_migrated():
                    for item in owner:
                        if not item.get_migrated():
                            continue
                        item.commit()
            except AttributeError:
                pass

    def register_plugin(self, plugin_name, plugin_model_instance):
        '''
        Register Plugin.

        Make the given plugin_model_instance - 'type' - responsible for the
        given plugin name. Remove any plugins that have registered with that
        name.
        Load the persistent settings for that plugin.
        '''
        assert(isinstance(plugin_model_instance, (marshalable,)))
        assert(callable(plugin_model_instance.migrate))
        try:
            stored_instance = self.find_by_name(plugin_name)
            self.remove(stored_instance)
        except NotFoundError:
            pass
        plugin_model_instance.name.set(plugin_name)
        plugin_model_instance.is_registered = True
        self.append(plugin_model_instance)
        try:
            # Load Plugins in backend, other apps receive them with REST
            self.load_plugins()
            if self.get_migrated():
                self.commit()
        except AttributeError:
            pass
