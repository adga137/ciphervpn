# -*- coding: utf-8 -*-
# named_value_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Model for listing named items.
'''

from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.util.errors import NotFoundError
from netsplice.model.named_value_item import (
    named_value_item as named_value_item_model
)


class named_value_list(marshalable_list):
    '''
    Marshalable model that contains multiple items of
    named_value_item_model.
    '''

    def __init__(self):
        marshalable_list.__init__(self, named_value_item_model)

    def find_by_name(self, name):
        '''
        Find the given name in the current list. Raises NotFoundError if the
        requested name is not in the list.
        '''
        for item in self:
            if item.name.get() == name:
                return item
        raise NotFoundError(name)

    def add_value(self, name, value):
        '''
        Add a Value to the named value list.
        '''
        new_value = self.item_model_class()
        new_value.name.set(name)
        new_value.value.set(value)
        self.append(new_value)

    def get_value(self, name):
        '''
        Return the validated and cleaned value. Raises NotFoundError if the
        requested name is not in the list.
        '''
        return self.find_by_name(name).value.get()
