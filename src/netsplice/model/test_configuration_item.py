# -*- coding: utf-8 -*-
# test_test_configuration_item.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Test for Configuration Item.
'''
import mock

from .configuration_item import configuration_item

def get_testobject(index=0):
    return configuration_item(index)

def test_add_value_adds():
    i = get_testobject()
    assert(len(i.values) == 0)
    i.add_value('test', 'value')
    assert(len(i.values) == 1)


def test_get_key_returns_key():
    i = get_testobject()
    i.key.set('test')
    result = i.get_key()
    assert(result == 'test')


def test_get_value_returns_value():
    i = get_testobject()
    i.key.set('test')
    i.add_value('test', 'value')
    result = i.get_value('test')
    assert(result == 'value')
