# -*- coding: utf-8 -*-
# log_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for any Event
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.model.log_item import log_item as log_item_model


class log_list(marshalable_list):

    def __init__(self, log_item_model_class=None):
        if log_item_model_class is None:
            marshalable_list.__init__(self, log_item_model)
        else:
            marshalable_list.__init__(self, log_item_model_class)
        self._index = 0

    def new_log(self, level='debug', origin='unknown', message=''):
        log_item_instance = self.item_model_class()
        log_item_instance.prepare(level, origin, message)
        self._index += 1
        log_item_instance.index.set(self._index)
        return log_item_instance

    def append_from_dict(self, log_dict_item):
        log_item_instance = self.new_log()
        log_item_instance.set_from_dict(log_dict_item)
        self.append(log_item_instance)
