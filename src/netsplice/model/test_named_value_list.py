# -*- coding: utf-8 -*-
# test_test_named_value_list.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Test for Named Value List.
'''
import mock

from .named_value_item import named_value_item
from .named_value_list import named_value_list
from netsplice.util.errors import NotFoundError

def get_testobject():
    return named_value_list()

def test_find_by_name_unknown_raises():
    l = get_testobject()
    try:
        l.find_by_name('unknown')
        assert(False)  # NotFoundError expected
    except NotFoundError:
        assert(True)


def test_find_by_name_known_returns():
    l = get_testobject()
    i = named_value_item()
    i.name.set('test')
    l.append(i)
    result = l.find_by_name('test')
    assert(isinstance(result, (named_value_item,)))
