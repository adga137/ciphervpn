# -*- coding: utf-8 -*-
# configuration_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Model for any Configuration Item
'''

import time
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.named_value_list import (
    named_value_list as named_value_list_model
)


class configuration_item(marshalable):

    def __init__(self, index):
        marshalable.__init__(self)
        self.index = field()
        self.key = field()
        self.values = named_value_list_model()
        self.index.set(index)

    def add_value(self, value_name, value):
        '''
        Add a value with the given name to the values list
        '''
        self.values.add_value(value_name, value)

    def get_key(self):
        '''
        Get Key.

        Return the key of the configuration item.
        '''
        return self.key.get()

    def get_value(self, value_name):
        '''
        Return the value (validated & cleaned) from the values list. Raises
        NotFoundException when the requested value_name is not available.
        '''
        return self.values.get_value(value_name)
