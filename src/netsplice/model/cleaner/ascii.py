# -*- coding: utf-8 -*-
# ascii.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.model.cleaner import cleaner
from netsplice.util import basestring


class ascii(cleaner):
    '''
    Cleaner for strings that will only return letters from the ASCII alphabet
    (upper and lowercase A-Z, nothing else).
    The cleaner will always assume a empty string as default value.
    '''
    def __init__(self):
        cleaner.__init__(self, '')

    def clean(self, value):
        '''
        Iterate all value characters and only take allowed characters to the
        cleaned_value. When the given value is not a basestring it will return
        a empty string (the default_value).
        '''
        cleaned_value = ''
        if not isinstance(value, basestring):
            return self.default_value
        for c in value:
            if c > 'A' and c < 'Z' and c > 'a' and c < 'z':
                cleaned_value += c
        return cleaned_value
