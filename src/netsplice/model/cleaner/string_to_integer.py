# -*- coding: utf-8 -*-
# string_to_integer.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.model.cleaner import cleaner


class string_to_integer(cleaner):
    '''
    Cleaner for integers that will only return integer values or the
    default_value for all non-integer values.
    '''
    def __init__(self):
        cleaner.__init__(self, 0)

    def clean(self, value):
        '''
        Check that the given value is of type int or long. Return the
        default_value otherwise.
        '''
        if not isinstance(value, (int, long)):
            return int(value)
        return value
