# -*- coding: utf-8 -*-
# credential_list_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Credential Lists.

Manage multiple credentials.
'''

from tornado import gen, locks

from netsplice.config import backend as config_backend
from netsplice.util.model.field import field
from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.model.credential import credential as credential_item
from netsplice.util.errors import NotFoundError
from netsplice.util import get_logger

logger = get_logger()


class credential_list(marshalable_list):
    '''
    Model for Credential Lists.

    Manage multiple credentials.
    '''

    def __init__(self):
        '''
        Initialize.

        Initialize persistent and transient members.
        '''
        marshalable_list.__init__(self, credential_item)

        self.master_key = field()

        self._wait_map = dict()
        self._wrong_accounts = list()

    def add(self, credential_model_instance):
        '''
        Add Credential.

        Adds the given credential model instance to the list and removes a
        possibly existing credential for the same account before. When
        the account_id is in the wait-map, the condition gets notified so the
        other process may continue.
        '''
        account_id = credential_model_instance.account_id.get()
        try:
            existing_credential = self.find_by_account_id(account_id)
            self.remove(existing_credential)
        except NotFoundError:
            pass
        self.append(credential_model_instance)
        if account_id in self._wait_map:
            self._wait_map[account_id].notify()
            del self._wait_map[account_id]

    def cancel(self):
        '''
        Cancel all waiting requests.

        Notify all wait conditions and clear the wait-map.
        '''
        for account_id in self._wait_map.keys():
            self._wait_map[account_id].notify()
        self._wait_map = dict()

    def mark_wrong(self, account_id):
        '''
        Mark as wrong.

        Mark credential als wrong so the next use will cause query event for
        the GUI.
        '''
        if account_id not in self._wrong_accounts:
            self._wrong_accounts.append(account_id)

    def mark_ok(self, account_id):
        '''
        Remove any wrong-mark for the credential.

        Removes the account_id from the wrong_accounts list.
        '''
        if account_id in self._wrong_accounts:
            self._wrong_accounts.remove(account_id)

    def is_wrong(self, account_id):
        '''
        Is account_id credential marked as wrong.

        Returns true if the account_id has been marked wrong previously.
        '''
        return account_id in self._wrong_accounts

    def clean(self):
        '''
        Cleanup Credentials.

        Remove all but the session credentials.
        '''
        remove_credentials = []
        for credential in self:
            if credential.store_password.get() != \
                    config_backend.STORE_PASSWORD_SESSION:
                remove_credentials.append(credential)
        for credential in remove_credentials:
            credential.purge()
            self.remove(credential)
            logger.debug(
                'removed credential for %s from memory'
                % (credential.account_id.get(),))
        logger.debug('%d credentials still stored in memory' % (len(self),))

    def find_by_account_id(self, account_id):
        '''
        Find by account_id.

        Finds the credential with the given account_id or raises a
        NotFoundError.
        '''
        for credential in self:
            if credential.account_id.get() == account_id:
                return credential
        raise NotFoundError('credential for %s not found' % (account_id,))

    @gen.coroutine
    def wait_for_account_id(self, account_id):
        '''
        Create a gen.lock that is updated by the gui/credential endpoint.

        Reuse a existing condition. When the condition gets notified, the
        credential_list is searched for the account_id and returned. When
        the account_id could not be found, None is returned.
        '''
        if account_id in self._wait_map:
            lock = self._wait_map[account_id]
        else:
            lock = locks.Condition()
            self._wait_map[account_id] = lock

        logger.debug('Waiting for credential response')
        yield lock.wait()
        try:
            credential = self.find_by_account_id(account_id)
            logger.debug('Credential for account was found')
            raise gen.Return(credential)
        except NotFoundError:
            pass
        raise gen.Return(None)
