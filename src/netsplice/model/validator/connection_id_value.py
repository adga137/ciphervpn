# -*- coding: utf-8 -*-
# connection_id.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Validator for connection id's.

Validator for connection ids that may be None.
'''

from netsplice.model.validator.connection_id import connection_id
from netsplice.util import get_logger

logger = get_logger()


class connection_id_value(connection_id):
    def __init__(self):
        connection_id.__init__(self)

    def is_valid(self, value):
        '''
        Account Ids are uuid4's (36 bytes)
        The value may be None
        '''
        if value is None:
            return True
        return connection_id.is_valid(self, value)
