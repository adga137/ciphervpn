# -*- coding: utf-8 -*-
# ip_address.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Validator for ip_addresses.
'''

from netsplice.util.model.validator import validator
from netsplice.util import basestring
import netsplice.util.win_inet_pton
import socket


class ip_address(validator):
    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        '''
        ipv4_addresses are nonempty strings that can be resolved by socket
        aton or pton.
        '''
        if not isinstance(value, (basestring)):
            return False

        if value == '':
            return False

        is_ipv4 = self.is_valid_ipv4_address(value)
        is_ipv6 = False
        if not is_ipv4:
            is_ipv6 = self.is_valid_ipv6_address(value)
        if not is_ipv4 and not is_ipv6:
            return False

        return True

    def is_valid_ipv4_address(self, address):
        try:
            socket.inet_pton(socket.AF_INET, address)
        except AttributeError:
            # No inet_pton here, sorry
            try:
                socket.inet_aton(address)
            except socket.error:
                return False
            return address.count('.') == 3
        except socket.error:
            # not a valid address
            return False

        return True

    def is_valid_ipv6_address(self, address):
        try:
            socket.inet_pton(socket.AF_INET6, address)
        except AttributeError:
            return False
        except socket.error:
            # not a valid address
            return False
        return True
