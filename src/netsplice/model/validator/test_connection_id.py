# -*- coding: utf-8 -*-
# test_connection_id.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Connection ID validator.

Checks Validator.
'''

from netsplice.model.validator.connection_id import (
    connection_id
)


def get_test_object():
    validator = connection_id()
    return validator


def test_is_valid_with_none_returns_false():
    validator = get_test_object()
    result = validator.is_valid(None)
    assert(result is False)


def test_is_valid_with_short_string_returns_false():
    validator = get_test_object()
    result = validator.is_valid('tooshort')
    assert(result is False)


def test_is_valid_with_int_returns_false():
    validator = get_test_object()
    result = validator.is_valid(123)
    assert(result is False)


def test_is_valid_without_dash_at_8_returns_false():
    validator = get_test_object()
    result = validator.is_valid('012345678901234567890123456789012345')
    assert(result is False)


def test_is_valid_without_dash_at_13_returns_false():
    validator = get_test_object()
    result = validator.is_valid('01234567-901234567890123456789012345')
    assert(result is False)


def test_is_valid_without_dash_at_18_returns_false():
    validator = get_test_object()
    result = validator.is_valid('01234567-9013-4567890123456789012345')
    assert(result is False)


def test_is_valid_without_dash_at_23_returns_false():
    validator = get_test_object()
    result = validator.is_valid('01234567-9013-4567-90123456789012345')
    assert(result is False)


def test_is_valid_with_all_dashes_returns_true():
    validator = get_test_object()
    result = validator.is_valid('01234567-9011-3456-9012-456789012345')
    assert(result is True)


def test_is_valid_with_valid_account_id_returns_true():
    validator = get_test_object()
    result = validator.is_valid('31c5aa5a-d1df-4ee8-9ede-86898effe163')
    assert(result is True)
