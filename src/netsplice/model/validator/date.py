# -*- coding: utf-8 -*-
# date.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

import re
from netsplice.util.model.validator import validator
from netsplice.util import basestring
from netsplice.util import get_logger

logger = get_logger()


class date(validator):
    '''
    Date Validator. Validates that a value is a date string.
    '''

    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        '''
        Ensure the given value is a string that contains
        numbers that match YYYY-MM-DDTHH:MM:SSZ
        '''
        if not isinstance(value, (basestring, int)):
            return False

        rx_date = re.compile(
            '^([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]'
            'T[0-9][0-9]:[0-9][0-9]:[0-9][0-9]Z)$')

        if re.match(rx_date, value):
            return True
        return False
