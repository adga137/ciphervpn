# -*- coding: utf-8 -*-
# none.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.model.validator import validator


class none(validator):
    '''
    None Validator. Validates that a value is none or one of the allowed sub-
    validators.
    '''

    def __init__(self, exp_or=[]):
        validator.__init__(self)
        self.exp_or = exp_or

    def is_valid(self, value):
        '''
        Ensure the given value is a value that is in the allowed values.
        '''
        if value is None:
            return True
        for or_validator in self.exp_or:
            state = or_validator.is_valid(value)
            if state:
                return True
        return False
