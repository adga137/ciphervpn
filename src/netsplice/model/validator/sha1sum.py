# -*- coding: utf-8 -*-
# sha1sum.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Validator for sha1sum.
'''
import re
from netsplice.util.model.validator import validator
from netsplice.util import basestring


class sha1sum(validator):
    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        '''
        sign_keys are nonempty base64 encoded strings.
        '''
        if not isinstance(value, (basestring)):
            return False

        if value == '':
            return False
        if len(value) != 40:
            return False
        rx_valid = re.compile('^([0-9a-f]*)$')

        if re.match(rx_valid, value):
            return True

        return False
