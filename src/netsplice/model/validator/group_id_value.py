# -*- coding: utf-8 -*-
# group_id_value.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Validator for backend Account Group id's
'''

from netsplice.model.validator.group_id import group_id
from netsplice.util import get_logger

logger = get_logger()


class group_id_value(group_id):
    def __init__(self):
        group_id.__init__(self)

    def is_valid(self, value):
        '''
        Group Ids are uuids (strings of 36 bytes) and are never None.
        This value however may be None for the case that it's a parent
        '''
        if value is None:
            return True
        return group_id.is_valid(self, value)
