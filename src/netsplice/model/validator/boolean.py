# -*- coding: utf-8 -*-
# boolean.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.model.validator import validator


class boolean(validator):
    '''
    Booean Validator. Validates that a value is of boolean type.
    '''

    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        '''
        Ensure the given value is a boolean value either as int (0/1) or as
        bool.
        '''
        if not isinstance(value, (int, bool)):
            return False
        return True
