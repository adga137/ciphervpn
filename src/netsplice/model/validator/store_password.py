# -*- coding: utf-8 -*-
# store_password.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Validator for store password hints.
'''

from netsplice.util.model.validator import validator
from netsplice.config import backend as config
from netsplice.util import basestring
from netsplice.util import get_logger

logger = get_logger()

VALID_STORE_TYPES = [
    config.STORE_PASSWORD_NEVER,
    config.STORE_PASSWORD_KEYSTORE,
    config.STORE_PASSWORD_SESSION,
]


class store_password(validator):
    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        '''
        Validate value is a enumeration of supported types.
        '''
        if not isinstance(value, (basestring)):
            return False

        if value not in VALID_STORE_TYPES:
            logger.warn(
                'The store password type "%s" is not valid. Use one of %s'
                % (value, str(VALID_STORE_TYPES)))
            return False

        return True
