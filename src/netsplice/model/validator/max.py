# -*- coding: utf-8 -*-
# max.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.model.validator import validator
from netsplice.util import long


class max(validator):
    '''
    Maximum Validator. Validates that a value is not larger than the given
    maximum.
    '''

    def __init__(self, max_value):
        validator.__init__(self)
        self.max_value = max_value

    def is_valid(self, value):
        '''
        Ensure the given value is a numeric value that has at most the value
        of the defined max_value
        '''
        if not isinstance(value, (int, long)):
            return False
        if value > self.max_value:
            return False
        return True
