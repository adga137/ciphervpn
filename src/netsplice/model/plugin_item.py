# -*- coding: utf-8 -*-
# plugin_item.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Plugin Item.

Model for a plugin item used to store plugin specific data.
'''

from netsplice.util.errors import NotFoundError
from netsplice.util.model.field import field
from netsplice.util.model.marshalable_persistent import marshalable_persistent


class plugin_item(marshalable_persistent):
    def __init__(self, owner):
        marshalable_persistent.__init__(self, owner)

        self.name = field(
            required=True,
            validators=[])

        self.is_registered = False
        self.is_collection = False

    def apply_values(self):
        '''
        Apply Values.

        Apply any value to application variables.
        This function is called every time the plugin-item is persisted
        The implementation is optional.
        '''
        pass
