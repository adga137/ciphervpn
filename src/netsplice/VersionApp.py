# -*- coding: utf-8 -*-
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Part of the packaging infrastructure, exposes the version to be added
to the package names.
'''

import argparse

from netsplice import __version__ as VERSION

version_parts = VERSION.split('.')
build_parts = version_parts[2].split('+')
major_number = int(version_parts[0])
minor_number = int(version_parts[1])
release_number = 0
if len(build_parts) == 1:
    build_number = int(version_parts[2])
    release_number = 0
else:
    build_number = int(build_parts[0])
    release_number = int(build_parts[1])

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Get the Netsplice version in various formats.',
        epilog='Copyright 2016 Netsplice Project')

    parser.add_argument('-n', '--nsh', action='store_true',
                        help=('Produce NSIS installer integers'))
    parser.add_argument('-d', '--deb', action='store_true',
                        help=('Produce DPKG version numbers'))
    parser.add_argument('-f', '--fedora', action='store_true',
                        help=('Produce SPEC version numbers'))

    opts, unknown = parser.parse_known_args()
    if opts.nsh:
        print('!define VERSIONMAJOR %d' % (major_number,))
        print('!define VERSIONMINOR %d' % (minor_number,))
        print('!define VERSIONBUILD %d' % (build_number,))
        print('!define VERSIONRELEASE %d' % (release_number,))
    elif opts.deb:
        print('%d.%d.%d' % (
            major_number, minor_number, build_number))
    elif opts.fedora:
        print('%d.%d.%d-%d' % (
            major_number, minor_number, build_number, release_number))
    else:
        print(VERSION)
