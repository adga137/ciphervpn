# -*- coding: utf-8 -*-
# gui.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Variables defined here are local to the GUI component of the client.
'''


# Qt will have to give the python-process some time to process events. So every
# LOOP_TIMER ms qt will hand over to python (eg for backend communication)
LOOP_TIMER = 500  # in ms

# Publish log to backend timeout
PUBLISH_LOG_TIMER = 500

# Emit Queue Timer
# Time how often the GUI-Thread will look for messages emitted in the
# backend_dispatcher. Smaller is better, but more cpu intensive.
# Relative to 1000 which is a noticeable lagging GUI
# Only relevant for OSX
EMIT_QUEUE_TIMER = 50

# Frequency to check if the backend is responding in seconds. This check will
# stop when the backend has become available
LOOP_TIMER_AVAILABLE = 0.1

# Frequency in which the ioloop thread is made active. Should be at least the
# minimum  required frequency for any periodic requests.
# High cpu impact: 0.01: 20% 0.1: 4% but almost no user impact (when <1)
LOOP_TIMER_NOOP = 0.5

# Frequency the backend-events controller call will be rescheduled. As the
# backend-controller is long-polling, this is the time between a received
# event and the next request.
# This has a low impact on cpu but high on user experience
LOOP_TIMER_EVENTS = 0.01

# Frequency the gui-log messages are resent to the backend when the backend
# is not yet up.
LOOP_TIMER_LOGS_SET = 0.5

# Frequency the gui-events (gui emits) are processed
# The gui and the backend only exchange information using emit() and signals
# to avoid segfaults. This frequency allows qt to receive the events from the
# GUI and process them in this thread.
# This has a relatively high impact: 0.1: ~10% 0.2: ~8% 0.3: 4%
LOOP_TIMER_QT_EVENTS = 0.3

# Grace Period after the GUI requested shutdown until this thread is stopped.
# After this timeout the backend_thread will be destroyed as the ioloop stops.
SHUTDOWN_TIMEOUT = 1

# Close to Systray
# True: Main window close will not stop the application or connections
# False: Main window close will stop all connections
CLOSE_TO_SYSTRAY = True

# Preferences that are exposed as settings
# the following list declares config values (of this config) that are
# exported to plist/ini/conf files so the gui can load the values on start
# before the backend is available.
SETTING_PREFERENCES = [
    'CLOSE_TO_SYSTRAY',
    'SHOW_WELCOME_ON_START',
    'START_HIDDEN',
    'THEME',
]

# Core components for preferences
CORE_PREFERENCE_COMPONENTS = {
    'ui': 'GUI',
    'backend': 'Backend',
}

# Show welcome on start
SHOW_WELCOME_ON_START = True

# Start Drag Distance in pixel
# -1: QtGui.QApplication.startDragDistance() (should be 4 on most systems)
START_DRAG_DISTANCE = -1

# Drag Scroll Timer in milliseconds
# Interval that DRAG_SCROLL_DISTANCE is moved on the scrollbar during Drag and
# Drop on scrollareas (account list)
DRAG_SCROLL_TIMER = 30

# Drag Scroll Distance.
# Distance in pixel? that the scrollarea should advance when the user hovers
# the scroll trigger zone.
DRAG_SCROLL_DISTANCE = 5

# Drag Scroll Zone Size
# Percent (0-1.0) of the scroll area's size that should trigger scrolling in a
# direction.
DRAG_SCROLL_ZONE_SIZE = 0.1

# Start Hidden
# Do or do not show the mainwindow on startup
START_HIDDEN = False

# Statusbar options
# Show or Hide elements in the statusbar. When all elements are hidden,
# the statusbar will hide.
STATUSBAR_SHOW_VERSION = True
STATUSBAR_SHOW_PRODUCT_OWNER = True
STATUSBAR_SHOW_CONNECTIONS = False
STATUSBAR_SHOW_HEARTBEAT = True
# Product Owner in statusbar
STATUSBAR_PRODUCT_OWNER = '\xa9 IPredator.se'

# Systray options
SYSTRAY_ANIMATE_ICON = True
SYSTRAY_SHOW_NOTIFICATIONS = True

# Syntax highlighting
SYNTAX_KEYWORD_COLOR = '#008000'
SYNTAX_COMMENT_COLOR = '#888888'
SYNTAX_FILE_COLOR = '#3366bb'

# Default Shortcuts
# QKeySequence Strings for actions.
SHORTCUT_CONNECT_ALL = 'shift+ctrl+a'
SHORTCUT_CREATE_CONNECTION = 'ctrl+n'
SHORTCUT_COLLAPSE_TOGGLE = 'shift+return'
SHORTCUT_HELP = 'F1'
SHORTCUT_SELECTED_CONNECT = 'ctrl+c'
SHORTCUT_SELECTED_DISCONNECT = 'ctrl+d'
SHORTCUT_SELECTED_RECONNECT = 'ctrl+r'
SHORTCUT_SELECTED_CONNECT_ALL = 'shift+ctrl+c'
SHORTCUT_SELECTED_DISCONNECT_ALL = 'shift+ctrl+d'
SHORTCUT_SELECTED_RECONNECT_ALL = 'shift+ctrl+r'
SHORTCUT_SELECTED_RESET = 'shift+ctrl+z'
SHORTCUT_SELECTED_DELETE = 'F8'
SHORTCUT_SELECTED_EDIT = 'F4'
SHORTCUT_SELECTED_RENAME = 'F2'
SHORTCUT_SELECTED_GROUP = 'ctrl+g'
SHORTCUT_SELECTED_UNGROUP = 'shift+ctrl+g'
SHORTCUT_SELECTED_MOVE_LEFT = 'shift+left'
SHORTCUT_SELECTED_MOVE_RIGHT = 'shift+right'
SHORTCUT_SELECTED_MOVE_UP = 'shift+up'
SHORTCUT_SELECTED_MOVE_DOWN = 'shift+down'
SHORTCUT_SHOW_LOGS = 'ctrl+l'
SHORTCUT_SHOW_PREFERENCES = 'ctrl+,'
SHORTCUT_QUIT = 'ctrl+q'

# Show log on failure
# Show the log when a failure occurs during connect.
SHOW_LOG_ON_CONNECTION_FAILURE = True

# When backend requests a notify error event in the gui
SHOW_LOG_ON_BACKEND_ERROR = True

# When the backend crashed
SHOW_LOG_ON_BACKEND_GONE = True

# Theme
THEME = 'default'

# Themes available. Defines the available themes.
THEMES_AVAILABLE = [
    'default',
    'blue',
    'red'
]
'''
Config for Accounts
'''


ACCOUNT_CONNECTED = 0
ACCOUNT_CONNECTING = 1
ACCOUNT_DISCONNECTED = 2
ACCOUNT_FAILED = 3

DROP_INDICATOR_HEIGHT = 20
DROP_INDICATOR_WIDTH = 20
DROP_INDICATOR_COLOR_R = 255
DROP_INDICATOR_COLOR_G = 0
DROP_INDICATOR_COLOR_B = 0
DROP_INDICATOR_COLOR_A = 128

# color for labels that are highlighted
LABEL_HIGHLIGHT_STYLESHEET = 'background-color: rgb(255,255,0)'
# ms to highlight a label
LABEL_HIGHLIGHT_TIME = 300

# MIME Types for drag-and-drop
MIME_TYPE_GROUP = 'application/vnd.netsplice.group'
MIME_TYPE_ACCOUNT = 'application/vnd.netsplice.account'

# Maximum width for a widget (QT CPP define)
MAX_WIDGET_WIDTH = 16777215
# Maximum height for a item (account_list_item)
MAX_ITEM_HEIGHT = 80

# Delay for animated icons
CONNECTING_ANIMATION_DELAY = 500

# Fatal Text
# This rich-text is displayed with Qt::tr when the backend crashes
FATAL_TEXT = (
    '<h1>Backend is gone</h1>'
    '<p>Please restart the application and consider sending a bug-report'
    ' to the Netsplice team.</p>'
)

# Preferences Text
# This rich-text is displayed with Qt::tr when the preference dialog
# is displayed.
PREFERENCES_TEXT = {
    'ui': (
        '<p>GUI preferences can be changed without a restart.</p>'
    ),
    'backend': (
        '<p>Changes to the backend require a restart!</p>'
        '<p>Read <a href="{API_URL}">API</a> and model/request'
        ' for valid values.</p>'
    )
}

# Welcome Text
# This rich-text is displayed with Qt::tr when the application starts
WELCOME_TEXT = (
    '<h1>Welcome to Netsplice</h1>'
    '<p>Netsplice is a cross-platform multi-protocol desktop VPN client'
    ' developed by IPredator.se. The following core features are'
    ' available:</p>'
    '<ul>'
    '<li>No logs are written to the disk</li>'
    '<li>Multiple concurrently active VPN connections</li>'
    '<li>Cross-platform: Windows, macOS, Linux</li>'
    '<li>Privilege separated processes for runtime</li>'
    '</ul>'
    '<p>If you like Netsplice please consider supporting further development'
    ' with some BTC or by using our VPN service.</p>'
    '<p>Netsplice is written in Python using Logbook, OpenSSL, OpenVPN,'
    ' PySide (Qt) and Tornadoweb licensed under the GPL3.</p>'
    '<p>Press F1 to get a short help or read the'
    ' <a href="{documentation_url}">documentation</a>.'
)

STARTUP_WAIT_MESSAGE = (
    '<p><b>Please Wait</b> until the'
    ' certificates for secure communication between components are generated '
    ' and the backend is available.'
    '<p>When the progress runs out of time, please review the log, remove any'
    ' previous config files or wait for a later release.'
)
STARTUP_WAIT_MESSAGE_MANUAL_ELEVATE = (
    '<p>The operating system may ask to elevate the'
    ' NetsplicePrivilegedApp. Please grant permission for this process to'
    ' allow the change of network preferences.'
)

NO_CONNECTIONS_CONFIGURED = (
    '<p><b>Connection List</b></p>'
    '<p>This connection list displays all configurations.'
    '<p>The items in this list can be grouped and'
    ' ordered. A simple double-click is used to start the connection and the'
    ' color of the item reflects the current state.'
    '<p>You have not yet configured a connection.'
    '<p>Click here or drag and drop a config-file'
    ' to create a new connection.'
    '<p>Press F1 to get a short help.'
)

# Icon that is used for stored accounts with no plugin that handles the type.
RES_ICON_TYPE_RAW = (
    ':/images/types/unknown.png')

RES_ICON_ACTION_CONNECT = (
    ':/images/actions/connect.png')
RES_ICON_ACTION_CREATE = (
    ':/images/actions/create.png')
RES_ICON_ACTION_DISCONNECT = (
    ':/images/actions/disconnect.png')
RES_ICON_ACTION_DELETE = (
    ':/images/actions/delete.png')
RES_ICON_ACTION_RECONNECT = (
    ':/images/actions/reconnect.png')
RES_ICON_ACTION_SETTINGS = (
    ':/images/actions/show_settings.png')
RES_ICON_ACTION_LOG = (
    ':/images/actions/show_log.png')

RES_ICON_APPLICATION = (
    ':/images/netsplice-icon.png')

RES_ICON_LOADING = (
    ':/images/status/loading.gif')

RES_ICON_STATUS_CONNECTED = (
    ':/images/status/connected.png')
RES_ICON_STATUS_CONNECTED_REDIRECT_GATEWAY = (
    ':/images/status/connected_redirect_gateway.png')
RES_ICON_STATUS_CONNECTED_SOCKS = (
    ':/images/status/connected_socks.png')
RES_ICON_STATUS_DISCONNECTED = (
    ':/images/status/disconnected.png')
RES_ICON_STATUS_CONNECTING_0 = (
    ':/images/status/connecting_0.png')
RES_ICON_STATUS_CONNECTING_1 = (
    ':/images/status/connecting_1.png')
RES_ICON_STATUS_CONNECTING_2 = (
    ':/images/status/connecting_2.png')
RES_ICON_STATUS_FAILED = (
    ':/images/status/failed.png')
RES_ICON_STATE_FAILURE = (
    ':/images/state/failure.png')
RES_ICON_STATE_SUCCESS = (
    ':/images/state/success.png')
RES_ICON_STATE_WARNING = (
    ':/images/state/warning.png')

# Messages for account item
QUESTION_REALLY_DELETE = (
    'Really delete account {account_name}?\n'
    'All settings for this account will be removed.')

QUESTION_REALLY_DELETE_GROUP = (
    'Really delete group {group_name}?\n'
    'All accounts will be moved to the Global group.')

QUESTION_ACCOUNT_TYPE_CHANGED = (
    'Account Type changed.\n'
    'Initialize settings with defaults?')

QUESTION_ACCOUNT_CHANGED = (
    'Account configuration changed.\n'
    'Closing the dialog will lose all changes.\n'
    'Really Close?')

GLOBAL_GROUP_NAME = 'Global'
NEW_GROUP_NAME = 'New Group'
GROUP_STATE = {
    'collapsed': {
        'action_text': 'Expand',
        'indicator': u'▶'
    },
    'expanded': {
        'action_text': 'Collapse',
        'indicator': u'▼'
    }
}

# Messages for system tray
CONNECTING = dict(
    title=r'Connecting',
    body=r'The account %(account_name)s is connecting.'
)
CONNECTED = dict(
    title=r'Connected',
    body=(
        r'The account {account_name} is now connected.')
)
CONNECTED_IP = dict(
    title=r'Connected: {ip_local}',
    body=(
        r'The account {account_name} is now connected.<br/>'
        'IP: {ip_local}')
)
CONNECTED_SOCKS = dict(
    title=r'Connected: {socks}',
    body=(
        r'The account {account_name} is now connected.<br/>'
        'Socks Port: {socks}')
)
CONNECTION_FAILED = dict(
    title=r'Connection Failed',
    body=(
        r'The account %(account_name)s failed to connect.'
        ' See Log for details.')
)
DISCONNECTED = dict(
    title=r'Disconnected',
    body=r'The account %(account_name)s is disconnected.'
)

# Chain Introduction

CHAIN_INTRODUCTION_TEXT = (
    '<b>Chain</b>'
    '<p>A chain controls how accounts are related and interact.'
    ' When chains of accounts are defined, the accounts are aggregated to'
    ' connect in a parent-child order.'
    ' This allows better control for complex routing setups.'
)

CHAIN_SUMMARY_TEXT = (
    '<p><i>The current account {account_name} will connect after'
    ' {parent_account_name} has connected.'
)
CHAIN_PARENT_DESCRIPTION = (
    '<p>The parent defines the account in the chain that is connected'
    ' before the current account can connect. Changing the parent will move'
    ' the account to the parents connection group in the account list.'
)
CHAIN_CONNECT_MODE_DESCRIPTION = (
    '<p>The connect mode defines how <b>children</b>'
    ' of the current account {chain_children_list} will connect when'
    ' <b>Connect All</b> is used.'
    '<br/><b>Parallel</b> connects all children at the same time.'
    '<br/><b>Sequence</b> connects all children from top to bottom.'
    '<br/><b>None</b> connects no children.'
)
CHAIN_FAILURE_MODE_DESCRIPTION = (
    '<p>The failure mode defines how to handle a connect failure (e.g. remote'
    ' not available or authentication failure).'
    '<br/><b>Break</b> stops the chain processing at the account that fails.'
    '<br/><b>Disconnect</b> forces all parents to disconnect.'
    '<br/><b>Ignore</b> continues to connect the children.'
    '<br/><b>Reconnect</b> reconnects the parent and retry the current'
    ' account.'
)
CHAIN_SUMMARY_TEXT_SEQUENCE = (
    '<p><i>All children ({chain_children_list}) will connect'
    ' from top to bottom, waiting that their predecessor is connected before'
    ' connecting.'
)
CHAIN_SUMMARY_TEXT_PARALLEL = (
    '<p><i>All children ({chain_children_list}) will connect at the same time.'
)
CHAIN_SUMMARY_TEXT_NONE = (
    '<p><i>Multiple children have the <b>Default Route</b> flag set. A manual'
    ' connect is required for each child.'
)
CHAIN_SUMMARY_TEXT_BREAK = (
    '<p><i>When {account_name} fails to connect, the children'
    ' ({chain_children_list})'
    ' will not connect. The accounts that have been connected up to this point'
    ' stay connected.'
)
CHAIN_SUMMARY_TEXT_DISCONNECT = (
    '<p><i>When {account_name} fails to connect or disconnects, all accounts'
    ' in the chain will disconnect.'
)
CHAIN_SUMMARY_TEXT_IGNORE = (
    '<p><i>When {account_name} fails to connect other accounts in the chain'
    ' will continue to connect.'
)
CHAIN_SUMMARY_TEXT_RECONNECT = (
    '<p><i>When {account_name} fails to connect, {parent_account_name}'
    ' will reconnect and this account will restart the connect.'
)
CHAIN_FAILURE_MODE_NAMES = {
    'break': 'Break',
    'disconnect': 'Disconnect',
    'reconnect': 'Reconnect',
    'ignore': 'Ignore',
}
CHAIN_CONNECT_MODE_NAMES = {
    'sequence': 'Sequence',
    'parallel': 'Parallel',
    'none': 'None',
}
# QSS used in osx because the buttons look strange otherwise
# This QSS is added to all themes
OSX_OVERRIDE_QSS = '''
    #log_viewer QPushButton[checkable="true"] {
      padding-top: 4px;
      padding-bottom: 5px;
      padding-left: 10px;
      padding-right: 10px;
      min-height: 22px;
      min-width: 40px;
    }
    #log_viewer #action_close, #log_viewer #action_clear_hidden {
      min-height: 22px;
      min-width: 120px;
    }
    #log_viewer #action_save, #log_viewer #action_clear_log {
      min-height: 22px;
      min-width: 60px;
    }
    QPushButton, QToolButton {
      padding-top: 4px;
      padding-bottom: 5px;
      padding-left: 10px;
      padding-right: 10px;
    }
    QPushButton {
      padding-top: 4px;
      padding-bottom: 5px;
      padding-left: 10px;
      padding-right: 10px;
      min-height: 22px;
      min-width: 120px;
    }
    QComboBox {
      padding-top: 5px;
      padding-bottom: 4px;
      padding-left: 5px;
      padding-right: 5px;
      min-height: 22px;
      min-width: 20px;
    }
'''
