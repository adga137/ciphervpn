# -*- coding: utf-8 -*-
# backend.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Variables defined here are local to the backend component of the client.
'''

# Maximum number of bytes to be loaded as configuration
MAX_CONFIG_SIZE = 32768

# Time to wait when multiple accounts are connected in sequence.
MAX_SEQUENCE_WAIT_AFTER_CONNECT_TIME = 1

# Maximum time to wait for sub-applications to shutdown in seconds
MAX_SHUTDOWN_TIME = 2

# Period in seconds in which an active connection should query for its status
STATUS_UPDATE_PERIOD = 1

# Group names
ROOT_GROUP_NAME = 'root'
GLOBAL_GROUP_NAME = 'global'
UNSET_GROUP_NAME = 'unset'

# Ready Message for GUI
# This message will be written by the backend and the GUI will wait until
# stdout of the backend contains this message.
BACKEND_READY_MESSAGE = 'Ready to process events.'

BACKEND_STARTUP_MESSAGE = 'Startup Progress'

# Credential Storage options
# Identifiers
STORE_PASSWORD_NEVER = 'never'
STORE_PASSWORD_KEYSTORE = 'keystore'
STORE_PASSWORD_SESSION = 'session'
STORE_PASSWORD_DEFAULT = STORE_PASSWORD_NEVER

# Available credential storage types with label
STORE_PASSWORD_TYPES = [
    {'label': 'Never', 'name': STORE_PASSWORD_NEVER},
    {'label': 'OS Keystore', 'name': STORE_PASSWORD_KEYSTORE},
    {'label': 'Session', 'name': STORE_PASSWORD_SESSION}
]

# Group may not provide a 'connect all'.
CONNECT_MODE_NONE = 'none'

# All accounts are connected at the same time.
CONNECT_MODE_PARALLEL = 'parallel'

# accounts are connected one-by-one ordered by weight
CONNECT_MODE_SEQUENCE = 'sequence'

CONNECT_MODES = [
    CONNECT_MODE_NONE,
    CONNECT_MODE_PARALLEL,
    CONNECT_MODE_SEQUENCE,
]

# connecting stops when a connection fails, the connected accounts stay
FAILURE_MODE_BREAK = 'break'

# connecting stops, already connected disconnect
FAILURE_MODE_DISCONNECT = 'disconnect'

# connecting continues when a connection fails
FAILURE_MODE_IGNORE = 'ignore'

# owning account reconnects, then the failed reconnects
FAILURE_MODE_RECONNECT = 'reconnect'

FAILURE_MODES = [
    FAILURE_MODE_BREAK,
    FAILURE_MODE_DISCONNECT,
    FAILURE_MODE_IGNORE,
    FAILURE_MODE_RECONNECT,
]
