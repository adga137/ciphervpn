# -*- coding: utf-8 -*-
# ipc.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Values that configure the IPC defaults.
'''


CERTIFICATE_KEY_LENGTH = 2048

# 1d for development (to get issues from invalid certificates etc).
# in rl: 365 ?
CERTIFICATE_VALID_DAYS = 90

# Use https with client certificates. Default: True.
# Makes debugging the API with curl harder
# NEVER commit with False!
HTTPS = True

# Use X-Signature header check for all requests
# Makes debugging the API with curl harder
# NEVER commit with False!
CHECK_SIGNATURE = True

# Configure HTTPRequest to timeout after the the given amount of seconds
# This should be a higher value than model.HEARTBEAT_SECONDS (~1.5)
REQUEST_TIMEOUT = 90


# HMAC Signature secret length
SHARED_SECRET_LENGTH = 64

# Timeout that a sub-application will wait before it bails
MAX_WAIT_FOR_SECRET = 10

# Location (in the ~/.config/NS_APPNAME directory) where the
# shared secret is stored
SHARED_SECRET_LOCATION = 'certificates'

# Name of the shared secret file
SHARED_SECRET_NAME = 'shared_secret'

# Time in seconds that a service will wait when it receives the shutdown
# message, before it shuts down the ioloop. Can be small, should be enough to
# respond to the process that called the shutdown endpoint.
SHUTDOWN_WAIT_TIME = 0.1

# Time in seconds that the services are polled for their active state during
# startup.
STARTUP_WAIT_LOOP_TIME = 0.1

# Loops before the application stops waiting for child processes
# and considers itself broken.
STARTUP_MAX_WAIT_LOOPS = 100

# Display a rotating dash instead of a countdown.
STARTUP_WAIT_ROTATING_DASH = True

# Time in seconds that the log model-change is rescheduled in the eventloop.
LOOP_TIMER_LOG_MODEL_CHANGE = 0.01

# Time in seconds that the model-change is rescheduled in the eventloop.
LOOP_TIMER_MODULE_MODEL_CHANGE = 0.1

# Rest Endpoints for all servers
REST_ENDPOINT_MODULES = '/config/modules'
REST_ENDPOINT_NAMESPACE = '/config/namespace'
REST_ENDPOINT_STATUS = '/status'
REST_ENDPOINT_VERSION = '/config/version'
