# -*- coding: utf-8 -*-
# connection.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Connection state machine config.
'''
#
# States
#
# Created, the connection instance is created
CREATED = 'backend.connection.state.created'
#: Setting up, the plugin is initializing subprocesses and asks for credentials
SETTING_UP = 'backend.connection.state.setting_up'
#: Initialized and never connected
INITIALIZED = 'backend.connection.state.initialized'
#: Connecting, waiting for user credentials in backend
CONNECTING = 'backend.connection.state.connecting'
#: Connecting Process, waiting for subprocess to finish
CONNECTING_PROCESS = 'backend.connection.state.connecting_process'
#: Connected, subprocess signaled success
CONNECTED = 'backend.connection.state.connected'

#: Disconnecting, prepare disconnect in backend
DISCONNECTING = 'backend.connection.state.disconnecting'
#: Disconnecting Process, waiting for subprocess to shutdown
DISCONNECTING_PROCESS = 'backend.connection.state.disconnecting_process'
#: Disconnected, the subprocess has closed the connection
DISCONNECTED = 'backend.connection.state.disconnected'

#: Disconnecting Failure, when a failure occured prepare disconnect in
#: backend. Used to pass the fail-state, feature-same as DISCONNECTING
DISCONNECTING_FAILURE = 'backend.connection.state.disconnecting_failure'
#: Disconnecting Failure Process. Same as DISCONNECTING_PROCESS preserving
#: the fail-state
DISCONNECTING_FAILURE_PROCESS = (
    'backend.connection.state.disconnecting_failure_process'
)
#: Disconnected Failure, Same as DISCONNECTED with fail-state
DISCONNECTED_FAILURE = 'backend.connection.state.disconnected_failure'

#: Reconnecting, prepare reconnect in backend
RECONNECTING = 'backend.connection.state.reconnecting'
#: Reconnecting Process, wait for the subprocess to reconnect
RECONNECTING_PROCESS = 'backend.connection.state.reconnecting_process'
#: Reconnected, the subprocess has finished the reconnect.
RECONNECTED = 'backend.connection.state.reconnected'


#: Exception occured in code
ABORTED = 'backend.connection.state.aborted'

#: All successful connected states
UP = 'backend.connection.state.up'

#: All states where no traffic should flow
DOWN = 'backend.connection.state.down'

#: All states of a connection
STATES = [
    CREATED,
    SETTING_UP,
    INITIALIZED,
    CONNECTING,
    CONNECTING_PROCESS,
    CONNECTED,
    DISCONNECTING,
    DISCONNECTING_PROCESS,
    DISCONNECTED,
    DISCONNECTING_FAILURE,
    DISCONNECTING_FAILURE_PROCESS,
    DISCONNECTED_FAILURE,
    RECONNECTING,
    RECONNECTING_PROCESS,
    RECONNECTED,
    ABORTED
]

#: All states including transients that are not real state-machine states
TRANSIENT_STATES = [
    UP,
    DOWN
]
TRANSIENT_STATES.extend(STATES)

#: All states that consider the connection active
ACTIVE_STATES = [
    CONNECTED,
    RECONNECTED,
    DISCONNECTED,
    DISCONNECTED_FAILURE
]

#: All states that consider the connection connected
CONNECTED_STATES = [
    CONNECTED,
    RECONNECTED,
]

#: All states that consider the connection disconnecting
DISCONNECTING_STATES = [
    DISCONNECTING,
    DISCONNECTING_PROCESS,
    DISCONNECTING_FAILURE,
    DISCONNECTING_FAILURE_PROCESS,
]
#: All states that consider the connection disconnecting in process
DISCONNECTING_PROCESS_STATES = [
    DISCONNECTING_PROCESS,
    DISCONNECTING_FAILURE_PROCESS,
]
#: All states that consider the connection disconnected
DISCONNECTED_STATES = [
    ABORTED,
    CREATED,
    DISCONNECTED,
    DISCONNECTED_FAILURE,
    INITIALIZED,
    SETTING_UP,
]
#: All states that make the connection disposable on cleanup
DISPOSABLE_STATES = [
    ABORTED,
    DISCONNECTED,
    DISCONNECTED_FAILURE,
]

#
# Triggers
#
#: Setup, used from connection broker
#: Prepare the connection subprocesses in the backend
SETUP = 'backend.connection.trigger.setup'
#: Setup finish, subprocesses are ready to connect
SETUP_FINISH = 'backend.connection.trigger.setup_finish'
#: Setup canceled, credential dialog canceled by user
SETUP_CANCEL = 'backend.connection.trigger.setup_cancel'

#: Connect, used from connection_broker.
#: Prepare the connection in the backend.
CONNECT = 'backend.connection.trigger.connect'
#: Connect Process, triggered from plugin before the subprocess takes
#: responsibility.
CONNECT_PROCESS = 'backend.connection.trigger.connect_process'
#: Connect Finish, triggered from plugin when the connection is up.
CONNECT_FINISH = 'backend.connection.trigger.connect_finish'

#: Disconnect, triggered from connection_broker.
DISCONNECT = 'backend.connection.trigger.disconnect'
#: Disconnect Plugin with a non-recovering info for the broker
DISCONNECT_ABORT = 'backend.connection.trigger.disconnect_abort'
#: Disconnect Plugin, fast (sigkill) that then triggers DISCONNECT
DISCONNECT_FORCE = 'backend.connection.trigger.disconnect_force'
DISCONNECT_PROCESS = 'backend.connection.trigger.disconnect_process'
DISCONNECT_FINISH = 'backend.connection.trigger.disconnect_finish'
DISCONNECT_FAILURE = 'backend.connection.trigger.disconnect_failure'
DISCONNECT_FAILURE_PROCESS = (
    'backend.connection.trigger.disconnect_failure_process'
)
DISCONNECT_FAILURE_FINISH = (
    'backend.connection.trigger.disconnect_failure_finish'
)

RECONNECT = 'backend.connection.trigger.reconnect'
RECONNECT_PROCESS = 'backend.connection.trigger.reconnect_process'
RECONNECT_FINISH = 'backend.connection.trigger.reconnect_finish'

TRIGGERS = [
    SETUP,
    SETUP_FINISH,
    SETUP_CANCEL,
    CONNECT,
    CONNECT_PROCESS,
    CONNECT_FINISH,
    DISCONNECT,
    DISCONNECT_FORCE,
    DISCONNECT_PROCESS,
    DISCONNECT_FINISH,
    DISCONNECT_FAILURE,
    DISCONNECT_FAILURE_PROCESS,
    DISCONNECT_FAILURE_FINISH,
    RECONNECT,
    RECONNECT_PROCESS,
    RECONNECT_FINISH,
]
