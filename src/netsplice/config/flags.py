# -*- coding: utf-8 -*-
# flags.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Flags defined here are shared flags between the modules.
The flags can be changed by the commandline arguments of the application.

Only use flags when no safer alternative is available and the setting needs
to span all modules / processes.
'''


# Store the needed loglevel globally since the logger handlers goes through
# threads and processes.
# This flag is controlled using the --debug option switch
DEBUG = False

# Verbosity for algorithms. Used for debugging
VERBOSE = 0
