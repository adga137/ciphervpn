# -*- coding: utf-8 -*-
# gui.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Configure active plugins
'''

ACTIVE_PLUGINS = [
    'netsplice.plugins.config_quality_check',
    'netsplice.plugins.debug_check',
    'netsplice.plugins.openvpn',
    'netsplice.plugins.ping',
    'netsplice.plugins.process_launcher',
    'netsplice.plugins.process_manager',
    'netsplice.plugins.process_sniper',
    'netsplice.plugins.profile_openvpn_ipredator_ipv4',
    'netsplice.plugins.profile_openvpn_ipredator_ipv6',
    'netsplice.plugins.profile_openvpn_ipredator_nat',
    'netsplice.plugins.profile_openvpn_riseup',
    'netsplice.plugins.profile_ssh_port_forward',
    'netsplice.plugins.profile_ssh_port_forward_reverse',
    'netsplice.plugins.profile_ssh_socks_proxy',
    'netsplice.plugins.profile_ssh_vpn',
    'netsplice.plugins.profile_tor_exit_ru_only',
    'netsplice.plugins.profile_tor_exit_us_only',
    'netsplice.plugins.profile_tor_friend_entry_and_exit',
    'netsplice.plugins.tor',
    'netsplice.plugins.ssh',
    'netsplice.plugins.update',
]
