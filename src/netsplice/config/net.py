# -*- coding: utf-8 -*-
# net.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Variables defined here are local to the backend component of the client.
'''


# REST paths
RP_CONF = '/configuration'
RP_NEWS = '/news'
RP_PROV = '/provider'
RP_SHUT = '/shutdown'
RP_SUPPORT = '/support'
RP_UPDATE = '/update'

# Module specific
RP_ACT_RESTART = '/action/restart'
RP_ACT_RESTART_CONN = '/action/restart/{conn-handle}'
RP_ACT_SHUT = '/action/shutdown'
RP_ACT_START = '/action/start'
RP_ACT_START_CONN = '/action/start/{conn-handle}'
RP_ACT_STATUS = '/action/info'
RP_ACT_STATUS_CONN = '/action/info/{conn-handle}'
RP_ACT_STOP = '/action/stop/{conn-handle}'
RP_ACT_STOP_CONN = '/action/stop/{conn-handle}'
RP_ACT_UPDATE = '/action/update'
RP_MOD_NEWS = '/module/news'
RP_MOD_PROV = '/module/provider'
RP_MOD_SUPPORT = '/module/support'
