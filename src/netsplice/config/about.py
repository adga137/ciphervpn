# -*- coding: utf-8 -*-
# about.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Variables for about / help output.
'''

# This texts will be translated using Qt::tr. The texts should be
# minimal.

ABOUT_TEXT = (
    r'<p>Netsplice is a cross-platform multi-protocol desktop client for VPN'
    ' services.'
    ' It is written in Python using Logbook, OpenSSL, OpenVPN, PySide (Qt)'
    ' and Tornadoweb licensed under the GPL3.</p>'
    '<p>'
    'Copyright: <a href="%(COPYRIGHT_URL)s">'
    '%(COPYRIGHT_URL)s</a> 2017</p>'
    '<p>License: <a href="%(LICENSE_URL)s">'
    '%(LICENSE_NAME)s</a></p>'
    '<p>Donate: <a href="%(SUPPORT_BITCOIN_URL)s">'
    '%(SUPPORT_BITCOIN)s</a></p>'
    '<p>Version: <b>%(VERSION)s</b></p>'
)

HELP_TEXT = (
    r'<p>Netsplice is a cross-platform multi-protocol desktop client for VPN'
    ' services.'
    ' It is written in Python using Logbook, OpenSSL, OpenVPN, PySide (Qt)'
    ' and Tornadoweb licensed under the GPL3.</p>'
    '<p>Version: <b>%(VERSION)s</b></p>'
    '<p>'
    'To provide feedback or get support contact us via:'
    '</p>'
    '<ul>'
    '<li>Mail: %(SUPPORT_EMAIL)s</li>'
    '<li><a href="%(IRC_CHAT_URL)s">'
    'IRC chat</a></li>'
    '</ul>'
    '<p>A more comprehensive documentation is available as an '
    ' <a href="%(DOC_URL)s">'
    'online manual</a>.</li>'
    '</ul>'
)

# Helptext copied from the documentation, extended to important details.
HELP_TEXT_LONG = (
    r'<p><b>Features</b>'
    '<p>Netsplice is an open source cross-platform multi-protocol VPN client'
    ' developed by IPredator.'
    '<p>The available features are:'
    '<ul>'
    '<li><b>OpenVPN</b>: Provides a GUI frontend to native OpenVPN'
    ' implementation.'
    '<li><b>Cross platform</b>: Binary packages are available for Windows,'
    ' macOS, Debian, Ubuntu, Fedora, and Arch.'
    '<li><b>Cross client</b>: Netsplice is not limited to OpenVPN. You can'
    ' also setup connection profiles for SSH and Tor.'
    '<li><b>Multi version binary</b>: OpenVPN is shipped with multiple'
    ' versions.'
    '<li><b>Alternative SSL versions</b>: You can chose between OpenSSL and'
    ' LibreSSL as SSL library.'
    '<li><b>Concurrent connections</b>: Allows being connected to multiple'
    ' VPNs at the same time.'
    '<li><b>Chained connections</b>: Specify dependencies between connections'
    ' - once your primary connection is up the secondary connections are '
    'automatically established.'
    '<li><b>No logs</b>: Netsplice does not store logs on your computer, if'
    ' you want to keep the log you have to export it.'
    '<li><b>Secure Update</b>: A privacy aware update mechanism reminds the'
    ' user when new versions are available. TLS fingerprint verification of'
    ' the update server and automatic signature verification of installation'
    ' packages ensure a secure update path.'
    '</ul>'

    '<p><br><b>Accounts</b>'
    '<p>Netsplice supports managing one or multiple accounts and allows to'
    ' launch them in a predefined order. Accounts are displayed in the'
    ' <i>Connection Status View</i> of the main window and the system tray'
    ' popup menu.'

    '<p><br><b>Connection Status</b>'
    '<p>When an account is connecting, Netsplice creates an internal'
    ' connection-object that is used to monitor the progress and state.'
    ' Important events are highlighted using colors. Connection properties are'
    ' shown as the following icons.'
    '<br/>'
    '<br/>&nbsp;<img src=":images/help/chain_info.png"/> Chained Account'
    '<br/>&nbsp;<img src=":images/help/chain_info_parallel.png"/> Parallel'
    ' connect of chained accounts'
    '<br/>&nbsp;<img src=":images/help/chain_info_sequence.png"/> Sequential'
    ' connect of chained accounts'
    '<br/>&nbsp;<img src=":images/help/chain_info_default_route.png"/>'
    ' Provides default route'

    '<p><br><b>Systray</b>'
    '<p>The system tray is used to inform the user that Netsplice is'
    ' running and which state it has. A context-menu is provided,'
    ' that allows to exit the application and access configured'
    ' accounts. A balloon message is displayed to notify the user about'
    ' relevant events.'

    '<p><br><b>Log Viewer</b>'
    '<p>Connecting to VPN services generates log messages'
    ' that should be reviewed in order to ensure correct function. The logs'
    ' are colored and can be filtered by severity. Log messages are collected'
    ' during the runtime of Netsplice to provide the user as well as the VPN'
    ' provider wint essential information about what is going on.'
    '<p>Netsplice does not store logs on your computer, if you want to keep the'
    ' log you have to export it.'

    '<p><br><b>Preferences</b>'
    '<p>Netsplice and plugin preferences can be configured in a list editor.'
    ' The preferences are grouped by GUI, backend and plugins.'
    ' GUI and plugin settings can be changed at runtime and have an immediate'
    ' effect. Backend preferences require a restart of Netsplice.'

    '<p><br><b>Plugins</b>'
    '<p>To extend the core functionality additional plugins can be activated.'
    '<!-- REGISTERED PLUGIN HELP -->'
)

COPYRIGHT_URL = 'https://ipredator.se'
PRODUCT_URL = 'https://ipredator.se/netsplice'
LICENSE_URL = 'https://ipredator.se/netsplice/documentation/gpl.html'
LICENSE_NAME = 'GPLv3'
SUPPORT_BITCOIN = '1EDkjbzrnsvVoZzdG22c5iaj3RWXPS8ksh'
SUPPORT_BITCOIN_URL = 'bitcoin:%(SUPPORT_BITCOIN)s?label=Netsplice'

SUPPORT_EMAIL = 'support@ipredator.se'
IRC_CHAT_URL = 'https://irc.ipredator.se'
DOC_URL = 'https://ipredator.se/netsplice/documentation'
FAQ_URL = 'https://netsplice.net/faq'
API_URL = 'https://netsplice.net/api'
