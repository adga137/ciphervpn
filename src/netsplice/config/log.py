# -*- coding: utf-8 -*-
# log.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# To configure the log-level modify the flags.DEBUG / flags.VERBOSE flag

# Format for log entries when displayed on a stderr or stdout stream
LOG_FORMAT = (
    u'[{record.time:%Y-%m-%d %H:%M:%S.%f}] '
    u'{record.extra[origin]} '
    u'{record.level_name: <8} - L#{record.lineno: <4} : '
    u'{record.module}:{record.func_name} - {record.message}')

# Format for log entries when passed to backend
LOG_MODEL_FORMAT = (
    u'{record.extra[origin]} '
    u'{record.level_name: <8} - L#{record.lineno: <4} : '
    u'{record.module}:{record.func_name} - {record.message}')

# Format for log entries when formatted to plaintext
LOG_TEXT_LINE_FORMAT = '{date:>30s} {origin:>12s} {message}\n'

# Format for log entries that have a error level
LOG_TEXT_ERROR_FORMAT = '\n\nError:\n{formated_line}\n\n'

# Format for log entries that have a critical level
LOG_TEXT_CRITICAL_FORMAT = '\n\nCritical Error:\n{formated_line}\n\n'

# Maximum number of unprocessed log entries. This is the buffer each
# application reserves.
MAX_UNPROCESSED_LOG_ENTRIES = 64

# Configure a Log-Handler that writes the application log to
# ~/.config/NS_NAME/logs. Should be disabled as the backend will receive log
# entries and collect and filter them for the gui
# When ~/.config/NS_NAME/logs/FORCE_LOG_FILES exists, logfiles will be written
# This is useful to debug PyInstaller executables.
WRITE_LOG_FILES = False

# location in ~/.config/NS_NAME where logs are stored
WRITE_LOG_FILES_LOCATION = 'logs'

# Configure a Log-Handler that writes the application log to the stderr.
# Useful during development on unix.
WRITE_STDERR = True

# Log Level for stderr logger
STDERR_LOG_LEVEL = 'DEBUG'

# Name for loggers that will display log entries on stderr. Be aware that
# Gui loads Backend and then receives all output (including the stderr from
# the logger) and outputs it again
STDERR_LOGGER_NAMES = ['Gui']

# Colors for the different levels in the logviewer.
LEVEL_COLORS = {
    'DEBUG': '#eaeaea',
    'INFO': '#ffffff',
    'WARNING': '#eebb00',
    'ERROR': '#ff7373',
    'CRITICAL': '#FF5353'
}

# Logviewer columns. Each column needs to be a dictionary with 'field' and
# 'label'. The field has to be a value field in the model.log_item structure.
# The label will be translated using tr().
# Default is [
#     {'field': 'date', 'label': 'Date'},
#     {'field': 'origin', 'label': 'Origin'},
#     {'field': 'message', 'label': 'Message'}
# ]
LOGVIEWER_COLUMNS = [
    {'field': 'date', 'label': 'Date'},
    {'field': 'origin', 'label': 'Origin'},
    {'field': 'message', 'label': 'Message'}
]

# The QT view will get very unresponsive with too many items. This limits
# the amount of memory consumed by the logview when it is open over an extended
# period.
# 500 is responsive on 1.2GhZ, 10000 lags up to 3 seconds.
MAX_LOGVIEWER_ITEMS = 5000

# Number of lines that a logentry has. This value is used to
# precalculate the line height for all items in the logviewer list for
# performance reasons.
TEXT_LINES_PER_ITEM = 3

# Timer that scrolls to the end of the view when new items are added and
# when the user has scrolled to the end. Value in milliseconds.
SCROLL_TO_END_TIMER = 100

# Update timer that triggers a redraw for the visible timeago dates
# Value in milliseconds
UPDATE_TIMER = 3000

# Date when displayed in logviewer.
# timeago: 22 seconds ago
# raw: 2016-01-31T12:34:56.12345Z (beware: UTC)
# time: local time
LOGVIEWER_DATE = 'timeago'

# Report Text Format
# Used for clipboard export and Save as
REPORT_WRAP_COLUMN = 72
TEXT_FORMAT_REPORT = '''Report
======

Software: Netsplice
Version: {version}
OS: {os}
Date: {date}

Before posting this on the internet, please check for sensitive data
like usernames, ip-addresses, path-names etc.

Log Items
=========
{log_text}
'''

# Inserted separator when wrapping long lines
WRAP_LINE_SEPARATOR = ' \\\n    '

# Number of seconds a newline is injected to the exported log text
REPORT_BLOCK_TIME = 2

# Line format for no context
LINE_FORMAT_NOCONTEXT = '{message}\n'
