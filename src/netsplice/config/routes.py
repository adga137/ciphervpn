# -*- coding: utf-8 -*-
# routes.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Routes for the applications. Enpoints is a map of namespaces that contain a
list of tuples with an URI description and a class name. The class_name is
expanded to namespace[.module].class_name.class_name for tornado.
'''


# Mandatory
endpoints = {
    # Common structure set in stone to simplify the 'randomness' of module
    # namespace conventions. This helps later when defining required API
    # subsets for further modules and name space validations.
    'netsplice.util.ipc': [
        (r'/config', 'configuration'),
        (r'/config/namespace', 'configuration_namespace'),
        (r'/config/modules', 'configuration_modules'),
        (r'/config/version', 'configuration_version'),
        (r'/status', 'status')
    ],
    # Backend structure
    'netsplice.backend': [
    ],

    'netsplice.network': [
        (r'/action/shutdown', 'shutdown_action'),
        (r'/module/configuration', 'module'),
        (r'/module/news', 'module'),
        (r'/module/provider', 'module'),
        (r'/module/support', 'module'),
    ],

    'netsplice.privileged': [
        (r'/action/shutdown', 'shutdown_action'),
        (r'/module/systeminfo', 'module'),
    ],

    'netsplice.unprivileged': [
        (r'/action/shutdown', 'shutdown_action'),
    ]
}
