#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
App that brokers remote network (http/s) access.
'''

import atexit
import multiprocessing
import sys

import netsplice.network.download as download_module

from netsplice import __version__ as VERSION
from netsplice.util import get_logger, take_logs

from netsplice.config import flags
from netsplice.config import constants
from netsplice.model.net import net as net_model
from netsplice.network.backend_dispatcher import backend_dispatcher
from netsplice.network.event_loop import event_loop
from netsplice.plugins import get_plugins
from netsplice.util import commandline
from netsplice.util.ipc.errors import (
    ServerStartFailedError,
    NoSharedSecretError
)
from netsplice.util.ipc.route import get_route
from netsplice.util.ipc.server import server
from netsplice.util.ipc.shared_secret import shared_secret

logger = get_logger()


@atexit.register
def stop_net():
    '''
    Handle shutdown.

    Print unpublished log entries.
    '''
    for log_item in take_logs():
        message = ''
        level = 'INFO'
        if isinstance(log_item, (dict,)):
            message = log_item['message']
            level = log_item['level']
        else:
            message = log_item.message.get()
            level = log_item.level.get()
        if level in ['ERROR', 'CRITICAL']:
            sys.stderr.write(message + '\n')
        else:
            sys.stdout.write(message + '\n')


def run_net():
    '''
    Run the NET for the application.

    :param options: A dict of options parsed from the command line.
    :type options: dict
    :param flags_dict: A dict containing the flag values set on app start.
    :type flags_dict: dict
    '''
    options = commandline.get_options()

    flags.DEBUG = options.debug
    logger.info('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    logger.info('Netsplice version %s' % VERSION)
    logger.info('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

    backend_host = constants.LOCALHOST
    backend_port = constants.PORT_BACKEND

    if options.port_owner is not None:
        backend_port = options.port_owner

    if options.host_owner is not None:
        backend_host = options.host_owner

    if options.host is None:
        options.host = constants.LOCALHOST

    if options.port is None:
        options.port = constants.PORT_NET

    # a shared secret has to be generated from the backend
    # before the network process may start
    try:
        shared_secret.load_from_file()
    except NoSharedSecretError:
        logger.error('Shared secret from backend could not be loaded')
        sys.exit(1)

    app = server(options.host, options.port, 'network')
    app.set_owner(backend_dispatcher(backend_host, backend_port))
    app.set_model(net_model())
    app.set_event_loop(event_loop())
    app.add_module(download_module)
    for plugin in get_plugins():
        app.add_plugin(plugin)
    app.register_plugin_components('util')
    app.register_plugin_components('network')

    try:
        app.start(get_route('netsplice.network'))
        sys.exit(app.exec_())
    except ServerStartFailedError:
        logger.critical('Application server failed to start')
        sys.exit(1)


if __name__ == '__main__':
    multiprocessing.freeze_support()
    run_net()
