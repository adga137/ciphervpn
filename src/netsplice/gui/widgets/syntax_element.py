# -*- coding: utf-8 -*-
# syntax_element.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Syntax Element for Syntax Highlighter
'''

from PySide import QtCore


class syntax_element(object):
    def __init__(self, syntax_class, start_pattern, end_pattern):
        self.syntax_class = syntax_class
        self.rx_start = QtCore.QRegExp(start_pattern)
        if end_pattern is None:
            self.rx_end = None
            self.is_block = False
        else:
            self.rx_end = QtCore.QRegExp(end_pattern)
            self.is_block = True
