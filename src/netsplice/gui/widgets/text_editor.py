# -*- coding: utf-8 -*-
# texteditor.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Account editor.
'''

from PySide import QtGui, QtCore
from netsplice.gui.widgets.syntax_highlighter import syntax_highlighter


class text_editor(QtGui.QTextEdit):
    '''
    Custom editor with syntax highlighter and keyword tooltip.
    '''

    def __init__(self, parent=None):
        QtGui.QTextEdit.__init__(self, parent)
        self.syntax_highlighter = syntax_highlighter(self)
        self.doc = {}

    def setup_keywords(self, keyword_map):
        '''
        Map the keywords into doc and syntax highlighter.
        '''
        for keyword, description in keyword_map.items():
            self.doc[keyword] = description
            self.syntax_highlighter.add_keyword(
                'default', 'keyword', r'\b' + keyword + r'\b')

    def setup_blocks(self, block_keywords):
        '''
        Map the keywords to match as blocks in the syntax highlighter.
        '''
        for block in block_keywords:
            self.syntax_highlighter.add_block(
                'default', 'file',
                r'<' + block + r'>*', r'</' + block + r'>')

    def event(self, event):
        '''
        Widget event handler to show documentation tooltip.
        '''
        if event.type() == QtCore.QEvent.ToolTip:
            event_position = event.pos()
            event_cursor = self.cursorForPosition(event_position)
            event_cursor.select(QtGui.QTextCursor.WordUnderCursor)
            word_under_cursor = event_cursor.selection().toPlainText()
            if word_under_cursor in self.doc:
                QtGui.QToolTip.showText(
                    event.globalPos(), self.doc[word_under_cursor])
                return True
        return QtGui.QTextEdit.event(self, event)
