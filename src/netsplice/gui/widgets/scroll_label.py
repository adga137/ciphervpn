# -*- coding: utf-8 -*-
# scroll_label.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Label that is wrapped in a QScrollArea.
'''

from PySide.QtGui import (
    QWidget, QScrollArea, QFrame, QVBoxLayout, QLayout, QLabel
)
from PySide.QtCore import Qt, Property

from netsplice.util import basestring


class scroll_label(QWidget):
    '''
    Label wrapped in a scroll area.

    Widget that wraps a QLabel into a QScrollArea with the required layouts
    to display longer messages (i.e. stackdumps in check account).
    '''

    def __init__(self, parent=None):
        '''
        Initialize Widget.

        Initialize Widget with layout and scroll area.
        '''
        QWidget.__init__(self, parent)

        self.scroll_area = QScrollArea(self)
        self.scroll_area.setFrameShape(QFrame.NoFrame)
        self.scroll_area.setVerticalScrollBarPolicy(
            Qt.ScrollBarAsNeeded)
        self.scroll_area.setWidgetResizable(True)

        self.root = QWidget(self)
        self.root_ui = QVBoxLayout(self)

        self.ui = QVBoxLayout(self.root)
        self.ui.setContentsMargins(0, 0, 0, 0)
        self.ui.setSizeConstraint(QLayout.SetMinimumSize)
        self.root_ui.setSizeConstraint(QLayout.SetMinimumSize)

        self.root_ui.setContentsMargins(0, 0, 0, 0)
        self.label = QLabel(self)
        self.label.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        self.ui.addWidget(self.label)
        self.root_ui.addWidget(self.scroll_area)

        self.scroll_area.setWidget(self.root)

    def getText(self):
        '''
        Get Label Text.

        Returns the text of the Label.
        '''
        return self.label.getText()

    def setAlignment(self, align_flags):
        '''
        Set alignment.

        Pass alignment to label.

        Arguments:
            align_flags (int): or'd flags (Qt::AlignLeft)
        '''
        self.label.setAlignment(align_flags)

    def setTextInteractionFlags(self, flags):
        '''
        Set text interaction flags.

        Pass text interactionflags to label.

        Arguments:
            flags (int): or'd flags
        '''
        self.label.setTextInteractionFlags(flags)

    def setScrollbarAlwaysOn(self):
        '''
        Set scrollbar always on.

        Ensure that the scrollbar is always displayed for the label.
        '''
        self.scroll_area.setVerticalScrollBarPolicy(
            Qt.ScrollBarAlwaysOn)

    def setText(self, text):
        '''
        Set label text.

        Set text in the label widget.
        '''
        self.label.setText(text)

    def setWordWrap(self, new_state):
        '''
        Set word wrap.

        Set that the text in the label wraps.
        '''
        self.label.setWordWrap(new_state)

    def setOpenExternalLinks(self, new_state):
        '''
        Set open external links.

        Set to open external links displayed in the label.
        '''
        self.label.setOpenExternalLinks(new_state)

    text = Property(basestring, setText, getText)
