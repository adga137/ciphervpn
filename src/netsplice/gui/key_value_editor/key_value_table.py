# -*- coding: utf-8 -*-
# keyvalue_editor.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Key Value Table.

Key Value Table capable of displaying items for key_value_editor.
'''

from PySide import QtGui, QtCore
from netsplice.model.named_value_list import named_value_list
from .key_value_table_delegate import key_value_table_delegate
from netsplice.util import get_logger

logger = get_logger()

COLOR_REMOVED = '#FF0000'
ITEM_ROLE_INDEX = QtCore.Qt.UserRole
ITEM_ROLE_DEFAULT = QtCore.Qt.UserRole + 1
ITEM_ROLE_REMOVED = QtCore.Qt.UserRole + 2
ITEM_ROLE_INSERTED = QtCore.Qt.UserRole + 3

# Must not contain spaces, otherwise parser will split it.
KEY_REMOVED_PREFIX = '##UI-REMOVED:'


class key_value_table(QtGui.QTableWidget):
    '''
    Key Value Table.

    Custom Editor with keywords as column 0 and value as column 1. Additional
    Values may be set using the values property of the key_value_item.
    Items may have a key and value documentation that is exposed as tooltip
    and identified by the items key.
    '''

    values_changed = QtCore.Signal()
    value_checked = QtCore.Signal()
    edit_row = QtCore.Signal()
    row_selected = QtCore.Signal(int)

    def __init__(self, parent=None):
        '''
        Initialize Widget.

        Initialize widget defaults and members.
        '''
        QtGui.QTableWidget.__init__(self, parent)

        self._custom_row_edit = None
        self._header_labels = [self.tr('Option'), self.tr('Value')]
        self._in_edit_mode = False
        self._all_items_inserted = False
        self._font_unchanged = QtGui.QFont()
        self._font_changed = QtGui.QFont()
        self._font_inserted = QtGui.QFont()
        self._font_changed.setBold(True)
        self._font_inserted.setItalic(True)
        self._brush_removed = QtGui.QBrush(QtGui.QColor(COLOR_REMOVED))
        tmp_item = QtGui.QTableWidgetItem('')
        self._brush_not_removed = tmp_item.foreground()
        self._last_selected_row = -1
        self._value_column = 1
        self._state_column = None
        self._state_map = dict()
        self._disabled_documentation_columns = list()
        self._disabled_editor_columns = list()
        self.key_documentation = dict()
        self.value_documentation = dict()
        self.setItemDelegate(key_value_table_delegate(self))

        self.setColumnCount(2)
        self.horizontalHeader().setResizeMode(
            QtGui.QHeaderView.Interactive)
        self.horizontalHeader().setStretchLastSection(True)
        self.verticalHeader().hide()
        self.setCornerButtonEnabled(False)
        self.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
        self.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.setAlternatingRowColors(True)
        self.itemDoubleClicked.connect(self._editor_started)
        self.itemClicked.connect(self._editor_closed)
        self.itemChanged.connect(self._commit_change)
        self.itemSelectionChanged.connect(self._selection_changed)
        self.edit_row.connect(self._edit_row)

    def _commit_change(self, item):
        '''
        Commit Change.

        Emit signal with new values if in edit_mode. Ends edit_mode.
        '''
        if self._is_checkable(item):
            self.setCurrentItem(
                item,
                QtGui.QItemSelectionModel.ClearAndSelect |
                QtGui.QItemSelectionModel.Rows)
            self.value_checked.emit()
            return
        if not self._in_edit_mode:
            return
        self._editor_closed(item)
        item_text = item.text()
        item_default = item.data(ITEM_ROLE_DEFAULT)
        item_inserted = item.data(ITEM_ROLE_INSERTED)
        if item_inserted:
            self._set_row_inserted(item.row())
        else:
            if item_default is not None:
                if isinstance(item_default, (list,)):
                    changed = True
                    for default_item in item_default:
                        if default_item.value.get() == item_text:
                            changed = False
                    if changed:
                        self._set_row_changed(item.row())
                    else:
                        self._set_row_unchanged(item.row())
                elif item_text != item_default.value.get():
                    self._set_row_changed(item.row())
                else:
                    self._set_row_unchanged(item.row())
            else:
                self._set_row_unchanged(item.row())
        self._update_tooltip(item)
        self.values_changed.emit()

    def _edit_row(self):
        '''
        Edit Row.

        Signal handler that launches a row editor if one is registered.
        '''
        if self._custom_row_edit is None:
            return
        self._custom_row_edit()

    def _editor_closed(self, item):
        '''
        Editor Closed.

        End Edit mode.
        '''
        self._in_edit_mode = False

    def _editor_started(self, item):
        '''
        Editor Started.

        Start Edit mode.
        '''
        self._in_edit_mode = True

    def _get_restore_value(self, column, default_value):
        '''
        Get Restore Value.

        Returns the default value for the given column. When there are
        multiple possible default value, return the first value that no other
        key contains.
        '''
        if isinstance(default_value, (list,)):
            possible_values = []
            for default_item in default_value:
                possible_values.append(default_item.value.get())
            current_key_item = self.item(self.currentItem().row(), 0)
            for default_item in default_value:
                # check all items with key
                for row in range(0, self.rowCount()):
                    key_item = self.item(row, 0)
                    value_item = self.item(row, column)
                    if key_item.text() != current_key_item.text():
                        continue
                    if value_item.text() == default_item.value.get():
                        if value_item.text() in possible_values:
                            possible_values.remove(value_item.text())
            if len(possible_values):
                value_data = possible_values[0]
            else:
                value_data = default_value[0].value.get()
        else:
            value_data = default_value.value.get()
        return value_data

    def _is_checkable(self, item):
        '''
        Test Checkable.

        Test if the given item is checkable.
        '''
        flags = item.flags()
        return flags & QtCore.Qt.ItemIsUserCheckable

    def _selection_changed(self):
        '''
        Selection Changed.

        Called, whenever the selection changes.
        '''
        selected_items = self.selectedItems()
        selected_row = -1
        already_selected = False
        if len(selected_items) is 0:
            if self.rowCount() is 0:
                selected_row = -1
            else:
                selected_row = 0
        else:
            for item in selected_items:
                row = self.row(item)
                if row == self._last_selected_row:
                    already_selected = True
                    # It is possible that the selection was extended
                    # then the new selected row notified.
                    continue
                already_selected = False
                selected_row = row
                break

        if already_selected:
            return

        if selected_row > 0:
            self.setCurrentItem(
                self.item(selected_row, 0),
                QtGui.QItemSelectionModel.ClearAndSelect |
                QtGui.QItemSelectionModel.Rows)
        self._last_selected_row = selected_row
        self.row_selected.emit(selected_row)

    def _set_row_changed(self, row):
        '''
        Set Row Changed.

        Indicate the given row that its values differ from the default input.
        '''
        for col in range(0, self.columnCount()):
            item = self.item(row, col)
            item.setFont(self._font_changed)
        if self._state_column is not None:
            item = self.item(row, self._state_column)
            item.setText(self._state_map['changed'])

    def _set_row_unchanged(self, row):
        '''
        Set Row Changed.

        Indicate the given row that its values differ from the default input.
        '''
        for col in range(0, self.columnCount()):
            item = self.item(row, col)
            item.setFont(self._font_unchanged)
            item.setForeground(self._brush_not_removed)
        if self._state_column is not None:
            item = self.item(row, self._state_column)
            item.setText(self._state_map['default'])

    def _set_row_removed(self, row):
        '''
        Set Row Removed.

        Reset all styles and indicate the given row that its values exist in
        the defaults, but not in the current values.
        '''
        self._set_row_unchanged(row)
        for col in range(0, self.columnCount()):
            item = self.item(row, col)
            item.setForeground(self._brush_removed)
        if self._state_column is not None:
            item = self.item(row, self._state_column)
            item.setText(self._state_map['removed'])

    def _set_row_inserted(self, row):
        '''
        Set Row Removed.

        Indicate the given row that its values exist in the defaults, but not
        in the settings.
        '''
        for col in range(0, self.columnCount()):
            item = self.item(row, col)
            item.setFont(self._font_inserted)
        if self._state_column is not None:
            item = self.item(row, self._state_column)
            item.setText(self._state_map['inserted'])

    def _update_tooltip(self, item):
        '''
        Update tooltip for item.

        Update the tooltip for the given item, assume its key has changed.

        Arguments:
            item -- Table Cell Widget that changed.
        '''
        key_item = self.item(item.row(), 0)
        value_item = self.item(item.row(), self._value_column)
        try:
            key_item.setToolTip(self.key_documentation[key_item.text()])
        except KeyError:
            pass
        try:
            value_item.setToolTip(self.value_documentation[value_item.text()])
        except KeyError:
            pass

    def set_status_column(self, column, state_map):
        '''
        Set State Column.

        Set the column that is updated by the table based on the state of the
        row with the texts from the state_map.
        '''
        self._state_column = column
        self._state_map = state_map

    def set_value_column(self, column):
        '''
        Set Value Column.

        Set the column that is used for values and for comparing to default
        values.
        '''
        self._value_column = column

    def _toggle_checkstate(self, item):
        '''
        Toggle Checkstate.

        Change the visual for checkable items. Emits change to editor.
        '''
        if not self._is_checkable(item):
            return
        new_checkstate = QtCore.Qt.Unchecked
        if item.checkState() == new_checkstate:
            new_checkstate = QtCore.Qt.Checked
        item.setCheckState(new_checkstate)
        self.values_changed.emit()

    def add_row(self, key, values,
                key_tooltip=None, values_tooltip=None):
        '''
        Add Row.

        Add a row with the given key and values. Optionally define tooltip for
        the key and values.
        Handles keys that have been marked as 'removed' by get_values
        Boolean values will be checkboxes.
        '''
        self.itemChanged.disconnect(self._commit_change)
        self.insertRow(self.rowCount())
        row = self.rowCount() - 1
        key_item = QtGui.QTableWidgetItem(key)
        if key.startswith(KEY_REMOVED_PREFIX):
            key_item = QtGui.QTableWidgetItem(key[len(KEY_REMOVED_PREFIX):])
            key_item.setData(ITEM_ROLE_REMOVED, True)
        if self._all_items_inserted:
            key_item.setData(ITEM_ROLE_INSERTED, True)
        key_item.setFlags(key_item.flags() ^ QtCore.Qt.ItemIsUserCheckable)
        if key_tooltip is not None:
            key_item.setToolTip(key_tooltip)

        self.setItem(row, 0, key_item)

        if len(values) + 1 > self.columnCount():
            self.setColumnCount(len(values) + 1)
            visible_columns = len(self._header_labels)
            for hidden_col in range(visible_columns, len(values) + 1):
                self.setColumnHidden(hidden_col, True)

        for (column, value) in enumerate(values):
            value_data = value.value.get()
            value_item = QtGui.QTableWidgetItem(value_data)
            if isinstance(value_data, (bool,)):
                value_item.setFlags(
                    QtCore.Qt.ItemIsUserCheckable |
                    QtCore.Qt.ItemIsSelectable |
                    QtCore.Qt.ItemIsEnabled)
                if value_data:
                    value_item.setCheckState(QtCore.Qt.Checked)
                else:
                    value_item.setCheckState(QtCore.Qt.Unchecked)
            else:
                value_item.setFlags(
                    value_item.flags() ^ QtCore.Qt.ItemIsUserCheckable)

            value_item.setData(ITEM_ROLE_INDEX, value)

            if values_tooltip is not None:
                value_item.setToolTip(values_tooltip)
            self.setItem(row, column + 1, value_item)
        if key_item.data(ITEM_ROLE_REMOVED):
            self._set_row_removed(row)
        else:
            self._set_row_unchanged(row)
        self.itemChanged.connect(self._commit_change)

    def can_edit(self, index):
        '''
        Can Edit.

        Returns true when the value in the index can be edited by the user.

        Arguments:
            index -- Model Index
        '''
        column = index.column()
        if column in self._disabled_editor_columns:
            return False
        if column is 0:
            return True
        if column is self._value_column:
            return True
        return False

    def disable_editor_column(self, column):
        '''
        Disable Editor Column.

        Disable that any editor is created for the given column.

        Arguments:
            column -- model index column.
        '''
        self._disabled_editor_columns.append(column)

    def enable_editor_column(self, column):
        '''
        Enable Editor Column.

        Re-enables the given column so it can be edited again.

        Arguments:
            column -- model index column.
        '''
        try:
            self._disabled_editor_columns.remove(column)
        except ValueError:
            pass

    def get_selected_key(self):
        '''
        Get Selected Key.

        Get the key of the currently highlighted row.
        '''
        selected_items = self.selectedItems()
        for item in selected_items:
            if self.column(item) != 0:
                continue
            return item.text()
        return None

    def get_selected_values(self):
        '''
        Get Selected Values.

        Compile a named_value_list for the current selection.
        '''
        values = named_value_list()
        selected_items = self.selectedItems()
        for item in selected_items:
            if self.column(item) != 0:
                continue
            row = self.row(item)
            key_item = self.item(row, 0)
            if key_item.text() == '':
                continue
            key = key_item.text()
            if key_item.data(ITEM_ROLE_REMOVED):
                key = KEY_REMOVED_PREFIX + key

            item_values = []
            for column in range(1, self.columnCount()):
                value_item = self.item(row, column)
                if value_item is None:
                    continue
                named_index_value = value_item.data(ITEM_ROLE_INDEX)
                item_value = values.item_model_class()
                if named_index_value:
                    item_value.name.set(named_index_value.name.get())
                else:
                    item_value.name.set(key)
                if self._is_checkable(value_item):
                    value = value_item.checkState() == QtCore.Qt.Checked
                else:
                    value = value_item.text()
                item_value.value.set(value)
                item_values.append(item_value)

            values.append({
                'key': key,
                'values': item_values
            })

        return values

    def get_values(self):
        '''
        Get Values.

        Compile a named_value_list for the current display.
        '''
        rows = self.rowCount()
        values = named_value_list()
        for row in range(0, rows):
            key_item = self.item(row, 0)
            if key_item.text() == '':
                continue
            key = key_item.text()
            if key_item.data(ITEM_ROLE_REMOVED):
                key = KEY_REMOVED_PREFIX + key

            item_values = []
            for column in range(1, self.columnCount()):
                value_item = self.item(row, column)
                named_index_value = value_item.data(ITEM_ROLE_INDEX)
                item_value = values.item_model_class()
                if named_index_value:
                    item_value.name.set(named_index_value.name.get())
                else:
                    item_value.name.set(key)
                if self._is_checkable(value_item):
                    value = value_item.checkState() == QtCore.Qt.Checked
                else:
                    value = value_item.text()
                item_value.value.set(value)
                item_values.append(item_value)

            values.append({
                'key': key,
                'values': item_values
            })

        return values

    def has_documentation(self, index):
        '''
        Has documentation.

        Evaluate if documentation for the column exists. For the first column
        the key_documentation is evaluated, the other columns need to be
        enabled with set_value_documenation

        Arguments:
            index -- Model Index
        '''
        column = index.column()
        if column in self._disabled_documentation_columns:
            return False
        if column is 0:
            if len(self.key_documentation.keys()) > 0:
                return True
        if column is self._value_column:
            if len(self.value_documentation.keys()) > 0:
                return True
        return False

    def in_edit_mode(self):
        '''
        In Edit Mode.

        Return True when an editor is currently displayed.
        '''
        return self._in_edit_mode

    def insert_row(self, template):
        '''
        Insert a new Row.

        Insert a row before the current selected row or at the begining of
        the table. Ensure that the flags are set for all columns.
        Scroll to the new row. Use the given template to setup the row to match
        patterns (eg also set hidden columns).
        '''
        row = self.rowCount()
        current = self.currentItem()
        if current:
            row = self.row(current)
        elif row > 0:
            row = 0
        self.itemChanged.disconnect(self._commit_change)
        self.insertRow(row)
        new_key_item = QtGui.QTableWidgetItem('')
        new_key_item.setFlags(
            new_key_item.flags() ^ QtCore.Qt.ItemIsUserCheckable)
        new_key_item.setData(ITEM_ROLE_INSERTED, True)
        new_key_item.setText(template.key.get())
        self.setItem(row, 0, new_key_item)

        if len(template.values) + 1 > self.columnCount():
            self.setColumnCount(len(template.values) + 1)
            visible_columns = len(self._header_labels)
            for hidden_col in range(visible_columns, len(template.values) + 1):
                self.setColumnHidden(hidden_col, True)

        for (column, template_item) in enumerate(template.values):
            new_item = QtGui.QTableWidgetItem('')
            new_item.setFlags(new_item.flags() ^ QtCore.Qt.ItemIsUserCheckable)
            value_data = template_item.value.get()
            if isinstance(template_item.value.get(), (bool,)):
                new_item.setFlags(
                    QtCore.Qt.ItemIsUserCheckable |
                    QtCore.Qt.ItemIsSelectable |
                    QtCore.Qt.ItemIsEnabled)
                if value_data:
                    new_item.setCheckState(QtCore.Qt.Checked)
                else:
                    new_item.setCheckState(QtCore.Qt.Unchecked)
            else:
                new_item.setText(value_data)
            new_item.setData(ITEM_ROLE_INDEX, template_item)
            self.setItem(row, column + 1, new_item)
        self._set_row_inserted(row)
        self.itemChanged.connect(self._commit_change)
        self.scrollTo(self.indexFromItem(new_item))
        self.setCurrentItem(
            new_item,
            QtGui.QItemSelectionModel.ClearAndSelect |
            QtGui.QItemSelectionModel.Rows)
        self.row_selected.emit(row)

    def update_selected_values(self, new_values):
        '''
        Update Selected Values.

        Set the values for the selected items with new values. Values need to
        be structured as::

            {
                'key': 'new key',
                'values': named_value_list
            }
        '''
        selected_items = self.selectedItems()
        self.itemChanged.disconnect(self._commit_change)
        new_values_index = 0
        for item in selected_items:
            if self.column(item) != 0:
                continue
            value_index = new_values_index
            new_values_index += 1

            row = self.row(item)
            key_item = self.item(row, 0)
            if key_item.text() == '':
                continue
            key_item.setText(new_values[value_index]['key'])
            values = new_values[value_index]['values']

            for column in range(1, self.columnCount()):
                value_item = self.item(row, column)
                named_index_value = value_item.data(ITEM_ROLE_INDEX)

                if self._is_checkable(value_item):
                    state = values.get_value(named_index_value.name.get())
                    if isinstance(state, (bool,)):
                        if state is True:
                            state = QtCore.Qt.Checked
                        else:
                            state = QtCore.Qt.Unchecked
                    value_item.setCheckState(state)
                else:
                    value_item.setText(
                        values.get_value(named_index_value.name.get()))
        self.itemChanged.connect(self._commit_change)

    def remove_selected_row(self):
        '''
        Remove selected Row.

        Removes the row that is currently selected and does nothing if no row
        is selected. When row is a inserted row, the complete entry is removed.
        '''
        current_item = self.currentItem()
        current = self.item(current_item.row(), 0)
        if current.data(ITEM_ROLE_INSERTED):
            # inserted rows can be removed from view.
            new_row = current.row()
            if new_row > 0:
                new_row = current.row() - 1
            self.removeRow(current.row())
            if self.rowCount() > 0:
                new_current_item = self.item(new_row, 0)
                self.setCurrentItem(
                    new_current_item,
                    QtGui.QItemSelectionModel.ClearAndSelect |
                    QtGui.QItemSelectionModel.Rows)
        else:
            self._set_row_removed(current.row())
            self.item(current.row(), 0).setData(ITEM_ROLE_REMOVED, True)
        self.values_changed.emit()

    def restore_selected_row(self):
        '''
        Restore selected Row.

        Restores all values for the selected row from the defaults. Leaves the
        key unchanged.
        '''
        current = self.currentItem()
        self.item(current.row(), 0).setData(ITEM_ROLE_REMOVED, False)
        for column in range(1, self.columnCount()):
            item = self.item(current.row(), column)
            value_data = ''
            default_value = item.data(ITEM_ROLE_DEFAULT)
            if default_value is not None:
                value_data = self._get_restore_value(column, default_value)
            if isinstance(value_data, (bool,)):
                if value_data:
                    item.setCheckState(QtCore.Qt.Checked)
                else:
                    item.setCheckState(QtCore.Qt.Unchecked)
            else:
                item.setText(value_data)
        self._set_row_unchanged(current.row())
        self.values_changed.emit()

    def reset_header(self):
        '''
        Reset Header.

        Reset the header columns based on the configured header labels.
        Ensure that the first column is minimal.
        Clear all columns that have no header label.
        '''
        self.setColumnCount(len(self._header_labels))
        self.resizeColumnsToContents()
        for (column, label) in enumerate(self._header_labels):
            option_column_item = QtGui.QTableWidgetItem(label)
            self.setHorizontalHeaderItem(column, option_column_item)

    def set_changed_from_defaults(self):
        '''
        Set Changed state from Defaults.

        Check all items if they have a default value and consider them changed
        when the values do not match
        '''
        rows = self.rowCount()
        for row in range(0, rows):
            item = self.item(row, self._value_column)
            if item.data(ITEM_ROLE_DEFAULT) is None:
                continue
            if isinstance(item.data(ITEM_ROLE_DEFAULT), (list,)):
                changed = True
                for default_item in item.data(ITEM_ROLE_DEFAULT):
                    if default_item.value.get() == item.text():
                        changed = False
                if not changed:
                    continue
                self._set_row_changed(row)
            elif item.data(ITEM_ROLE_DEFAULT).value.get() != item.text():
                self._set_row_changed(row)

    def set_default_values(self, key, values_list):
        '''
        Set default values.

        Set values for given key that is considered default.
        '''
        self.itemChanged.disconnect(self._commit_change)
        for row in range(0, self.rowCount()):
            key_item = self.item(row, 0)
            if not key_item:
                continue
            if key != key_item.text():
                continue
            for column in range(1, self.columnCount()):
                value_item = self.item(row, column)
                column_values = []
                for values in values_list:
                    column_values.append(values[column - 1])
                value_item.setData(
                    ITEM_ROLE_DEFAULT, column_values)
        self.itemChanged.connect(self._commit_change)

    def set_filter(self, filter_columns, filter_text):
        '''
        Set visual Filter.

        Hides all rows that do not contain the filter_text in the
        filter_column.
        '''
        invert = False
        filter_string = filter_text
        try:
            if filter_string[0] == '!':
                invert = True
                filter_string = filter_string[1:]
        except IndexError:
            pass
        rows = self.rowCount()
        for row in range(0, rows):
            if filter_string == '':
                self.setRowHidden(row, False)
                continue
            match = False
            for filter_column in filter_columns:
                value_item = self.item(row, filter_column)
                if filter_string in value_item.text():
                    match |= True
            if invert:
                match = not match
            self.setRowHidden(row, not match)

    def set_header(self, label_list):
        '''
        Set Header.

        Set available Header labels.
        '''
        self._header_labels = label_list
        self.reset_header()

    def set_inserted_from_defaults(self):
        '''
        Set Inserted state from Defaults.

        Check all items if they have a default value and consider them
        inserted when no default was found.
        '''
        rows = self.rowCount()
        for row in range(0, rows):
            item = self.item(row, 1)
            if item.data(ITEM_ROLE_DEFAULT) is None:
                self._set_row_inserted(row)

    def set_inserted(self, inserted):
        '''
        Set inserted.

        Set the complete table to behave as if values were inserted.

        Arguments:
            inserted -- True/False
        '''
        self._all_items_inserted = True

    def set_row_edit_callback(self, callback_function):
        '''
        Set Row Edit Callback.

        Override the default cell editor with an editor for the complete
        row.
        '''
        if callback_function is None:
            self._custom_row_edit = None
            self.itemDoubleClicked.disconnect()
            self.itemDoubleClicked.connect(self._editor_started)
        else:
            self._custom_row_edit = callback_function
            self.itemDoubleClicked.disconnect()
            self.itemDoubleClicked.connect(self._custom_row_edit)

    def set_selected_key(self, key):
        '''
        Set Selected Key.

        Selects the row with the given key.
        '''
        row_selected = False
        if key is None:
            self.setCurrentItem(
                self.currentItem(), QtGui.QItemSelectionModel.Deselect)
        for row in range(0, self.rowCount()):
            key_item = self.item(row, 0)
            if not key_item:
                continue
            if key != key_item.text():
                continue
            self.setCurrentItem(
                key_item,
                QtGui.QItemSelectionModel.ClearAndSelect |
                QtGui.QItemSelectionModel.Rows)
            self.scrollTo(self.indexFromItem(key_item))
            self.row_selected.emit(row)
            row_selected = True
            break
        if row_selected is False:
            # when there is a first-row, make it selected
            # both emit and setCurrentItem are required
            if self.rowCount() > 0:
                first_item = self.item(0, 0)
                self.setCurrentItem(
                    first_item,
                    QtGui.QItemSelectionModel.ClearAndSelect |
                    QtGui.QItemSelectionModel.Rows)
                self.row_selected.emit(0)
                self.verticalScrollBar().setValue(0)
            else:
                self.row_selected.emit(-1)
