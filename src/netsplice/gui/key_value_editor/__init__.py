# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
key_value editor widget module
'''

from netsplice.gui.account_edit.model import model as module_model
from netsplice.gui.key_value_editor.key_value_editor import key_value_editor

name = 'key_value_editor'

endpoints = []

model = module_model()


def initialize(parent, dispatcher):
    return key_value_editor(parent, dispatcher)
