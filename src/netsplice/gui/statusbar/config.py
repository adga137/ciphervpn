# -*- coding: utf-8 -*-
# config.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Config for Statusbar
'''

# State Icons
RES_ICON_HEARTBEAT_CONNECTED = (
    ':/images/statusbar/heartbeat_connected.png')
RES_ICON_HEARTBEAT_WARNING = (
    ':/images/statusbar/heartbeat_warning.png')
RES_ICON_HEARTBEAT_ERROR = (
    ':/images/statusbar/heartbeat_error.png')
