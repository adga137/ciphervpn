# -*- coding: utf-8 -*-
# statusbar.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Custom statusbar manager.
'''

from PySide import QtGui
from netsplice import __version__ as VERSION
from netsplice.config import gui as config_gui
from netsplice.gui.statusbar import config


class statusbar(QtGui.QStatusBar):
    '''
    Custom Statusbar.
    '''

    def __init__(self, parent, dispatcher):
        QtGui.QStatusBar.__init__(self, parent)
        self.backend = dispatcher
        self.backend_available = False
        self.heartbeat_icon = QtGui.QLabel(self)
        self.heartbeat_icon.setToolTip(self.tr('Heartbeat status'))
        self.heartbeat_icon_connected = QtGui.QIcon()
        self.heartbeat_icon_error = QtGui.QIcon()
        self.heartbeat_icon_warning = QtGui.QIcon()
        self._load_icons()
        self.message = QtGui.QLabel(self)
        self.connections_display = QtGui.QLabel(self)
        self.connections_display.setToolTip(
            self.tr('Number of active connections'))
        self.version_display = QtGui.QLabel(self)
        self.version_display.setToolTip(self.tr('Application Version'))
        self.product_owner = QtGui.QLabel(self)
        self.product_owner.setText(config_gui.STATUSBAR_PRODUCT_OWNER)
        self.version_display.setText(VERSION)
        self.addWidget(self.message)
        self.addPermanentWidget(self.heartbeat_icon)
        self.addPermanentWidget(self.connections_display)
        self.addPermanentWidget(self.version_display)
        self.addPermanentWidget(self.product_owner)
        self.backend.application.event_loop.connections_changed.connect(
            self._backend_connections_changed)
        self.backend.application.event_loop.preferences_changed.connect(
            self._backend_preferences_changed)
        self.backend.heartbeat_connected.connect(
            self._backend_heartbeat_connected)
        self.backend.heartbeat_warning.connect(
            self._backend_heartbeat_warning)
        self.backend.heartbeat_error.connect(
            self._backend_heartbeat_error)
        self._backend_heartbeat_warning()

    def _backend_connections_changed(
            self, connection_list, failed_connection_list):
        self.connections_display.setText(
            self.tr('C: {connections}').format(
                connections=len(connection_list)))

    def _backend_heartbeat_connected(self):
        self._set_heartbeat_icon(self.heartbeat_icon_connected.pixmap(16, 16))

    def _backend_heartbeat_error(self):
        self._set_heartbeat_icon(self.heartbeat_icon_error.pixmap(16, 16))

    def _backend_heartbeat_warning(self):
        self._set_heartbeat_icon(self.heartbeat_icon_warning.pixmap(16, 16))

    def _backend_preferences_changed(self, model):
        '''
        Backend Preferences changed.

        Display the elements of the statusbar according to the preferences.
        '''
        has_items = False
        if model.ui.statusbar_show_version.get():
            self.version_display.show()
            has_items = True
        else:
            self.version_display.hide()
        if model.ui.statusbar_show_product_owner.get():
            self.product_owner.show()
            has_items = True
        else:
            self.product_owner.hide()
        if model.ui.statusbar_show_connections.get():
            self.connections_display.show()
            has_items = True
        else:
            self.connections_display.hide()
        if model.ui.statusbar_show_heartbeat.get():
            self.heartbeat_icon.show()
            has_items = True
        else:
            self.heartbeat_icon.hide()
        if not has_items:
            self.hide()
        else:
            self.show()

    def _load_icons(self):
        self.heartbeat_icon_connected.addPixmap(
            QtGui.QPixmap(config.RES_ICON_HEARTBEAT_CONNECTED))
        self.heartbeat_icon_error = QtGui.QIcon(
            QtGui.QPixmap(config.RES_ICON_HEARTBEAT_ERROR))
        self.heartbeat_icon_warning = QtGui.QIcon(
            QtGui.QPixmap(config.RES_ICON_HEARTBEAT_WARNING))

    def _set_heartbeat_icon(self, pixmap):
        self.heartbeat_icon.setPixmap(pixmap)
