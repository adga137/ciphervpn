# -*- coding: utf-8 -*-
# event_loop.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Event loop for GUI app. Waits for model changes on event-module and calls
dispatcher functions that registered on those events.
'''

import json
import traceback

from PySide.QtCore import QCoreApplication, QObject, QTimer, Signal

import netsplice.backend.event as event_module
import netsplice.backend.event.names as names
import netsplice.backend.event.types as types
import netsplice.config.gui as config
from netsplice.gui.model.response.gui import (
    gui as response_gui_model
)
from netsplice.gui.model.gui import (
    gui as gui_model
)
from netsplice.util import get_logger, get_logger_handler
from netsplice.util.ipc.event_loop import event_loop as event_loop_base

logger = get_logger()


class event_loop(QObject, event_loop_base):
    notify_error = Signal(basestring)
    connection_error = Signal(basestring)
    query_username_password = Signal(basestring)
    query_private_key_password = Signal(basestring)

    # Signals for GUI that should be used instead of marshalling a
    # backend_changed json.
    accounts_changed = Signal(list, list, list, list)
    connections_changed = Signal(list, list)
    gui_model_changed = Signal(object)
    log_changed = Signal(list)
    preferences_changed = Signal(object)
    preferences_plugins_changed = Signal(object)

    def __init__(self):
        QObject.__init__(self)
        event_loop_base.__init__(self, 'Gui')

        self.log_timer = QTimer()
        self.log_timer.timeout.connect(self.publish_logs)
        self.log_timer.setInterval(config.PUBLISH_LOG_TIMER)
        self._last_json = {
            'preferences': '',
            'preferences_plugins': '',
            'accounts': '',
            'connections': '',
            'gui': '',
        }
        self._recent_connection_map = {}

    def _connections_changed(self, connections, failed_connections):
        '''
        Connections changed.

        Evaluates if the state of a connection has changed.
        '''
        map = {}
        for connection_item in connections:
            map[connection_item.id.get()] = connection_item.state.get()
        for connection_item in failed_connections:
            map[connection_item.id.get()] = connection_item.state.get()
        if map != self._recent_connection_map:
            self._recent_connection_map = map
            return True
        return False

    def start(self):
        self.log_timer.start()
        event_loop_base.start(self)
        # not a coroutine, do not call base
        # no module events used in the QT-based ui, they use signals and
        # slot.

    def process_events(self, model_json):
        '''
        Process Events.

        Process events from backend and emit signals modules may connect to.
        Ensure that json is not emitted multiple times, find events that are
        not yet processed (by index) and process them.
        '''
        response_model = response_gui_model()
        response_model.from_json(model_json)
        event_model_list = response_model.events
        if self._last_json['gui'] != model_json:
            self._last_json['gui'] = model_json
            model = gui_model()
            model.from_json(model_json)
            connections_changed = self._connections_changed(
                model.connections, model.failed_connections)
            if connections_changed:
                self.connections_changed.emit(
                    model.connections, model.failed_connections)
            self.gui_model_changed.emit(model)

        if not len(event_model_list):
            return
        if event_model_list[-1].index.get() <= self.events_processed:
            return
        for event in event_model_list:
            if event.index.get() <= self.events_processed:
                continue
            self.events_processed = event.index.get()
            self.process_event(event)

    def process_event(self, event_model_instance):
        '''
        Process Event.

        Process single event from backend and emit signals modules may connect
        to.
        '''
        event_name = event_model_instance.name.get()
        event_type = event_model_instance.type.get()
        event_data = event_model_instance.data.get()

        if event_type == types.NOTIFY:
            if event_name == names.CONNECTION_ERROR:
                self.connection_error.emit(event_data)
            if event_name == names.ERROR:
                self.notify_error.emit(event_data)
        if event_type == types.QUERY:
            if event_name == names.USERNAME_PASSWORD:
                self.query_username_password.emit(event_data)
            if event_name == names.PRIVATE_KEY_PASSWORD:
                self.query_private_key_password.emit(event_data)
        self.process_plugin_event(event_model_instance)
        QCoreApplication.processEvents()

    def process_plugin_event(self, event_model_instance):
        '''
        Process Plugin Event.

        Process single event in plugins that have registered for the event's
        origin.

        Decorators:
            gen.continue

        Arguments:
            event_model_instance (model.event): event to be processed

        '''
        event_destinations = event_module.registered_destinations
        registered_event_destinations = event_destinations.keys()
        for plugin_module in self.plugin_modules:
            if type(plugin_module) not in registered_event_destinations:
                continue
            plugin_events = event_destinations[type(plugin_module)]
            if event_model_instance.name.get() not in plugin_events:
                continue

            try:
                plugin_module.process_event(event_model_instance)
            except Exception as errors:
                tb = ''.join(traceback.format_exc(errors))
                logger.warn(
                    'Exception during plugin event handling: %s %s. (%s)'
                    % (str(errors), type(errors), tb))

    def process_preferences_events(self, model_json):
        '''
        Process Preferences Events.

        Emit events when the preferences have changed.
        '''
        model = self.application.get_model().preferences
        model.from_json(model_json)
        if self._last_json['preferences'] != model_json:
            self.preferences_changed.emit(model)
            self._last_json['preferences'] = model_json

            accounts_json = model.accounts.to_json()
            accounts_json += model.grouped_accounts.to_json()
            accounts_json += model.groups.to_json()
            accounts_json += model.chains.to_json()
            if self._last_json['accounts'] != accounts_json:
                self.accounts_changed.emit(
                    model.accounts, model.groups, model.grouped_accounts,
                    model.chains)

    def process_preferences_plugins_events(self, model_json):
        '''
        Process Plugins Events.

        Emit events when the preferences for plugins have changed.
        '''
        model = self.application.get_model().preferences.plugins
        if self._last_json['preferences_plugins'] != model_json:
            json_model = json.loads(model_json)
            for plugin in model:
                plugin_name = plugin.name.get()
                model_plugin = model.find_by_name(plugin_name)
                json_model_plugin = None
                for json_model_item in json_model:
                    if json_model_item['name'] != plugin_name:
                        continue
                    json_model_plugin = json_model_item
                    break
                if json_model_plugin is None:
                    continue
                model_plugin.from_dict(json_model_plugin)
                model_plugin.apply_values()
            self.preferences_plugins_changed.emit(model)
            self._last_json['preferences_plugins'] = model_json

    def publish_logs(self):
        '''
        Send the log_items to the backend when the dispatcher is available.
        '''
        if self.dispatcher is None:
            return
        if self.dispatcher.is_shutting_down():
            return
        if not self.dispatcher.is_active():
            return
        logs = get_logger_handler().take_logs()
        for log_item in logs:
            self.dispatcher.logs_set.emit(log_item.to_json())
