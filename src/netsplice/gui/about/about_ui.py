# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/gui/about/views/about.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_About(object):
    def setupUi(self, about):
        about.setObjectName("about")
        about.setGeometry(QtCore.QRect(0, 0, 700, 680))
        self.about_layout = QtGui.QVBoxLayout(about)
        self.about_layout.setObjectName("about_layout")
        self.about_message_line = QtGui.QWidget(about)
        self.about_message_line.setObjectName("about_message_line")
        self.about_message_line_layout = QtGui.QHBoxLayout(self.about_message_line)
        self.about_message_line_layout.setContentsMargins(0, 0, 0, 0)
        self.about_message_line_layout.setObjectName("about_message_line_layout")
        self.application_icon = QtGui.QLabel(self.about_message_line)
        self.application_icon.setProperty("maximumSize", QtCore.QSize(64, 64))
        self.application_icon.setObjectName("application_icon")
        self.about_message_line_layout.addWidget(self.application_icon)
        self.message_label = QtGui.QLabel(self.about_message_line)
        self.message_label.setWordWrap(True)
        self.message_label.setOpenExternalLinks(True)
        self.message_label.setObjectName("message_label")
        self.about_message_line_layout.addWidget(self.message_label)
        self.about_layout.addWidget(self.about_message_line)
        self.plugin_about_tabs = QtGui.QTabWidget(about)
        self.plugin_about_tabs.setObjectName("plugin_about_tabs")
        self.about_layout.addWidget(self.plugin_about_tabs)
        spacerItem = QtGui.QSpacerItem(5, 5, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.MinimumExpanding)
        self.about_layout.addItem(spacerItem)
        self.action_layout = QtGui.QHBoxLayout()
        self.action_layout.setObjectName("action_layout")
        spacerItem1 = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.action_layout.addItem(spacerItem1)
        self.button_logs = QtGui.QPushButton(about)
        self.button_logs.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.button_logs.setObjectName("button_logs")
        self.action_layout.addWidget(self.button_logs)
        self.button_ok = QtGui.QPushButton(about)
        self.button_ok.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.button_ok.setObjectName("button_ok")
        self.action_layout.addWidget(self.button_ok)
        self.about_layout.addLayout(self.action_layout)

        self.retranslateUi(about)
        QtCore.QMetaObject.connectSlotsByName(about)

    def retranslateUi(self, about):
        about.setWindowTitle(QtGui.QApplication.translate("About", "Netsplice About", None, QtGui.QApplication.UnicodeUTF8))
        self.button_logs.setText(QtGui.QApplication.translate("About", "Logs", None, QtGui.QApplication.UnicodeUTF8))
        self.button_ok.setText(QtGui.QApplication.translate("About", "Ok", None, QtGui.QApplication.UnicodeUTF8))

