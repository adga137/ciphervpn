# -*- coding: utf-8 -*-
# about.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
About editor.

Widget for displaying information about the application.
'''

from PySide.QtCore import Qt
from PySide.QtGui import (
    QFrame, QLabel, QPixmap,
    QScrollArea, QSizePolicy, QSpacerItem,
    QWidget, QVBoxLayout)
from netsplice import __version__ as VERSION
from netsplice.config import about as config_about
from netsplice.config.gui import RES_ICON_APPLICATION
from netsplice.gui.widgets.dialog import dialog
from netsplice.gui.about.about_ui import Ui_About
from netsplice.gui.account_edit.type_editor_factory import get_account_plugins


class about(dialog):
    '''
    Custom widget for displaying information about the application.
    '''

    def __init__(self, parent=None, dispatcher=None):
        '''
        Initialize module.

        Initialize the dialog and member variables.
        '''
        dialog.__init__(self, parent)
        self.mainwindow = parent
        self.backend = dispatcher
        self.ui = Ui_About()
        self.ui.setupUi(self)
        self.ui.button_ok.clicked.connect(self.close)
        self.ui.button_logs.clicked.connect(self.show_logs)
        application_logo = QPixmap(RES_ICON_APPLICATION)
        self.ui.application_icon.setPixmap(application_logo)
        self.ui.application_icon.setScaledContents(True)
        self.setWindowTitle(self.tr('About Netsplice - %s') % (VERSION,))
        self.init_core_text()
        self.init_plugin_text()

    def init_core_text(self):
        '''
        Init Core Text.

        Initialize the core application text.
        '''
        bitcoin_url = config_about.SUPPORT_BITCOIN_URL % {
            'SUPPORT_BITCOIN': config_about.SUPPORT_BITCOIN}

        msg = self.tr(config_about.ABOUT_TEXT)
        msg = msg % {
            'COPYRIGHT_URL': config_about.COPYRIGHT_URL,
            'LICENSE_URL': config_about.LICENSE_URL,
            'LICENSE_NAME': config_about.LICENSE_NAME,
            'SUPPORT_BITCOIN': config_about.SUPPORT_BITCOIN,
            'SUPPORT_BITCOIN_URL': bitcoin_url,
            'VERSION': VERSION}
        self.ui.message_label.setText(msg)
        self.ui.message_label.setTextInteractionFlags(
            Qt.TextBrowserInteraction)

    def init_plugin_text(self):
        '''
        Init Plugin Text.

        Initialize the text for all Plugins.
        '''
        for plugin_item in self.backend.application.plugins:
            tab_label = None
            tab_description = None
            try:
                tab_label = plugin_item.config.PLUGIN_NAME
            except AttributeError:
                # Ignore plugins that did not provide a PLUGIN_NAME
                continue
            try:
                tab_label = plugin_item.config.DISPLAY_NAME
            except AttributeError:
                # use PLUGIN_NAME for tab label, fix it for end users
                tab_label = tab_label.replace('_', ' ')
                tab_label = tab_label.title()
            try:
                tab_description = plugin_item.config.gui.DESCRIPTION
            except AttributeError:
                tab_description = self.tr('No Plugin Description')

            tab_widget = QWidget(self)
            tab_widget_layout = QVBoxLayout(tab_widget)
            scroll_area = QScrollArea(tab_widget)
            scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
            scroll_area.setWidgetResizable(True)
            scroll_area.setFrameShape(QFrame.NoFrame)

            tab_widget_description = QLabel(tab_widget)
            tab_widget_description.setContentsMargins(2, 0, 0, 0)
            tab_widget_description.setText(
                tab_description)
            tab_widget_description.setWordWrap(True)
            tab_widget_description.setAlignment(Qt.AlignTop | Qt.AlignLeft)
            scroll_area.setWidget(tab_widget_description)

            tab_widget_layout.addWidget(scroll_area)
            bottom_spacer = QSpacerItem(
                1, 1, QSizePolicy.Minimum, QSizePolicy.MinimumExpanding)
            tab_widget_layout.addSpacerItem(bottom_spacer)

            try:
                custom_about = plugin_item.gui.about(
                    self, self.backend)
                custom_about.setContentsMargins(0, 0, 0, 0)
                tab_widget_layout.addWidget(custom_about)
            except AttributeError:
                # No custom about widget.
                pass

            self.ui.plugin_about_tabs.addTab(
                tab_widget, tab_label)

    def show_logs(self):
        '''
        Show logs.

        Display the logs window by triggering an event-loop signal that
        causes the mainwindow to raise the logwindow.
        '''
        event_loop = self.backend.application.event_loop
        event_loop.notify_error.emit('')
