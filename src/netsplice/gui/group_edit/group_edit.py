# -*- coding: utf-8 -*-
# group_edit.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Group editor.

Edit group settings with a specialized editor and manage general actions.
'''
from PySide.QtCore import (Signal)
from netsplice.gui.widgets.dialog import dialog

from netsplice.gui.group_edit.group_edit_ui import Ui_GroupEdit
from netsplice.gui.group_edit.model.response.group_id import (
    group_id as group_id_model
)
from netsplice.gui.group_edit.model.response.group_edit_item import (
    group_edit_item as group_edit_item_model
)
from netsplice.util.errors import NotFoundError
from netsplice.util import basestring, escape_html


class group_edit(dialog):
    '''
    Custom Widget for editing and creating Groups.

    Defines the widget that is used to configure groups.
    '''

    create_done_signal = Signal(object)
    create_failed_signal = Signal(object)
    load_done_signal = Signal(object)
    load_failed_signal = Signal(object)
    model_loaded_signal = Signal(object)
    model_stored_signal = Signal(basestring)
    model_reset_signal = Signal()
    update_done_signal = Signal(object)
    update_failed_signal = Signal(object)

    def __init__(self, parent=None, dispatcher=None):
        '''
        Initialize Module.

        Initialize the dialog and member variables.
        '''
        dialog.__init__(self, parent)
        self.mainwindow = parent
        self.backend = dispatcher
        self.ui = Ui_GroupEdit()
        self.ui.setupUi(self)
        self.ui.general_tab_layout.setContentsMargins(10, 9, 10, 0)
        self.ui.general_tab_vert_layout.setContentsMargins(0, 0, 0, 0)
        self.ui.group_name_type_layout.setContentsMargins(0, 0, 0, 0)
        self.ui.group_name_label_widget_layout.setContentsMargins(0, 0, 0, 0)

        self.parent_group_id = None
        self._creating = False
        self._signals_connected = False
        self._default_description = None

        self._init_ui()
        self._connect_signals()
        self._reset_widgets()

        preferences = self.backend.application.get_model().preferences
        self.update_model(
            preferences.accounts, preferences.groups,
            preferences.grouped_accounts, preferences.chains)

    def _commit(self):
        '''
        Commit Handler.

        Call group_type_editor to save a previously loaded account.
        '''
        self.ui.button_commit.setEnabled(False)
        self.ui.button_create.setEnabled(False)

        group_id = self.editor_model.id.get()
        weight = self.editor_model.weight.get()
        collapsed = self.editor_model.collapsed.get()
        parent_group_id = self.editor_model.parent.get()
        group_name = self.ui.group_name_edit.text()
        self.backend.group_update.emit(
            group_id,
            group_name,
            weight,
            collapsed,
            parent_group_id,
            self.update_done_signal,
            self.update_failed_signal)

    def _backend_model_changed(self, model):
        '''
        Model Change Handler.

        Handle when the backend model has changed. Used to display errors
        of the check function and enable/disable the commit button based
        on the active connection list.
        '''
        pass

    def _cancel(self):
        '''
        Cancel Handler.

        Reset the control and close the window.
        '''
        self._reset_widgets()
        self.mainwindow.accounts_activity()
        self.model_reset_signal.emit()
        self.hide()

    def _config_changed(self):
        '''
        Config Changed.

        Handle that a config value has been changed.
        '''
        has_name = True
        if self.ui.group_name_edit.text() == '':
            has_name = False
        self.ui.button_commit.setEnabled(has_name)
        self.ui.button_create.setEnabled(has_name)

        if has_name:
            if self.ui.button_commit.isVisible():
                self.ui.button_commit.setDefault(True)
            else:
                self.ui.button_create.setDefault(True)

    def _connect_signals(self):
        '''
        Connect Signals.

        Connect local and widget signals to member functions.
        '''
        if self._signals_connected:
            return
        self.backend.application.event_loop.gui_model_changed.connect(
            self._backend_model_changed)

        self.backend.application.event_loop.accounts_changed.connect(
            self.update_model)

        self.create_done_signal.connect(self._create_done)
        self.create_failed_signal.connect(self._create_failed)

        self.load_done_signal.connect(self._load_done)
        self.load_failed_signal.connect(self._load_failed)

        self.update_done_signal.connect(self._update_done)
        self.update_failed_signal.connect(self._update_failed)

        self.ui.group_name_edit.textChanged.connect(self._config_changed)
        self.ui.button_cancel.clicked.connect(self._cancel)
        self.ui.button_commit.clicked.connect(self._commit)
        self.ui.button_create.clicked.connect(self._create)
        self._signals_connected = True

    def _create(self):
        '''
        Create Handler.

        Create a new account with the current configuration.
        '''
        self.ui.button_commit.setEnabled(False)
        self.ui.button_create.setEnabled(False)
        weight = 0
        try:
            child_groups = self.groups.find_children(self.parent_group_id)
            for group in child_groups:
                if group.weight.get() > weight:
                    weight = group.weight.get()
        except NotFoundError:
            pass
        parent_group_id = self.parent_group_id
        group_name = self.ui.group_name_edit.text()
        self.backend.group_create.emit(
            group_name,
            weight,
            parent_group_id,
            self.create_done_signal,
            self.create_failed_signal)

    def _create_failed(self, errors):
        '''
        Create Failed.

        Handle when create account returned with a error.
        '''
        self._show_error(self.tr('Create failed'), errors)

    def _create_done(self, response):
        '''
        Create Done.

        Handle when create account has been successfully completed. Close the
        Window.
        '''
        group_id = group_id_model()
        group_id.from_json(response)
        self.model_stored_signal.emit(group_id.id.get())

        self._reset_widgets()
        self.ui.button_commit.setEnabled(True)
        self.mainwindow.accounts_activity()
        self.hide()
        self.backend.preferences_get.emit()

    def _disconnect_signals(self):
        '''
        Disconnect Signals.

        Disconnect local and widget signals from member functions.
        '''
        if not self._signals_connected:
            return
        self.backend.application.event_loop.gui_model_changed.disconnect(
            self._backend_model_changed)
        self.backend.application.event_loop.accounts_changed.disconnect(
            self.update_model)

        self.create_done_signal.disconnect(self._create_done)
        self.create_failed_signal.disconnect(self._create_failed)

        self.load_done_signal.disconnect(self._load_done)
        self.load_failed_signal.disconnect(self._load_failed)

        self.update_done_signal.disconnect(self._update_done)
        self.update_failed_signal.disconnect(self._update_failed)

        self.ui.group_name_edit.textChanged.disconnect(self._config_changed)
        self.ui.button_cancel.clicked.disconnect(self._cancel)
        self.ui.button_commit.clicked.disconnect(self._commit)
        self.ui.button_create.clicked.disconnect(self._create)
        self._signals_connected = False

    def _format_error(self, errors):
        '''
        Format Error.

        Formats an error message (from exceptions).
        '''
        formated_error = ''
        if str(errors):
            formated_error = str(errors)
            formated_error = escape_html(formated_error)
            formated_error = formated_error.replace(
                '\n', '<br/>')

        return formated_error

    def _init_ui(self):
        '''
        Initialize UI.

        Connect the ui signals to handler functions.
        '''
        pass

    def _load_done(self, response):
        '''
        Load Done.

        Handles when loading a account has been returned from the backend.
        Used because a account needs to request editable attributes that are
        not available to the default account model.
        '''
        response_model = group_edit_item_model()
        response_model.from_json(response)
        self._load_model(response_model)

    def _load_failed(self, errors):
        '''
        Load failed.

        Handle when loading account attributes failed.
        '''
        self._show_error(self.tr('Load Group Failed'), errors)

    def _load_model(self, group_model_instance):
        '''
        Load Model.

        Load account from the given model instance.
        '''
        self.editor_model = group_model_instance
        self.ui.group_name_edit.setText(
            self.editor_model.name.get())

        self.group_type = None
        self.model_loaded_signal.emit(group_model_instance)

    def _reset_widgets(self):
        '''
        Reset Widgets.

        Reset the account widgets to default values.
        '''
        self.editor_model = group_edit_item_model()
        self.ui.group_name_edit.setText('')
        self.ui.group_edit_tabs.setCurrentIndex(0)
        self.ui.user_errors.setText('')
        self.ui.user_errors.setVisible(False)
        self._config_changed()

    def _show_error(self, message, errors):
        '''
        Show Error.

        Helper to display error messages alongside with exeption errors
        '''
        formated_error = message
        formated_error = '<br/>'
        formated_error += self._format_error(errors)

        self.ui.user_errors.setText(formated_error)
        self.ui.user_errors.setVisible(True)

    def _update_done(self, response):
        '''
        Update done.

        Handle when updating a existing account has completed. Closes the
        dialog window.
        '''
        group_id = group_id_model()
        group_id.from_json(response)
        self.model_stored_signal.emit(group_id.id.get())
        self._reset_widgets()
        self.ui.button_commit.setEnabled(True)
        self.ui.button_create.setEnabled(True)
        self.mainwindow.accounts_activity()
        self.hide()

    def _update_failed(self, errors):
        '''
        Update failed.

        Handle when updating a existing account has failed. Display the error.
        '''
        self._show_error(self.tr('Update failed'), errors)

    def _set_readonly(self, state):
        '''
        Set Readonly.

        Set the group_edit dialog readonly while a connection is active.
        '''
        self.ui.group_name_edit.setEnabled(not state)
        # update commit / create button
        self._config_changed()

    def create(self, parent_group_id=None):
        '''
        Create new Account.

        Configures the dialog to create a new account with the optional given
        parent group id. Hide Commit Button and show Create Button
        '''
        self.parent_group_id = parent_group_id
        self._creating = True
        self.ui.button_commit.setVisible(False)
        self.ui.button_create.setVisible(True)
        self.ui.button_create.setEnabled(False)
        self._reset_widgets()
        self.model_loaded_signal.emit(None)

    def hideEvent(self, event):
        '''
        Hide Event.

        Disconnect signals from widgets.
        '''
        self._disconnect_signals()

    def showEvent(self, event):
        '''
        Show Event.

        Connect signals to backend and widgets.
        '''
        self._connect_signals()

    def load(self, group_id):
        '''
        Load existing Account.

        Configures the dialog to edit a existing account identified by the
        given group_id. Hide Create button and show Commit button.
        '''
        self._creating = False
        self.ui.button_commit.setVisible(True)
        self.ui.button_create.setVisible(False)
        self._reset_widgets()
        self.backend.group_get.emit(
            group_id,
            self.load_done_signal,
            self.load_failed_signal)

    def reset(self):
        '''
        Reset the Dialog.

        Forget all user-entered and loaded data.
        '''
        self._reset_widgets()

    def update_model(
            self, accounts_model, groups_model,
            grouped_accounts_model, chains_model):
        '''
        Update Model.

        The accounts changed in the backend. Store the model and make sure
        there is a root_item visible. When no accounts are available, display
        a message that allows the creation of a new account.

        Arguments:
            accounts_model (list): list of all accounts
            groups_model (list): list of all groups
            grouped_accounts_model (list): \
                list of all accounts with association to a group and a weight
            chains_model (list): list of all accounts in their \
                chain location
        '''
        self.accounts = accounts_model
        self.groups = groups_model
        self.grouped_accounts = grouped_accounts_model
        self.chains = chains_model
