# -*- coding: utf-8 -*-
# account_edit_tab_general.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Account editor tab general.

Edit general account properties
'''

import operator
from PySide.QtCore import (Qt, Signal)
from PySide.QtGui import QMessageBox, QWidget, QSizePolicy
from netsplice.config import constants
from netsplice.config import gui as config_gui
from netsplice.gui.account_edit import config
from netsplice.gui.account_edit.errors import BadAccountTypeError

from netsplice.gui.account_edit.account_edit_tab_general_ui import (
    Ui_AccountEditTabGeneral
)
from netsplice.gui.account_edit.type_editor_factory import (
    get_account_types, get_account_profiles
)

from netsplice.gui.account_edit.model.response.account_edit_item import (
    account_edit_item as account_edit_item_model
)
from netsplice.util.model.errors import ValidationError
from netsplice.util import basestring, escape_html, get_logger

logger = get_logger()

STACK_HINT_ENABLED = 0
STACK_HINT_DISABLED = 1


class account_edit_tab_general(QWidget):
    '''
    Widget for editing general account properties.

    '''
    config_changed = Signal()
    type_changed = Signal(basestring)
    account_profile_items_changed = Signal()
    account_profile_changed = Signal(object)
    change_account_profile_description = Signal()
    model_loaded_signal = Signal(object)

    def __init__(self, parent=None):
        '''
        Initialize Module.

        Initialize the dialog and member variables.
        '''
        QWidget.__init__(self, parent)
        self.mainwindow = None
        self.backend = None
        self.editor_proxy = None
        self.editor_model = account_edit_item_model()
        self.model = account_edit_item_model()

        self.ui = Ui_AccountEditTabGeneral()
        self.ui.setupUi(self)
        self.ui.general_tab_layout.setContentsMargins(10, 9, 10, 0)
        self.ui.general_tab_vert_layout.setContentsMargins(0, 0, 0, 0)
        self.ui.account_name_type_layout.setContentsMargins(0, 0, 0, 0)
        self.ui.account_name_label_widget_layout.setContentsMargins(0, 0, 0, 0)

        self.parent_group_id = None

        self._signals_connected = False
        self._custom_profile = {
            'label': self.tr('Custom'),
            'default': None,
            'description': None,
            'default_route': False,
            'process_launcher': [],
            'process_sniper': []
        }
        self._default_profile = {
            'label': self.tr('Default'),
            'default': None,
            'description': None,
            'default_route': False,
            'process_launcher': [],
            'process_sniper': []
        }
        self._default_description = None

        self.account_profile_changed.connect(
            self._account_profile_changed)
        self.account_profile_items_changed.connect(
            self._account_profile_items_changed)
        self.change_account_profile_description.connect(
            self._change_account_profile_description)

        self._init_ui()
        self._reset_widgets()

        self._default_route_changed(self.get_default_route())
        self._autostart_changed(self.get_autostart())
        self._enabled_changed(self.get_enabled())

    def _account_profile_changed(self, account_profile):
        '''
        Account Profile Changed

        Slot that is connected to the account_profile_changed signal.
        Change the default_route flag in the tab.

        Arguments:
            account_profile (dict): dict with account_profile
        '''
        if account_profile['default_route']:
            self.ui.basic_default_route_enabled.setCheckState(Qt.Checked)
        else:
            self.ui.basic_default_route_enabled.setCheckState(Qt.Unchecked)
        self._config_changed()

    def _account_profile_items_changed(self):
        '''
        Account profile items changed.

        Iterate all account profiles in the selectbox and find if the user
        has selected it for the current data.
        '''
        if self._signals_connected:
            self.ui.account_profile_select.currentIndexChanged.disconnect(
                self._change_account_profile)
        for index in range(0, self.ui.account_profile_select.count()):
            profile = self.ui.account_profile_select.itemData(index)
            try:
                if not self.editor_proxy.is_account_profile(profile):
                    continue
            except NotImplementedError:
                continue
            self.ui.account_profile_select.setCurrentIndex(index)
            if profile != self._custom_profile:
                break
        if self._signals_connected:
            self.ui.account_profile_select.currentIndexChanged.connect(
                self._change_account_profile)

    def _autostart_changed(self, new_state):
        '''
        Autostart Changed.

        Change hint according to the new state.
        '''
        self.editor_model.autostart.set(new_state)
        if new_state is 0 or new_state is False:
            self.ui.basic_autostart_hints.setCurrentIndex(
                STACK_HINT_DISABLED)
        else:
            self.ui.basic_autostart_hints.setCurrentIndex(
                STACK_HINT_ENABLED)
        self._config_changed()

    def _change_account_type(self):
        '''
        Change Account Type Handler.

        Try to load the selected account_type, fallback if required.
        '''
        self.change_account_profile_description.emit()
        account_type = self.get_type()
        if account_type is None:
            return
        if account_type == self.editor_proxy.account_type:
            return
        initialize = False
        if self.editor_proxy.account_type is None:
            initialize = True
        try:
            self.editor_proxy.set_account_type(
                account_type, self.backend)
        except BadAccountTypeError:
            self.editor_proxy.set_account_type(
                config.DEFAULT_ACCOUNT_TYPE, self.backend)

        self.editor_model.type.set(account_type)

        if self._signals_connected:
            self.ui.account_profile_select.currentIndexChanged.disconnect(
                self._change_account_profile)
        while self.ui.account_profile_select.count():
            self.ui.account_profile_select.removeItem(0)
        self.ui.account_profile_select.addItem(
            self._custom_profile['label'], self._custom_profile)
        self.ui.account_profile_select.addItem(
            self._default_profile['label'], self._default_profile)
        try:
            for profile_item in get_account_profiles(account_type):
                self.ui.account_profile_select.addItem(
                    self.tr(profile_item['label']), profile_item)
        except BadAccountTypeError:
            pass

        if self._signals_connected:
            self.ui.account_profile_select.currentIndexChanged.connect(
                self._change_account_profile)

        account_types = get_account_types()
        for registered_account_type in account_types:
            if registered_account_type['type'] != account_type:
                continue
            self._default_description = registered_account_type['description']
            break

        self.account_profile_items_changed.emit()

        self.editor_proxy.set_model(self.editor_model)
        if not initialize:
            message = self.tr(config_gui.QUESTION_ACCOUNT_TYPE_CHANGED)

            confirm = QMessageBox.question(
                None, constants.NS_NAME,
                message,
                QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
            try:
                if confirm == QMessageBox.Yes:
                    self.editor_proxy.set_account_profile(
                        self._default_profile)
                else:
                    self.editor_proxy.set_model(self.editor_model)
            except NotImplementedError:
                pass

            self.account_profile_items_changed.emit()
        self.change_account_profile_description.emit()
        self.type_changed.emit(account_type)

    def _change_account_profile(self):
        '''
        Change Account Profile Handler.

        Load the selected profile to the account configuration
        '''
        self.change_account_profile_description.emit()
        account_profile = self.ui.account_profile_select.itemData(
            self.ui.account_profile_select.currentIndex())
        if account_profile is None:
            return
        if account_profile == self._custom_profile:
            return
        try:
            self.editor_proxy.set_account_profile(account_profile)
            self.account_profile_changed.emit(account_profile)
        except BadAccountTypeError:
            self.editor_proxy.set_account_profile(
                self._default_profile)

    def _change_account_profile_description(self):
        '''
        Change Account Profile Handler.

        Load the selected profile profile description to the hints display.
        '''
        account_profile = self.ui.account_profile_select.itemData(
            self.ui.account_profile_select.currentIndex())
        if account_profile is None:
            return
        if account_profile == self._custom_profile:
            self.ui.basic_hints.hide()
            return
        if account_profile == self._default_profile:
            account_profile['description'] = self._default_description

        if account_profile['description'] is not None:
            self.ui.basic_hints.setText(
                account_profile['description'])
            self.ui.basic_hints.setOpenExternalLinks(True)
            self.ui.basic_hints.show()
        else:
            self.ui.basic_hints.hide()

    def _config_changed(self):
        '''
        Config Changed.

        Handle that a config value has been changed.
        '''
        self.config_changed.emit()

    def _connect_signals(self):
        '''
        Connect Signals.

        Connect local and widget signals to member functions.
        '''
        if self._signals_connected:
            return

        self.ui.basic_enabled.stateChanged.connect(
            self._enabled_changed)
        self.ui.basic_autostart_enabled.stateChanged.connect(
            self._autostart_changed)
        self.ui.basic_default_route_enabled.stateChanged.connect(
            self._default_route_changed)

        self.ui.account_name_edit.textChanged.connect(self._config_changed)

        self.ui.account_type_select.currentIndexChanged.connect(
            self._change_account_type)
        self.ui.account_profile_select.currentIndexChanged.connect(
            self._change_account_profile)
        self._signals_connected = True

    def _default_route_changed(self, new_state):
        '''
        Autostart Changed.

        Change hint according to the new state.
        '''
        self.editor_model.default_route.set(new_state)
        if new_state is 0 or new_state is False:
            self.ui.basic_default_route_hints.setCurrentIndex(
                STACK_HINT_DISABLED)
        else:
            self.ui.basic_default_route_hints.setCurrentIndex(
                STACK_HINT_ENABLED)
        self._config_changed()

    def _disconnect_signals(self):
        '''
        Disconnect Signals.

        Disconnect local and widget signals from member functions.
        '''
        if not self._signals_connected:
            return

        self.ui.basic_enabled.stateChanged.disconnect(
            self._enabled_changed)
        self.ui.basic_default_route_enabled.stateChanged.disconnect(
            self._default_route_changed)
        self.ui.basic_autostart_enabled.stateChanged.disconnect(
            self._autostart_changed)

        self.ui.account_name_edit.textChanged.disconnect(self._config_changed)

        self.ui.account_type_select.currentIndexChanged.disconnect(
            self._change_account_type)
        self.ui.account_profile_select.currentIndexChanged.disconnect(
            self._change_account_profile)
        self._signals_connected = False

    def _enabled_changed(self, new_state):
        '''
        Enabled Changed.

        Change hint according to the new state.
        '''
        self.editor_model.enabled.set(new_state)
        if new_state is 0 or new_state is False:
            self.ui.basic_enabled_hints.setCurrentIndex(
                STACK_HINT_DISABLED)
        else:
            self.ui.basic_enabled_hints.setCurrentIndex(
                STACK_HINT_ENABLED)
        self._config_changed()

    def get_autostart(self):
        '''
        Get Autostart.

        Return the state of the basic autostart checkbox.
        '''
        state = self.ui.basic_autostart_enabled.checkState()
        return state == Qt.Checked

    def _get_state_html_image(self, state):
        '''
        Create Image Tag.

        Create Image tag for the given state.
        '''
        return '<img src=":/images/state/%s.png" width="16"/>' % (state,)

    def _format_error(self, errors):
        '''
        Format Error.

        Formats a error message (from exceptions).
        '''
        formated_error = ''
        if str(errors):
            error_image = self._get_state_html_image('failed')
            formated_error = str(errors)
            formated_error = escape_html(formated_error)
            formated_error = formated_error.replace(
                '\n', '<br/>' + error_image)
            formated_error = error_image + formated_error

        return formated_error

    def _init_ui(self):
        '''
        Initialize UI.

        Connect the ui signals to handler functions.
        '''
        account_types = get_account_types()
        account_types.sort(key=operator.itemgetter('label'))
        for item in account_types:
            if item['type'] == 'Raw':
                continue
            self.ui.account_type_select.addItem(
                self.tr(item['label']), item['type'])
        self.ui.basic_hints.setSizePolicy(
            QSizePolicy.Expanding, QSizePolicy.MinimumExpanding)

    def _load_done(self, response):
        '''
        Load Done.

        Handles when loading a account has been returned from the backend.
        Used because a account needs to request editable attributes that are
        not available to the default account model.
        '''
        response_model = account_edit_item_model()
        response_model.from_json(response)
        self._load_model(response_model)

    def _load_failed(self, errors):
        '''
        Load failed.

        Handle when loading account attributes failed.
        '''
        self._show_error(
            self.tr('Load Account Failed'),
            errors)

    def _load_model(self, account_model_instance):
        '''
        Load Model.

        Load account from the given model instance.
        '''
        self.editor_model = account_model_instance
        self.model = account_edit_item_model()
        try:
            if account_model_instance.id.value is not None:
                self.model.from_json(account_model_instance.to_json())
        except ValidationError:
            pass

        self.ui.account_name_edit.setText(
            self.editor_model.name.get())

        if self._signals_connected:
            self.ui.account_type_select.currentIndexChanged.disconnect(
                self._change_account_type)
        self.ui.account_type_select.setCurrentIndex(
            self.ui.account_type_select.findData(
                self.editor_model.type.get()))
        if self._signals_connected:
            self.ui.account_type_select.currentIndexChanged.connect(
                self._change_account_type)
        self.editor_proxy.account_type = None
        self.account_type = None
        enabled = self.editor_model.enabled.get()
        default_route = self.editor_model.default_route.get()
        autostart = self.editor_model.autostart.get()
        if enabled:
            self.ui.basic_enabled.setCheckState(Qt.Checked)
        else:
            self.ui.basic_enabled.setCheckState(Qt.Unchecked)

        if default_route:
            self.ui.basic_default_route_enabled.setCheckState(Qt.Checked)
        else:
            self.ui.basic_default_route_enabled.setCheckState(Qt.Unchecked)

        if autostart:
            self.ui.basic_autostart_enabled.setCheckState(Qt.Checked)
        else:
            self.ui.basic_autostart_enabled.setCheckState(Qt.Unchecked)
        self._enabled_changed(enabled)
        self._default_route_changed(default_route)
        self._autostart_changed(autostart)
        self._change_account_type()
        self.model_loaded_signal.emit(account_model_instance)

    def _reset_status(self):
        '''
        Reset Status.

        Reset the status output to empty and hide the QLabels.
        '''
        pass

    def _reset_widgets(self):
        '''
        Reset Widgets.

        Reset the account widgets to default values.
        '''
        self.editor_model = account_edit_item_model()
        self.model = account_edit_item_model()

        self.ui.account_name_edit.setText('')
        if self._signals_connected:
            self.ui.account_type_select.currentIndexChanged.disconnect(
                self._change_account_type)
        self.ui.account_type_select.setCurrentIndex(
            self.ui.account_type_select.findData(config.DEFAULT_ACCOUNT_TYPE))
        if self._signals_connected:
            self.ui.account_type_select.currentIndexChanged.connect(
                self._change_account_type)
        self.ui.basic_enabled.setCheckState(Qt.Checked)
        self.ui.basic_autostart_enabled.setCheckState(Qt.Unchecked)
        self.ui.basic_default_route_enabled.setCheckState(Qt.Unchecked)
        self._reset_status()
        self._config_changed()

    def _set_readonly(self, state):
        '''
        Set Readonly.

        Set the account_edit dialog readonly while a connection is active.
        '''
        self.ui.account_name_edit.setEnabled(not state)
        self.ui.account_type_select.setEnabled(not state)
        self.ui.account_profile_select.setEnabled(not state)
        self.ui.basic_enabled.setEnabled(not state)
        self.ui.basic_default_route_enabled.setEnabled(not state)
        self.ui.basic_autostart_enabled.setEnabled(not state)
        self.editor_proxy.setEnabled(not state)
        # update commit / create button
        self._config_changed()

    def _show_error(self, message, errors):
        '''
        Show Error.

        Helper to display error messages alongside with exeption errors
        '''
        formated_error = message
        formated_error = '<br/>'
        formated_error += self._format_error(errors)

        self.ui.user_errors.setText(formated_error)
        self.ui.user_errors.setVisible(True)

    def hideEvent(self, event):
        '''
        Hide Event.

        Disconnect signals from widgets.
        '''
        self._disconnect_signals()

    def showEvent(self, event):
        '''
        Show Event.

        Connect signals to backend and widgets.
        '''
        self._connect_signals()

    def can_commit(self):
        '''
        Can Commit.

        The widget is complete to commit.
        '''
        has_name = self.ui.account_name_edit.text() != ''
        return has_name and self.isEnabled()

    def change_account_type(self):
        '''
        Change account type.

        Switch the selectbox and the associated widgets.
        '''
        self._change_account_type()

    def config_values_changed(self):
        '''
        Config Values Changed.

        Evaluate if the config is different from the loaded values.
        '''
        changed = False
        changed |= self.get_name() != self.model.name.get()
        changed |= self.get_type() != self.model.type.get()
        # profile is not evaluated as it sets the values in the type related
        # tabs that will return changed or not.

        changed |= self.get_enabled() != self.model.enabled.get()
        changed |= self.get_default_route() != self.model.default_route.get()
        changed |= self.get_autostart() != self.model.autostart.get()
        return changed

    def get_default_route(self):
        '''
        Get Default Route.

        Return the state of the basic default route checkbox.
        '''
        state = self.ui.basic_default_route_enabled.checkState()
        return state == Qt.Checked

    def get_default_profile(self):
        '''
        Get Default Profile.

        Return the default profile.
        '''
        return self._default_profile

    def get_enabled(self):
        '''
        Get Enabled.

        Return the state of the basic enabled checkbox.
        '''
        state = self.ui.basic_enabled.checkState()
        return state == Qt.Checked

    def get_name(self):
        '''
        Get Name.

        Return the name of the account.
        '''
        return self.ui.account_name_edit.text()

    def get_type(self):
        '''
        Get Type.

        Return the current selected account type.
        '''
        return self.ui.account_type_select.itemData(
            self.ui.account_type_select.currentIndex())

    def load(self, account_id):
        '''
        Load existing Account.

        Configures the dialog to edit a existing account identified by the
        given account_id. Hide Create button and show Commit button.
        '''
        self._creating = False
        self.ui.button_commit.setVisible(True)
        self.ui.button_create.setVisible(False)
        self._reset_widgets()
        self.backend.account_get_for_edit.emit(
            account_id,
            self.load_done_signal,
            self.load_failed_signal)

    def reset(self):
        '''
        Reset the Dialog.

        Forget all user-entered and loaded data.
        '''
        self._reset_widgets()

    def set_mainwindow(self, mainwindow):
        '''
        Set Mainwindow.

        Set the mainwindow widget to access the backend.

        Arguments:
            mainwindow ([type]): [description]
        '''
        self.mainwindow = mainwindow
        if self.mainwindow is None:
            return
        self.backend = mainwindow.backend
        self._connect_signals()

    def set_editor_proxy(self, editor_proxy):
        '''
        Set Editor Proxy.

        Set the widget used to edit the type related properties.

        Arguments:
            editor_proxy (account_type_editor_proxy): widget
        '''
        self.editor_proxy = editor_proxy

    def set_model(self, model):
        '''
        Set Model.

        Set the model for the tab.

        Arguments:
            model (model.account_item): model of account currently edited.
        '''
        self._load_model(model)
