# -*- coding: utf-8 -*-
# account_edit_tab_chain.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Account editor tab for chain.

Edit account chain properties.
'''

from PySide.QtCore import Signal
from PySide.QtGui import QWidget
from netsplice.config import backend as config_backend
from netsplice.config import gui as config_gui
from netsplice.config.backend import (
    FAILURE_MODE_BREAK, FAILURE_MODE_DISCONNECT, FAILURE_MODE_RECONNECT,
    FAILURE_MODE_IGNORE, CONNECT_MODE_SEQUENCE, CONNECT_MODE_PARALLEL,
    CONNECT_MODE_NONE
)

from netsplice.gui.account_edit.account_edit_tab_chain_ui import (
    Ui_AccountEditTabChain
)

from netsplice.gui.account_edit.model.response.account_edit_item import (
    account_edit_item as account_edit_item_model
)
from netsplice.gui.model.chain_list_item import (
    chain_list_item as chain_list_item_model
)
from netsplice.util.errors import NotFoundError
from netsplice.util import basestring, escape_html, get_logger

logger = get_logger()

STACK_HINT_ENABLED = 0
STACK_HINT_DISABLED = 1


class account_edit_tab_chain(QWidget):
    '''
    Widget for editing chain related account properties.
    '''
    config_changed = Signal()
    model_stored_signal = Signal(basestring)
    store_done = Signal(basestring)
    store_failed = Signal(basestring)
    profile_changed = Signal(object)

    def __init__(self, parent=None):
        '''
        Initialize Module.

        Initialize the dialog and member variables.
        '''
        QWidget.__init__(self, parent)
        self.mainwindow = None
        self.backend = None
        self._signals_connected = False
        self._get_name_callback = None
        self.ui = Ui_AccountEditTabChain()
        self.ui.setupUi(self)
        self.ui.chain_grid_widget.setContentsMargins(0, 10, 0, 0)

        self.editor_model = account_edit_item_model()
        self.model = chain_list_item_model()

        self._init_ui()
        self._reset_widgets()
        self.model_stored_signal.connect(self._store_chain)

    def _config_changed(self):
        '''
        Config Changed.

        Handle that a config value has been changed.
        '''
        self._update_text()
        self.config_changed.emit()

    def _connect_mode_changed(self):
        '''
        Connect Mode Changed.

        The user selected a different connect_mode.
        '''
        self._config_changed()

    def _connect_signals(self):
        '''
        Connect Signals.

        Connect local and widget signals to member functions.
        '''
        if self._signals_connected:
            return
        if self.backend is None:
            return
        self.backend.application.event_loop.accounts_changed.connect(
            self.update_model)
        self.ui.select_parent.currentIndexChanged.connect(
            self._parent_changed)
        self.ui.select_connect_mode.currentIndexChanged.connect(
            self._connect_mode_changed)
        self.ui.select_failure_mode.currentIndexChanged.connect(
            self._failure_mode_changed)
        self._signals_connected = True

    def _disconnect_signals(self):
        '''
        Disconnect Signals.

        Disconnect local and widget signals from member functions.
        '''
        if not self._signals_connected:
            return
        if self.backend is None:
            return
        self.backend.application.event_loop.accounts_changed.disconnect(
            self.update_model)
        self.ui.select_parent.currentIndexChanged.disconnect(
            self._parent_changed)
        self.ui.select_connect_mode.currentIndexChanged.disconnect(
            self._connect_mode_changed)
        self.ui.select_failure_mode.currentIndexChanged.disconnect(
            self._failure_mode_changed)

        self._signals_connected = False

    def _failure_mode_changed(self):
        '''
        Failure Mode Changed.

        The user selected a different failure_mode.
        '''
        self._config_changed()

    def _get_connect_mode(self):
        '''
        Get connect_mode.

        Return the current selected connect mode.
        '''
        return self.ui.select_connect_mode.itemData(
            self.ui.select_connect_mode.currentIndex())

    def _get_failure_mode(self):
        '''
        Get failure_mode.

        Return the current selected failure mode.
        '''
        return self.ui.select_failure_mode.itemData(
            self.ui.select_failure_mode.currentIndex())

    def _get_model_id(self):
        '''
        Get Model Id.

        Return the model_id from the current set model or editor_model.

        Returns:
            (string): None when no id was set (create mode)
        '''
        model_id = None
        if self.model.id.value is not None:
            model_id = self.model.id.get()
        elif self.editor_model.id.value is not None:
            model_id = self.editor_model.id.get()
        return model_id

    def _get_model_parent_id(self):
        '''
        Get Model Parent Id.

        Return the parent id of the model chain instance.

        Returns:
            (string): None when no id was set (create mode)
        '''
        model_id = self._get_model_id()
        parent_id = None
        if model_id is None:
            return None
        try:
            chain = self.chains.find_by_id(model_id)
            if chain.parent_id.get() is not None:
                parent_account = self.accounts.find_by_id(
                    chain.parent_id.get())
                parent_id = parent_account.id.get()
        except NotFoundError:
            pass
        return parent_id

    def _get_parent_id(self):
        '''
        Get parent_id.

        Return the current selected parent_id.
        '''
        return self.ui.select_parent.itemData(
            self.ui.select_parent.currentIndex())

    def _init_ui(self):
        '''
        Initialize UI.

        Connect the ui signals to handler functions.
        '''
        self.ui.chain_introduction.setText(
            self.tr(config_gui.CHAIN_INTRODUCTION_TEXT))

    def _parent_changed(self):
        '''
        Parent Changed.

        Parent has changed in combo box.
        '''
        self._config_changed()

    def _reset_status(self):
        '''
        Reset Status.

        Reset the status output to empty and hide the QLabels.
        '''
        pass

    def _reset_widgets(self):
        '''
        Reset Widgets.

        Reset the account widgets to default values.
        '''
        self._reset_status()
        self._config_changed()

    def _set_readonly(self, state):
        '''
        Set Readonly.

        Set the account_edit dialog readonly while a connection is active.
        '''
        self.ui.select_connect_mode.setReadOnly(state)
        self.ui.select_failure_mode.setReadOnly(state)
        self.ui.select_parent.setReadOnly(state)

    def _sort_key_name(self, dict_item):
        '''
        Sort Key Name.

        Return the value for the name field in the dict_item.

        Arguments:
            dict_item (dict): dict with a name field.
        '''
        return dict_item['name']

    def _store_chain(self, account_id):
        '''
        Store Chain.

        Handler for the account_edit dialog store event. This occurs when the
        account is created/updated shortly before the dialog is closed.
        The indices of the comboboxes are not available anymore.

        Arguments:
            account_id (string): account_id
        '''
        parent_id = self._get_parent_id()
        if parent_id is None:
            parent_id = ''
        weight = self.model.weight.get()
        collapsed = False
        connect_mode = self._get_connect_mode()
        failure_mode = self._get_failure_mode()
        self.backend.chain_update.emit(
            parent_id, account_id, weight,
            connect_mode, failure_mode, collapsed,
            self.store_done, self.store_failed)

    def _update_connect_modes(self):
        '''
        Update Connect Modes.

        Update the connect modes combo box.
        '''
        if self._signals_connected:
            self.ui.select_connect_mode.currentIndexChanged.disconnect(
                self._connect_mode_changed)

        while self.ui.select_connect_mode.count():
            self.ui.select_connect_mode.removeItem(0)

        for connect_mode in config_backend.CONNECT_MODES:
            self.ui.select_connect_mode.addItem(
                self.tr(config_gui.CHAIN_CONNECT_MODE_NAMES[connect_mode]),
                connect_mode)
        self.ui.select_connect_mode.setCurrentIndex(
            self.ui.select_connect_mode.findData(
                self.model.connect_mode.get()))

        if self._signals_connected:
            self.ui.select_connect_mode.currentIndexChanged.connect(
                self._connect_mode_changed)

    def _update_failure_modes(self):
        '''
        Update Failure Modes.

        Update the failure modes combo box.
        '''
        if self._signals_connected:
            self.ui.select_failure_mode.currentIndexChanged.disconnect(
                self._failure_mode_changed)

        while self.ui.select_failure_mode.count():
            self.ui.select_failure_mode.removeItem(0)
        for failure_mode in config_backend.FAILURE_MODES:
            self.ui.select_failure_mode.addItem(
                self.tr(config_gui.CHAIN_FAILURE_MODE_NAMES[failure_mode]),
                failure_mode)
        self.ui.select_failure_mode.setCurrentIndex(
            self.ui.select_failure_mode.findData(
                self.model.failure_mode.get()))

        if self._signals_connected:
            self.ui.select_failure_mode.currentIndexChanged.connect(
                self._failure_mode_changed)

    def _update_parent_accounts(self):
        '''
        Update Parent Accounts.

        Update the combobox with possible parents, excluding self.
        '''
        parent_id = self._get_model_parent_id()
        model_id = self._get_model_id()

        if self._signals_connected:
            self.ui.select_parent.currentIndexChanged.disconnect(
                self._parent_changed)

        while self.ui.select_parent.count():
            self.ui.select_parent.removeItem(0)
        self.ui.select_parent.addItem(
            self.tr('No Parent'), None)
        sortable_accounts = list()
        for account in self.accounts:
            if account.id.get() == model_id:
                # do not add self
                continue
            sortable_accounts.append({
                'name': account.name.get(),
                'id': account.id.get()})
        sorted_accounts = sorted(sortable_accounts, key=self._sort_key_name)
        for account in sorted_accounts:
            self.ui.select_parent.addItem(
                account['name'], account['id'])

        self.ui.select_parent.setCurrentIndex(
            self.ui.select_parent.findData(parent_id))

        if self._signals_connected:
            self.ui.select_parent.currentIndexChanged.connect(
                self._parent_changed)

    def _update_text(self):
        '''
        Update Text.

        Update summary text according to the current chain configuration.
        '''
        connect_mode = self._get_connect_mode()
        failure_mode = self._get_failure_mode()
        account_name = escape_html(self.editor_model.name.get())
        if self._get_name_callback is not None:
            account_name = escape_html(self._get_name_callback())
        parent_account_name = self.tr('No Parent')
        chain_children_list = self.tr('No Children')
        try:
            if self.model.id.value is None:
                raise NotFoundError('')
            if self._get_parent_id() is not None:
                parent_account = self.accounts.find_by_id(
                    self._get_parent_id())
                parent_account_name = escape_html(parent_account.name.get())
            chain_children = self.chains.get_children(self.model.id.get())
            if len(chain_children) > 0:
                chain_children_list = ''
                for child in chain_children:
                    child_account = self.accounts.find_by_id(child.id.get())
                    child_account_name = escape_html(child_account.name.get())
                    if chain_children_list != '':
                        if chain_children[-1] == child:
                            chain_children_list += ' ' + self.tr('and') + ' '
                        else:
                            chain_children_list += ', '
                    chain_children_list += child_account_name
        except NotFoundError:
            # not yet a instance in backend
            pass

        if connect_mode == CONNECT_MODE_SEQUENCE:
            summary = self.tr(config_gui.CHAIN_SUMMARY_TEXT_SEQUENCE)
            self.ui.connect_mode_summary.setText(summary.format(
                account_name=account_name,
                parent_account_name=parent_account_name,
                chain_children_list=chain_children_list))
        if connect_mode == CONNECT_MODE_PARALLEL:
            summary = self.tr(config_gui.CHAIN_SUMMARY_TEXT_PARALLEL)
            self.ui.connect_mode_summary.setText(summary.format(
                account_name=account_name,
                parent_account_name=parent_account_name,
                chain_children_list=chain_children_list))
        if connect_mode == CONNECT_MODE_NONE:
            summary = self.tr(config_gui.CHAIN_SUMMARY_TEXT_NONE)
            self.ui.connect_mode_summary.setText(summary.format(
                account_name=account_name,
                parent_account_name=parent_account_name,
                chain_children_list=chain_children_list))
        if failure_mode == FAILURE_MODE_BREAK:
            summary = self.tr(config_gui.CHAIN_SUMMARY_TEXT_BREAK)
            self.ui.failure_mode_summary.setText(summary.format(
                account_name=account_name,
                parent_account_name=parent_account_name,
                chain_children_list=chain_children_list))
        if failure_mode == FAILURE_MODE_DISCONNECT:
            summary = self.tr(config_gui.CHAIN_SUMMARY_TEXT_DISCONNECT)
            self.ui.failure_mode_summary.setText(summary.format(
                account_name=account_name,
                parent_account_name=parent_account_name,
                chain_children_list=chain_children_list))
        if failure_mode == FAILURE_MODE_RECONNECT:
            summary = self.tr(config_gui.CHAIN_SUMMARY_TEXT_RECONNECT)
            self.ui.failure_mode_summary.setText(summary.format(
                account_name=account_name,
                parent_account_name=parent_account_name,
                chain_children_list=chain_children_list))
        if failure_mode == FAILURE_MODE_IGNORE:
            summary = self.tr(config_gui.CHAIN_SUMMARY_TEXT_IGNORE)
            self.ui.failure_mode_summary.setText(summary.format(
                account_name=account_name,
                parent_account_name=parent_account_name,
                chain_children_list=chain_children_list))
        summary = self.tr(config_gui.CHAIN_SUMMARY_TEXT)
        self.ui.parent_summary.setText(summary.format(
            account_name=account_name,
            parent_account_name=parent_account_name,
            chain_children_list=chain_children_list))
        summary = self.tr(config_gui.CHAIN_PARENT_DESCRIPTION)
        self.ui.parent_description.setText(summary.format(
            account_name=account_name,
            parent_account_name=parent_account_name,
            chain_children_list=chain_children_list))
        summary = self.tr(config_gui.CHAIN_CONNECT_MODE_DESCRIPTION)
        self.ui.connect_mode_description.setText(summary.format(
            account_name=account_name,
            parent_account_name=parent_account_name,
            chain_children_list=chain_children_list))
        summary = self.tr(config_gui.CHAIN_FAILURE_MODE_DESCRIPTION)
        self.ui.failure_mode_description.setText(summary.format(
            account_name=account_name,
            parent_account_name=parent_account_name,
            chain_children_list=chain_children_list))

    def hideEvent(self, event):
        '''
        Hide Event.

        Disconnect signals from widgets.
        '''
        self._disconnect_signals()

    def showEvent(self, event):
        '''
        Show Event.

        Connect signals to backend and widgets.
        '''
        self._connect_signals()
        self._update_text()

    def can_commit(self):
        '''
        Can Commit.

        The widget is complete to commit.
        '''
        return self.isEnabled()

    def config_values_changed(self):
        '''
        Config Values Changed.

        Evaluate if the config is different from the loaded values.
        '''
        changed = False
        changed |= self._get_parent_id() != self.model.parent_id.get()
        changed |= self._get_connect_mode() != self.model.connect_mode.get()
        changed |= self._get_failure_mode() != self.model.failure_mode.get()
        return changed

    def reset(self):
        '''
        Reset the Dialog.

        Forget all user-entered and loaded data.
        '''
        self._reset_widgets()

    def set_get_name(self, get_name_callback):
        '''
        Set Get Name.

        Set Callback to get the name for the current account.

        Arguments:
            get_name_callback -- function pointer
        '''
        self._get_name_callback = get_name_callback

    def set_mainwindow(self, mainwindow):
        '''
        Set Mainwindow.

        Set the mainwindow widget to access the backend.

        Arguments:
            mainwindow ([type]): [description]
        '''
        self.mainwindow = mainwindow
        if self.mainwindow is None:
            return
        self.backend = mainwindow.backend
        self._connect_signals()
        preferences_model = self.backend.application.get_model().preferences
        self.update_model(
            preferences_model.accounts, preferences_model.groups,
            preferences_model.grouped_accounts, preferences_model.chains)

    def set_model(self, model):
        '''
        Set Model.

        Set the model for the tab.

        Arguments:
            model (model.account_item): model of account currently edited.
        '''
        self.editor_model = model
        try:
            if model.id.value is not None:
                self.model = self.chains.find_by_id(model.id.get())
            self._update_text()
            self._update_parent_accounts()
            self._update_connect_modes()
            self._update_failure_modes()
        except NotFoundError:
            pass

    def update_model(
            self, accounts_model, groups_model,
            grouped_accounts_model, chains_model):
        '''
        Update model.

        Update the values of the account based on the latest model changes.
        '''
        self.accounts = accounts_model
        self.groups = groups_model
        self.grouped_accounts = grouped_accounts_model
        self.chains = chains_model

        self._update_parent_accounts()
        if self.model is None:
            return
        try:
            model_id = self._get_model_id()
            if model_id is None:
                raise NotFoundError('')
            self.model = self.chains.find_by_id(model_id)
            self._update_text()
        except NotFoundError:
            pass
