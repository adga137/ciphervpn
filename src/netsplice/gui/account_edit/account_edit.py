# -*- coding: utf-8 -*-
# account_edit.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Account editor.

Edit account settings with a specialized editor and manage general actions.
'''

from PySide.QtCore import (Signal)
from PySide.QtGui import QMessageBox
from netsplice.config import gui as config_gui
from netsplice.config import constants
from netsplice.gui.widgets.dialog import dialog

from netsplice.gui.account_edit.account_edit_ui import Ui_AccountEdit

from netsplice.gui.account_edit.model.response.account_edit_item import (
    account_edit_item as account_edit_item_model
)
from netsplice.gui.account_edit.type_editor_factory import get_account_plugins
from netsplice.gui.model.request.account_id import (
    account_id as account_id_model
)
from netsplice.util import basestring, escape_html, get_logger

logger = get_logger()

STACK_HINT_ENABLED = 0
STACK_HINT_DISABLED = 1


class account_edit(dialog):
    '''
    Custom Widget for editing and creating Accounts.

    Defines the basic functionality and uses tab_editor to
    proxy the specific functions.
    '''

    account_disconnect_done_signal = Signal(object)
    apply_done_signal = Signal(object)
    apply_done_reconnect_signal = Signal(object)
    apply_failed_signal = Signal(object)
    backend_done_signal = Signal(object)
    config_changed = Signal()
    create_done_signal = Signal(object)
    create_failed_signal = Signal(object)
    create_regroup_signal = Signal(object)
    load_done_signal = Signal(object)
    load_failed_signal = Signal(object)
    model_loaded_signal = Signal(object)
    model_stored_signal = Signal(basestring)
    model_reset_signal = Signal()

    def __init__(self, parent=None, dispatcher=None):
        '''
        Initialize Module.

        Initialize the dialog and member variables.
        '''
        dialog.__init__(self, parent)
        self.mainwindow = parent
        self.backend = dispatcher
        self.ui = Ui_AccountEdit()
        self.ui.setupUi(self)
        self.ui.tab_general.set_mainwindow(self.mainwindow)
        self.ui.tab_chain.set_mainwindow(self.mainwindow)
        self.ui.tab_chain.set_get_name(lambda: self.ui.tab_general.get_name())
        self.ui.tab_general.set_editor_proxy(self.ui.tab_editor)

        self.editor_model = account_edit_item_model()
        self.parent_group_id = None
        self._creating = False
        self._plugin_tabs = []
        self._signals_connected = False
        self._reconnect_account_id = None
        self._init_ui()
        self._connect_signals()
        self.reset()

    def _account_profile_changed(self, account_profile):
        '''
        Account Profile Changed.

        The account profile has been changed. Notify the plugin tabs about
        the change.

        Arguments:
            account_profile (dict): new account profile
        '''
        for tab in self._plugin_tabs:
            try:
                tab.account_profile_changed.emit(account_profile)
            except AttributeError:
                pass

    def _apply(self):
        '''
        Apply Handler.

        Call tab_editor to save a previously loaded account.
        '''
        self.ui.button_apply.setEnabled(False)
        self.ui.button_apply_reconnect.setEnabled(False)
        self.ui.button_create.setEnabled(False)
        self._reset_status()

        account_id = self.editor_model.id.get()
        account_name = self.ui.tab_general.get_name()
        autostart = self.ui.tab_general.get_autostart()
        enabled = self.ui.tab_general.get_enabled()
        default_route = self.ui.tab_general.get_default_route()
        self.ui.tab_editor.apply(
            account_id, account_name, enabled, default_route, autostart,
            self.apply_done_signal, self.apply_failed_signal)

    def _apply_done(self, response):
        '''
        Apply done.

        Handle when updating an existing account has completed.
        '''
        account_id = account_id_model()
        account_id.from_json(response)
        self.model_stored_signal.emit(account_id.id.get())

    def _apply_done_reconnect(self, response):
        '''
        Update done.

        Handle when updating an existing account has completed.
        Call the backend to disconnect and reconnect the account.
        '''
        account_id = account_id_model()
        account_id.from_json(response)
        self.model_stored_signal.emit(account_id.id.get())
        # disconnect/connect to handle type changes
        self.backend.account_disconnect.emit(
            self._reconnect_account_id,
            self.account_disconnect_done_signal, self.apply_failed_signal)

    def _apply_done_connect(self, response):
        '''
        Update done.

        Handle when updating an existing account has completed. Closes the
        dialog window.
        '''
        self.backend.account_connect.emit(
            self._reconnect_account_id,
            self.backend_done_signal, self.apply_failed_signal)
        self._reconnect_account_id = None

    def _apply_failed(self, errors):
        '''
        Apply failed.

        Handle when updating an existing account has failed. Display the
        error.
        '''
        self._show_error(
            self.tr('apply failed'),
            errors)
        self._reconnect_account_id = None

    def _apply_reconnect(self):
        '''
        Apply Reconnect Handler.

        Call apply and reconnect afterwards.
        '''
        self.ui.button_apply.setEnabled(False)
        self.ui.button_apply_reconnect.setEnabled(False)
        self.ui.button_create.setEnabled(False)
        self._reset_status()

        account_id = self.editor_model.id.get()
        account_name = self.ui.tab_general.get_name()
        autostart = self.ui.tab_general.get_autostart()
        enabled = self.ui.tab_general.get_enabled()
        default_route = self.ui.tab_general.get_default_route()
        self._reconnect_account_id = account_id
        self.ui.tab_editor.apply(
            account_id, account_name, enabled, default_route, autostart,
            self.apply_done_reconnect_signal, self.apply_failed_signal)

    def _backend_model_changed(self, model):
        '''
        Model Change Handler.

        Handle when the backend model has changed. Used to enable/disable
        the apply button based on the active connection list.
        '''
        self._set_readonly(False)
        self.ui.button_apply_reconnect.setVisible(False)
        if self.editor_model is None:
            return
        if self.editor_model.id.value is None:
            return
        editor_account_id = self.editor_model.id.get()
        for connection in model.connections:
            connection_account_id = connection.account_id.get()
            if connection_account_id != editor_account_id:
                continue
            if connection.is_connecting() or connection.is_connected():
                self.ui.button_apply_reconnect.setVisible(True)

    def _backend_done(self, response):
        '''
        Backend Done.

        Handle that the backend has successfully completed a task that
        does not need more attention.
        Usually this signal is followed by a backend_model_changed.
        '''
        pass

    def _cancel(self):
        '''
        Cancel Handler.

        Reset the control and close the window.
        '''
        self.reset()
        self.mainwindow.accounts_activity()
        self.model_reset_signal.emit()
        self.hide()

    def _close(self):
        '''
        Close Handler.

        Reset the control and close the window. Check if unapplied values
        exist and request if closing is ok then
        '''
        if self.config_values_changed():
            confirm = QMessageBox.question(
                None, constants.NS_NAME,
                config_gui.QUESTION_ACCOUNT_CHANGED,
                QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if confirm == QMessageBox.No:
                return
        self.reset()
        self.mainwindow.accounts_activity()
        self.model_reset_signal.emit()
        self.hide()

    def _change_account_type(self, account_type):
        '''
        Change Account Type Handler.

        Load the tabs for the account type.
        '''
        for tab in self._plugin_tabs:
            try:
                if not callable(tab.visible_for_account_type):
                    continue
                if tab.visible_for_account_type(account_type):
                    self.ui.account_edit_tabs.addTab(tab, tab.tab_label)
                else:
                    self.ui.account_edit_tabs.removeTab(
                        self.ui.account_edit_tabs.indexOf(tab))
            except AttributeError:
                pass

    def _config_changed(self):
        '''
        Config Changed.

        Handle that a config value has been changed.
        '''
        self.config_changed.emit()
        has_general = self.ui.tab_general.can_commit()
        has_chain = self.ui.tab_chain.can_commit()
        has_type = self.ui.tab_editor.can_commit()

        self.ui.button_apply.setEnabled(False)
        self.ui.button_apply_reconnect.setEnabled(False)
        self.ui.button_create.setEnabled(False)

        if has_general & has_chain & has_type:
            changed = self.config_values_changed()
            self.ui.button_apply.setEnabled(changed)
            self.ui.button_apply_reconnect.setEnabled(changed)
            self.ui.button_create.setEnabled(True)

            if self.ui.button_apply.isVisible():
                self.ui.button_apply.setDefault(True)
            else:
                self.ui.button_create.setDefault(True)

    def _connect_signals(self):
        '''
        Connect Signals.

        Connect local and widget signals to member functions.
        '''
        if self._signals_connected:
            return
        self.backend.application.event_loop.gui_model_changed.connect(
            self._backend_model_changed)

        self.create_done_signal.connect(self._create_done)
        self.create_failed_signal.connect(self._create_failed)
        self.create_regroup_signal.connect(self._create_regroup)

        self.load_done_signal.connect(self._load_done)
        self.load_failed_signal.connect(self._load_failed)

        self.apply_done_signal.connect(self._apply_done)
        self.apply_done_reconnect_signal.connect(self._apply_done_reconnect)
        self.account_disconnect_done_signal.connect(self._apply_done_connect)
        self.apply_failed_signal.connect(self._apply_failed)
        self.backend_done_signal.connect(self._backend_done)

        self.ui.button_apply.clicked.connect(self._apply)
        self.ui.button_apply_reconnect.clicked.connect(self._apply_reconnect)
        self.ui.button_cancel.clicked.connect(self._cancel)
        self.ui.button_close.clicked.connect(self._close)
        self.ui.button_create.clicked.connect(self._create)
        self.ui.tab_editor.config_changed.connect(
            self._config_changed)
        self.ui.tab_editor.config_changed.connect(
            self.ui.tab_general.account_profile_items_changed)

        self.ui.tab_chain.config_changed.connect(
            self._config_changed)
        self.ui.tab_general.account_profile_changed.connect(
            self._account_profile_changed)
        self.ui.tab_general.config_changed.connect(
            self._config_changed)
        self.ui.tab_general.type_changed.connect(
            self._change_account_type)
        self.model_stored_signal.connect(
            self.ui.tab_chain.model_stored_signal)

        for plugin_item in self._plugin_tabs:
            try:
                plugin_item.config_changed.connect(self._config_changed)
            except AttributeError:
                pass

        self._signals_connected = True

    def _create(self):
        '''
        Create Handler.

        Create a new account with the current configuration.
        '''
        self.ui.button_apply.setEnabled(False)
        self.ui.button_create.setEnabled(False)
        self._reset_status()

        account_name = self.ui.tab_general.get_name()
        autostart = self.ui.tab_general.get_autostart()
        enabled = self.ui.tab_general.get_enabled()
        default_route = self.ui.tab_general.get_default_route()
        finish_signal = self.create_done_signal

        if self.parent_group_id is not None:
            finish_signal = self.create_regroup_signal

        self.ui.tab_editor.create(
            account_name, enabled, default_route, autostart,
            self.parent_group_id,
            finish_signal,
            self.create_failed_signal)

    def _create_failed(self, errors):
        '''
        Create Failed.

        Handle when create account returned with a error.
        '''
        self._show_error(
            self.tr('create failed'),
            errors)

    def _create_done(self, response):
        '''
        Create Done.

        Handle when create account has been successfully completed. Close the
        Window.
        '''
        account_id = account_id_model()
        account_id.from_json(response)
        self.model_stored_signal.emit(account_id.id.get())

        self.reset()
        self.ui.button_apply.setEnabled(True)
        self.mainwindow.accounts_activity()
        self.hide()

    def _create_regroup(self, response):
        '''
        Create Regroup.

        Create account done, regroup the account.
        '''
        account_id = account_id_model()
        account_id.from_json(response)
        self.backend.grouped_account_update.emit(
            account_id.id.get(), self.parent_group_id, 0,
            self.create_done_signal, self.create_failed_signal)

    def _disconnect_signals(self):
        '''
        Disconnect Signals.

        Disconnect local and widget signals from member functions.
        '''
        if not self._signals_connected:
            return
        self.backend.application.event_loop.gui_model_changed.disconnect(
            self._backend_model_changed)

        self.create_done_signal.disconnect(self._create_done)
        self.create_failed_signal.disconnect(self._create_failed)

        self.load_done_signal.disconnect(self._load_done)
        self.load_failed_signal.disconnect(self._load_failed)

        self.apply_done_signal.disconnect(self._apply_done)
        self.apply_done_reconnect_signal.disconnect(
            self._apply_done_reconnect)
        self.account_disconnect_done_signal.disconnect(
            self._apply_done_connect)
        self.apply_failed_signal.disconnect(self._apply_failed)
        self.backend_done_signal.connect(self._backend_done)

        self.ui.button_apply.clicked.disconnect(self._apply)
        self.ui.button_apply_reconnect.clicked.disconnect(
            self._apply_reconnect)
        self.ui.button_cancel.clicked.disconnect(self._cancel)
        self.ui.button_close.clicked.disconnect(self._close)
        self.ui.button_create.clicked.disconnect(self._create)
        self.ui.tab_editor.config_changed.disconnect(
            self._config_changed)
        self.ui.tab_editor.config_changed.disconnect(
            self.ui.tab_general.account_profile_items_changed)
        self.ui.tab_chain.config_changed.disconnect(
            self._config_changed)
        self.ui.tab_general.account_profile_changed.disconnect(
            self._account_profile_changed)
        self.ui.tab_general.config_changed.disconnect(
            self._config_changed)
        self.ui.tab_general.type_changed.disconnect(
            self._change_account_type)
        self.model_stored_signal.disconnect(
            self.ui.tab_chain.model_stored_signal)

        for plugin_item in self._plugin_tabs:
            try:
                plugin_item.config_changed.disconnect(self._config_changed)
            except AttributeError:
                pass

        self._signals_connected = False

    def _get_state_html_image(self, state):
        '''
        Create Image Tag.

        Create Image tag for the given state.
        '''
        return '<img src=":/images/state/%s.png" width="16"/>' % (state,)

    def _format_error(self, errors):
        '''
        Format Error.

        Formats an error message (from exceptions).
        '''
        formated_error = ''
        if str(errors):
            error_image = self._get_state_html_image('failed')
            formated_error = str(errors)
            formated_error = escape_html(formated_error)
            formated_error = formated_error.replace(
                '\n', '<br/>' + error_image)
            formated_error = error_image + formated_error

        return formated_error

    def _init_ui(self):
        '''
        Initialize UI.

        Connect the UI signals to handler functions.
        '''
        account_plugins = get_account_plugins()
        for plugin_item in account_plugins:
            widget_class = plugin_item['constructor']
            tab_label = (
                '%s'
                % (plugin_item['label']))
            widget = widget_class(self, self.backend)
            widget.tab_label = tab_label
            widget.tab_ui = self.ui.account_edit_tabs
            self._plugin_tabs.append(widget)
            try:
                tab_index = widget.tab_index
                self.ui.account_edit_tabs.insertTab(
                    tab_index, widget, widget.tab_label)
            except AttributeError:
                self.ui.account_edit_tabs.addTab(widget, widget.tab_label)

    def _load_done(self, response):
        '''
        Load Done.

        Handles when loading an account has been returned from the backend.
        Used because an account needs to request editable attributes that are
        not available to the default account model.
        '''
        response_model = account_edit_item_model()
        response_model.from_json(response)
        self._load_model(response_model)

    def _load_failed(self, errors):
        '''
        Load failed.

        Handle when loading account attributes failed.
        '''
        self._show_error(
            self.tr('Load Account Failed'),
            errors)

    def _load_model(self, account_model_instance):
        '''
        Load Model.

        Load account from the given model instance.
        '''
        self.editor_model = account_model_instance
        self.ui.tab_chain.set_model(account_model_instance)
        self.ui.tab_editor.set_model(account_model_instance)
        self.ui.tab_general.set_model(account_model_instance)
        self.model_loaded_signal.emit(account_model_instance)

    def _reset_status(self):
        '''
        Reset Status.

        Reset the status output to empty and hide the QLabels.
        '''
        pass

    def _reset_widgets(self):
        '''
        Reset Widgets.

        Reset the account widgets to default values.
        '''
        self.editor_model = account_edit_item_model()
        self.ui.tab_chain.set_model(self.editor_model)
        self.ui.tab_editor.set_model(self.editor_model)
        self.ui.user_errors.setText('')
        self.ui.user_errors.setVisible(False)
        self.ui.tab_general.set_model(self.editor_model)
        self.ui.tab_editor.account_type = None
        for tab in self._plugin_tabs:
            try:
                tab.reset()
            except AttributeError:
                pass
        self.ui.button_apply_reconnect.setVisible(False)
        self.ui.account_edit_tabs.setCurrentIndex(0)
        self.ui.tab_chain.reset()
        self.ui.tab_general.reset()
        self._reset_status()
        self._config_changed()

    def _set_readonly(self, state):
        '''
        Set Readonly.

        Set the account_edit dialog readonly while a connection is active.
        '''
        self.ui.tab_editor.setEnabled(not state)
        self.ui.tab_general.setEnabled(not state)
        self.ui.tab_chain.setEnabled(not state)
        for tab in self._plugin_tabs:
            tab.setEnabled(not state)
        # update apply / create button
        self._config_changed()

    def _show_error(self, message, errors):
        '''
        Show Error.

        Helper to display error messages alongside with exeption errors
        '''
        formated_error = message
        formated_error = '<br/>'
        formated_error += self._format_error(errors)

        self.ui.user_errors.setText(formated_error)
        self.ui.user_errors.setVisible(True)

    def config_values_changed(self):
        '''
        Config Values Changed.

        Evaluate if the config is different from the loaded values.
        '''
        changed = False
        changed |= self.ui.tab_general.config_values_changed()
        changed |= self.ui.tab_chain.config_values_changed()
        changed |= self.ui.tab_editor.config_values_changed()
        for plugin_item in self._plugin_tabs:
            if changed:
                break
            try:
                changed |= plugin_item.config_values_changed()
            except AttributeError:
                pass
        return changed

    def create(self, parent_group_id=None):
        '''
        Create new Account.

        Configures the dialog to create a new account with the optional given
        parent group ID. Hide Commit Button and show Create Button.
        '''
        self.parent_group_id = parent_group_id
        self._creating = True
        self.ui.button_apply.setVisible(False)
        self.ui.button_apply_reconnect.setVisible(False)
        self.ui.button_close.setVisible(False)
        self.ui.button_cancel.setVisible(True)
        self.ui.button_create.setVisible(True)
        self.ui.button_create.setEnabled(False)
        self.reset()
        self.ui.tab_general.change_account_type()
        self.ui.tab_editor.set_account_profile(
            self.ui.tab_general.get_default_profile())
        self.model_loaded_signal.emit(None)
        self.ui.tab_general.account_profile_items_changed.emit()
        self.ui.tab_general.change_account_profile_description.emit()

    def hideEvent(self, event):
        '''
        Hide Event.

        Disconnect signals from widgets.
        '''
        self._disconnect_signals()

    def showEvent(self, event):
        '''
        Show Event.

        Connect signals to backend and widgets.
        '''
        self._connect_signals()

    def load(self, account_id):
        '''
        Load existing Account.

        Configures the dialog to edit a existing account identified by the
        given account_id. Hide Create button and show Commit button.
        '''
        self._creating = False
        self.ui.button_apply.setVisible(True)
        self.ui.button_apply_reconnect.setVisible(True)
        self.ui.button_close.setVisible(True)
        self.ui.button_create.setVisible(False)
        self.ui.button_cancel.setVisible(False)
        self.reset()
        self.backend.account_get_for_edit.emit(
            account_id,
            self.load_done_signal,
            self.load_failed_signal)

    def reset(self):
        '''
        Reset the Dialog.

        Forget all user-entered and loaded data.
        '''
        self._reset_widgets()
