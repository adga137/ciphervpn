# -*- coding: utf-8 -*-
# check_account.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Model for checking account configuration.
'''

from netsplice.gui.account_edit.model.validator.account_type import (
    account_type as account_type_validator
)
from netsplice.gui.model.validator.configuration import (
    configuration as configuration_validator
)
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable


class account_check(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.configuration = field(
            default=False,
            required=True,
            validators=[configuration_validator()])

        self.type = field(
            required=False,
            validators=[account_type_validator()])
