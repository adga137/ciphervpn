# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/gui/account_edit/raw/views/account_type_editor.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_AccountTypeEditor(object):
    def setupUi(self, tor_editor):
        tor_editor.setObjectName("tor_editor")
        self.vboxlayout = QtGui.QVBoxLayout(tor_editor)
        self.vboxlayout.setObjectName("vboxlayout")
        self.tor_editor_message = QtGui.QWidget(tor_editor)
        self.tor_editor_message.setObjectName("tor_editor_message")
        self.hboxlayout = QtGui.QHBoxLayout(self.tor_editor_message)
        self.hboxlayout.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout.setObjectName("hboxlayout")
        self.type = QtGui.QLabel(self.tor_editor_message)
        self.type.setEnabled(True)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.type.sizePolicy().hasHeightForWidth())
        self.type.setSizePolicy(sizePolicy)
        self.type.setMaximumSize(QtCore.QSize(32, 32))
        self.type.setSizeIncrement(QtCore.QSize(32, 32))
        self.type.setCursor(QtCore.Qt.SizeAllCursor)
        self.type.setText("")
        self.type.setPixmap(QtGui.QPixmap(":/images/types/unknown.png"))
        self.type.setScaledContents(True)
        self.type.setIndent(8)
        self.type.setObjectName("type")
        self.hboxlayout.addWidget(self.type)
        self.not_implemented = QtGui.QLabel(self.tor_editor_message)
        self.not_implemented.setObjectName("not_implemented")
        self.hboxlayout.addWidget(self.not_implemented)
        self.vboxlayout.addWidget(self.tor_editor_message)
        self.plain_config_edit = text_editor(tor_editor)
        self.plain_config_edit.setObjectName("plain_config_edit")
        self.vboxlayout.addWidget(self.plain_config_edit)
        spacerItem = QtGui.QSpacerItem(10, 10, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.vboxlayout.addItem(spacerItem)

        self.retranslateUi(tor_editor)
        QtCore.QMetaObject.connectSlotsByName(tor_editor)

    def retranslateUi(self, tor_editor):
        self.not_implemented.setText(QtGui.QApplication.translate("AccountTypeEditor", "RAW Account Type Editor", None, QtGui.QApplication.UnicodeUTF8))

from netsplice.gui.widgets.text_editor import text_editor
