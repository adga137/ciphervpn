# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/gui/account_edit/views/account_edit.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_AccountEdit(object):
    def setupUi(self, account_edit):
        account_edit.setObjectName("account_edit")
        account_edit.setGeometry(QtCore.QRect(0, 0, 300, 450))
        self.account_edit_layout = QtGui.QVBoxLayout(account_edit)
        self.account_edit_layout.setObjectName("account_edit_layout")
        self.account_edit_tabs = QtGui.QTabWidget(account_edit)
        self.account_edit_tabs.setObjectName("account_edit_tabs")
        self.tab_general = account_edit_tab_general()
        self.tab_general.setObjectName("tab_general")
        self.account_edit_tabs.addTab(self.tab_general, "")
        self.tab_editor = account_type_editor_proxy()
        self.tab_editor.setObjectName("tab_editor")
        self.account_edit_tabs.addTab(self.tab_editor, "")
        self.tab_chain = account_edit_tab_chain()
        self.tab_chain.setObjectName("tab_chain")
        self.account_edit_tabs.addTab(self.tab_chain, "")
        self.account_edit_layout.addWidget(self.account_edit_tabs)
        self.user_errors = scroll_label(account_edit)
        self.user_errors.setVisible(False)
        self.user_errors.setText("")
        self.user_errors.setObjectName("user_errors")
        self.account_edit_layout.addWidget(self.user_errors)
        self.action_layout = QtGui.QHBoxLayout()
        self.action_layout.setObjectName("action_layout")
        spacerItem = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.action_layout.addItem(spacerItem)
        self.button_cancel = QtGui.QPushButton(account_edit)
        self.button_cancel.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.button_cancel.setObjectName("button_cancel")
        self.action_layout.addWidget(self.button_cancel)
        self.button_check = QtGui.QPushButton(account_edit)
        self.button_check.setVisible(False)
        self.button_check.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.button_check.setObjectName("button_check")
        self.action_layout.addWidget(self.button_check)
        self.button_create = QtGui.QPushButton(account_edit)
        self.button_create.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.button_create.setObjectName("button_create")
        self.action_layout.addWidget(self.button_create)
        self.button_close = QtGui.QPushButton(account_edit)
        self.button_close.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.button_close.setObjectName("button_close")
        self.action_layout.addWidget(self.button_close)
        self.button_apply = QtGui.QPushButton(account_edit)
        self.button_apply.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.button_apply.setObjectName("button_apply")
        self.action_layout.addWidget(self.button_apply)
        self.button_apply_reconnect = QtGui.QPushButton(account_edit)
        self.button_apply_reconnect.setVisible(False)
        self.button_apply_reconnect.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.button_apply_reconnect.setObjectName("button_apply_reconnect")
        self.action_layout.addWidget(self.button_apply_reconnect)
        self.account_edit_layout.addLayout(self.action_layout)

        self.retranslateUi(account_edit)
        QtCore.QMetaObject.connectSlotsByName(account_edit)

    def retranslateUi(self, account_edit):
        account_edit.setWindowTitle(QtGui.QApplication.translate("AccountEdit", "Netsplice Account Settings", None, QtGui.QApplication.UnicodeUTF8))
        self.account_edit_tabs.setTabText(self.account_edit_tabs.indexOf(self.tab_general), QtGui.QApplication.translate("AccountEdit", "General", None, QtGui.QApplication.UnicodeUTF8))
        self.account_edit_tabs.setTabText(self.account_edit_tabs.indexOf(self.tab_editor), QtGui.QApplication.translate("AccountEdit", "Editor", None, QtGui.QApplication.UnicodeUTF8))
        self.account_edit_tabs.setTabText(self.account_edit_tabs.indexOf(self.tab_chain), QtGui.QApplication.translate("AccountEdit", "Chain", None, QtGui.QApplication.UnicodeUTF8))
        self.button_cancel.setText(QtGui.QApplication.translate("AccountEdit", "Cancel", None, QtGui.QApplication.UnicodeUTF8))
        self.button_check.setText(QtGui.QApplication.translate("AccountEdit", "Check", None, QtGui.QApplication.UnicodeUTF8))
        self.button_create.setText(QtGui.QApplication.translate("AccountEdit", "Create", None, QtGui.QApplication.UnicodeUTF8))
        self.button_close.setText(QtGui.QApplication.translate("AccountEdit", "Close", None, QtGui.QApplication.UnicodeUTF8))
        self.button_apply.setText(QtGui.QApplication.translate("AccountEdit", "Apply", None, QtGui.QApplication.UnicodeUTF8))
        self.button_apply_reconnect.setText(QtGui.QApplication.translate("AccountEdit", "Apply and Reconnect", None, QtGui.QApplication.UnicodeUTF8))

from netsplice.gui.account_edit.account_edit_tab_general import account_edit_tab_general
from netsplice.gui.account_edit.account_edit_tab_chain import account_edit_tab_chain
from netsplice.gui.account_edit.account_type_editor_proxy import account_type_editor_proxy
from netsplice.gui.widgets.scroll_label import scroll_label
