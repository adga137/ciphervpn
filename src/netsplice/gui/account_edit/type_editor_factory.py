# -*- coding: utf-8 -*-
# type_editor_factory.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Factory to get editor for account_type.

Used to abstract the widget specific to a account type from the account_edit
widget.
'''

import sys

from netsplice.gui.account_edit.errors import BadAccountTypeError
from netsplice.gui.account_edit.raw.account_type_editor import (
    account_type_editor as raw_account_type_editor
)
from netsplice.util import get_logger

logger = get_logger()

this = sys.modules[__name__]
this.account_types = {}
this.account_plugins = []
this.account_profiles = {}


def register_account_type(account_type, label, description, constructor):
    '''
    Register Account Type.

    Register the given constructor for the given account_type.
    '''
    this.account_types[account_type] = {
        'label': label,
        'description': description,
        'constructor': constructor,
    }


def register_account_profile(
        account_type, label, default, description,
        default_route=False, process_launcher=[], process_sniper=[]):
    '''
    Register Account Profile.

    Register a profile for the given account_type with the given default and
    description.
    '''
    if account_type not in this.account_profiles.keys():
        this.account_profiles[account_type] = []
    this.account_profiles[account_type].append({
        'label': label,
        'default': default,
        'description': description,
        'default_route': default_route,
        'process_launcher': process_launcher,
        'process_sniper': process_sniper
    })


def register_plugin(label, constructor):
    '''
    Register Plugin.

    Register a Plugin that is available to all account_types.
    '''
    this.account_plugins.append({
        'constructor': constructor,
        'label': label
    })


def get_account_plugins():
    '''
    Get Account Plugins.

    Compile a list of account plugins (that are widgets) suitable for adding
    them to the account_edit widget.
    '''
    plugins = []
    for plugin in this.account_plugins:
        plugins.append({
            'constructor': plugin['constructor'],
            'label': plugin['label']})

    return plugins


def get_account_types():
    '''
    Get Account Types.

    Compile a type-label list for the registered accounts.
    '''
    account_types = []
    for type_name, account_type in this.account_types.items():
        account_types.append({
            'type': type_name,
            'label': account_type['label'],
            'description': account_type['description']
        })
    return account_types


def get_account_profiles(account_type):
    '''
    Get Account Profiles.

    Compile a type-label list for the registered accounts.
    '''
    try:
        account_profiles = []
        for profile in this.account_profiles[account_type]:
            account_profiles.append(profile)
        return account_profiles
    except KeyError:
        raise BadAccountTypeError(account_type)


def factory(account_type, parent, dispatcher):
    '''
    Factory that returns the widget to edit the account_type.

    Raise BadAccountTypeError when the given account_type is not supported.
    '''
    try:
        return this.account_types[account_type]['constructor'](
            parent, dispatcher)
    except KeyError:
        raise BadAccountTypeError(account_type)


register_account_type('Raw', 'Raw', 'Raw', raw_account_type_editor)
