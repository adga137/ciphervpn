# -*- coding: utf-8 -*-
# texteditor.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Account Type Editor Proxy.

Widget that changes the account_type_editor based on the account_type.
'''

from PySide import QtGui
from PySide.QtCore import Qt, Signal
from netsplice.gui.account_edit.type_editor_factory import factory
from netsplice.util import get_logger

logger = get_logger()


class account_type_editor_proxy(QtGui.QWidget):
    '''
    Account Type Editor Proxy.

    Defines interface between account_edit and account_type_editor.
    '''

    config_changed = Signal()

    def __init__(self, parent=None):
        '''
        Initialize.

        Initialize widget with a layout that will contain the specific editor.
        '''
        QtGui.QWidget.__init__(self, parent)
        self.account_type = None
        self.model_json = ''
        self.layout = QtGui.QVBoxLayout(self)
        self.ui = None
        self.model = None
        self.layout.setContentsMargins(0, 0, 0, 0)

    def _config_changed(self):
        '''
        Config changed handler.

        Proxy-emit the config changed event from the specialized account type
        editor.
        '''
        self.config_changed.emit()

    def is_account_profile(self, profile):
        '''
        Set Account Profile.

        Set the given profile as default for the account.
        '''
        return self.ui.is_account_profile(profile)

    def set_account_type(self, account_type, dispatcher):
        '''
        Set Account Type.

        Sets the account type and load the widget for that type.
        '''
        if self.account_type == account_type:
            return
        self.account_type = account_type
        self.ui = factory(account_type, self, dispatcher)
        self.ui.config_changed.connect(self._config_changed)
        self.ui.set_model(self.model)
        while (self.layout.count() != 0):
            layout_item = self.layout.takeAt(0)
            layout_item.widget().deleteLater()
        self.layout.addWidget(self.ui)
        self.setFocusPolicy(Qt.TabFocus)
        self.setFocusProxy(self.ui)

    def set_account_profile(self, profile):
        '''
        Set Account Profile.

        Set the given profile as default for the account.
        '''
        if self.ui is None:
            return
        self.ui.set_account_profile(profile)
        self._config_changed()

    def set_model(self, account_model):
        '''
        Set Model.

        Set Model for the widget.
        '''
        self.model = account_model
        if self.ui is not None:
            self.ui.set_model(self.model)
            self._config_changed()

    def can_check(self):
        '''
        Evaluate form completeness.

        Check values in widget are complete to check the account.
        '''
        if self.ui is None:
            return False
        return self.ui.can_check()

    def can_commit(self):
        '''
        Evaluate form completeness for create or update.

        Checks values in widget are complete to create or update the account.
        '''
        if self.ui is None:
            return False
        return self.ui.can_commit()

    def check(self, check_started_signal, check_failed_signal):
        '''
        Check account values.

        Check the type values for errors.
        '''
        return self.ui.check(check_started_signal, check_failed_signal)

    def config_values_changed(self):
        '''
        Config Values Changed.

        Evaluate if the config is different from the loaded values.
        '''
        if self.ui is None:
            return False
        return self.ui.config_values_changed()

    def create(
            self, account_name, enabled, default_route, autostart, group_id,
            create_done_signal, create_failed_signal):
        '''
        Create a new account.

        Create a new account in the given group with the given name and the
        type values.
        '''
        return self.ui.create(
            account_name,
            enabled,
            default_route,
            autostart,
            group_id,
            create_done_signal,
            create_failed_signal)

    def apply(
            self, account_id, account_name, enabled, default_route, autostart,
            apply_done_signal, apply_failed_signal):
        '''
        Update existing account.

        Update an existing account with the given name and the type values.
        '''
        return self.ui.apply(
            account_id,
            account_name,
            enabled,
            default_route,
            autostart,
            apply_done_signal,
            apply_failed_signal)
