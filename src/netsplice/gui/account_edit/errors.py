# -*- coding: utf-8 -*-
# errors.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Errors specific to the account_edit.

Defines errors that are specific to the current module.
'''


class BadAccountTypeError(Exception):
    '''
    Bad Account Type Error.

    Indicates that a unknown account type was provided to a factory.
    '''

    def __init__(self, message):
        '''
        Initialize Module.

        Initialize Exception base.
        '''
        Exception.__init__(self, message)
