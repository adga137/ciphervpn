# -*- coding: utf-8 -*-
# texteditor.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Account Type editor abstract base.

Base for modules that allow editing configurations of accounts of specific
type.
'''


class abstract_account_type_editor(object):
    '''
    Custom Editor with syntax highlighter and keyword tooltip.

    Defines shared functionality and abstract functions.
    '''

    def __init__(self, type=None, dispatcher=None):
        '''
        Initialize.

        Initialize.
        '''
        self.ui = None
        self.model = None
        self.backend = dispatcher
        self.type = type

    def is_account_profile(self, profile):
        '''
        Is Account Profile.

        Return True when the current (import) configuration matches the
        profile default.
        '''
        raise NotImplementedError()

    def set_account_profile(self, profile):
        '''
        Set Account Profile.

        Set the given profile as default for the account.
        '''
        raise NotImplementedError()

    def set_model(self, account_model):
        '''
        Set Model.

        Set Model for the widget.
        '''
        self.model = account_model

    def can_check(self):
        '''
        Evaluate form completeness.

        Checks values in widget are complete to check the account.
        '''
        raise NotImplementedError()

    def can_commit(self):
        '''
        Evaluate form completeness for create or update.

        Checks values in widget are complete to create or update the account.
        '''
        raise NotImplementedError()

    def check(self, check_done_signal, check_failed_signal):
        '''
        Check account values.

        Check the type values for errors.
        '''
        raise NotImplementedError()

    def config_values_changed(self):
        '''
        Config Values Changed.

        Evaluate if the config is different from the loaded values.
        '''
        raise NotImplementedError()

    def create(
            self, account_name, group_id,
            create_done_signal, create_failed_signal):
        '''
        Create a new account.

        Create a new account in the given group with the given name and the
        type values.
        '''
        raise NotImplementedError()

    def apply(self, account_name, apply_done_signal, apply_failed_signal):
        '''
        Update existing account.

        Update a existing account with the given name and the type values.
        '''
        raise NotImplementedError()
