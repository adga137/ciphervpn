# -*- coding: utf-8 -*-
# preferences.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Preferences window
'''

import json

from PySide.QtCore import (Qt, Signal)
from PySide.QtGui import (
    QFrame, QLabel,
    QScrollArea,
    QWidget, QVBoxLayout
)
from preferences_ui import Ui_preferences
from netsplice.config import gui as config
from netsplice.config import about as config_about
from netsplice.gui.widgets.dialog import dialog
from netsplice.util.parser import factory as get_parser  # config_parser
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.util.errors import NotFoundError
from netsplice.gui.key_value_editor.key_value_editor import key_value_editor


class preferences(dialog):
    '''
    Window that displays preferences of the application.
    '''
    backend_done = Signal(basestring)
    backend_failed = Signal(basestring)

    def __init__(self, parent, dispatcher):
        '''
        Initialize the widget.
        '''
        dialog.__init__(self, parent)

        self.backend = dispatcher
        self.ui_model = None
        self.backend_model = None
        self.model = None
        self.ui_components = {}
        self.ui_plugins = {}
        self.ui = Ui_preferences()
        self.ui.setupUi(self)
        self.preference_header = [
            self.tr('Option'),
            self.tr('Type'),
            self.tr('Value'),
        ]

        self.ui.close_button.clicked.connect(self._close)

        event_loop = self.backend.application.event_loop
        event_loop.preferences_changed.connect(
            self._preferences_changed)
        event_loop.preferences_plugins_changed.connect(
            self._preferences_plugins_changed)

        self.backend_done.connect(self._backend_done)

        self._setup_core_items()
        self._setup_plugins(
            self.backend.application.plugins)

        self._preferences_changed(
            self.backend.application.get_model().preferences)
        self._preferences_plugins_changed(
            self.backend.application.get_model().preferences.plugins)

    def _backend_done(self):
        '''
        Backend done.

        Request the new preferences.
        '''
        self.backend.preferences_get.emit()

    def _close(self):
        '''
        Close the dialog.
        '''
        self.close()

    def _setup_core_items(self):
        '''
        Setup Core Items.

        Setup Tabs for general configuration of application - non plugin,
        global stuff.
        '''
        for component_name, tab_label in \
                config.CORE_PREFERENCE_COMPONENTS.items():
            no_tr_tab_description = config.PREFERENCES_TEXT[component_name]
            tab_description = self.tr(no_tr_tab_description).format(
                API_URL=config_about.API_URL
            )
            tab_widget = QWidget(self)
            tab_widget_layout = QVBoxLayout(tab_widget)

            # Tab Description
            scroll_area = QScrollArea(tab_widget)
            scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
            scroll_area.setWidgetResizable(True)
            scroll_area.setFrameShape(QFrame.NoFrame)
            scroll_area.setMaximumHeight(50)

            tab_widget_description = QLabel(tab_widget)
            tab_widget_description.setText(
                tab_description)
            tab_widget_description.setOpenExternalLinks(True)
            tab_widget_description.setWordWrap(True)
            tab_widget_description.setAlignment(Qt.AlignTop | Qt.AlignLeft)
            scroll_area.setWidget(tab_widget_description)

            tab_widget_layout.addWidget(scroll_area)
            # Tab Content
            component_editor = key_value_editor()
            component_editor.disable_editor_column(0)
            component_editor.set_value_column(2)
            component_editor.set_header(self.preference_header)
            component_editor.values_changed.connect(
                self._values_changed)
            component_editor.value_checked.connect(
                self._values_changed)
            component_editor.component_name = component_name

            self.ui_components[component_name] = component_editor
            tab_widget.editor = component_editor

            tab_widget_layout.addWidget(component_editor)
            self.ui.preferences_tabs.addTab(
                tab_widget, self.tr(tab_label))

    def _setup_plugins(self, plugins):
        '''
        Setup Plugins.

        Create an action button and a key_value_editor for every given
        plugin. Register the created widgets so they can be switched and
        received.
        '''
        model = self.backend.application.get_model().preferences.plugins
        for plugin_item in plugins:
            name = None
            tab_label = None
            tab_description = None
            plugin_model = None
            try:
                name = plugin_item.config.PLUGIN_NAME
                tab_label = name
                plugin_model = model.find_by_name(name)
            except AttributeError:
                # ignore plugins that have no name
                continue
            except NotFoundError:
                continue
            plugin_config = self._plugin_model_to_config(plugin_model)
            if len(plugin_config) is 0:
                continue
            try:
                tab_label = plugin_item.config.DISPLAY_NAME
            except AttributeError:
                # plugins that did not provide a PLUGIN_NAME
                tab_label = tab_label.title()
                tab_label = tab_label.replace('_', ' ')

            try:
                tab_description = plugin_item.config.gui.PREFERENCES_HELP
            except AttributeError:
                tab_description = None

            tab_widget = QWidget(self)
            tab_widget_layout = QVBoxLayout(tab_widget)

            # Tab Description
            scroll_area = QScrollArea(tab_widget)
            scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
            scroll_area.setWidgetResizable(True)
            scroll_area.setFrameShape(QFrame.NoFrame)
            scroll_area.setMaximumHeight(50)

            tab_widget_description = QLabel(tab_widget)
            tab_widget_description.setText(
                tab_description)
            tab_widget_description.setWordWrap(True)
            tab_widget_description.setAlignment(Qt.AlignTop | Qt.AlignLeft)
            scroll_area.setWidget(tab_widget_description)

            tab_widget_layout.addWidget(scroll_area)

            # Tab Content
            plugin_editor = key_value_editor()
            plugin_editor.set_value_column(2)
            plugin_editor.disable_editor_column(0)
            plugin_editor.set_header(self.preference_header)
            plugin_editor.model_changed.emit(
                plugin_config)
            plugin_editor.values_changed.connect(
                self._values_changed)
            plugin_editor.value_checked.connect(
                self._values_changed)
            plugin_editor.component_name = 'plugins'
            plugin_editor.plugin_name = name

            tab_widget_layout.addWidget(plugin_editor)
            tab_widget.editor = plugin_editor

            self.ui.preferences_tabs.addTab(
                tab_widget, tab_label)
            self.ui_plugins[name] = plugin_editor

    def _model_to_config(self, model):
        '''
        Model To Config.

        Convert the given model attributes to a line-config that can
        be interpreted by the key_value_editor.
        '''
        parser = get_parser('Line')
        for name in sorted(model.__dict__):
            if name.startswith('_'):
                continue
            attribute = model.__dict__[name]
            if not isinstance(attribute, (field, marshalable)):
                continue
            line = parser.current_element
            line.key.set(self.tr(name))
            if isinstance(attribute, (list,)):
                line.add_value('type', '<type \'list\'>')
                line.add_value('value', attribute.to_json())
            elif isinstance(attribute.get(), (bool,)):
                line.add_value('type', str(type(attribute.get())))
                line.add_value('value', attribute.get())
            else:
                line.add_value('type', str(type(attribute.get())))
                line.add_value('value', str(attribute.get()))
            parser.commit()
        return parser.get_config()

    def _plugin_model_to_config(self, model):
        '''
        Plugin Model to Config.

        Helper that removes the 'name' field from the plugin values as this
        option should never change.
        '''
        plugin_config = self._model_to_config(model)
        for element in plugin_config:
            if element.key.get() == 'name':
                plugin_config.remove(element)
                break
        return plugin_config

    def _preferences_changed(self, model):
        '''
        Preferences Changed.

        Load the values provided by the backend when not currently editing a
        value.
        '''
        for index in range(0, self.ui.preferences_tabs.count() - 1):
            stack_widget = self.ui.preferences_tabs.widget(index)
            if isinstance(stack_widget, (key_value_editor,)):
                if stack_widget.in_edit_mode():
                    return

        self.model = model

        for component_name in config.CORE_PREFERENCE_COMPONENTS.keys():
            component_model = self.model.__dict__[component_name]
            component = self.ui_components[component_name]
            component.model_changed.emit(
                self._plugin_model_to_config(component_model))

    def _preferences_plugins_changed(self, model):
        '''
        Preferences Changed.

        Load the values provided by the backend when not currently editing a
        value.
        '''
        for index in range(0, self.ui.preferences_tabs.count() - 1):
            stack_widget = self.ui.preferences_tabs.widget(index)
            if isinstance(stack_widget, (key_value_editor,)):
                if stack_widget.in_edit_mode():
                    return

        for plugin in model:
            try:
                self.ui_plugins[plugin.name.get()].model_changed.emit(
                    self._plugin_model_to_config(plugin))
            except KeyError:
                pass

    def _values_changed(self, new_values):
        '''
        Update the values from the widget to the backend.
        '''
        active_tab = self.ui.preferences_tabs.currentWidget()
        active_editor = active_tab.editor
        component_name = active_editor.component_name

        model = self.model.__dict__[component_name].__dict__
        try:
            plugin_name = active_editor.plugin_name
            model = self.model.plugins.find_by_name(plugin_name).__dict__
        except AttributeError:
            plugin_name = None

        for name in model:
            if name == '_owner':
                continue
            is_complex = False
            attribute = model[name]
            if not isinstance(attribute, (field,)):
                continue
            if isinstance(attribute, (marshalable,)):
                # complex values
                model_value = attribute.to_json()
            else:
                model_value = attribute.get()
            attribute_value = None
            try:
                attribute_value_item = new_values.find_by_key(name)
                attribute_value = attribute_value_item.get_value('value')
                attribute_type = attribute_value_item.get_value('type')
            except NotFoundError:
                continue
            if model_value == attribute_value:
                continue
            if attribute_value is None:
                attribute_value = ''
                if attribute_type == '<type \'int\'>':
                    attribute_value = '-1'

            if attribute_type == '<type \'list\'>':
                parsed_values = []
                if attribute_value != '':
                    parsed_values = json.loads(attribute_value)
                attribute_value = json.dumps(parsed_values)
                is_complex = True
            elif attribute_type == '<type \'int\'>':
                attribute_value = int(attribute_value)

            if component_name in config.CORE_PREFERENCE_COMPONENTS.keys():
                if is_complex:
                    self.backend.preferences_complex_set.emit(
                        component_name, name, attribute_value,
                        self.backend_done, self.backend_failed)
                else:
                    self.backend.preferences_value_set.emit(
                        component_name, name, attribute_value,
                        self.backend_done, self.backend_failed)
            if component_name == 'plugins':
                self.backend.preferences_plugin_set.emit(
                    plugin_name, name, attribute_value,
                    self.backend_done, self.backend_failed)
