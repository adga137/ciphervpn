# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/gui/preferences/views/preferences.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_preferences(object):
    def setupUi(self, preferences):
        preferences.setObjectName("preferences")
        preferences.resize(520, 439)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/images/netsplice-icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        preferences.setWindowIcon(icon)
        self.verticalLayout = QtGui.QVBoxLayout(preferences)
        self.verticalLayout.setObjectName("verticalLayout")
        self.preferences_tabs = QtGui.QTabWidget(preferences)
        self.preferences_tabs.setObjectName("preferences_tabs")
        self.verticalLayout.addWidget(self.preferences_tabs)
        self.button_layout = QtGui.QHBoxLayout()
        self.button_layout.setObjectName("button_layout")
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.button_layout.addItem(spacerItem)
        self.close_button = QtGui.QPushButton(preferences)
        self.close_button.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.close_button.setObjectName("close_button")
        self.button_layout.addWidget(self.close_button)
        self.verticalLayout.addLayout(self.button_layout)

        self.retranslateUi(preferences)
        QtCore.QMetaObject.connectSlotsByName(preferences)

    def retranslateUi(self, preferences):
        preferences.setWindowTitle(QtGui.QApplication.translate("preferences", "Netsplice Preferences", None, QtGui.QApplication.UnicodeUTF8))
        self.close_button.setText(QtGui.QApplication.translate("preferences", "Close", None, QtGui.QApplication.UnicodeUTF8))

