# -*- coding: utf-8 -*-
# dispatcher_endpoints.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Endpoints used by dispatcher class(es).
'''


#
# Endpoints used by backend_dispatcher
#
BACKEND_STATUS = (
    r'status')
BACKEND_GUI_ACCOUNTS_CONNECT = (
    r'module/gui/accounts/%(account_id)s/connect')
BACKEND_GUI_ACCOUNTS_DISCONNECT = (
    r'module/gui/accounts/%(account_id)s/disconnect')
BACKEND_GUI_ACCOUNTS_RECONNECT = (
    r'module/gui/accounts/%(account_id)s/reconnect')
BACKEND_GUI_ACCOUNTS_RESET = (
    r'module/gui/accounts/%(account_id)s/reset')
BACKEND_GUI_ACTIONS_UPDATE = (
    r'module/gui/actions/update')
BACKEND_GUI_ACTIONS_SHUTDOWN = (
    r'module/gui/actions/shutdown')
BACKEND_GUI_CHAIN_CONNECT = (
    r'module/gui/chain/%(account_id)s/connect')
BACKEND_GUI_CREDENTIAL = (
    r'module/gui/credential')
BACKEND_GUI_EVENTS = (
    r'module/gui/events')
BACKEND_GUI_GROUPS_CONNECT = (
    r'module/gui/groups/%(group_id)s/connect')
BACKEND_GUI_GROUPS_DISCONNECT = (
    r'module/gui/groups/%(group_id)s/disconnect')
BACKEND_GUI_GROUPS_RECONNECT = (
    r'module/gui/groups/%(group_id)s/reconnect')
BACKEND_GUI_MODEL = (
    r'module/gui')
BACKEND_GUI_LOGS = (
    r'module/gui/logs')
BACKEND_GUI_LOGS_OFFSET = (
    r'module/gui/logs/offset/%(offset)s')
BACKEND_PREFERENCES = (
    r'module/preferences')
BACKEND_PREFERENCES_EVENTS = (
    r'module/preferences/events')
BACKEND_PREFERENCES_ACCOUNTS = (
    r'module/preferences/accounts')
BACKEND_PREFERENCES_ACCOUNTS_ITEM = (
    r'module/preferences/accounts/%(account_id)s')
BACKEND_PREFERENCES_CHAINS = (
    r'module/preferences/chains')
BACKEND_PREFERENCES_CHAIN_ITEM = (
    r'module/preferences/chains/%(chain_id)s')
BACKEND_PREFERENCES_GROUPS = (
    r'module/preferences/groups')
BACKEND_PREFERENCES_GROUPS_ITEM = (
    r'module/preferences/groups/%(group_id)s')
BACKEND_PREFERENCES_GROUPED_ACCOUNTS = (
    r'module/preferences/grouped_accounts')
BACKEND_PREFERENCES_GROUPED_ACCOUNTS_ITEM = (
    r'module/preferences/grouped_accounts/%(account_id)s')
BACKEND_PREFERENCES_VALUE = (
    r'module/preferences/value/%(collection_name)s/%(field_name)s')
BACKEND_PREFERENCES_COMPLEX = (
    r'module/preferences/complex/%(collection_name)s/%(field_name)s')
BACKEND_PREFERENCES_PLUGIN_ITEM = (
    r'module/preferences/'
    'plugin/%(plugin_name)s/'
    'attribute/%(field_name)s')
BACKEND_PREFERENCES_PLUGIN_ALL = (
    r'module/preferences/plugin/%(plugin_name)s')
BACKEND_PREFERENCES_PLUGIN_COLLECTION_INSTANCE = (
    r'module/preferences/'
    'plugin/%(plugin_name)s/'
    'collection/%(collection_name)s/'
    'instance_id/%(instance_id)s')
