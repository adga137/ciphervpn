# -*- coding: utf-8 -*-
# log_gui-model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Model for the log list view.
'''

import locale
import datetime
import iso8601
import textwrap
import timeago
from dateutil import tz
from textwrap import wrap

from PySide.QtCore import Qt, QAbstractItemModel, QModelIndex, QSize
from PySide.QtGui import QBrush, QColor, QFontMetrics

from netsplice.util import get_logger
from netsplice.config import log as log_config

LEVEL_ERROR = 'ERROR'
LEVEL_CRITICAL = 'CRITICAL'

logger = get_logger()


class log_gui_model(QAbstractItemModel):
    '''
    GUI Model to be used by a tree or listview.

    Implements a filter mechanism and the configuration of the columns.
    '''

    def __init__(self, parent):
        '''
        Initialize.

        :param parent: Owner of the model (usually logviewer)
        '''
        QAbstractItemModel.__init__(self, parent)

        # Private attributes.
        self.log_items = []
        self._parent = parent
        self._brushes = {}
        self._extra_filter = {}
        self._level_filter = {}
        self._text_filter = None
        self._text_filter_case_sensitive = False
        self._last_format_log_item = None

        # Frequently used values that should not be constructed everytime
        # data is called.
        for name in log_config.LEVEL_COLORS.keys():
            self._brushes[name] = QBrush(
                QColor(log_config.LEVEL_COLORS[name]))
        self.reset_default_size()

    def add_log_item(self, log_item_instance):
        '''
        Add an item to the collection.

        Adds the given item to the log_items.
        '''
        self.log_items.append(log_item_instance)

    def columnCount(self, parent):
        '''
        Column count.

        QAbstractItemModel override.
        '''
        return len(log_config.LOGVIEWER_COLUMNS)

    def data(self, index, role):
        '''
        Data for Index and Role.

        QAbstractItemModel override.
        Configures each index size and alignment.
        Returns background color and tooltip.
        Returns value for the cells.
        '''
        if not index.isValid():
            return None
        if role == Qt.SizeHintRole:
            return self.default_size
        if role == Qt.TextAlignmentRole:
            return Qt.AlignLeft | Qt.AlignTop

        log_item_instance = index.internalPointer()
        if role == Qt.BackgroundRole:
            return self._brushes[log_item_instance.level.get().upper()]
        if role == Qt.ToolTipRole:
            # Wrap tooltip on column 80 to make it readable.
            return '\n'.join(textwrap.wrap(
                log_item_instance.formated_message.get(), 80))
        if role == Qt.DisplayRole:
            column_config = log_config.LOGVIEWER_COLUMNS[index.column()]
            if column_config['field'] == 'date':
                return self.data_date(log_item_instance)
            if column_config['field'] in log_item_instance.__dict__:
                return log_item_instance.__dict__[column_config['field']].get()

        return None

    def data_date(self, log_item_instance):
        '''
        Date Data for log_item_instance.

        Return the date formatted according to log_config.
        '''
        log_date = log_item_instance.date.get()
        if log_config.LOGVIEWER_DATE == 'raw':
            return log_date
        if log_config.LOGVIEWER_DATE == 'time':
            log_date_date = iso8601.parse_date(log_date).replace(
                tzinfo=tz.tzutc()).astimezone(tz.tzlocal())
            return log_date_date.strftime(locale.nl_langinfo(locale.T_FMT))
        if log_config.LOGVIEWER_DATE == 'timeago':
            now = datetime.datetime.utcnow()
            log_date_date = iso8601.parse_date(log_date).replace(
                tzinfo=None)
            return timeago.format(log_date_date, now)
        # raw
        return log_item_instance.date.get()

    def wrap_line(self, formated_line, wrap_column):
        '''
        Wrap Line.

        Make the formated line more readable by wraping it on the given
        wrap_column.
        Special magic for opening brackets puts a additional newline and
        indent before those to make wrapped logs readable.

        Arguments:
            formated_line -- String that needs wrapping.
            wrap_column -- column to wrap on.
        '''
        components = formated_line.split('(')
        wrapped_lines = []
        line = ''
        for index, value in enumerate(components):
            if index is 0:
                continue
            components[index] = '(' + value
        for component in components:
            if len(component) + len(line) >= wrap_column:
                wrapped_lines.extend(wrap(line, wrap_column))
                line = ''

            if len(component) < wrap_column:
                line += component
                continue

            line += component
            wrapped_lines.extend(wrap(line, wrap_column))
            line = ''
        if line != '':
            wrapped_lines.extend(wrap(line, wrap_column))
        formated_line = ''
        for line in wrapped_lines:
            if formated_line != '':
                formated_line+= log_config.WRAP_LINE_SEPARATOR
            formated_line += line
        formated_line+= '\n'
        return formated_line

    def format_log_item(
            self, log_item, text_format=None, wrap_column=0,
            highlight_error=True, highlight_critical=True,
            highlight_time=True):
        '''
        Format a log item.

        Returns a plaintext representation for the logitem.
        '''
        if text_format is None:
            text_format = log_config.LOG_TEXT_LINE_FORMAT
        log_dict = log_item.get_json_dict()
        log_dict['message'] = log_dict['message'].replace('\n', ' ')
        formated_line = text_format.format(**log_dict)
        if wrap_column > 0:
            formated_line = self.wrap_line(formated_line, wrap_column)
        level = log_item.level.get().upper()
        if highlight_error and level == LEVEL_ERROR:
            last_level = None
            format_error = True
            if self._last_format_log_item is not None:
                last_level = self._last_format_log_item.level.get().upper()
                if log_item.level.get() == last_level:
                    format_error = False
            if format_error:
                formated_line = (
                    log_config.LOG_TEXT_ERROR_FORMAT.format(
                        formated_line=formated_line))

        if highlight_critical and level == LEVEL_CRITICAL:
            last_level = None
            format_error = True
            if self._last_format_log_item is not None:
                last_level = self._last_format_log_item.level.get().upper()
                if log_item.level.get() == last_level:
                    format_error = False
            if format_error:
                formated_line = (
                    log_config.LOG_TEXT_CRITICAL_FORMAT.format(
                        formated_line=formated_line))

        if highlight_time > 0:
            if self._last_format_log_item is None:
                last_date_string = '1970-01-01'
            else:
                last_date_string = self._last_format_log_item.date.get()
            log_date_string = log_item.date.get()
            last_date = iso8601.parse_date(last_date_string)
            log_date = iso8601.parse_date(log_date_string)
            if log_date - last_date > datetime.timedelta(
                seconds=highlight_time):
                formated_line = (
                    '\n\n{log_date}\n{formated_line}'.format(
                        log_date=log_date_string,
                        formated_line=formated_line))
        self._last_format_log_item = log_item
        return formated_line

    def headerData(self, section, orientation, role):
        '''
        Label data for header.

        QAbstractItemModel override.
        Returns config values for columns.
        '''
        if orientation != Qt.Horizontal or role != Qt.DisplayRole:
            return None
        return self.tr(log_config.LOGVIEWER_COLUMNS[section]['label'])

    def index(self, row, column, parent):
        '''
        Return Index for row, column in the given parent.

        QAbstractItemModel override.
        Parent is ignored as the log-model is a flat model.
        '''
        if row >= len(self.log_items):
            return QModelIndex()
        return self.createIndex(row, column, self.log_items[row])

    def is_hidden(self, row):
        '''
        Filter helper.

        Evaluates if according to the configured filters the item in the
        given row should be hidden.
        '''
        index = self.index(row, 0, QModelIndex())
        log_item = index.internalPointer()
        level = log_item.level.get().upper()
        if level in self._level_filter:
            if self._level_filter[level] is False:
                return True
        if isinstance(self._extra_filter, (dict,)):
            for extra_key in self._extra_filter.keys():
                if self._extra_filter[extra_key] is None:
                    continue
                if extra_key not in log_item.extra.__dict__:
                    continue
                extra_value = log_item.extra.__dict__[extra_key].get()
                if extra_value != self._extra_filter[extra_key]:
                    return True

        if self._text_filter is None or self._text_filter == '':
            return False
        text_filter = self._text_filter
        message = log_item.message.get()
        if self._text_filter_case_sensitive is False:
            text_filter = text_filter.upper()
            message = message.upper()
        if message.find(text_filter) is -1:
            return True
        return False

    def get_filter(self):
        '''
        Filter helper.

        Evaluate a printable filter from the current settings.
        '''
        filters = []
        if len(self._level_filter) > 0:
            filter = ''
            for level, value in self._level_filter.items():
                if value:
                    continue
                filter += ' %s' % (level,)
            if filter != '':
                filter = '%s:%s' % (self.tr('level'), filter)
                filters.append(filter)

        if isinstance(self._extra_filter, (dict,)):
            filter = ''
            for extra_key in self._extra_filter.keys():
                filter += ' %s' % (extra_key,)
            if filter != '':
                filter = '%s:%s' % (self.tr('extra'), filter)
                filters.append(filter)

        if not (self._text_filter is None or self._text_filter == ''):
            # ommit the unfiltered user input text
            filter = '%s' % (self.tr('text'))
            filters.append(filter)

        if self._text_filter_case_sensitive is True:
            filter = '%s' % (self.tr('case sensitive'))
            filters.append(filter)
        return ' and '.join(filters)

    def parent(self, index):
        '''
        Return parent.

        QAbstractItemModel override.
        '''
        return QModelIndex()

    def reset(self):
        '''
        Reset Model.

        QAbstractItemModel override.
        '''
        QAbstractItemModel.reset(self)
        self.reset_default_size()

    def reset_default_size(self):
        '''
        Reset the default size with config values.
        '''
        font_metrics = QFontMetrics(self._parent.fontMetrics())
        line_height = font_metrics.lineSpacing()
        self.default_size = QSize(
            1, line_height * log_config.TEXT_LINES_PER_ITEM)

    def rowCount(self, parent):
        '''
        Row count.

        QAbstractItemModel override.
        Returns the length of the log_items list for invalid parent (root)
        and 0 for any other index.
        '''
        if not parent.isValid():
            return len(self.log_items)
        return 0

    def list_to_plain_text(
            self, index_list, text_format=None, wrap_column=0,
            highlight_error=True, highlight_critical=True,
            highlight_time=True):
        '''
        Create plaintext for log items.

        Returns a line for each item that is not filtered and is selected.
        Avoids multiple same items from index_lists created by qt.
        '''
        text = ''
        list_indices = []
        for index in index_list:
            log_item = index.internalPointer()
            if log_item.index.get() in list_indices:
                continue
            list_indices.append(log_item.index.get())
            text += self.format_log_item(
                log_item,
                text_format=text_format,
                wrap_column=wrap_column,
                highlight_error=highlight_error,
                highlight_critical=highlight_critical,
                highlight_time=highlight_time)
        self._last_format_log_item = None
        return text

    def set_extra_filter(self, extra_filter):
        '''
        Set extra filter.

        :param extra_filter: Dictionary with values in log_item extra.
        '''
        self._extra_filter = extra_filter

    def set_level_filter(self, level_filter):
        '''
        Set level filter.

        :param level_filter: Dictionary with levels that should be filtered.
        '''
        self._level_filter = level_filter

    def set_text_filter(self, text_filter, case_sensitive=False):
        '''
        Set text filter.

        :param text_filter: text to be found in the message of a log_item
        :param case_sensitive: ignore case for find.
        '''
        self._text_filter = text_filter
        self._text_filter_case_sensitive = case_sensitive

    def to_plain_text(
            self, text_format=None, wrap_column=0,
            highlight_error=True, highlight_critical=True,
            highlight_time=True):
        '''
        Create plaintext for log items.

        Returns a line for each item that is not filtered.
        '''
        text = ''
        for row, log_item in enumerate(self.log_items):
            if self.is_hidden(row):
                continue
            text += self.format_log_item(
                log_item,
                text_format=text_format,
                wrap_column=wrap_column,
                highlight_error=highlight_error,
                highlight_critical=highlight_critical,
                highlight_time=highlight_time)
        self._last_format_log_item = None
        return text
