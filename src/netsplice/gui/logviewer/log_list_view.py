# -*- coding: utf-8 -*-
# logviewer.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Log List View.

Treeview that implements functionality related to log items.
'''
from PySide.QtGui import (
    QTreeView, QMenu, QAction, QApplication, QAbstractItemView
)
from netsplice.util import get_logger, log_report
from netsplice.config import log as config_log


logger = get_logger()


class log_list_view(QTreeView):
    '''
    Log List View.

    Treeview with custom context menu.
    '''

    def __init__(self, parent):
        '''
        Initialize the widget.

        Initialize tree and actions.
        '''
        QTreeView.__init__(self, parent)
        self.setSelectionMode(QAbstractItemView.ExtendedSelection)

        # Actions without UI definition
        self.action_copy_all_report = QAction(self)
        self.action_copy_all_report.setObjectName('action_copy_all_report')
        self.action_copy_all_report.setText(self.tr('Copy All'))
        self.action_copy_all_report.triggered.connect(self._copy_all_report)


        self.action_copy_selected_nocontext = QAction(self)
        self.action_copy_selected_nocontext.setObjectName(
            'action_copy_selected_nocontext')
        self.action_copy_selected_nocontext.setText(
            self.tr('Copy Selected (No Context)'))
        self.action_copy_selected_nocontext.triggered.connect(
            self._copy_selected_nocontext)

        self.action_copy_selected_report = QAction(self)
        self.action_copy_selected_report.setObjectName(
            'action_copy_selected_report')
        self.action_copy_selected_report.setText(
            self.tr('Copy Selected'))
        self.action_copy_selected_report.triggered.connect(
            self._copy_selected_report)

    def _copy_all(self):
        '''
        Copy All.

        Copy all log items as plaintext to clipboard.
        '''
        clipboard = QApplication.clipboard()
        clipboard.setText(self.get_all_as_text())

    def _copy_all_report(self):
        '''
        Copy All.

        Copy all log items as plaintext to clipboard.
        '''
        clipboard = QApplication.clipboard()
        clipboard.setText(self.get_all_as_report())

    def _copy_selected_nocontext(self):
        '''
        Copy Selected.

        Copy selected log items as plaintext to clipboard.
        '''
        clipboard = QApplication.clipboard()
        clipboard.setText(self.get_selected_as_nocontext())

    def _copy_selected_report(self):
        '''
        Copy Selected.

        Copy selected log items as plaintext to clipboard.
        '''
        clipboard = QApplication.clipboard()
        clipboard.setText(self.get_selected_as_report())

    def get_all_as_report(self):
        '''
        Get All as Report.

        Get all items as text from and compile a report.
        '''
        log_text = self.model().to_plain_text(
            text_format=config_log.LINE_FORMAT_NOCONTEXT,
            wrap_column=config_log.REPORT_WRAP_COLUMN,
            highlight_time=config_log.REPORT_BLOCK_TIME)
        return log_report(log_text)

    def get_selected_as_report(self):
        '''
        Get All as Text.

        Get all items as text from model.
        '''
        log_text = self.model().list_to_plain_text(
            self.selectedIndexes(),
            text_format=config_log.LINE_FORMAT_NOCONTEXT,
            wrap_column=config_log.REPORT_WRAP_COLUMN,
            highlight_time=config_log.REPORT_BLOCK_TIME)
        return log_report(log_text)

    def get_selected_as_nocontext(self):
        '''
        Get All as Text.

        Get all items as text from model.
        '''
        return self.model().list_to_plain_text(
            self.selectedIndexes(),
            text_format=config_log.LINE_FORMAT_NOCONTEXT,
            highlight_time=0)

    def contextMenuEvent(self, event):
        '''
        Context Menu Event.

        Protected Overload to create a context menu for the item.
        '''
        context_position = self.mapToGlobal(event.pos())
        menu = QMenu(self)
        menu.addAction(self.action_copy_selected_nocontext)
        menu.addAction(self.action_copy_selected_report)
        menu.addAction(self.action_copy_all_report)

        menu.exec_(context_position)
        return True
