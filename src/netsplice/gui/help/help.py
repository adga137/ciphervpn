# -*- coding: utf-8 -*-
# help.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Help View.

Widget for displaying help for the application.
'''

from PySide.QtCore import Qt
from PySide.QtGui import (
    QFrame, QLabel, QPixmap,
    QScrollArea, QSizePolicy, QSpacerItem,
    QWidget, QVBoxLayout)
from netsplice import __version__ as VERSION
from netsplice.config import about as config_about
from netsplice.config.gui import RES_ICON_APPLICATION
from netsplice.gui.widgets.dialog import dialog
from netsplice.gui.help.help_ui import Ui_Help
from netsplice.gui.account_edit.type_editor_factory import get_account_plugins


class help(dialog):
    '''
    Custom Widget for displaying information help the application.
    '''

    def __init__(self, parent=None, dispatcher=None):
        '''
        Initialize Module.

        Initialize the dialog and member variables.
        '''
        dialog.__init__(self, parent)
        self.mainwindow = parent
        self.backend = dispatcher
        self.ui = Ui_Help()
        self.ui.setupUi(self)
        self.ui.button_ok.clicked.connect(self.close)
        self.ui.button_logs.clicked.connect(self.show_logs)
        application_logo = QPixmap(RES_ICON_APPLICATION)
        self.ui.application_icon.setPixmap(application_logo)
        self.ui.application_icon.setScaledContents(True)
        self.setWindowTitle(self.tr('Netsplice Help - %s') % (VERSION,))
        self.init_core_text()

    def init_core_text(self):
        '''
        Init Core Text.

        Initialize the core application text.
        '''
        bitcoin_url = config_about.SUPPORT_BITCOIN_URL % {
            'SUPPORT_BITCOIN': config_about.SUPPORT_BITCOIN}

        msg = self.tr(config_about.HELP_TEXT)
        long_msg = self.tr(config_about.HELP_TEXT_LONG)
        msg = msg % {
            'SUPPORT_EMAIL': config_about.SUPPORT_EMAIL,
            'IRC_CHAT_URL': config_about.IRC_CHAT_URL,
            'DOC_URL': config_about.DOC_URL,
            'FAQ_URL': config_about.FAQ_URL,
            'COPYRIGHT_URL': config_about.COPYRIGHT_URL,
            'LICENSE_URL': config_about.LICENSE_URL,
            'LICENSE_NAME': config_about.LICENSE_NAME,
            'VERSION': VERSION}
        self.ui.message_label.setText(msg)
        self.ui.message_label.setTextInteractionFlags(
            Qt.TextBrowserInteraction)
        self.ui.message_long_label.setText(long_msg)
        self.ui.message_long_label.setTextInteractionFlags(
            Qt.TextBrowserInteraction)

    def show_logs(self):
        '''
        Show logs.

        Display the logs window by triggering a event-loop signal that causes
        the mainwindow to raise the logwindow.
        '''
        event_loop = self.backend.application.event_loop
        event_loop.notify_error.emit('')
