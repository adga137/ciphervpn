# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/gui/help/views/help.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Help(object):
    def setupUi(self, about):
        about.setObjectName("about")
        about.setGeometry(QtCore.QRect(0, 0, 700, 680))
        self.help_layout = QtGui.QVBoxLayout(about)
        self.help_layout.setObjectName("help_layout")
        self.help_message_line = QtGui.QWidget(about)
        self.help_message_line.setObjectName("help_message_line")
        self.help_message_line_layout = QtGui.QHBoxLayout(self.help_message_line)
        self.help_message_line_layout.setContentsMargins(0, 0, 0, 0)
        self.help_message_line_layout.setObjectName("help_message_line_layout")
        self.application_icon = QtGui.QLabel(self.help_message_line)
        self.application_icon.setProperty("maximumSize", QtCore.QSize(64, 64))
        self.application_icon.setObjectName("application_icon")
        self.help_message_line_layout.addWidget(self.application_icon)
        self.message_label = QtGui.QLabel(self.help_message_line)
        self.message_label.setWordWrap(True)
        self.message_label.setOpenExternalLinks(True)
        self.message_label.setObjectName("message_label")
        self.help_message_line_layout.addWidget(self.message_label)
        self.help_layout.addWidget(self.help_message_line)
        self.message_long_label = scroll_label(about)
        self.message_long_label.setWordWrap(True)
        self.message_long_label.setOpenExternalLinks(True)
        self.message_long_label.setObjectName("message_long_label")
        self.help_layout.addWidget(self.message_long_label)
        self.action_layout = QtGui.QHBoxLayout()
        self.action_layout.setObjectName("action_layout")
        spacerItem = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.action_layout.addItem(spacerItem)
        self.button_logs = QtGui.QPushButton(about)
        self.button_logs.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.button_logs.setObjectName("button_logs")
        self.action_layout.addWidget(self.button_logs)
        self.button_ok = QtGui.QPushButton(about)
        self.button_ok.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.button_ok.setObjectName("button_ok")
        self.action_layout.addWidget(self.button_ok)
        self.help_layout.addLayout(self.action_layout)

        self.retranslateUi(about)
        QtCore.QMetaObject.connectSlotsByName(about)

    def retranslateUi(self, about):
        about.setWindowTitle(QtGui.QApplication.translate("Help", "Netsplice Help", None, QtGui.QApplication.UnicodeUTF8))
        self.button_logs.setText(QtGui.QApplication.translate("Help", "Logs", None, QtGui.QApplication.UnicodeUTF8))
        self.button_ok.setText(QtGui.QApplication.translate("Help", "Ok", None, QtGui.QApplication.UnicodeUTF8))

from netsplice.gui.widgets.scroll_label import scroll_label
