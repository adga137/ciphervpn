# -*- coding: utf-8 -*-
# mainwindow.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Main window for Netsplice.
'''

import sys

from PySide import QtCore, QtGui
from PySide.QtGui import (
    QMenu
)
from PySide.QtCore import (
    QTimer, Qt
)

from netsplice.config import about as config_about
from netsplice.config import backend as config_backend
from netsplice.config import gui as config

from netsplice.gui import icons_rc
from netsplice.gui import locale_rc
from netsplice.gui import get_menu_items
from netsplice.gui.mainwindow.mainwindow_ui import Ui_MainWindow
from netsplice.gui.mainwindow.theme import theme

from netsplice.gui.model.account_list_item import (
    account_list_item as account_list_item_model
)
from netsplice.gui.model.group_item import (
    group_item as group_item_model
)
from netsplice.gui.preferences.qt_settings import qt_settings
from netsplice.util import get_logger, basestring
from netsplice.util.errors import NotFoundError

logger = get_logger()

ACTIVITY_STARTUP = 0
ACTIVITY_WELCOME = 1
ACTIVITY_FATAL_SHUTDOWN = 2
ACTIVITY_ACCOUNTS = 3

MAX_CONFIG_SIZE = 65536

QUERY_RESTART_TIMEOUT = 300

PLUGABLE_MENU_NAMES = [
    'menu_actions',
    'menu_preferences',
    'menu_debug',
    'menu_help',
    'menubar'
]


class mainwindow(QtGui.QMainWindow):
    '''
    Main window for login and presenting status updates to the user.
    '''

    # Signals
    new_updates = QtCore.Signal(object)
    raise_window = QtCore.Signal([])
    backend_done = QtCore.Signal(basestring)
    backend_failed = QtCore.Signal(basestring)
    create_account_from_text = QtCore.Signal(basestring)

    def __init__(self, application):
        '''
        Constructor for the main window.
        '''
        QtGui.QMainWindow.__init__(self)

        self.backend = application.get_dispatcher()

        # Setup UI
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.accounts_layout.setContentsMargins(0, 0, 2, 0)
        self.ui.activity_layout.setContentsMargins(0, 0, 0, 0)
        self.ui.main_buttons_layout.setContentsMargins(4, 0, 3, 0)
        self.menuBar().setNativeMenuBar(not sys.platform.startswith('linux'))
        self.settings = qt_settings()
        self.theme = theme()
        self.settings.load()
        self.theme.change(config.THEME)

        # Used to know if we are in the final steps of quitting
        self._quitting = False
        self._finally_quitting = False
        self._system_quit = False
        self._welcome_closed = False
        self._was_available = False
        self._backend_not_available_count = 0

        # early deferral of query notifications
        self._query_private_key_password_timer = None
        self._query_username_password_timer = None

        # Menu

        self.ui.action_help.triggered.connect(self._help)
        self.ui.action_preferences.triggered.connect(self._show_preferences)
        self.ui.action_show_logs.triggered.connect(self._show_application_log)
        self.ui.action_about.triggered.connect(self._about)
        self.ui.action_quit.triggered.connect(self.quit)
        self.ui.action_create_account_activity.triggered.connect(
            self._account_create)
        self.create_account_from_text.connect(self._account_create_from_text)
        self.ui.action_connect_all.triggered.connect(self._connect_all)

        self._set_plugin_menu_items()
        self._set_shortcut_keys()

        # welcome
        self.ui.button_close_welcome.clicked.connect(self._close_welcome)
        message = self.tr(config.WELCOME_TEXT)
        self.ui.welcome_text.setText(message.format(
            documentation_url=config_about.DOC_URL))
        self.ui.welcome_text.setTextInteractionFlags(
            Qt.TextBrowserInteraction)
        # startup
        self.ui.startup_text.setText(self.tr(config.STARTUP_WAIT_MESSAGE))

        # darwin and win32 need a user-confirm or password entry to start
        # the privileged process. the correct fix is to install the privileged
        # as win32 service and suid the osx binary.
        manual_elevate_privileged = sys.platform.startswith('win32') \
            or sys.platform.startswith('darwin')
        if manual_elevate_privileged:
            self.ui.startup_text_manual_elevate.setText(
                self.tr(config.STARTUP_WAIT_MESSAGE_MANUAL_ELEVATE))
            self.ui.startup_text_manual_elevate.show()

        # fatal
        self.ui.button_fatal_quit.clicked.connect(self.quit)
        self.ui.fatal_text.setText(self.tr(config.FATAL_TEXT))

        # accounts
        self.ui.account_list.set_mainwindow(self)
        self.ui.account_list.account_create.connect(
            self._account_create)
        self.ui.account_list.account_log.connect(
            self._account_log)
        self.ui.account_list.account_settings.connect(
            self._account_settings)
        self.ui.account_list.group_create.connect(
            self._group_create)
        self.ui.account_list.group_settings.connect(
            self._group_settings)
        self.ui.button_create_account.clicked.connect(
            self._account_create)
        self.ui.button_connect_accounts.clicked.connect(
            self._connect_accounts)
        self._connect_accounts_menu = QMenu(self)
        self.ui.button_connect_accounts.setMenu(self._connect_accounts_menu)
        self._accounts = None
        self._groups = None
        self._grouped_accounts = None
        self._connections = None
        self._failed_connections = None
        self._dialogs = {}

        event_loop = self.backend.application.event_loop
        event_loop.connection_error.connect(self._connection_log)
        event_loop.notify_error.connect(self._notify_error_log)
        event_loop.query_username_password.connect(
            self._query_username_password)
        event_loop.query_private_key_password.connect(
            self._query_private_key_password)
        event_loop.preferences_changed.connect(
            self._backend_preferences_changed)
        event_loop.connections_changed.connect(
            self._backend_connections_changed)

        statusbar_module = self.backend.application.get_module('statusbar')
        self.statusbar = statusbar_module.initialize(
            self, self.backend)
        self.setStatusBar(self.statusbar)

        self.backend.available.connect(self._backend_available)
        self.backend.not_available.connect(self._backend_not_available)
        self.backend.process_gone.connect(self._backend_gone)
        self.backend.startup_progress.connect(self._startup_progress)

        self.raise_window.connect(self._raise_mainwindow)
        self._systray = None
        self._show_systray()
        # last minute UI manipulations
        self._set_window_position()
        if config.START_HIDDEN:
            self.ensure_invisible()
        else:
            self.ensure_visible()

        self.backend.check_available.emit()

        try:
            self.backend.qt_backend_bridge(QtCore.QTimer)
        except AttributeError:
            pass

    def _about(self):
        '''
        Display the 'About Netsplice' dialog.
        '''
        try:
            self.get_dialog('about').show()
        except NotFoundError:
            about_module = self.backend.application.get_module(
                'about')
            self.add_dialog(
                'about', about_module.initialize(self, self.backend))
            self.get_dialog('about').show()

    def _account_create(self, parent_group_id=None):
        '''
        Account Create.

        Create an account with the given parent group id.
        Initialize the account_edit module if not yet initialized. Reset the
        form contents and show the dialog.
        '''
        dialog = None
        try:
            dialog = self.get_dialog('account_edit')
        except NotFoundError:
            account_edit_module = self.backend.application.get_module(
                'account_edit')
            self.add_dialog(
                'account_edit', account_edit_module.initialize(
                    self, self.backend))
            dialog = self.get_dialog('account_edit')
        root_group_id = self._groups.find_root().id.get()
        global_group_id = self._groups.find_global().id.get()
        if parent_group_id is None:
            parent_group_id = global_group_id
        if parent_group_id == root_group_id:
            parent_group_id = global_group_id

        dialog.reset()
        dialog.create(parent_group_id)
        dialog.show()

    def _account_create_from_text(self, config_text):
        '''
        Account Create.

        Create a account with the given config.
        Initialize the account_edit module if not yet initialized, reset the
        form contents and show the dialog.
        '''
        dialog = None
        try:
            dialog = self.get_dialog('account_edit')
        except NotFoundError:
            account_edit_module = self.backend.application.get_module(
                'account_edit')
            self.add_dialog(
                'account_edit', account_edit_module.initialize(
                    self, self.backend))
            dialog = self.get_dialog('account_edit')

        dialog.reset()
        dialog.create(None)
        # account_item = account_edit_item_model()
        # account_item.id.set(str(uuid.uuid4()))
        # account_item.name.set(self.tr('Dropped Config'))
        # account_item.configuration.set(config_text)
        # account_item.import_configuration.set(config_text)
        # dialog.model_loaded_signal.emit(account_item)
        logger.warn('No implementation for create with text yet')
        dialog.show()

    def _account_log(self, model_json):
        '''
        Display log for account only.

        Configure the logviewer with an extra filter for account_id of
        model. When the logviewer already is open, reconfigure it and raise.

        :param model_json: account_list_item_model json string.
        '''
        account_item = account_list_item_model()
        account_item.from_json(model_json)

        dialog = None
        try:
            dialog = self.get_dialog('logviewer')
        except NotFoundError:
            logviewer_module = self.backend.application.get_module(
                'logviewer')
            self.add_dialog(
                'logviewer', logviewer_module.initialize(
                    self, self.backend))
            dialog = self.get_dialog('logviewer')

        dialog.set_filter({
            'extra': {
                'account_id': account_item.id.get()
            },
            'text': '',
            'case_sensitive': False,
            'level': 'DEBUG, INFO, WARNING, ERROR, CRITICAL'
        })
        dialog.show()

    def _account_settings(self, model_json):
        '''
        Account Settings.

        Modify an account with the given model. The model has to contain an
        account with all attributes.
        Initialize the account_edit module if not yet initialized, reset the
        form contents, load the account_list_item and show the dialog.
        '''
        dialog = None
        try:
            dialog = self.get_dialog('account_edit')
        except NotFoundError:
            account_edit_module = self.backend.application.get_module(
                'account_edit')
            self.add_dialog(
                'account_edit', account_edit_module.initialize(
                    self, self.backend))
            dialog = self.get_dialog('account_edit')
        account_model_instance = account_list_item_model()
        account_model_instance.from_json(model_json)
        dialog.reset()
        dialog.load(account_model_instance.id.get())
        dialog.show()

    def _backend_available(self):
        '''
        Backend available.

        The backend has become available. Notify the systray, request the
        backend preferences and model and hide the startup progress.
        When the backend already was up once, no notification will be shown.
        '''
        if self._was_available:
            # Application returned from suspend / hanging pc
            # do not notify
            self._backend_not_available_count = 0
            # restart scheduled get of models
            self.backend.events_get.emit()
            self.backend.preferences_events_get.emit()
            self._systray.set_backend_available(True)
        else:
            if self._systray is not None:
                self._systray.set_backend_available(True)
            # get the models and schedule get of models
            self.backend.model_get.emit()
            self.backend.preferences_get.emit()
            self.backend.events_get.emit()
            self.backend.preferences_events_get.emit()
        self._was_available = True
        self.ui.startup_text.hide()
        self.ui.startup_message.hide()
        self.ui.startup_progress.hide()
        if config.SHOW_WELCOME_ON_START and not self._welcome_closed:
            self._raise_activity(ACTIVITY_WELCOME)
            self._welcome_closed = True
        else:
            self._raise_activity(ACTIVITY_ACCOUNTS)

    def _backend_connections_changed(self, connections, failed_connections):
        '''
        Backend Connections Changed.

        The backend connections have changed. Update the private connections
        caches.
        '''
        self._connections = connections
        self._failed_connections = failed_connections
        self._update_connect_accounts()

    def _backend_not_available(self):
        '''
        Backend Not Available.

        The backend has become unavailable. Notify the systray when the backend
        does not come up after 10 cycles.
        '''
        if self._backend_not_available_count > 10:
            if self._systray is not None:
                self._systray.set_backend_available(False)
        self._backend_not_available_count += 1

    def _backend_gone(self):
        '''
        Backend Gone.

        The backend has shutdown, display a message to the user along with
        the latest log entries.
        '''
        self._raise_activity(ACTIVITY_FATAL_SHUTDOWN)
        if not config.SHOW_LOG_ON_BACKEND_GONE:
            return
        self._show_application_log()

        dialog = None
        try:
            dialog = self.get_dialog('logviewer')
        except NotFoundError:
            logviewer_module = self.backend.application.get_module(
                'logviewer')
            self.add_dialog(
                'logviewer', logviewer_module.initialize(
                    self, self.backend))
            dialog = self.get_dialog('logviewer')
        dialog._backend_gone()

    def _backend_preferences_changed(self, model):
        '''
        Backend Preferences Changed.

        The backend preferences have changed. Update the early gui settings
        (QSettings), remember the available accounts and set the shortcut
        keys.
        '''
        self.settings.update_gui(model.ui)
        self.settings.update_backend(model.backend)
        self.settings.store()

        self.theme.change(config.THEME)
        self._accounts = model.accounts
        self._groups = model.groups
        self._grouped_accounts = model.grouped_accounts
        if len(self._accounts):
            self.ui.button_create_account.hide()
            self.ui.button_connect_accounts.show()
            self._update_connect_accounts()
        else:
            self.ui.button_create_account.show()
            self.ui.button_connect_accounts.hide()
        self._set_shortcut_keys()

    def _close_welcome(self):
        '''
        Close Welcome message.

        Remember the decision of the user wether to show 'Welcome' message
        on every startup and show the accounts.
        '''
        if self.ui.show_welcome_on_start.isChecked():
            self.backend.preferences_value_set.emit(
                'ui', 'show_welcome_on_start', True,
                self.backend_done, self.backend_failed)
        else:
            self.backend.preferences_value_set.emit(
                'ui', 'show_welcome_on_start', False,
                self.backend_done, self.backend_failed)

        # Show Account List, regardless of choice of checkbox.
        self._raise_activity(ACTIVITY_ACCOUNTS)

    def _connect_all(self):
        '''
        Connect All.

        Connect all known accounts.
        '''
        self.backend.group_connect.emit(
            self.ui.account_list.groups.find_root().id.get(),
            self.backend_done, self.backend_failed)

    def _connect_account(self):
        '''
        Connect Account.

        Connect a single account.
        '''
        sender_action = self.sender()
        self.backend.account_connect.emit(
            sender_action.account_id,
            self.backend_done, self.backend_failed)

    def _connect_accounts(self):
        '''
        Connect Accounts.

        Connect all known accounts when all accounts are in the global
        group. Otherwise the connect_accounts_action that triggered the
        event has added the group_id which has to be connected.
        '''
        sender_action = self.sender()
        try:
            self.backend.group_connect.emit(
                sender_action.group_id,
                self.backend_done, self.backend_failed)
        except AttributeError:
            if len(self._accounts) < 1 or len(self._groups) > 2:
                raise
            self.backend.group_connect.emit(
                self.ui.account_list.groups.find_root().id.get(),
                self.backend_done, self.backend_failed)

    def _connection_log(self, connection_id):
        '''
        Display log for connection only.

        Configure the logviewer with an extra filter for connection_id of
        the model. When the logviewer already is open, reconfigure it and
        raise.

        :param: connection_id: id of connection that should be shown.
        '''
        if not config.SHOW_LOG_ON_CONNECTION_FAILURE:
            return

        dialog = None
        try:
            dialog = self.get_dialog('logviewer')
        except NotFoundError:
            logviewer_module = self.backend.application.get_module(
                'logviewer')
            self.add_dialog(
                'logviewer', logviewer_module.initialize(
                    self, self.backend))
            dialog = self.get_dialog('logviewer')
        extra = dict()
        if self._connections is not None:
            account_id = None
            try:
                connection = self._connections.find_by_id(connection_id)
                account_id = connection.account_id.get()
            except NotFoundError:
                try:
                    connection = self._failed_connections.find_by_id(
                        connection_id)
                    account_id = connection.account_id.get()
                except NotFoundError:
                    pass
            if account_id is not None:
                extra['account_id'] = account_id

        dialog.set_filter({
            'extra': extra,
            'text': '',
            'case_sensitive': False,
            'level': 'WARNING, ERROR, CRITICAL'
        })
        if not dialog.isVisible():
            dialog.show()

    def _group_create(self, parent_group_id=None):
        '''
        Group Create.

        Create a group with the given parent group id.
        Initialize the group_edit module if not yet initialized, reset the
        form contents and show the dialog.
        '''
        dialog = None
        try:
            dialog = self.get_dialog('group_edit')
        except NotFoundError:
            group_edit_module = self.backend.application.get_module(
                'group_edit')
            self.add_dialog(
                'group_edit', group_edit_module.initialize(
                    self, self.backend))
            dialog = self.get_dialog('group_edit')

        dialog.reset()
        dialog.create(parent_group_id)
        dialog.show()

    def _group_settings(self, model_json):
        '''
        Group Settings.

        Modify a group with the given model. The model has to contain a
        group with with all attributes.
        Initialize the group_edit module if not yet initialized, reset the
        form contents, load the group_list_item and show the dialog.
        '''
        dialog = None
        try:
            dialog = self.get_dialog('group_edit')
        except NotFoundError:
            group_edit_module = self.backend.application.get_module(
                'group_edit')
            self.add_dialog(
                'group_edit', group_edit_module.initialize(
                    self, self.backend))
            dialog = self.get_dialog('group_edit')
        group_model_instance = group_item_model()
        group_model_instance.from_json(model_json)
        dialog.reset()
        dialog.load(group_model_instance.id.get())
        dialog.show()

    def _help(self):
        '''
        Display the Netsplice help dialog.
        '''
        dialog = None
        try:
            dialog = self.get_dialog('help')
        except NotFoundError:
            help_module = self.backend.application.get_module(
                'help')
            self.add_dialog(
                'help',
                help_module.initialize(self, self.backend))
            dialog = self.get_dialog('help')
        dialog.show()

    def _notify_error_log(self, filter):
        '''
        Display log for given filter.

        Configure the logviewer with a extra filter of model.
        When the logviewer already is open, reconfigure it and raise.

        :param: filter: string (error-code) that should be shown.
        '''
        if not config.SHOW_LOG_ON_BACKEND_ERROR:
            return

        dialog = None
        try:
            dialog = self.get_dialog('logviewer')
        except NotFoundError:
            logviewer_module = self.backend.application.get_module(
                'logviewer')
            self.add_dialog(
                'logviewer', logviewer_module.initialize(
                    self, self.backend))
            dialog = self.get_dialog('logviewer')

        if not dialog.isVisible():
            dialog.set_filter({
                'text': str(filter),
                'case_sensitive': False,
                'level': 'WARNING, ERROR, CRITICAL'
            })
            dialog.show()

    def _query_private_key_password(self, account_id):
        '''
        Display a Private-Key-Password Dialog.

        Display a dialog that requests a password from the user. The
        result will be sent to the backend.
        Always recreates the dialog as the dialog deletes itself on close.

        :param account_id: id of account that requires the username/password.
        '''
        if self._accounts is None:
            self._query_private_key_password_timer = QTimer(self)
            self._query_private_key_password_timer.timeout.connect(
                lambda: self._query_private_key_password(account_id))
            self._query_private_key_password_timer.setInterval(
                QUERY_RESTART_TIMEOUT)
            self._query_private_key_password_timer.setSingleShot(True)
            self._query_private_key_password_timer.start()
            return
        credential_module = self.backend.application.get_module(
            'credential')
        dialog = credential_module.initialize(
            self, self.backend)
        try:
            dialog.set_account(
                self._accounts.find_by_id(account_id))
            dialog.set_type(dialog.TYPE_PRIVATE_KEY_PASSWORD)
            dialog.show()
        except NotFoundError:
            dialog.cancel_action()

    def _query_username_password(self, account_id):
        '''
        Display a Username-Password Dialog.

        Display a dialog that requests username and password from the user. The
        result will be sent to the backend.
        Always recreates the dialog as the dialog deletes itself on close.

        :param account_id: id of account that requires the username/password.
        '''
        if self._accounts is None:
            self._query_username_password_timer = QTimer(self)
            self._query_username_password_timer.timeout.connect(
                lambda: self._query_username_password(account_id))
            self._query_username_password_timer.setInterval(
                QUERY_RESTART_TIMEOUT)
            self._query_username_password_timer.setSingleShot(True)
            self._query_username_password_timer.start()
            return
        credential_module = self.backend.application.get_module(
            'credential')
        dialog = credential_module.initialize(
            self, self.backend)
        try:
            dialog.set_account(
                self._accounts.find_by_id(account_id))
            dialog.set_type(dialog.TYPE_USERNAME_PASSWORD)
            dialog.show()
        except NotFoundError:
            dialog.cancel_action()

    def _quit_final(self):
        '''
        Final steps to quit the app, starting from here we don't care about
        running services or user interaction, just quit.
        '''
        # We can end up here because all the services are stopped or because a
        # timeout was triggered. Since we want to run this only once, we exit
        # if this is called twice.
        if self._finally_quitting:
            return

        self._finally_quitting = True

        self.close()

    def _raise_activity(self, activity_index):
        '''
        Raise Activity.

        Set the widget-stack to the given activity index.
        '''
        self.ui.activity.setCurrentIndex(activity_index)

    def _raise_mainwindow(self):
        '''
        Raise Mainwindow

        Triggered when we receive a RAISE_WINDOW event.
        '''
        TOPFLAG = QtCore.Qt.WindowStaysOnTopHint
        self.setWindowFlags(self.windowFlags() | TOPFLAG)
        self.show()
        self.setWindowFlags(self.windowFlags() & ~TOPFLAG)
        self.show()
        if sys.platform.startswith('darwin'):
            self.raise_()

    def _set_plugin_menu_items(self):
        '''
        Set Plugin Menu Items.

        Populate the main menu with actions defined by plugins.
        '''
        for plugin_menu_name in PLUGABLE_MENU_NAMES:
            try:
                menu_items = get_menu_items(plugin_menu_name)
                menu = self.ui.__dict__[plugin_menu_name]
            except KeyError:
                # No plugin entries for plugin_menu_name defined
                # or the menu in the ui-file is not defined
                continue
            for menu_item in menu_items:
                before_actions = menu.actions()
                before_action = None
                if menu_item['before'] is None:
                    # Auto before last separator
                    for action in before_actions:
                        if action.isSeparator():
                            before_action = action
                else:
                    # Discover the action or menu with the defined name
                    for action in before_actions:
                        object_name = action.objectName()
                        if object_name == '' and action.menu() is not None:
                            object_name = action.menu().objectName()
                        if object_name == menu_item['before']:
                            before_action = action
                            break

                menu_action = QtGui.QAction(self)
                menu_action.setEnabled(True)
                if menu_item['separator']:
                    menu_action.setSeparator(True)
                else:
                    menu_action.setText(menu_item['label'])
                    if menu_item['callback'] is not None:
                        menu_action.triggered.connect(
                            menu_item['callback'])
                    object_name = (
                        '%s_%s' % (plugin_menu_name, menu_item['name']))
                    menu_action.setObjectName(object_name)
                    menu_action.setShortcut(QtGui.QKeySequence(
                        menu_item['shortcut']))

                self.ui.__dict__[plugin_menu_name].insertAction(
                    before_action, menu_action)

    def _set_shortcut_keys(self):
        '''
        Set Shortcut Keys.

        Set the key shortcuts.
        '''
        self.ui.action_help.setShortcut(QtGui.QKeySequence(
            config.SHORTCUT_HELP))
        self.ui.action_help.setShortcutContext(QtCore.Qt.ApplicationShortcut)

        self.ui.action_connect_all.setShortcut(QtGui.QKeySequence(
            config.SHORTCUT_CONNECT_ALL))
        self.ui.action_connect_all.setShortcutContext(
            QtCore.Qt.ApplicationShortcut)

        self.ui.action_create_account_activity.setShortcut(QtGui.QKeySequence(
            config.SHORTCUT_CREATE_CONNECTION))
        self.ui.action_create_account_activity.setShortcutContext(
            QtCore.Qt.ApplicationShortcut)

        self.ui.action_show_logs.setShortcut(QtGui.QKeySequence(
            config.SHORTCUT_SHOW_LOGS))
        self.ui.action_show_logs.setShortcutContext(
            QtCore.Qt.ApplicationShortcut)

        self.ui.action_preferences.setShortcut(QtGui.QKeySequence(
            config.SHORTCUT_SHOW_PREFERENCES))
        self.ui.action_preferences.setShortcutContext(
            QtCore.Qt.ApplicationShortcut)

        self.ui.action_quit.setShortcut(QtGui.QKeySequence(
            config.SHORTCUT_QUIT))
        self.ui.action_quit.setShortcutContext(QtCore.Qt.ApplicationShortcut)

    def _set_window_position(self):
        '''
        Set Window Position.

        Center the main window based on the desktop geometry.
        When settings (QSettings) for the geometry are available, use the
        settings.
        '''
        geometry = self.settings.get_mainwindow_geometry()

        if geometry is None:
            app = QtGui.QApplication.instance()
            width = app.desktop().width()
            height = app.desktop().height()
            window_width = self.size().width()
            window_height = self.size().height()
            x = width - window_width - (window_width / 4.0)
            y = (height / 2.0) - (window_height / 2.0)
            self.move(x, y)
        else:
            self.restoreGeometry(geometry)

    def _show_application_log(self, log_filter=None):
        '''
        Display all available log items.

        Show the window with the history of messages logged until now
        and display the new ones on receival. Configure an existing
        logviewer to set the given filters.
        '''

        dialog = None
        try:
            dialog = self.get_dialog('logviewer')
        except NotFoundError:
            logviewer_module = self.backend.application.get_module(
                'logviewer')
            self.add_dialog(
                'logviewer', logviewer_module.initialize(
                    self, self.backend))
            dialog = self.get_dialog('logviewer')
        dialog.set_filter(log_filter)
        dialog.show()

    def _show_preferences(self):
        '''
        TRIGGERS:
            self.ui.btnPreferences.clicked (disabled for now)
            self.ui.action_preferences

        Display the preferences window.
        '''

        dialog = None
        try:
            dialog = self.get_dialog('preferences')
        except NotFoundError:
            preferences_module = self.backend.application.get_module(
                'preferences')
            self.add_dialog(
                'preferences', preferences_module.initialize(
                    self, self.backend))
            dialog = self.get_dialog('preferences')
        dialog.show()

    def _show_systray(self):
        '''
        Sets up the systray icon.
        '''
        if self._systray is not None:
            self._systray.setVisible(True)
            return

        module = self.backend.application.get_module('systray')
        self._systray = module.initialize(self, self.backend)

    def _update_connect_accounts(self):
        '''
        Update the connect accounts button with the available groups as
        dropdown.
        '''
        self._connect_accounts_menu.clear()
        if self._groups is None:
            # On startup, before the groups are loaded. When they are
            # loaded this method is called again.
            return
        global_group = self._groups.find_global()
        root_group = self._groups.find_root()
        if len(self._accounts) > 0:
            for account in self._accounts:
                account_name = account.name.get()
                menu_action = self._connect_accounts_menu.addAction(
                    account_name)
                menu_action.account_id = account.id.get()
                menu_action.triggered.connect(self._connect_account)
                try:
                    connection = self._connections.find_by_account_id(
                        account.id.get())
                    if not connection.is_disconnected():
                        menu_action.setEnabled(False)
                except AttributeError:
                    pass
                except NotFoundError:
                    pass

        if len(self._accounts) > 0 and len(self._groups) > 0:
            menu_action = self._connect_accounts_menu.addAction(
                self.tr('Account/Group Separator'))
            menu_action.setEnabled(True)
            menu_action.setSeparator(True)

        if len(self._groups) > 0:
            for group in self._groups:
                if group.id.get() == root_group.id.get():
                    continue
                group_name = group.name.get()
                if group.id.get() == global_group.id.get():
                    group_name = self.tr(config.GLOBAL_GROUP_NAME)
                menu_action = self._connect_accounts_menu.addAction(
                    group_name)
                menu_action.group_id = group.id.get()
                menu_action.triggered.connect(self._connect_accounts)
            menu_action = self._connect_accounts_menu.addAction(
                self.tr('Create Connection Separator'))
            menu_action.setEnabled(True)
            menu_action.setSeparator(True)
        menu_action = self._connect_accounts_menu.addAction(
            self.tr('Create Connection'))
        menu_action.triggered.connect(self._account_create)
        if len(self._connect_accounts_menu.actions()) > 1:
            self.ui.button_connect_accounts.setMenu(
                self._connect_accounts_menu)
        else:
            self.ui.button_connect_accounts.setMenu(None)

    def _startup_progress(self, message):
        '''
        Startup Progress.

        Display a message from the backend stdout message to show current
        task in progress bar.
        '''
        startup_message = message.replace(
            '%s: ' % (config_backend.BACKEND_STARTUP_MESSAGE,),
            '').replace('\n', '')
        words = startup_message.split(' ')
        progress_value = int(words[0])
        del words[0]
        startup_message = ' '.join(words).encode('utf-8')

        self.ui.startup_progress.setValue(progress_value)
        self.ui.startup_message.setText(self.tr(startup_message))

    # QMainWindow overloads.

    def closeEvent(self, e):
        '''
        Reimplementation of closeEvent to close to tray.
        '''
        if not e.spontaneous():
            # If the system requested the `close` then we should quit.
            self._system_quit = True
            self.quit(disable_autostart=False)
            return

        if QtGui.QSystemTrayIcon.isSystemTrayAvailable() and \
                config.CLOSE_TO_SYSTRAY:
            self.ensure_invisible()
            e.ignore()
            return
        else:
            self.quit()

        # self._settings.set_geometry(self.saveGeometry())
        # self._settings.set_windowstate(self.saveState())

        QtGui.QMainWindow.closeEvent(self, e)

    # Public interface.

    def accounts_activity(self):
        '''
        Raise account activity.

        Public interface to allow other widgets to ensure that the accounts are
        displayed.
        '''
        self._raise_activity(ACTIVITY_ACCOUNTS)

    def add_dialog(self, name, dialog_instance):
        '''
        Add Dialog.

        Add the given dialog to the dialog_map.

        Arguments:
            name (string): name of the dialog
            dialog_instance (QDialog): instance of a dialog.
        '''
        self._dialogs[name] = dialog_instance

    def ensure_invisible(self):
        '''
        TRIGGERS:
            self._action_visible.triggered

        Ensure that the window is hidden.
        '''
        # We set this in order to avoid dialogs shutting down the
        # app on close, as they will be the only visible window.
        # e.g.: PreferencesWindow, LoggerWindow
        QtGui.QApplication.setQuitOnLastWindowClosed(False)
        self.hide()

    def ensure_visible(self):
        '''
        TRIGGERS:
            self._action_visible.triggered

        Ensure that the window is visible and raised.
        '''
        QtGui.QApplication.setQuitOnLastWindowClosed(True)
        self.show()

        if sys.platform.startswith('linux'):
            # On Ubuntu, activateWindow doesn't work reliably, so
            # we do the following as a workaround. See
            # https://bugreports.qt-project.org/browse/QTBUG-24932
            # for more details.
            QtGui.QX11Info.setAppUserTime(0)

        self.activateWindow()
        self.raise_()

    def get_dialog(self, name):
        '''
        Get Dialog.

        Return a registered dialog by name.

        Arguments:
            name (string): name of the dialog

        Returns:
            QDialog -- Instance of the dialog.

        Raises:
            NotFoundError -- No dialog with the given name found.
        '''
        if name not in self._dialogs.keys():
            raise NotFoundError('%s is not a dialog in mainwindow')
        return self._dialogs[name]

    def quit(self, disable_autostart=True):
        '''
        Start the quit sequence and wait for services to finish.
        Cleanup and close the main window before quitting.

        :param disable_autostart: whether we should disable the autostart
                                  feature or not
        :type disable_autostart: bool
        '''
        if self._quitting:
            return

        self._quitting = True
        self.backend.shutdown.emit()
        self.settings.store_mainwindow_geometry(self.saveGeometry())

        # First thing to do quitting, hide the mainwindow and show tooltip
        self.hide()
        if not self._system_quit and self._systray is not None:
            self._systray.show_quit_message()

        # Explicitly process events to display tooltip immediately
        QtCore.QCoreApplication.processEvents(0, 10)

        # Set this in case that the app is hidden
        QtGui.QApplication.setQuitOnLastWindowClosed(True)

        self._quit_final()
