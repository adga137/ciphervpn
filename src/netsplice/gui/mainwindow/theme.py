# -*- coding: utf-8 -*-
# theme.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Theme for application.

Loader for themes.
Supports QSS files with less mixins for named colors.
'''

import os
import sys
from PySide.QtCore import QFile, QTextStream, QCoreApplication
from netsplice.config import gui as config_gui
import netsplice.gui.themes_rc


class theme(object):
    '''
    Theme object.

    Loads the theme, defines fallback/default.
    '''

    def __init__(self):
        '''
        Initialize object.

        Sets and loads defaults.
        '''
        self.default_less = ''
        self.default_qss = ''
        self.name = None
        self.less = ''
        self.qss = ''
        self._load_rc_default()

    def _get_qss(self):
        '''
        Getter for QSS.

        Transforms QSS with LESS variables to QSS that Qt can load.
        '''
        if self.qss == '':
            self.qss = self.default_qss
        if self.less == '':
            self.less = self.default_less

        qss = self.qss
        if sys.platform.startswith('darwin'):
            qss += config_gui.OSX_OVERRIDE_QSS

        for line in self.less.split('\n'):
            if ':' not in line or ';' not in line:
                continue
            rule_parts = line.split(':')
            rule_name = rule_parts[0]
            rule_value_parts = rule_parts[1].split(';')
            rule_value = rule_value_parts[0].strip()
            qss = qss.replace(rule_name, rule_value)
        return qss

    def _load_file_name(self):
        '''
        Load Theme from Filename.

        Tries to load the theme by name. When the files cannot be found,
        the QSS/LESS members are not modified.
        '''
        file_qss = '%s.qss' % (self.name,)
        file_less = '%s.less' % (self.name,)
        if os.path.exists(file_qss):
            with open(file_qss) as file_handle:
                self.qss = file_handle.read()
        if os.path.exists(file_less):
            with open(file_less) as file_handle:
                self.less = file_handle.read()

    def _load_rc_default(self):
        '''
        Load default Theme.

        Loads the default theme that will be used when the name cannot be
        found as file or as resource.
        '''
        rc_default_qss = QFile(':/themes/default.qss')
        rc_default_less = QFile(':/themes/default.less')
        rc_default_qss.open(QFile.ReadOnly | QFile.Text)
        ts = QTextStream(rc_default_qss)
        self.default_qss = ts.readAll()
        rc_default_less.open(QFile.ReadOnly | QFile.Text)
        ts = QTextStream(rc_default_less)
        self.default_less = ts.readAll()

    def _load_rc_name(self):
        '''
        Load theme from resource.

        Loads a theme that is compiled as resource file. When no resource
        with the given name exists, QSS/LESS members are not modified.
        '''
        rc_qss = QFile(':/themes/%s.qss' % (self.name,))
        rc_less = QFile(':/themes/%s.less' % (self.name,))
        if rc_qss.exists():
            rc_qss.open(QFile.ReadOnly | QFile.Text)
            ts = QTextStream(rc_qss)
            self.qss = ts.readAll()
        if rc_less.exists():
            rc_less.open(QFile.ReadOnly | QFile.Text)
            ts = QTextStream(rc_less)
            self.less = ts.readAll()

    def change(self, name):
        '''
        Change the Theme.

        Loads the given name QSS and LESS files from the resource store or
        filesystem. When neither resource nor file are available, the
        default theme is used.
        '''
        if self.name == name:
            return
        self.name = name
        self.qss = ''
        self.less = ''
        self._load_rc_name()
        if self.qss == '':
            self._load_file_name()

        QCoreApplication.instance().setStyleSheet(self._get_qss())
