# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/gui/account_list/views/account_list.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_AccountList(object):
    def setupUi(self, account_list):
        account_list.setObjectName("account_list")
        self.root_layout = QtGui.QVBoxLayout(account_list)
        self.root_layout.setSizeConstraint(QtGui.QLayout.SetMinAndMaxSize)
        self.root_layout.setObjectName("root_layout")
        self.toolbar = QtGui.QToolBar(account_list)
        self.toolbar.setObjectName("toolbar")
        self.root_layout.addWidget(self.toolbar)
        self.scroll_area = QtGui.QScrollArea(account_list)
        self.scroll_area.setFrameShape(QtGui.QFrame.NoFrame)
        self.scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.scroll_area.setWidgetResizable(True)
        self.scroll_area.setObjectName("scroll_area")
        self.account_list_container = QtGui.QWidget()
        self.account_list_container.setObjectName("account_list_container")
        self.account_list_container_layout = QtGui.QVBoxLayout(self.account_list_container)
        self.account_list_container_layout.setObjectName("account_list_container_layout")
        self.account_list_items = QtGui.QWidget(self.account_list_container)
        self.account_list_items.setObjectName("account_list_items")
        self.account_list_items_layout = QtGui.QVBoxLayout(self.account_list_items)
        self.account_list_items_layout.setContentsMargins(0, 0, 0, 0)
        self.account_list_items_layout.setObjectName("account_list_items_layout")
        self.account_list_container_layout.addWidget(self.account_list_items)
        spacerItem = QtGui.QSpacerItem(0, 1, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.MinimumExpanding)
        self.account_list_container_layout.addItem(spacerItem)
        self.scroll_area.setWidget(self.account_list_container)
        self.root_layout.addWidget(self.scroll_area)

        self.retranslateUi(account_list)
        QtCore.QMetaObject.connectSlotsByName(account_list)

    def retranslateUi(self, account_list):
        pass

