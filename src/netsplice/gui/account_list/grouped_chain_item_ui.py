# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/gui/account_list/views/grouped_chain_item.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_GroupedChainItem(object):
    def setupUi(self, grouped_chain_item):
        grouped_chain_item.setObjectName("grouped_chain_item")
        self.grouped_chain_item_layout = QtGui.QVBoxLayout(grouped_chain_item)
        self.grouped_chain_item_layout.setObjectName("grouped_chain_item_layout")
        self.chain_item_widget = chain_item(grouped_chain_item)
        self.chain_item_widget.setObjectName("chain_item_widget")
        self.grouped_chain_item_layout.addWidget(self.chain_item_widget)

        self.retranslateUi(grouped_chain_item)
        QtCore.QMetaObject.connectSlotsByName(grouped_chain_item)

    def retranslateUi(self, grouped_chain_item):
        pass

from netsplice.gui.account_list.chain_item import chain_item
