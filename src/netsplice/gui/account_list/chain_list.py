# -*- coding: utf-8 -*-
# chain_list.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Widget to have multiple chain_items on the same level.
'''

from PySide.QtCore import Signal

from netsplice.gui.account_list.chain_list_ui import (
    Ui_ChainList
)
from netsplice.gui.model.chain_list import (
    chain_list as chain_list_model
)
from netsplice.gui.widgets.list_item import list_item as list_item_widget
from netsplice.util.errors import (
    NotFoundError
)


class chain_list(list_item_widget):
    '''
    Custom widget to have multiple chain_items on the same level.
    '''
    account_item_dropped = Signal(basestring, basestring, int)
    account_create = Signal(basestring)
    account_log = Signal(basestring)
    account_settings = Signal(basestring)

    def __init__(self, parent, chain_item_type, mainwindow=None, model=None):
        list_item_widget.__init__(self, parent)
        self.chain_item_type = chain_item_type
        self.mainwindow = None
        self.backend = None
        self.model = None

        # model cache
        self.accounts = parent.accounts
        self.groups = parent.groups
        self.grouped_accounts = parent.grouped_accounts
        self.chains = parent.chains
        self.chain_items = chain_list_model()

        self.ui = Ui_ChainList()
        self.ui.setupUi(self)
        self.ui.chain_list_layout.setContentsMargins(12, 0, 0, 0)
        self.set_model(model)
        self.set_mainwindow(mainwindow)

    def _add_new_chain_items(self):
        '''
        Add chain items.

        Create a chain item in the current group for each item that
        is in the model but not in the widget.
        '''
        for chain_item_model in self.chain_items:
            if self.has_item(chain_item_model):
                continue
            child_widget = self.chain_item_type(
                self, self.mainwindow, chain_item_model)

            child_widget.update_model(
                self.accounts, self.groups, self.grouped_accounts,
                self.chains)
            child_widget.account_create.connect(self.account_create)
            child_widget.account_log.connect(self.account_log)
            child_widget.account_settings.connect(self.account_settings)
            child_widget.account_item_dropped.connect(
                self.account_item_dropped)
            child_widget.selected.connect(self.selected)
            self.add_item(child_widget)

    def _order_weighted_widget(self, widget_item):
        '''
        Order weighted Widget.

        Return the weight of the given widget item. This method is used for
        the sort.

        Arguments:
            widget_item (list_widget_item): Widget in the group.

        Returns:
            integer -- weight of the widget.
        '''
        return widget_item['weight']

    def _remove_abandoned_chain_items(self):
        '''
        Remove Abandoned Chain items.

        Remove items that are no longer in the preferences of the backend.
        '''
        removed_widgets = []
        for index in range(0, self.ui.chain_list_layout.count()):
            layout_item = self.ui.chain_list_layout.itemAt(index)
            widget = layout_item.widget()
            if not isinstance(widget, (self.chain_item_type,)):
                continue
            try:
                self.chain_items.find_by_id(
                    widget.model.id.get())
            except NotFoundError:
                removed_widgets.append(widget)

        for widget in removed_widgets:
            self.remove_item(widget)

    def _sort_items(self):
        '''
        Sort Items.

        Sort items in the chain list.
        '''
        sortable_widgets = []
        sorted_layout_items = []

        # Collect group items
        for index in range(0, self.ui.chain_list_layout.count()):
            layout_item = self.ui.chain_list_layout.itemAt(index)
            widget = layout_item.widget()
            if not isinstance(widget, (self.chain_item_type,)):
                continue
            chain = self.chains.find_by_id(widget.model.id.get())
            sortable_widgets.append({
                'widget': widget,
                'weight': chain.weight.get()})

        sorted_widgets = sorted(
            sortable_widgets,
            key=self._order_weighted_widget)
        reverse_sorted_widgets = reversed(sorted_widgets)

        # Take items into a list in the order they are sorted
        for widget_item in reverse_sorted_widgets:
            sorted_layout_items.append(
                self.ui.chain_list_layout.takeAt(
                    self.ui.chain_list_layout.indexOf(widget_item['widget'])))

        # Put items into widget (in reverse order as always insert)
        for item in sorted_layout_items:
            self.ui.chain_list_layout.insertItem(0, item)

    def _update_child_chain_items(self):
        '''
        Update Child Chain Items.

        Update the items that are direct children in the chain_list.
        Remove abandoned items and add new items.
        '''
        self._remove_abandoned_chain_items()
        self._add_new_chain_items()

    def closeEvent(self, event):
        '''
        Close Event.

        Widget was closed.

        Arguments:
            event (Qt.QEvent): Event detail.
        '''
        for index in range(0, self.ui.chain_list_layout.count()):
            layout_item = self.ui.chain_list_layout.itemAt(index)
            widget = layout_item.widget()
            widget.close()
            widget.deleteLater()
        self.is_destroyed = True

    def accept_mime_data(self, accept_mime_data):
        '''
        Accept Mime Data.

        Return True when the given mime_data has the correct format and
        may be evaluated its contents.

        Arguments:
            mime_data (QtCore.QMimeData): Mime data of element to be dropped.

        Returns:
            bool -- True when the mime data can be accepted.
        '''
        return False

    #
    # Public Interface
    #

    def add_item(self, widget_item):
        '''
        Add Item.

        Add a widget to the layout

        Arguments:
            widget_item (list_item_widget): Widget to be added.
        '''
        self.ui.chain_list_layout.addWidget(widget_item)

    def has_item(self, model_instance):
        '''
        Has Item.

        Check if the layout contains a widget for the given model_instance

        Arguments:
            model_instance (model.chain_item): Item with id field.

        Returns:
            bool -- True when the item exists in the widget layout.
        '''
        for index in range(0, self.ui.chain_list_layout.count()):
            layout_item = self.ui.chain_list_layout.itemAt(index)
            widget = layout_item.widget()
            if not isinstance(widget, (list_item_widget,)):
                continue
            if widget.model.id.get() == model_instance.id.get():
                return True
        return False

    def is_collapsed(self):
        '''
        Is Collapsed.

        Return True if the list is in collapsed state.
        '''
        return not self.ui.chain_list.isVisible()

    def remove_item(self, widget_item):
        '''
        Remove Item.

        Remove and delete item from the widget layout.

        Arguments:
            widget_item (list_item_widget): item to be removed.
        '''
        layout_item = self.ui.chain_list_layout.takeAt(
            self.ui.chain_list_layout.indexOf(widget_item))
        if layout_item is None:
            return
        widget_item.close()

        layout_item.widget().deleteLater()

    def set_collapsed(self, state):
        '''
        Set Collapsed

        Set the chain list visibility.
        '''
        if not state:
            self.ui.chain_list.show()
        else:
            self.ui.chain_list.hide()

    def set_mainwindow(self, mainwindow):
        '''
        Set Main Window.

        Set the mainwindow (with its backend) to the chain list widget for
        receiving model updates and triggering actions.

        Arguments:
            mainwindow (mainwindow): main application widget.
        '''
        self.mainwindow = mainwindow
        if self.mainwindow is None:
            return
        self.backend = mainwindow.backend

        self.backend.application.event_loop.accounts_changed.connect(
            self.update_model)

    def set_model(self, model):
        '''
        Set Model.

        Set model for the widget.

        Arguments:
            model (model.chain_item): Chain item that represents the parent \
                of all chain_items in the list.
        '''
        self.model = model

    def update_model(
            self, accounts_model, groups_model,
            grouped_accounts_model, chains_model):
        '''
        Update Model

        Update model cache instances with the given models. Update the chain
        items after the collections are updated.

        Arguments:
            accounts_model (list): All configured accounts
            groups_model (list): All configured groups
            grouped_accounts_model (list): \
                All configured accounts with a group parent (unchained)
            chains_model (list): All configured accounts with \
                their chain relation.
        '''
        self.accounts = accounts_model
        self.groups = groups_model
        self.grouped_accounts = grouped_accounts_model
        self.chains = chains_model

        self.chain_items = chain_list_model()
        for chain in self.chains:
            if chain.parent_id.get() == self.model.id.get():
                self.chain_items.append(chain)

        self._update_child_chain_items()
        self._sort_items()
