# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/gui/account_list/views/account_item.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_AccountItem(object):
    def setupUi(self, account):
        account.setObjectName("account")
        self.account_item_layout = QtGui.QHBoxLayout(account)
        self.account_item_layout.setObjectName("account_item_layout")
        spacerItem = QtGui.QSpacerItem(16, 0, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.account_item_layout.addItem(spacerItem)
        self.type = QtGui.QLabel(account)
        self.type.setEnabled(True)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.type.sizePolicy().hasHeightForWidth())
        self.type.setSizePolicy(sizePolicy)
        self.type.setMaximumSize(QtCore.QSize(32, 32))
        self.type.setSizeIncrement(QtCore.QSize(32, 32))
        self.type.setCursor(QtCore.Qt.SizeAllCursor)
        self.type.setText("")
        self.type.setScaledContents(True)
        self.type.setIndent(8)
        self.type.setObjectName("type")
        self.account_item_layout.addWidget(self.type)
        self.main_layout = QtGui.QVBoxLayout()
        self.main_layout.setObjectName("main_layout")
        self.account_name = QtGui.QLabel(account)
        self.account_name.setText("")
        self.account_name.setTextFormat(QtCore.Qt.PlainText)
        self.account_name.setObjectName("account_name")
        self.main_layout.addWidget(self.account_name)
        self.main_action_layout = QtGui.QHBoxLayout()
        self.main_action_layout.setObjectName("main_action_layout")
        self.status_icon = QtGui.QLabel(account)
        self.status_icon.setText("")
        self.status_icon.setPixmap(QtGui.QPixmap(":/images/status/disconnected.png"))
        self.status_icon.setScaledContents(True)
        self.status_icon.setMaximumSize(QtCore.QSize(16, 16))
        self.status_icon.setSizeIncrement(QtCore.QSize(16, 16))
        self.status_icon.setObjectName("status_icon")
        self.main_action_layout.addWidget(self.status_icon)
        self.status_text = QtGui.QLabel(account)
        self.status_text.setText("")
        self.status_text.setObjectName("status_text")
        self.main_action_layout.addWidget(self.status_text)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.main_action_layout.addItem(spacerItem1)
        self.main_layout.addLayout(self.main_action_layout)
        self.velocity = QtGui.QWidget(account)
        self.velocity.setObjectName("velocity")
        self.velocity_layout = QtGui.QHBoxLayout(self.velocity)
        self.velocity_layout.setContentsMargins(0, 0, 0, 0)
        self.velocity_layout.setObjectName("velocity_layout")
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.velocity_layout.addItem(spacerItem2)
        self.read_velocity = QtGui.QLabel(self.velocity)
        self.read_velocity.setText("")
        self.read_velocity.setObjectName("read_velocity")
        self.velocity_layout.addWidget(self.read_velocity)
        self.write_velocity = QtGui.QLabel(self.velocity)
        self.write_velocity.setText("")
        self.write_velocity.setObjectName("write_velocity")
        self.velocity_layout.addWidget(self.write_velocity)
        self.main_layout.addWidget(self.velocity)
        spacerItem3 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.main_layout.addItem(spacerItem3)
        self.account_item_layout.addLayout(self.main_layout)
        self.action_connect = QtGui.QAction(account)
        self.action_connect.setEnabled(True)
        self.action_connect.setObjectName("action_connect")
        self.action_connect_all = QtGui.QAction(account)
        self.action_connect_all.setEnabled(True)
        self.action_connect_all.setObjectName("action_connect_all")
        self.action_collapse = QtGui.QAction(account)
        self.action_collapse.setEnabled(True)
        self.action_collapse.setObjectName("action_collapse")
        self.action_disconnect = QtGui.QAction(account)
        self.action_disconnect.setEnabled(True)
        self.action_disconnect.setObjectName("action_disconnect")
        self.action_reconnect = QtGui.QAction(account)
        self.action_reconnect.setEnabled(True)
        self.action_reconnect.setObjectName("action_reconnect")
        self.action_delete = QtGui.QAction(account)
        self.action_delete.setEnabled(True)
        self.action_delete.setObjectName("action_delete")
        self.action_group = QtGui.QAction(account)
        self.action_group.setEnabled(True)
        self.action_group.setObjectName("action_group")
        self.action_ungroup = QtGui.QAction(account)
        self.action_ungroup.setEnabled(True)
        self.action_ungroup.setObjectName("action_ungroup")
        self.action_move_left = QtGui.QAction(account)
        self.action_move_left.setEnabled(True)
        self.action_move_left.setObjectName("action_move_left")
        self.action_move_right = QtGui.QAction(account)
        self.action_move_right.setEnabled(True)
        self.action_move_right.setObjectName("action_move_right")
        self.action_move_up = QtGui.QAction(account)
        self.action_move_up.setEnabled(True)
        self.action_move_up.setObjectName("action_move_up")
        self.action_move_down = QtGui.QAction(account)
        self.action_move_down.setEnabled(True)
        self.action_move_down.setObjectName("action_move_down")
        self.action_log = QtGui.QAction(account)
        self.action_log.setEnabled(True)
        self.action_log.setObjectName("action_log")
        self.action_settings = QtGui.QAction(account)
        self.action_settings.setEnabled(True)
        self.action_settings.setObjectName("action_settings")
        self.action_new = QtGui.QAction(account)
        self.action_new.setEnabled(True)
        self.action_new.setObjectName("action_new")
        self.action_reset = QtGui.QAction(account)
        self.action_reset.setEnabled(True)
        self.action_reset.setObjectName("action_reset")
        account.addAction(self.action_connect)
        account.addAction(self.action_connect_all)
        account.addAction(self.action_collapse)
        account.addAction(self.action_disconnect)
        account.addAction(self.action_reconnect)
        account.addAction(self.action_delete)
        account.addAction(self.action_group)
        account.addAction(self.action_ungroup)
        account.addAction(self.action_move_left)
        account.addAction(self.action_move_right)
        account.addAction(self.action_move_up)
        account.addAction(self.action_move_down)
        account.addAction(self.action_log)
        account.addAction(self.action_settings)
        account.addAction(self.action_new)
        account.addAction(self.action_reset)

        self.retranslateUi(account)
        QtCore.QMetaObject.connectSlotsByName(account)

    def retranslateUi(self, account):
        self.action_connect.setText(QtGui.QApplication.translate("AccountItem", "Connect", None, QtGui.QApplication.UnicodeUTF8))
        self.action_connect_all.setText(QtGui.QApplication.translate("AccountItem", "Connect All", None, QtGui.QApplication.UnicodeUTF8))
        self.action_collapse.setText(QtGui.QApplication.translate("AccountItem", "Collapse", None, QtGui.QApplication.UnicodeUTF8))
        self.action_disconnect.setText(QtGui.QApplication.translate("AccountItem", "Disconnect", None, QtGui.QApplication.UnicodeUTF8))
        self.action_reconnect.setText(QtGui.QApplication.translate("AccountItem", "Reconnect", None, QtGui.QApplication.UnicodeUTF8))
        self.action_delete.setText(QtGui.QApplication.translate("AccountItem", "Delete", None, QtGui.QApplication.UnicodeUTF8))
        self.action_group.setText(QtGui.QApplication.translate("AccountItem", "Group", None, QtGui.QApplication.UnicodeUTF8))
        self.action_ungroup.setText(QtGui.QApplication.translate("AccountItem", "Ungroup", None, QtGui.QApplication.UnicodeUTF8))
        self.action_move_left.setText(QtGui.QApplication.translate("AccountItem", "Move Left", None, QtGui.QApplication.UnicodeUTF8))
        self.action_move_right.setText(QtGui.QApplication.translate("AccountItem", "Move Right", None, QtGui.QApplication.UnicodeUTF8))
        self.action_move_up.setText(QtGui.QApplication.translate("AccountItem", "Move Up", None, QtGui.QApplication.UnicodeUTF8))
        self.action_move_down.setText(QtGui.QApplication.translate("AccountItem", "Move Down", None, QtGui.QApplication.UnicodeUTF8))
        self.action_log.setText(QtGui.QApplication.translate("AccountItem", "Log", None, QtGui.QApplication.UnicodeUTF8))
        self.action_settings.setText(QtGui.QApplication.translate("AccountItem", "Settings", None, QtGui.QApplication.UnicodeUTF8))
        self.action_new.setText(QtGui.QApplication.translate("AccountItem", "Create", None, QtGui.QApplication.UnicodeUTF8))
        self.action_reset.setText(QtGui.QApplication.translate("AccountItem", "Reset", None, QtGui.QApplication.UnicodeUTF8))

