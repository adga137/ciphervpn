# -*- coding: utf-8 -*-
# account_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Account List for the mainwindow. Display accounts in groups and with their
chains. Uses the *_item implementations starting from a root group item.

.. code-block:: text

    account_item
        /\
        || account associated in chain_item
        ||
    chain_item  <-------------\ child chain items
        /\ \---chain_list-----/
        ||
        ||
    grouped_chain_item grouped account id = one chain item
        /\
        || multiple items in group
        ||
    group_item  <----\ subgroup_items
        /\  \--------/
        ||
        || multiple group items, root_item
        ||
        ||  new_item (initial_view)
        ||   /\
        ||   ||
    account_list
        /\
        ||
    mainwindow

'''

from PySide.QtCore import Signal, QTimer
from PySide.QtGui import QWidget, QSizePolicy, QAction, QIcon, QPixmap

from netsplice.config import gui as config_gui
from netsplice.gui.account_list.account_list_ui import Ui_AccountList
from netsplice.gui.account_list.account_item import (
    account_item
)
from netsplice.gui.account_list.grouped_chain_item import (
    grouped_chain_item
)
from netsplice.gui.account_list.chain_item import (
    chain_item
)
from netsplice.gui.account_list.group_item import (
    group_item
)
from netsplice.gui.account_list.new_item import (
    new_item
)
from netsplice.util.errors import NotFoundError
from netsplice.util import basestring

# ms to wait when the first account is still the root account
WAIT_FOR_FIRST_ACCOUNT = 500


class account_list(QWidget):
    '''
    Widget Listing the Accounts organized in groups and chains.

    .. code-block:: text

        account_list
            group_item(root) - invisible
                group_item(global) - immutable
                    grouped_chain_item
                        chain_item
                            account_item
                            chain_list
                                chain_item
                                    account_item
    '''
    backend_done = Signal(basestring)
    drop_done = Signal(basestring)
    backend_failed = Signal(basestring)
    account_item_dropped = Signal(basestring, basestring, int)
    account_item_dropped_on_group = Signal(basestring, basestring, int)
    account_create = Signal(basestring)
    account_log = Signal(basestring)
    account_settings = Signal(basestring)
    group_create = Signal(basestring)
    group_settings = Signal(basestring)
    group_dropped = Signal(basestring, basestring, int)
    selected = Signal(basestring)

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.mainwindow = None
        self.backend = None

        # model cache
        self.accounts = None
        self.groups = None
        self.grouped_accounts = None
        self.chains = None

        # ui items
        self.ui = Ui_AccountList()
        self.ui.setupUi(self)
        self.ui.root_layout.setContentsMargins(0, 0, 0, 0)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.root_item = None

        self.current_selected_widget = None
        self._current_group = None

        self._setup_toolbar()
        self._connect_actions()

    def _backend_done(self):
        '''
        Backend Done.

        Ensure that the toolbar actions are enabled correct.
        Slot that should be called whenever an item called a backend function.
        '''
        self._update_toolbar_actions()

    def _backend_model_changed(self, model):
        '''
        Backend model changed.

        The backend model has changed - update the toolbar.
        '''
        self._update_toolbar_actions()

    def _connect(self):
        '''
        Connect Action.

        Handler for the toolbar connect action.
        '''
        widget = self._get_current_widget()
        widget.ui.action_connect.trigger()

    def _connect_actions(self):
        '''
        Connect Actions.

        Connect signals with callbacks.
        '''
        self.drop_done.connect(self._drop_done)
        self.backend_done.connect(self._backend_done)
        self.account_item_dropped.connect(self._drop_account)
        self.account_item_dropped_on_group.connect(self._drop_account_on_group)
        self.group_dropped.connect(self._drop_group)
        self.selected.connect(self._item_selected)

    def _create(self):
        '''
        Create Action Handler.

        Trigger the account_create signal.
        '''
        self.account_create.emit(self._current_group)

    def _create_root_item(self):
        '''
        Create Root Item.

        Create item that holds all groups and connect the signals that will
        be emitted from the item.
        The item will be initialized with the current model and update itself
        when the backend changes.
        '''
        root_item = self.groups.find_root()
        self.root_item = group_item(self, self.mainwindow, root_item)
        self.ui.account_list_items_layout.insertWidget(0, self.root_item)
        self.root_item.update_model(
            self.accounts, self.groups, self.grouped_accounts, self.chains)

        self.root_item.backend_done.connect(self._backend_done)
        self.root_item.account_item_dropped.connect(
            self._drop_account)
        self.root_item.account_item_dropped_on_group.connect(
            self._drop_account_on_group)
        self.root_item.group_dropped.connect(
            self._drop_group)
        self.root_item.selected.connect(
            self._item_selected)
        self.root_item.account_create.connect(self.account_create)
        self.root_item.account_log.connect(self.account_log)
        self.root_item.account_settings.connect(self.account_settings)
        self.root_item.group_create.connect(self.group_create)
        self.root_item.group_settings.connect(self.group_settings)
        self.root_item.set_root(True)
        self._select_first_item()

    def _disconnect(self):
        '''
        Disconnect Action.

        Handler for the toolbar disconnect action.
        '''
        widget = self._get_current_widget()
        widget.ui.action_disconnect.trigger()

    def _drop_account(self, dropped_account_id, account_id, weight):
        '''
        Drop Account.

        Handle that a account was dropped with the given weight.
        Evaluates if this is a chain-drop or a group-drop

        Arguments:
            dropped_account_id (string): id of the dropped account
            account_id (string): id of the account that received the drop
            weight (int): weight to indicate before/after drop
        '''
        dropped_account = self.accounts.find_by_id(dropped_account_id)
        chain = self.chains.find_by_id(account_id)
        dropped_chain = self.chains.find_by_id(dropped_account_id)
        if chain.parent_id.get() is None:
            grouped_account = self.grouped_accounts.find_by_id(
                account_id)
            account_group_id = grouped_account.group_id.get()

            self.backend.grouped_account_update.emit(
                dropped_account.id.get(),
                account_group_id,
                weight,
                self.drop_done, self.backend_failed)
        else:
            self.backend.chain_update.emit(
                chain.parent_id.get(), dropped_account_id, weight,
                dropped_chain.connect_mode.get(),
                dropped_chain.failure_mode.get(), False,
                self.drop_done, self.backend_failed)

    def _drop_account_on_group(self, dropped_account_id, group_id, weight):
        '''
        Drop Account on Group.

        Account was dropped on the given group

        Arguments:
            dropped_account_id ([type]): [description]
            group_id ([type]): [description]
            weight ([type]): [description]
        '''
        dropped_account = self.accounts.find_by_id(dropped_account_id)
        self.backend.grouped_account_update.emit(
            dropped_account.id.get(),
            group_id,
            weight,
            self.drop_done, self.backend_failed)

    def _drop_done(self, response):
        '''
        Drop Done.

        Reget the preferences after the drop completed.

        Arguments:
            response (string): json of the api-response.
        '''
        self.backend.preferences_get.emit()

    def _drop_group(self, dropped_group_id, group_id, weight):
        '''
        Drop Group.

        Drop a group on another group

        Arguments:
            dropped_group_id (string): group that was dropped.
            group_id (string): group that received the drop.
            weight (int): weight for before/after position.
        '''
        dropped_group = self.groups.find_by_id(dropped_group_id)
        group = self.groups.find_by_id(group_id)
        self.backend.group_update.emit(
            dropped_group.id.get(),
            dropped_group.name.get(),
            weight,
            dropped_group.collapsed.get(),
            group.id.get(),
            self.drop_done, self.backend_failed)

    def _get_current_widget(self):
        '''
        Return the current selected widget's account or None.
        '''
        if not self.current_selected_widget:
            return None  # XXX raise

        return self.current_selected_widget

    def _get_first_account_item(self, widget):
        '''
        Get Account Item.

        Get Account Item from the given widget that may be the parent of
        a account_item.

        Arguments:
            widget (QWidget): Widget that may contain a account_item, None \
                when there is no account item.
        '''
        item_widget = widget
        if isinstance(item_widget, (group_item,)):
            account_items = item_widget.get_account_widgets()
            group_items = item_widget.get_group_widgets()
            if len(account_items):
                item_widget = account_items[0]
            elif len(group_items):
                item_widget = self._get_first_account_item(group_items[0])

        if isinstance(item_widget, (grouped_chain_item,)):
            item_widget = item_widget.ui.chain_item_widget

        if isinstance(item_widget, (chain_item,)):
            item_widget = item_widget.ui.chain_account_widget

        if isinstance(item_widget, (account_item,)):
            return item_widget

        return None

    def _item_selected(self, item_widget):
        '''
        Item Selected.

        Mark the given item selected and update the toolbar.

        Arguments:
            item_widget (QWidget): list_item widget
        '''
        if self.current_selected_widget:
            self.current_selected_widget.set_active(False)
        self.current_selected_widget = item_widget
        self.current_selected_widget.set_active(True)
        self._update_toolbar_actions()
        if isinstance(item_widget, (group_item,)):
            self._current_group = item_widget.model.id.get()

        if isinstance(item_widget, (grouped_chain_item,)):
            self._current_group = item_widget.model.group_id.get()

        account_widget = None
        if isinstance(item_widget, (chain_item,)):
            account_widget = item_widget.ui.chain_account_widget

        if isinstance(item_widget, (account_item,)):
            account_widget = item_widget

        if account_widget is not None:
            try:
                chain = self.chains.find_by_id(account_widget.model.id.get())
                while chain.parent_id.get():
                    chain = self.chains.find_by_id(chain.parent_id.get())
                grouped_account = self.grouped_accounts.find_by_id(
                    chain.id.get())
                self._current_group = grouped_account.group_id.get()
            except NotFoundError:
                self._current_group = self.root_item.model.id.get()

    def _reconnect(self):
        '''
        Reconnect Action.

        Handler for the toolbar reconnect action.
        '''
        widget = self._get_current_widget()
        widget.ui.action_reconnect.trigger()

    def _select_first_item(self):
        '''
        Select First Item.

        Select the first item. Try to find a account.
        '''
        first_account = self._get_first_account_item(self.root_item)
        if first_account is None:
            first_account = self.root_item
        self._item_selected(first_account)

    def _setup_toolbar(self):
        '''
        Setup Toolbar.

        Setup the actions and add them to the toolbar.
        '''
        create_icon = QIcon()
        connect_icon = QIcon()
        disconnect_icon = QIcon()
        reconnect_icon = QIcon()
        settings_icon = QIcon()
        self.action_create = QAction(self.tr('Create'), self)
        self.action_connect = QAction(self.tr('Connect'), self)
        self.action_disconnect = QAction(self.tr('Disonnect'), self)
        self.action_reconnect = QAction(self.tr('Reconnect'), self)
        self.action_show_settings = QAction(self.tr('Settings'), self)

        self.action_connect.triggered.connect(self._connect)
        self.action_create.triggered.connect(self._create)
        self.action_disconnect.triggered.connect(self._disconnect)
        self.action_reconnect.triggered.connect(self._reconnect)
        self.action_show_settings.triggered.connect(self._show_settings)

        connect_icon.addPixmap(
            QPixmap(config_gui.RES_ICON_ACTION_CONNECT),
            QIcon.Normal, QIcon.Off)
        create_icon.addPixmap(
            QPixmap(config_gui.RES_ICON_ACTION_CREATE),
            QIcon.Normal, QIcon.Off)
        disconnect_icon.addPixmap(
            QPixmap(config_gui.RES_ICON_ACTION_DISCONNECT),
            QIcon.Normal, QIcon.Off)
        reconnect_icon.addPixmap(
            QPixmap(config_gui.RES_ICON_ACTION_RECONNECT),
            QIcon.Normal, QIcon.Off)
        settings_icon.addPixmap(
            QPixmap(config_gui.RES_ICON_ACTION_SETTINGS),
            QIcon.Normal, QIcon.Off)

        self.action_connect.setIcon(connect_icon)
        self.action_create.setIcon(create_icon)
        self.action_disconnect.setIcon(disconnect_icon)
        self.action_reconnect.setIcon(reconnect_icon)
        self.action_show_settings.setIcon(settings_icon)

        self.ui.toolbar.addAction(self.action_create)
        self.ui.toolbar.addAction(self.action_show_settings)
        self.ui.toolbar.addAction(self.action_connect)
        self.ui.toolbar.addAction(self.action_disconnect)
        self.ui.toolbar.addAction(self.action_reconnect)

    def _show_settings(self):
        '''
        Show Settings Action.

        Handler for the toolbar settings action.
        '''
        widget = self._get_current_widget()
        widget.ui.action_settings.trigger()

    def _update_toolbar_actions(self):
        '''
        Update Toolbar Actions.

        Toggle enabled and disabled toolbar actions depending on the selected
        item.
        '''
        self.action_connect.setEnabled(False)
        self.action_disconnect.setEnabled(False)
        self.action_reconnect.setEnabled(False)
        self.action_show_settings.setEnabled(False)

        if self.current_selected_widget is None:
            return
        if len(self.accounts) is None:
            return

        if isinstance(self.current_selected_widget, (group_item,)):
            has_accounts = len(self.accounts)
            has_connected_accounts = False
            has_disconnected_accounts = True
            has_connecting_accounts = False
            global_id = self.groups.find_global().id.get()
            root_id = self.groups.find_root().id.get()
            current_id = self.current_selected_widget.model.id.get()
            is_immutable = current_id == global_id or current_id == root_id
            self.action_show_settings.setEnabled(not is_immutable)

            self.action_connect.setEnabled(
                has_disconnected_accounts and has_accounts)
            self.action_disconnect.setEnabled(
                has_connected_accounts or has_connecting_accounts)
            self.action_reconnect.setEnabled(has_connected_accounts)

        if isinstance(self.current_selected_widget, (account_item,)):
            is_connected = self.current_selected_widget.connected
            is_connecting = self.current_selected_widget.connecting
            is_enabled = self.current_selected_widget.user_enabled
            self.action_show_settings.setEnabled(True)
            if not isinstance(is_connected, (bool,)) or \
                    not isinstance(is_connecting, (bool,)) or \
                    not isinstance(is_enabled, (bool,)):
                # gets a property when current_selected_widget is not
                # initialized (eg after delete)
                return

            self.action_connect.setEnabled(
                is_enabled and not is_connected and not is_connecting)
            self.action_disconnect.setEnabled(
                is_connected or is_connecting)
            self.action_reconnect.setEnabled(is_connected)

    def contextMenuEvent(self, event):
        '''
        Context Menu Event.

        Qt overload to build a custom context menu from the item's actions and
        display the menu.

        Arguments:
            event (Qt.Event): event that triggered the context menu.
        '''
        self.root_item.contextMenuEvent(event)

    def set_mainwindow(self, mainwindow):
        '''
        Set the mainwindow (with its backend) to the widget for receiving model
        updates and triggering actions.

        Arguments:
            mainwindow (mainwindow): main application widget.
        '''
        self.mainwindow = mainwindow
        if self.mainwindow is None:
            return
        self.backend = self.mainwindow.backend

        self.account_new = new_item(self, self.mainwindow)
        self.account_new.hide()
        self.ui.root_layout.insertWidget(1, self.account_new)

        self.backend.application.event_loop.accounts_changed.connect(
            self.update_model)

        self.backend.application.event_loop.gui_model_changed.connect(
            self._backend_model_changed)

    def update_model(
            self, accounts_model, groups_model,
            grouped_accounts_model, chains_model):
        '''
        Update Model.

        The accounts changed in the backend. Store the model and make sure
        there is a root_item visible. When no accounts are available, display
        a message that allows the creation of a new account.

        Arguments:
            accounts_model (list): list of all accounts
            groups_model (list): list of all groups
            grouped_accounts_model (list): \
                list of all accounts with association to a group and a weight
            chains_model (list): list of all accounts in their \
                chain location
        '''
        self.accounts = accounts_model
        self.groups = groups_model
        self.grouped_accounts = grouped_accounts_model
        self.chains = chains_model

        if self.root_item is None:
            self._create_root_item()

        root_item = self.groups.find_root()
        if root_item.id.get() != self.root_item.model.id.get():
            # remove root item when id has changed
            layout_item = self.ui.takeAt(self.ui.indexOf(self.root_item))
            if layout_item is not None:
                layout_item.widget().deleteLater()
            self._create_root_item()

        self._update_toolbar_actions()

        # display account_new when no accounts are configured
        if len(self.accounts) is 0:
            self.account_new.show()
            self.ui.scroll_area.hide()
        else:
            self.account_new.hide()
            self.ui.scroll_area.show()
