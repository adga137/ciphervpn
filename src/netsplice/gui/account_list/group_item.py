# -*- coding: utf-8 -*-
# group_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Custom account group item.
'''

from PySide.QtGui import (
    QMessageBox, QKeySequence, QMenu, QColor, QBrush, QPen
)
from PySide.QtCore import (
    Signal, Qt, QRect, QMimeData, Property
)

from netsplice.config import constants
from netsplice.config import gui as config_gui
from netsplice.util.errors import (
    NotFoundError, NotUniqueError
)
from netsplice.gui.account_list.grouped_chain_item import (
    grouped_chain_item
)
from netsplice.gui.account_list.group_item_ui import Ui_GroupItem

from netsplice.gui.model.connection_list import (
    connection_list as connection_list_model
)
from netsplice.gui.model.group_list import (
    group_list as group_list_model
)
from netsplice.gui.model.grouped_account_list import (
    grouped_account_list as grouped_account_list_model
)
from netsplice.gui.widgets.list_item import list_item as list_item_widget

DROP_INDICATROR_TEXT_COLOR = QColor(0, 0, 0, 255)


class group_item(list_item_widget):
    '''
    Custom Widget for grouping grouped_chain_items.
    Group items are displayed in account_list and group_item and may contain
    grouped_chain_items.
    '''
    account_create = Signal(basestring)
    account_item_dropped = Signal(basestring, basestring, int)
    account_item_dropped_on_group = Signal(basestring, basestring, int)
    account_log = Signal(basestring)
    account_settings = Signal(basestring)
    backend_done = Signal(basestring)
    backend_failed = Signal(basestring)
    group_create = Signal(basestring)
    group_dropped = Signal(basestring, basestring, int)
    group_settings = Signal(basestring)

    def __init__(self, parent, mainwindow=None, model=None):
        list_item_widget.__init__(self, parent)
        self.mainwindow = None
        self.backend = None
        self._root = False
        self.parent_item = parent
        self.model = None

        # model cache
        self.accounts = None
        self.groups = None
        self.grouped_accounts = None
        self.chains = None
        self.connections = connection_list_model()

        # ui items
        self.ui = Ui_GroupItem()
        self.ui.setupUi(self)
        self.ui.group_items.setContentsMargins(1, 10, 1, 0)
        self.is_global_group_item = False
        self.is_group_drag = False
        self.drop_after = False
        self.drop_before = False
        self.drop_into = False

        self.grouped_chain_items = grouped_account_list_model()
        self.child_groups = group_list_model()
        self.set_mainwindow(mainwindow)
        self.set_model(model)

        self._connect_actions()
        self._set_shortcut_keys()

    def _account_dropped(self, dropped_account_id, account_id, weight):
        '''
        Account Dropped.

        Handler for completed drop of dropped_account_id on account_id with
        the weight indicating before or after.

        Arguments:
            dropped_account_id (string): id of dropped account
            account_id (string): id of account dropped on (self)
            weight (integer): weight of drop (< before > after)
        '''
        self.account_item_dropped_on_group.emit(
            dropped_account_id, self.model.id.get(), weight)

    def _add_new_grouped_chain_items(self):
        '''
        Add grouped chain items.

        Create a grouped_chain item in the current group for each item that
        is in the model but not in the widget.
        '''
        for chain_item_model in self.grouped_chain_items:
            if self.has_item(chain_item_model):
                continue
            child_widget = grouped_chain_item(
                self, self.mainwindow, chain_item_model)

            child_widget.selected.connect(self.selected)
            child_widget.account_create.connect(self.account_create)
            child_widget.account_log.connect(self.account_log)
            child_widget.account_settings.connect(self.account_settings)
            child_widget.account_item_dropped.connect(
                self.account_item_dropped)

            child_widget.update_model(
                self.accounts, self.groups, self.grouped_accounts,
                self.chains)
            self.add_item(child_widget)

    def _add_new_group_items(self):
        '''
        Add group items.

        Create a group item in the current group for each item that
        is in the model but not in the widget.
        '''
        for group in self.child_groups:
            if self.has_item(group):
                continue
            child_widget = group_item(self, self.mainwindow, group)

            child_widget.backend_done.connect(self.backend_done)
            child_widget.account_create.connect(
                self.account_create)
            child_widget.account_item_dropped.connect(
                self.account_item_dropped)
            child_widget.account_item_dropped_on_group.connect(
                self.account_item_dropped_on_group)
            child_widget.account_log.connect(self.account_log)
            child_widget.account_settings.connect(
                self.account_settings)
            child_widget.group_create.connect(
                self.group_create)
            child_widget.group_settings.connect(
                self.group_settings)
            child_widget.group_dropped.connect(self.group_dropped)
            child_widget.selected.connect(self.selected)

            child_widget.update_model(
                self.accounts, self.groups, self.grouped_accounts,
                self.chains)
            self.add_item(child_widget)

    def _backend_done(self):
        '''
        Backend done.

        Handler for any successful actions that may have modified the
        preferences.
        '''
        self.backend.preferences_get.emit()

    def _backend_model_changed(self, model):
        '''
        Model Changed.

        Handle the backend model change. Find connection for the
        account_id representing this icon and update the status.
        '''
        self.connections = model.connections

    def _backend_preferences_changed(self, model):
        '''
        Backend Preferences Changed.

        The backend preferences have changed. Update the configured shortcuts
        for the widget.

        Arguments:
            model (model.preferences): New preferences model
        '''
        self._set_shortcut_keys()

    def _connect_actions(self):
        '''
        Connect Actions.

        Connect the ui-element signals with member functions.
        '''
        self.ui.action_connect.triggered.connect(self._connect)
        self.ui.action_disconnect.triggered.connect(self._disconnect)
        self.ui.action_reconnect.triggered.connect(self._reconnect)
        self.ui.action_delete.triggered.connect(self._delete)
        self.ui.action_collapse.triggered.connect(self._collapse)
        self.ui.action_new.triggered.connect(self._new)
        self.ui.action_settings.triggered.connect(self._settings)
        self.ui.action_indicator.clicked.connect(self._collapse)

        self.backend_done.connect(self._backend_done)

    def _collapse(self):
        '''
        Collapse.

        Toggle collapse of the group. Do not collapse the root group
        and avoid storing the global group collapsed.
        '''
        if self.model.id.get() == self.groups.find_root().id.get():
            # Avoid that the root group is collapsed
            return
        self.model.collapsed.set(not self.model.collapsed.get())
        self._update_state()
        if self.model.id.get() == self.groups.find_global().id.get():
            # Avoid that the global group state (immutable) is stored
            return
        self.backend.group_update.emit(
            self.model.id.get(),
            self.model.name.get(),
            self.model.weight.get(),
            self.model.collapsed.get(),
            self.model.parent.get(),
            self.backend_done,
            self.backend_failed)

    def _connect(self):
        '''
        Group Connect.

        Connect all accounts of the group
        '''
        self.backend.group_connect.emit(
            self.model.id.get(),
            self.backend_done, self.backend_failed)

    def _delete(self):
        '''
        Delete.

        Request the backend to the group when it is not root, global and after
        the user has confirmed.
        '''
        if self.model.id.get() == self.groups.find_root().id.get():
            return
        if self.model.id.get() == self.groups.find_global().id.get():
            return
        message = self.tr(config_gui.QUESTION_REALLY_DELETE_GROUP)

        if self.ui.group_items.count() > 0:
            confirm = QMessageBox.question(
                None, constants.NS_NAME,
                message.format(group_name=self.model.name.get()),
                QMessageBox.Ok | QMessageBox.Cancel, QMessageBox.Ok)
            if confirm == QMessageBox.Cancel:
                return
        self.backend.group_delete.emit(
            self.model.id.get(),
            self.backend_done, self.backend_failed)

    def _disconnect(self):
        '''
        Disconnect Group.

        Disconnect all currently connected accounts of this group.
        '''
        self.backend.group_disconnect.emit(
            self.model.id.get(),
            self.backend_done, self.backend_failed)

    def _new(self):
        '''
        New Group.

        Emit to the mainwindow that the user wants to create a new group in the
        current group.
        '''
        self.group_create.emit(self.model.id.get())

    def _order_weighted_widget(self, widget_item):
        '''
        Order weighted Widget.

        Return the weight of the given widget item. This method is used for
        the sort.

        Arguments:
            widget_item (list_widget_item): Widget in the group.

        Returns:
            integer -- weight of the widget.
        '''
        return widget_item['weight']

    def _reconnect(self):
        '''
        Reconnect Group.

        Reconnect all currently connected accounts in the group.
        '''
        self.backend.group_reconnect.emit(
            self.model.id.get(),
            self.backend_done, self.backend_failed)

    def _settings(self):
        '''
        Settings.

        Emit to the mainwindow that the user wants to change the group
        settings.
        '''
        self.group_settings.emit(self.model.to_json())

    def _remove_abandoned_grouped_chain_items(self):
        '''
        Remove abandoned grouped chain items.

        Remove Grouped Chain items that are no longer in the preferences.
        '''
        removed_widgets = []
        for index in range(0, self.ui.group_items.count()):
            layout_item = self.ui.group_items.itemAt(index)
            widget = layout_item.widget()
            if not isinstance(widget, (grouped_chain_item,)):
                continue
            try:
                self.grouped_chain_items.find_by_id(
                    widget.model.id.get())
            except NotFoundError:
                removed_widgets.append(widget)
        for widget in removed_widgets:
            self.remove_item(widget)

    def _remove_abandoned_groups(self):
        '''
        Remove abandoned groups.

        Remove groups that are no longer subgroups of the current group.
        '''
        removed_widgets = []
        for index in range(0, self.ui.group_items.count()):
            layout_item = self.ui.group_items.itemAt(index)
            widget = layout_item.widget()
            if not isinstance(widget, (group_item,)):
                continue
            try:
                self.child_groups.find_by_id(
                    widget.model.id.get())
            except NotFoundError:
                removed_widgets.append(widget)
        for widget in removed_widgets:
            self.remove_item(widget)

    def _set_shortcut_keys(self):
        '''
        Set Shortcut Keys.

        Modify ui actions with preference values.
        The preferences values are already set to the config in main-window.
        '''
        self.ui.action_connect.setShortcut(QKeySequence(
            config_gui.SHORTCUT_SELECTED_CONNECT))
        self.ui.action_connect.setShortcutContext(Qt.WidgetShortcut)
        self.ui.action_disconnect.setShortcut(QKeySequence(
            config_gui.SHORTCUT_SELECTED_DISCONNECT))
        self.ui.action_disconnect.setShortcutContext(Qt.WidgetShortcut)
        self.ui.action_settings.setShortcut(QKeySequence(
            config_gui.SHORTCUT_SELECTED_EDIT))
        self.ui.action_settings.setShortcutContext(
            Qt.WidgetShortcut)

    def _sort_items(self):
        '''
        Sort Items.

        Sort items in the group.
        '''
        sortable_widgets = []
        sorted_layout_items = []

        # Collect group items
        for index in range(0, self.ui.group_items.count()):
            layout_item = self.ui.group_items.itemAt(index)
            widget = layout_item.widget()
            if not isinstance(widget, (group_item,)):
                continue
            sortable_widgets.append({
                'widget': widget,
                'weight': widget.model.weight.get()})

        # Collect account items
        for index in range(0, self.ui.group_items.count()):
            layout_item = self.ui.group_items.itemAt(index)
            widget = layout_item.widget()
            if not isinstance(widget, (grouped_chain_item,)):
                continue
            sortable_widgets.append({
                'widget': widget,
                'weight': widget.model.weight.get()})

        sorted_widgets = reversed(sorted(
            sortable_widgets,
            key=self._order_weighted_widget))

        # Take items into a list in the order they are sorted
        for widget_item in sorted_widgets:
            sorted_layout_items.append(
                self.ui.group_items.takeAt(
                    self.ui.group_items.indexOf(widget_item['widget'])))

        # Put items into widget in reverse order
        for item in sorted_layout_items:
            self.ui.group_items.insertItem(0, item)

    def _update_actions(self):
        '''
        Update Actions.

        Enable or disable actions depending on model state.
        '''
        can_rename = True
        can_delete = True
        can_collapse = True
        can_disconnect = False
        can_reconnect = False
        can_connect = False

        if self.model.id.get() == self.groups.find_root().id.get():
            can_collapse = False
            can_rename = False
            can_delete = False
        if self.model.id.get() == self.groups.find_global().id.get():
            can_rename = False
            can_delete = False
        try:
            # Check current group
            group_accounts = self.grouped_accounts.find_by_group_id(
                self.model.id.get())
            can_connect = True
            has_connected = False
            has_disconnected = False
            for grouped_account in group_accounts:
                try:
                    connection = self.connections.find_by_account_id(
                        grouped_account.id.get())
                    if connection.is_disconnected():
                        has_disconnected = True
                    if connection.is_connected():
                        has_connected = True
                except NotFoundError:
                    has_disconnected = True
                    continue
            if has_connected:
                can_disconnect = True
                can_reconnect = True
            if has_connected and not has_disconnected:
                can_connect = False
        except NotFoundError:
            pass

        try:
            # Check all subgroups
            groups = self.groups.find_children(self.model.id.get())
            can_connect = True
            has_connected = False
            has_disconnected = False
            for group in groups:
                try:
                    group_accounts = self.grouped_accounts.find_by_group_id(
                        group.id.get())

                    for grouped_account in group_accounts:
                        try:
                            connection = self.connections.find_by_account_id(
                                grouped_account.id.get())
                            if connection.is_disconnected():
                                has_disconnected = True
                            if connection.is_connected():
                                has_connected = True
                        except NotFoundError:
                            has_disconnected = True
                            continue
                    if has_connected:
                        can_disconnect = True
                        can_reconnect = True
                    if has_connected and not has_disconnected:
                        can_connect = False
                except NotFoundError:
                    continue
        except NotFoundError:
            pass

        self.ui.action_delete.setEnabled(can_delete)
        self.ui.action_settings.setEnabled(can_rename)
        self.ui.action_collapse.setEnabled(can_collapse)
        self.ui.action_connect.setEnabled(can_connect)
        self.ui.action_disconnect.setEnabled(can_disconnect)
        self.ui.action_reconnect.setEnabled(can_reconnect)

    def _update_grouped_chain_items(self):
        '''
        Update grouped chain items.

        Remove, add and update all grouped_chain_items in the group.
        '''
        self._remove_abandoned_grouped_chain_items()
        self._add_new_grouped_chain_items()
        self._update_grouped_chain_models()

    def _update_grouped_chain_models(self):
        '''
        Update grouped chain models.

        Update model of all grouped_chain_items to the values in the current
        preferences.
        '''
        for index in range(0, self.ui.group_items.count()):
            layout_item = self.ui.group_items.itemAt(index)
            widget = layout_item.widget()
            if not isinstance(widget, (grouped_chain_item,)):
                continue
            new_model = self.grouped_accounts.find_by_id(
                widget.model.id.get())
            widget.model.weight.set(new_model.weight.get())
            widget.model.name.set(new_model.name.get())

    def _update_groups(self):
        '''
        Update Groups.

        Remove, add and update subgroups.
        '''
        self._remove_abandoned_groups()
        self._add_new_group_items()
        self._update_group_models()

    def _update_group_models(self):
        '''
        Update group models.

        Update the models of all subgroups.
        '''
        for index in range(0, self.ui.group_items.count()):
            layout_item = self.ui.group_items.itemAt(index)
            widget = layout_item.widget()
            if not isinstance(widget, (group_item,)):
                continue
            new_model = self.groups.find_by_id(
                widget.model.id.get())
            widget.model.weight.set(new_model.weight.get())
            widget.model.name.set(new_model.name.get())

    def _update_title(self):
        '''
        Update Title.

        Update Title of the group.
        '''
        if self.model.name.get() is None:
            return
        if self.is_global_group_item:
            self.ui.name.setText(self.tr(config_gui.GLOBAL_GROUP_NAME))
        elif self.model.name.get() != self.ui.name.text():
            self.ui.name.setText(self.model.name.get())

    def _update_state(self):
        '''
        Update State.

        Update state of the group item. Change the action text for the context
        menu and the group indicator.
        Hide the root-group name.
        '''
        if self.model.collapsed.get():
            self.ui.group_items_widget.hide()
            self.ui.action_collapse.setText(self.tr(
                config_gui.GROUP_STATE['collapsed']['action_text']))
            self.ui.action_indicator.setText(
                config_gui.GROUP_STATE['collapsed']['indicator'])
        else:
            self.ui.group_items_widget.show()
            self.ui.action_collapse.setText(self.tr(
                config_gui.GROUP_STATE['expanded']['action_text']))
            self.ui.action_indicator.setText(
                config_gui.GROUP_STATE['expanded']['indicator'])

        if self.model.id.get() == self.groups.find_root().id.get():
            self.ui.group_name_widget.hide()
        else:
            self.ui.group_name_widget.show()

    # Widget virtual implementations

    def contextMenuEvent(self, event):
        '''
        Context Menu Event.

        Qt overload to build a custom context menu from the item's actions and
        display the menu.

        Arguments:
            event (Qt.Event): event that triggered the context menu.
        '''
        self._update_actions()
        self.selected.emit(self)
        context_position = self.mapToGlobal(event.pos())
        menu = QMenu(self)
        menu.addAction(self.ui.action_connect)
        menu.addAction(self.ui.action_disconnect)
        menu.addAction(self.ui.action_reconnect)
        menu.addSeparator()
        menu.addAction(self.ui.action_new)
        menu.addAction(self.ui.action_delete)
        menu.addSeparator()
        menu.addAction(self.ui.action_collapse)
        menu.addAction(self.ui.action_new)
        menu.addAction(self.ui.action_settings)
        menu.addSeparator()
        menu.exec_(context_position)

    def mouseDoubleClickEvent(self, event):
        '''
        Mouse double click event.

        Qt overload to handle that the user double clicked on a group.
        Toggles the collapse state.

        Arguments:
            event (Qt.event): event that triggered this eventhandler.
        '''
        self._collapse()

    # List Item Widget interface implementation

    def accept_mime_data(self, mime_data):
        '''
        Accept Mime Data.

        Handler for drag and drop to decide if the given mime_data can be
        accepted by the widget.
        Return True when the given mime_data has the correct format and
        may be evaluated its contents.

        Arguments:
            mime_data (QtCore.QMimeData): Mime data of element to be dropped.

        Returns:
            bool -- True when the mime data can be accepted.

        Raises:
            NotUniqueError -- When the info in the mimedata indicates that \
                the drop will configure a invalid parent.
        '''
        self.drop_after = False
        self.drop_before = False
        self.drop_into = True
        if self._root:
            return False
        if mime_data.hasFormat(config_gui.MIME_TYPE_GROUP):
            content = mime_data.data(config_gui.MIME_TYPE_GROUP)
            try:
                if str(self.model.id.get()) == content:
                    # drag on self
                    return False
                drop_group = self.groups.find_by_id(content)
                parent_groups = self.groups.get_parent_groups(self.model)
                if drop_group in parent_groups:
                    raise NotUniqueError('Drop group is a parent.')
            except NotFoundError:
                return False
            except NotUniqueError:
                return False
            self.is_group_drag = True
            self.drop_into = True
            return True

        if mime_data.hasFormat(config_gui.MIME_TYPE_ACCOUNT):
            content = mime_data.data(config_gui.MIME_TYPE_ACCOUNT)
            # exploit danger
            try:
                account = self.grouped_accounts.find_by_id(content)
                if account.group_id.get() == self.model.id.get():
                    return False
            except NotFoundError:
                pass
            self.is_group_drag = False
            self.drop_into = True
            return True
        return False

    def drag_end(self):
        '''
        Drag End.

        Cleanup any state that was setup during dragging.
        '''
        self.drop_after = False
        self.drop_before = False
        self.drop_into = False

    def drag_move(self, position):
        '''
        Drag Move.

        Evaluate how the drag would be handled when it is dropped at the given
        position. Evaluate if the drop is before, after or into the group.

        Arguments:
            position (QtCore.QPoint): Current point of the drag.
        '''
        top = QRect()
        top.setHeight(self.geometry().height() / 2)
        top.setWidth(self.geometry().width())
        front = QRect()
        front.setHeight(self.geometry().height())
        front.setWidth(self.geometry().width() / 5)  # only small region
        if self.is_group_drag:
            if top.contains(position):
                self.drop_after = False
                self.drop_before = True
            else:
                self.drop_after = True
                self.drop_before = False

            self.drop_into = False
            if front.contains(position):
                self.drop_into = True
            if self._root:
                drop_before = False
                drop_after = False
                drop_into = True
        else:
            self.drop_into = True
            self.drop_after = False
            self.drop_before = False

    def drop_mime_data(self, mime_data):
        '''
        Drop mime data.

        Handle the dropped mime_data after drop has finished.

        Arguments:
            mime_data (QtCore.QMimeData): Mime data of the drop

        Returns:
            bool -- True if the drop was handled with success.

        Raises:
            NotUniqueError -- When a drop would create a invalid parent.
        '''
        if self._root:
            return False
        drop_weight = self.model.weight.get()
        target_group_id = self.model.parent.get()
        if self.drop_after:
            drop_weight += 1
        if self.drop_before:
            drop_weight -= 1

        if target_group_id is None:
            target_group_id = self.model.id.get()
        if self.drop_into:
            drop_weight = 0
            try:
                target_group_accounts = self.grouped_accounts.find_by_group_id(
                    target_group_id)
                for grouped_account in target_group_accounts:
                    if drop_weight < grouped_account.weight.get():
                        drop_weight = grouped_account.weight.get()
            except NotFoundError:
                pass
            target_group_id = self.model.id.get()

        if mime_data.hasFormat(config_gui.MIME_TYPE_GROUP):
            dragged_group_id = str(mime_data.data(config_gui.MIME_TYPE_GROUP))
            try:
                if str(self.model.id.get()) == dragged_group_id:
                    # drag on self
                    return False
                drop_group = self.groups.find_by_id(dragged_group_id)
                parent_groups = self.groups.get_parent_groups(self.model)
                if drop_group in parent_groups:
                    raise NotUniqueError('Drop group is a parent.')
            except NotFoundError:
                return False
            except NotUniqueError:
                return False
            self.group_dropped.emit(
                dragged_group_id, target_group_id, drop_weight)

        if mime_data.hasFormat(config_gui.MIME_TYPE_ACCOUNT):
            dragged_account_id = mime_data.data(
                config_gui.MIME_TYPE_ACCOUNT)
            self.account_item_dropped_on_group.emit(
                dragged_account_id, target_group_id, drop_weight)

    def get_mime_data(self):
        '''
        Get Mime Data.

        Return QtCore.QMimeData() that contains a dropable handle.

        Returns:
            QtCore.QMimeData -- Dropable handle.
        '''
        mime_data = QMimeData()
        mime_data.setData(config_gui.MIME_TYPE_GROUP, str(self.model.id.get()))
        return mime_data

    def paint_drop_regions(self, painter):
        '''
        Paint drop regions.

        Paint drop regions based on the status of the widget.

        Arguments:
            painter (QtGui.QPainter): Painter of the widget.
        '''
        top = QRect()
        top.setHeight(config_gui.DROP_INDICATOR_HEIGHT)
        top.setWidth(self.geometry().width())
        bottom = QRect()
        bottom.setTop(
            self.geometry().height() - config_gui.DROP_INDICATOR_HEIGHT)
        bottom.setHeight(config_gui.DROP_INDICATOR_HEIGHT)
        bottom.setWidth(self.geometry().width())
        front = QRect()
        front.setTop(0)
        front.setHeight(self.geometry().height())
        front.setWidth(config_gui.DROP_INDICATOR_WIDTH)
        drop_target_color = QColor(
            config_gui.DROP_INDICATOR_COLOR_R,
            config_gui.DROP_INDICATOR_COLOR_G,
            config_gui.DROP_INDICATOR_COLOR_B,
            config_gui.DROP_INDICATOR_COLOR_A)
        drop_target_brush = QBrush(
            drop_target_color, Qt.SolidPattern)
        drop_target_pen = QPen(Qt.NoPen)
        painter.setPen(drop_target_pen)
        font = painter.font()
        font.setPointSize(10)
        painter.setFont(font)
        painter.setPen(drop_target_pen)
        indicator_rect = None
        text_hint = ''
        rotated = False
        if self.drop_before:
            indicator_rect = top
            text_hint = self.tr('Before')

        if self.drop_after:
            indicator_rect = bottom

            text_hint = self.tr('After')

        if self.drop_into:
            indicator_rect = front
            text_hint = ''

        if indicator_rect is not None:
            painter.setBrush(drop_target_brush)
            painter.drawRect(indicator_rect)

        if text_hint != '':
            painter.setPen(DROP_INDICATROR_TEXT_COLOR)
            text_rect = indicator_rect
            text_rect.translate(0, 2)
            painter.drawText(text_rect, Qt.AlignHCenter, text_hint)

    def closeEvent(self, event):
        '''
        Close Event.

        Widget was closed.

        Arguments:
            event (Qt.QEvent): Event detail.
        '''
        for widget in self.get_account_widgets():
            widget.close()
        for widget in self.get_group_widgets():
            widget.close()
        self.is_destroyed = True

    # Public Interface may be called from other widgets

    def add_item(self, widget_item):
        '''
        Add Item.

        Add widget to the group_items.

        Arguments:
            widget_item (list_widget_item): item to be added to the
                group_items
        '''
        self.ui.group_items.addWidget(widget_item)

    def ensure_visible(self):
        '''
        Ensure Visible.

        Evaluate the position of the group in the parent scroll area and
        make the group visible with a margin.
        '''
        # get offset in parent
        # get offset of all parents that are list_widget_items
        widget_top = 0
        widget_margin = 50
        widget_height = self.height()
        parent_widget = self.parent()
        scroll_area = None
        while parent_widget is not None:
            if parent_widget.inherits('QScrollArea'):
                scroll_area = parent_widget
                break
            parent_widget = parent_widget.parent()

        parent_widget = self.parent()
        while parent_widget is not None and \
                parent_widget is not scroll_area:
            if isinstance(parent_widget, (list_item_widget,)):
                widget_top += parent_widget.pos().y()
            parent_widget = parent_widget.parent()

        scroll_area.verticalScrollBar().setValue(
            widget_top - widget_height - widget_margin)

    def get_account_widgets(self):
        '''
        Get Items Widgets.

        Return the account list items.
        '''
        account_widgets = []
        for index in range(0, self.ui.group_items.count()):
            layout_item = self.ui.group_items.itemAt(index)
            widget = layout_item.widget()
            if isinstance(widget, (grouped_chain_item,)):
                account_widgets.append(widget)
        return account_widgets

    def get_group_widgets(self):
        '''
        Get Items.

        Return the account group items.
        '''
        group_widgets = []
        for index in range(0, self.ui.group_items.count()):
            layout_item = self.ui.group_items.itemAt(index)
            widget = layout_item.widget()
            if isinstance(widget, (group_item,)):
                group_widgets.append(widget)
        return group_widgets

    def get_root(self):
        '''
        Get Root.

        Return True when this item is the root-item.
        '''
        return self._root

    def has_item(self, model_instance):
        '''
        Has Item.
        Check if the layout contains a widget for the given model_instance

        Arguments:
            model_instance (model.chain_item): Item with id field.

        Returns:
            bool -- True when the item exists in the widget layout.
        '''
        for index in range(0, self.ui.group_items.count()):
            layout_item = self.ui.group_items.itemAt(index)
            widget = layout_item.widget()
            if not isinstance(widget, (list_item_widget,)):
                continue
            if widget.model.id.get() == model_instance.id.get():
                return True
        return False

    def remove_item(self, widget_item):
        '''
        Remove Item.

        Remove and delete item from the widget layout.

        Arguments:
            widget_item (list_item_widget): item to be removed.
        '''
        layout_item = self.ui.group_items.takeAt(
            self.ui.group_items.indexOf(widget_item))
        if layout_item is None:
            return
        widget_item.close()

        layout_item.widget().deleteLater()

    def rename(self):
        '''
        Rename.

        Allow rename from the account_list by forwarding to private method.
        '''
        self._rename()

    def reset(self):
        '''
        Reset.

        Reset group
        '''
        pass

    def set_mainwindow(self, mainwindow):
        '''
        Set Main Window.

        Set the mainwindow (with its backend) to the widget for receiving model
        updates and triggering actions.

        Arguments:
            mainwindow (mainwindow): main application widget.
        '''
        self.mainwindow = mainwindow
        if self.mainwindow is None:
            return
        self.backend = mainwindow.backend

        self.backend.application.event_loop.accounts_changed.connect(
            self.update_model)
        self.backend.application.event_loop.preferences_changed.connect(
            self._backend_preferences_changed)
        self.backend.application.event_loop.gui_model_changed.connect(
            self._backend_model_changed)

    def set_model(self, model):
        '''
        Set Model.

        Set model for the widget.

        Arguments:
            model (model.group_item): Group item that represents the item in
                the list.
        '''
        self.model = model

        if self.model is None:
            return

    def set_root(self, is_root):
        '''
        Set Root.

        Set the item root state. When root is true, the content margins are
        removed.
        '''
        self._root = is_root
        if is_root:
            self.ui.group_items_widget.setContentsMargins(0, 0, 0, 0)

    def update_model(
            self, accounts_model, groups_model,
            grouped_accounts_model, chains_model):
        '''
        Update Model

        Update model cache instances with the given models. Update the group
        items after the collections are updated.

        Arguments:
            accounts_model (list): All configured accounts
            groups_model (list): All configured groups
            grouped_accounts_model (list): \
                All configured accounts with a group parent (unchained)
            chains_model (list): All configured accounts with \
                their chain relation.
        '''
        self.accounts = accounts_model
        self.groups = groups_model
        self.grouped_accounts = grouped_accounts_model
        self.chains = chains_model

        for group in self.groups:
            if group.id.get() != self.model.id.get():
                continue
            self.model = group
            break
        self.child_groups = self.groups.find_children(
            self.model.id.get())

        self.grouped_chain_items = grouped_account_list_model()
        try:
            self.grouped_chain_items = self.grouped_accounts.find_by_group_id(
                self.model.id.get())
        except NotFoundError:
            pass
        if self.model.immutable.get():
            self.can_drag = False
            if self.model.id.get() == self.groups.find_global().id.get():
                self.is_global_group_item = True

        self._update_title()
        self._update_state()
        self._update_groups()
        self._update_grouped_chain_items()
        self._update_actions()
        self._sort_items()

    # Property that indicates if the list_item is a root item
    # Setting the property forces a recalculation of the stylesheet
    # list_item[root="true"] > QLabel { color: white; }
    # list_item[root="false"] > QLabel { color: black; }
    root = Property(
        bool, get_root, set_root)
