# -*- coding: utf-8 -*-
# test_account_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Connection.

Checks connection base functionality.
'''

import mock
import sys

from PySide.QtGui import QApplication, QWidget, QLabel, QAction
from PySide.QtCore import Signal, QObject
from netsplice.config import connection as config_connection
from netsplice.gui.account_list.account_item import account_item
from netsplice.gui.model.connection_list_item import connection_list_item
from netsplice.util.model.field import (
    field as marshalable_field
)


qApp = QApplication(sys.argv)


class mock_widget(QWidget):
    def __init__(self, parent):
        QWidget.__init__(self, parent)
        self.scroll_area = None
        self.ui = mock_ui(self)
        self.reset()

    def reset(self):
        self._set_state_failed_called = False

    def _set_state_failed(self):
        self._set_state_failed_called = True


class mock_ui(QWidget):
    def __init__(self, parent):
        QWidget.__init__(self, parent)
        self.read_velocity = QLabel(self)
        self.write_velocity = QLabel(self)
        self.action_delete = QAction(self)
        self.action_disconnect = QAction(self)


class mock_event_loop(QObject):
    accounts_changed = Signal(object)
    gui_model_changed = Signal(object)
    preferences_changed = Signal(object)

    def __init__(self):
        QObject.__init__(self)
        pass


class mock_application(QObject):
    def __init__(self):
        QObject.__init__(self)
        self.event_loop = mock_event_loop()
        pass


class mock_mainwindow(QObject):
    def __init__(self):
        self.backend = mock_backend()


class mock_backend(QObject):
    account_connect = Signal(basestring, Signal, Signal)
    account_disconnect = Signal(basestring, Signal, Signal)
    account_reset = Signal(basestring, Signal, Signal)

    def __init__(self):
        QObject.__init__(self)
        self.application = mock_application()
        self.reset()

    def reset(self):
        self.model_changed_called = False


class mock_model(object):
    def __init__(self):
        self.id = marshalable_field()
        self.name = marshalable_field()
        self.type = marshalable_field()
        self.enabled = marshalable_field()
        self.default_route = marshalable_field()


root_widget = QWidget()


def get_test_object():

    parent = mock_widget(root_widget)
    mainwindow = mock_mainwindow()
    model = mock_model()
    control = account_item(parent, mainwindow, model)
    control.mock_widget = parent

    return control


@mock.patch('PySide.QtCore.QTimer.singleShot')
def test_animate_connecting_icon_starts_timer(mock_singleshot):
    c = get_test_object()
    c._connecting = True
    c._animate_connecting_icon()
    mock_singleshot.assert_called()


def test_animate_connecting_icon_resets_connection_index_when_not_connecting():
    c = get_test_object()
    c._connecting = False
    c._connecting_index = 1
    c._animate_connecting_icon()
    assert(c._connecting_index == -1)


def test_cancel_connect_sets_state():
    c = get_test_object()
    c._cancel_connect()
    assert(not c._connecting)
    assert(not c._connected)


def test_connect_sets_state():
    c = get_test_object()
    c._animate_connecting_icon = lambda: 1 + 1
    c._connect()
    assert(c._connecting)
    assert(not c._connected)


def test_disconnect_sets_state():
    c = get_test_object()
    c._disconnect()
    assert(not c._connecting)
    assert(not c._connected)


def test_disconnect_sets_text():
    pass


def test_disconnect_resets_menu():
    pass


def test_reset_action_triggers_redraw():
    pass


def test_reset_calls_reset():
    c = get_test_object()
    c._reset()
    pass


def test_reset_sets_state():
    c = get_test_object()
    c._reset()
    assert(not c._connecting)
    assert(not c._connected)
    assert(c._connecting_index == -1)


def test_readable_velocity_none_returns_empty():
    c = get_test_object()
    assert('' == c._readable_velocity(None))


def test_readable_velocity_1000_returns_bps():
    c = get_test_object()
    assert('1000 B/s' == c._readable_velocity(1000))


def test_readable_velocity_100000_returns_kps():
    c = get_test_object()
    print c._readable_velocity(100000)
    assert('100.00 kB/s' == c._readable_velocity(100000))


def test_readable_velocity_10000000_returns_mps():
    c = get_test_object()
    print c._readable_velocity(10000000)
    assert('10.00 MB/s' == c._readable_velocity(10000000))


def test_readable_velocity_10000000000_returns_gps():
    c = get_test_object()
    print c._readable_velocity(10000000000)
    assert('10.00 GB/s' == c._readable_velocity(10000000000))


def test_readable_velocity_10000000000000_returns_tps():
    c = get_test_object()
    print c._readable_velocity(10000000000000)
    assert('10.00 TB/s' == c._readable_velocity(10000000000000))


def test_update_velocity_uses_udp_if_available():
    c = get_test_object()
    cmi = connection_list_item()
    cmi.read_udp_velocity.set(1000)
    cmi.write_udp_velocity.set(1000)
    cmi.read_tap_velocity.set(500)
    cmi.write_tap_velocity.set(500)
    c._update_velocity(cmi)
    assert('1000 B/s' in c.ui.read_velocity.text())
    assert('1000 B/s' in c.ui.write_velocity.text())


def test_update_velocity_uses_tap_if_udp_not_available():
    c = get_test_object()
    cmi = connection_list_item()
    cmi.read_udp_velocity.set(None)
    cmi.write_udp_velocity.set(None)
    cmi.read_tap_velocity.set(500)
    cmi.write_tap_velocity.set(500)
    c._update_velocity(cmi)
    print c.ui.read_velocity.text()
    print c.ui.write_velocity.text()

    assert('500 B/s' in c.ui.read_velocity.text())
    assert('500 B/s' in c.ui.write_velocity.text())


def test_update_state_sets_state_connected():
    c = get_test_object()
    cmi = connection_list_item()
    cmi.state.set(config_connection.CONNECTED)
    c.update_status(cmi)
    assert(c._connected is True)


def test_update_state_sets_state_connecting():
    c = get_test_object()
    cmi = connection_list_item()
    cmi.state.set(config_connection.CONNECTING)
    c.update_status(cmi)
    assert(c._connecting)
    cmi.state.set(config_connection.CONNECTING_PROCESS)
    c.update_status(cmi)
    assert(c._connecting)


def test_update_state_sets_state_failed():
    c = get_test_object()
    cmi = connection_list_item()
    cmi.state.set(config_connection.DISCONNECTED_FAILURE)
    c.mock_widget.reset()
    c._set_state_failed = c.mock_widget._set_state_failed
    c.update_status(cmi)
    assert(c.mock_widget._set_state_failed_called)
