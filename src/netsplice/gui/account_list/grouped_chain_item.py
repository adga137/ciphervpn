# -*- coding: utf-8 -*-
# grouped_chain_item.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Widget to wrap ordered chain items (grouped_account_items). Each
grouped_chain_item contains a single chain_item.
'''

from PySide.QtCore import Signal

from netsplice.gui.account_list.grouped_chain_item_ui import (
    Ui_GroupedChainItem
)
from netsplice.gui.widgets.list_item import list_item as list_item_widget
from netsplice.util.errors import (
    NotFoundError
)


class grouped_chain_item(list_item_widget):
    '''
    Widget to wrap ordered chain items (grouped_account_items). Each
    grouped_chain_item contains a single chain_item.
    '''
    account_create = Signal(basestring)
    account_item_dropped = Signal(basestring, basestring, int)
    account_log = Signal(basestring)
    account_settings = Signal(basestring)

    def __init__(self, parent, mainwindow=None, model=None):
        list_item_widget.__init__(self, parent)
        self.mainwindow = None
        self.backend = None
        self.model = None

        # model cache
        self.accounts = parent.accounts
        self.groups = parent.groups
        self.grouped_accounts = parent.grouped_accounts
        self.chains = parent.chains

        self.ui = Ui_GroupedChainItem()
        self.ui.setupUi(self)
        self.ui.grouped_chain_item_layout.setContentsMargins(6, 0, 0, 0)
        self.ui.chain_item_widget.setContentsMargins(0, 6, 0, 0)
        self.set_model(model)
        self.set_mainwindow(mainwindow)

    def closeEvent(self, event):
        '''
        Close Event.

        Widget was closed.

        Arguments:
            event (Qt.QEvent): Event detail.
        '''
        self.ui.chain_item_widget.close()
        self.is_destroyed = True

    def set_mainwindow(self, mainwindow):
        '''
        Set Main Window.

        Set the main window for the widget to access the backend dispatcher.
        The owning widget will call this method when the widget is created
        through the ui-file, otherwise the constructor is used.
        Connect the model updates with the backend of the mainwindow.

        Arguments:
            mainwindow (gui.mainwindow): instance of the gui mainwindow
        '''
        self.mainwindow = mainwindow
        if self.mainwindow is None:
            return
        self.backend = mainwindow.backend

        self.backend.application.event_loop.accounts_changed.connect(
            self.update_model)

        self.ui.chain_item_widget.set_mainwindow(mainwindow)
        self.ui.chain_item_widget.account_create.connect(self.account_create)
        self.ui.chain_item_widget.account_log.connect(self.account_log)
        self.ui.chain_item_widget.account_settings.connect(
            self.account_settings)
        self.ui.chain_item_widget.account_item_dropped.connect(
            self.account_item_dropped)
        self.ui.chain_item_widget.selected.connect(self.selected)

    def set_model(self, model):
        '''
        Set Model.

        Set model for the widget instance.

        Arguments:
            model (model.account_list_item): account_item with name and type
        '''
        self.model = model
        try:
            chain_item = self.chains.find_by_id(self.model.id.get())
            self.ui.chain_item_widget.set_model(chain_item)
        except NotFoundError:
            pass

    def update_model(
            self, accounts_model, groups_model,
            grouped_accounts_model, chains_model):
        '''
        Update Model

        Update model cache instances with the given models. Update the
        grouped_account items after the collections are updated.

        Arguments:
            accounts_model (list): All configured accounts
            groups_model (list): All configured groups
            grouped_accounts_model (list): \
                All configured accounts with a group parent (unchained)
            chains_model (list): All configured accounts with \
                their chain relation.
        '''
        self.accounts = accounts_model
        self.groups = groups_model
        self.grouped_accounts = grouped_accounts_model
        self.chains = chains_model

        for grouped_account in self.grouped_accounts:
            if grouped_account.id.get() != self.model.id.get():
                continue
            self.set_model(grouped_account)
            break
