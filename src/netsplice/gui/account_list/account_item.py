# -*- coding: utf-8 -*-
# account_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Custom account list item.

Gui Item that represents a preference account.
'''

from PySide.QtGui import (
    QMessageBox, QSizePolicy, QPixmap, QKeySequence, QMenu, QColor, QBrush,
    QPen
)
from PySide.QtCore import (
    Qt, Signal, QTimer, QMimeData, Property, QRect
)
from netsplice.config import constants
from netsplice.config import gui as config_gui
from netsplice.gui import get_account_type_pixmap
from netsplice.gui.widgets.list_item import list_item as list_item_widget

from netsplice.gui.account_list.account_item_ui import Ui_AccountItem
from netsplice.gui.model.response.group_id import (
    group_id as group_id_response_model
)
from netsplice.gui.model.account_list import (
    account_list as account_list_model
)
from netsplice.gui.model.group_list import (
    group_list as group_list_model
)
from netsplice.gui.model.grouped_account_list import (
    grouped_account_list as grouped_account_list_model
)
from netsplice.gui.model.chain_list import (
    chain_list as chain_list_model
)
from netsplice.util.errors import NotFoundError
from netsplice.util import get_logger, basestring

logger = get_logger()

DROP_INDICATROR_TEXT_COLOR = QColor(0, 0, 0, 255)


class account_item(list_item_widget):
    '''
    Custom Widget Listing the Accounts.

    Item to be displayed in an account_group_item.
    '''

    account_create = Signal(basestring)
    account_item_dropped = Signal(basestring, basestring, int)
    account_item_dropped_in_chain = Signal(basestring, basestring, int)
    account_log = Signal(basestring)
    account_reset = Signal()
    account_settings = Signal(basestring)
    backend_done = Signal(basestring)
    backend_failed = Signal(basestring)

    chain_move_left = Signal(basestring)
    chain_move_right = Signal(basestring)
    chain_move_up = Signal(basestring)
    chain_move_down = Signal(basestring)
    chain_collapse = Signal()

    group_step_move_account = Signal(basestring)

    def __init__(self, parent, mainwindow=None, model=None):
        '''
        Initialize Module.

        Setup default values for a account item.
        '''
        list_item_widget.__init__(self, parent)
        self.model = None
        self.backend = None
        self.mainwindow = None
        self.parent_group_id = None

        self._load_pixmaps()
        self._connected = False
        self._connecting = False
        self._failed = False
        self._enabled = False
        self._connecting_index = -1
        self._collapsed = False
        self._backend_connections_processed = False
        self._connect_mode = None
        self._default_route = False

        # model cache
        self.accounts = account_list_model()
        self.groups = group_list_model()
        self.grouped_accounts = grouped_account_list_model()
        self.chains = chain_list_model()

        self.ui = Ui_AccountItem()

        # Set the size hints for the item. When the height is not configured
        # the widget will expand to ~400px height
        # XXX high res displays?!
        self.setMaximumSize(
            config_gui.MAX_WIDGET_WIDTH, config_gui.MAX_ITEM_HEIGHT)
        self.setSizePolicy(
            QSizePolicy.Preferred, QSizePolicy.Maximum)
        self.ui.setupUi(self)
        self.set_mainwindow(mainwindow)
        self.set_model(model)

        self.drop_before = False
        self.drop_after = False
        self.drop_into_chain = False

        self._connect_actions()
        self._set_shortcut_keys()

    def _account_item_dropped(self, dropped_account_id, account_id, weight):
        '''
        Account Dropped.

        Account was dropped on this account item.

        Arguments:
            dropped_account_id (string): id of dropped account
            account_id (string): id of account dropped on (self)
            weight (integer): weight of drop (< before > after)
        '''
        self.account_item_dropped.emit(
            dropped_account_id,
            account_id,
            weight)

    def _account_item_dropped_in_chain(
            self, dropped_account_id, account_id, weight):
        '''
        Account Dropped in Chain.

        Account was dropped on this account item with the intend to make it a
        child of the target.

        Arguments:
            dropped_account_id (string): id of dropped account
            account_id (string): id of account dropped on (self)
            weight (integer): weight of drop (< before > after)
        '''
        self.account_item_dropped_in_chain.emit(
            dropped_account_id,
            account_id,
            weight)

    def _animate_connecting_icon(self):
        '''
        Animate connecting icon.

        Self-calling function that sets the connecting icon of the item
        to the next state.
        Will reset, when the internal connecting state is False.
        '''
        if not self._get_connecting():
            # End animation by checking the connecting state.
            # Causes next update_connecting_icon() to start animation
            self._connecting_index = -1
            return

        self.ui.status_icon.setPixmap(
            self.PIXMAP_CONNECTING[self._connecting_index])
        self._connecting_index += 1
        if self._connecting_index >= len(self.PIXMAP_CONNECTING):
            self._connecting_index = 0
        animate = lambda: self._animate_connecting_icon()
        QTimer.singleShot(
            config_gui.CONNECTING_ANIMATION_DELAY, animate)

    def _backend_done(self):
        '''
        Backend Done.

        Rerequest the current model
        '''
        if self.backend is not None:
            self.backend.preferences_get.emit()

    def _backend_model_changed(self, model):
        '''
        Model Changed.

        Handle the backend model change. Find connection for the
        account_id representing this icon and update the status.
        '''
        connection = None
        failed_connection = None
        try:
            connection = model.connections.find_by_account_id(
                self.model.id.get())
        except NotFoundError:
            pass
        try:
            failed_connection = model.failed_connections.find_by_account_id(
                self.model.id.get())
        except NotFoundError:
            pass
        self._backend_connections_processed = True
        if failed_connection is not None:
            self.update_status(failed_connection)
        elif connection:
            self.update_status(connection)
        else:
            self.reset()

    def _can_drop_account(self, account_id):
        '''
        Can Drop Account.

        Evaluate if the given account_id can be dropped on/before/after the
        current account.

        Arguments:
            account_id (string): account_id to be dropped

        Returns:
            bool -- True when drop is ok, False if not
        '''
        if str(self.model.id.get()) == account_id:
            return False

        children = self.chains.get_children_for_id(account_id)
        for child in children:
            if child.id.get() == self.model.id.get():
                return False
        return True

    def _cancel_connect(self):
        '''
        Cancel Connect.

        Action handler that triggers a disconnect to the backend.
        Set internal state to get updated ui.
        '''
        self.backend.account_disconnect.emit(
            self.model.id.get(),
            self.backend_done, self.backend_failed)
        self._set_state_disconnected()

    def _collapse(self):
        '''
        Collapse.

        Action handler that toggles the chain_list to collapse.
        '''
        self.chain_collapse.emit()

    def _connect(self):
        '''
        Connect.

        Action handler that triggers connect to the backend.
        Set internal state to get updated ui.
        '''
        self.backend.account_connect.emit(
            self.model.id.get(),
            self.backend_done, self.backend_failed)
        self._set_state_connecting()

    def _connect_all(self):
        '''
        Connect All.

        Action handler that triggers connect of all chain children to the
        backend. Set internal state to get updated ui.
        '''
        self.backend.chain_connect.emit(
            self.model.id.get(),
            self.backend_done, self.backend_failed)
        self._set_state_connecting()

    def _connect_actions(self):
        '''
        Connect Actions.

        Connect the ui-element signals with member functions.
        '''
        self.ui.action_connect.triggered.connect(self._connect)
        self.ui.action_connect_all.triggered.connect(self._connect_all)
        self.ui.action_disconnect.triggered.connect(self._disconnect)
        self.ui.action_reconnect.triggered.connect(self._reconnect)
        self.ui.action_reset.triggered.connect(self._reset)
        self.ui.action_delete.triggered.connect(self._delete)
        self.ui.action_collapse.triggered.connect(self._collapse)

        self.ui.action_group.triggered.connect(self._group)
        self.ui.action_ungroup.triggered.connect(self._ungroup)

        self.ui.action_move_left.triggered.connect(self._move_left)
        self.ui.action_move_right.triggered.connect(self._move_right)
        self.ui.action_move_down.triggered.connect(self._move_down)
        self.ui.action_move_up.triggered.connect(self._move_up)

        self.ui.action_log.triggered.connect(self._show_logviewer)
        self.ui.action_settings.triggered.connect(self._show_settings)
        self.ui.action_new.triggered.connect(self._new)

        self.group_step_move_account.connect(self._group_step_move_account)
        self.backend_done.connect(self._backend_done)

    def _delete(self):
        '''
        Delete.

        Action handler that triggers delete in the preference backend.
        '''
        message = self.tr(config_gui.QUESTION_REALLY_DELETE)

        confirm = QMessageBox.question(
            None, constants.NS_NAME,
            message.format(account_name=self.model.name.get()),
            QMessageBox.Ok | QMessageBox.Cancel, QMessageBox.Ok)
        if confirm == QMessageBox.Cancel:
            return

        self.backend.account_delete.emit(
            self.model.id.get(),
            self.backend_done, self.backend_failed)

    def _disconnect(self):
        '''
        Disconnect.

        Action handler that triggers disconnect in the backend.
        Set internal state to get updated ui.
        '''
        self._set_state_disconnected()
        self.backend.account_disconnect.emit(
            self.model.id.get(),
            self.backend_done, self.backend_failed)

    def _get_collapsed(self):
        '''
        Get Collapsed.

        Return the collapsed state for properties.
        '''
        return self._collapsed

    def _get_connected(self):
        '''
        Get Connected.

        Return the connected state for properties.
        '''
        return self._connected

    def _get_connecting(self):
        '''
        Get Connecting.

        Return the connecting state for properties.
        '''
        return self._connecting

    def _get_default_route(self):
        '''
        Get Default Route.

        Return the connecting state for properties.
        '''
        return self._default_route

    def _get_enabled(self):
        '''
        Get Enabled.

        Return the enabled state for properties.
        '''
        return self._enabled

    def _get_failed(self):
        '''
        Get Failed.

        Return the failed state for properties.
        '''
        return self._failed

    def _get_connect_mode(self):
        '''
        Get Parent connect Mode.

        Return the failed state for properties.
        '''
        return self._connect_mode

    def _group(self):
        '''
        Group.

        Create a Group and move the current item into that group.
        '''
        self.backend.group_create.emit(
            self.tr(config_gui.NEW_GROUP_NAME),
            0,
            self.parent_group_id,
            self.group_step_move_account, self.backend_failed)

    def _grouped_account_sort_weight(self, grouped_account):
        '''
        Grouped Account Sort Weight.

        Return the weight for the grouped account for sorting.

        Arguments:
            grouped_account (model.grouped_account): account to extract the \
                weight from.

        Returns:
            int -- weight of grouped account.
        '''
        return grouped_account.weight.get()

    def _group_step_move_account(self, create_json):
        '''
        Group step move account.

        Second step in grouping a account. Move the current account into the
        new group.

        Arguments:
            create_json (string): json of create group response (contains \
                group_id)
        '''
        model = group_id_response_model()
        model.from_json(create_json)
        new_group_id = model.id.get()
        self.backend.grouped_account_update.emit(
            self.model.id.get(),
            new_group_id,
            0,
            self.backend_done, self.backend_failed)

    def _load_pixmaps(self):
        '''
        Load Pixmaps.

        Load Pixmaps that will be used for certain states.
        This avoids loading them again and again.
        '''
        self.PIXMAP_CONNECTED = QPixmap(
            config_gui.RES_ICON_STATUS_CONNECTED)
        self.PIXMAP_CONNECTED_REDIRECT_GATEWAY = QPixmap(
            config_gui.RES_ICON_STATUS_CONNECTED_REDIRECT_GATEWAY)
        self.PIXMAP_CONNECTED_SOCKS = QPixmap(
            config_gui.RES_ICON_STATUS_CONNECTED_SOCKS)
        self.PIXMAP_DISCONNECTED = QPixmap(
            config_gui.RES_ICON_STATUS_DISCONNECTED)
        self.PIXMAP_FAILED = QPixmap(
            config_gui.RES_ICON_STATUS_FAILED)
        self.PIXMAP_CONNECTING = []
        self.PIXMAP_CONNECTING.append(QPixmap(
            config_gui.RES_ICON_STATUS_CONNECTING_0))
        self.PIXMAP_CONNECTING.append(QPixmap(
            config_gui.RES_ICON_STATUS_CONNECTING_1))
        self.PIXMAP_CONNECTING.append(QPixmap(
            config_gui.RES_ICON_STATUS_CONNECTING_2))

    def _move_left(self):
        '''
        Move Left.

        Action Handler for Moving in chains.
        '''
        account_id = self.model.id.get()
        chain = self.chains.find_by_id(account_id)
        chain_parent = self.chains.find_by_id(chain.parent_id.get())
        new_parent_account_id = chain_parent.parent_id.get()
        if new_parent_account_id is None:
            parent_id = chain.parent_id.get()
            parent_grouped_account = self.grouped_accounts.find_by_id(
                parent_id)
            self.account_item_dropped.emit(
                account_id,
                parent_id,
                parent_grouped_account.weight.get() + 1)
        else:
            self.chain_move_left.emit(account_id)

    def _move_right(self):
        '''
        Move Right.

        Action Handler for Moving in chains.
        '''
        account_id = self.model.id.get()
        chain = self.chains.find_by_id(account_id)
        if chain.parent_id.get() is None:
            weight = 0
            new_parent_account_id = 0
            grouped_account = self.grouped_accounts.find_by_id(account_id)
            group_siblings = self.grouped_accounts.find_by_group_id(
                grouped_account.group_id.get())
            sorted_siblings = sorted(
                group_siblings, key=self._grouped_account_sort_weight)
            prev_sibling = None
            for account in sorted_siblings:
                if account.id.get() == account_id:
                    break
                prev_sibling = account
            if prev_sibling is None:
                raise NotFoundError(
                    'Move right does not work with no previous account.')
            new_parent_account_id = prev_sibling.id.get()
            prev_children = self.chains.get_children(prev_sibling.id.get())
            for child in prev_children:
                if child.weight.get() > weight:
                    weight = child.weight.get()
            weight += 1
            self.backend.chain_update.emit(
                new_parent_account_id, account_id, weight,
                chain.connect_mode.get(), chain.failure_mode.get(), False,
                self.backend_done,
                self.backend_failed)
        else:
            self.chain_move_right.emit(self.model.id.get())

    def _move_up(self):
        '''
        Move Up.

        Action Handler for Moving in groups.
        Set a new weight that is before the previous item.
        '''
        chain_item = self.chains.find_by_id(self.model.id.get())
        if chain_item.parent_id.get() is None:
            grouped_account = self.grouped_accounts.find_by_id(
                self.model.id.get())
            self.account_item_dropped.emit(
                grouped_account.id.get(),
                grouped_account.id.get(),
                grouped_account.weight.get() - 11)
        else:
            self.chain_move_up.emit(
                self.model.id.get())

    def _move_down(self):
        '''
        Move Down.

        Action Handler for Moving in groups.
        Set a new weight that is after the next item.
        '''
        chain_item = self.chains.find_by_id(self.model.id.get())
        if chain_item.parent_id.get() is None:
            grouped_account = self.grouped_accounts.find_by_id(
                self.model.id.get())
            self.account_item_dropped.emit(
                grouped_account.id.get(),
                grouped_account.id.get(),
                grouped_account.weight.get() + 11)
        else:
            self.chain_move_down.emit(
                self.model.id.get())

    def _new(self):
        '''
        New Account.

        Trigger the new account dialog.
        '''
        self.account_create.emit(self.parent_group_id)

    def _reconnect(self):
        '''
        Reconnect.

        Action Handler that triggers reconnect in the backend.
        Set the internal state to update the UI.
        '''
        self._set_state_connecting()
        self.backend.account_reconnect.emit(
            self.model.id.get(),
            self.backend_done, self.backend_failed)

    def _reset(self):
        '''
        Reset.

        Action handler that triggers a reset for the current account in the
        backend.
        '''
        self.reset()
        self.backend.account_reset.emit(
            self.model.id.get(),
            self.backend_done, self.backend_failed)

    def _set_connected(self, state):
        '''
        Set Connected.

        Property setter that ensures recalculation of item stylesheet.
        '''
        self._connected = state
        self.setStyleSheet('/* /')

    def _set_connecting(self, state):
        '''
        Set Connecting.

        Property setter that ensures recalculation of item stylesheet.
        '''
        self._connecting = state
        self.setStyleSheet('/* /')

    def _set_enabled(self, state):
        '''
        Set Enabled.

        Property setter that ensures recalculation of item stylesheet.
        '''
        self._enabled = state
        self.setStyleSheet('/* /')

    def _set_default_route(self, state):
        '''
        Set Default Route.

        Property setter that ensures recalculation of item stylesheet.
        '''
        self._default_route = state
        self.setStyleSheet('/* /')

    def _set_failed(self, state):
        '''
        Set Failed.

        Property setter that ensures recalculation of item stylesheet.
        '''
        self._failed = state
        self.setStyleSheet('/* /')

    def _set_connect_mode(self, mode):
        '''
        Set Parent connect mode.

        Property setter that ensures recalculation of item stylesheet.
        '''
        self._connect_mode = mode
        self.setStyleSheet('/* /')

    def _set_shortcut_keys(self):
        '''
        Set Shortcut Keys.

        Modify ui actions with preference values.
        '''
        self.ui.action_collapse.setShortcut(QKeySequence(
            config_gui.SHORTCUT_COLLAPSE_TOGGLE))
        self.ui.action_collapse.setShortcutContext(Qt.WidgetShortcut)
        self.ui.action_connect.setShortcut(QKeySequence(
            config_gui.SHORTCUT_SELECTED_CONNECT))
        self.ui.action_connect.setShortcutContext(Qt.WidgetShortcut)
        self.ui.action_connect_all.setShortcut(QKeySequence(
            config_gui.SHORTCUT_SELECTED_CONNECT_ALL))
        self.ui.action_connect_all.setShortcutContext(Qt.WidgetShortcut)
        self.ui.action_disconnect.setShortcut(QKeySequence(
            config_gui.SHORTCUT_SELECTED_DISCONNECT))
        self.ui.action_disconnect.setShortcutContext(Qt.WidgetShortcut)
        self.ui.action_reconnect.setShortcut(QKeySequence(
            config_gui.SHORTCUT_SELECTED_RECONNECT))
        self.ui.action_reconnect.setShortcutContext(Qt.WidgetShortcut)
        self.ui.action_reset.setShortcut(QKeySequence(
            config_gui.SHORTCUT_SELECTED_RESET))
        self.ui.action_reset.setShortcutContext(Qt.WidgetShortcut)
        self.ui.action_delete.setShortcut(QKeySequence(
            config_gui.SHORTCUT_SELECTED_DELETE))
        self.ui.action_delete.setShortcutContext(Qt.WidgetShortcut)

        self.ui.action_group.setShortcut(QKeySequence(
            config_gui.SHORTCUT_SELECTED_GROUP))
        self.ui.action_group.setShortcutContext(Qt.WidgetShortcut)
        self.ui.action_ungroup.setShortcut(QKeySequence(
            config_gui.SHORTCUT_SELECTED_UNGROUP))
        self.ui.action_ungroup.setShortcutContext(Qt.WidgetShortcut)

        self.ui.action_move_left.setShortcut(QKeySequence(
            config_gui.SHORTCUT_SELECTED_MOVE_LEFT))
        self.ui.action_move_left.setShortcutContext(Qt.WidgetShortcut)
        self.ui.action_move_right.setShortcut(QKeySequence(
            config_gui.SHORTCUT_SELECTED_MOVE_RIGHT))
        self.ui.action_move_right.setShortcutContext(Qt.WidgetShortcut)
        self.ui.action_move_up.setShortcut(QKeySequence(
            config_gui.SHORTCUT_SELECTED_MOVE_UP))
        self.ui.action_move_up.setShortcutContext(Qt.WidgetShortcut)
        self.ui.action_move_down.setShortcut(QKeySequence(
            config_gui.SHORTCUT_SELECTED_MOVE_DOWN))
        self.ui.action_move_down.setShortcutContext(Qt.WidgetShortcut)

        self.ui.action_settings.setShortcut(QKeySequence(
            config_gui.SHORTCUT_SELECTED_EDIT))
        self.ui.action_settings.setShortcutContext(
            Qt.WidgetShortcut)

    def _set_state_connected(self, connection_model_instance):
        '''
        Set state connected.

        Set the item to be connected by updating the ui appropriate
        and enabling and disabling actions.
        '''
        ip_local = ''
        redirect_gateway = False
        socks = ''
        try:
            ip_local = connection_model_instance.environment.get_value(
                'connection_ifconfig_local')
            redirect_gateway = connection_model_instance.environment.get_value(
                'connection_redirect_gateway')
            if redirect_gateway == '1':
                redirect_gateway = True
            else:
                redirect_gateway = False
        except NotFoundError:
            pass
        try:
            ip_local = connection_model_instance.environment.get_value(
                'connection_ifconfig_ipv6_local')
        except NotFoundError:
            pass
        try:
            socks = connection_model_instance.environment.get_value(
                'connection_socks')
        except NotFoundError:
            pass

        self._set_connected(True)
        self._set_connecting(False)
        self._set_failed(False)
        if redirect_gateway:
            self.ui.status_icon.setPixmap(
                self.PIXMAP_CONNECTED_REDIRECT_GATEWAY)
        elif socks != '':
            self.ui.status_icon.setPixmap(
                self.PIXMAP_CONNECTED_SOCKS)
        else:
            self.ui.status_icon.setPixmap(self.PIXMAP_CONNECTED)

        if ip_local != '':
            self.ui.status_text.setText(
                self.tr(config_gui.CONNECTED_IP['title']).format(
                    ip_local=ip_local))
            self.ui.status_text.setToolTip(
                self.tr(config_gui.CONNECTED_IP['body']).format(
                    account_name=self.model.name.get(),
                    ip_local=ip_local))
        elif socks != '':
            self.ui.status_text.setText(
                self.tr(config_gui.CONNECTED_SOCKS['title']).format(
                    socks=socks))
            self.ui.status_text.setToolTip(
                self.tr(config_gui.CONNECTED_SOCKS['body']).format(
                    account_name=self.model.name.get(),
                    socks=socks))
        else:
            self.ui.status_text.setText(
                self.tr(config_gui.CONNECTED['title']))
            self.ui.status_text.setToolTip(
                self.tr(config_gui.CONNECTED['body']).format(
                    account_name=self.model.name.get()))

        if ip_local != '' or socks != '':
            self.ui.status_text.setTextInteractionFlags(
                Qt.TextSelectableByMouse)
        self.ui.action_connect.setEnabled(False)
        self.ui.action_connect_all.setEnabled(False)
        self.ui.action_reconnect.setEnabled(True)
        self.ui.action_disconnect.setEnabled(True)
        self.ui.action_reset.setEnabled(True)
        self.ui.action_delete.setEnabled(False)

    def _set_state_connecting(self):
        '''
        Set state connecting.

        Set the item to be connecting by updating the ui appropriate
        and enabling and disabling actions.
        '''
        self._set_connected(False)
        self._set_connecting(True)
        self._set_failed(False)
        if self._connecting_index == -1:
            self._animate_connecting_icon()
        self.ui.status_text.setText(
            self.tr(config_gui.CONNECTING['title']))
        self.ui.status_text.setToolTip(
            self.tr(config_gui.CONNECTING['body'])
            % {'account_name': self.model.name.get()})
        self.ui.status_text.setTextInteractionFlags(
            Qt.NoTextInteraction)
        self.ui.action_connect.setEnabled(False)
        self.ui.action_connect_all.setEnabled(False)
        self.ui.action_disconnect.setEnabled(True)
        self.ui.action_reconnect.setEnabled(False)
        self.ui.action_reset.setEnabled(True)
        self.ui.action_delete.setEnabled(False)

    def _set_state_disconnected(self):
        '''
        Set state disconnected.

        Set the item to be disconnected by updating the ui appropriate
        and enabling and disabling actions.
        '''
        self._set_connected(False)
        self._set_connecting(False)
        self._set_failed(False)
        self.ui.status_icon.setPixmap(
            self.PIXMAP_DISCONNECTED)
        self.ui.status_text.setText(
            self.tr(config_gui.DISCONNECTED['title']))
        self.ui.status_text.setToolTip(
            self.tr(config_gui.DISCONNECTED['body'])
            % {'account_name': self.model.name.get()})
        self.ui.status_text.setTextInteractionFlags(
            Qt.NoTextInteraction)
        self.ui.read_velocity.setText('')
        self.ui.write_velocity.setText('')
        self.ui.action_connect.setEnabled(True)
        self.ui.action_connect_all.setEnabled(True)
        self.ui.action_delete.setEnabled(True)
        self.ui.action_disconnect.setEnabled(False)
        self.ui.action_reconnect.setEnabled(False)
        self.ui.action_reset.setEnabled(False)

    def _set_state_failed(self):
        '''
        Set state failed.

        Set the item to be failed by updating the ui appropriate
        and enabling and disabling actions.
        '''
        self._set_connected(False)
        self._set_connecting(False)
        self._set_failed(True)
        self.ui.status_icon.setPixmap(self.PIXMAP_FAILED)
        self.ui.status_text.setText(
            self.tr(config_gui.CONNECTION_FAILED['title']))
        self.ui.status_text.setToolTip(
            self.tr(config_gui.CONNECTION_FAILED['body'])
            % {'account_name': self.model.name.get()})
        self.ui.status_text.setTextInteractionFlags(
            Qt.NoTextInteraction)
        self.ui.read_velocity.setText('')
        self.ui.write_velocity.setText('')
        self.ui.action_connect.setEnabled(True)
        self.ui.action_connect_all.setEnabled(True)
        self.ui.action_delete.setEnabled(True)
        self.ui.action_disconnect.setEnabled(False)
        self.ui.action_reconnect.setEnabled(False)
        self.ui.action_reset.setEnabled(True)

    def _show_logviewer(self):
        '''
        Show Logviewer.

        Action handler that shows the logviewer for the current account.
        '''
        self.account_log.emit(self.model.to_json())

    def _show_settings(self):
        '''
        Show Settings.

        Action handler that shows the settings for the current account.
        '''
        self.account_settings.emit(self.model.to_json())

    def _readable_velocity(self, bytes_per_second):
        '''
        Readable Velocity

        Create a readable string for the given bytes_per_second.

        1 MB = 1000000 bytes (= 10002 B = 106 B) is the definition recommended
        by the International System of Units (SI) and the International
        Electrotechnical Commission IEC.[2] This definition is used in
        networking contexts and most storage media, particularly hard drives,
        flash-based storage,[3] and DVDs, and is also consistent with the other
        uses of the SI prefix in computing, such as CPU clock speeds or
        measures of performance.

        Arguments:
            bytes_per_second (int): bytes per second
        '''
        if bytes_per_second is None:
            return ''
        if bytes_per_second > 1000 ** 4:
            return '%.2f %s' % (
                float(bytes_per_second) / (1000 ** 4), self.tr('TB/s'))
        if bytes_per_second > 1000 ** 3:
            return '%.2f %s' % (
                float(bytes_per_second) / (1000 ** 3), self.tr('GB/s'))
        if bytes_per_second > 1000 ** 2:
            return '%.2f %s' % (
                float(bytes_per_second) / (1000 ** 2), self.tr('MB/s'))
        if bytes_per_second > 1000:
            return '%.2f %s' % (
                float(bytes_per_second) / 1000, self.tr('kB/s'))

        return '%d %s' % (bytes_per_second, self.tr('B/s'))

    def _ungroup(self):
        '''
        Ungroup.

        Take the current item and place it in the parent group.
        '''
        my_group = self.groups.find_by_id(self.parent_group_id)
        parent_parent_group_id = my_group.parent.get()
        if parent_parent_group_id is None:
            return
        self.backend.grouped_account_update.emit(
            self.model.id.get(),
            parent_parent_group_id,
            0,
            self.backend_done, self.backend_failed)

    def _update_velocity(self, connection_model_instance):
        '''
        Update Velocity.

        Update text on connected item. Prefer UDP velocity, fallback to tap
        velocity.
        '''
        if connection_model_instance.read_udp_velocity.get() is not None:
            self.ui.read_velocity.setText(u"\u2193" + self._readable_velocity(
                connection_model_instance.read_udp_velocity.get()))
        elif connection_model_instance.read_tap_velocity.get() is not None:
            self.ui.read_velocity.setText(u"\u2193" + self._readable_velocity(
                connection_model_instance.read_tap_velocity.get()))
        else:
            self.ui.read_velocity.setText(u"\u2193" + self._readable_velocity(
                0))

        if connection_model_instance.write_udp_velocity.get() is not None:
            self.ui.write_velocity.setText(u"\u2191" + self._readable_velocity(
                connection_model_instance.write_udp_velocity.get()))
        elif connection_model_instance.write_tap_velocity.get() is not None:
            self.ui.write_velocity.setText(u"\u2191" + self._readable_velocity(
                connection_model_instance.write_tap_velocity.get()))
        else:
            self.ui.write_velocity.setText(u"\u2191" + self._readable_velocity(
                0))

    # Widget virtual implementations

    def contextMenuEvent(self, event):
        '''
        Context Menu Event.

        Creates a QMenu with actions for an account.
        '''
        context_position = self.mapToGlobal(event.pos())
        self.selected.emit(self)
        self.update_actions()
        has_chain_children = self.chains.has_children(self.model.id.get())
        menu = QMenu(self)
        menu.addAction(self.ui.action_connect)
        if has_chain_children:
            menu.addAction(self.ui.action_connect_all)
        menu.addAction(self.ui.action_reconnect)
        menu.addAction(self.ui.action_disconnect)
        menu.addSeparator()
        menu.addAction(self.ui.action_log)
        menu.addAction(self.ui.action_settings)
        if has_chain_children:
            menu.addAction(self.ui.action_collapse)
        menu.addSeparator()
        menu.addAction(self.ui.action_new)
        menu.addAction(self.ui.action_delete)
        menu.addAction(self.ui.action_reset)
        menu.addSeparator()
        menu.addAction(self.ui.action_group)
        menu.addAction(self.ui.action_ungroup)
        menu.addSeparator()
        menu.addAction(self.ui.action_move_left)
        menu.addAction(self.ui.action_move_right)
        menu.addAction(self.ui.action_move_up)
        menu.addAction(self.ui.action_move_down)
        menu.exec_(context_position)

    def mouseDoubleClickEvent(self, event):
        '''
        Double click Event.

        Connects the account.
        '''
        if not self.model.enabled.get():
            return
        if self._get_connected() or self._get_connecting():
            self._show_logviewer()
            return
        self._connect()

    #
    # Drag and Drop Handling
    #

    def accept_mime_data(self, mime_data):
        '''
        Accept Mime Data.

        Drag&Drop handler for mime-data that accepts dragged accounts that are
        not the current account list item.
        '''
        self.drop_after = False
        self.drop_before = False

        if not mime_data.hasFormat(config_gui.MIME_TYPE_ACCOUNT):
            return False
        content = mime_data.data(config_gui.MIME_TYPE_ACCOUNT)
        return self._can_drop_account(content)

    def get_mime_data(self):
        '''
        Get Mime Data.

        Return the current account as dragable item.
        '''
        mime_data = QMimeData()
        mime_data.setData(
            config_gui.MIME_TYPE_ACCOUNT, str(self.model.id.get()))
        return mime_data

    def drag_end(self):
        '''
        Drag End Handler.

        Resets values that are used during drag & drop.
        '''
        self.drop_after = False
        self.drop_before = False
        self.drop_into_chain = False

    def drag_move(self, position):
        '''
        Drag Move Handler.

        Evaluates the given position on the item and sets before/after values
        based on the position.
        '''
        top = QRect()
        right = QRect()
        top.setHeight(self.geometry().height() / 2)
        top.setWidth(self.geometry().width())
        right.setLeft(
            self.geometry().left() + self.geometry().width() / 3 * 2)
        right.setHeight(self.geometry().height())
        right.setWidth(self.geometry().width() / 3)
        if top.contains(position):
            self.drop_after = False
            self.drop_before = True
            self.drop_into_chain = False
        else:
            if right.contains(position):
                self.drop_into_chain = True
                self.drop_after = False
                self.drop_before = False
            else:
                self.drop_after = True
                self.drop_before = False
                self.drop_into_chain = False

    def drop_mime_data(self, mime_data):
        '''
        Drop Event Handler.

        Emits an account_item_dropped signal with weight based on the drop
        position.
        '''
        if not mime_data.hasFormat(config_gui.MIME_TYPE_ACCOUNT):
            return False
        account_id = mime_data.data(config_gui.MIME_TYPE_ACCOUNT)
        if not self._can_drop_account(account_id):
            return
        drop_weight = 0
        chain_item = self.chains.find_by_id(self.model.id.get())
        if chain_item.parent_id.get() is None:
            grouped_account = self.grouped_accounts.find_by_id(
                self.model.id.get())
            drop_weight = grouped_account.weight.get()
        else:
            drop_weight = chain_item.weight.get()

        if self.drop_after:
            drop_weight += 1
        if self.drop_before:
            drop_weight -= 1
        if self.drop_into_chain:
            chain_children = self.chains.get_children(self.model.id.get())
            drop_weight = 0
            if len(chain_children) > 0:
                drop_weight = chain_children[-1].weight.get() + 1
        if mime_data.hasFormat(config_gui.MIME_TYPE_ACCOUNT):
            dragged_account_id = str(mime_data.data(
                config_gui.MIME_TYPE_ACCOUNT))
            if self.drop_into_chain:
                self._account_item_dropped_in_chain(
                    dragged_account_id,
                    self.model.id.get(),
                    drop_weight)
            else:
                self._account_item_dropped(
                    dragged_account_id,
                    self.model.id.get(),
                    drop_weight)

    def paint_drop_regions(self, painter):
        '''
        Painter for drop Regions.

        Draws a transparent indicator on the item that hints the user on the
        drop position.
        '''
        top = QRect()
        bottom = QRect()
        bottom_right = QRect()
        top.setHeight(config_gui.DROP_INDICATOR_HEIGHT)
        top.setWidth(self.geometry().width())
        bottom.setTop(
            self.geometry().height() - config_gui.DROP_INDICATOR_HEIGHT)
        bottom.setHeight(config_gui.DROP_INDICATOR_HEIGHT)
        bottom.setWidth(self.geometry().width())
        bottom_right.setTop(
            self.geometry().height() - config_gui.DROP_INDICATOR_HEIGHT)
        bottom_right.setLeft(
            self.geometry().left() + self.geometry().width() / 3 * 2)
        bottom_right.setHeight(config_gui.DROP_INDICATOR_HEIGHT)
        bottom_right.setWidth(self.geometry().width() / 3)
        drop_target_color = QColor(
            config_gui.DROP_INDICATOR_COLOR_R,
            config_gui.DROP_INDICATOR_COLOR_G,
            config_gui.DROP_INDICATOR_COLOR_B,
            config_gui.DROP_INDICATOR_COLOR_A)
        drop_target_brush = QBrush(
            drop_target_color, Qt.SolidPattern)
        drop_target_pen = QPen(Qt.NoPen)
        font = painter.font()
        font.setPointSize(10)
        painter.setFont(font)
        painter.setPen(drop_target_pen)
        indicator_rect = None
        text_hint = ''
        if self.drop_before:
            indicator_rect = top

            text_hint = self.tr('Before')

        if self.drop_after:
            indicator_rect = bottom

            text_hint = self.tr('After')

        if self.drop_into_chain:
            indicator_rect = bottom_right

            text_hint = self.tr('As Child')

        if indicator_rect is not None:
            painter.setBrush(drop_target_brush)
            painter.drawRect(indicator_rect)

        if text_hint != '':
            painter.setPen(DROP_INDICATROR_TEXT_COLOR)
            text_rect = indicator_rect
            text_rect.translate(0, 2)
            painter.drawText(text_rect, Qt.AlignHCenter, text_hint)

    # Public Interface

    def reset(self):
        '''
        Reset the account.

        Changes the values to defaults after a previous failed connection.
        '''
        self.ui.status_icon.setPixmap(self.PIXMAP_DISCONNECTED)
        self.ui.status_text.setText(self.tr(config_gui.DISCONNECTED['title']))
        self.ui.status_text.setToolTip(
            self.tr(config_gui.DISCONNECTED['body'])
            % {'account_name': self.model.name.get()})
        self.ui.action_connect.setEnabled(True)
        self.ui.action_connect_all.setEnabled(True)
        self.ui.action_reconnect.setEnabled(False)
        self.ui.action_disconnect.setEnabled(False)
        self.ui.action_reset.setEnabled(True)
        self.ui.action_delete.setEnabled(True)
        self._set_connected(False)
        self._set_connecting(False)
        self._set_failed(False)
        self._connecting_index = -1

    def set_collapsed(self, collapsed):
        '''
        Set Collapsed.

        Set the account item collapsed (eg has chain children that are
        invisible).

        Arguments:
            collapsed (bool): collapse state.
        '''
        self._collapsed = collapsed
        self.setStyleSheet('/* /')
        if self._collapsed:
            self.ui.action_collapse.setText(self.tr('Expand'))
        else:
            self.ui.action_collapse.setText(self.tr('Collapse'))

    def set_mainwindow(self, mainwindow):
        '''
        Set Main Window.

        Set the main window for the widget to access the backend dispatcher.
        The owning widget will call this method when the widget is created
        through the ui-file, otherwise the constructor is used.
        Connect the model updates with the backend of the mainwindow.

        Arguments:
            mainwindow (gui.mainwindow): instance of the gui mainwindow
        '''
        self.mainwindow = mainwindow
        if self.mainwindow is None:
            return
        self.backend = mainwindow.backend

        self.backend.application.event_loop.accounts_changed.connect(
            self.update_model)

        self.backend.application.event_loop.gui_model_changed.connect(
            self._backend_model_changed)

    def set_model(self, model):
        '''
        Set Model.

        Set model for the widget instance.

        Arguments:
            model (model.account_list_item): account_item with name and type
        '''
        self.model = model
        if self.model is None:
            return
        self.update_values(self.model)

    def update_actions(self):
        '''
        Update Actions.

        Update the actions of the contextmenu by evaluating the model values.
        '''
        can_ungroup = False
        can_group = False
        can_move_left = False
        can_move_right = False
        can_move_down = False
        can_move_up = False
        is_enabled = self.model.enabled.get()
        try:
            group_account = self.grouped_accounts.find_by_id(
                self.model.id.get())
            group_accounts = self.grouped_accounts.find_by_group_id(
                group_account.group_id.get())
            max_weight = 0
            for grouped_account in group_accounts:
                if max_weight < grouped_account.weight.get():
                    max_weight = grouped_account.weight.get()
            min_weight = max_weight
            for grouped_account in group_accounts:
                if min_weight > grouped_account.weight.get():
                    min_weight = grouped_account.weight.get()
            can_move_up = group_account.weight.get() > min_weight
            can_move_down = group_account.weight.get() < max_weight
            if can_move_up:
                can_move_right = True

            parent_group_id = group_account.group_id.get()
            self.parent_group_id = parent_group_id
            root_group = self.groups.find_root()
            global_group = self.groups.find_global()
            if root_group.id.get() != parent_group_id and \
                    global_group.id.get() != parent_group_id:
                can_ungroup = True
            can_group = True
        except NotFoundError:
            pass

        try:
            chain = self.chains.find_by_id(
                self.model.id.get())
            can_move_left = chain.parent_id.get() is not None

            if can_move_left:
                parent_children = self.chains.get_children(
                    chain.parent_id.get())
                can_move_down = True
                can_move_up = True
                if len(parent_children) > 1:
                    if parent_children[0].id.get() == self.model.id.get():
                        can_move_up = False
                    elif parent_children[-1].id.get() == self.model.id.get():
                        can_move_down = False
                else:
                    can_move_up = False
                    can_move_down = False
                if can_move_up:
                    can_move_right = True
        except NotFoundError:
            pass
        self.ui.action_move_up.setEnabled(can_move_up)
        self.ui.action_move_down.setEnabled(can_move_down)
        self.ui.action_group.setEnabled(can_group)
        self.ui.action_ungroup.setEnabled(can_ungroup)
        self.ui.action_move_left.setEnabled(can_move_left)
        self.ui.action_move_right.setEnabled(can_move_right)

        if not is_enabled:
            self.ui.action_connect.setEnabled(False)
            self.ui.action_connect_all.setEnabled(False)
            self.ui.action_log.setEnabled(False)

    def update_model(
            self, accounts_model, groups_model,
            grouped_accounts_model, chains_model):
        '''
        Update model.

        Update the values of the account based on the latest model changes.
        '''
        self.accounts = accounts_model
        self.groups = groups_model
        self.grouped_accounts = grouped_accounts_model
        self.chains = chains_model
        try:
            self.model = self.accounts.find_by_id(self.model.id.get())
            self.update_values(self.model)
            self.update_actions()
        except NotFoundError:
            pass

    def update_status(self, connection_model_instance):
        '''
        Update Account Status.

        Updates the actions during a active connection.
        Change velocity display, and call setters from model state.

        Arguments:
            connection_model_instance (model.connection_list_item):
                connection instance for the given account.
        '''
        self.ui.action_delete.setEnabled(False)
        self.ui.action_disconnect.setEnabled(False)

        self._update_velocity(connection_model_instance)

        if not connection_model_instance.is_connecting():
            self._set_connecting(False)
            if connection_model_instance.is_connected():
                self._set_state_connected(connection_model_instance)
            else:
                if connection_model_instance.is_failed():
                    self._set_state_failed()
                else:
                    self._set_state_disconnected()
        else:
            # This seems duplicate to the connect / disconnect actions but the
            # intention is that the user gets a un-lagged response in the ui
            # regardless of the backend model updates. But other locations of
            # the code may trigger the state-change too.
            self._set_state_connecting()
            self.ui.action_connect.setEnabled(False)
            self.ui.action_connect_all.setEnabled(False)

    def update_values(self, account_model_instance):
        '''
        Update Values.

        Updates the visible attributes of the account: name and icon.
        '''
        if self.ui.account_name.text() != account_model_instance.name.get():
            self.ui.account_name.setText(account_model_instance.name.get())
        account_type = account_model_instance.type.get()
        self.ui.type.setPixmap(
            get_account_type_pixmap(account_type))
        if self._get_enabled() != account_model_instance.enabled.get():
            self._set_enabled(account_model_instance.enabled.get())
        if self._backend_connections_processed is False:
            self._set_state_disconnected()
        if self._get_default_route() != \
                account_model_instance.default_route.get():
            self._set_default_route(account_model_instance.default_route.get())
        try:
            chain_connect_mode = self.chains.find_by_id(
                account_model_instance.id.get()).connect_mode.get()
            if not self.chains.has_children(account_model_instance.id.get()):
                chain_connect_mode = ''
            if self._connect_mode != chain_connect_mode:
                self._set_connect_mode(chain_connect_mode)
        except NotFoundError:
            pass

    # Properties that allow styling of the custom drawn widget states

    # Property that indicates if the account_list_item is connected or
    # connecting.
    # Setting the property forces a recalculation of the stylesheet
    # account_list_item[connected="true"] { color: white; }
    # account_list_item[connected="false"][connecting="true"] { color: black; }
    collapsed = Property(
        bool, _get_collapsed, set_collapsed)
    connected = Property(
        bool, _get_connected, _set_connected)
    connecting = Property(
        bool, _get_connecting, _set_connecting)
    failed = Property(
        bool, _get_failed, _set_failed)
    user_enabled = Property(
        bool, _get_enabled, _set_enabled)
    connect_mode = Property(
        unicode, _get_connect_mode, _set_connect_mode)
    default_route = Property(
        bool, _get_default_route, _set_default_route)
