# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/gui/account_list/views/chain_list.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_ChainList(object):
    def setupUi(self, chain_list_container):
        chain_list_container.setObjectName("chain_list_container")
        self.chain_list_container_layout = QtGui.QVBoxLayout(chain_list_container)
        self.chain_list_container_layout.setObjectName("chain_list_container_layout")
        self.chain_list = QtGui.QWidget(chain_list_container)
        self.chain_list.setObjectName("chain_list")
        self.chain_list_layout = QtGui.QVBoxLayout(self.chain_list)
        self.chain_list_layout.setContentsMargins(0, 0, 0, 0)
        self.chain_list_layout.setObjectName("chain_list_layout")
        self.chain_list_container_layout.addWidget(self.chain_list)
        spacerItem = QtGui.QSpacerItem(0, 1, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.MinimumExpanding)
        self.chain_list_container_layout.addItem(spacerItem)

        self.retranslateUi(chain_list_container)
        QtCore.QMetaObject.connectSlotsByName(chain_list_container)

    def retranslateUi(self, chain_list_container):
        pass

