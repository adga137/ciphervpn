# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/gui/account_list/views/chain_item.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_ChainItem(object):
    def setupUi(self, chain_item_container):
        chain_item_container.setObjectName("chain_item_container")
        self.chain_item_container_layout = QtGui.QVBoxLayout(chain_item_container)
        self.chain_item_container_layout.setObjectName("chain_item_container_layout")
        self.chain_item = QtGui.QWidget(chain_item_container)
        self.chain_item.setObjectName("chain_item")
        self.chain_item_layout = QtGui.QVBoxLayout(self.chain_item)
        self.chain_item_layout.setContentsMargins(0, 0, 0, 0)
        self.chain_item_layout.setObjectName("chain_item_layout")
        self.chain_account_widget = account_item(self.chain_item)
        self.chain_account_widget.setObjectName("chain_account_widget")
        self.chain_item_layout.addWidget(self.chain_account_widget)
        self.chain_item_container_layout.addWidget(self.chain_item)
        spacerItem = QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.MinimumExpanding)
        self.chain_item_container_layout.addItem(spacerItem)

        self.retranslateUi(chain_item_container)
        QtCore.QMetaObject.connectSlotsByName(chain_item_container)

    def retranslateUi(self, chain_item_container):
        pass

from netsplice.gui.account_list.account_item import account_item
