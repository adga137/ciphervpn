# -*- coding: utf-8 -*-
# chain_item.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Widget display account_items in chains.
'''

from PySide.QtCore import Signal

from netsplice.gui.account_list.chain_list import chain_list
from netsplice.gui.account_list.chain_item_ui import (
    Ui_ChainItem
)
from netsplice.gui.widgets.list_item import list_item as list_item_widget
from netsplice.util.errors import (
    NotFoundError
)
from netsplice.util import get_logger

logger = get_logger()


class chain_item(list_item_widget):
    '''
    Custom Widget to display account_item's in chains.
    '''
    account_create = Signal(basestring)
    account_item_dropped = Signal(basestring, basestring, int)
    account_log = Signal(basestring)
    account_new = Signal(basestring)
    account_settings = Signal(basestring)
    backend_done = Signal(basestring)
    backend_failed = Signal(basestring)
    chain_move_left = Signal(basestring)
    chain_move_right = Signal(basestring)
    chain_move_up = Signal(basestring)
    chain_move_down = Signal(basestring)
    move_done = Signal(basestring)
    preferences_changed = Signal(int)

    def __init__(self, parent, mainwindow=None, model=None):
        list_item_widget.__init__(self, parent)
        self.mainwindow = None
        self.backend = None
        self.model = None
        self.move_queue = dict()

        # model cache
        self.accounts = parent.accounts
        self.groups = parent.groups
        self.grouped_accounts = parent.grouped_accounts
        self.chains = parent.chains

        self.ui = Ui_ChainItem()
        self.ui.setupUi(self)
        self.setContentsMargins(0, 0, 0, 0)
        self.ui.chain_item_container_layout.setContentsMargins(0, 0, 0, 0)
        self.ui.chain_item_layout.setContentsMargins(4, 0, 0, 0)
        self.chain_list_widget = None
        self.set_model(model)
        self.set_mainwindow(mainwindow)

        self.move_done.connect(self._move_done)

    def _connect_widget(self, widget):
        '''
        Connect Widget.

        Connect the given widget with signals.

        Arguments:
            widget (QWidget): list item widget
        '''
        widget.selected.connect(self.selected)
        widget.account_log.connect(self.account_log)
        widget.account_settings.connect(self.account_settings)
        widget.account_create.connect(self.account_create)
        widget.account_item_dropped.connect(self.account_item_dropped)

        if isinstance(widget, (chain_list,)):
            return

        widget.account_item_dropped_in_chain.connect(
            self._drop_into_chain)
        widget.chain_collapse.connect(self._toggle_collapse)
        widget.chain_move_left.connect(self._move_left)
        widget.chain_move_right.connect(self._move_right)
        widget.chain_move_up.connect(self._move_up)
        widget.chain_move_down.connect(self._move_down)

    def _drop_into_chain(self, dropped_account_id, account_id, weight):
        '''
        Drop Into chain.

        Handle that dropped_account_id should have account_id as new
        chain_parent.

        Arguments:
            dropped_account_id (string): account_id that was dropped
            account_id (string): account_id that is the new parent
            weight (int): position in the children
        '''
        chain = self.chains.find_by_id(dropped_account_id)
        self.backend.chain_update.emit(
            account_id, dropped_account_id, weight,
            chain.connect_mode.get(), chain.failure_mode.get(), False,
            self.move_done, self.backend_failed)

    def _move_done(self, result):
        '''
        Move Done.

        Move in chain has completed, rerequest the preferences to ensure
        correct display.

        Arguments:
            result (string): backend response for move.
        '''
        self.backend.preferences_get.emit()

    def _move_left(self, account_id):
        '''
        Move Left.

        Move the given account id to its parent parent with a weight that it
        will be positioned after its current parent

        Arguments:
            account_id (string): account to be moved.
        '''
        chain = self.chains.find_by_id(account_id)
        chain_parent = self.chains.find_by_id(chain.parent_id.get())
        new_parent_account_id = chain_parent.parent_id.get()
        weight = chain_parent.weight.get() + 1

        self.backend.chain_update.emit(
            new_parent_account_id, account_id, weight,
            chain.connect_mode.get(), chain.failure_mode.get(), False,
            self.move_done,
            self.backend_failed)

    def _move_right(self, account_id):
        '''
        Move Right.

        Move the given account_id to have the parent of its predecessor
        in the current list. Will use a weight to be last in the new parent.

        Arguments:
            account_id (string): account to be moved.
        '''
        chain = self.chains.find_by_id(account_id)
        weight = 0
        parent_children = self.chains.get_children(chain.parent_id.get())
        prev_child = None
        for child in parent_children:
            if child.id.get() == account_id:
                break
            prev_child = child
        if prev_child is None:
            logger.warn('Inconsistent chain structure.')
            return
        prev_children = self.chains.get_children(prev_child.id.get())
        new_parent_account_id = prev_child.id.get()
        if len(prev_children) > 0:
            weight = prev_children[-1].weight.get() + 1

        self.backend.chain_update.emit(
            new_parent_account_id, account_id, weight,
            chain.connect_mode.get(), chain.failure_mode.get(), False,
            self.move_done,
            self.backend_failed)

    def _move_up(self, account_id):
        '''
        Move Up.

        Reduce weight for the given account_id.

        Arguments:
            account_id (string): account to be moved.
        '''
        chain = self.chains.find_by_id(account_id)
        self.backend.chain_update.emit(
            chain.parent_id.get(), account_id, chain.weight.get() - 11,
            chain.connect_mode.get(), chain.failure_mode.get(), False,
            self.move_done,
            self.backend_failed)

    def _move_down(self, account_id):
        '''
        Move Down.

        Increase weight for the given account_id.

        Arguments:
            account_id (string): account to be moved.
        '''
        chain = self.chains.find_by_id(account_id)
        self.backend.chain_update.emit(
            chain.parent_id.get(), account_id, chain.weight.get() + 11,
            chain.connect_mode.get(), chain.failure_mode.get(), False,
            self.move_done,
            self.backend_failed)

    def _toggle_collapse(self):
        '''
        Toggle Collapse.

        Toggle collapse for the chain list if there is one.
        '''
        if self.chain_list_widget is None:
            return

        self.chain_list_widget.set_collapsed(
            not self.chain_list_widget.is_collapsed())
        parent_id = self.model.parent_id.get()
        if parent_id is None:
            parent_id = ''
        self.backend.chain_update.emit(
            parent_id, self.model.id.get(),
            self.model.weight.get(),
            self.model.connect_mode.get(), self.model.failure_mode.get(),
            self.chain_list_widget.is_collapsed(),
            self.move_done, self.backend_failed)

    def _update_chain_list_items(self):
        '''
        Update chain item.

        Create the chain_list widget when the chain_item has children. When
        no children are available, the list widget is removed.
        When no mainwindow is available, this method does nothing.
        '''
        if not self.mainwindow:
            return
        if self.chains.has_children(self.model.id.get()):
            if self.chain_list_widget is None:
                self.chain_list_widget = chain_list(
                    self, chain_item, self.mainwindow, self.model)
                self.chain_list_widget.update_model(
                    self.accounts, self.groups, self.grouped_accounts,
                    self.chains)
                self._connect_widget(self.chain_list_widget)

                self.ui.chain_item_layout.addWidget(self.chain_list_widget)
        else:
            if self.chain_list_widget is not None:
                layout_item = self.ui.chain_item_layout.takeAt(
                    self.ui.chain_item_layout.indexOf(self.chain_list_widget))
                if layout_item is not None:
                    layout_item.widget().deleteLater()
                self.chain_list_widget.close()
                self.chain_list_widget = None

    def _update_state(self):
        '''
        Update State.

        Update state of the chain item with the current model values.
        '''
        if self.chain_list_widget is None:
            return
        collapsed = self.model.collapsed.get()
        self.chain_list_widget.set_collapsed(collapsed)
        self.ui.chain_account_widget.set_collapsed(collapsed)

    def closeEvent(self, event):
        '''
        Close Event.

        Widget was closed.

        Arguments:
            event (Qt.QEvent): Event detail.
        '''
        self.ui.chain_account_widget.close()
        if self.chain_list_widget is not None:
            self.chain_list_widget.close()
        self.is_destroyed = True

    def accept_mime_data(self, accept_mime_data):
        '''
        Accept Mime Data.

        Return True when the given mime_data has the correct format and
        may be evaluated its contents.

        Arguments:
            mime_data (QtCore.QMimeData): Mime data of element to be dropped.

        Returns:
            bool -- True when the mime data can be accepted.
        '''
        return False

    #
    # Public Interface
    #

    def update_model(
            self, accounts_model, groups_model,
            grouped_accounts_model, chains_model):
        '''
        Update Model

        Update model cache instances with the given models.

        Arguments:
            accounts_model (list): All configured accounts
            groups_model (list): All configured groups
            grouped_accounts_model (list): \
                All configured accounts with a group parent (unchained)
            chains_model (list): All configured accounts with \
                their chain relation.
        '''
        self.accounts = accounts_model
        self.groups = groups_model
        self.grouped_accounts = grouped_accounts_model
        self.chains = chains_model

        try:
            self.model = self.chains.find_by_id(self.model.id.get())

            self._update_state()
            self._update_chain_list_items()
        except NotFoundError:
            pass

    def set_mainwindow(self, mainwindow):
        '''
        Set the mainwindow (with its backend) to the widget for receiving model
        updates and triggering actions.

        Arguments:
            mainwindow (mainwindow): main application widget.
        '''
        self.mainwindow = mainwindow
        if self.mainwindow is None:
            return
        self.backend = mainwindow.backend

        self.backend.application.event_loop.accounts_changed.connect(
            self.update_model)

        self.ui.chain_account_widget.set_mainwindow(mainwindow)
        self._connect_widget(self.ui.chain_account_widget)

        self._update_chain_list_items()

    def set_model(self, model):
        '''
        Set Model.

        Set model for the widget.

        Arguments:
            model (model.group_item): Group item that represents the item \
                in the list.
        '''
        self.model = model

        if self.model is None:
            return
        try:
            account_model = self.accounts.find_by_id(model.id.get())
            self.ui.chain_account_widget.set_model(account_model)
            self.ui.chain_account_widget.update_model(
                self.accounts, self.groups, self.grouped_accounts,
                self.chains)
        except NotFoundError:
            pass
        self._update_state()
        self._update_chain_list_items()
