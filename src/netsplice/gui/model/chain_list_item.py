# -*- coding: utf-8 -*-
# chain_list_item.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for chain items.
'''

from netsplice.config import model as config_model
from netsplice.config import backend as config_backend
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.validator.account_id import (
    account_id as account_id_validator
)
from netsplice.model.validator.account_id_value import (
    account_id_value as account_id_value_validator
)
from netsplice.gui.model.validator.weight import (
    weight as weight_validator
)
from netsplice.model.validator.boolean import (
    boolean as boolean_validator
)
from netsplice.model.validator.enum import (
    enum as enum_validator
)
from netsplice.model.validator.version import (
    version as version_validator
)


class chain_list_item(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.id = field(
            required=True,
            validators=[account_id_validator()])

        self.parent_id = field(
            required=False,
            default=None,
            validators=[account_id_value_validator()])

        self.connect_mode = field(
            required=False,
            default=config_backend.CONNECT_MODE_SEQUENCE,
            validators=[enum_validator(config_backend.CONNECT_MODES)])

        self.failure_mode = field(
            required=False,
            default=config_backend.FAILURE_MODE_BREAK,
            validators=[enum_validator(config_backend.FAILURE_MODES)])

        self.weight = field(
            required=False,
            default=0,
            validators=[weight_validator()])

        self.collapsed = field(
            required=False,
            default=False,
            validators=[boolean_validator()])

        self.version = field(
            required=True,
            default=config_model.VERSION,
            validators=[version_validator()])
