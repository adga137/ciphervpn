# -*- coding: utf-8 -*-
# chain_list.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Account chain Preferences.
'''
import sys
from netsplice.util.model.marshalable_list import (
    marshalable_list
)
from netsplice.gui.model.chain_list_item import (
    chain_list_item as chain_list_item_model
)
from netsplice.util.errors import (
    NotFoundError, ConnectedError
)
from netsplice.util import get_logger

logger = get_logger()


class chain_list(marshalable_list):
    def __init__(self):
        marshalable_list.__init__(self, chain_list_item_model)

    def _get_weight(self, chain_model_instance):
        '''
        Get Weight.

        Return weight for a chain item for sorting.
        '''
        return chain_model_instance.weight.get()

    def apply_values(self):
        '''
        Apply Values.

        Apply values from the preferences to the config so other
        components can use the values.
        '''
        pass

    def find_by_id(self, account_id):
        '''
        Find By Id.

        Find a chain for an account with the given account_id.
        '''
        for chain in self:
            if chain.id.get() == account_id:
                return chain
        raise NotFoundError(
            'No chain for account %s found.' % (account_id,))

    def find_by_parent_id(self, parent_account_id):
        '''
        Find by Parent Id.

        Return a chain that has the given parent_account_id
        defined as parent. When no account with the parent_account_id is
        found, a NotFoundError is raised.
        '''
        chain_items = []
        for chain in self:
            if chain.parent_id.get() == parent_account_id:
                chain_items.append(chain)
        if len(chain_items) is 0:
            raise NotFoundError(
                'No parent chain for account %s found.'
                % (parent_account_id,))
        return chain_items

    def get_account_list(self, chain_accounts, accounts):
        '''
        Get Account List.

        Return a list of account_items that match the ids in the
        chain_accounts. This method operates on a given
        chain_accounts list to allow sorting and filtering before getting
        the accounts.
        '''
        account_list = []
        for chain in chain_accounts:
            account_list.append(accounts.find_by_id(chain.id.get()))
        return account_list

    def has_children(self, account_id):
        '''
        Has Children.

        Check if there are chain children for the given account_id.

        Arguments:
            account_id (string): account_id to check.

        Returns:
            bool -- True when at least one children exists.
        '''
        try:
            self.find_by_parent_id(account_id)
            return True
        except NotFoundError:
            return False

    def get_children(self, account_id):
        '''
        Get Children.

        Return all direct children for the given account_id.

        Arguments:
            account_id (string): id for which the children should be compiled

        Returns:
            list -- sorted list of chains.
        '''
        children_list = list()
        sorted_children_list = list()
        for chain in self:
            if chain.parent_id.get() == account_id:
                children_list.append(chain)

        sorted_children_iterator = sorted(
            children_list, key=self._order_weighted)
        for chain in sorted_children_iterator:
            sorted_children_list.append(chain)
        return sorted_children_list

    def get_children_for_id(self, account_id):
        '''
        Get Chain for id

        Return the all accounts that are chained to the given account_id.

        [a -> b -> c -> d] for b returns [c, d]
        [a -> b -> c -> d] for a returns [b, c, d]

        '''
        chain_children = []
        siblings = []
        start_chain = self.find_by_id(account_id)
        try:
            siblings = self.get_siblings_for_id(start_chain.id.get())
        except NotFoundError:
            return chain_children

        for item in siblings:
            chain_children.append(item)
            try:
                chain_children.extend(
                    self.get_children_for_id(item.id.get()))
            except NotFoundError:
                pass
        return chain_children

    def get_parents_for_id(self, account_id):
        '''
        Get Parents for id.

        Return the parent chain for the given account_id.

        [a -> b -> c -> d] for b returns [a]
        '''
        parents = []
        try:
            chain = self.find_by_id(account_id)
            while chain.parent_id.get() is not None:
                chain = self.find_by_id(chain.parent_id.get())
                if chain in parents:
                    logger.warn('Recursion in chain')
                    break
                parents.append(chain)
        except NotFoundError as errors:
            logger.error('Chain is broken at %s' % (str(errors,)))
            raise errors
        return parents

    def get_siblings_for_id(self, account_id):
        '''
        Get Siblings for id.

        Return the accounts that are chained directly to the given account_id.

        [a -> b -> c -> d] for b returns [c]
        [a -> b -> c -> d] for a returns [b]
        [a -> b, a -> c] for a returns [b, c]

        '''
        chain_siblings = []
        siblings = []
        start_chain = self.find_by_id(account_id)

        try:
            siblings = self.find_by_parent_id(start_chain.id.get())
        except NotFoundError:
            return chain_siblings

        for item in siblings:
            chain_siblings.append(item)

        return chain_siblings

    def _order_weighted(self, chain_item):
        return chain_item.weight.get()
