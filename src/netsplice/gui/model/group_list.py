# -*- coding: utf-8 -*-
# group_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for GUI process. Stores all information the GUI should know about
'''

import netsplice.config.backend as config

from netsplice.util.errors import NotFoundError
from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.gui.model.group_item import (
    group_item as group_item_model
)


class group_list(marshalable_list):
    def __init__(self):
        marshalable_list.__init__(self, group_item_model)

    def get_parent_groups(self, root_group):
        '''
        Get Parent Groups.

        Return a list of groups that are parent to the given root_group.
        When a parent is listed multiple times, a NotUniqueError is raised.

        :param root_group backend.preferences.model.group_item
        '''
        groups = []
        parent_id = root_group.parent.get()
        while parent_id is not None:
            parent_group = self.find_by_id(parent_id)
            if parent_group in groups:
                raise NotUniqueError(
                    'Parents is duplicate: %s'
                    % (parent_id,))
            groups.append(parent_group)
            parent_id = parent_group.parent.get()
        return groups

    def find_by_id(self, group_id):
        for group in self:
            if group.id.get() == group_id:
                return group
        raise NotFoundError(
            'No group with id %s found.' % (group_id,))

    def find_root(self):
        for group in self:
            if group.immutable.get() is False:
                continue
            if group.parent.get() is None:
                return group
        raise NotFoundError('No group with None parent found.')

    def find_global(self):
        for group in self:
            if group.immutable.get() is False:
                continue
            if group.name.get() == config.GLOBAL_GROUP_NAME:
                return group
        raise NotFoundError(
            'No group with name %s parent found.'
            % (config.GLOBAL_GROUP_NAME,))

    def find_children(self, group_id):
        children = group_list()
        for group in self:
            if group.parent.get() == group_id:
                children.append(group)
        return children
