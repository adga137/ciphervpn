# -*- coding: utf-8 -*-
# connection_list_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Model for listing accounts.
'''

from netsplice.config import connection as config_connection
from netsplice.model.named_value_list import (
    named_value_list as named_value_list_model
)
from netsplice.model.validator.account_id_value import (
    account_id_value as account_id_value_validator
)
from netsplice.model.validator.boolean import boolean as boolean_validator
from netsplice.model.validator.byte import byte as byte_validator
from netsplice.model.validator.connection_id import (
    connection_id as connection_id_validator
)
from netsplice.model.validator.enum import enum as enum_validator
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable


class connection_list_item(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.id = field(
            required=True,
            validators=[connection_id_validator()])

        self.account_id = field(
            required=True,
            validators=[account_id_value_validator()])

        self.failed = field(
            default=False,
            required=True,
            validators=[boolean_validator()])

        self.state = field(
            default=None,
            required=True,
            validators=[enum_validator(config_connection.STATES)])

        self.read_tap_velocity = field(
            default=0,
            required=True,
            validators=[byte_validator()])

        self.write_tap_velocity = field(
            default=0,
            required=True,
            validators=[byte_validator()])

        self.read_udp_velocity = field(
            default=0,
            required=True,
            validators=[byte_validator()])

        self.write_udp_velocity = field(
            default=0,
            required=True,
            validators=[byte_validator()])

        self.environment = named_value_list_model()
