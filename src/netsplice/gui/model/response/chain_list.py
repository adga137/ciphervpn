# -*- coding: utf-8 -*-
# chain_list.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for listing stored chains via REST response.
'''

from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.gui.model.response.chain_item import (
    chain_item as chain_item_model
)


class chain_list(marshalable_list):
    '''
    Marshalable model that contains multiple items of
    chain_item_model.
    '''
    def __init__(self):
        marshalable_list.__init__(self, chain_item_model)
