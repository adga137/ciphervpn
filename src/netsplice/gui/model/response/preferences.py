# -*- coding: utf-8 -*-
# preferences.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Model for Preferences Responses.
'''

from netsplice.gui.model.response.account_list import (
    account_list as account_list_model
)
from netsplice.gui.model.response.chain_list import (
    chain_list as chain_list_model
)
from netsplice.gui.model.response.group_list import (
    group_list as group_list_model
)
from netsplice.gui.model.response.grouped_account_list import (
    grouped_account_list as grouped_account_list_model
)
from netsplice.gui.model.response.ui import (
    ui as ui_model
)
from netsplice.gui.model.response.backend import (
    backend as backend_model
)
from netsplice.model.plugins import (
    plugins as plugins_model
)
from netsplice.gui.model.request import (
    model as request_model
)
from netsplice.gui.model.response import (
    model as response_model
)
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable


class preferences(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.accounts = account_list_model()
        self.ui = ui_model()
        self.backend = backend_model()
        self.chains = chain_list_model()
        self.groups = group_list_model()
        self.grouped_accounts = grouped_account_list_model()
        self.plugins = plugins_model(None)
        self.request = request_model()
        self.response = response_model()

    def register_plugins(self, plugins):
        for plugin in plugins:
            self.plugins.register_plugin(
                plugin.name.get(),
                plugin)
