# -*- coding: utf-8 -*-
# account_list_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Model for listing accounts.
'''

from netsplice.gui.model.request import (
    model as request_model
)
from netsplice.gui.model.response.account_list_item import (
    account_list_item
)
from netsplice.gui.model.response import (
    model as response_model
)
from netsplice.model.plugins import (
    plugins as plugins_model
)
from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.util.errors import NotFoundError


class account_list(marshalable_list):
    '''
    Marshalable model that contains multiple items of
    account_list_item_model.
    '''
    def __init__(self):
        marshalable_list.__init__(self, account_list_item)

        self.plugins = plugins_model(self)
        self.request = request_model()
        self.response = response_model()

    def register_plugins(self, plugins):
        for plugin in plugins:
            self.plugins.register_plugin(
                plugin.name.get(),
                plugin)

    def find_by_id(self, account_id):
        for account in self:
            if account.id.get() == account_id:
                return account
        raise NotFoundError(
            'No account with id %s found.' % (account_id,))
