# -*- coding: utf-8 -*-
# account_list_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Model for listing accounts.
'''

from netsplice.gui.model.account_list_item import (
    account_list_item
)
from netsplice.gui.model.request import (
    model as request_model
)
from netsplice.gui.model.response import (
    model as response_model
)
from netsplice.model.plugins import (
    plugins as plugins_model
)
from netsplice.model.log_item import log_item as log_item_model
from netsplice.util.errors import NotFoundError
from netsplice.util.model.marshalable_list import marshalable_list


class account_list(marshalable_list):
    '''
    Marshalable model that contains multiple items of
    account_list_item_model.
    '''
    def __init__(self):
        marshalable_list.__init__(self, account_list_item)

        self.plugins = plugins_model(self)
        self.request = request_model()
        self.response = response_model()

    def add_account(self, account_model_instance):
        '''
        Add Account.

        Add the given (module) account_model_instance to the account_list by
        creating a new instance, populate it with values and append it to the
        collection.
        '''
        account = self.item_model_class()
        account.id.set(account_model_instance.id.get())
        self.append(account)

    def delete_account(self, account_id):
        '''
        Delete Account.

        Delete Account with given account_id.
        '''
        account = self.find_by_id(account_id)
        self.remove(account)

    def find_by_id(self, account_id):
        '''
        Find by Id.

        Return the account with the given id or raise a NotFoundError.
        '''
        for account in self:
            if account.id.get() == account_id:
                return account
        raise NotFoundError(
            'No account with id %s found.' % (account_id,))
