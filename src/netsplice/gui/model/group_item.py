# -*- coding: utf-8 -*-
# group_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for GUI process. Stores all information the GUI should know about
'''

from netsplice.config import backend as config_backend
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.gui.model.validator.group_name import (
    group_name as group_name_validator
)
from netsplice.gui.model.validator.weight import (
    weight as weight_validator
)
from netsplice.model.validator.boolean import boolean as boolean_validator
from netsplice.model.validator.enum import (
    enum as enum_validator
)
from netsplice.model.validator.group_id_value import (
    group_id_value as group_id_value_validator
)


class group_item(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.id = field(
            required=True,
            validators=[group_id_value_validator()])

        self.name = field(
            required=False,
            validators=[group_name_validator()])

        self.parent = field(
            required=False,
            default=None,
            validators=[group_id_value_validator()])

        self.weight = field(
            required=False,
            default=0,
            validators=[weight_validator()])

        self.autostart = field(
            required=False,
            default=0,
            validators=[boolean_validator()])

        self.connect_mode = field(
            required=False,
            default=config_backend.CONNECT_MODE_SEQUENCE,
            validators=[enum_validator(config_backend.CONNECT_MODES)])

        self.failure_mode = field(
            required=False,
            default=config_backend.FAILURE_MODE_BREAK,
            validators=[enum_validator(config_backend.FAILURE_MODES)])

        self.collapsed = field(
            required=False,
            default=False,
            validators=[boolean_validator()])

        self.enabled = field(
            required=False,
            default=True,
            validators=[boolean_validator()])

        self.immutable = field(
            required=False,
            default=False,
            validators=[boolean_validator()])
