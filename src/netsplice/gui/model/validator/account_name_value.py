# -*- coding: utf-8 -*-
# account_name_value.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Validator account name values.
'''

from netsplice.util.model.validator import validator
from netsplice.util import basestring


class account_name_value(validator):
    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        '''
        Account names are simple nonempty strings.
        '''
        # XXX here too are constraints not all chars should go here.  You
        # never know how the backend looks like and you dont want people to
        # allow to mess with injections etc.
        if not isinstance(value, (basestring)):
            return False

        if value == '':
            return False

        return True
