# -*- coding: utf-8 -*-
# config.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Config for Systray
'''


# Delay before a (optional) welcome message is displayed in the
# system tray. Milliseconds
SYSTRAY_STARTUP_DELAY = 1000

# Delay for animated icons
SYSTRAY_ANIMATION_DELAY = 500

# State Icons
RES_ICON_TRAY = (
    ':/images/systray/disconnected.png')
RES_ICON_TRAY_CONNECTED = (
    ':/images/systray/connected.png')
RES_ICON_TRAY_CONNECTING_0 = (
    ':/images/systray/connecting_0.png')
RES_ICON_TRAY_CONNECTING_1 = (
    ':/images/systray/connecting_1.png')
RES_ICON_TRAY_CONNECTING_2 = (
    ':/images/systray/connecting_2.png')
RES_ICON_TRAY_BACKEND_NOT_AVAILABLE = (
    ':/images/systray/backend_not_available.png')
