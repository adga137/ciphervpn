# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/gui/credential/views/credential.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Credential(object):
    def setupUi(self, Credential):
        Credential.setObjectName("Credential")
        Credential.resize(280, 220)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/images/netsplice-icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Credential.setWindowIcon(icon)
        self.main_layout = QtGui.QGridLayout(Credential)
        self.main_layout.setObjectName("main_layout")
        self.messages = QtGui.QWidget(Credential)
        self.messages.setObjectName("messages")
        self.messages_layout = QtGui.QVBoxLayout(self.messages)
        self.messages_layout.setContentsMargins(0, 0, 0, 0)
        self.messages_layout.setObjectName("messages_layout")
        self.reason = QtGui.QLabel(self.messages)
        self.reason.setObjectName("reason")
        self.messages_layout.addWidget(self.reason)
        self.wrong_password = QtGui.QLabel(self.messages)
        self.wrong_password.setVisible(False)
        self.wrong_password.setObjectName("wrong_password")
        self.messages_layout.addWidget(self.wrong_password)
        self.main_layout.addWidget(self.messages, 0, 0, 1, 2)
        self.username_label = QtGui.QLabel(Credential)
        self.username_label.setObjectName("username_label")
        self.main_layout.addWidget(self.username_label, 1, 0, 1, 1)
        self.username = QtGui.QLineEdit(Credential)
        self.username.setObjectName("username")
        self.main_layout.addWidget(self.username, 1, 1, 1, 1)
        self.password_label = QtGui.QLabel(Credential)
        self.password_label.setObjectName("password_label")
        self.main_layout.addWidget(self.password_label, 2, 0, 1, 1)
        self.password = QtGui.QLineEdit(Credential)
        self.password.setEchoMode(QtGui.QLineEdit.Password)
        self.password.setObjectName("password")
        self.main_layout.addWidget(self.password, 2, 1, 1, 1)
        self.store_password_label = QtGui.QLabel(Credential)
        self.store_password_label.setObjectName("store_password_label")
        self.main_layout.addWidget(self.store_password_label, 3, 0, 1, 1)
        self.store_password_type = QtGui.QComboBox(Credential)
        self.store_password_type.setObjectName("store_password_type")
        self.main_layout.addWidget(self.store_password_type, 3, 1, 1, 1)
        self.widget = QtGui.QWidget(Credential)
        self.widget.setObjectName("widget")
        self.action_layout = QtGui.QHBoxLayout(self.widget)
        self.action_layout.setContentsMargins(0, 0, 0, 0)
        self.action_layout.setObjectName("action_layout")
        self.action_cancel = QtGui.QPushButton(self.widget)
        self.action_cancel.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.action_cancel.setObjectName("action_cancel")
        self.action_layout.addWidget(self.action_cancel)
        self.action_ok = QtGui.QPushButton(self.widget)
        self.action_ok.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.action_ok.setObjectName("action_ok")
        self.action_layout.addWidget(self.action_ok)
        self.main_layout.addWidget(self.widget, 4, 0, 1, 2)

        self.retranslateUi(Credential)
        QtCore.QMetaObject.connectSlotsByName(Credential)
        Credential.setTabOrder(self.username, self.password)
        Credential.setTabOrder(self.password, self.store_password_type)
        Credential.setTabOrder(self.store_password_type, self.action_ok)
        Credential.setTabOrder(self.action_ok, self.action_cancel)

    def retranslateUi(self, Credential):
        Credential.setWindowTitle(QtGui.QApplication.translate("Credential", "Credentials Required", None, QtGui.QApplication.UnicodeUTF8))
        self.reason.setText(QtGui.QApplication.translate("Credential", "Reason", None, QtGui.QApplication.UnicodeUTF8))
        self.wrong_password.setText(QtGui.QApplication.translate("Credential", "Wrong Password", None, QtGui.QApplication.UnicodeUTF8))
        self.username_label.setText(QtGui.QApplication.translate("Credential", "Username:", None, QtGui.QApplication.UnicodeUTF8))
        self.password_label.setText(QtGui.QApplication.translate("Credential", "Password:", None, QtGui.QApplication.UnicodeUTF8))
        self.store_password_label.setText(QtGui.QApplication.translate("Credential", "Store Password:", None, QtGui.QApplication.UnicodeUTF8))
        self.action_cancel.setText(QtGui.QApplication.translate("Credential", "Cancel", None, QtGui.QApplication.UnicodeUTF8))
        self.action_ok.setText(QtGui.QApplication.translate("Credential", "Ok", None, QtGui.QApplication.UnicodeUTF8))

