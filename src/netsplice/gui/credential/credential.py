# -*- coding: utf-8 -*-
# credential.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Credential input Window.

Dialog that asks for username and password for a given account.
'''
from PySide.QtCore import (
    Qt, Signal, QTimer
)
from netsplice.config import backend as config_backend
from credential_ui import Ui_Credential
from netsplice.gui.widgets.dialog import dialog
from netsplice.util import get_logger

logger = get_logger()


class credential(dialog):
    '''
    Credential Dialog.

    Dialog that allows the user to input username and password.

    '''
    TYPE_USERNAME_PASSWORD = 'username_password'
    TYPE_PRIVATE_KEY_PASSWORD = 'private_key_password'
    backend_success = Signal(basestring)
    backend_wrong = Signal(basestring)
    backend_failure = Signal(basestring)
    cancel_done = Signal(basestring)

    def __init__(self, parent, dispatcher):
        '''
        Initialize the widget.

        :param parent: The widget that owns the logviewer (usualy mainwindow)
        :param dispatcher: backend dispatcher for model change events
        '''
        dialog.__init__(self, parent)

        self.type = self.TYPE_USERNAME_PASSWORD

        # Load UI
        self.ui = Ui_Credential()
        self.ui.setupUi(self)
        self.set_minimal()
        self.backend = dispatcher
        self._add_store_types()
        self.ui.action_ok.clicked.connect(self.ok_action)
        self.ui.action_cancel.clicked.connect(self.cancel_action)
        self.backend_failure.connect(self._backend_failure)
        self.backend_wrong.connect(self._backend_wrong)
        self.backend_success.connect(self._backend_success)
        self.cancel_done.connect(self._cancel_done)

    def _add_store_types(self):
        '''
        Add store types.

        Adds configured store types to combobox that allows the user to select
        how this password is handled.
        '''
        default_index = 0
        for index, item in enumerate(config_backend.STORE_PASSWORD_TYPES):
            self.ui.store_password_type.addItem(
                item['label'], item['name'])
            if item['name'] == config_backend.STORE_PASSWORD_DEFAULT:
                default_index = index
        self.ui.store_password_type.setCurrentIndex(default_index)

    def _backend_failure(self, message):
        '''
        Backend failure handler.

        The backend signaled a failure processing the username & password
        combination.
        '''
        pass

    def _backend_success(self, model_json):
        '''
        Handle a successful authentication by the privileged backend.

        Reset all input and close the dialog.
        '''
        self.deleteLater()

    def _backend_wrong(self, message):
        '''
        Handle wrong Username/Password.

        Displays the message and focuses the password field.
        '''
        self.ui.wrong_password.setText(message)
        self.ui.wrong_password.show()
        self.ui.password.setFocus()

    def _cancel_done(self):
        '''
        Cancel Message Done.

        Cancel has been processed by the backend.
        '''
        self.backend.account_reset.emit(
            self.model.id.get(),
            self.backend_success, self.backend_failure)

    def _set_form_elements(self):
        '''
        Set form elements.

        Hide or show form elements based on the dialog type.
        '''
        if self.type == self.TYPE_PRIVATE_KEY_PASSWORD:
            self.ui.username.hide()
            self.ui.username_label.hide()

    def _set_reason_text(self):
        '''
        Set Reason Text.

        Set the reason label over the username so the user knows what the
        prompt is about. Formats a html message from translated snippets.
        '''
        account_name = ''
        account_type = ''
        if self.model is not None:
            account_name = self.model.name.get()
            account_type = self.model.type.get()
        reason_tr = self.tr(r'Please enter your credentials')
        reason_tr += '<br/>'
        reason_tr += self.tr(r'for %(type)s')
        reason_tr += '<br/>'
        reason_tr += r'<strong>"%(name)s"</strong>'
        if self.type == self.TYPE_PRIVATE_KEY_PASSWORD:
            reason_tr += '<br/><strong>%s</strong>' % (self.tr('Private Key'),)
        reason_tr = '<center>' + reason_tr + '</center>'
        self.ui.reason.setText(reason_tr % {
            'type': account_type,
            'name': account_name})

    def cancel_action(self):
        '''
        Cancel Action.

        Close the window and forget everything that has been entered.
        '''
        self.ui.username.setText('')
        self.ui.password.setText('')
        self.close()

    def closeEvent(self, event):
        '''
        Widget Close event.

        Signal the backend that the prompt will never be completed.
        '''
        self.backend.credential_cancel_request.emit(
            self.cancel_done, self.backend_failure)

    def hideEvent(self, event):
        '''
        Widget Hide event.

        Signal the backend that the prompt will never be completed.
        '''
        self.ui.username.setText('')
        self.ui.password.setText('')
        self.close()

    def ok_action(self):
        '''
        OK Action.

        Send the filled form to the backend and wait for it to succeed.

        '''
        if self.ui.password.text() == '' and self.ui.username.hasFocus():
            self.ui.password.setFocus()
            return
        password_store_policy = self.ui.store_password_type.itemData(
            self.ui.store_password_type.currentIndex())
        config_backend.STORE_PASSWORD_DEFAULT = password_store_policy
        self.backend.credential_send.emit(
            self.model.id.get(),
            self.ui.username.text(),
            self.ui.password.text(),
            password_store_policy,
            self.backend_success, self.backend_failure)

    def set_account(self, account_model_instance):
        '''
        Set Account.

        Set the Account that wants a password.
        '''
        self.model = account_model_instance

    def set_type(self, query_type):
        '''
        Set Type.

        Set the dialog type: 'username_password' or 'private_key'
        '''
        self.type = query_type
        self._set_form_elements()
        self._set_reason_text()
