# -*- coding: utf-8 -*-
# utils.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Log Model Handler.

Interface to logbook that receives all logging infos and puts them in a
fifo-list. This class does not use the model.log_list to allow the logger to
be used in the marshalable utilities.
'''

import sys

from logbook import (
    StringFormatterHandlerMixin,
    Handler
)
from logbook.base import get_level_name
from threading import Lock

from netsplice.config import log as config
from netsplice.config import flags as flags


class logger_model_handler(Handler, StringFormatterHandlerMixin):
    '''
    Logger Model Handler.

    Logbook handler that buffers log entries and commits them to a marshalable
    model.
    '''

    def __init__(
            self, name=None, filter=None, bubble=False,
            buffer_size=config.MAX_UNPROCESSED_LOG_ENTRIES):
        '''
        Initialize.

        Initialize members and base.
        '''
        Handler.__init__(
            self,
            filter=filter,
            bubble=bubble)
        self._name = name
        if self._name is None:
            self._name = sys.executable
        StringFormatterHandlerMixin.__init__(self, config.LOG_MODEL_FORMAT)
        self._buffer_size = buffer_size

        self._is_marshalable = False
        self._log = []
        self._log_index = 0
        self._lock = Lock()

    def append(self, log_item, marshalable=True):
        '''
        Append.

        Append a log item.
        '''
        try:
            self.lock()
            if isinstance(log_item, (dict,)):
                log_item['index'] = self._log_index
            else:
                log_item.index.set(self._log_index)

            if marshalable:
                self._log.append_from_dict(log_item)
                self._log.commit()
            else:
                self._log.append(log_item)

            if len(self._log) > self._buffer_size:
                del self._log[0]
            self._log_index += 1
        finally:
            self.unlock()

    def check_log_content(self, message_content):
        '''
        Check Log Content.

        Evaluate the log buffer for the given message content. Return True
        when the message content is in the log.
        '''
        found = False
        try:
            self.lock()
            for log_item in self._log:
                message = ''
                if isinstance(log_item, (dict,)):
                    message = log_item['message']
                else:
                    message = log_item.message.get()
                if message_content not in message:
                    continue
                found = True
                break
        finally:
            self.unlock()
        return found

    def emit(self, record):
        '''
        Emit Log Record.

        Handle a log message by creating a mocked log_item from the record.
        Pulls the information and closes the record so the file/line
        information can be extracted.
        '''
        record.pull_information()
        record.close()
        log_item = {}
        record_dict = record.to_dict(json_safe=True)
        log_item['date'] = record_dict['time']
        log_item['level'] = get_level_name(record_dict['level'])
        log_item['origin'] = self._name
        log_item['message'] = record_dict['message']
        log_item['formated_message'] = self.format(record)
        if log_item['message'] is None or log_item['message'].strip() == '':
            log_item['message'] = log_item['formated_message']
        log_item['code_location'] = (
            'L#%s : %s::%s'
            % (
                record_dict['lineno'],
                record_dict['module'],
                record_dict['func_name']))
        log_item['extra'] = {}
        if flags.VERBOSE > 0 and ' 200 ' in log_item['message']:
            # avoid tornado-logs causing accumulating log posts
            # during debug
            return
        if 'extra' in record_dict.keys():
            if 'connection_id' in record_dict['extra'].keys():
                log_item['extra']['connection_id'] = (
                    record_dict['extra']['connection_id'])
        self.append(log_item, self._is_marshalable)

    def get_logs(self):
        '''
        Get Logs.

        Return the current available log_entries and keep the current buffer.
        '''
        logs = []
        try:
            self.lock()
            for log_item in self._log:
                logs.append(log_item)
        finally:
            self.unlock()
        return logs

    def lock(self):
        '''
        Lock the handler.

        The Log may be caused from different threads. Ensure that each thread
        gets a consistent log object. Used by this class to fence calls.
        '''
        self._lock.acquire()

    def unlock(self):
        '''
        Unlock the handler.

        Release the lock on the handler.
        '''
        self._lock.release()

    def set_model(self, log_list_model):
        '''
        Set Model.

        Set the given log_list model as model and transfer all log items that
        have been buffered up to now.
        '''
        self._is_marshalable = True
        buffered_logs = self.take_logs()
        try:
            self.lock()
            self._log = log_list_model
            for log_dict_item in buffered_logs:
                self._log.append_from_dict(log_dict_item)
            if len(buffered_logs):
                self._log.commit()
        finally:
            self.unlock()

    def take_logs(self):
        '''
        Take Logs.

        Return the current available log_entries and purge the current buffer.
        '''
        logs = []
        try:
            self.lock()
            for log_item in self._log:
                logs.append(log_item)
            del self._log[:]
        finally:
            self.unlock()
        return logs
