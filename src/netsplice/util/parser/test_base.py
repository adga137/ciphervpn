# -*- coding: utf-8 -*-
# test_base.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Tests for Parser Base
'''

import mock
import os
import sys
from .base import base
from netsplice.model.configuration_item import (
    configuration_item as configuration_item_model
)
from netsplice.model.configuration_list import (
    configuration_list as configuration_list_model
)
from netsplice.util.errors import (
    InvalidUsageError
)


def get_instance():
    instance = base()
    return instance


def test_analyze_with_empty_current_token_returns_false():
    instance = get_instance()
    instance.current_token = ''
    result = instance.analyze()
    print(result)
    # analyze should not do anything with empty current
    assert(result is False)


def test_analyze_with_current_token_returns_true():
    instance = get_instance()
    instance.current_token = 'testvalue'
    result = instance.analyze()
    # analyze should not do anything with empty current
    assert(result is True)


def test_commit_extends_elements():
    instance = get_instance()
    before_len = len(instance.elements)
    instance.commit()
    after_len = len(instance.elements)
    assert(before_len < after_len)
    assert(before_len == after_len - 1)


def test_commit_resets_current_element():
    instance = get_instance()
    instance.current_element.index.set(1234)
    instance.current_element.key.set('testkey')
    instance.current_element.add_value('value', 'testvalue')
    instance.commit()
    assert(instance.current_element.index.get() != 1234)
    assert(instance.current_element.key.get() != 'test_key')
    assert(len(instance.current_element.values) == 0)


def test_get_config_returns_elements():
    instance = get_instance()
    instance.commit()
    assert(len(instance.elements) == 1)
    result = instance.get_config()
    assert(len(result) == 1)


def test_get_element_index_with_none_raises():
    instance = get_instance()
    try:
        result = instance.get_element_index(None)
        assert(False)  # Expect Exception
    except InvalidUsageError:
        assert(True)
    except:
        assert(False)  # Expect Named Exception


def test_get_element_index_with_configuration_returns_index():
    instance = get_instance()
    item = configuration_item_model(1234)
    result = instance.get_element_index(item)
    assert(result == 1234)


def test_get_insert_template_returns_empty_configuration_item():
    instance = get_instance()
    result = instance.get_insert_template()
    assert(isinstance(result, (configuration_item_model,)))
    assert(result.get_value('token') == '')


def test_get_text_without_elements_returns_emtpy_string():
    instance = get_instance()
    result = instance.get_text()
    assert(result == '')


def test_get_text_with_elements_returns_spaced_string():
    instance = get_instance()
    a_item = configuration_item_model(0)
    b_item = configuration_item_model(0)
    c_item = configuration_item_model(0)
    a_item.values.add_value('token', 'a')
    b_item.values.add_value('token', 'b')
    c_item.values.add_value('token', 'c')
    instance.elements.append(a_item)
    instance.elements.append(b_item)
    instance.elements.append(c_item)

    result = instance.get_text()
    assert(result == 'a b c')


def test_reset_empties_element_list():
    instance = get_instance()
    instance.commit()
    assert(len(instance.elements) == 1)
    instance.reset()
    assert(len(instance.elements) == 0)


def test_reset_resets_current_element():
    instance = get_instance()
    instance.current_element.key.set('testkey')
    instance.current_element.values.add_value('value', 'testvalue')
    instance.reset()
    assert(instance.current_element.key.get() != 'testkey')
    assert(len(instance.current_element.values) == 0)


def test_set_config_with_none_raises():
    instance = get_instance()
    try:
        instance.set_config(None)
        assert(False)  # expected Exception
    except InvalidUsageError:
        assert(True)  # named Exception raised
    except Exception as error:
        print(error)
        assert(False)  # unknown Exception raised


def test_set_config_with_configuration_list_fills_elements():
    instance = get_instance()
    configuration_list = configuration_list_model()
    item = configuration_item_model(0)
    configuration_list.append(item)
    instance.set_config(configuration_list)
    result = len(instance.elements)
    print('expect length of 1', result)
    assert(result == len(configuration_list))


def test_set_text_with_none_raises():
    instance = get_instance()
    try:
        instance.set_text(None)
        assert(False)  # expected Exception
    except InvalidUsageError:
        assert(True)  # named Exception raised
    except Exception as error:
        print(error)
        assert(False)  # unknown Exception raised


@mock.patch('netsplice.util.parser.base.base.analyze')
def test_set_text_with_single_word_calls_analyze(mock_analyze):
    instance = get_instance()
    instance.set_text('word')

    assert(mock_analyze.called == 1)


@mock.patch('netsplice.util.parser.base.base.commit')
@mock.patch('netsplice.util.parser.base.base.analyze')
def test_set_text_with_single_word_calls_commit(mock_analyze, mock_commit):
    instance = get_instance()
    instance.set_text('word')

    assert(mock_commit.called == 1)


@mock.patch('netsplice.util.parser.base.base.commit')
@mock.patch('netsplice.util.parser.base.base.analyze')
def test_set_text_with_empty_string_not_calls_commit_or_analyze(
        mock_analyze, mock_commit):
    instance = get_instance()
    instance.set_text('')

    assert(mock_analyze.called == 0)
    assert(mock_commit.called == 0)
