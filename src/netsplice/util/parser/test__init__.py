# -*- coding: utf-8 -*-
# test_base.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Tests for Parser Base
'''

from . import register_parser, reset_registered, factory, get_parser

from netsplice.util.errors import (
    UnknownParserTypeError
)


class mock_parser(object):
    def __init__(self):
        pass


def test_register_parser_adds_parser():
    reset_registered()
    result = get_parser()
    assert(len(result.keys()) == 0)
    register_parser('type', mock_parser)
    result = get_parser()
    assert(len(result.keys()) == 1)

def test_factory_known_returns_instance():
    reset_registered()
    register_parser('type', mock_parser)
    result = factory('type')
    assert(isinstance(result, (mock_parser,)))


def test_factory_unknown_raises():
    reset_registered()
    register_parser('type', mock_parser)
    try:
        factory('unknown')
        assert(False)  # Exception expected
    except UnknownParserTypeError:
        assert(True)
