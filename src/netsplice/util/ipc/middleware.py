# -*- coding: utf-8 -*-
# middleware.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
IPC Middleware.

Common functionality for all IPC handler implementations.
'''

from tornado import web

from netsplice.config import flags as flags_config
from netsplice.config import ipc as config
from netsplice.util.ipc.signature import signature
from netsplice.util.ipc.errors import ModuleNotFoundError
from netsplice.util import get_logger, basestring

logger = get_logger()


# XXX If security is taken seriously mitigations for hospital / bicycle
# attacks or timing attacks should be considered too :).
# https://guidovranken.files.wordpress.com/2015/12/https-bicycle-attack.pdf
class middleware(web.RequestHandler, signature):
    '''
    Middleware Object.

    Inherits Requset Handler to overload 'prepare' and signature to check the
    hmac_secret for all requests.
    '''

    def __init__(self, request, response):
        '''
        Initialize Module.

        Prepares members for each request.
        '''
        web.RequestHandler.__init__(self, request, response)
        signature.__init__(self)
        self.gui = None
        self.model = None
        self.network = None
        self.owner = None
        self.privileged = None

    def prepare(self):
        '''
        Prepare Request for handlers.

        Middleware to check each Request for signature, when signature is
        invalid, 401 is returned and no further processing of the is done. The
        middleware could be attacked (DOS) using very large posts.
        Exposes the application members to the handler functions.
        '''
        if flags_config.DEBUG:
            logger.debug(
                'Middleware Request: %s %s'
                % (self.request.method, self.request.uri))
        if config.CHECK_SIGNATURE:
            signature = self.request.headers.get('X-Signature', None)

            if signature is None:
                raise web.HTTPError(401)

            endpoint = self.request.uri[1:]
            utf8_body = self.request.body.decode('utf-8')

            if not self.is_authentic(signature, endpoint + utf8_body):
                self.set_error_code(
                    1000,
                    'signature is invalid')
                raise web.HTTPError(401)

        # Expose application members to controller
        self.gui = self.application.gui
        self.model = self.application.model
        self.network = self.application.network
        self.owner = self.application.owner
        self.privileged = self.application.privileged

    # Getters for controller

    def get_gui(self):
        '''
        Get GUI.

        Returns the gui dispatcher if available or raises a AttributeError.
        '''
        if self.gui is None:
            raise AttributeError('gui is not set in application')
        return self.gui

    def get_model(self):
        '''
        Get Model.

        Returns the model if available or raises a AttributeError.
        '''
        if self.model is None:
            raise AttributeError('Model is not set in application')
        return self.model

    def get_network(self):
        '''
        Get Network.

        Returns the network dispatcher if available or raises a AttributeError.
        '''
        if self.network is None:
            raise AttributeError('Network is not set in application')
        return self.network

    def get_owner(self):
        '''
        Get Owner.

        Returns the owner if available or raises a AttributeError.
        '''
        if self.owner is None:
            raise AttributeError('Owner is not set in application')
        return self.owner

    def get_privileged(self):
        '''
        Get Privileged.

        Returns the privileged dispatcher if available or raises a
        AttributeError.
        '''
        if self.privileged is None:
            raise AttributeError('Privileged is not set in application')
        return self.privileged

    # Error Handling headers
    def set_error_code(self, code, detail=None):
        '''
        Set Error Code.

        Sets a additional X-Error header to the response when a
        error has occurred.
        When the error occurs in the backend (usually gui to backend api but
        also the submodules access the backend) a gui notification is emitted
        so the gui may open the logview.
        '''
        self.set_header('X-Error', code)
        message = str(code)
        if isinstance(detail, (basestring,)):
            message += ', ' + detail
        elif isinstance(detail, (Exception,)):
            # XXX review controllers to always have good detail message
            #     add tr-text message
            import traceback
            tb_message = ''
            if isinstance(detail, (Exception,)):
                tb_message = traceback.format_exc(detail)
            message += ', ' + tb_message
        logger.error(message)
        if detail is not None:
            self.set_header(
                'X-Errordetail', str(detail). replace('\n', '\\n'))
        try:
            gui_module = self.application.get_module('gui')
            if gui_module:
                gui_module.model.events.notify_error(code)
        except ModuleNotFoundError:
            pass
