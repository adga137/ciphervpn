# -*- coding: utf-8 -*-
# server.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

from tornado import web, log
from netsplice.config import flags
from netsplice.util.ipc import status
from netsplice.util.ipc.route import get_route
from netsplice.util.ipc.application import application


class server_application(web.Application, application):
    '''
    IPC Application is a private class for server that is used to store
    additional properties that can be evaluated by the controller instances.
    Those properties are the dispatcher that the controller can access. The
    Application exposes some default endpoints that are common to all IPC REST
    services.
    '''

    def __init__(self):
        web.Application.__init__(self, [])
        application.__init__(self)
        if flags.VERBOSE > 0:
            log.enable_pretty_logging()

        # properties shared by the application
        self.status = status.STARTING

    def set_endpoints(self, endpoints):
        for route in get_route('netsplice.util.ipc'):
            endpoints.append(route)
        web.Application.__init__(self, endpoints)
