# -*- coding: utf-8 -*-
# service.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Service.

REST Service Base to connect endpoints with controller classes. Wraps tornado
verbs to generalize request implementations in dispatchers. Responsible for
setting up https and hmac.
'''

import urllib
import ssl
from socket import error as socket_error
from tornado import httpclient, ioloop, gen, locks

from netsplice.config import ipc as config
from netsplice.config import flags
from netsplice.util.ipc.certificates import certificates
from netsplice.util.ipc.signature import signature
from netsplice.util import get_logger, basestring
from netsplice.util.ipc.errors import (
    ModuleNotFoundError, ServerConnectionError
)

logger = get_logger()

# In the service an error occurred that should be forwarded to the user.
# The application should continue to work, but the current operation will be
# interrupted.
# this name is duplicate to the backend.event so no import dependency exists.
ERROR = 'error'

# Event Type
NOTIFY = 'notify'


class service(signature):
    '''
    Service Base.

    REST Service Base to connect endpoints with controller classes. Wraps
    tornado verbs to generalize request implementations in dispatchers.
    Responsible for setting up https and hmac.
    '''

    def __init__(
            self, host, port,
            host_owner, port_owner, server_role, client_role):
        '''
        Initialize.

        Initialize Members and Base.
        '''
        signature.__init__(self)
        self.host = host
        self.port = port
        self.host_owner = host_owner
        self.port_owner = port_owner
        self.server_role = server_role
        self.client_role = client_role
        self.certificates = certificates()

        if config.HTTPS:
            self.base_url = 'https://'
        else:
            logger.warn('Using HTTP Client for testing only')
            self.base_url = 'http://'
        self.base_url += self.host + ':' + str(self.port)
        self.service_client = None
        self.application = None
        self._shutting_down = False
        self._active = False
        self._available = locks.Condition()
        self.wait_for_service = False

    def exec_(self):
        '''
        Exec.

        Start the tornado ioloop that allows to yield corouties.
        '''
        ioloop.IOLoop.current().start()

    @gen.coroutine
    def _send_with_body(self, method, endpoint, body):
        '''
        Send with Body.

        HMAC Sign the endpoint with the body and create a X-Signature header.
        Prepare and dispatch the request.
        Catch all exceptions to log them.
        '''
        if self._shutting_down:
            raise ServerConnectionError(
                'Shutdown (%s/%s) in progress, no more requests are processed.'
                ' Please restart the application.'
                % (self.server_role, self.client_role))
        if not self._active and self.wait_for_service:
            yield self._available.wait()
        if body is not None:
            signature = self.sign(endpoint + body)
        else:
            signature = self.sign(endpoint)
        headers = {
            'X-Signature': signature
        }

        request = httpclient.HTTPRequest(
            url=self._get_url(endpoint),
            method=method,
            body=body,
            headers=headers,
            request_timeout=config.REQUEST_TIMEOUT,
            client_key=self.certificates.get_client_key_path(
                self.client_role, self.server_role),
            client_cert=self.certificates.get_client_certificate_path(
                self.client_role, self.server_role),
            ca_certs=self.certificates.get_ca_path(self.server_role))
        try:
            response = yield self._dispatch(request)
            self._active = True
            raise gen.Return(response)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            # already handled in dispatch
            raise errors
        except Exception as errors:
            if flags.DEBUG:
                # This kind of errors should be mapped to error_codes
                # in the service dispatcher, otherwise the application
                # cannot filter or handle them.
                logger.debug(
                    '%s(%s): %s' %
                    (str(errors), type(errors), self._get_url(endpoint)))
            raise errors

    @gen.coroutine
    def post(self, endpoint, body):
        '''
        Post.

        Send the given body to the given endpoint using the POST verb.
        '''
        response = yield self._send_with_body('POST', endpoint, body)
        raise gen.Return(response)

    @gen.coroutine
    def put(self, endpoint, body):
        '''
        Put.

        Send the given body to the given endpoint using the PUT verb.
        '''
        response = yield self._send_with_body('PUT', endpoint, body)
        raise gen.Return(response)

    @gen.coroutine
    def delete(self, endpoint):
        '''
        Delete.

        Send the DELETE verb to given endpoint without any data.
        '''
        response = yield self._send_with_body('DELETE', endpoint, None)
        raise gen.Return(response)

    @gen.coroutine
    def _dispatch(self, request):
        '''
        Dispatch Request.

        Execute the async request. Handle Exceptions:

        * socket: during startup and shutdown, errors can be ignored, otherwise
          they are fatal. handles ssl certificate expired.
        * IOError: when certificates are broken or expired.

        Catch all exceptions to log them.
        '''
        if self._shutting_down:
            raise ServerConnectionError(
                'Shutdown (%s/%s) in progress, no more requests are processed.'
                ' Please restart the application.'
                % (self.server_role, self.client_role))
        service_client = httpclient.AsyncHTTPClient()
        try:
            response = yield service_client.fetch(request)
            if response is not None:
                raise gen.Return(response)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            if self._active:
                self._active = False
                logger.error(
                    'Service (%s) shutdown because of a socket error.'
                    ' Please restart the application. %s. %s.'
                    % (self.server_role, request.url, str(errors)))
                yield self.service_shutdown()
            elif isinstance(errors, (ssl.SSLError,)):
                logger.error(
                    'Service (%s) detected that the certificate is'
                    ' invalid instead of retrying again and again, the'
                    ' application is shutdown: %s'
                    % (self.server_role, str(errors),))
                yield self.service_shutdown()
            raise errors
        except IOError as errors:
            logger.error(
                '%s. certificate mismatch, remote with wrong cert.'
                % (str(errors),))
            yield self.service_shutdown()
            raise errors

    @gen.coroutine
    def get(self, endpoint, parameters):
        '''
        Get.

        HMAC Sign the endpoint with the query parameters and create a
        X-Signature header.
        Prepare and dispatch the request. Handle Exceptions:

        * socket: during startup and shutdown, errors can be ignored, otherwise
          they are fatal. handles ssl certificate expired.
        * IOError: when certificates are broken or expired.

        Catch all exceptions to log them in debug mode.
        '''
        if self._shutting_down:
            raise ServerConnectionError(
                'Shutdown (%s/%s) in progress, no more requests are processed.'
                ' Please restart the application.'
                % (self.server_role, self.client_role))
        headers = {
            'X-Signature': self.sign(endpoint + self._get_query(parameters))
        }
        if not self._active and self.wait_for_service:
            yield self._available.wait()

        request = httpclient.HTTPRequest(
            url=self._get_url(endpoint, parameters),
            method='GET',
            headers=headers,
            request_timeout=config.REQUEST_TIMEOUT,
            client_key=self.certificates.get_client_key_path(
                self.client_role, self.server_role),
            client_cert=self.certificates.get_client_certificate_path(
                self.client_role, self.server_role),
            ca_certs=self.certificates.get_ca_path(self.server_role))
        try:
            response = yield self._dispatch(request)
            self._active = True
            raise gen.Return(response)
        except gen.Return as return_value:
            raise return_value
        except Exception as errors:
            if self._active and not self._shutting_down:
                if flags.DEBUG:
                    # This kind of errors should be mapped to error_codes
                    # in the service dispatcher, otherwise the application
                    # cannot filter or handle them.
                    logger.debug(
                        '%s: %s'
                        % (str(errors), self._get_url(endpoint, parameters)))
            raise errors

    def _get_query(self, parameters):
        '''
        Get Query Parameters.

        Encode the given dict to values suitable to put them in a URL.
        '''
        query = ''
        if parameters is not None:
            try:
                query = urllib.urlencode(parameters)
            except AttributeError:  # Python3
                query = urllib.parse.urlencode(parameters)
        return query

    def _get_url(self, endpoint, parameters=None):
        '''
        Get URL.

        Return a URL for the given endpoint and parameters as string.
        '''
        url = self.base_url + '/' + endpoint
        query = self._get_query(parameters)

        if query != '':
            url += '?' + query
        return url

    def get_available(self):
        '''
        Get Available.

        Get the active status of the service.
        '''
        return self._active

    def is_active(self):
        '''
        Is Active.
        '''
        return self._active

    def is_shutting_down(self):
        '''
        Is shutting down.
        '''
        return self._shutting_down

    def set_error_code(self, code, detail):
        '''
        Set Error Code.

        Create a event so the backend can act on the given error code. Log
        the details.
        '''
        message = (
            'Netsplice Error: %s (%s->%s)'
            % (str(code), self.client_role, self.server_role))
        if isinstance(detail, (basestring,)):
            message += ', ' + detail
        elif isinstance(detail, (Exception,)):
            # XXX review controllers to always have good detail message
            #     add tr-text message
            import traceback
            tb_message = ''
            if isinstance(detail, (Exception,)):
                tb_message = traceback.format_exc(detail)
            message += ', ' + tb_message
        logger.error(message)
        try:
            gui_module = self.application.get_module('gui')
            if gui_module:
                gui_module.model.events.notify_error(code)
            return
        except ModuleNotFoundError:
            pass

    def set_available(self):
        '''
        Set Available.

        Notify all waiting requests that they now can start.
        '''
        self._active = True
        self._available.notify()

    @gen.coroutine
    def service_shutdown(self):
        '''
        Service Shutdown.

        The service decided to shutdown. Change the protected flag to
        allow code to avoid future requests.
        '''
        self._shutting_down = True
