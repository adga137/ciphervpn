# -*- coding: utf-8 -*-
# signature.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
HMAC signature implementation allows sign and verify of a payload with a
shared secret.
'''

import hmac
import hashlib

from netsplice.util.ipc.shared_secret import shared_secret


class signature(object):
    def __init__(self):
        pass

    def _constant_time_compare(self, val1, val2):
        '''
        Return True if the two strings are equal, False otherwise. The time
        taken is independent of the number of characters that match.
        '''
        if len(val1) != len(val2):
            return False
        result = 0
        for comparison_a, comparison_b in zip(val1, val2):
            result |= ord(comparison_a) ^ ord(comparison_b)
        return result == 0

    def is_authentic(self, signature, payload):
        '''
        Checks that the given signature is valid for the given payload
        this assumes that the signer used the same secret
        authentic but may be replayed.
        '''
        return self._constant_time_compare(signature,
                                           self.sign(payload))

    def sign(self, payload):
        '''
        Create a signature for the given payload. The signature is a 40 byte
        string and can be checked with is_authentic
        '''
        payload_bytes = payload
        key_bytes = shared_secret.get()
        try:
            payload_bytes = payload_bytes.encode('utf-8')
            # python3 <> 2
        except AttributeError:
            pass
        try:
            key_bytes = bytes(key_bytes, 'ascii')
            # python3 <> 2
        except TypeError:
            pass
        return hmac.new(key_bytes,
                        payload_bytes,
                        hashlib.sha1).hexdigest()
