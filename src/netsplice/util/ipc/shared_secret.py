# -*- coding: utf-8 -*-
# shared_secret.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Singleton to handle loading and using a single shared secret. Shared amog
different processes (backend, privileged, network etc).
'''

import os
import random

from netsplice.config import ipc as config
from netsplice.util.errors import AccessError
from netsplice.util.ipc.errors import (
    InvalidSharedSecretError,
    NoSharedSecretError
)
from netsplice.util.path import (
    get_location,
    mkdir_p
)
from netsplice.util.files import (
    set_urw_only,
    safe_write
)


class shared_secret_singleton(object):
    def __init__(self):
        secret_length = config.SHARED_SECRET_LENGTH
        self.secret_value = bytearray(
            random.getrandbits(8) for i in range(secret_length))

    def check_access(self, read_only=True):
        '''
        Check access.

        Check that we can access the locations.
        '''
        location = get_location(config.SHARED_SECRET_LOCATION)
        if not os.access(location, os.R_OK):
            raise AccessError(
                'Shared Secret location %s cannot be read.'
                % (location,))
        if read_only:
            return
        if not os.access(location, os.W_OK):
            raise AccessError(
                'Shared Secret location %s cannot be written.'
                % (location,))

    def get(self):
        # reason for gui->backend 401
        return self.secret_value

    def get_filename(self):
        return os.path.join(
            get_location(
                config.SHARED_SECRET_LOCATION), config.SHARED_SECRET_NAME)

    def setup(self, shared_secret=None):
        '''
        Write the current (random) secret_value to she file that will be
        loaded by other processes.
        '''
        if shared_secret is None:
            shared_secret = self.secret_value
        if len(shared_secret) != config.SHARED_SECRET_LENGTH:
            raise InvalidSharedSecretError()

        shared_secret_filename = self.get_filename()
        mkdir_p(get_location(config.SHARED_SECRET_LOCATION))
        safe_write(shared_secret_filename, shared_secret)
        set_urw_only(shared_secret_filename)
        self.secret_value = shared_secret

    def load_from_file(self):
        '''
        Load shared secret from file.
        '''
        shared_secret = ''
        shared_secret_filename = self.get_filename()
        if not os.path.isfile(shared_secret_filename):
            raise NoSharedSecretError()
        with open(shared_secret_filename, 'rb') as file_handle:
            shared_secret = file_handle.read(config.SHARED_SECRET_LENGTH)
        if len(shared_secret) != config.SHARED_SECRET_LENGTH:
            raise InvalidSharedSecretError()
        self.secret_value = shared_secret

shared_secret = shared_secret_singleton()
