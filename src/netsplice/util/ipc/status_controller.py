# -*- coding: utf-8 -*-
# configuration_status_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

import json

from netsplice.util.ipc.middleware import middleware


class status_controller(middleware):
    '''
    Expose the status of the module. It describes the IPC-status
    possible stati are ok, error, shutting down, starting
    the stati are defined in util.ipc.status.
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def get(self):
        '''
        Get Status of application.
        '''
        self.write(json.dumps({
            "status": self.application.status}))
        self.set_status(200)
        self.finish()
