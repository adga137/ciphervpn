# -*- coding: utf-8 -*-
# configuration_version_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''
import json
from netsplice.util.ipc.middleware import middleware
from netsplice import __version__ as VERSION


class configuration_version_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def get(self):
        '''
        Get version for service.
        '''
        version = {
            "version": VERSION
        }
        self.write(json.dumps(version, indent=4, separators=(',', ': ')))
        self.set_status(200)
        self.finish()
