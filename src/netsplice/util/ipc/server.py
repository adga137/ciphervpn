# -*- coding: utf-8 -*-
# server.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

import ssl
import sys

from socket import error as socket_error
from tornado import ioloop, httpserver

from netsplice.config import ipc as config
from netsplice.config import flags
from netsplice.util import get_logger, take_logs
from netsplice.util.ipc import status
from netsplice.util.ipc.certificates import certificates
from netsplice.util.ipc.errors import ServerStartFailedError
from netsplice.util.ipc.server_application import server_application

logger = get_logger()


class server(server_application):
    '''
    IPC Server is the class that Applications instantiate to expose a REST
    interface of their functionality. The Application may expose various
    dispatcher that can be used by the controller handling a request. The
    server requires a role to load the correct certificates.
    '''
    def __init__(self, host, port, role):
        server_application.__init__(self)
        self.host = host
        self.port = port
        self.role = role
        self.context = None

    def get_ssl_context(self):
        certs = certificates()
        if self.context is not None:
            return self.context
        certificate_path = certs.get_server_certificate_path(
            self.role)
        key_path = certs.get_server_key_path(
            self.role)
        ca_path = certs.get_ca_path(
            self.role)
        context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
        context.verify_mode = ssl.CERT_REQUIRED
        try:
            context.load_cert_chain(certificate_path, key_path)
            context.load_verify_locations(ca_path)
            # XXX TODO: register shutdown server when server certificate
            # expires otherwise the server cannot be safely terminated:
            # eg priv runs as root and does no longer accept 'shutdown'
            # because all client certificates are expired.
        except IOError:
            raise ServerStartFailedError(
                'Certificate failed to load: %s' % (key_path,))
        self.context = context
        return context

    def start(self, endpoints):
        '''
        Setup the server with the given endpoints. The endpoints are a list of
        URLSpec objects or (regexp, request_class) tuples. Only dispatcher that
        have been set before the call to start() will be exposed to the
        controller.
        '''
        for module in self.modules:
            for endpoint in module.endpoints:
                endpoints.append(endpoint)
        self.set_endpoints(endpoints)
        if config.HTTPS:
            server = httpserver.HTTPServer(
                self,
                ssl_options=self.get_ssl_context())
        else:
            print('Starting HTTP server for testing and debugging only.')
            print('Host: %s, Port: %s' % (self.port, self.host))
            server = httpserver.HTTPServer(
                self)
        try:
            server.listen(self.port, self.host)

            self.status = status.OK
        except socket_error as errors:
            logger.critical(
                'socket error: %s. %s:%s'
                % (str(errors), self.host, self.port))
            raise ServerStartFailedError(str(errors))
        except Exception as errors:
            raise ServerStartFailedError(str(errors))

    def exec_(self):
        '''
        Exec.

        Start the event_loop and initiate the tornado ioloop that allows async
        processing.
        '''
        if self.event_loop is not None:
            ioloop.IOLoop.current().add_callback(self.event_loop.start)
        ioloop.IOLoop.current().start()

    def flush_logs(self):
        '''
        Flush logs.

        Write out any logs that are in the logger-buffer to stderr. This
        might only be useful for debugging issues during startup
        '''
        for log_item in take_logs():
            message = ''
            level = 'INFO'
            if isinstance(log_item, (dict,)):
                message = log_item['message']
                level = log_item['level']
            else:
                message = log_item.message.get()
                level = log_item.level.get()
            try:
                if level in ['ERROR', 'CRITICAL']:
                    sys.stderr.write(message + '\n')
                else:
                    sys.stdout.write(message + '\n')
            except Exception as errors:
                pass
            sys.stdout.flush()

    def wait_for_subprocesses(
            self, service_list,
            progress_callback, progress_start, progress_steps,
            complete_callback, wait_loops=0):
        '''
        Wait for Subprocesses.

        Execute complete_callback, when all services in the list signaled
        ready. Runs in a non-blocking loop until all services in the list are
        active.
        '''

        num_active = 0
        inactive = []
        for service in service_list:
            if not service.is_active():
                inactive.append(service.server_role)
                continue
            num_active += 1
        if len(service_list) == num_active:
            complete_callback()
        else:
            if flags.DEBUG:
                self.flush_logs()
            max_wait_loops = config.STARTUP_MAX_WAIT_LOOPS
            if sys.platform.startswith('win32'):
                # Windows is extremely slow in starting the subprocesses
                # give it more time.
                max_wait_loops *= 2
            percent = progress_start
            loop_percent = max_wait_loops / progress_steps
            percent += wait_loops // loop_percent
            message = ('Wait %d for %s'
                % (max_wait_loops - wait_loops,
                    ', '.join(inactive),))
            if config.STARTUP_WAIT_ROTATING_DASH:
                dash = ['|', '/', '-', '\\']
                message = ('Wait %s for %s'
                    % (dash[wait_loops % (len(dash))],
                        ', '.join(inactive),))
            progress_callback(percent, message)
            if wait_loops >= max_wait_loops:
                # rather display this message than have a unclosable
                # application.
                logger.critical(
                    'Not all (%s) subprocesses were started in time.'
                    % (', '.join(inactive)))
                complete_callback()
                return
            ioloop.IOLoop.current().call_later(
                config.STARTUP_WAIT_LOOP_TIME,
                lambda: self.wait_for_subprocesses(
                    service_list,
                    progress_callback, progress_start, progress_steps,
                    complete_callback, wait_loops + 1))
