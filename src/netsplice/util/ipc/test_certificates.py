# -*- coding: utf-8 -*-
# service.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

import mock
import os
from netsplice.util.ipc.certificates import certificates
from netsplice.config import ipc as config

# avoid username to be checked in tests
os.environ['HOME'] = '.'

config.CERTIFICATE_KEY_LENGTH = 2048
config.CERTIFICATE_VALID_DAYS = 1


def test_openssl_can_be_executed():
    '''
    Check that openssl can be executed.
    This is used without mock to ensure that
    regardless of the platform the command is
    executed
    '''
    inst = certificates()
    return_value = str(inst.openssl(['version']))
    print(return_value)
    assert('OpenSSL' in return_value)


@mock.patch('netsplice.util.ipc.certificates.set_urw_only')
@mock.patch('netsplice.util.ipc.certificates.process_dispatcher')
def test_create_ca_constructs_dispatcher(mock_dispatcher, mock_set_urw_only):
    print(mock_dispatcher, mock_set_urw_only)
    inst = certificates()
    inst.create_ca('TESTCANAME')
    mock_dispatcher.assert_called_with(
        'openssl', [
            'req',
            '-sha256',
            '-x509',
            '-new',
            '-nodes',
            '-subj',
            '/CN=localhostcaTESTCANAME',
            '-key',
            './.config/Netsplice/certificates/TESTCANAME.ca.key',
            '-days',
            '1',
            '-out',
            './.config/Netsplice/certificates/TESTCANAME.ca.crt',
        ],
        None)


@mock.patch('netsplice.util.ipc.certificates.set_urw_only')
@mock.patch('netsplice.util.ipc.certificates.process_dispatcher')
def test_create_key_constructs_dispatcher(mock_dispatcher, mock_set_urw_only):
    inst = certificates()
    inst.create_key('TESTKEYNAME')
    mock_dispatcher.assert_called_with(
        'openssl', [
            'genrsa',
            '-out',
            './.config/Netsplice/certificates/TESTKEYNAME.key',
            '2048'
        ],
        None)


def test__check_openssl_verify_output_with_error_returns_false():
    inst = certificates()
    result = inst._check_openssl_verify_output('output with error')
    assert(result is False)


def test__check_openssl_verify_output_with_selfsigned_returns_false():
    inst = certificates()
    result = inst._check_openssl_verify_output(
        'verify error:num=18:self signed certificate')
    assert(result is False)


def test__check_openssl_verify_output_with_error_returns_true():
    inst = certificates()
    result = inst._check_openssl_verify_output('output without the word')
    assert(result is True)
