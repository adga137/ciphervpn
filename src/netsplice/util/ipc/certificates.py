# -*- coding: utf-8 -*-
# certificates.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


'''
# Make sure SHA256 FP style certs are generated -> openssl.cnf
'''

import os
import sys

from netsplice.config import ipc as config
from netsplice.config import flags
from netsplice.util import get_logger
from netsplice.util.path import get_location
from netsplice.util.path import mkdir_p
from netsplice.util.files import set_urw_only
from netsplice.util.process.dispatcher import dispatcher as process_dispatcher
from netsplice.util.process.errors import ProcessError
from netsplice.util.errors import AccessError

logger = get_logger()


class certificates(object):
    def __init__(self):
        self.options = dict(
            location=get_location('certificates'),
            key_length=config.CERTIFICATE_KEY_LENGTH,
            valid_days=config.CERTIFICATE_VALID_DAYS)
        self.progress_handler = None
        self.progress_start = 0
        self.progress_end = 0

    def check_access(self, read_only=False):
        '''
        Check Access.

        Check that we can access the files.
        '''
        location = self.options['location']
        if not os.path.exists(location):
            # first start / cleaned preferences
            return
        if not os.access(location, os.R_OK):
            raise AccessError(
                'Location %s cannot be read.'
                % (location,))
        if read_only:
            return
        if not os.access(location, os.W_OK):
            raise AccessError(
                'Location %s cannot be written.'
                % (location,))

    def openssl(self, arguments):
        output = None
        if sys.platform.startswith('win32') and \
                ('req' in arguments or 'openrsa' in arguments):
            arguments.append('-config')
            arguments.append('openssl/openssl.cnf')

        process = process_dispatcher('openssl', arguments, None)
        process.set_prefix('openssl')
        try:
            output = process.start_subprocess_sync()
        except ProcessError as errors:
            logger.error('Subprocess errors: %s' % (errors,))
            output = str(errors)

        return output

    def create_ca(self, ca_name):
        '''
        CN must be unique and different from csr-CN
        '''
        options = self.options
        options['ca_name'] = ca_name
        self.create_key('%(ca_name)s.ca' % options)
        self.openssl([
            'req',
            '-sha256',
            '-x509',
            '-new',
            '-nodes',
            '-subj', '/CN=localhostca%(ca_name)s' % options,
            '-key', '%(location)s/%(ca_name)s.ca.key' % options,
            '-days', str(options['valid_days']),
            '-out', '%(location)s/%(ca_name)s.ca.crt' % options])
        ca_crt_filename = os.path.join(
            options['location'], options['ca_name'] + '.ca.crt')
        set_urw_only(ca_crt_filename)

    def create_key(self, name):
        options = self.options
        options['name'] = name
        self.openssl([
            'genrsa',
            '-out', '%(location)s/%(name)s.key' % options,
            str(options['key_length'])])
        key_filename = os.path.join(
            options['location'], options['name'] + '.key')
        set_urw_only(key_filename)

    def create_csr(self, name):
        options = self.options
        options['name'] = name
        self.openssl([
            'req',
            '-sha256',
            '-new',
            '-subj', '/CN=localhost%(name)s' % options,
            '-key', '%(location)s/%(name)s.key' % options,
            '-out', '%(location)s/%(name)s.csr' % options])
        csr_filename = os.path.join(
            options['location'], options['name'] + '.csr')
        set_urw_only(csr_filename)

    def sign_csr(self, name, ca_name):
        options = self.options
        options['name'] = name
        options['ca_name'] = ca_name
        self.openssl([
            'x509',
            '-req',
            '-sha256',
            '-in', '%(location)s/%(name)s.csr' % options,
            '-CA', '%(location)s/%(ca_name)s.ca.crt' % options,
            '-CAkey', '%(location)s/%(ca_name)s.ca.key' % options,
            '-CAserial', '%(location)s/%(ca_name)s.ca.srl' % options,
            '-CAcreateserial',
            '-out', '%(location)s/%(name)s.crt' % options,
            '-days', str(options['valid_days'])])
        crt_filename = os.path.join(
            options['location'], options['name'] + '.crt')
        set_urw_only(crt_filename)

    def create_server(self, name):
        self.create_key(name)
        self.create_csr(name)
        self.create_ca(name)
        self.sign_csr(name, name)

    def create_client(self, server_name, client_name):
        options = dict(
            server=server_name,
            client=client_name)
        name = '%(server)s-%(client)s-client' % options
        self.create_key(name)
        self.create_csr(name)
        self.sign_csr(name, server_name)

    def valid(self):
        is_valid = True
        try:
            self._progress(0, 'Verify server certificates')
            is_valid &= self.verify_server('backend')
            is_valid &= self.verify_server('network')
            is_valid &= self.verify_server('privileged')
            is_valid &= self.verify_server('unprivileged')
            self._progress(30, 'Verify backend to service certificates')
            is_valid &= self.verify_client('backend', 'gui')
            is_valid &= self.verify_client('backend', 'privileged')
            is_valid &= self.verify_client('backend', 'network')
            self._progress(80, 'Verify service to backend certificates')
            is_valid &= self.verify_client('privileged', 'backend')
            is_valid &= self.verify_client('unprivileged', 'backend')
            is_valid &= self.verify_client('network', 'backend')
        except:
            is_valid = False
        return is_valid

    def verify_client_files(self, server_name, client_name):
        options = self.options
        options['server_name'] = server_name
        options['client_name'] = client_name
        ca_crt_filename = (
            '%(location)s/%(server_name)s.ca.crt'
            % options)
        client_certificate_path = (
            '%(location)s/%(server_name)s-%(client_name)s-client.crt'
            % options)
        if not os.path.exists(ca_crt_filename):
            return False
        if not os.path.exists(client_certificate_path):
            return False
        return True

    def verify_client(self, server_name, client_name):
        if not self.verify_client_files(server_name, client_name):
            return False
        options = self.options
        options['server_name'] = server_name
        options['client_name'] = client_name
        output = self.openssl([
            'verify',
            '-CAfile',
            '%(location)s/%(server_name)s.ca.crt' % options,
            '%(location)s/%(server_name)s-%(client_name)s-client.crt'
            % options])
        return self._check_openssl_verify_output(output)

    def _check_openssl_verify_output(self, output):
        if 'error' in output:
            return False
        return True

    def set_progress_handler(self, start, end, handler):
        '''
        Set Progress Handler.

        Set the handler as progress handler function and display the progress
        in between the given start and end values.
        '''
        self.progress_start = start
        self.progress_end = end
        self.progress_handler = handler

    def _progress(self, percent, message):
        '''
        Output Progress.

        Output a progress of the certificate setup.
        '''
        if self.progress_handler is None:
            return

        handler_range = self.progress_end - self.progress_start

        handler_percent = percent * handler_range / 100 + self.progress_start
        self.progress_handler(handler_percent, message)

    def verify_server_files(self, server_name):
        options = self.options
        options['server_name'] = server_name
        ca_crt_filename = (
            '%(location)s/%(server_name)s.ca.crt'
            % options)
        crt_filename = (
            '%(location)s/%(server_name)s.crt'
            % options)
        if not os.path.exists(ca_crt_filename):
            return False
        if not os.path.exists(crt_filename):
            return False
        return True

    def verify_server(self, server_name):
        if not self.verify_server_files(server_name):
            return False
        options = self.options
        options['server_name'] = server_name
        output = self.openssl([
            'verify',
            '-CAfile',
            '%(location)s/%(server_name)s.ca.crt' % options,
            '%(location)s/%(server_name)s.crt' % options])
        return self._check_openssl_verify_output(output)

    def setup(self):
        if self.valid():
            self._progress(100, 'Certificates are valid')
            return
        mkdir_p(self.options['location'])
        self._progress(0, 'Create server certificates')
        self.create_server('backend')
        self.create_server('network')
        self.create_server('privileged')
        self.create_server('unprivileged')
        self._progress(30, 'Create backend to service certificates')
        self.create_client('backend', 'privileged')
        self.create_client('backend', 'unprivileged')
        self.create_client('backend', 'network')
        self._progress(80, 'Create service to backend certificates')
        self.create_client('backend', 'gui')
        self.create_client('unprivileged', 'backend')
        self.create_client('privileged', 'backend')
        self.create_client('network', 'backend')

        # drop the ca-keys to prevent signing new clients
        os.remove("%(location)s/backend.ca.key" % self.options)
        os.remove("%(location)s/unprivileged.ca.key" % self.options)
        os.remove("%(location)s/privileged.ca.key" % self.options)
        os.remove("%(location)s/network.ca.key" % self.options)
        self._progress(100, 'Certificates created')

    def get_client_key_path(self, client_role, server_role):
        options = dict(
            server_role=server_role,
            client_role=client_role)
        client_key_path = os.path.join(
            self.options['location'],
            '%(server_role)s-%(client_role)s-client.key' % options)
        if flags.VERBOSE > 5:
            logger.debug(
                'get_client_key_path: %s %s %s'
                % (client_role, server_role, client_key_path))
        return client_key_path

    def get_client_certificate_path(self, client_role, server_role):
        options = dict(
            server_role=server_role,
            client_role=client_role)
        client_certificate_path = os.path.join(
            self.options['location'],
            '%(server_role)s-%(client_role)s-client.crt' % options)
        if flags.VERBOSE > 5:
            logger.debug(
                'get_client_certificate_path: %s %s %s'
                % (client_role, server_role, client_certificate_path))
        return client_certificate_path

    def get_server_key_path(self, server_role):
        options = dict(
            server_role=server_role)
        server_key_path = os.path.join(
            self.options['location'],
            '%(server_role)s.key' % options)
        if flags.VERBOSE > 5:
            logger.debug(
                'get_server_key_path: %s %s'
                % (server_role, server_key_path))
        return server_key_path

    def get_server_certificate_path(self, server_role):
        options = dict(
            server_role=server_role)
        server_certificate_path = os.path.join(
            self.options['location'],
            '%(server_role)s.crt' % options)
        if flags.VERBOSE > 5:
            logger.debug(
                'get_server_certificate_path: %s %s'
                % (server_role, server_certificate_path))
        return server_certificate_path

    def get_ca_path(self, server_role):
        options = dict(
            server_role=server_role)
        ca_path = os.path.join(
            self.options['location'],
            '%(server_role)s.ca.crt' % options)
        if flags.VERBOSE > 5:
            logger.debug(
                'get_ca_path: %s %s' % (server_role, ca_path))
        return ca_path
