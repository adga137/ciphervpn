# -*- coding: utf-8 -*-
# service.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

from .route import get_route_class


def test_get_route_class_returns_simple_controller_class():
    r = get_route_class('n.s', '/some/ep', 'cn')
    e = 'n.s.cn_controller.cn_controller'
    assert(r == e)


def test_get_route_class_returns_module_controller_class():
    r = get_route_class('n.s', '/module/mod/ep', 'cn')
    e = 'n.s.mod.cn_controller.cn_controller'
    assert(r == e)
