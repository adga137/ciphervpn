# -*- coding: utf-8 -*-
# server.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

import threading

from netsplice.config import flags
from netsplice.util import get_logger_handler
from netsplice.util import get_logger
from netsplice.util.ipc.errors import (
    ModuleNotFoundError, ModuleAlreadyRegisterdError
)

logger = get_logger()


class application(object):
    def __init__(self):
        self.dispatcher = None
        self.gui = None
        self.model = None
        self.network = None
        self.owner = None
        self.privileged = None
        self.unprivileged = None
        self.modules = []
        self.plugins = []
        self.plugin_dispatchers = []
        self.plugin_event_handler = []
        self.event_loop = None
        self.shutdown = False
        self._lock = threading.Lock()

    def add_dispatcher_plugin(self, core_dispatcher, name, dispatcher_class):
        '''
        Add Dispatcher Plugin.

        Add the given dispatcher class to the with the given name.
        '''
        self.lock()
        self.plugin_dispatchers.append({
            'core': core_dispatcher,
            'name': name,
            'class': dispatcher_class
        })
        self.unlock()

    def add_event_plugin(self, name, eventhandler_class):
        '''
        Add Event Plugin.

        Add the given event handler class to the with the given name.
        '''
        self.lock()
        self.plugin_event_handler.append({
            'name': name,
            'class': eventhandler_class
        })
        self.unlock()

    def add_module(self, module):
        '''
        Add Module.

        Add Module to the application and register the module in the
        event_loop. Raises a ModuleAlreadyRegisterdError when the module is
        already in the modules-list.
        '''
        for existing_module in self.modules:
            if existing_module.name == module.name:
                raise ModuleAlreadyRegisterdError(module.name)
        self.lock()
        self.modules.append(module)
        module.application = self
        if self.event_loop is not None:
            self.event_loop.register_module(module)
        self.unlock()

    def add_plugin(self, plugin):
        '''
        Add Plugin.

        Add Plugin to the list of known plugins.
        '''
        self.lock()
        self.plugins.append(plugin)
        self.unlock()

    def get_module(self, name):
        for module in self.modules:
            if module.name == name:
                return module
        raise ModuleNotFoundError(name)

    def get_dispatcher(self):
        return self.dispatcher

    def get_event_loop(self):
        return self.event_loop

    def get_gui(self):
        return self.gui

    def get_model(self):
        return self.model

    def get_network(self):
        return self.network

    def get_owner(self):
        return self.owner

    def get_plugin_dispatchers(self):
        '''
        Get Plugin Dispatchers.

        Thread safe access to the plugin dispatchers.
        '''
        self.lock()
        dispatchers = []
        dispatchers.extend(self.plugin_dispatchers)
        self.unlock()
        return dispatchers

    def get_privileged(self):
        return self.privileged

    def get_unprivileged(self):
        return self.unprivileged

    def lock(self):
        '''
        Lock.

        Lock application members.
        '''
        self._lock.acquire()

    def remove_module(self, module_name):
        self.lock()
        rm_module = None
        for module in self.modules:
            if module.name == module_name:
                rm_module = module
                break
        if rm_module is not None:
            self.modules.remove(rm_module)
        self.unlock()

    def set_dispatcher(self, dispatcher):
        self.lock()
        self.dispatcher = dispatcher
        self.dispatcher.application = self
        self.unlock()

    def set_event_loop(self, event_loop):
        self.lock()
        self.event_loop = event_loop
        self.event_loop.application = self
        self.unlock()

    def set_gui(self, gui):
        self.lock()
        self.gui = gui
        self.gui.application = self
        self.unlock()

    def set_model(self, model):
        self.lock()
        self.model = model
        self.model.application = self
        logger_handler = get_logger_handler()
        logger_handler.set_model(self.model.log)
        self.unlock()

    def set_network(self, network):
        self.lock()
        self.network = network
        self.network.application = self
        self.unlock()

    def set_owner(self, owner):
        self.lock()
        self.owner = owner
        self.owner.application = self
        self.unlock()

    def set_privileged(self, privileged):
        self.lock()
        self.privileged = privileged
        self.privileged.application = self
        self.unlock()

    def set_unprivileged(self, unprivileged):
        self.lock()
        self.unprivileged = unprivileged
        self.unprivileged.application = self
        self.unlock()

    def register_plugin_components(self, component):
        '''
        Register Plugin Components.

        Run ``plugin.register(self, component)`` for each known plugin.
        Always run in the requested dependency order. The dependency is
        determined by a very simple method. When dependency is not
        resolvable, a error is logged.
        '''
        registered_plugins = []
        self.lock()
        available_plugins = list(self.plugins)
        self.unlock()
        while len(available_plugins):
            registered = False
            for plugin in available_plugins:
                skip = False
                for dependency in plugin.dependencies:
                    if dependency not in registered_plugins:
                        skip = True
                        break
                if skip:
                    continue

                try:
                    plugin.register(self, component)
                except KeyError:
                    if flags.DEBUG:
                        logger.warn(
                            'Plugin %s has no component %s.'
                            % (str(plugin), component))
                registered_plugins.append(plugin)
                available_plugins.remove(plugin)
                if flags.DEBUG:
                    logger.info(
                        'registered plugin %s component %s'
                        % (str(plugin), component))
                registered = True
                break
            if not registered:
                logger.error(
                    'Component %s for plugins %s could not be registered due'
                    ' to dependency problems.'
                    % (component, str(available_plugins),))
                break

        # activate plugin components
        for event_handler in self.plugin_event_handler:
            self.event_loop.register_plugin_module(
                event_handler['class'](self))

        signal_dispatcher = False
        for dispatcher in self.plugin_dispatchers:
            if dispatcher['core'].server_role == 'backend' \
                    and dispatcher['core'].client_role == 'gui':
                signal_dispatcher = True
                continue
            else:
                dispatcher['core'].__dict__[dispatcher['name']] = \
                    dispatcher['class'](dispatcher['core'])

        if signal_dispatcher:
            self.get_dispatcher().plugins_changed.emit()

    def unlock(self):
        '''
        Unlock.

        Unlock application members.
        '''
        self._lock.release()
