# -*- coding: utf-8 -*-
# service.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

from netsplice.util.ipc.signature import signature
from netsplice.util.ipc.shared_secret import shared_secret


def test__constant_time_compare_with_difference_returns_false():
    '''
    '''
    inst = signature()
    return_value = inst._constant_time_compare('A', 'B')
    assert(return_value is False)


def test__constant_time_compare_with_same_returns_true():
    '''
    '''
    inst = signature()
    return_value = inst._constant_time_compare('A', 'A')
    assert(return_value is True)


def test_sign_with_payload():
    inst = signature()
    shared_secret.secret_value = 'TEST53CRET'
    return_value = inst.sign('ABC')
    assert(return_value == 'e1918c3bdcbf619517a0e5312ea2e67582356d53')


def test_is_authentic_with_correct_signature_returns_true():
    inst = signature()
    shared_secret.secret_value = 'TEST53CRET'
    return_value = inst.is_authentic(
        'e1918c3bdcbf619517a0e5312ea2e67582356d53',
        'ABC')
    assert(return_value is True)


def test_is_authentic_with_bad_signature_returns_false():
    inst = signature()
    shared_secret.secret_value = 'TEST53CRET'
    return_value = inst.is_authentic(
        'bad5190a400bad5190a400ebad5190a400ebad51',
        'ABC')
    assert(return_value is False)
