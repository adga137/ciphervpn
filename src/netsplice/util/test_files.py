import mock
import os
import sys
from StringIO import StringIO
from files import (
    check_and_fix_urw_only,
    safe_write
)
from .path import (
    get_location,
    mkdir_p
)
from ..config import flags


def get_test_filename():
    test_path = get_location('tests')
    mkdir_p(test_path)
    filename = '%(test_path)s/test.file' % dict(test_path=test_path)
    with open(filename, 'w') as file_handle:
        file_handle.write('test_contents')
    print filename
    return filename


@mock.patch('netsplice.util.files.logger')
@mock.patch('netsplice.util.files.stat.S_IMODE')
def test_check_and_fix_urw_only_S_IMODE_called(mock_simode, mock_logger):
    '''
    Check that stat is used to get the mode.
    '''
    check_and_fix_urw_only(get_test_filename())
    assert(mock_simode.called)


@mock.patch('netsplice.util.files.logger')
@mock.patch('netsplice.util.files.set_urw_only')
@mock.patch('netsplice.util.files.stat.S_IMODE')
def test_check_and_fix_urw_only_os_chmod_for_rw_rw_rw_called(
        mock_simode, mock_set_urw_only, mock_logger):
    '''
    Check that stat is used to get the mode.
    '''
    mock_simode.return_value = int('0666', 8)
    check_and_fix_urw_only(get_test_filename())
    assert(mock_set_urw_only.called)


@mock.patch('netsplice.util.files.logger')
@mock.patch('netsplice.util.files.set_urw_only')
@mock.patch('netsplice.util.files.stat.S_IMODE')
def test_check_and_fix_urw_only_os_chmod_for_rw_r_r_called(
        mock_simode, mock_set_urw_only, mock_logger):
    '''
    Check that stat is used to get the mode.
    '''
    mock_simode.return_value = int('0644', 8)
    check_and_fix_urw_only(get_test_filename())
    assert(mock_set_urw_only.called)


@mock.patch('netsplice.util.files.logger')
@mock.patch('netsplice.util.files.set_urw_only')
@mock.patch('netsplice.util.files.stat.S_IMODE')
def test_check_and_fix_urw_only_os_chmod_for_urw_not_called(
        mock_simode, mock_set_urw_only, mock_logger):
    '''
    Check that stat is used to get the mode.
    '''
    mock_simode.return_value = int('0600', 8)
    check_and_fix_urw_only(get_test_filename())
    assert(mock_set_urw_only.called is False)


@mock.patch('netsplice.util.files.logger')
def test_safe_write_creates_file(mock_logger):
    '''
    Check that the function creates a file with the given content.
    '''
    content = 'some content'
    result_content = ''
    filename = get_test_filename()
    safe_write(filename, content)
    with open(filename) as file_handle:
        result_content = file_handle.read(32)
    assert(result_content == content)


@mock.patch('netsplice.util.files.logger')
def test_safe_write_with_empty_content_creates_file(mock_logger):
    '''
    Check that the function creates a file with the given content.
    '''
    content = ''
    result_content = ''
    filename = get_test_filename()
    safe_write(filename, content)
    with open(filename) as file_handle:
        result_content = file_handle.read(32)
    assert(result_content == content)


@mock.patch('netsplice.util.files.logger')
def test_safe_write_renames_file_before_writing(mock_logger):
    '''
    Check that the original is still readable (as it is opened before) with
    the original content.
    '''
    content = 'some content'
    new_content = 'new content'
    result_content = ''
    filename = get_test_filename()
    safe_write(filename, content)
    with open(filename) as file_handle:
        # now the handle should be on the existing file.
        # safe_write it again it will return the old value
        safe_write(filename, new_content)
        result_content = file_handle.read(32)
        print result_content
        assert(result_content != new_content)
    # now that the handle is closed, the new content should be available
    with open(filename) as file_handle:
        # now the handle should be on the newly created file.
        result_content = file_handle.read(32)
        print result_content
        assert(result_content == new_content)


@mock.patch('netsplice.util.files.logger')
@mock.patch('netsplice.util.files.os.rename')
def test_safe_write_rename_called(mock_rename, mock_logger):
    '''
    Check that os.rename is called
    '''
    filename = get_test_filename()
    content = 'some content'
    safe_write(filename, content)
    assert(mock_rename.called)


@mock.patch('netsplice.util.files.logger')
@mock.patch('netsplice.util.files.hashlib.sha256')
def test_safe_write_hashlib_sha256_called(mock_sha256, mock_logger):
    '''
    Check that hashlib.sha256 is called
    '''
    filename = get_test_filename()
    content = 'some content'
    safe_write(filename, content)
    assert(mock_sha256.called)


def teardown():
    '''
    Cleanup files created by the safe_write tests.
    '''
    try:
        os.remove(get_test_filename())

    except:
        pass
