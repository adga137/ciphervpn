# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Logger access functions.
'''
import datetime
import os
import sys

from netsplice import __version__ as VERSION
from netsplice.util.logger import logger
from netsplice.util.timer import timer
from netsplice.config import constants
from netsplice.config import log as config_log


def get_logger(name=None):
    '''
    Get the Logger, create the instance if required. Returns the logbook
    instance that provides a documented and familiar interface.
    '''
    if not logger.is_setup():
        if name is None:
            name = os.path.basename(sys.executable).replace(
                constants.NS_NAME, '').replace('App', '')
            if not getattr(sys, 'frozen', False):
                name = os.path.basename(sys.argv[0]).replace(
                    constants.NS_NAME, '').replace('App.py', '')
        logger.setup(name)
    return logger.get_logger()


def get_logger_handler():
    '''
    Get the logger model handler to access the logs that logbook has written
    or set the model to act on new log entries.
    '''
    if not logger.is_setup():
        logger.setup()
    return logger.model_handler


def get_timer(name=None):
    '''
    Get Timer.

    Return the timer singleton for simple performance measurements.

    Keyword arguments:
        name -- name of the timer (default: {None})
    '''
    if not timer.is_setup():
        if name is None:
            name = os.path.basename(sys.executable).replace(
                constants.NS_NAME, '').replace('App', '')
            if not getattr(sys, 'frozen', False):
                name = os.path.basename(sys.argv[0]).replace(
                    constants.NS_NAME, '').replace('App.py', '')
        timer.setup(name, get_logger())
    return timer.get_timer()


def take_logs():
    '''
    Return the current available log_entries and purge the current buffer.
    '''
    if not logger.is_setup():
        return []
    return logger.take_logs()


def log_report(log_text):
    '''
    Log Report.

    Compile a report suitable for support requests.

    Arguments:
        log_text -- lines of log text
    '''
    report = config_log.TEXT_FORMAT_REPORT.format(
        date=datetime.datetime.now(),
        log_text=log_text,
        version=VERSION,
        os=os.name
    )
    return report


def escape_html(unescaped_text):
    '''
    Escape HTML.

    Replace &<> with &amp;, &gt; and &lt;

    Arguments:
        unescaped_text -- text that may contain html
    '''
    if unescaped_text is None:
        return unescaped_text
    escaped_text = unescaped_text.replace('&', '&amp;')
    escaped_text = escaped_text.replace('<', '&lt;')
    escaped_text = escaped_text.replace('>', '&gt;')
    return escaped_text


try:
    basestring = basestring
except NameError:
    basestring = str


try:
    long = long
except NameError:
    long = int
