# -*- coding: utf-8 -*-
# stream_to_log.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Stream to Log.

Asynchronous thread that reads a stream until it is closed and puts its
lines to the log member of the given model (as log instance).
Used for subprocess stdout/stderr.
'''

import threading
import time
from netsplice.util import get_logger

logger = get_logger()

FILTER_ALL = 'stream_to_log_filter_all'


class stream_to_log(threading.Thread):
    '''
    Helper class to implement asynchronous reading of a file
    in a separate thread. Pushes read lines on a queue to
    be consumed in another thread.
    '''

    def __init__(
            self,
            fd,
            fd_name,
            lock=None,
            logger_extra=None,
            filter=None,
            process=None):
        assert callable(fd.close)
        assert callable(fd.readline)
        threading.Thread.__init__(self)
        self._fd = fd
        self._fd_name = fd_name
        self._logger_extra = logger_extra
        self._stopped = False
        self._process = process
        if filter is not None:
            self._filter = filter
        else:
            self._filter = []

        if lock is not None:
            self._lock = lock
        else:
            self._lock = threading.RLock()

    def run(self):
        '''
        Read lines and put them on the model.
        '''
        try:
            for line in iter(self._fd.readline, ''):
                if self._stopped:
                    break
                if not isinstance(line, (bytes,)):
                    continue
                str_line = line.decode('utf-8')
                if str_line.strip() == '':
                    # The stream may only be stuck. Avoid a cpu-eating
                    # loop in python3 where readline will always return a
                    # empty string when the handle is closed.
                    time.sleep(0.1)
                    continue
                filtered = False
                if self._filter == FILTER_ALL:
                    continue
                for filter in self._filter:
                    if filter in str_line:
                        filtered = True
                        break
                if filtered:
                    continue
                self.lock()
                extra = {}
                if self._logger_extra is not None:
                    extra = self._logger_extra
                extra['origin'] = 'log-stream-%s' % (self._fd_name,)
                if '\r' in str_line:
                    str_line = str_line.replace('\r', '')
                if self._fd_name == 'stderr':
                    logger.error(str_line, extra=extra)
                else:
                    logger.info(str_line, extra=extra)
                self.unlock()
        except ValueError:
            # self._fd_readline closed
            self.lock()
            self._stopped = True
            self.unlock()
            self._fd.close()

    def stop(self):
        '''
        Stop.

        Set the flag that causes run to return and close the filehandle.
        '''
        self.lock()
        self._stopped = True
        self.unlock()
        if self._fd:
            self.join()
            self._fd.close()

    def lock(self):
        '''
        Lock the Model
        '''
        self._lock.acquire()

    def unlock(self):
        '''
        Unlock the model.
        '''
        self._lock.release()
