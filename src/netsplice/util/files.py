# -*- coding: utf-8 -*-
# files.py
# Copyright (C) 2015 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Files.

File helper methods.
'''

from . import get_logger
import os
import sys
import stat
import time
import hashlib
from netsplice.config import flags

logger = get_logger()


def check_and_fix_urw_only(filename):
    '''
    Test for 600 mode and try to set it if anything different found.

    Might raise OSError.

    :param filename: filename path
    :type filename: str
    '''
    mode = stat.S_IMODE(os.stat(filename).st_mode)
    if mode != int('600', 8):
        try:
            logger.warning('Bad permission %s on %s attempting to set 600' %
                           (oct(mode), filename,))
            set_urw_only(filename)
        except OSError:
            logger.error('Error while trying to chmod 600 %s' %
                         filename)
            raise


def set_urw_only(filename):
    '''
    Set File accessible only by creator.

    Change the access modifier for the given filename to 600. Insert a
    artifical sleep on win32 platform to ensure the OS has completed writing
    the file.
    '''
    try:
        if sys.platform.startswith('win32'):
            # for some reason the file cannot be found if it was 'just'
            # created (safe_write, subprocess)
            WIN_CHMOD_SLEEP_FIRST = 0.1
            time.sleep(WIN_CHMOD_SLEEP_FIRST)
        os.chmod(filename, stat.S_IRUSR | stat.S_IWUSR)
        if flags.DEBUG:
            logger.debug('chmod 600 on %s done' % (filename,))
    except OSError:
        logger.error('Error while trying to chmod 600 %s' %
                     filename)
        raise


def get_mtime(filename):
    '''
    Return the modified time or None if the file doesn't exist.

    :param filename: path to check
    :type filename: str

    :rtype: str
    '''
    try:
        mtime = time.ctime(os.path.getmtime(filename)) + ' GMT'
        return mtime
    except OSError:
        return None


def safe_write(filename, file_contents):
    '''
    Safe Write.

    Write to a file using a temporary name first and rename after success.
    Avoids empty files due to disk-full events.
    Avoids corrupted files by checking hash before making the file active.
    '''
    if flags.DEBUG:
        logger.debug('safe write: %s' % (filename,))
    tmp_filename = filename + '.tmp'
    file_bytes = file_contents
    try:
        # python 3 <> 2
        file_bytes = file_contents.encode('utf-8')
    except AttributeError:
        pass

    content_hash = hashlib.sha256(file_bytes).digest()
    with open(tmp_filename, 'wb') as file_handle:
        file_handle.write(file_bytes)
    if sys.platform.startswith('win32'):
        # for some reason the file is returned different when
        # read immediately
        WIN_CHMOD_SLEEP_FIRST = 0.1
        time.sleep(WIN_CHMOD_SLEEP_FIRST)
    new_contents = open(tmp_filename, 'rb').read(len(file_bytes))
    new_content_hash = hashlib.sha256(new_contents).digest()
    if new_content_hash == content_hash:
        if os.path.isfile(filename):
            if flags.DEBUG:
                logger.debug(
                    'removing %s before renaming %s'
                    % (filename, tmp_filename))
            os.remove(filename)
        if flags.DEBUG:
            logger.debug(
                'renaming %s to %s' % (tmp_filename, filename))
        os.rename(tmp_filename, filename)
        if flags.DEBUG:
            logger.debug('file %s written' % (filename,))
    else:
        logger.error(
            'hashes do not match: is: %s vs :should: %s'
            % (new_content_hash, content_hash))
        raise IOError()
