# -*- coding: utf-8 -*-
# marshalable_persistent.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Persistent marshalable.

Extend marshalables to generate readable json and with persistent
functionality.
'''

import copy
import json

from tornado import ioloop, gen, locks

from netsplice.config import flags
from netsplice.util.errors import NotFoundError
from netsplice.util.model.errors import ValidationError
from netsplice.util.model.marshalable import marshalable
from netsplice.util.model.persistent import persistent
from netsplice.util import get_logger

import netsplice.config.model as config

logger = get_logger()


class marshalable_persistent(persistent, marshalable):
    '''
    Base class for all models that are stored to persistent memory.

    The marshal takes care to validate the objects upon receiving and to clean
    the values before sending. It is a kind of Structure Definition for JSON.
    To make use of the marshalable let your model inherit from marshalable and
    define all members using the util.model.field class.
    '''

    def __init__(self, owner, prefix=None):
        marshalable.__init__(self)
        persistent.__init__(self, owner, prefix)

    def to_json(self):
        '''
        Serialize the current model to JSON using json.dumps. Only valid and
        cleaned values are serialized. When a value is invalid its cleaner
        default value will be used instead. Only attributes that are
        util.model.fields will be serialized. The persistent json is formated
        for better readability.
        '''
        return json.dumps(
            self.get_json_dict(),
            indent=4, separators=(',', ': '))
