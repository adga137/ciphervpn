# -*- coding: utf-8 -*-
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

from .field import field
from .validator import validator, ValidationError
from .cleaner import cleaner


class never_validator(validator):
    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        return False


class always_validator(validator):
    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        return True


class vacuum_cleaner(cleaner):
    def __init__(self):
        cleaner.__init__(self, '')

    def clean(self, value):
        return ''


class dirty_cleaner(cleaner):
    def __init__(self):
        cleaner.__init__(self, '')

    def clean(self, value):
        cleaned_value = ''
        for c in value:
            cleaned_value += c + '.'
        return cleaned_value


def test_get_invalid_value_returns_default_value():
    f = field(default='DEFAULT', validators=[never_validator()])
    f.set_value('SomeValue')
    assert(f.get() is f.get_default_value())


def test_get_valid_value_returns_value():
    f = field(default='DEFAULT', validators=[always_validator()])
    f.set_value('SomeValue')
    assert(f.get() is 'SomeValue')
    assert(f.get() is not f.get_default_value())


def test_get_cleaned_returns_cleaned_value():
    f = field(cleaner=[vacuum_cleaner()])
    f.set_value('SomeValue')
    assert(f.get_cleaned() is '')
    f = field(cleaner=[dirty_cleaner()])
    f.set_value('SomeValue')
    assert(f.get_cleaned() == 'S.o.m.e.V.a.l.u.e.')


def test_is_valid_on_valid_fields_returns_true():
    f = field(default='DEFAULT', validators=[always_validator()])
    f.set_value('SomeValue')
    assert(f.is_valid() is True)
    f = field(default='DEFAULT', validators=[
        always_validator(),
        always_validator()])
    f.set_value('SomeValue')
    assert(f.is_valid() is True)


def test_is_valid_on_invalid_fields_returns_false():
    f = field(default='DEFAULT', validators=[never_validator()])
    f.set_value('SomeValue')
    assert(f.is_valid() is False)


def test_is_valid_on_somewhat_invalid_fields_returns_false():
    f = field(default='DEFAULT', validators=[
        always_validator(),
        never_validator()])
    f.set_value('SomeValue')
    assert(f.is_valid() is False)


def test_set_only_sets_valid_cleaned_values():
    f = field(validators=[always_validator()], cleaner=[vacuum_cleaner()])
    f.set('SomeValue')
    assert(f.get_value() is '')


def test_set_invalid_value_raises_exception():
    f = field(validators=[never_validator()])
    try:
        f.set('SomeValue')
        assert(False)  # expected ValidationError
    except ValidationError:
        assert(True)


def test_set_invalid_value_does_not_change_value():
    f = field(validators=[never_validator()])
    f.set_value('SomePresetValue')  # set raw
    try:
        f.set('SomeNewValue')
    except ValidationError:
        pass
    assert(f.get_value() is 'SomePresetValue')


def test_validate_on_valid_fields_returns_true():
    f = field(default='DEFAULT', validators=[always_validator()])
    f.set_value('SomeValue')
    assert(f.validate() is True)
    f = field(default='DEFAULT', validators=[
        always_validator(),
        always_validator()])
    f.set_value('SomeValue')
    assert(f.validate() is True)


def test_validate_on_invalid_fields_raises_exception():
    f = field(default='DEFAULT', validators=[never_validator()])
    f.set_value('SomeValue')
    try:
        f.validate()
        assert(False)  # never_validator should raise
    except ValidationError:
        assert(True)


def test_validate_on_somewhat_invalid_fields_raises_exception():
    f = field(default='DEFAULT', validators=[
        always_validator(),
        never_validator()])
    f.set_value('SomeValue')
    try:
        f.validate()
        assert(False)  # never_validator should raise
    except ValidationError:
        assert(True)
