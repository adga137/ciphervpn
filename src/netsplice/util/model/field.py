# -*- coding: utf-8 -*-
# field.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

from netsplice.util.model.cleaner_none import none as none_cleaner
from netsplice.util.model.validator_none import none as none_validator


class field(object):
    '''
    Field for model attributes. Allows to attach cleaner and validators for a
    model field that will be automatically processed my a marshal.
    '''
    def __init__(
            self,
            required=True,
            default=None,
            validators=[none_validator()],
            cleaner=[none_cleaner()]):
        '''
        Define the field and set if it is required. Fields that are defined
        without any validators or cleaners will have the none_cleaner and
        none_validator attached by default. Validators and Cleaners can be
        attached later.
        '''
        self.required = required
        self.default_value = default
        self.validators = validators
        self.cleaner = cleaner
        self.value = None
        self.serialize_json = True

    def get(self):
        '''
        Return a validated and cleaned value. Will return the cleaned-default
        value when the current field_value is invalid.
        XXX ? performance ?
        '''
        if self.is_valid():
            return self.get_cleaned()

        return self.default_value

    def get_cleaned(self):
        cleaned_value = self.value

        for field_cleaner in self.cleaner:
            cleaned_value = field_cleaner.clean(cleaned_value)

        return cleaned_value

    def get_default_value(self):
        '''
        Return the default value for the field.
        '''
        return self.default_value

    def is_valid(self):
        '''
        Validate the value of the field with the current validators. Returns
        false when the first validator fails.
        '''
        valid = True

        for field_validator in self.validators:
            valid = valid and field_validator.is_valid(self.value)

            if not valid:
                break

        return valid

    def get_value(self):
        '''
        Return the unvalidated and uncleaned value.
        '''
        return self.value

    def set(self, value):
        '''
        Set the given value to the field_value when the value is valid. Raises
        a ValidatorError if the value does not validate.
        '''
        updated = False

        for field_validator in self.validators:
            field_validator.validate(value)  # raises ValidatorError

        cleaned_value = value

        for field_cleaner in self.cleaner:
            cleaned_value = field_cleaner.clean(cleaned_value)

        if value != self.value:
            updated = True
        self.value = cleaned_value

        return updated

    def set_value(self, value):
        '''
        Set a unvalidated and uncleaned value.
        '''
        self.value = value

    def validate(self):
        '''
        Validate the value of the field with the current validators. Raises a
        Validator error when the value is invalid.
        '''
        for field_validator in self.validators:
            field_validator.validate(self.value)  # raises ValidatorError

        return True
