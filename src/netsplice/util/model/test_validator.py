# -*- coding: utf-8 -*-
# test_validator.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Test for the validator object.
'''

import mock
from .validator import validator
from .errors import ValidationError

def get_testobject():
    return validator()

@mock.patch('netsplice.util.model.validator.validator.is_valid')
def test_validate_is_valid_returns_true(mock_is_valid):
    mock_is_valid.return_value = True
    v = get_testobject()
    result = v.validate('test')
    assert(result is True)


@mock.patch('netsplice.util.model.validator.validator.is_valid')
def test_validate_not_is_valid_raises(mock_is_valid):
    mock_is_valid.return_value = False
    v = get_testobject()
    try:
        v.validate('test')
        assert(False)  # ValidationError expected
    except ValidationError:
        assert(True)


def test_is_valid_abstract_raises():
    v = get_testobject()
    try:
        v.is_valid('test')
        assert(False)  # NotImplementedError expected
    except NotImplementedError:
        assert(True)
