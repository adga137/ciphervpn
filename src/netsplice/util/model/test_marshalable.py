# -*- coding: utf-8 -*-
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Tests for marshal wrapper.
'''

import json
import mock

from .field import field
from .validator import validator, ValidationError
from .cleaner import cleaner
from .marshalable import marshalable
from .marshalable_list import marshalable_list
from netsplice.util.errors import NotFoundError, InvalidUsageError

class never_validator(validator):
    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        return False


class always_validator(validator):
    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        return True


class vacuum_cleaner(cleaner):
    def __init__(self):
        cleaner.__init__(self, '')

    def clean(self, value):
        return ''


class dirty_cleaner(cleaner):
    def __init__(self):
        cleaner.__init__(self, '')

    def clean(self, value):
        cleaned_value = ''
        for c in value:
            cleaned_value += c + '.'
        return cleaned_value


class simple_model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.id = field(required=False)
        self.name = field(required=False)
        self.address = field(required=False)


class simple_owner_model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.id = field(required=False)
        self.name = field(required=False)
        self.address = field(required=False)
        self._owner = field(required=False)


class simple_extra_model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.id = field(required=False)
        self.name = field(required=False)
        self.address = field(required=False)
        self.extra = 1


class simple_unserialized_model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.id = field(required=False)
        self.name = field(required=False)
        self.address = field(required=False)
        self.unserialized = field(required=False)
        self.unserialized.serialize_json = False


class impossible_model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.id = field(validators=[never_validator()])
        self.name = field(validators=[never_validator()])
        self.address = field(validators=[never_validator()])


class nested_model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.child = simple_model()


class nested_nested_model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.child = nested_model()


class nested_list_child_model(marshalable_list):
    def __init__(self):
        marshalable_list.__init__(self, simple_model)


class nested_list_model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.child = nested_list_child_model()


class required_model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.id = field(required=True)
        self.name = field(required=True)
        self.address = field(required=True)


class vacuum_model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.id = field(required=True, cleaner=[vacuum_cleaner()])
        self.name = field(required=True, cleaner=[vacuum_cleaner()])
        self.address = field(required=True, cleaner=[vacuum_cleaner()])

class empty_model(marshalable):
    def __init__(self):
        marshalable.__init__(self)


def test_to_json_with_simple_model_returns_string():
    m = simple_model()
    e = '{"id": null, "name": null, "address": null}'
    a = m.to_json()
    print('expected', e)
    print('actual', a)
    assert(json.loads(a) == json.loads(e))


def test_to_json_with_simple_model_with_values_returns_string():
    m = simple_model()
    m.id.set(0)
    m.name.set('netsplice')
    m.address.set('https://netsplice.org')
    e = '{"id": 0, "name": "netsplice", "address": "https://netsplice.org"}'
    a = m.to_json()
    print('expected', e)
    print('actual', a)
    assert(json.loads(a) == json.loads(e))


def test_to_json_with_stripped_simple_model_returns_string():
    m = simple_model()
    # replace field's with basic-types
    m.id = 0
    m.name = 'netsplice'
    m.address = 'https://netsplice.org'
    # do not copy for new tests, this is actually a developer mistake to
    # assign values like this
    a = m.to_json()
    e = '{}'
    print('expected', e)
    print('actual', a)
    assert(json.loads(a) == json.loads(e))


def test_to_json_with_nested_model_returns_string():
    m = nested_model()
    # replace field's with basic-types
    m.child.id.set(1)
    m.child.name.set('netsplice')
    m.child.address.set('https://netsplice.org')
    a = m.to_json()
    e = ('{"child": {"id": 1, "name": "netsplice", '
         '"address": "https://netsplice.org"}}')
    print('expected', e)
    print('actual', a)
    assert(json.loads(a) == json.loads(e))


def test_to_json_with_nested_list_model_returns_string():
    m = nested_list_model()
    # replace field's with basic-types
    m.child.append(m.child.item_model_class())
    m.child.append(m.child.item_model_class())
    m.child[0].id.set(1)
    m.child[0].name.set('netsplice')
    m.child[0].address.set('https://netsplice.org')
    m.child[1].id.set(2)
    m.child[1].name.set('other')
    m.child[1].address.set('https://other.org')
    a = m.to_json()
    e = ('{"child": [{"id": 1, "name": "netsplice", '
         '"address": "https://netsplice.org"},{"id": 2, '
         '"name": "other", "address": "https://other.org"}]}')
    print('expected', e)
    print('actual', a)
    assert(json.loads(a) == json.loads(e))


def test_from_json_with_empty_values_for_simple_model_sets_all_values():
    m = simple_model()

    m.id.set(0)
    m.name.set('netsplice')
    m.address.set('https://netsplice.org')

    m.from_json('{"id": null, "name": null, "address": null}')

    assert(m.id.get() is None)
    assert(m.name.get() is None)
    assert(m.address.get() is None)


def test_from_json_with_values_for_simple_model_sets_values():
    m = simple_model()

    m.id.set(10)
    m.name.set('something')
    m.address.set('https://someurl.org')

    j = '{"id": 0, "name": "netsplice", "address": "https://netsplice.org"}'
    m.from_json(j)

    assert(m.id.get() is 0)
    assert(m.name.get() == 'netsplice')
    assert(m.address.get() == 'https://netsplice.org')


@mock.patch('netsplice.util.model.marshalable.logger')
def test_from_json_with_invalid_values_raises_exception(mock_logger):
    m = impossible_model()

    j = '{"id": 0, "name": "netsplice", "address": "https://netsplice.org"}'
    try:
        m.from_json(j)
        assert(False)  # impossible_model should always raise
    except ValidationError:
        assert(True)


@mock.patch('netsplice.util.model.marshalable.logger')
def test_from_json_without_reqired_values_raises_exception(mock_logger):
    m = required_model()

    j = '{"id": 0, "name": "netsplice"}'
    try:
        m.from_json(j)
        assert(False)  # required_model should raise because of missing address
    except ValidationError:
        assert(True)


def test_from_json_with_nested_value_returns_nested_model():
    m = nested_model()

    j = (
        '{"child": '
        '{"id": 0, "name": "netsplice", "address": "https://netsplice.org"}'
        '}')
    print(j)
    m.from_json(j)
    print('actual', m.to_json())
    assert(m.child.id.get() is 0)
    assert(m.child.name.get() == 'netsplice')
    assert(m.child.address.get() == 'https://netsplice.org')


@mock.patch('netsplice.util.model.marshalable.logger')
def test_from_json_with_missing_nested_value_raises_exception(mock_logger):
    m = nested_model()

    j = ('{}')
    print(j)
    try:
        m.from_json(j)
        assert(False)  # missing nested value should raise exception
    except ValidationError:
        assert(True)


def test_from_json_with_nested_nested_value_returns_nested_model():
    m = nested_nested_model()

    j = (
        '{"child":'
        '{"child": '
        '{"id": 0, "name": "netsplice", "address": "https://netsplice.org"}'
        '}'
        '}')
    print(j)
    m.from_json(j)
    print('actual', m.to_json())
    assert(m.child.child.id.get() is 0)
    assert(m.child.child.name.get() == 'netsplice')
    assert(m.child.child.address.get() == 'https://netsplice.org')


@mock.patch('netsplice.util.model.marshalable.logger')
def test_from_json_with_missing_nested_nested_value_raises_exception(
        mock_logger):
    m = nested_nested_model()

    j = ('{"child":{}}')
    print(j)
    try:
        m.from_json(j)
        print(m.to_json())
        assert(False)  # missing nested value should raise exception
    except ValidationError as e:
        print(e)
        assert(True)


def test_is_valid_jsons_with_valid_json_returns_true_and_constant():
    m = simple_model()

    m.id.set(10)
    m.name.set('something')
    m.address.set('https://someurl.org')

    j = '{"id": 0, "name": "netsplice", "address": "https://netsplice.org"}'
    assert(m.is_valid_jsons(j) is True)

    assert(m.id.get() is 10)
    assert(m.name.get() == "something")
    assert(m.address.get() == 'https://someurl.org')


def test_is_valid_jsons_with_invalid_json_returns_false():
    m = impossible_model()

    j = '{"id": 0, "name": "netsplice", "address": "https://netsplice.org"}'
    assert(m.is_valid_jsons(j) is False)
    m = required_model()

    j = '{"id": 0, "name": "netsplice"}'
    assert(m.is_valid_jsons(j) is False)


def test_clean_with_cleanable_field_values_cleans():
    m = vacuum_model()

    m.id.set_value(10)
    m.name.set_value('something')
    m.address.set_value('https://someurl.org')
    m.clean()
    assert(m.id.get() is '')
    assert(m.name.get() == '')
    assert(m.address.get() == '')


def test__get_weak_model_class_without_find_by_name_returns_base():
    m = simple_model()
    f = marshalable_list(nested_model)  # provokes AttributeError
    d = dict()
    result = m._get_weak_model_class(f, d)
    print(type(result))
    assert(isinstance(result, (nested_model,)))


def test__get_weak_model_class_without_name_returns_base():
    m = simple_model()
    f = marshalable_list(nested_model)
    def fn_find_by_name(name):
        return required_model
    f.find_by_name = fn_find_by_name
    d = dict()  # provokes KeyError
    result = m._get_weak_model_class(f, d)
    print(type(result))
    assert(isinstance(result, (nested_model,)))


def test__get_weak_model_class_without_dict_name_returns_base():
    m = simple_model()
    f = marshalable_list(nested_model)
    def fn_find_by_name(name):
        raise NotFoundError('test')
    f.find_by_name = fn_find_by_name
    d = dict(name='test')
    result = m._get_weak_model_class(f, d)
    print(type(result))
    assert(isinstance(result, (nested_model,)))


def test__get_weak_model_class_returns_weak_base():
    m = simple_model()
    f = marshalable_list(nested_model)
    def fn_find_by_name(name):
        return required_model()
    f.find_by_name = fn_find_by_name
    d = dict(name='test')
    result = m._get_weak_model_class(f, d)
    print(type(result))
    assert(isinstance(result, (required_model,)))


def test_get_unknown_field_raises():
    m = simple_model()
    try:
        m.get('unknown')
        assert(False)  # Expect NotFoundError
    except NotFoundError:
        assert(True)


def test_get_nested_field_raises():
    m = nested_model()
    try:
        m.get('child')
        assert(False)  # Expect NotFoundError
    except InvalidUsageError:
        assert(True)


def test_get_json_dict_without_attributes_returns_empty():
    m = empty_model()
    result = m.get_json_dict()
    print(result)
    assert(len(result.keys()) is 0)


def test_get_json_dict_simple_attributes_returns_simple():
    m = simple_model()
    result = m.get_json_dict()
    print(result)
    assert('address' in result.keys())
    assert('id' in result.keys())
    assert('name' in result.keys())


def test_get_json_dict_simple_attributes_owner_returns_simple():
    m = simple_owner_model()
    result = m.get_json_dict()
    print(result)
    assert('_owner' not in result.keys())


def test_get_json_dict_simple_extra_returns_simple():
    m = simple_extra_model()
    result = m.get_json_dict()
    print(result)
    assert('extra' not in result.keys())


def test_get_json_dict_simple_unserialized_returns_simple():
    m = simple_unserialized_model()
    result = m.get_json_dict()
    print(result)
    assert('unserialized' not in result.keys())

class field_instance_item(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.fitem = field()

class field_instance_list(marshalable_list):
    def __init__(self):
        marshalable_list.__init__(self, field_instance_item)

class field_instance_model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.mlist = field_instance_list()
        self.ilist = field()
