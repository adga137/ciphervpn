# -*- coding: utf-8 -*-
# field.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

import copy
import json

from tornado import ioloop, gen, locks

from netsplice.config import flags
from netsplice.util.errors import (
    NotFoundError, InvalidUsageError
)
from netsplice.util.model.errors import ValidationError
from netsplice.util.model.field import field
from netsplice.util import get_logger

import netsplice.config.model as config

logger = get_logger()


class marshalable(field):
    '''
    Base class for all models that are passed through a (REST) interface and
    need to be serialized to JSON and parsed on the other end. The marshal
    takes care to validate the objects upon receiving and to clean the values
    before sending. It is a kind of Structure Definition for JSON. To make use
    of the marshalable let your model inherit from marshalable and define all
    members using the util.model.field class.
    '''

    def __init__(self):
        field.__init__(self)
        self._model_changed_condition = locks.Condition()
        self._model_changed_index = 0
        self._model_changed_last = -1

    def _get_weak_model_class(self, field_instance, dict_instance):
        '''
        Return Weak Model Class.

        Return the item_model_class of the field_instance and when a named
        field is available, use the definition of the named field.
        '''
        model_class = field_instance.item_model_class()
        try:
            model_class = type(
                field_instance.find_by_name(dict_instance['name']))()
        except AttributeError:
            pass
        except NotFoundError:
            pass
        except KeyError:
            pass
        return model_class

    def get(self, field_name):
        '''
        Get Attribute.

        Return a validated attribute value or raise a not found value.
        In general it is preferred to use model.field.get(), but sometimes
        this function is more convienient in generic code.
        '''
        try:
            return self.__dict__[field_name].get()
        except KeyError:
            raise NotFoundError(
                '"%s" is not a attribute in the model'
                % (field_name,))
        except TypeError:
            raise InvalidUsageError(
                '"%s" is not a simple attribute and needs to be accessed using'
                ' the property.'
                % (field_name,))

    def get_json_dict(self):
        '''
        Finds all "field" attributes and validates and cleans them to put them
        into a dict for json serialization.
        '''
        json_dict = dict()
        for attr in self.__dict__.keys():
            if attr == '_owner':
                continue
            field_instance = getattr(self, attr)
            if not isinstance(field_instance, field):
                continue
            if not field_instance.serialize_json:
                continue
            if isinstance(field_instance, (list)):
                json_list = []
                for field_instance_item in field_instance:
                    if not isinstance(field_instance_item, field):
                        continue
                    if isinstance(field_instance_item, marshalable):
                        json_list.append(field_instance_item.get_json_dict())
                    else:
                        json_list.append(field_instance_item.get())
                if isinstance(field_instance, marshalable):
                    json_dict[attr] = json_list
                elif len(json_list):
                    json_dict[attr] = json_list
                continue
            # get checks if valid and returns default value if not
            # and only returns clean values
            if isinstance(field_instance, list):
                json_list = []
                for field_instance_item in field_instance:
                    json_list.append(field_instance_item.get_json_dict())
                json_dict[attr] = json_list
            elif isinstance(field_instance, marshalable):
                json_dict[attr] = field_instance.get_json_dict()
            else:
                json_dict[attr] = field_instance.get()
        return json_dict

    def to_json(self):
        '''
        Serialize the current model to JSON using json.dumps. Only valid and
        cleaned values are serialized. When a value is invalid its cleaner
        default value will be used instead. Only attributes that are
        util.model.fields will be serialized.
        '''
        if flags.DEBUG:
            return json.dumps(
                self.get_json_dict(),
                indent=4, separators=(',', ': '))
        return json.dumps(self.get_json_dict())

    def from_dict(self, field_dict):
        '''
        Put all value in a clean_dict first to prevent half-filling the fields
        when some value is invalid.
        '''
        clean_json_dict = dict()
        for attr in self.__dict__:
            if attr == '_owner':
                continue
            field_instance = getattr(self, attr)
            if not isinstance(field_instance, field):
                continue
            if attr not in field_dict and field_instance.required:
                # missing required field -> raise
                message = (
                    '%s.%s is required but not a field in the model.'
                    % (str(type(self)), attr))
                if flags.DEBUG:
                    message += '%s %s' % (self.__dict__, field_dict)
                logger.error(message)
                raise ValidationError(message)
            if isinstance(field_instance, list):
                clean_json_list = []
                try:
                    for dict_instance in field_dict[attr]:
                        tmp_field = self._get_weak_model_class(
                            field_instance, dict_instance)
                        tmp_field.from_dict(dict_instance)
                        clean_json_list.append(tmp_field)
                    clean_json_dict[attr] = clean_json_list
                except KeyError:
                    if field_instance.required:
                        message = (
                            '%s.%s is required but not a field in the model:'
                            ' %s vs %s'
                            % (str(type(self)), attr,
                                self.__dict__, field_dict))
                        logger.error(message)
                        raise ValidationError(message)
            else:
                if isinstance(field_instance, marshalable):
                    if attr not in field_dict:
                        if not field_instance.required:
                            continue
                        message = (
                            '%s is missing in JSON for %s.'
                            % (attr, str(type(self))))
                        logger.error(message)
                        raise ValidationError(message)
                    tmp_field = type(field_instance)()
                    tmp_field.from_dict(field_dict[attr])
                    clean_json_dict[attr] = tmp_field.get_json_dict()
                else:
                    tmp_field = copy.deepcopy(field_instance)
                    tmp_field.set_value(tmp_field.get_default_value())
                    if attr in field_dict:
                        # Missing optional field: use default value Set,
                        # validate and clean the value from the field_dict
                        # Raises ValidationError when not valid
                        try:
                            tmp_field.set(field_dict[attr])
                        except ValidationError as errors:
                            message = (
                                'Attr %s.%s in JSON has a invalid value'
                                '(%s): %s.'
                                % (str(type(self)), attr,
                                    field_dict[attr], errors))
                            logger.error(message)
                            raise ValidationError(message)
                    # The value is valid or default, so no need to use get and
                    # revalidate.
                    clean_json_dict[attr] = tmp_field.get_value()

        # All values are cleaned, all required values are available, now set
        # the values of all fields from the cleaned dict.
        for attr in clean_json_dict:
            field_instance = getattr(self, attr)
            if isinstance(field_instance, list):
                del field_instance[:]
                for item in clean_json_dict[attr]:
                    field_instance.append(item)
            else:
                if isinstance(field_instance, marshalable):
                    field_instance.from_dict(clean_json_dict[attr])
                else:
                    field_instance.set_value(clean_json_dict[attr])

    def from_json(self, jsons):
        '''
        Fill the attributes of type util.model.field with the values of the
        given json string. The values will only be filled if all required
        attributes are provided and they have valid values. Will raise
        ValidationError otherwise.
        '''
        self.from_dict(json.loads(jsons))

    def is_valid_jsons(self, jsons):
        '''
        Check if the given JSON string can be considered valid.
        '''
        json_dict = json.loads(jsons)
        valid = True
        for attr in self.__dict__:
            field_instance = getattr(self, attr)
            if not isinstance(field_instance, field):
                continue
            if attr not in json_dict and field_instance.required:
                valid = False
                break
            if attr not in json_dict:
                continue
            tmp_field = copy.deepcopy(field_instance)
            tmp_field.set_value(json_dict[attr])
            valid = valid and tmp_field.is_valid()
            if not valid:
                break
        return valid

    def clean(self):
        '''
        Execute the cleaners for all fields in the model. Will ensure that all
        fields only contain cleaned data.
        '''
        for attr in self.__dict__:
            field_instance = getattr(self, attr)
            if not isinstance(field_instance, field):
                continue
            clean_value = field_instance.get_cleaned()
            field_instance.set_value(clean_value)

    def commit(self):
        '''
        Trigger that the model has changed.
        '''
        self._model_changed_condition.notify_all()
        self._model_changed_index += 1

    def migrate(self, jsons):
        '''
        Migrate.

        Populate the instance with the default value or the value from the
        JSON.
        '''
        logger.info('Migation for %s' % (str(type(self)),))
        json_dict = json.loads(jsons)
        valid = True
        for attr in self.__dict__:
            field_instance = getattr(self, attr)
            if attr == '_owner':
                continue
            if not isinstance(field_instance, (field,)):
                continue
            if isinstance(field_instance, (list,)):
                continue
            default_value = field_instance.get_default_value()
            if attr not in json_dict:
                field_instance.set(default_value)
                logger.warn(
                    'Migration: using default value (%s) for %s'
                    ' as no value for it exists yet.'
                    % (default_value, attr,))
                continue
            tmp_field = copy.deepcopy(field_instance)
            tmp_field.set_value(json_dict[attr])
            if not tmp_field.is_valid():
                field_instance.set_value(default_value)
                logger.warn(
                    'Migration: use default value (%s) for %s'
                    ' as the current stored value (%s) is invalid.'
                    % (default_value, attr, json_dict[attr]))
            else:
                field_instance.set_value(json_dict[attr])

        # the result should be serializable, otherwise raise a ValidationError
        self.to_json()
        logger.info('Migration for %s succeeded.' % (str(type(self)),))

    @gen.coroutine
    def model_changed(self):
        '''
        Model Changed.

        Coroutine that waits until the model has changed or the heartbeat
        timeout is reached.

        Return False when the timeout was reached, True when the model was
        commited.
        '''
        state = True
        if self._model_changed_last == self._model_changed_index:
            heartbeat_timeout = ioloop.IOLoop.current().time()
            heartbeat_timeout += config.HEARTBEAT_SECONDS
            state = yield self._model_changed_condition.wait(
                timeout=heartbeat_timeout)
            if self._model_changed_last != self._model_changed_index:
                state = True
        self._model_changed_last = self._model_changed_index
        raise gen.Return(state)

    def set_named(self, field_name, field_value):
        '''
        Set Named Attribute.

        Set the given field_value to the current model.
        Raises ValidationError when the field_value is not valid.
        Raises NotFoundError when the given field_name does not exist.
        Raises InvalidUsageError when field_name is a complex property
        In general it is preferred to use model.field.set(value), but sometimes
        this function is more convenient in generic code.
        '''
        try:
            self.__dict__[field_name].set(field_value)
        except KeyError:
            raise NotFoundError(
                '"%s" is not a attribute in the model.'
                % (field_name,))
        except TypeError:
            raise InvalidUsageError(
                '"%s" is not a simple attribute and needs to be accessed using'
                ' the property.'
                % (field_name,))
