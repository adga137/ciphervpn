# -*- coding: utf-8 -*-
# field.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

import json

from netsplice.config import flags
from netsplice.util.model.errors import ValidationError
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable


class marshalable_list(list, marshalable):
    '''
    Base class for all models that are passed through a (REST) interface and
    need to be serialized to JSON and parsed on the other end.  The marshal
    takes care to validate the objects upon receiving and to clean the
    values before sending.  It is a kind of Structure Definition for JSON.
    To make use of the marshalable_list let your model inherits from
    marshalable_list and defines what type is to be expected in the
    list-items by providing a util.model.field class.
    '''

    def __init__(self, item_model_class):
        marshalable.__init__(self)
        field.__init__(self)
        list.__init__(self)
        self.item_model_class = item_model_class

    def get_json_list(self):
        '''
        Generates all items into a json array '[]'
        '''
        json_list = list()
        for item in self:
            json_list.append(item.get_json_dict())
        return json_list

    def to_json(self):
        '''
        Serialize the current model to JSON using json.dumps. All items will
        get marshaled. Fields in model will be ignored.
        '''
        if flags.DEBUG:
            return json.dumps(
                self.get_json_list(),
                indent=4, separators=(',', ': '))
        return json.dumps(self.get_json_list())

    def from_json(self, jsons):
        '''
        Use the instances of the json array to fill the marshalable_items.
        '''
        json_list = json.loads(jsons)
        if not isinstance(json_list, (list)):
            raise ValidationError('JSON is not of type array')
        self.from_list(json_list)

    def from_list(self, json_list):
        '''
        From List.

        Process all items in the given ist and replace the current list with
        its elements after validation.
        '''

        # Put all value is a clean_list first to prevent half-filling the
        # instances when some value is invalid.
        clean_model_list = list()
        for json_item in json_list:
            item_model = self.item_model_class()
            item_model.from_dict(json_item)
            clean_model_list.append(item_model)

        # All values are cleaned, all required values are available now set
        # the items to the list.
        del self[:]
        for item_model in clean_model_list:
            self.append(item_model)

    def is_valid_jsons(self, jsons):
        '''
        Check if the given JSON string can be considered valid.
        '''
        valid = True
        json_list = json.loads(jsons)
        if not isinstance(json_list, (list)):
            return False
        # Put all value is a clean_list first to prevent half-filling the
        # instances when some value is invalid.
        for json_item in json_list:
            item_model = self.item_model_class()
            jsons_item = json.dumps(json_item)
            valid = valid and item_model.is_valid_jsons(jsons_item)
            if not valid:
                break
        return valid

    def clean(self):
        '''
        Execute the cleaners for all items in the model. Will ensure that all
        fields in all items only contain cleaned data.
        '''
        for marshalable_item in self:
            marshalable_item.clean()
