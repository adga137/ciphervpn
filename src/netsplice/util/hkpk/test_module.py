# -*- coding: utf-8 -*-
# https_hkpk.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
HKPK HTTPS test
'''

import os
from .download import download

SIGN_KEY_PEM = '''-----BEGIN PUBLIC KEY-----
MIIBI.......
-----END PUBLIC KEY-----
'''

PINSET = [
    '85c0ff1b51ab123e5a66a55d0226a211f213cababdc6ca39d78fad8be1455dfa'
]

signature = (
    'LZVp2NLVuMGa4Py5+5se4cH4O+IcFtNkyfP+NK00r2yYX5mDmc96WBcGJL8iKMHewTbgue6'
    'OPR/l49KC0JMmW1xs2uxTVumcU0jAEoBWNLtujiQnqCJUySoJUFINwjMvy+AwaJfbUvlEiq'
    '+RDmmrKFFS7hBI7n23fYSFReM2AsbXcx6qoBMHUndvxlX4Db/x4zyF3wemcfzlyTKOCad9w'
    'bBj4OO4zzJxlkFcE2eC+otk9GD+hFPRrmMH2upY6fNIWRZcrD3FUXrFuo2SrCphqA84atMv'
    '1XgXOGNMsKRbv544vOwLMQ3ZpuCSX2KtG2DJU5KvqmMnFUJXNP1Fg1c75Q=='
)


def _test_integration():
    target_file = '/tmp/module_test_file'
    url = 'https://remote/url'
    d = download(
        url=url,
        target=target_file,
        signature=signature,
        sign_key=SIGN_KEY_PEM,
        pinset=PINSET)
    d.remove_targets()
    d.start()
    assert(os.path.exists(target_file))

if __name__ == '__main__':
    _test_integration()

# usage:
# cd [base]/src/netsplice/util $ python -m hkpk.test_module
