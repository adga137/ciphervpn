# -*- coding: utf-8 -*-
# test_download_ioloop.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Async Download Tests.
'''
from tornado.testing import gen_test, AsyncTestCase
from tornado import gen
from .download_ioloop import download_ioloop
from .download_ioloop import CHUNK_NOTIFY_COUNT
from .errors import (
    SignatureError, NetworkError, InvalidError
)
from urllib3.exceptions import (
    MaxRetryError, ReadTimeoutError
)
import mock

SIGNATURE = (
    'LZVp2NLVuMGa4Py5+5se4cH4O+IcFtNkyfP+NK00r2yYX5mDmc96WBcGJL8iKMHewTbgue6'
    'OPR/l49KC0JMmW1xs2uxTVumcU0jAEoBWNLtujiQnqCJUySoJUFINwjMvy+AwaJfbUvlEiq'
    '+RDmmrKFFS7hBI7n23fYSFReM2AsbXcx6qoBMHUndvxlX4Db/x4zyF3wemcfzlyTKOCad9w'
    'bBj4OO4zzJxlkFcE2eC+otk9GD+hFPRrmMH2upY6fNIWRZcrD3FUXrFuo2SrCphqA84atMv'
    '1XgXOGNMsKRbv544vOwLMQ3ZpuCSX2KtG2DJU5KvqmMnFUJXNP1Fg1c75Q=='
)


class mock_filehandle(object):
    def __init__(self):
        self.data = ''
        self._close_called = False

    def close(self):
        self._close_called = True

    def read(self):
        return self.data

    def seek(self, offset):
        pass

    def write(self, new_data):
        self.data += new_data


class mock_request(object):
    def __init__(self):
        self.data = []
        self.header = {
            'Content-Length': 0
        }
        self._supports_chunked_reads = True
        self._supports_chunked_reads_sideeffect = None
        self.status = 200

    def getheader(self, header_name):
        return self.header[header_name]

    def stream(self, decode_content=True):
        return self.data

    def supports_chunked_reads(self):
        if self._supports_chunked_reads_sideeffect:
            raise self._supports_chunked_reads_sideeffect
        return self._supports_chunked_reads

    def read(self, l):
        return self.data

    def __exit__(self, a, b, c):
        pass

    def __enter__(self):
        return self


class mock_connection_pool(object):
    def __init__(self):
        self._request = mock_request()

    def request(self, method, url, preload_content=False):
        return self._request


def get_test_object():
    o = download_ioloop(
        'https://localhost/file', '/tmp/_test_download_file', [])
    o._fh_target = mock_filehandle()
    o._fh_target_incomplete = mock_filehandle()
    o._fh_target_invalid = mock_filehandle()
    o._fh_target_unchecked = mock_filehandle()
    o.signature = SIGNATURE
    o.active = True
    return o


class AsyncDownloadTests(AsyncTestCase):
    '''
    Test Async Download

    Tests for the async download implementation

    Extends:
        AsyncTestCase
    '''
    @gen.coroutine
    def mock_notify(self, complete, bps):
        self.notify_called = True
        self.notify_args.append((complete, bps))

    @gen.coroutine
    def mock_chunked_download(self, request, target_file):
        self.chunked_download_called = True

    @gen.coroutine
    def mock_download(self):
        self.download_called = True

    @gen.coroutine
    def mock_download_se(self):
        raise self.mock_download_side_effect

    @gen.coroutine
    def mock_download_side_effect(self, side_effect):
        self.mock_download_side_effect = side_effect
        return self.mock_download_se

    @gen_test
    def test_chunked_download_notifies_100(self):
        o = get_test_object()
        request = mock_request()
        request.data = ['1', '2', '3']
        request.header['Content-Length'] = 3
        filehandle = mock_filehandle()
        self.notify_called = False
        self.notify_args = []
        o._notify = self.mock_notify
        yield o._chunked_download(request, filehandle)
        assert(self.notify_called is True)
        assert(self.notify_args[len(self.notify_args) - 1][0] == 100)

    @gen_test
    def test_chunked_download_notifies_progress(self):
        o = get_test_object()
        request = mock_request()
        l = 0
        for i in range(0, 1000):
            request.data.append(str(i))
            l += len(str(i))
        request.header['Content-Length'] = l
        filehandle = mock_filehandle()
        self.notify_called = False
        self.notify_args = list()
        o._notify = self.mock_notify
        yield o._chunked_download(request, filehandle)
        assert(self.notify_called is True)
        assert(len(self.notify_args) == 1000 / CHUNK_NOTIFY_COUNT + 1)

    @gen_test
    def test_download_404_raises(self):
        o = get_test_object()
        o.pool = mock_connection_pool()
        o.pool._request.status = 404
        try:
            yield o._download()
            assert(False)  # Network error expected
        except NetworkError:
            assert(True)

    @gen_test
    def test_download_500_raises(self):
        o = get_test_object()
        o.pool = mock_connection_pool()
        o.pool._request.status = 500
        try:
            yield o._download()
            assert(False)  # Network error expected
        except NetworkError:
            assert(True)

    @gen_test
    def test_download_maxretry_raises(self):
        o = get_test_object()
        o.pool = mock_connection_pool()
        o.pool._request.status = 200
        try:
            o.pool._request._supports_chunked_reads_sideeffect = \
                MaxRetryError('', '', '')
            yield o._download()
            assert(False)  # Network error expected
        except NetworkError:
            assert(True)

    @gen_test
    def test_download_readtimeout_raises(self):
        o = get_test_object()
        o.pool = mock_connection_pool()
        o.pool._request.status = 200
        try:
            o.pool._request._supports_chunked_reads_sideeffect = \
                ReadTimeoutError('', '', '')
            yield o._download()
            assert(False)  # Network error expected
        except NetworkError:
            assert(True)

    @gen_test
    def test_download_chunk_supported_downloads_chunked(
            self,):
        o = get_test_object()
        o.pool = mock_connection_pool()
        o.pool._request.status = 200
        o.pool._request._supports_chunked_reads = True
        self.chunked_download_called = False
        o._chunked_download = self.mock_chunked_download
        yield o._download()
        assert(self.chunked_download_called is True)

    @mock.patch('shutil.copyfileobj')
    @gen_test
    def test_download_chunk_supported_downloads_one(
            self, mock_copyfileobj):
        o = get_test_object()
        o.pool = mock_connection_pool()
        o.pool._request.status = 200
        o.pool._request._supports_chunked_reads = False
        self.chunked_download_called = False
        o._chunked_download = self.mock_chunked_download
        yield o._download()
        mock_copyfileobj.assert_called()
        assert(self.chunked_download_called is False)

    @gen_test
    def test_notify_calls_registered(self):
        o = get_test_object()
        o.registered_notify.append(self.mock_notify)
        self.notify_called = 0
        self.notify_args = list()
        yield o._notify(100, 1)
        assert(self.notify_called is True)
        assert(self.notify_args[0] == (100, 1))

    @mock.patch('netsplice.util.hkpk.download.download._release_targets')
    @mock.patch(
        'netsplice.util.hkpk.download.download.'
        '_move_unchecked_to_invalid')
    @mock.patch(
        'netsplice.util.hkpk.download.'
        'download._move_unchecked_to_target')
    @mock.patch(
        'netsplice.util.hkpk.download.download.'
        '_move_incomplete_to_unchecked')
    @mock.patch('netsplice.util.hkpk.download.download._check_signature')
    @mock.patch('netsplice.util.hkpk.download.download._occupy_targets')
    @gen_test
    def test_start(
            self,
            mock_occupy_targets, mock_checksignature,
            mock_move_incomplete_to_unchecked, mock_move_unchecked_to_target,
            mock_move_unchecked_to_invalid,
            mock_release_targets):
        o = get_test_object()
        self.download_called = False
        o._download = self.mock_download
        yield o.start()
        mock_occupy_targets.assert_called()
        assert(self.download_called is True)
        mock_move_incomplete_to_unchecked.assert_called()
        mock_checksignature.assert_called()
        mock_move_unchecked_to_target.assert_called()

        mock_release_targets.assert_not_called()
        mock_move_unchecked_to_invalid.assert_not_called()

    @mock.patch('netsplice.util.hkpk.download.download._release_targets')
    @mock.patch(
        'netsplice.util.hkpk.download.download.'
        '_move_unchecked_to_invalid')
    @mock.patch(
        'netsplice.util.hkpk.download.'
        'download._move_unchecked_to_target')
    @mock.patch(
        'netsplice.util.hkpk.download.download.'
        '_move_incomplete_to_unchecked')
    @mock.patch('netsplice.util.hkpk.download.download._check_signature')
    @mock.patch('netsplice.util.hkpk.download.download._occupy_targets')
    @gen_test
    def test_start_without_signature(
            self,
            mock_occupy_targets, mock_checksignature,
            mock_move_incomplete_to_unchecked, mock_move_unchecked_to_target,
            mock_move_unchecked_to_invalid,
            mock_release_targets):
        o = get_test_object()
        o._download = self.mock_download
        self.download_called = False
        o.signature = None
        yield o.start()
        mock_occupy_targets.assert_called()
        assert(self.download_called is True)
        mock_move_incomplete_to_unchecked.assert_called()
        mock_checksignature.assert_not_called()
        mock_move_unchecked_to_target.assert_called()

        mock_release_targets.assert_not_called()
        mock_move_unchecked_to_invalid.assert_not_called()

    @mock.patch('netsplice.util.hkpk.download.download._release_targets')
    @mock.patch(
        'netsplice.util.hkpk.download.download.'
        '_move_unchecked_to_invalid')
    @mock.patch(
        'netsplice.util.hkpk.download.'
        'download._move_unchecked_to_target')
    @mock.patch(
        'netsplice.util.hkpk.download.download.'
        '_move_incomplete_to_unchecked')
    @mock.patch('netsplice.util.hkpk.download.download._check_signature')
    @mock.patch('netsplice.util.hkpk.download.download._occupy_targets')
    @mock.patch(
        'netsplice.util.hkpk.download_ioloop.'
        'download_ioloop._notify_error')
    @mock.patch(
        'netsplice.util.hkpk.download_ioloop.'
        'download_ioloop._download')
    @gen_test
    def test_start_handles_invalid_fp(
            self,
            mock_download, mock_notify_error, mock_occupy_targets,
            mock_checksignature,
            mock_move_incomplete_to_unchecked, mock_move_unchecked_to_target,
            mock_move_unchecked_to_invalid,
            mock_release_targets):
        o = get_test_object()

        mock_download.side_effect = InvalidError('invalid', 'host', 'valid')
        mock_notify_error.return_value = gen.Future()
        mock_notify_error.return_value.set_result(0)

        yield o.start()
        mock_notify_error.assert_called()
        mock_release_targets.assert_called()

        mock_move_unchecked_to_invalid.assert_not_called()

    @mock.patch('netsplice.util.hkpk.download.download._release_targets')
    @mock.patch(
        'netsplice.util.hkpk.download.download.'
        '_move_unchecked_to_invalid')
    @mock.patch(
        'netsplice.util.hkpk.download.'
        'download._move_unchecked_to_target')
    @mock.patch(
        'netsplice.util.hkpk.download.download.'
        '_move_incomplete_to_unchecked')
    @mock.patch('netsplice.util.hkpk.download.download._check_signature')
    @mock.patch('netsplice.util.hkpk.download.download._occupy_targets')
    @mock.patch(
        'netsplice.util.hkpk.download_ioloop.'
        'download_ioloop._notify_error')
    @mock.patch(
        'netsplice.util.hkpk.download_ioloop.'
        'download_ioloop._download')
    @gen_test
    def test_start_handles_network_error(
            self,
            mock_download, mock_notify_error,
            mock_occupy_targets, mock_checksignature,
            mock_move_incomplete_to_unchecked, mock_move_unchecked_to_target,
            mock_move_unchecked_to_invalid,
            mock_release_targets):
        o = get_test_object()
        o.signature = None
        mock_download.side_effect = NetworkError('test')
        mock_notify_error.return_value = gen.Future()
        mock_notify_error.return_value.set_result(0)
        yield o.start()
        mock_notify_error.assert_called()
        mock_release_targets.assert_called()

        mock_move_unchecked_to_invalid.assert_not_called()

    @mock.patch('netsplice.util.hkpk.download.download._release_targets')
    @mock.patch(
        'netsplice.util.hkpk.download.download.'
        '_move_unchecked_to_invalid')
    @mock.patch(
        'netsplice.util.hkpk.download.'
        'download._move_unchecked_to_target')
    @mock.patch(
        'netsplice.util.hkpk.download.download.'
        '_move_incomplete_to_unchecked')
    @mock.patch('netsplice.util.hkpk.download.download._check_signature')
    @mock.patch('netsplice.util.hkpk.download.download._occupy_targets')
    @mock.patch(
        'netsplice.util.hkpk.download_ioloop.'
        'download_ioloop._notify_error')
    @mock.patch(
        'netsplice.util.hkpk.download_ioloop.'
        'download_ioloop._download')
    @gen_test
    def test_start_handles_signature_error(
            self,
            mock_download, mock_notify_error,
            mock_occupy_targets, mock_checksignature,
            mock_move_incomplete_to_unchecked, mock_move_unchecked_to_target,
            mock_move_unchecked_to_invalid,
            mock_release_targets):
        o = get_test_object()
        o.signature = ''
        mock_download.side_effect = SignatureError('test')
        mock_notify_error.return_value = gen.Future()
        mock_notify_error.return_value.set_result(0)
        yield o.start()
        mock_notify_error.assert_called()
        mock_release_targets.assert_called()
        mock_move_unchecked_to_invalid.assert_called()

    @mock.patch('netsplice.util.hkpk.download.download._release_targets')
    @mock.patch(
        'netsplice.util.hkpk.download.download.'
        '_move_unchecked_to_invalid')
    @mock.patch(
        'netsplice.util.hkpk.download.'
        'download._move_unchecked_to_target')
    @mock.patch(
        'netsplice.util.hkpk.download.download.'
        '_move_incomplete_to_unchecked')
    @mock.patch('netsplice.util.hkpk.download.download._check_signature')
    @mock.patch('netsplice.util.hkpk.download.download._occupy_targets')
    @mock.patch(
        'netsplice.util.hkpk.download_ioloop.'
        'download_ioloop._notify_error')
    @mock.patch(
        'netsplice.util.hkpk.download_ioloop.'
        'download_ioloop._download')
    @gen_test
    def test_start_handles_io_error(
            self,
            mock_download, mock_notify_error,
            mock_occupy_targets, mock_checksignature,
            mock_move_incomplete_to_unchecked, mock_move_unchecked_to_target,
            mock_move_unchecked_to_invalid,
            mock_release_targets):
        o = get_test_object()
        o.signature = None
        mock_download.side_effect = IOError(13, 'test')
        mock_notify_error.return_value = gen.Future()
        mock_notify_error.return_value.set_result(0)
        yield o.start()
        mock_notify_error.assert_called()
        mock_release_targets.assert_called()

        mock_move_unchecked_to_invalid.assert_not_called()
