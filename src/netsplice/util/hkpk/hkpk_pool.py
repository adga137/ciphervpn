# -*- coding: utf-8 -*-
# hkpk_pool.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
HKPK HTTPS Connection Pool

Customized HTTPS pool with a verify_conn method that checks the fingerprint
of the connection with a list of known fingerprints.
'''
import certifi
import base64
import hashlib

from M2Crypto import BIO, X509
from urllib3 import HTTPSConnectionPool
from .errors import InvalidError


class hkpk_pool(HTTPSConnectionPool):
    '''
    URLLib3 Connection Pool.

    Connection Pool for a host with a pre-configured timeout and a validate
    connection method that checks the public key checksum with the list of
    known fingerprints.

    Extends:
        HTTPSConnectionPool

    '''
    def __init__(self, host, timeout=None, pinset=[]):
        HTTPSConnectionPool.__init__(
            self, host, 443,
            cert_reqs='CERT_REQUIRED', ca_certs=certifi.where(),
            timeout=timeout)
        self.pinset = []
        for fingerprint in pinset:
            self.pinset.append(fingerprint.replace(' ', ''))

    def get_pk_sha256_from_der(self, der):
        '''
        Get Public Key sha256 sum from (binary) DER.

        Load a certificate in DER format and extract the public key sha256
        checksum as string.

        Arguments:
            der (bytes): X509 certificate in binary DER format.

        Returns:
            string -- Public Key sha256 hash as hexdigest.
        '''
        x509 = X509.load_cert_string(der, X509.FORMAT_DER)
        mem = BIO.MemoryBuffer()
        x509.get_pubkey().get_rsa().save_pub_key_bio(mem)
        pk_der = mem.getvalue().split('\n')[1:-2]
        pk_base64 = ''.join(pk_der)
        pk_raw = base64.b64decode(pk_base64)
        pk_sha256 = hashlib.sha256(pk_raw).hexdigest()
        return pk_sha256

    def get_pk_sha256_from_cert(self, cert):
        '''
        Get Public Key sha256 sum from (text/pem) cert.

        Load a certificate in DER format and extract the public key sha256
        checksum as string.

        Arguments:
            cert (string): X509 certificate in PEM format.

        Returns:
            string -- Public Key sha256 hash as hexdigest.
        '''
        x509 = X509.load_cert_string(cert, X509.FORMAT_PEM)

        mem = BIO.MemoryBuffer()
        x509.get_pubkey().get_rsa().save_pub_key_bio(mem)
        pk_der = mem.getvalue().split('\n')[1:-2]
        pk_base64 = ''.join(pk_der)
        pk_raw = base64.b64decode(pk_base64)
        pk_sha256 = hashlib.sha256(pk_raw).hexdigest()
        return pk_sha256

    def _validate_conn(self, conn):
        '''
        Validate Connection.

        Overload of pool validate_conn to run extra checks on the server
        certificate.

        Arguments:
            conn (urllib connection): Connection in handshake mode.

        Returns:
            bool -- Connection is verified.

        Raises:
            HKPKInvalidError -- The fingerprint is not in the known pinset.
        '''
        HTTPSConnectionPool._validate_conn(self, conn)
        if not conn.is_verified:
            return False
        pk_sha256 = None
        try:
            pk_sha256 = self.get_pk_sha256_from_der(
                conn.sock.getpeercert(binary_form=True))
        except X509.X509Error:
            raise InvalidError(
                pk_sha256,
                conn.host,
                self.pinset)

        if pk_sha256 not in self.pinset:
            raise InvalidError(
                pk_sha256,
                conn.host,
                self.pinset)

        return True
