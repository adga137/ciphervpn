# -*- coding: utf-8 -*-
# utils.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Logs utility.

Defines a singleton that can be used in the application.
Configures the log using config/log.
Uses logbook as base so it is easy to understand logging for new developers.
```
    from netsplice.util import get_logger
    logger = get_logger()
    logger.info('Message visible in Log')
```
'''

import logbook
import os
import sys

from logbook.more import ColorizedStderrHandler

from netsplice.util.path import (get_location, mkdir_p)
from netsplice.util.logger_model_handler import logger_model_handler
from netsplice.config import flags
from netsplice.config import constants
from netsplice.config import log as config


class logger_singleton(object):
    '''
    Logger Singleton.

    Singleton implementation that allows using the same logger instance in
    different classes.
    '''

    def __init__(self):
        '''
        Initialize.

        Initialize Members.
        '''
        self.level = logbook.WARNING
        if flags.DEBUG:
            self.level = logbook.NOTSET
        self.logger = None
        self.model_handler = None

    def _force_log_files(self):
        '''
        Force Log Files

        Override the config value and store logfiles anyway.
        '''
        force_file = os.path.join(
            get_location(config.WRITE_LOG_FILES_LOCATION),
            'FORCE_LOG_FILES')
        if os.path.exists(force_file):
            return True
        return False

    def _setup_model_handler(self, name):
        '''
        Setup Model Handler.

        Setup the model handler that puts all log messages into a model.
        '''
        self.model_handler = logger_model_handler(name=name)
        self.model_handler.push_application()

    def _setup_file_handler(self, name):
        '''
        Setup File Handler.

        Setup the file handler that puts all log messages into a file. This
        mode should only be used on custom deployments as this can leak
        sensitive information.
        '''
        mkdir_p(get_location(config.WRITE_LOG_FILES_LOCATION))
        log_file = os.path.join(
            get_location(config.WRITE_LOG_FILES_LOCATION),
            '%s.log' % (name,))

        file_handler = logbook.RotatingFileHandler(
            log_file,
            format_string=config.LOG_FORMAT,
            bubble=True)
        file_handler.perform_rollover()
        file_handler.push_application()

    def _setup_stderr_handler(self, name):
        '''
        Setup Stderr Handler.

        Setup the StdErr handler that puts all log messages on the stderr
        output. Only useful during development and debugging in terminals.
        '''
        if name in config.STDERR_LOGGER_NAMES:
            stream_handler = ColorizedStderrHandler(
                level=logbook.__dict__[config.STDERR_LOG_LEVEL],
                format_string=config.LOG_FORMAT,
                bubble=True)
            stream_handler.push_application()

    def get_logger(self):
        '''
        Get Logger.

        Return the logger singleton for usage in the application. Only exposes
        the documented logbook
        (http://logbook.readthedocs.io/en/stable/index.html) interface.
        '''
        return self.logger

    def is_setup(self):
        '''
        Setup was done.

        Check if the singleton has been setup.
        '''
        return self.logger is not None

    def setup(self, name):
        '''
        Setup.

        Configure the logbook handler that are active for the current
        application.
        '''
        self._setup_model_handler(name)
        if config.WRITE_LOG_FILES or self._force_log_files():
            self._setup_file_handler(name)
        if config.WRITE_STDERR:
            self._setup_stderr_handler(name)
        self.logger = logbook.Logger(constants.NS_NAME)

    def take_logs(self):
        '''
        Take Logs.

        Return the current available log_entries and purge the current buffer.
        '''
        return self.model_handler.take_logs()


logger = logger_singleton()
