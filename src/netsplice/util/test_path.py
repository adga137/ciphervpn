import mock
import os
import sys
from StringIO import StringIO
from files import (
    check_and_fix_urw_only,
    safe_write
)
from .path import get_location
from ..config import flags


def get_test_filename():
    test_path = get_location('tests')
    filename = '%(test_path)s/test.file' % dict(test_path=test_path)
    print(filename)
    return filename


def teardown():
    '''
    Cleanup files created by the safe_write tests.
    '''
    try:
        os.remove(get_test_filename())

    except:
        pass
