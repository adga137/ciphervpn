import mock
import os
import sys
import io
import psutil

from .reaper import reaper

import netsplice.config.process as config
import netsplice.config.flags as flags

from netsplice.model.process import process as process_model
from netsplice.util.errors import NotInitializedError


class mock_psutil_proc_item(object):
    def __init__(self):
        self.pid = -1
        self._cmdline = []
        self._ppid = -1
        self._username = 'none'
        self.kill_called = False
        self.terminate_called = False
        self.wait_called = False

    def ppid(self):
        return self._ppid

    def cmdline(self):
        return self._cmdline

    def kill(self):
        self.kill_called = True

    def terminate(self):
        self.terminate_called = True

    def username(self):
        return self._username

    def wait(self, timeout=0):
        self.wait_called = True


class mock_psutil_proc_list(list):
    def __init__(self, attr_list_dict):
        for ps_item in attr_list_dict:
            item = mock_psutil_proc_item()
            for (key, value) in ps_item.items():
                if isinstance(value, (Exception,)):
                    evalue = value

                    def mock_exc(var_arg=None):
                        raise evalue

                    item.__dict__[key] = mock_exc
                else:
                    attr = '_%s' % (key,)
                    if key == 'pid':
                        attr = 'pid'
                    item.__dict__[attr] = value
            self.append(item)


class MockError(Exception):
    def __init__(self):
        Exception.__init__(self)


def mock_popenmock_popen_process(*args, **kwargs):
    class process_mock(object):
        def __init__(self):
            self.stdout = io.StringIO()
            self.stderr = io.StringIO()
    return process_mock()


@mock.patch('os.getpid')
def test_debug_calls_os_getpid(mock_os_getpid):
    r = reaper()
    mock_os_getpid.assert_called()


@mock.patch('os.getpid', return_value=1)
@mock.patch('psutil.process_iter')
def test_find_children_ignores_own_process(
        mock_psutil_process_iter, mock_os_getpid):
    '''
    check that no children are returned when the proc_list ppid is not
    what os.getpid returns.
    '''
    r = reaper()
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 2, 'cmdline': ['/bin/test']}
    ])
    res = r.find_children()
    print(res)
    assert(len(res) is 0)


@mock.patch('os.getpid', return_value=1)
@mock.patch('psutil.process_iter')
def test_find_children_ignores_empty_commandline(
        mock_psutil_process_iter, mock_os_getpid):
    '''
    check that no children are returned when the commandline for the procs
    with the same ppid is empty
    '''
    r = reaper()
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'cmdline': ['']}
    ])
    res = r.find_children()
    print(res)
    assert(len(res) is 0)


@mock.patch('os.getpid', return_value=1)
@mock.patch('psutil.process_iter')
def test_find_children_ignores_zombie_process(
        mock_psutil_process_iter, mock_os_getpid):
    r = reaper()
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'cmdline': psutil.ZombieProcess(1)}
    ])
    res = r.find_children()
    print(res)
    assert(len(res) is 0)


@mock.patch('os.getpid', return_value=1)
@mock.patch('psutil.process_iter')
def test_find_children_ignores_access_denied(
        mock_psutil_process_iter, mock_os_getpid):
    r = reaper()
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'cmdline': psutil.AccessDenied(1)}
    ])
    res = r.find_children()
    print(res)
    assert(len(res) is 0)


@mock.patch('os.getpid', return_value=1)
@mock.patch('psutil.process_iter')
def test_find_children_returns_list(
        mock_psutil_process_iter, mock_os_getpid):
    r = reaper()
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'cmdline': ['/bin/bash']},
        {'ppid': 1, 'pid': 3, 'cmdline': ['/bin/ash']},
    ])
    res = r.find_children()
    print(res)
    for item in res:
        assert(item.ppid() == 1)
    assert(len(res) is 2)


@mock.patch('psutil.process_iter')
def test_find_subprocess_source_finds_python_process(
        mock_psutil_process_iter):
    r = reaper()
    r.execute_source = True
    o_sys_executable = sys.executable
    sys.executable = '/usr/bin/python'
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'cmdline': ['/usr/bin/python', 'test.py']},
    ])
    result = r.find_subprocess(['test.py'])
    sys.executable = o_sys_executable
    print(result)
    assert(len(result) is 1)
    assert(result[0].pid == 2)


@mock.patch('psutil.process_iter')
def test_find_subprocess_ignores_zombie_process(
        mock_psutil_process_iter):
    r = reaper()
    r.execute_source = True
    o_sys_executable = sys.executable
    sys.executable = '/usr/bin/python'
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'cmdline': psutil.ZombieProcess(1)},
    ])
    result = r.find_subprocess(['test.py'])
    sys.executable = o_sys_executable
    print(result)
    assert(len(result) is 0)


@mock.patch('psutil.process_iter')
def test_find_subprocess_ignores_access_denied(
        mock_psutil_process_iter):
    r = reaper()
    r.execute_source = True
    o_sys_executable = sys.executable
    sys.executable = '/usr/bin/python'
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'cmdline': psutil.AccessDenied(1)},
    ])
    result = r.find_subprocess(['test.py'])
    sys.executable = o_sys_executable
    print(result)
    assert(len(result) is 0)


@mock.patch('psutil.process_iter')
def test_find_subprocess_ignores_no_such_process(
        mock_psutil_process_iter):
    r = reaper()
    r.execute_source = True
    o_sys_executable = sys.executable
    sys.executable = '/usr/bin/python'
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'cmdline': psutil.NoSuchProcess(1)},
    ])
    result = r.find_subprocess(['test.py'])
    sys.executable = o_sys_executable
    print(result)
    assert(len(result) is 0)


@mock.patch('psutil.process_iter')
def test_find_subprocess_ignores_short_commandline(
        mock_psutil_process_iter):
    r = reaper()
    r.execute_source = True
    o_sys_executable = sys.executable
    sys.executable = '/usr/bin/python'
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'cmdline': ['']},
    ])
    result = r.find_subprocess(['test.py'])
    sys.executable = o_sys_executable
    print(result)
    assert(len(result) is 0)


@mock.patch('netsplice.util.process.reaper.logger.warn')
@mock.patch('psutil.process_iter')
def test_find_subprocess_source_executable_not_matching_verbose_gt_5_warns(
        mock_psutil_process_iter, mock_logger_warn):
    flags.VERBOSE = 6
    r = reaper()
    r.execute_source = True
    o_sys_executable = sys.executable
    sys.executable = '/usr/bin/python'
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'cmdline': ['test_nomatch.py', 'test']},
    ])
    result = r.find_subprocess(['test.py'])
    print(result)
    sys.executable = o_sys_executable
    mock_logger_warn.assert_called()
    flags.VERBOSE = 0
    assert(len(result) is 0)


@mock.patch('psutil.process_iter')
def test_find_subprocess_source_executable_not_matching_ignores(
        mock_psutil_process_iter):
    r = reaper()
    r.execute_source = True
    o_sys_executable = sys.executable
    sys.executable = '/usr/bin/python'
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'cmdline': ['test_nomatch.py', 'test']},
    ])
    flags.VERBOSE = 0
    result = r.find_subprocess(['test.py'])
    print(result)
    sys.executable = o_sys_executable
    assert(len(result) is 0)


@mock.patch('netsplice.util.process.reaper.logger.warn')
@mock.patch('psutil.process_iter')
def test_find_subprocess_arg_count_not_matching_verbose_gt_5_warns(
        mock_psutil_process_iter, mock_logger_warn):
    flags.VERBOSE = 6
    r = reaper()
    r.execute_source = False
    o_sys_executable = sys.executable
    sys.executable = '/usr/bin/python'
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'cmdline': [
            'test.py', 'arg_nomatch', 'arg_nomatch']},
    ])
    result = r.find_subprocess(['test.py', 'arg'])
    print(result)
    sys.executable = o_sys_executable
    flags.VERBOSE = 0
    mock_logger_warn.assert_called()
    assert(len(result) is 0)


@mock.patch('netsplice.util.process.reaper.logger.warn')
@mock.patch('psutil.process_iter')
def test_find_subprocess_arg_count_not_matching_ignores(
        mock_psutil_process_iter, mock_logger_warn):
    r = reaper()
    r.execute_source = False
    o_sys_executable = sys.executable
    sys.executable = '/usr/bin/python'
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'cmdline': [
            'test.py', 'arg_nomatch', 'arg_nomatch']},
    ])
    result = r.find_subprocess(['test.py', 'arg'])
    print(result)
    sys.executable = o_sys_executable
    assert(len(result) is 0)


@mock.patch('netsplice.util.process.reaper.logger.warn')
@mock.patch('psutil.process_iter')
def test_find_subprocess_args_not_matching_verbose_gt_5_warns(
        mock_psutil_process_iter, mock_logger_warn):
    flags.VERBOSE = 6
    r = reaper()
    r.execute_source = False
    o_sys_executable = sys.executable
    sys.executable = '/usr/bin/python'
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'cmdline': [
            'test.py', 'arg', 'nomatch']},
    ])
    result = r.find_subprocess(['test.py', 'arg', 'arg'])
    print(result)
    sys.executable = o_sys_executable
    flags.VERBOSE = 0
    mock_logger_warn.assert_called()
    assert(len(result) is 0)


@mock.patch('netsplice.util.process.reaper.logger.warn')
@mock.patch('psutil.process_iter')
def test_find_subprocess_ignore_args_not_matching(
        mock_psutil_process_iter, mock_logger_warn):
    r = reaper()
    r.execute_source = False
    o_sys_executable = sys.executable
    sys.executable = '/usr/bin/python'
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'cmdline': [
            'test.py', 'arg', 'nomatch']},
    ])
    result = r.find_subprocess(['test.py', 'arg', 'arg'])
    sys.executable = o_sys_executable
    assert(len(result) is 0)


@mock.patch('netsplice.util.process.reaper.logger.warn')
@mock.patch('psutil.process_iter')
def test_find_subprocess_returns_processes(
        mock_psutil_process_iter, mock_logger_warn):
    flags.VERBOSE = 0
    r = reaper()
    r.execute_source = False
    o_sys_executable = sys.executable
    sys.executable = '/usr/bin/python'
    mock_psutil_process_iter.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'cmdline': [
            'test.py', 'arg', 'brg']},
        {'ppid': 1, 'pid': 3, 'cmdline': [
            'test.py', 'arg', 'brg']},
    ])
    result = r.find_subprocess(['test.py', 'arg', 'brg'])
    sys.executable = o_sys_executable
    assert(len(result) is 2)


@mock.patch('netsplice.util.process.reaper.reaper.find_children')
@mock.patch('netsplice.util.process.reaper.logger')
def test_kill_children_calls_find_children(
        mock_logger, mock_find_children):
    r = reaper()
    r.kill_children()
    mock_find_children.assert_called()


@mock.patch('netsplice.util.process.reaper.reaper.find_children')
@mock.patch('netsplice.util.process.reaper.logger')
def test_kill_children_exception_logs_exception(
        mock_logger, mock_find_children):
    r = reaper()
    mock_find_children.return_value = mock_psutil_proc_list([
        {'pid': 1, 'kill': psutil.ZombieProcess(1)}
    ])
    r.kill_children()
    mock_logger.critical.assert_called()


@mock.patch('netsplice.util.process.reaper.reaper.find_children')
@mock.patch('netsplice.util.process.reaper.logger')
def test_kill_children_exception_returns_has_zombies(
        mock_logger, mock_find_children):
    r = reaper()
    mock_find_children.return_value = mock_psutil_proc_list([
        {'pid': 1, 'kill': psutil.ZombieProcess(1)},
        {'pid': 2}
    ])
    has_zombies = r.kill_children()
    assert(has_zombies is True)

    mock_find_children.return_value = mock_psutil_proc_list([
        {'pid': 1},
        {'pid': 2}
    ])
    has_zombies = r.kill_children()
    assert(has_zombies is False)


@mock.patch('netsplice.util.process.reaper.reaper.find_subprocess')
def test_kill_other_subprocess_calls_find_subprocess(
        mock_find_subprocess):
    r = reaper()
    r.kill_other_subprocess([])
    mock_find_subprocess.assert_called()


@mock.patch('netsplice.util.process.reaper.reaper.find_subprocess')
def test_kill_other_subprocess_ignores_own_pid(
        mock_find_subprocess):
    r = reaper()
    r.pid = 2
    mock_find_subprocess.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'username': 'test', 'cmdline': [
            'test.py', 'test']}
    ])
    r.kill_other_subprocess([])
    assert(mock_find_subprocess.return_value[0].terminate_called is False)


@mock.patch('netsplice.util.process.reaper.reaper.find_subprocess')
def test_kill_other_subprocess_ignores_parent_pid(
        mock_find_subprocess):
    r = reaper()
    r.pid = 1
    mock_find_subprocess.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'username': 'test', 'cmdline': [
            'test.py', 'test']}
    ])
    r.kill_other_subprocess([])
    assert(mock_find_subprocess.return_value[0].terminate_called is False)


@mock.patch('netsplice.util.process.reaper.reaper.find_subprocess')
def test_kill_other_subprocess_ignores_zombie_process(
        mock_find_subprocess):
    r = reaper()
    r.pid = 1
    mock_find_subprocess.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'username': 'test',
            'cmdline': psutil.ZombieProcess(1)}
    ])
    r.kill_other_subprocess([])
    assert(mock_find_subprocess.return_value[0].terminate_called is False)


@mock.patch('netsplice.util.process.reaper.reaper.find_subprocess')
def test_kill_other_subprocess_ignores_access_denied(
        mock_find_subprocess):
    r = reaper()
    r.pid = 1
    mock_find_subprocess.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'username': 'test',
            'cmdline': psutil.AccessDenied(1)}
    ])
    result = r.kill_other_subprocess([])
    assert(mock_find_subprocess.return_value[0].terminate_called is False)
    assert(result is False)


@mock.patch('netsplice.util.process.reaper.reaper.find_subprocess')
def test_kill_other_subprocess_ignores_no_such_process(
        mock_find_subprocess):
    r = reaper()
    r.pid = 1
    mock_find_subprocess.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'username': 'test',
            'cmdline': psutil.NoSuchProcess(1)}
    ])
    result = r.kill_other_subprocess([])
    assert(mock_find_subprocess.return_value[0].terminate_called is False)
    assert(result is False)


@mock.patch('netsplice.util.process.reaper.reaper.find_subprocess')
def test_kill_other_subprocess_ignores_proc_parent_pid_equals_pid(
        mock_find_subprocess):
    r = reaper()
    r.pid = 1
    r.parent_pid = 2
    mock_find_subprocess.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'username': 'test',
            'cmdline': ['test.py', 'args']}
    ])
    result = r.kill_other_subprocess([])
    assert(mock_find_subprocess.return_value[0].terminate_called is False)
    assert(result is False)


@mock.patch('netsplice.util.process.reaper.reaper.find_subprocess')
@mock.patch('netsplice.util.process.reaper.logger')
def test_kill_other_subprocess_warns(
        mock_logger, mock_find_subprocess):
    r = reaper()
    r.pid = 3
    mock_find_subprocess.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'username': 'test',
            'cmdline': ['test.py', 'args']}
    ])
    result = r.kill_other_subprocess([])
    mock_logger.warn.assert_called()
    assert(mock_find_subprocess.return_value[0].terminate_called is True)
    assert(result is True)


@mock.patch('netsplice.util.process.reaper.reaper.find_subprocess')
@mock.patch('netsplice.util.process.reaper.logger')
def test_kill_other_subprocess_calls_terminate(
        mock_logger, mock_find_subprocess):
    r = reaper()
    r.pid = 3
    mock_find_subprocess.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'username': 'test',
            'cmdline': ['test.py', 'args']}
    ])
    result = r.kill_other_subprocess([])
    assert(mock_find_subprocess.return_value[0].terminate_called is True)
    assert(result is True)


@mock.patch('netsplice.util.process.reaper.reaper.find_subprocess')
@mock.patch('netsplice.util.process.reaper.logger')
def test_kill_other_subprocess_calls_wait(
        mock_logger, mock_find_subprocess):
    r = reaper()
    r.pid = 3
    mock_find_subprocess.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'username': 'test',
            'cmdline': ['test.py', 'args']}
    ])
    result = r.kill_other_subprocess([])
    assert(mock_find_subprocess.return_value[0].wait_called is True)
    assert(result is True)


@mock.patch('netsplice.util.process.reaper.reaper.find_subprocess')
@mock.patch('netsplice.util.process.reaper.logger')
def test_kill_other_subprocess_logs_termination(
        mock_logger, mock_find_subprocess):
    r = reaper()
    r.pid = 3
    mock_find_subprocess.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'username': 'test',
            'cmdline': ['test.py', 'args']}
    ])
    result = r.kill_other_subprocess([])
    mock_logger.info.assert_called()
    assert(mock_find_subprocess.return_value[0].terminate_called is True)
    assert(result is True)


@mock.patch('netsplice.util.process.reaper.reaper.find_subprocess')
@mock.patch('netsplice.util.process.reaper.logger')
def test_kill_other_subprocess_calls_kill_on_timeout(
        mock_logger, mock_find_subprocess):
    r = reaper()
    r.pid = 3
    mock_find_subprocess.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'username': 'test',
            'cmdline': ['test.py', 'args'], 'wait': psutil.TimeoutExpired(0)}
    ])
    result = r.kill_other_subprocess([])
    mock_logger.info.assert_called()
    assert(mock_find_subprocess.return_value[0].kill_called is True)
    assert(result is True)


@mock.patch('netsplice.util.process.reaper.reaper.find_subprocess')
@mock.patch('netsplice.util.process.reaper.logger')
def test_kill_other_subprocess_logs_no_such_process(
        mock_logger, mock_find_subprocess):
    r = reaper()
    r.pid = 3
    mock_find_subprocess.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'username': 'test',
            'terminate': psutil.NoSuchProcess(0)}
    ])
    result = r.kill_other_subprocess([])
    mock_logger.warn.assert_called_with('No Such Process')
    assert(result is False)


@mock.patch('netsplice.util.process.reaper.reaper.find_subprocess')
@mock.patch('netsplice.util.process.reaper.logger')
def test_kill_other_subprocess_logs_access_denied(
        mock_logger, mock_find_subprocess):
    r = reaper()
    r.pid = 3
    mock_find_subprocess.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'username': 'test',
            'terminate': psutil.AccessDenied(0)}
    ])
    result = r.kill_other_subprocess([])
    mock_logger.critical.assert_called()
    assert(result is False)


@mock.patch('netsplice.util.process.reaper.reaper.find_subprocess')
@mock.patch('netsplice.util.process.reaper.logger')
def test_kill_other_subprocess_logs_other_exceptions(
        mock_logger, mock_find_subprocess):
    r = reaper()
    r.pid = 3
    mock_find_subprocess.return_value = mock_psutil_proc_list([
        {'ppid': 1, 'pid': 2, 'username': 'test',
            'terminate': Exception()}
    ])
    result = r.kill_other_subprocess([])
    mock_logger.critical.assert_called()
    assert(result is False)


@mock.patch('psutil.Process')
def test_kill_pid_calls_kill(
        mock_psutil_process):
    mock_psutil_process.return_value = mock_psutil_proc_item()
    mock_psutil_process.return_value.pid = 1
    r = reaper()
    result = r.kill_pid(1)
    assert(mock_psutil_process.return_value.kill_called is True)
    assert(result is True)


@mock.patch('netsplice.util.process.reaper.logger')
@mock.patch('psutil.Process')
def test_kill_pid_logs_no_such_process(
        mock_psutil_process, mock_logger):
    def mock_exc():
        raise psutil.NoSuchProcess(0)
    mock_psutil_process.return_value = mock_psutil_proc_item()
    mock_psutil_process.return_value.kill = mock_exc
    r = reaper()
    result = r.kill_pid(1)
    mock_logger.warn.assert_called()
    assert(result is False)


@mock.patch('netsplice.util.process.reaper.logger')
@mock.patch('psutil.Process')
def test_kill_pid_logs_zombie_process(
        mock_psutil_process, mock_logger):
    def mock_exc():
        raise psutil.ZombieProcess(0)
    mock_psutil_process.return_value = mock_psutil_proc_item()
    mock_psutil_process.return_value.kill = mock_exc
    r = reaper()
    result = r.kill_pid(1)
    mock_logger.warn.assert_called()
    assert(result is False)


@mock.patch('netsplice.util.process.reaper.logger')
@mock.patch('psutil.Process')
def test_kill_pid_logs_access_denied(
        mock_psutil_process, mock_logger):
    def mock_exc():
        raise psutil.AccessDenied(0)
    mock_psutil_process.return_value = mock_psutil_proc_item()
    mock_psutil_process.return_value.kill = mock_exc
    r = reaper()
    result = r.kill_pid(1)
    mock_logger.critical.assert_called()
    assert(result is False)


@mock.patch('netsplice.util.process.reaper.reaper.kill_pid')
@mock.patch('os.getpid', return_value=9999)
def test_kill_self_calls_kill_pid(
        mock_os_getpid, mock_kill_pid):
    r = reaper()
    r.kill_self()
    mock_kill_pid.assert_called_with(9999)
