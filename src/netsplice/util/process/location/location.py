# -*- coding: utf-8 -*-
# darwin.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Location base for common location functions.
'''
import os
import sys

from netsplice.config import process as config
from netsplice.config import flags
from netsplice.util import get_logger
from netsplice.util.process.errors import ProcessError
from netsplice.util.process.executable_extension import (
    factory as executable_factory
)
logger = get_logger()


class location(object):

    def __init__(self):
        # Name of the executable to be located.
        self._name = None
        # Optional version.
        self._version = None
        # Optional Prefix path.
        self._prefix = None
        # Optional Search path.
        self._custom_search_paths = None
        # Executing on a source-tree.
        self.execute_source = config.execute_source

        # Paths the location instance uses to compose the output locations
        # dev: git-root/src/netsplice
        # prod: c:\program files (x86)\netsplice
        self._path = self.get_script_path()

        # dev: git-root
        # prod: c:\program files (x86)\netsplice
        self._install_path = self.get_install_path()

        # dev: venv/ns/bin
        # prod: c:\program files (x86)\netsplice
        self._self_executable_path = os.path.dirname(sys.executable)

    def get_binary_name(self):
        '''
        Return the binary name (name + optional exe or python) for the
        configured process.
        '''
        if self.execute_source and \
                self._name in config.SUBAPP_NAMES:
            binary_name = sys.executable
            return binary_name

        executable_extension = executable_factory(sys.platform)
        binary_name = executable_extension.get(self._name)

        return binary_name

    def get_binary_path(self):
        '''
        Prepare the binary path and raise errors if it cannot be constructed.
        Raises a Error, when the compiled filename does not exist.
        '''
        search_paths = self.get_binary_search_paths()

        binary_name = self.get_binary_name()
        for path in search_paths:
            prefixed_binary_name = binary_name
            if self._prefix is not None:
                prefixed_binary_name = os.path.join(
                    self._prefix,
                    binary_name)
            if self._name in config.LIBEXEC_BINARY_SYSTEM_LOCATIONS.keys():
                if path in config.LIBEXEC_BINARY_SYSTEM_LOCATIONS[self._name]:
                    prefixed_binary_name = binary_name
            if self._version is not None:
                prefixed_binary_name = os.path.join(
                    self._prefix,
                    self._version,
                    binary_name)

            possible_binary_path = os.path.join(
                path,
                prefixed_binary_name)

            if os.path.exists(possible_binary_path):
                return possible_binary_path
        raise ProcessError(
            'Executable %s not found in %s/%s.'
            % (binary_name, str(search_paths), self._prefix))

    def get_resource_path(self):
        '''
        Prepare a resource path and raise errors if it cannot be constructed.
        Raises a Error, when the compiled filename does not exist.
        '''
        search_paths = self.get_binary_search_paths()
        resource_name = self._name
        for path in search_paths:
            prefixed_resource_name = resource_name
            if self._prefix is not None:
                prefixed_resource_name = os.path.join(
                    self._prefix,
                    resource_name)
            if self._name in config.LIBEXEC_BINARY_SYSTEM_LOCATIONS.keys():
                if path in config.LIBEXEC_BINARY_SYSTEM_LOCATIONS[self._name]:
                    prefixed_resource_name = resource_name
            if self._version is not None:
                prefixed_resource_name = os.path.join(
                    self._prefix,
                    self._version,
                    resource_name)

            possible_resource_name = os.path.join(
                path,
                prefixed_resource_name)

            if os.path.exists(possible_resource_name):
                return os.path.abspath(possible_resource_name)
        raise ProcessError(
            'Resource %s not found in %s/%s.'
            % (resource_name, str(search_paths), self._prefix))

    def get_binary_search_paths(self):
        '''
        Prepare a search-path for the binary. Switch between git-locations
        and install locations.
        '''
        paths = []
        if self._custom_search_paths is not None:
            return self._custom_search_paths
        paths.append(self._self_executable_path)
        paths.append(self.get_libexec_path())

        if self._version is not None:
            return paths

        # Binary System Locations only apply for unversioned calls
        if self._name in config.LIBEXEC_BINARY_SYSTEM_LOCATIONS.keys():
            for location in config.LIBEXEC_BINARY_SYSTEM_LOCATIONS[self._name]:
                paths.append(str(location))
        if flags.DEBUG:
            logger.debug('Search Path: %s' % (str(paths),))
        return paths

    def get_install_path(self):
        '''
        Return the path that corresponds to the project root in a git-checkout
        environment.
        '''
        script_path = self.get_script_path()
        source_root = os.path.join(
            'src',
            'netsplice',
            '')
        install_path = script_path.replace(source_root, '')
        return install_path

    def get_libexec_path(self):
        '''
        Get Libexec Path.

        Return the path that is used to find executables delivered with the
        application.
        '''
        if self.execute_source:
            # Locate in _install_path/var/build
            return os.path.join(
                str(self._install_path),
                str(config.LIBEXEC_BUILD))
        else:
            # Locate in _install_path/libexec
            return os.path.join(
                str(self._self_executable_path),
                str(config.LIBEXEC))

    def get_prefix_path(self):
        '''
        Return the path where the software is installed to identify the
        locations of the binaries.
        '''
        if config.execute_source:
            script = os.path.dirname(os.path.realpath(__file__))
            root_path = os.path.join(
                'src', 'netsplice', 'util', 'process', 'location')
            prefix_path = script.replace(root_path, '')
        else:
            executable = os.path.dirname(os.path.realpath(__file__))
            root_path = os.path.join('bin')
            prefix_path = executable.replace(root_path, '')
        return prefix_path

    def get_script_path(self):
        '''
        Return the path where this module is installed to util/process.
        Useful for execute_source mode.
        '''
        script = os.path.dirname(os.path.realpath(__file__))
        module_path = os.path.join('util', 'process', 'location')
        script_path = script.replace(module_path, '')
        return script_path

    def set_custom_search_paths(self, search_paths):
        '''
        Set the search paths to be used instead of the application libexec
        locations.
        '''
        self._custom_search_paths = search_paths

    def set_name(self, name):
        '''
        Set the name for the binary that is to be located.
        '''
        self._name = name

    def set_version(self, version):
        '''
        Set the version for the binary that is to be located.
        '''
        self._version = version

    def set_prefix(self, prefix):
        '''
        Set the prefix for the binary that is to be located.
        '''
        self._prefix = prefix
