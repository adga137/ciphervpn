# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Factory for getting the a installed executable depending on the current
platform.
'''

import os
from netsplice.config import process as config

from netsplice.util.process.location.darwin import (
    darwin as darwin_location
)
from netsplice.util.process.location.posix import (
    posix as posix_location
)
from netsplice.util.process.location.win32 import (
    win32 as win32_location
)


def factory(sys_platform):
    '''
    Takes the given sys_platform (should be sys.platform format) and
    returns the executable filename. Assumes a posix environment unless
    the given platform needs something special.
    '''
    if sys_platform.startswith('darwin'):
        return darwin_location()
    if sys_platform.startswith('win32'):
        return win32_location()
    return posix_location()
