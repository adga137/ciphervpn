# -*- coding: utf-8 -*-
# application.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Application base for common application functions.
'''
import os
import subprocess
import sys
import threading

from netsplice.config import process as config
from netsplice.config import flags
from netsplice.util import get_logger
from netsplice.util.process.errors import ProcessError
from netsplice.util.process.executable_extension import (
    factory as executable_factory
)
from netsplice.util.process.location import (
    factory as location_factory
)
from netsplice.util.stream_to_log import stream_to_log
logger = get_logger()


class application(object):

    def __init__(self):
        self._name = None
        self._prefix = None
        self._arguments = []
        self._environment = None
        self._detach = False
        self._location = location_factory(sys.platform)
        self._process = None
        self._stderr_reader = None
        self._stdout_reader = None
        self._threading_lock = threading.RLock()
        self._working_directory = None

    def get_popen_arguments(self):
        '''
        Get Popen Arguments.

        Return the argument list used for popen. This builds a combination
        of the executable name in the correct location and the provided
        arguments.

        Overload if the platform requires wrapping.
        '''
        self._location.set_name(self._name)
        self._location.set_prefix(self._prefix)
        popen_arguments = []
        popen_arguments.append(self._location.get_binary_path())
        for argument in self._arguments:
            popen_arguments.append(argument)
        return popen_arguments

    def get_process_arguments(self):
        '''
        Get Process Arguments.

        Return the arguments that the process should look like in the
        tasklist.
        This method looks like duplicate of get_popen_arguments but is meant
        to be overloaded by the platforms in order to allow launchers to be
        used.

        Overload if the platform uses wrapping.
        '''
        self._location.set_name(self._name)
        self._location.set_prefix(self._prefix)
        popen_arguments = []
        popen_arguments.append(self._location.get_binary_path())
        for argument in self._arguments:
            popen_arguments.append(argument)
        return popen_arguments

    def get_popen_function(self):
        '''
        Get Popen Function.

        Return a function that has the same interface as subprocess.Popen.
        The function has to return a instance that has a 'pid' property and
        a 'terminate' function.

        Overload if the platform requires wrapping.
        '''
        return subprocess.Popen

    def get_environment(self):
        '''
        Get Environment.

        Return a dict with the os.environ values and the values provided
        by set_environment. None if no values were set.
        '''
        environment = None
        if self._environment is not None:
            environment = os.environ.copy()
            for (key, value) in self._environment.items():
                if value is None and key in environment.keys():
                    del environment[key]
                    continue
                environment[str(key)] = str(value)
        return environment

    def set_arguments(self, arguments):
        '''
        Set Arguments.

        Set the arguments for the application.
        '''
        self._arguments = arguments

    def set_environment(self, environment):
        '''
        Set Environment.

        Set a named_value_list as environment for the process.
        '''
        self._environment = dict()
        for (key, value) in environment.items():
            if value is None and key in environment.keys():
                del environment[key]
                continue
            self._environment[str(key)] = str(value)

    def set_detach(self, detach):
        '''
        Set Detach.

        Set the application detaches from this process.
        '''
        self._detach = detach

    def set_name(self, name):
        '''
        Set the name for the binary that is to be executed.
        '''
        self._name = name

    def set_prefix(self, prefix):
        '''
        Set the prefix for the binary that is to be executed.
        '''
        self._prefix = prefix

    def set_working_directory(self, working_directory):
        '''
        Set Working Directory.

        Set the working directory for the binary.
        '''
        if working_directory == '':
            self._working_directory = None
        else:
            self._working_directory = working_directory

    def start(self):
        '''
        Start.

        Start the application and return a process handle for the
        started application.
        '''
        preexec_fn = None
        startupinfo = None
        stderr = None
        stdin = None
        stdout = None
        try:
            # The following is true only on Windows.
            if hasattr(subprocess, 'STARTUPINFO'):
                # On Windows, subprocess calls will show a command window by
                # default when run from Pyinstaller with the ``--noconsole``
                # option. Avoid this distraction. The Subprocesses Backend,
                # Net, Privileged should be usable as commandline tools for
                # admins, but the enduser must not see that.
                startupinfo = subprocess.STARTUPINFO()
                startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
            if self._working_directory is not None:
                os.chdir(self._working_directory)
            if hasattr(os, 'setpgrp'):
                preexec_fn = os.setpgrp

            if not self._detach:
                stderr = subprocess.PIPE
                stdin = subprocess.PIPE
                stdout = subprocess.PIPE

            self._process = self.get_popen_function()(
                self.get_popen_arguments(),
                startupinfo=startupinfo,
                preexec_fn=preexec_fn,
                stderr=stderr,
                stdin=stdin,
                stdout=stdout,
                env=self.get_environment())

            if not self._detach:
                self._stdout_reader = stream_to_log(
                    self._process.stdout,
                    'stdout',
                    lock=self._threading_lock,
                    logger_extra=None,
                    filter=None)
                self._stderr_reader = stream_to_log(
                    self._process.stderr,
                    'stderr',
                    lock=self._threading_lock,
                    logger_extra=None,
                    filter=None)
                self._stdout_reader.start()
                self._stderr_reader.start()
            logger.info('Application %s started' % (self._name,))
        except subprocess.CalledProcessError as errors:
            # set_error(1000)
            logger.error(str(errors))
            raise ProcessError('CalledProcessError:' + str(errors))
        except OSError as errors:
            # set_error(1001)
            logger.error(str(errors))
            raise ProcessError('OSError:' + str(errors))
        return self._process
