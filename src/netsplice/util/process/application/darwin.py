# -*- coding: utf-8 -*-
# darwin.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Application for executables with the app extension.
'''
import time
import subprocess
import sys
from netsplice.util.process.application.posix import posix
from netsplice.util.process.location import (
    factory as location_factory
)

MAX_WAIT_FOR_PID = 10  # Timeout to wait for pid in seconds


class darwin(posix):

    def __init__(self):
        posix.__init__(self)

    def is_app(self):
        '''
        Is App.

        Return true if the given name is a osx bundle (.app directory with
        plist that describes the real program)
        '''
        if self._name.endswith('.app'):
            return True
        return False

    def get_app_arguments(self):
        '''
        Get App Arguments.

        Return a open wrapped argument list that is used to create the
        application.
        '''
        location = location_factory(sys.platform)
        location.set_name(self._name)
        popen_arguments = []
        popen_arguments.append('/usr/bin/open')
        popen_arguments.append('-a')
        popen_arguments.append(location.get_binary_path())
        popen_arguments.append('--args')
        for argument in self._arguments:
            popen_arguments.append(argument)
        return popen_arguments

    def get_app_process_arguments(self):
        '''
        Get App Process Arguments.

        Return the arguments that the process should look like in the
        tasklist.
        '''
        location = location_factory(sys.platform)
        location.set_name(self._name)
        popen_arguments = []
        popen_arguments.append(location.get_binary_path())
        for argument in self._arguments:
            popen_arguments.append(argument)
        return popen_arguments

    def get_process_arguments(self):
        '''
        Get Process Arguments.

        Return the arguments that the process should look like in the
        tasklist.
        '''
        if self.is_app():
            return self.get_app_process_arguments()
        return posix.get_process_arguments(self)

    def get_app_popen_function(self):
        '''
        Get App Popen function.

        Returns a Popen function pointer to the subprocess wrapper.
        '''
        return darwin_app_subprocess(self._name).Popen

    def get_popen_arguments(self):
        '''
        Get Popen arguments.

        Return app arguments for apps or posix arguments for any other
        application.
        '''
        if self.is_app():
            return self.get_app_arguments()
        return posix.get_popen_arguments(self)

    def get_popen_function(self):
        '''
        Get Popen function.

        Return asn wrapper for apps or posix arguments for any other
        application.
        '''
        if self.is_app():
            return self.get_app_popen_function()
        return posix.get_popen_function(self)


class darwin_app_subprocess(object):
    '''
    Darwin app subprocess.

    Subprocess implementation that uses lsappinfo to identify the real process
    launched in the system.
    '''
    def __init__(self, name):
        '''
        Initialize.

        Initialize members.
        '''
        self.pid = None
        self.returncode = None
        self.stderr = None
        self.stdin = None
        self.stdout = None
        self._name = name
        self._open_process = None
        self._app_asn_list = []

    def _initialize_application_list(self):
        '''
        Initialize Application List.

        Record the current application list for our name so we can find the
        new application.
        '''
        self._app_asn_list = []
        asn_finds = subprocess.check_output([
            'lsappinfo', 'find' 'bundlepath=%s' % (self._name,)])
        for asn in asn_finds.split(':'):
            if asn.strip() == '':
                continue
            self._app_asn_list.append(asn)

    def _has_process_pid(self):
        '''
        Has Process Pid.

        Check the launchd lsappinfo if a application matching our name
        appeared in the list of applications. If it has appeared, register
        its pid and return True, False when not yet available.
        '''
        check_output_arguments = [
            'lsappinfo', 'find', 'bundlepath=%s' % (self._name,)]
        asn_finds = subprocess.check_output(check_output_arguments)

        for asn in asn_finds.split('ASN:'):
            if asn.strip() == '':
                continue
            if asn in self._app_asn_list:
                # ignore asn that was available before
                # application started
                continue
            check_output_arguments = [
                'lsappinfo', 'info', 'ASN:%s' % (asn,)]
            app_info = subprocess.check_output(check_output_arguments)
            # parse app info
            # look for a line that starts 'pid = 1234'
            for line in app_info.split('\n'):
                if not line.strip().startswith('pid'):
                    continue
                li = line.strip().split(' ')
                if li[0] != 'pid':
                    continue
                if li[1] != '=':
                    continue
                self.pid = int(li[2])
                return True
        return False

    def _wait_for_process_pid(self):
        '''
        Wait for Process.

        Check if we found a pid for the started application. Sleep when
        the application was not found yet.
        '''
        for loops in range(0, MAX_WAIT_FOR_PID):
            if not self._has_process_pid():
                time.sleep(1)
                continue
            break

    def Popen(
            self, argv_list,
            stdin=None, stdout=None, stderr=None, startupinfo=None,
            preexec_fn=None, env=None):
        '''
        Process Open.

        Start a app and wait until it is up.
        '''
        has_pipes = not (stderr is None and stdin is None and stdout is None)
        if not has_pipes:
            self._initialize_application_list()
        self._open_process = subprocess.Popen(
            argv_list, stderr=stderr, stdin=stdin, stdout=stdout,
            startupinfo=startupinfo,
            preexec_fn=preexec_fn, env=env)
        if has_pipes:
            self.stderr = self._open_process.stderr
            self.stdin = self._open_process.stdin
            self.stdout = self._open_process.stdout
        else:
            self._wait_for_process_pid()
        return self

    def terminate(self):
        '''
        Terminate.

        Terminate the process that was created with Popen.
        '''
        self._open_process.terminate()

    def wait(self):
        '''
        Wait.

        Wait until the process created by Popen is finished. Set the
        returncode.
        '''
        self._open_process.wait()
        self.returncode = self._open_process.returncode
