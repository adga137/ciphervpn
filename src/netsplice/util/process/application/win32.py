# -*- coding: utf-8 -*-
# win32.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Application for executables in the startmenu.
'''
import sys
import subprocess
import time
from netsplice.util.process.application.posix import posix
from netsplice.util.process.platform import factory as platform_factory


MAX_WAIT_FOR_PID = 10  # Timeout to wait for pid in seconds


class win32(posix):

    def __init__(self):
        posix.__init__(self)

    def get_popen_function(self):
        '''
        Get App Popen function.

        Returns a Popen function pointer to the subprocess wrapper.
        '''
        return win32_app_subprocess(self._name).Popen


class win32_app_subprocess(object):
    '''
    Win32 app subprocess.

    Subprocess implementation that passes options for detached processes.
    '''
    def __init__(self, name):
        '''
        Initialize.

        Initialize members.
        '''
        self.pid = None
        self.returncode = None
        self.stderr = None
        self.stdin = None
        self.stdout = None
        self._before_process_list = []

    def _has_process_pid(self):
        '''
        Has Process Pid.

        Check the process list for something new matching our commandline,
        record the new process pid.
        '''
        platform = platform_factory(sys.platform)
        for process in platform.process_iter():
            cmdline = process.cmdline()
            if len(cmdline) is 0:
                continue
            pid = process.pid
            if cmdline[0] != self._argv_list[0]:
                continue
            was_before = False
            for before_process in self._before_process_list:
                before_cmdline = before_process.cmdline()
                before_pid = before_process.pid
                if before_cmdline[0] != self._argv_list[0]:
                    continue
                if before_pid == pid:
                    was_before = True
                    break
                continue
            if was_before:
                continue
            self.pid = pid
            return True
        return False

    def _is_script(self, argv_list):
        '''
        Is Script.

        Evaluate if the command in the argv list is a script to use
        shell=True on Popen.
        '''
        is_script = False
        is_script |= argv_list[0].endswith('.cmd')
        is_script |= argv_list[0].endswith('.bat')
        is_script |= argv_list[0].endswith('.ps1')
        is_script |= argv_list[0].endswith('.ps2')
        return is_script

    def _wait_for_process_pid(self):
        '''
        Wait for Process.

        Check if we found a pid for the started application. Sleep when
        the application was not found yet.
        '''
        time.sleep(3)
        for loops in range(0, MAX_WAIT_FOR_PID):
            if not self._has_process_pid():
                time.sleep(1)
                continue
            break

    def Popen(
            self, argv_list,
            stdin=None, stdout=None, stderr=None, startupinfo=None,
            preexec_fn=None, env=None):
        '''
        Process Open.

        Start a app and wait until it is up.
        Handle the win32 specifics to have a detached process when no
        pipes are attached.
        '''
        import win32process
        creationflags = win32process.DETACHED_PROCESS
        platform = platform_factory(sys.platform)
        is_script = self._is_script(argv_list)
        has_pipes = not (stderr is None and stdin is None and stdout is None)
        popen_argv = argv_list
        if is_script:
            popen_argv = argv_list[0]
            creationflags = 0
        if has_pipes:
            # Do not detach when pipes are requested.
            creationflags = 0
        self._before_process_list = platform.process_iter()
        self._argv_list = platform.clean_arguments(argv_list)
        self._open_process = subprocess.Popen(
            popen_argv, stderr=stderr, stdin=stdin, stdout=stdout,
            startupinfo=startupinfo, preexec_fn=preexec_fn, env=env,
            close_fds=(not has_pipes), creationflags=creationflags,
            shell=is_script)
        self.pid = self._open_process.pid
        if has_pipes:
            self.stderr = self._open_process.stderr
            self.stdin = self._open_process.stdin
            self.stdout = self._open_process.stdout
        if not is_script and not has_pipes:
            self._wait_for_process_pid()
        return self

    def terminate(self):
        '''
        Terminate.

        Terminate the process that was created with Popen.
        '''
        self._open_process.terminate()

    def wait(self):
        '''
        Wait.

        Wait until the process created by Popen is finished. Set the
        returncode.
        '''
        self._open_process.wait()
        self.returncode = self._open_process.returncode
