# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Platform methods for wrappers that return the expected values for the main
code.
Factory for getting the appropriate platform dependent module.
'''
from netsplice.util.process.platform.posix import (
    posix as posix_platform
)
from netsplice.util.process.platform.win32 import (
    win32 as win32_platform
)


def factory(sys_platform):
    '''
    Takes the given sys_platform (should be sys.platform format) and
    returns the executable filename. Assumes a posix environment unless
    the given platform needs something special.
    '''
    if sys_platform.startswith('win32'):
        return win32_platform()
    return posix_platform()
