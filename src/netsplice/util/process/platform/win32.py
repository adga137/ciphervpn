# -*- coding: utf-8 -*-
# win32.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Modify given argv list so it the argv including executable_name is passed to
popen. dispatcher.start() will execute this list.

Command is executed as-is, the executable has the uac_info flag.
Wraps the command into a command that is executed after the UAC dialog was
confirmed.
'''
import netsplice.config.process as config
from ctypes import (
    byref, c_buffer, c_int, c_ulong, c_long, c_wchar, sizeof, Structure
)
import time
import os
import psutil
import sys

from netsplice.util.process.platform.posix import posix
from netsplice.util.process.errors import ProcessError

MAX_PATH = 260
PROCESS_QUERY_LIMITED_INFORMATION = 0x1000
TH32CS_SNAPPROCESS = 0x02

kernel32 = None
psapi = None
windll = None

if sys.platform.startswith('win32'):
    from ctypes import windll
    kernel32 = windll.kernel32
    psapi = windll.psapi


class PROCESSENTRY32(Structure):
    _fields_ = [
        ('dwSize', c_ulong),
        ('cntUsage', c_ulong),
        ('th32ProcessID', c_ulong),
        ('th32DefaultHeapID', c_int),
        ('th32ModuleID', c_ulong),
        ('cntThreads', c_ulong),
        ('th32ParentProcessID', c_ulong),
        ('pcPriClassBase', c_long),
        ('dwFlags', c_ulong),
        ('szExeFile', c_wchar * MAX_PATH)
    ]


class psutil_process_override(object):
    def __init__(self, pid, cmdline):
        self.pid = pid
        self._process = psutil.Process(self.pid)
        self._cmdline = cmdline

    def ppid(self):
        return self._process.ppid()

    def cmdline(self):
        return self._cmdline

    def kill(self):
        return self._process.kill()

    def name(self):
        if len(self._cmdline) > 0:
            return self._cmdline[0]
        return ''

    def terminate(self):
        return self._process.terminate()

    def username(self):
        return self._process.username()

    def wait(self, timeout=0):
        return self._process.wait(timeout)


class win32(posix):

    def __init__(self):
        '''
        Initialize.

        No members required.
        '''
        pass

    def _get_pid_list(self):
        '''
        Get PID List.

        Enumerate the running processes and return a list of pid's.
        '''
        # Allocate 16 block c_ulongs until the process list fits into the
        # buffer.
        all_pids_enumerated = False
        block = 1
        pid_buffer_needed = c_ulong()
        pid_processes = []

        while all_pids_enumerated is False:
            arr = c_ulong * (16 * block)
            pid_buffer = arr()
            pid_buffer_size = sizeof(pid_buffer)
            psapi.EnumProcesses(
                byref(pid_buffer), pid_buffer_size,
                byref(pid_buffer_needed))

            if pid_buffer_needed.value < pid_buffer_size:
                all_pids_enumerated = True
                for pid in pid_buffer:
                    pid_processes.append(pid)
                break
            block += 1

        return pid_processes

    def _get_process_map(self):
        '''
        Get Process Map.

        Alternative implementation to get a pid-cmd map. used as fallback
        when psutil returns empty cmdline.
        inspired by:
        http://code.activestate.com/recipes/305279 / Eric Koome
        '''
        process_name = c_buffer(4096)
        process_map = dict()

        # Get path to process executable for every PID that is owned by the
        # user.
        for pid in self._get_pid_list():
            # Get handle to the process based on PID
            process_handle = kernel32.OpenProcess(
                PROCESS_QUERY_LIMITED_INFORMATION, False, pid)
            if not process_handle:
                process_map[str(pid)] = []
                continue
            process_name_length = psapi.GetProcessImageFileNameA(
                process_handle, byref(process_name), len(process_name))
            if process_name_length is 0:
                process_map[str(pid)] = []
                continue
            cmdline = ''.join([i for i in process_name if ord(i) != 0])
            process_map[str(pid)] = [cmdline.lower()]

            # cleanup
            for i in range(len(process_name)):
                process_name[i] = '\x00'

            kernel32.CloseHandle(process_handle)
        return process_map

    def clean_arguments(self, unclean_list):
        '''
        Clean Arguments.

        Clean Arguments for popen.
        '''
        clean_list = list()
        for arg in unclean_list:
            clean_list.append(arg.lower())
        return clean_list

    def getppid(self):
        '''
        Get Parent Process Id.

        Windows implementation of os.getppid.
        Should not be required in python3.

        :return: The pid of the parent of this process.
        '''
        _kernel32dll = windll.Kernel32
        CloseHandle = _kernel32dll.CloseHandle
        CreateToolhelp32Snapshot = kernel32.CreateToolhelp32Snapshot
        GetCurrentProcessId = kernel32.GetCurrentProcessId
        Process32First = _kernel32dll.Process32FirstW
        Process32Next = _kernel32dll.Process32NextW

        result = 0
        process_entry = PROCESSENTRY32()
        process_entry.dwSize = sizeof(PROCESSENTRY32)
        my_pid = GetCurrentProcessId()
        snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0)

        try:
            have_record = Process32First(snapshot, byref(process_entry))

            while have_record:
                if my_pid == process_entry.th32ProcessID:
                    result = process_entry.th32ParentProcessID
                    break

                have_record = Process32Next(snapshot, byref(process_entry))

        finally:
            CloseHandle(snapshot)

        return result

    def process_iter(self):
        '''
        Process Iteration.

        Override psutil.process_iter with a implementation that uses the
        win32 python api to query process details.
        '''
        proc_list = list()
        proc_map = self._get_process_map()
        for pid in proc_map.iterkeys():
            try:
                proc_list.append(
                    psutil_process_override(
                        pid=int(pid), cmdline=proc_map[pid]))
            except psutil.NoSuchProcess:
                continue
        return proc_list

    def username(self):
        '''
        Username.

        Return user from environment.
        '''
        return os.environ.get('USERNAME')
