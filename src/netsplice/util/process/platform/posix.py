# -*- coding: utf-8 -*-
# posix.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Platform for POSIX systems.
'''
import os
import psutil

from netsplice.util.process.platform.base import base


class posix(base):
    '''
    Posix Platform.

    Methods that have overloads for other platforms with their default
    posix python implementation
    '''

    def __init__(self):
        '''
        Initialize.

        No members required.
        '''
        pass

    def clean_arguments(self, unclean_list):
        '''
        Clean Arguments.

        Clean Arguments for popen.
        '''
        clean_list = list()
        clean_list.extend(unclean_list)
        return clean_list

    def getppid(self):
        '''
        Get Parent Process Id.

        Return the Parent Process Id.
        '''
        return os.getppid()

    def process_iter(self):
        '''
        Process Iterator.

        Return a list of processes that can be iterated.
        '''
        return psutil.process_iter()

    def username(self):
        '''
        Username.

        Return user from environment.
        '''
        return os.environ.get('USER')
