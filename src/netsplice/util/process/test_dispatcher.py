import mock
import os
import sys
import io

from .dispatcher import dispatcher

import netsplice.config.process as config

from netsplice.model.process import process as process_model
from netsplice.util.errors import NotInitializedError


class MockError(Exception):
    def __init__(self):
        Exception.__init__(self)


def mock_popenmock_popen_process(*args, **kwargs):
    class process_mock(object):
        def __init__(self):
            self.stdout = io.StringIO()
            self.stderr = io.StringIO()
    return process_mock()


def test_source_constructor_basic_no_exception():
    config.execute_source = True
    d = dispatcher(
        'some-executable-name', {}, model=process_model())
    assert(True)  # No exception generated


def test_source_constructor_options_no_exception():
    config.execute_source = True
    d = dispatcher(
        'some-executable-name', {'--option': 'value'}, model=process_model())
    assert(True)  # No exception generated


def test_source_constructor_elevator_no_exception():
    config.execute_source = True
    d = dispatcher(
        'some-executable-name', {}, elevated=True, model=process_model())
    assert(True)  # No exception generated


def test_prod_constructor_basic_no_exception():
    config.execute_source = False
    d = dispatcher(
        'some-executable-name', {}, model=process_model())
    assert(True)  # No Error raised


def test_prod_constructor_options_no_exception():
    config.execute_source = False
    d = dispatcher(
        'some-executable-name', {'--option': 'value'}, model=process_model())
    assert(True)  # No Error raised


def test_prod_constructor_elevator_no_exception():
    config.execute_source = False
    d = dispatcher(
        'some-executable-name', {}, elevated=True, model=process_model())
    assert(True)  # No Error raised


def test_get_subprocess_not_started_raises_error():
    try:
        d = dispatcher(
            'some-executable-name', {}, elevated=True, model=process_model())
        d.get_subprocess()
        assert(False)
    except NotInitializedError:
        assert(True)
    except:
        assert(False)  # Unknown Error raised


def test_get_subprocess_started_returns_instance():
    try:
        d = dispatcher(
            'some-executable-name', {}, elevated=True, model=process_model())
        d._process = True
        assert(d.get_subprocess() is True)
    except:
        assert(False)  # Unknown Error raised


@mock.patch('netsplice.util.logger')
@mock.patch(
    'netsplice.util.process.dispatcher.subprocess.Popen',
    side_effect=mock_popenmock_popen_process
)
@mock.patch(
    'netsplice.util.process.dispatcher.os.path.exists',
    return_value=True
)
def test_start_subprocess_source_adds_py_to_arguments(
        mock_exists, mock_popen, mock_logger):
    config.execute_source = True
    d = dispatcher(
        'NetspliceBackendApp', {}, model=process_model())
    d.start_subprocess()
    script = os.path.dirname(os.path.realpath(__file__))
    module_path = os.path.join('util', 'process',)
    script_path = script.replace(module_path, '')

    expected_executable = os.path.join(
        script_path,
        'NetspliceBackendApp.py')
    name, args, kwargs = mock_popen.mock_calls[0]
    print('name: %s, args: %s, kwargs: %s' % (name, args, kwargs))
    print('expected: %s in args' % (expected_executable,))
    if expected_executable in args[0]:
        assert(True)
    else:
        assert(False)


@mock.patch('netsplice.util.logger')
@mock.patch(
    'netsplice.util.process.dispatcher.subprocess.Popen',
    side_effect=MockError
)
@mock.patch(
    'netsplice.util.process.dispatcher.os.path.exists',
    return_value=True
)
def test_start_subprocess_prod_does_not_add_py_to_arguments(
        mock_exists, mock_popen, mock_logger):
    config.execute_source = False
    d = dispatcher(
        'NetspliceBackendApp', {}, model=process_model())
    try:
        d.start_subprocess()
    except MockError:
        expected_executable = os.path.join(
            os.path.dirname(sys.executable),
            'NetspliceBackendApp')
        print('expected_executable', expected_executable)
        mock_popen.assert_called_with([
            expected_executable],
            env=None, preexec_fn=None, startupinfo=None,
            stderr=-1, stdin=-1, stdout=-1)
        assert(True)
    except Exception as e:
        print(e)
        assert(False)  # Unknown Error Raised
