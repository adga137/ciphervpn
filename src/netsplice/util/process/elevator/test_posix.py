# -*- coding: utf-8 -*-
# test_posix.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

from netsplice.util.process.elevator.posix import posix
import netsplice.config.process as config


def get_test_object():
    return posix()


def test_modify_argv_list_simple_returns_list():
    o = get_test_object()
    config.execute_source = False
    config.GRAPHICAL_SUDO_APPLICATION = 'pkexec'
    result = o.modify_argv_list(['executable', 'arg1', 'arg2'])
    print(result)
    assert(isinstance(result, (list,)))
    assert('pkexec' in result)
    assert('executable' in result)
    assert('arg1' in result)
    assert('arg2' in result)
    config.execute_source = True


def test_modify_argv_list_complex_sudo_returns_list():
    o = get_test_object()
    config.execute_source = False
    config.GRAPHICAL_SUDO_APPLICATION = ['pkexec', '-i']
    result = o.modify_argv_list(['executable', 'arg1', 'arg2'])
    print(result)
    assert(isinstance(result, (list,)))
    assert('pkexec' in result)
    assert('-i' in result)
    assert('executable' in result)
    assert('arg1' in result)
    assert('arg2' in result)
    config.execute_source = True


def test_modify_argv_list_execute_from_source_returns_same_list():
    o = get_test_object()
    config.execute_source = True
    config.GRAPHICAL_SUDO_APPLICATION = 'pkexec'
    result = o.modify_argv_list(['executable', 'arg1', 'arg2'])
    print(result)
    assert(isinstance(result, (list,)))
    assert('pkexec' not in result)
    assert('executable' in result)
    assert('arg1' in result)
    assert('arg2' in result)
    config.execute_source = True
