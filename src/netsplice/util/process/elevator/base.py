# -*- coding: utf-8 -*-
# elevator_base.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Elevator Base.

Define interface for all elevators.
'''
import subprocess


class base(object):
    '''
    Elevator Base.

    Interface implementation for all elevators.
    '''

    def __init__(self):
        '''
        Initialize.

        No members required.
        '''
        pass

    def modify_argv_list(self, popen_argv_list):
        '''
        Modify Popen Arguments.

        Modify the given popen_argv_list in a way that causes elevation.
        '''
        return popen_argv_list

    def get_process_constructor(self):
        '''
        Get Process constructor.

        Return a function reference compatible with subprocess.Popen.
        '''
        return subprocess.Popen

    def get_process_wait_constructor(self):
        '''
        Get Process constructor.

        Return a function reference compatible with subprocess.check_output.
        '''
        return subprocess.check_output
