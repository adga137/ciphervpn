# -*- coding: utf-8 -*-
# test_darwin.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

from netsplice.util.process.elevator.darwin import darwin


def get_test_object():
    return darwin()


def test__quote_simple_returns_string():
    o = get_test_object()
    result = o._quote(['a', 'b', 'c'])
    print(result)
    assert(result == '"\\"a\\" \\"b\\" \\"c\\""')


def test__quote_singlequotes_returns_string():
    o = get_test_object()
    result = o._quote(['a', 'b\'x', 'c'])
    print(result)
    assert(result == '"\\"a\\" \\"b\\\'x\\" \\"c\\""')


def test__quote_doublequotes_returns_string():
    o = get_test_object()
    result = o._quote(['a', 'b"x', 'c'])
    print(result)
    assert(result == '"\\"a\\" \\"b\\\\"x\\" \\"c\\""')


def test__quote_newlines_returns_string():
    o = get_test_object()
    result = o._quote(['a', 'b\nx', 'c'])
    print(result)
    assert(result == '"\\"a\\" \\"b\\nx\\" \\"c\\""')


def test_modify_argv_list_simple_returns_list():
    o = get_test_object()
    result = o.modify_argv_list(['executable', 'arg1', 'arg2'])
    print(result)
    assert(isinstance(result, (list,)))
    assert('osascript' in result)
    assert('-e' in result)
    assert(
        ('do shell script "\\"executable\\" \\"arg1\\" \\"arg2\\""'
        ' with prompt "Netsplice needs administrative permissions to'
        ' execute executable."'
        ' with administrator privileges') in result)
