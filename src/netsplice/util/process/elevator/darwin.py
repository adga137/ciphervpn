# -*- coding: utf-8 -*-
# darwin.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Modify given argv list so it the argv including executable_name is passed to
popen. dispatcher.start() will execute this list.

Wraps the command in a osascript so the user gets prompted for admin rights.
Tries to avoid the obvious injections.
'''

from netsplice.util.process.elevator.base import base
from netsplice.util import get_logger
import os
import stat

logger = get_logger()


class darwin(base):

    def __init__(self):
        '''
        Initialize.

        No members required.
        '''
        base.__init__(self)

    def _quote(self, popen_argv_list):
        '''
        Quote.

        Create a string suitable for osastring expression from the
        given popen_argv_list.
        '''
        quoted = ''
        quoted_args = []
        for arg in popen_argv_list:
            quoted_arg = arg.replace('"', '\\\\"').replace('\'', '\\\'')
            quoted_args.append('\\"%s\\"' % (quoted_arg,))

        quoted = ' '.join(quoted_args)
        quoted = quoted.replace('\n', '\\n')
        return '"%s"' % (quoted,)

    def modify_argv_list(self, popen_argv_list):
        '''
        Modify Popen Arguments.

        Convert the popen_argv_list to string, escaping the quotes and
        wrap into a osascript expression.
        When the NetsplicePrivilegedApp is elevated, the osascript will
        set the setuid/setgid bit and change the ownership to root:wheel.
        This reduces the dialog to be displayed only once after installation.
        '''
        if 'NetsplicePrivilegedApp' in popen_argv_list[0]:
            # check if the executable has the setuid bit
            stat_info = os.stat(popen_argv_list[0])
            owned_by_root = stat_info.st_uid == 0 and stat_info.st_gid == 0
            has_suid = stat_info.st_mode & stat.S_ISUID
            has_guid = stat_info.st_mode & stat.S_ISGID
            is_elevated = owned_by_root and has_suid and has_guid
            if is_elevated:
                # we have set the bits in a previous run, do not
                # modify the argv list then
                return popen_argv_list

            modified_popen_argv_list = ['osascript', '-e']
            quoted_argv = self._quote(popen_argv_list)
            osascript_chown = (
                'do shell script "chown root:wheel \\"{executable_name}\\""'
                ' with prompt "Netsplice needs administrative permissions to'
                ' configure interfaces and routes."'
                ' with administrator privileges\n').format(
                    executable_name=popen_argv_list[0])
            osascript_chmod = (
                'do shell script "chmod +s \\"{executable_name}\\""'
                ' with prompt "Netsplice needs administrative permissions to'
                ' configure interfaces and routes."'
                ' with administrator privileges\n').format(
                    executable_name=popen_argv_list[0])
            osascript_app = (
                'do shell script {quoted_argv}').format(
                    quoted_argv=quoted_argv)
            modified_popen_argv_list.append(osascript_chown)
            modified_popen_argv_list.append('-e')
            modified_popen_argv_list.append(osascript_chmod)
            modified_popen_argv_list.append('-e')
            modified_popen_argv_list.append(osascript_app)
            print(modified_popen_argv_list)
            return modified_popen_argv_list

        # Anything that is not the PrivilegedApp needs a permission
        # every time it is elevated
        modified_popen_argv_list = ['osascript', '-e']
        quoted_argv = self._quote(popen_argv_list)
        osascript = (
            'do shell script {quoted_argv}'
            ' with prompt "Netsplice needs administrative permissions to'
            ' execute {executable_name}."'
            ' with administrator privileges').format(
                executable_name=popen_argv_list[0],
                quoted_argv=quoted_argv)
        modified_popen_argv_list.append(osascript)
        return modified_popen_argv_list
