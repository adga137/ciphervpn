# -*- coding: utf-8 -*-
# win32.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Modify given argv list so it the argv including executable_name is passed to
popen. dispatcher.start() will execute this list.

Command is executed as-is, the executable has the uac_info flag.
Wraps the command into a command that is executed after the UAC dialog was
confirmed.
'''
import netsplice.config.process as config
import ctypes
import time
from netsplice.util.process.elevator.base import base
from netsplice.util.process.errors import ProcessError


class win32(base):

    def __init__(self):
        '''
        Initialize.

        No members required.
        '''
        pass

    def _is_admin(self):
        '''
        Is Admin.

        Return True if the user is a admin.
        '''
        return ctypes.windll.shell32.IsUserAnAdmin()

    def get_process_constructor(self):
        '''
        Get Process constructor.

        Return a function reference compatible with Popen.
        '''
        return win32_elevated_subprocess().Popen


class blocking_stream(object):
    '''
    Blocking Stream.

    Mock for stderr/stdout for elevated process.
    '''
    def __init__(self):
        '''
        Initialize.

        Do Nothing.
        '''
        pass

    def readline(self, n=-1):
        '''
        Readline.

        Return empty but wait to avoid wasting cpu resources.
        '''
        time.sleep(1)
        return ''

    def close(self):
        '''
        Close.

        Do Nothing.
        '''
        pass


class win32_elevated_subprocess(object):
    '''
    Win32 elevated subprocess.

    Subprocess implementation that uses win32 functions to start a
    process with elevated rights.
    '''
    def __init__(self):
        '''
        Initialize.

        Initialize members.
        '''
        self.stdout = blocking_stream()
        self.stderr = blocking_stream()
        self._proc_info = None

    def Popen(
            self, argv_list,
            stdin=None, stdout=None, stderr=None, startupinfo=None,
            preexec_fn=None, env=None):
        '''
        Process Open.

        Start a process with elevated rights.
        '''
        import win32con
        from win32com.shell.shell import ShellExecuteEx
        from win32com.shell import shellcon
        import pywintypes
        if type(argv_list) not in (list,):
            raise ValueError('argv_list is not a list.')
        cmd = '"%s"' % (argv_list[0],)
        params = " ".join(['"%s"' % (x,) for x in argv_list[1:]])
        show_cmd = win32con.SW_HIDE
        lp_verb = 'runas'  # causes UAC elevation prompt.
        try:
            self._proc_info = ShellExecuteEx(
                nShow=show_cmd,
                fMask=shellcon.SEE_MASK_NOCLOSEPROCESS,
                lpVerb=lp_verb,
                lpFile=cmd,
                lpParameters=params)
        except pywintypes.error as errors:
            raise ProcessError(str(errors))
        return self

    def terminate(self):
        '''
        Terminate.

        Terminate the process that was created with Popen.
        '''
        from win32process import TerminateProcess
        if self._proc_info is not None:
            TerminateProcess(self._proc_info['hProcess'], 0)
