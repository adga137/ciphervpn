# -*- coding: utf-8 -*-
# reaper.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Process Reaper.

Kill Processes.

Death, also known as the Grim Reaper, is a common element in culture and
history. As a personified force it has been imagined in many different ways.
In some mythologies, the Grim Reaper causes the victim's death by coming to
collect him. In turn, people in some stories try to hold on to life by
avoiding Death's visit, or by fending Death off with bribery or tricks.
'''

import os
import psutil
import sys
import netsplice.config.process as config

from netsplice.config import flags
from netsplice.util import get_logger

from netsplice.util.process.errors import ProcessError
from netsplice.util.process.platform import factory as platform_factory

logger = get_logger()


class reaper(object):
    '''
    Reaper.

    Methods that are used to kill processes.
    '''

    def __init__(self):
        '''
        Initialize.

        Initialize members from options.
        '''
        if flags.DEBUG:
            logger.debug('Process reaper')
        self.execute_source = config.execute_source
        self.pid = os.getpid()
        self.parent_pid = platform_factory(sys.platform).getppid()

    def find_children(self):
        '''
        Find Children.

        Find processes that have a parent-pid that matches mine.
        '''
        children = []
        self.pid = os.getpid()
        # use psutil.process_iter on all platforms to receive arguments
        for proc in psutil.process_iter():
            if proc.ppid() != self.pid:
                continue
            try:
                cmdline = proc.cmdline()
                if cmdline[0] == '':
                    continue
                children.append(proc)
            except psutil.ZombieProcess:
                continue
            except psutil.AccessDenied:
                continue
        return children

    def find_subprocess(self, popen_arguments):
        '''
        Find subprocess.

        Find processes that match the given popen_arguments.
        '''
        processes = []
        stripped_arguments = []
        platform = platform_factory(sys.platform)
        if self.execute_source and sys.executable not in popen_arguments[0]:
            stripped_arguments.append(sys.executable.split(os.path.sep)[-1])
        for arg in popen_arguments:
            stripped_arguments.append(arg.split(os.path.sep)[-1])
        stripped_arguments = platform.clean_arguments(
            stripped_arguments)
        # use psutil.process_iter on all platforms to receive arguments
        for proc in psutil.process_iter():
            # check whether the process name matches
            try:
                cmdline = proc.cmdline()
            except psutil.ZombieProcess:
                continue
            except psutil.AccessDenied:
                continue
            except psutil.NoSuchProcess:
                continue
            if len(cmdline) < 2:
                continue
            cmdline = platform.clean_arguments(cmdline)
            executable_matches = stripped_arguments[0] in cmdline[0]
            arguments_match = False
            if self.execute_source and len(cmdline) > 1:
                executable_matches = stripped_arguments[1] in cmdline[1]
            if not executable_matches:
                if flags.VERBOSE > 5:
                    logger.warn(
                        'Executables do not match: %s' % (str(cmdline),))
                executable_matches = False
                continue
            arguments_match = len(stripped_arguments) == len(cmdline)
            if not arguments_match:
                if flags.VERBOSE > 5:
                    logger.warn(
                        'Parameter count does not match: %d / %d'
                        % (len(stripped_arguments), len(cmdline)))
                arguments_match = False
                continue
            arguments_match = True
            for parg, carg in zip(stripped_arguments, cmdline):
                if parg not in carg:
                    if flags.VERBOSE > 5:
                        logger.warn(
                            'Parameter does not match: %s / %s'
                            % (parg, carg))
                    arguments_match = False
                    break
            if not arguments_match:
                continue
            logger.warn('found: %d %s' % (proc.pid, str(cmdline)))
            processes.append(proc)
        return processes

    def kill_children(self):
        '''
        Kill children.

        Try to kill all children of mine.
        Return False when a child could not get killed (zombie).
        '''
        has_zombies = False
        children = self.find_children()
        for proc in children:
            try:
                proc.kill()
            except Exception as errors:
                has_zombies = True
                logger.critical(
                    'Failed to kill %d %s (%s).'
                    % (proc.pid, str(errors), type(errors)))
        return has_zombies

    def kill_other_subprocess(self, popen_arguments):
        '''
        Try to kill other Subprocesses.

        When a matching process is found but cannot be killed (permission)
        an exception is raised.
        Returns True when a process was found and killed, False otherwise.
        '''
        killed = False

        for proc in self.find_subprocess(popen_arguments):
            if proc.pid == self.pid:
                # no suicide

                continue
            if proc.pid == self.parent_pid:
                # no ancestor
                continue
            username = None
            cmdline = []
            ppid = 0
            try:
                username = proc.username()
                cmdline = proc.cmdline()
                ppid = proc.ppid()

            except psutil.ZombieProcess:
                continue
            except psutil.AccessDenied:
                continue
            except psutil.NoSuchProcess:
                continue
            if ppid == self.pid:
                # no own children
                continue

            logger.warn(
                'Found other executable: pid: %d ppid: %d'
                ' self.pid: %d self.parent_pid: %d cmdline: %s'
                % (proc.pid, ppid, self.pid, self.parent_pid, str(cmdline),))
            try:
                proc.terminate()
                proc.wait(config.KILL_TIMEOUT)
                logger.info(
                    'Terminated: %d %s'
                    % (proc.pid, str(cmdline)))
                killed = True
            except psutil.TimeoutExpired:
                logger.warn(
                    'SIGTERM received timeout, killing: %d'
                    % (proc.pid,))
                proc.kill()
                logger.info(
                    'Killed after timeout: %d %s'
                    % (proc.pid, str(cmdline)))
                killed = True
            except psutil.NoSuchProcess:
                logger.warn('No Such Process')
            except psutil.AccessDenied:
                logger.critical(
                    'Cannot ensure unique process, same process exists with'
                    ' elevated privileges: %s / User: %s'
                    % (str(cmdline), username))
            except Exception as errors:
                logger.critical(
                    'Exception: %s %s'
                    % (str(errors), type(errors)))

        return killed

    def kill_pid(self, pid):
        '''
        Kill PID.

        Kill the process with the given pid. This is used by the backend to
        kill processes that have elevated execution rights.
        Returns True when the process was killed, False otherwise.
        '''
        try:
            proc = psutil.Process(pid)
            proc.kill()
            return True
        except psutil.ZombieProcess:
            logger.warn('Zombie proccess failed to kill.')
        except psutil.NoSuchProcess:
            logger.warn('Process with pid %d was not found.' % (pid,))
        except psutil.AccessDenied:
            logger.critical(
                'Cannot kill process %d, it has more privileges.'
                % (pid,))
        return False

    def kill_self(self):
        '''
        Kill Self.

        Kill the current process. This method should only be used as a last
        resort when other methods of shutting down failed.
        '''
        self.kill_pid(os.getpid())
