# -*- coding: utf-8 -*-
# utils.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import mock
import os
import sys

import logbook
from .logger_model_handler import logger_model_handler
from ..config import log as config


def test_emit_adds_entry_to_logs():
    # Check that a record ends up in the log
    record = logbook.LogRecord('Channel', logbook.DEBUG, 'Message')
    record.heavy_init()
    l = logger_model_handler()
    l.emit(record)
    result = len(l._log)
    expected = 1
    print expected, result
    assert(result == expected)


def test_emit_ensures_buffer_size():
    # Check that a given buffer_size is enforced on emit.
    l = logger_model_handler(buffer_size=4)
    for i in range(0, 10):
        record = logbook.LogRecord('Channel', logbook.DEBUG, 'Message')
        record.heavy_init()
        l.emit(record)
    result = len(l._log)
    expected = 4
    print expected, result
    assert(expected == result)


def test_take_logs_clears_buffer():
    # Check that take_logs clears the current buffer.
    record = logbook.LogRecord('Channel', logbook.DEBUG, 'Message')
    record.heavy_init()
    l = logger_model_handler()
    l.emit(record)
    l.take_logs()
    result = len(l._log)
    expected = 0
    print expected, result
    assert(expected == result)
