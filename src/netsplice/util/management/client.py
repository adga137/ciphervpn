# -*- coding: utf-8 -*-
# client.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Management client
'''

import socket
import threading

from .channel import channel

VERBOSE = 0


class client(channel):
    '''
    Public client interface that exposes the channels to the application.
    '''
    def __init__(self, host, port, handler):
        self.create_socket()
        self.set_address(host, port)
        self._socket.connect(self._address)
        self._handler = handler
        self._handler_thread = threading.Thread(target=self._init_handler)
        self._handler_thread.daemon = True
        self._handler_thread.start()
        if VERBOSE > 0:
            print('management client initialized: %s %d' % (host, port))

    def _init_handler(self):
        self._handler(self._socket)
