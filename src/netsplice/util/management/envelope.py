# -*- coding: utf-8 -*-
# envelope.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Management message envelopes
'''

import json


class envelope(object):

    separator = '\n\n'

    def __init__(self, data={}):
        self._data = {}
        self._string = ''
        self.encode(data)

    def __repr__(self):
        return self._string

    def encode(self, data={}):
        # unittest
        self._data = data
        self._string = json.dumps(data)
        return self._string

    def decode(self, string_data):
        # unittest
        messages = string_data.split(envelope.separator)
        if len(messages) < 2:
            raise BufferError('Message incomplete')
        self._string = messages[0]
        self._data = json.loads(self._string)
        return self._data

    def get_data(self):
        return self._data


class envelope_stream(object):

    def __init__(self):
        self._unprocessed = []
        self.envelopes = []

    def append(self, string_data):
        self._unprocessed.append(string_data)
        self.process()

    def consume(self, envelope):
        # unittest
        unprocessed = ''.join(self._unprocessed)
        envelope_string = "%s%s" % (str(envelope), envelope.separator)
        if envelope_string not in unprocessed:
            return
        envelope_position = unprocessed.find(envelope_string)
        unprocessed = "%s%s" % (unprocessed[0:envelope_position],
                                unprocessed[envelope_position +
                                len(envelope_string):len(unprocessed)])
        self._unprocessed = []
        self._unprocessed.append(unprocessed)

    def process(self):
        try:
            stream_envelope = envelope()
            unprocessed = ''.join(self._unprocessed)
            stream_envelope.decode(unprocessed)
            self.envelopes.append(stream_envelope)
            self.consume(stream_envelope)
        except BufferError:
            # print('message incomplete')
            pass
        except ValueError:
            print('could not decode')
            self.consume('')
        except Exception as errors:
            print('general exception', errors)
            raise

    def has_envelopes(self):
        return len(self.envelopes) > 0

    def take_envelope(self):
        envelope = self.envelopes[0]
        del self.envelopes[0]
        return envelope
