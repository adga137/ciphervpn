import mock

from .stream_to_log import stream_to_log

import netsplice.config.process as config
import netsplice.config.flags as flags


class mock_fd(object):
    def __init__(self):
        self.close_called = False
        self.readline_called = False
        self.readline_return_value = ''
        self.readline_return_value_index = 0

    def close(self):
        self.close_called = True

    def readline(self):
        self.readline_called = True
        if len(self.readline_return_value) <= self.readline_return_value_index:
            return ''
        retval = self.readline_return_value[self.readline_return_value_index]
        self.readline_return_value_index += 1
        return retval


class mock_lock(object):
    def __init__(self):
        pass


def test_stream_to_log_with_filter_sets_filter():
    fd = mock_fd()
    fd_name = 'test_fd_name'
    lock = mock_lock()
    logger_extra = {}
    filter = ['test_filter']
    unit = stream_to_log(
        fd, fd_name, lock, logger_extra, filter)
    assert(unit._filter == ['test_filter'])


def test_stream_to_log_without_filter_sets_empty_filter():
    fd = mock_fd()
    fd_name = 'test_fd_name'
    lock = mock_lock()
    logger_extra = {}
    filter = None
    unit = stream_to_log(
        fd, fd_name, lock, logger_extra, filter)
    assert(unit._filter == [])


def test_stream_to_log_with_lock_sets_lock():
    fd = mock_fd()
    fd_name = 'test_fd_name'
    lock = mock_lock()
    logger_extra = {}
    filter = None
    unit = stream_to_log(
        fd, fd_name, lock, logger_extra, filter)
    assert(unit._lock == lock)


def test_stream_to_log_without_lock_sets_new_lock():
    fd = mock_fd()
    fd_name = 'test_fd_name'
    lock = None
    logger_extra = {}
    filter = None
    unit = stream_to_log(
        fd, fd_name, lock, logger_extra, filter)
    assert(unit._lock is not None)


@mock.patch('netsplice.util.stream_to_log.logger.info')
def test_run_if_stopped_does_not_log(mock_logger):
    fd = mock_fd()
    fd.readline_return_value = ['not_logged']
    fd_name = 'test_fd_name'
    lock = None
    logger_extra = {}
    filter = None
    unit = stream_to_log(
        fd, fd_name, lock, logger_extra, filter)
    unit._stopped = True
    unit.run()
    mock_logger.assert_not_called()


@mock.patch('netsplice.util.stream_to_log.logger.info')
def test_run_does_not_log_empty_lines(mock_logger):
    fd = mock_fd()
    fd.readline_return_value = ['']
    fd_name = 'test_fd_name'
    lock = None
    logger_extra = {}
    filter = None
    unit = stream_to_log(
        fd, fd_name, lock, logger_extra, filter)
    unit.run()
    mock_logger.assert_not_called()


@mock.patch('netsplice.util.stream_to_log.logger.info')
def test_run_does_not_log_trim_empty_lines(mock_logger):
    fd = mock_fd()
    fd.readline_return_value = ['  ']
    fd_name = 'test_fd_name'
    lock = None
    logger_extra = {}
    filter = None
    unit = stream_to_log(
        fd, fd_name, lock, logger_extra, filter)
    unit.run()
    mock_logger.assert_not_called()


@mock.patch('netsplice.util.stream_to_log.logger.info')
def test_run_does_not_log_filtered_lines(mock_logger):
    fd = mock_fd()
    fd.readline_return_value = ['message with match']
    fd_name = 'test_fd_name'
    lock = None
    logger_extra = {}
    filter = ['match']
    unit = stream_to_log(
        fd, fd_name, lock, logger_extra, filter)
    unit.run()
    mock_logger.assert_not_called()


@mock.patch('netsplice.util.stream_to_log.logger.info')
def test_run_does_log_un_filtered_lines(mock_logger):
    fd = mock_fd()
    fd.readline_return_value = ['message with match', 'unfiltered']
    fd_name = 'test_fd_name'
    lock = None
    logger_extra = {}
    filter = ['match']
    unit = stream_to_log(
        fd, fd_name, lock, logger_extra, filter)
    unit.run()
    mock_logger.assert_called()


@mock.patch('netsplice.util.stream_to_log.logger.info')
def test_run_does_log_without_extra(mock_logger):
    fd = mock_fd()
    fd.readline_return_value = ['message with match', 'unfiltered']
    fd_name = 'test_fd_name'
    lock = None
    logger_extra = None
    filter = ['match']
    unit = stream_to_log(
        fd, fd_name, lock, logger_extra, filter)
    unit.run()
    mock_logger.assert_called()


@mock.patch('netsplice.util.stream_to_log.logger.info')
def test_run_with_win_newline_logs_without_newline(mock_logger):
    fd = mock_fd()
    fd.readline_return_value = ['message with\rmatch']
    fd_name = 'test_fd_name'
    lock = None
    logger_extra = {}
    filter = None
    unit = stream_to_log(
        fd, fd_name, lock, logger_extra, filter)
    unit.run()
    mock_logger.assert_called_with(
        'message withmatch', extra={'origin': 'log-stream-test_fd_name'})


@mock.patch('netsplice.util.stream_to_log.logger.info')
def test_run_any_fd_name_log_to_info(mock_logger):
    fd = mock_fd()
    fd.readline_return_value = ['message with match']
    fd_name = 'test_fd_name'
    lock = None
    logger_extra = {}
    filter = None
    unit = stream_to_log(
        fd, fd_name, lock, logger_extra, filter)
    unit.run()
    mock_logger.assert_called()


@mock.patch('netsplice.util.stream_to_log.logger.error')
def test_run_stderr_fd_name_log_to_error(mock_logger):
    fd = mock_fd()
    fd.readline_return_value = ['message with match']
    fd_name = 'stderr'
    lock = None
    logger_extra = {}
    filter = None
    unit = stream_to_log(
        fd, fd_name, lock, logger_extra, filter)
    unit.run()
    mock_logger.assert_called()


@mock.patch('netsplice.util.stream_to_log.stream_to_log.join')
def test_stop_sets_stopped(
        mock_join):
    fd = mock_fd()
    fd.readline_return_value = ['message with match']
    fd_name = 'stderr'
    lock = None
    logger_extra = {}
    filter = None
    unit = stream_to_log(
        fd, fd_name, lock, logger_extra, filter)
    assert(unit._stopped is False)
    unit.stop()
    assert(unit._stopped is True)


@mock.patch('netsplice.util.stream_to_log.stream_to_log.join')
def test_stop_closes_fd(
        mock_join):
    fd = mock_fd()
    fd.readline_return_value = ['message with match']
    fd_name = 'stderr'
    lock = None
    logger_extra = {}
    filter = None
    unit = stream_to_log(
        fd, fd_name, lock, logger_extra, filter)
    unit.stop()
    assert(fd.close_called is True)


@mock.patch('netsplice.util.stream_to_log.stream_to_log.join')
def test_stop_ignores_missing_fd(
        mock_join):
    fd = mock_fd()
    fd.readline_return_value = ['message with match']
    fd_name = 'stderr'
    lock = None
    logger_extra = {}
    filter = None
    unit = stream_to_log(
        fd, fd_name, lock, logger_extra, filter)
    unit._fd = None
    assert(unit._stopped is False)
    unit.stop()
    assert(unit._stopped is True)
