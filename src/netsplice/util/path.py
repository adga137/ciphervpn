# -*- coding: utf-8 -*-
# path.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Path helper methods.

Methods that return or manipulate paths.
'''

import os
import errno
import sys
from netsplice.config import constants

# Optional home value, used to override by commandline option
# when a service is started as different user (NetsplicePrivilegedApp)
this = sys.modules[__name__]
this.g_config_home = None


try:
    # Only available on debian systems with python-dirspec package installed.
    from dirspec.basedir import get_xdg_config_home

except ImportError:
    def get_xdg_config_home():
        '''
        Get Config Home.

        Minimal fallback implementation for get_xdg_config_home.
        Returns $HOME on unix systems and LOCALAPPDATA for windows.
        '''
        env_home = ''
        if 'HOME' in os.environ:
            # UNIX systems
            env_home = os.environ['HOME']
        elif 'LOCALAPPDATA' in os.environ:
            # Windows
            return os.environ['LOCALAPPDATA']

        if env_home is None or env_home == '':
            return '.'

        return '%s/.config/' % (env_home,)


def get_path_prefix():
    '''
    Return the platform dependent path prefix.

    :param standalone: If True it will return the prefix for a standalone
                       application.
                       Otherwise, it will return the system default for
                       configuration storage.
    :type standalone: bool
    '''
    if this.g_config_home is None:
        config_home = get_xdg_config_home()
    else:
        config_home = this.g_config_home

    return config_home


def get_location(name):
    '''
    Get Location.

    Return a path in the config location (eg ~/.config/Netsplice/name).
    '''
    location = os.path.join(get_path_prefix(), constants.NS_NAME, name)
    return location


def get_preferences_location(name=None):
    '''
    Get Preferences Location.

    Return the path where preferences are stored.
    '''
    location = get_location('preferences')
    if name is None:
        return location
    return os.path.join(location, name)


def mkdir_p(path):
    '''
    Make Directory.

    Creates the path and all the intermediate directories that don't
    exist.

    Might raise OSError. Does not raise when the given path exists.

    :param path: path to create
    :type path: str
    '''
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def set_config_home(path):
    '''
    Set Config Home.

    Override the default path prefix to allow loading configs
    of user as elevated.
    '''
    this.g_config_home = path
