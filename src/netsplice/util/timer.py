# -*- coding: utf-8 -*-
# timer.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Timer utility.

Defines a singleton that can be used in the application.
```
    from netsplice.util import get_timer
    timer = get_timer()
    timer.start('measurement1')
    # do something
    timer.stop('measurement1')
    timer.report()
```
'''

import time


class timer_singleton(object):
    '''
    Timer Singleton.

    Singleton implementation that allows using the same timer instance in
    different classes.
    '''

    def __init__(self):
        '''
        Initialize.

        Initialize Members.
        '''
        self.timer = None

    def get_timer(self):
        '''
        Get Timer.

        Return the timer singleton for usage in the application.
        '''
        return self.timer

    def is_setup(self):
        '''
        Setup was done.

        Check if the singleton has been setup.
        '''
        return self.timer is not None

    def setup(self, name, logger):
        '''
        Setup.

        Configure the logbook handler that are active for the current
        application.
        '''
        self.timer = _timer(name, logger)


class _measurement(object):
    '''
    Private class to store a single measurement.
    '''
    def __init__(self):
        '''
        Creating a instance records the start time.
        '''
        self.start = time.time()
        self.end = None

    def stop(self):
        '''
        Stop.

        Record the end time.
        '''
        self.end = time.time()

    def __str__(self):
        '''
        String.

        Produce a readable information from start/end.
        '''
        if self.end is None:
            return 'still measuring'
        return '%f seconds' % (
            self.end - self.start)


class _timer(object):
    '''
    Timer object, a single instance is managed as singleton.
    '''
    def __init__(self, name, logger):
        self.name = name
        self.measurements = dict()
        self.logger = logger

    def start(self, name):
        '''
        Start.

        Start Measurement by creating a new measurement instance.

        Arguments:
            name -- Name for the measurement.
        '''
        self.measurements[name] = _measurement()

    def stop(self, name):
        '''
        Stop.

        Stop a previously started measurement. When called with a nonexistent
        name, a KeyError will be raised. When called multiple times, the
        last occurence of stop will be stored.

        Arguments:
            name -- Name for the measurement.
        '''
        self.measurements[name].stop()
        message = '%s took %s' % (
            name, str(self.measurements[name]))
        self.logger.debug(message)

    def report(self):
        '''
        Report.

        Produce a multiline text from all measurements.
        '''
        report = ''
        for name in self.measurements.keys():
            report += '%s took %s\n' % (name, str(self.measurements[name]))
        self.logger.debug(report)


timer = timer_singleton()
