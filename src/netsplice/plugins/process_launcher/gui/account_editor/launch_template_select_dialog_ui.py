# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/plugins/process_launcher/gui/account_editor/views/launch_template_select_dialog.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_LaunchTemplateSelectDialog(object):
    def setupUi(self, launch_template_select_dialog):
        launch_template_select_dialog.setObjectName("launch_template_select_dialog")
        launch_template_select_dialog.setGeometry(QtCore.QRect(0, 0, 640, 480))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/images/netsplice-icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        launch_template_select_dialog.setWindowIcon(icon)
        self.editor_layout = QtGui.QVBoxLayout(launch_template_select_dialog)
        self.editor_layout.setObjectName("editor_layout")
        self.template_groups = QtGui.QTabWidget(launch_template_select_dialog)
        self.template_groups.setObjectName("template_groups")
        self.editor_layout.addWidget(self.template_groups)
        self.actions = QtGui.QWidget(launch_template_select_dialog)
        self.actions.setObjectName("actions")
        self.actions_layout = QtGui.QHBoxLayout(self.actions)
        self.actions_layout.setContentsMargins(0, 0, 0, 0)
        self.actions_layout.setObjectName("actions_layout")
        spacerItem = QtGui.QSpacerItem(5, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.actions_layout.addItem(spacerItem)
        spacerItem1 = QtGui.QSpacerItem(5, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.actions_layout.addItem(spacerItem1)
        self.close_action = QtGui.QPushButton(self.actions)
        self.close_action.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.close_action.setObjectName("close_action")
        self.actions_layout.addWidget(self.close_action)
        self.editor_layout.addWidget(self.actions)

        self.retranslateUi(launch_template_select_dialog)
        QtCore.QMetaObject.connectSlotsByName(launch_template_select_dialog)

    def retranslateUi(self, launch_template_select_dialog):
        launch_template_select_dialog.setWindowTitle(QtGui.QApplication.translate("LaunchTemplateSelectDialog", "Process Launcher Templates", None, QtGui.QApplication.UnicodeUTF8))
        self.close_action.setText(QtGui.QApplication.translate("LaunchTemplateSelectDialog", "Cancel", None, QtGui.QApplication.UnicodeUTF8))

