# -*- coding: utf-8 -*-
# account_editor.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Account editor.

Extend Account Editor widget.
'''
import json
import sys
import os
import uuid

from PySide.QtGui import QWidget, QMessageBox
from PySide.QtCore import Qt, Signal
from .account_editor_ui import Ui_AccountEditor
from .rule_dialog import rule_dialog
from .launch_template_select import launch_template_select

from netsplice.config import constants as constants
from netsplice.gui.key_value_editor import key_value_editor as key_value_editor
from netsplice.model.configuration_list import (
    configuration_list as configuration_list_model
)
from netsplice.plugins.process_launcher.config import gui as gui_config
from netsplice.plugins.process_launcher.config import (
    PLUGIN_NAME, FIELD_ID, FIELD_ACTIVE, FIELD_ELEVATED, FIELD_EVENT_NAME,
    FIELD_EXECUTABLE, FIELD_PARAMETERS, FIELD_WORKING_DIRECTORY,
    FIELD_PROFILE, KEY_REMOVED_PREFIX
)
from netsplice.plugins.process_launcher.config import (
    templates as config_templates
)

from netsplice.plugins.process_launcher.gui.model.response.plugin_collection \
    import (
        plugin_collection as response_plugin_collection_model
    )
from netsplice.util.model.field import field
from netsplice.util.model.errors import ValidationError
from netsplice.util.errors import NotFoundError
from netsplice.util.parser import factory as get_config_parser
from netsplice.util import get_logger, basestring, escape_html
from netsplice.util.process.location import (
    factory as location_factory
)


logger = get_logger()

PLUGIN_COLLECTION = 'accounts'


class account_editor(QWidget):
    '''
    Account Editor.

    Custom Editor for Process Launcher options.
    This widget has to be embedded in a ui-file.
    '''

    backend_failed = Signal(basestring)
    config_changed = Signal()
    command_list_changed = Signal()
    plugin_collection_loaded = Signal(basestring)
    plugin_collection_stored = Signal(basestring)
    test = Signal(
        basestring, basestring,
        bool, bool, basestring, list, basestring)
    test_done = Signal()
    test_failed = Signal(basestring)
    kill_by_id = Signal(basestring)

    def __init__(self, parent):
        '''
        Initialize Module.

        Initialize widget and abstract base, Setup UI.
        '''
        QWidget.__init__(self, parent)
        self.backend = None
        self.account_model = None
        self.editor_model = response_plugin_collection_model()
        self.model = response_plugin_collection_model()
        self.parent_account_editor = None
        self._id_uuid_map = dict()
        self._uuid_id_map = dict()
        self._uuid_elevated_map = dict()
        self.ui = Ui_AccountEditor()
        self.ui.setupUi(self)
        self.ui.editor_layout.setContentsMargins(0, 0, 0, 0)
        self.setFocusPolicy(Qt.TabFocus)
        self.ui.command_list.set_inserted(True)
        self.ui.command_list.set_header([
            self.tr('Event'),
            self.tr('Id'),
            self.tr('Active'),
            self.tr('Elevated'),
            self.tr('Executable'),
            self.tr('Parameters'),
            self.tr('Profile'),
            self.tr('Working Directory'),
        ])
        self.ui.command_list.enable_toolbar(
            key_value_editor.INSERT |
            key_value_editor.EDIT |
            key_value_editor.REMOVE)
        self.ui.command_list.add_toolbar_action(
            key_value_editor.SELECTED,
            self.tr('Test Command'), self._test_toolbar)
        self.ui.command_list.set_row_edit_callback(
            self._edit_row_callback)
        self.ui.command_list.set_row_insert_callback(
            self._insert_row_callback)

    def _add_rule(
            self, event_name, id, active, elevated, executable, parameters,
            working_directory, profile=False):
        '''
        Add Rule.

        Add rule to launch List.
        '''
        plugin_collection = self.get_values()
        launch_item = plugin_collection.launch_list.item_model_class()
        launch_item.event.set(event_name)
        launch_item.id.set(id)
        launch_item.active.set(active)
        launch_item.elevated.set(elevated)
        launch_item.executable.set(executable)
        launch_item.parameters.set(parameters)
        launch_item.working_directory.set(working_directory)
        launch_item.profile.set(profile)
        plugin_collection.launch_list.append(launch_item)
        self.editor_model.from_json(plugin_collection.to_json())
        self._update_ui()
        self.config_changed.emit()

    def _edit_row_callback(self, table_widget_item=None):
        '''
        Row Editor.

        Display a dialog that loads the values of the current selected row
        and updates this row on commit.
        '''
        if table_widget_item is not None:
            self.ui.command_list.table.closePersistentEditor(
                table_widget_item)
        dialog = rule_dialog(self, self.backend)
        selected_values = self.ui.command_list.get_selected_values()
        for value in selected_values:
            event_name = value.key.get()
            id = self._id_uuid_map[value.get_value(FIELD_ID)]
            active = value.get_value(FIELD_ACTIVE)
            elevated = value.get_value(FIELD_ELEVATED)
            if elevated == 'True':
                elevated = True
            else:
                elevated = False
            executable = value.get_value(FIELD_EXECUTABLE)
            parameters = []
            try:
                parameters = json.loads(value.get_value(FIELD_PARAMETERS))
            except ValueError:
                pass
            working_directory = value.get_value(FIELD_WORKING_DIRECTORY)
            dialog.set_values(
                event_name, id, active, elevated, executable, parameters,
                working_directory)
            break
        dialog.update_mode()
        dialog.commit.connect(self._update_rule)
        dialog.setModal(True)
        dialog.show()
        return dialog

    def _get_elevated_script_location(self):
        '''
        Get Elevated Script Location.

        Return the path that the elevated scripts are located in.
        '''
        location = location_factory(sys.platform)
        return os.path.join(location.get_libexec_path(), PLUGIN_NAME)

    def _insert_row_callback(self):
        '''
        Insert Rule.

        Display a dialog that inserts a new rule when commited.
        '''
        dialog = launch_template_select(self, self.backend)
        dialog.commit.connect(
            self._insert_rule_from_template)
        dialog.setModal(True)
        dialog.show()

    def _insert_rule_from_template(self, new_launch_item):
        '''
        Insert Rule from Template.

        Insert a previously selected rule template by showing the correct
        rule_dialog configuration.
        '''
        dialog = rule_dialog(self, self.backend)
        dialog.create_mode()
        dialog.set_values(
            new_launch_item.key.get(),
            new_launch_item.get_value(FIELD_ID),
            new_launch_item.get_value(FIELD_ACTIVE),
            new_launch_item.get_value(FIELD_ELEVATED),
            new_launch_item.get_value(FIELD_EXECUTABLE),
            json.loads(new_launch_item.get_value(FIELD_PARAMETERS)),
            new_launch_item.get_value(FIELD_WORKING_DIRECTORY))
        dialog.commit.connect(self._add_rule)
        dialog.rejected.connect(self._insert_row_callback)
        dialog.setModal(True)
        dialog.show()

    def _load_plugin_values(self, account_instance):
        '''
        Load Plugin Values.

        The account model was loaded, request the plugin value.
        '''
        if account_instance is None:
            return
        account_id = account_instance.id.get()
        if account_id is None:
            return
        self.account_model = account_instance
        self.backend.preferences_plugin_collection_instance_get.emit(
            PLUGIN_NAME, PLUGIN_COLLECTION, account_id,
            self.plugin_collection_loaded, self.backend_failed)

    def _plugin_collection_loaded(self, model_json):
        '''
        Plugin Collection Loaded.

        The request for the plugin values completed with the given model_json.
        '''
        self.editor_model = response_plugin_collection_model()
        self.editor_model.from_json(model_json)
        self.model = response_plugin_collection_model()
        self.model.from_json(model_json)
        self._update_ui()
        self.config_changed.emit()

    def _update_ui(self):
        '''
        Update UI.

        Update the command list with the launch items from the editor model.
        '''
        parser = get_config_parser('Line')
        self._id_uuid_map = dict()
        self._uuid_id_map = dict()
        for item in self.editor_model.launch_list:
            line = parser.current_element
            line.key.set(item.event.get())
            user_id = str(len(self._id_uuid_map.keys()))
            self._id_uuid_map[user_id] = item.id.get()
            self._uuid_id_map[item.id.get()] = user_id
            self._uuid_elevated_map[item.id.get()] = item.elevated.get()
            line.add_value(FIELD_ID, user_id)
            line.add_value(FIELD_ACTIVE, item.active.get())
            line.add_value(FIELD_ELEVATED, str(item.elevated.get()))
            line.add_value(FIELD_EXECUTABLE, item.executable.get())
            line.add_value(FIELD_PARAMETERS, json.dumps(item.parameters.get()))
            line.add_value(FIELD_PROFILE, item.profile.get())
            line.add_value(
                FIELD_WORKING_DIRECTORY, item.working_directory.get())
            parser.commit()
        self.command_list_changed.emit()
        self.ui.command_list.model_changed.emit(
            parser.get_config())

    def _plugin_collection_stored(self):
        '''
        Plugin Collection Stored.

        The plugin values have been stored.
        '''
        values = self.get_values()
        self.model.from_json(values.to_json())

    def _reset_plugin_values(self):
        '''
        Reset Plugin Values.

        The account model was reset, commit the plugin value.
        '''
        parser = get_config_parser('Line')

        self.ui.command_list.model_changed.emit(
            parser.get_config())
        self.ui.errors.setText('')
        self.ui.errors.hide()

    def _setup_connections(self):
        '''
        Setup Connections.

        Setup the connections for the widget.
        '''
        self.ui.command_list.values_changed.connect(
            self._values_changed)
        self.ui.command_list.value_checked.connect(
            self._value_checked)
        if self.parent_account_editor is not None:
            self.parent_account_editor.model_loaded_signal.connect(
                self._load_plugin_values)
            self.parent_account_editor.model_reset_signal.connect(
                self._reset_plugin_values)
            self.parent_account_editor.model_stored_signal.connect(
                self._store_plugin_values)
        self.plugin_collection_loaded.connect(self._plugin_collection_loaded)
        self.plugin_collection_stored.connect(self._plugin_collection_stored)
        self.test.connect(self._test)
        self.test_done.connect(self._test_done)
        self.test_failed.connect(self._test_failed)

    def _store_plugin_values(self, account_id):
        '''
        Store Plugin Values.

        The account model was stored, commit the plugin value. There is no
        way to interrupt the storage, all values have been validated before.
        '''
        try:
            values = self.get_values()
            self.backend.preferences_plugin_collection_instance_set.emit(
                PLUGIN_NAME, PLUGIN_COLLECTION, account_id, values,
                self.plugin_collection_stored, self.backend_failed)
        except ValidationError:
            pass

    def _test_toolbar(self):
        '''
        Test.

        Test the current selected list item.
        '''
        selected_values = self.ui.command_list.get_selected_values()
        if len(selected_values) is 0:
            raise Exception('No item selected')
        selected_value = selected_values[0]

        event_name = selected_value.key.get()
        id = self._id_uuid_map[selected_value.values.find_by_name(
            FIELD_ID).value.get()]
        active = selected_value.values.find_by_name(
            FIELD_ACTIVE).value.get()
        elevated = selected_value.values.find_by_name(
            FIELD_ELEVATED).value.get()
        executable = selected_value.values.find_by_name(
            FIELD_EXECUTABLE).value.get()
        if elevated == 'True':
            elevated = True
        else:
            elevated = False
        try:
            parameters = json.loads(
                selected_value.values.find_by_name(
                    FIELD_PARAMETERS).value.get())
        except ValueError:
            parameters = []
        working_directory = selected_value.values.find_by_name(
            FIELD_WORKING_DIRECTORY).value.get()
        self.test.emit(
            event_name, id, active, elevated, executable, parameters,
            working_directory)

    def _test(
            self, event_name, id, active, elevated, executable, parameters,
            working_directory):
        '''
        Test.

        Test the current values with a current process-list.
        '''
        if not active:
            QMessageBox.warning(
                None, constants.NS_NAME,
                gui_config.MESSAGE_INACTIVE_CANNOT_TEST,
                QMessageBox.Ok, QMessageBox.Ok)
            return

        if elevated:
            message = gui_config.MESSAGE_ELEVATED_CANNOT_TEST.format(
                LIBEXEC=self._get_elevated_script_location())
            QMessageBox.warning(
                None, constants.NS_NAME,
                message,
                QMessageBox.Ok, QMessageBox.Ok)
            return

        plugin_collection = response_plugin_collection_model()
        launch_item = plugin_collection.launch_list.item_model_class()
        try:

            launch_item.event.set(event_name)
            launch_item.id.set(id)
            launch_item.active.set(True)
            launch_item.elevated.set(elevated)
            launch_item.executable.set(executable)
            launch_item.parameters.set(parameters)
            launch_item.working_directory.set(working_directory)
            logger.debug('Process Launch: %s' % (launch_item.to_json(),))

            self.backend.process_launcher.launch.emit(
                launch_item,
                self.test_done, self.test_failed)
        except ValidationError as errors:
            self._test_failed(str(errors))

    def _test_done(self):
        '''
        Test Done.

        The process has launched.
        '''
        logger.debug('Process launch completed.')
        message = self.tr(gui_config.MESSAGE_SUCCESS_TEST)
        QMessageBox.question(
            None, constants.NS_NAME,
            message,
            QMessageBox.Ok | QMessageBox.Ok, QMessageBox.Ok)

    def _test_failed(self, response):
        '''
        Test Failed.

        The process has not launched.
        '''
        logger.error('%s.' % (response,))
        message = self.tr(gui_config.MESSAGE_FAILED_TEST)
        QMessageBox.warning(
            None, constants.NS_NAME,
            message,
            QMessageBox.Ok, QMessageBox.Ok)

    def _update_rule(
            self, event_name, id, active, elevated, executable, parameters,
            working_directory):
        '''
        Update Rule.

        Update the current selected rule with the values from the dialog.
        '''
        selected_values = self.ui.command_list.get_selected_values()
        for value in selected_values:
            value.key.set(event_name)
            value.values.find_by_name(FIELD_ID).value.set(
                self.get_user_id(id))
            value.values.find_by_name(FIELD_ACTIVE).value.set(active)
            value.values.find_by_name(FIELD_ELEVATED).value.set(
                str(elevated))
            value.values.find_by_name(FIELD_EXECUTABLE).value.set(executable)
            value.values.find_by_name(FIELD_PARAMETERS).value.set(
                json.dumps(parameters))
            value.values.find_by_name(FIELD_WORKING_DIRECTORY).value.set(
                working_directory)
            value.values.find_by_name(FIELD_PROFILE).value.set(False)
            break
        self.ui.command_list.update_selected_values(selected_values)

        plugin_collection = self.get_values()
        self.editor_model.from_json(plugin_collection.to_json())
        self._update_ui()
        self.config_changed.emit()

    def _value_checked_canceled(self, id):
        '''
        Value Checked Canceled.

        After the elevated checkbox was enabled, the dialog has been canceled.
        The elevated checkbox should be reverted.
        '''
        plugin_collection = self.get_values()
        parser = get_config_parser('Line')
        for item in plugin_collection.launch_list:
            line = parser.current_element
            line.key.set(item.event.get())
            if id == item.id.get():
                item.elevated.set(not item.elevated.get())
                self._uuid_elevated_map[item.id.get()] = item.elevated.get()
            line.add_value(FIELD_ID, self.get_user_id(item.id.get()))
            line.add_value(FIELD_ACTIVE, item.active.get())
            line.add_value(FIELD_ELEVATED, str(item.elevated.get()))
            line.add_value(FIELD_EXECUTABLE, item.executable.get())
            line.add_value(FIELD_PARAMETERS, json.dumps(item.parameters.get()))
            line.add_value(FIELD_PROFILE, item.profile.get())
            line.add_value(
                FIELD_WORKING_DIRECTORY, item.working_directory.get())
            parser.commit()

        self.ui.command_list.model_changed.emit(
            parser.get_config())

    def _value_checked(self, new_row_values):
        '''
        Value Checked.

        A value has been toggled, check if it is the elevated checkable and
        open the rule_dialog to update the executable.
        '''
        try:
            id = self._id_uuid_map[new_row_values[0].get_value(FIELD_ID)]
            elevated = new_row_values[0].get_value(FIELD_ELEVATED)
            if elevated == 'True':
                elevated = True
            else:
                elevated = False
            if self._uuid_elevated_map[id] is False and elevated is True:
                dialog = self._edit_row_callback()
                dialog.rejected.connect(
                    lambda: self._value_checked_canceled(id))
            self._uuid_elevated_map[id] = elevated

            plugin_collection = self.get_values()
            self.editor_model.from_json(plugin_collection.to_json())
            self._update_ui()
            self.config_changed.emit()
        except NotFoundError:
            return
        except IndexError:
            return

    def _values_changed(self, new_values):
        '''
        Values Changed.

        Key Value table values have changed, validate the values and output
        errors.
        '''
        plugin_collection = response_plugin_collection_model()
        plugin_collection.name.set(PLUGIN_NAME)
        errors = []
        for value in new_values:
            event_name = value.key.get()
            if event_name.startswith(KEY_REMOVED_PREFIX):
                continue
            id = self._id_uuid_map[value.get_value(FIELD_ID)]
            active = value.get_value(FIELD_ACTIVE)
            elevated = value.get_value(FIELD_ELEVATED)
            executable = value.get_value(FIELD_EXECUTABLE)
            parameters = value.get_value(FIELD_PARAMETERS)
            working_directory = value.get_value(FIELD_WORKING_DIRECTORY)
            if elevated == 'True':
                elevated = True
            else:
                elevated = False
            launch_item = plugin_collection.launch_list.item_model_class()
            launch_item.elevated.set(elevated)
            launch_item.active.set(active)
            launch_item.id.set(id)
            launch_item.profile.set(False)
            try:
                launch_item.event.set(event_name)
            except ValidationError:
                errors.append(
                    self.tr(r'%s is not a valid event.')
                    % (event_name,))
            try:
                launch_item.executable.set(executable)
            except ValidationError:
                errors.append(
                    self.tr(r'%s is not a valid executable.')
                    % (executable,))
            try:
                launch_item.parameters.set(json.loads(parameters))
            except ValidationError:
                errors.append(
                    self.tr(r'%s are not valid parameters.')
                    % (json.dumps(parameters),))
            except ValueError:
                errors.append(
                    self.tr(r'%s is not a valid JSON.')
                    % (json.dumps(parameters),))
            try:
                launch_item.working_directory.set(working_directory)
            except ValidationError:
                errors.append(
                    self.tr(r'%s is not a valid working_directory.')
                    % (working_directory,))

            plugin_collection.launch_list.append(launch_item)

        if len(errors) is 0:
            # set the validated values, most notably, remove the items
            # that have the KEY_REMOVED_PREFIX
            self.editor_model.from_json(plugin_collection.to_json())
            self._update_ui()
            self.ui.errors.hide()
            self.config_changed.emit()
        else:
            error_text = ''
            for error in errors:
                error_text += '%s<br/>' % (escape_html(error),)
            self.ui.errors.setText(error_text)
            self.ui.errors.show()

    def add_from_template(self, template, profile):
        '''
        Add from Template.

        Add launcher from template, find the template by the command.

        Arguments:
            template_name (string): command of the template.
        '''
        if isinstance(template, (dict,)):
            self._add_rule(
                template[FIELD_EVENT_NAME],
                template[FIELD_ID],
                True,
                template[FIELD_ELEVATED],
                template[FIELD_EXECUTABLE],
                template[FIELD_PARAMETERS],
                template[FIELD_WORKING_DIRECTORY],
                profile=True)
        else:
            for config_template in config_templates.TEMPLATES:
                if config_template[FIELD_EXECUTABLE] != template:
                    continue
                self._add_rule(
                    config_template[FIELD_EVENT_NAME],
                    str(uuid.uuid4()),
                    True,
                    config_template[FIELD_ELEVATED],
                    config_template[FIELD_EXECUTABLE],
                    config_template[FIELD_PARAMETERS],
                    config_template[FIELD_WORKING_DIRECTORY],
                    profile=True)

    def config_values_changed(self):
        '''
        Config Values Changed.

        Evaluate if the config is different from the loaded values.
        '''
        changed = False
        launch_list = self.ui.command_list.get_values()
        changed = len(self.model.launch_list) != len(launch_list)
        try:
            for launch_item in launch_list:
                if changed:
                    break
                model_launch_item = None
                launch_item_id = launch_item.get_value(FIELD_ID)
                launch_item_uuid = self._id_uuid_map[launch_item_id]
                for model_item in self.model.launch_list:
                    if model_item.id.get() == launch_item_uuid:
                        model_launch_item = model_item
                        break
                if model_launch_item is None:
                    changed = True
                    break
                changed |= (
                    model_launch_item.executable.get() !=
                    launch_item.get_value(FIELD_EXECUTABLE))
                changed |= (
                    model_launch_item.event.get() !=
                    launch_item.key.get())
                changed |= (
                    str(model_launch_item.elevated.get()) !=
                    str(launch_item.get_value(FIELD_ELEVATED)))
                changed |= (
                    str(model_launch_item.parameters.get()) !=
                    str(launch_item.get_value(FIELD_PARAMETERS)))
                changed |= (
                    model_launch_item.working_directory.get() !=
                    launch_item.get_value(FIELD_WORKING_DIRECTORY))
                changed |= (
                    model_launch_item.active.get() !=
                    launch_item.get_value(FIELD_ACTIVE))
        except Exception as e:
            import traceback
            logger.error(traceback.format_exc(e))

        return changed

    def get_values(self):
        '''
        Get Values.

        Return the values suitable for storage.
        '''
        new_values = self.ui.command_list.get_values()
        plugin_collection = response_plugin_collection_model()
        plugin_collection.name.set(PLUGIN_NAME)
        for value in new_values:
            event_name = value.key.get()
            if event_name.startswith(KEY_REMOVED_PREFIX):
                continue
            id = self._id_uuid_map[value.get_value(FIELD_ID)]
            active = value.get_value(FIELD_ACTIVE)
            elevated = value.get_value(FIELD_ELEVATED)
            if elevated == 'True':
                elevated = True
            else:
                elevated = False
            executable = value.get_value(FIELD_EXECUTABLE)
            try:
                parameters = json.loads(value.get_value(FIELD_PARAMETERS))
            except ValueError:
                parameters = []
            working_directory = value.get_value(FIELD_WORKING_DIRECTORY)
            profile = value.get_value(FIELD_PROFILE)
            launch_item = plugin_collection.launch_list.item_model_class()
            launch_item.event.set(event_name)
            launch_item.id.set(id)
            launch_item.active.set(active)
            launch_item.elevated.set(elevated)
            launch_item.executable.set(executable)
            launch_item.parameters.set(parameters)
            launch_item.working_directory.set(working_directory)
            launch_item.profile.set(profile)
            plugin_collection.launch_list.append(launch_item)

        return plugin_collection

    def get_user_id(self, id):
        '''
        Get User ID.

        Return the user ID for the given UUID.
        '''
        return self._uuid_id_map[id]

    def remove_profile_items(self):
        '''
        Remove Profile Items.

        Remove all items that have the profile flag set.
        '''
        new_values = self.ui.command_list.get_values()
        plugin_collection = response_plugin_collection_model()
        plugin_collection.name.set(PLUGIN_NAME)
        for value in new_values:
            profile = value.get_value(FIELD_PROFILE)
            if profile is True:
                # Drop profile flagged launchers.
                continue
            event_name = value.key.get()
            id = self._id_uuid_map[value.get_value(FIELD_ID)]
            active = value.get_value(FIELD_ACTIVE)
            elevated = value.get_value(FIELD_ELEVATED)
            if elevated == 'True':
                elevated = True
            else:
                elevated = False
            executable = value.get_value(FIELD_EXECUTABLE)
            try:
                parameters = json.loads(value.get_value(FIELD_PARAMETERS))
            except ValueError:
                parameters = []
            working_directory = value.get_value(FIELD_WORKING_DIRECTORY)
            profile = value.get_value(FIELD_PROFILE)
            launch_item = plugin_collection.launch_list.item_model_class()
            launch_item.event.set(event_name)
            launch_item.id.set(id)
            launch_item.active.set(active)
            launch_item.elevated.set(elevated)
            launch_item.executable.set(executable)
            launch_item.parameters.set(parameters)
            launch_item.working_directory.set(working_directory)
            launch_item.profile.set(profile)
            plugin_collection.launch_list.append(launch_item)

        plugin_collection = self.get_values()
        self.editor_model.from_json(plugin_collection.to_json())
        self._update_ui()

    def set_account_editor(self, account_editor_widget):
        '''
        Set Account Editor.

        Set the parent account_editor_widget.
        '''
        self.parent_account_editor = account_editor_widget
        self._setup_connections()

    def set_dispatcher(self, dispatcher):
        '''
        Set dispatcher.

        Set the backend dispatcher for the widget.
        '''
        self.backend = dispatcher
