# -*- coding: utf-8 -*-
# rule_dialog.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Create Rule Dialog.

Dialog to create or edit rules for launch_list.
'''
import os
import sys
import shlex
import subprocess

from PySide.QtCore import Qt, Signal
from PySide.QtGui import QFileDialog, QMessageBox
from .rule_dialog_ui import Ui_RuleDialog

from netsplice.gui.widgets.dialog import dialog
from netsplice.model.configuration_item import (
    configuration_item as configuration_item_model
)
from netsplice.config import constants
from netsplice.config import process as config_process
from netsplice.plugins.process_launcher import config as config
from netsplice.plugins.process_launcher.config import gui as config_gui
from netsplice.plugins.process_launcher.config import events as config_events
from netsplice.util import basestring
from netsplice.util.process.location import (
    factory as location_factory
)


class rule_dialog(dialog):
    '''
    Rule Dialog.

    Dialog that allows the creation or modify of a rule.
    '''

    backend_failed = Signal(basestring)
    commit = Signal(
        basestring, basestring,
        bool, bool, basestring, list, basestring)
    cancel = Signal()
    test_done = Signal(basestring)

    def __init__(self, parent, dispatcher):
        '''
        Initialize Module.

        Initialize widget and abstract base, Setup UI.
        '''
        dialog.__init__(self, parent)
        self.set_minimal()
        self.backend = dispatcher
        self.ui = Ui_RuleDialog()
        self.ui.setupUi(self)
        self.setFocusPolicy(Qt.TabFocus)
        self.ui.ok_action.setFocus()

        self.ui.cancel_action.clicked.connect(
            self._cancel)
        self.ui.parameters.textChanged.connect(
            self._parameters_changed)
        self.ui.elevated_executable.textChanged.connect(
            self._elevated_executable_changed)
        self.ui.executable.textChanged.connect(
            self._executable_changed)
        self.ui.parameters.textChanged.connect(
            self._parameters_changed)
        self.ui.ok_action.clicked.connect(
            self._commit)
        self.ui.browse_executable_action.clicked.connect(
            self._browse_executable)
        self.ui.browse_elevated_executable_action.clicked.connect(
            self._browse_elevated_executable)
        self.ui.browse_working_directory_action.clicked.connect(
            self._browse_working_directory)
        self.ui.test_action.clicked.connect(
            self._test)
        hint_text = self.tr(config_gui.HINT_ELEVATED)
        if sys.platform.startswith('win32'):
            hint_text += self.tr(config_gui.HINT_ELEVATED_WIN32).format(
                LIBEXEC=self._get_elevated_script_location())
        else:
            hint_text += self.tr(config_gui.HINT_ELEVATED_UNIX).format(
                LIBEXEC=self._get_elevated_script_location())
        self.ui.label_elevated_hint.setText(hint_text)
        self._elevated = False
        self._id = None

        self._parameters_changed()

        for name in config_events.EVENT_NAMES:
            self.ui.event_select.addItem(
                self.tr(name), name)

    def _browse_elevated_executable(self):
        '''
        Browse Elevated Executable.

        Open a FileOpen dialog in the libexec_path.
        '''
        start_directory = self._get_elevated_script_location()
        start_filename = start_directory
        if self.ui.elevated_executable.text() != '':
            start_filename = os.path.join(
                self._get_elevated_script_location(),
                self.ui.elevated_executable.text())
        if not os.path.isfile(start_filename):
            start_filename = start_directory
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.ExistingFile)
        dialog.setOption(QFileDialog.ReadOnly)
        executable = dialog.getOpenFileName(
            self,
            self.tr('Choose Elevated Executable'),
            start_filename)
        if executable[0] != '':
            elevated_relative_path = os.path.relpath(
                executable[0], start_directory)
            is_absolute = elevated_relative_path.startswith('/')
            is_out = elevated_relative_path.startswith('..')
            if is_out or is_absolute:
                message = config_gui.MESSAGE_ELEVATED_ONLY_IN_LIBEXEC.format(
                    LIBEXEC=self._get_elevated_script_location())
                QMessageBox.warning(
                    None, constants.NS_NAME,
                    message,
                    QMessageBox.Ok, QMessageBox.Ok)
            else:
                self.ui.elevated_executable.setText(elevated_relative_path)

    def _browse_executable(self):
        '''
        Browse Executable.

        Open a FileOpen dialog that allows selection of executable.
        '''
        dialog = QFileDialog()
        if self.ui.executable.text() != '':
            start_directory = self.ui.executable.text()
        else:
            start_directory = os.path.curdir
            if sys.platform.startswith('win32'):
                dialog.setNameFilter(self.tr('Executables (*.exe)'))
                try:
                    start_directory = os.environ['PROGRAMFILES']
                except KeyError:
                    pass
            elif sys.platform.startswith('darwin'):
                start_directory = '/Applications'
                dialog.setNameFilter(self.tr('Executables (*.app)'))
            else:
                start_directory = '/usr'
        dialog.setFileMode(QFileDialog.ExistingFile)
        dialog.setOption(QFileDialog.ReadOnly)
        executable = dialog.getOpenFileName(
            self,
            self.tr('Choose Executable'),
            start_directory)
        if executable[0] != '':
            self.ui.executable.setText(executable[0])

    def _browse_working_directory(self):
        '''
        Browse Working Directory.

        Open a FileOpen dialog that allows selection of directory.
        '''
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.Directory)
        dialog.setOption(QFileDialog.ShowDirsOnly)
        directory = dialog.getExistingDirectory(
            self,
            self.tr('Choose Directory'),
            os.path.curdir)
        if directory != '':
            self.ui.working_directory.setText(directory)

    def _cancel(self):
        '''
        Cancel.

        Cancel the dialog, emit rejected so the callee can revert to
        original values.
        '''
        self.reject()

    def _commit(self):
        '''
        Commit.

        Send a signal with the dialog's selection.
        '''
        event_name = self.ui.event_select.itemData(
            self.ui.event_select.currentIndex())
        state = self.ui.active.checkState()
        active = False
        if state == Qt.Checked:
            active = True
        executable = ''
        parameters = []
        working_directory = ''
        if self._elevated:
            executable = self.ui.elevated_executable.text()
        else:
            executable = self.ui.executable.text()
            try:
                parameters = self._get_parameters()
            except ValueError:
                parameters = []
            working_directory = self.ui.working_directory.text()
        self.commit.emit(
            event_name, self._id, active, self._elevated, executable,
            parameters, working_directory)
        self.accept()

    def _elevated_executable_changed(self):
        '''
        Elevated Executable Changed.

        Enable or disable the ok-action based on the LineEdit value.
        '''
        if not self._elevated:
            return
        self._update_ok_action()

    def _executable_changed(self):
        '''
        Executable Changed.

        Enable or disable the ok-action based on the LineEdit value.
        '''
        if self._elevated:
            return
        self._update_ok_action()

    def _get_elevated_script_location(self):
        '''
        Get Elevated Script Location.

        Return the path that the elevated scripts are located in.
        '''
        location = location_factory(sys.platform)
        return os.path.join(location.get_libexec_path(), config.PLUGIN_NAME)

    def _get_parameters(self):
        '''
        Get Parameters.

        Get the parameters line-edit and parse it to a list.
        Raises ValueError when the string is badly quoted.
        '''
        parameters = []
        str_parameters = self.ui.parameters.text()
        parameters = shlex.split(str_parameters)
        return parameters

    def _parameters_changed(self):
        '''
        Commandline Changed.

        Enable or disable the ok-action based on the LineEdit value.
        '''
        if self._elevated:
            return
        self._update_ok_action()

    def _set_parameters(self, parameters):
        '''
        Set Parameters.

        Set parameters line-edit from a list.
        '''
        str_parameters = ''
        str_parameters = subprocess.list2cmdline(parameters)
        self.ui.parameters.setText(str_parameters)

    def _test(self):
        '''
        Test.

        Test the given configuration.
        '''
        event_name = self.ui.event_select.itemData(
            self.ui.event_select.currentIndex())
        state = self.ui.active.checkState()
        active = False
        executable = ''
        parameters = []
        working_directory = ''
        if state == Qt.Checked:
            active = True
        if self._elevated:
            executable = self.ui.elevated_executable.text()
        else:
            executable = self.ui.executable.text()
            try:
                parameters = self._get_parameters()
            except ValueError:
                pass
            working_directory = self.ui.working_directory.text()

        self.parent().test.emit(
            event_name, self._id, active, self._elevated, executable,
            parameters, working_directory)

    def _update_ok_action(self):
        '''
        Update ok action.

        Check that the dialog is valid and enable or disable the ok action.
        '''
        valid = True
        if self._elevated:
            if self.ui.elevated_executable.text() == '':
                valid = False
            if '..' in self.ui.elevated_executable.text():
                valid = False
            full_executable_filename = os.path.join(
                self._get_elevated_script_location(),
                self.ui.elevated_executable.text())
            if not os.path.isfile(full_executable_filename):
                valid = False
        else:
            if self.ui.executable.text() == '':
                valid = False
            is_file = os.path.isfile(self.ui.executable.text())
            is_directory = os.path.isdir(self.ui.executable.text())
            if not is_file and not is_directory:
                valid = False
        try:
            shlex.split(self.ui.parameters.text())
        except ValueError:
            valid = False
        self.ui.ok_action.setEnabled(valid)

    def create_mode(self):
        '''
        Create Mode.

        Rename the Ok action to display "Create".
        '''
        self.ui.ok_action.setText(self.tr('Create'))
        self.ui.test_action.setVisible(False)

    def set_values(
            self, event_name, id, active, elevated, executable, parameters,
            working_directory):
        '''
        Set Values.

        Set the controls to the given values.
        '''
        self.ui.event_select.setCurrentIndex(
            self.ui.event_select.findData(event_name))
        state = Qt.Unchecked
        if active:
            state = Qt.Checked
        self.ui.active.setCheckState(state)
        self._elevated = elevated
        self._id = id
        self.ui.elevated.hide()
        self.ui.not_elevated.hide()
        if self._elevated:
            if executable.startswith('/'):
                executable = ''
            self._set_parameters(list())
            self.ui.elevated_executable.setText(executable)
            self.ui.elevated.show()
            self.ui.test_action.hide()
            self._elevated_executable_changed()
        else:
            self.ui.executable.setText(executable)
            self._set_parameters(parameters)
            self.ui.working_directory.setText(working_directory)
            self.ui.test_action.show()
            self.ui.not_elevated.show()
            self._executable_changed()

    def update_mode(self):
        '''
        Update Mode.

        Rename the Ok action to display "Update".
        '''
        self.ui.ok_action.setText(self.tr('Update'))
