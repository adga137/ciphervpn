# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/plugins/process_launcher/gui/account_editor/views/account_editor.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_AccountEditor(object):
    def setupUi(self, process_launcher):
        process_launcher.setObjectName("process_launcher")
        process_launcher.setMinimumSize(QtCore.QSize(100, 80))
        self.editor_layout = QtGui.QVBoxLayout(process_launcher)
        self.editor_layout.setObjectName("editor_layout")
        self.detail_stack = QtGui.QStackedWidget(process_launcher)
        self.detail_stack.setObjectName("detail_stack")
        self.basic = QtGui.QWidget()
        self.basic.setObjectName("basic")
        self.basic_layout = QtGui.QGridLayout(self.basic)
        self.basic_layout.setObjectName("basic_layout")
        self.errors = QtGui.QLabel(self.basic)
        self.errors.setWordWrap(True)
        self.errors.setVisible(False)
        self.errors.setObjectName("errors")
        self.basic_layout.addWidget(self.errors, 0, 0, 1, 3)
        self.command_list = key_value_editor(self.basic)
        self.command_list.setObjectName("command_list")
        self.basic_layout.addWidget(self.command_list, 1, 0, 1, 3)
        self.detail_stack.addWidget(self.basic)
        self.editor_layout.addWidget(self.detail_stack)

        self.retranslateUi(process_launcher)
        self.detail_stack.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(process_launcher)

    def retranslateUi(self, process_launcher):
        pass

from netsplice.gui.key_value_editor.key_value_editor import key_value_editor
