# -*- coding: utf-8 -*-
# launch_template_select.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Launch Template Select

Dialog to select from one of many launch item templates.
'''
import json
import sys
import uuid

from PySide.QtCore import Qt, Signal
from PySide.QtGui import (
    QWidget, QFrame, QVBoxLayout,
    QScrollArea, QSpacerItem, QSizePolicy
)
from .launch_template_select_dialog_ui import Ui_LaunchTemplateSelectDialog
from .launch_template_select_item_ui import Ui_LaunchTemplateSelectItem

from netsplice.gui.widgets.dialog import dialog
from netsplice.model.configuration_item import (
    configuration_item as configuration_item_model
)
from netsplice.plugins.process_launcher.config import backend as config_backend
from netsplice.plugins.process_launcher.config import (
    templates as config_templates
)
from netsplice.util import basestring

FIELD_ID = 'id'
FIELD_EVENT_NAME = 'event_name'
FIELD_ACTIVE = 'active'
FIELD_DESCRIPTION = 'description'
FIELD_ELEVATED = 'elevated'
FIELD_EXECUTABLE = 'executable'
FIELD_PARAMETERS = 'parameters'
FIELD_PLATFORM_FILTER = 'platform_filter'
FIELD_WORKING_DIRECTORY = 'working_directory'


class launch_template_select(dialog):
    '''
    Rule Dialog.

    Dialog that allows the creation or modify of a rule.
    '''

    backend_failed = Signal(basestring)
    commit = Signal(object)
    cancel = Signal()

    def __init__(self, parent, dispatcher):
        '''
        Initialize Module.

        Initialize widget and abstract base, Setup UI.
        '''
        dialog.__init__(self, parent)
        self._event_name = config_backend.DEFAULT_EVENT
        self._elevated = False
        self._executable = ''
        self._parameters = list()
        self._working_directory = ''
        self.set_minimal()
        self.backend = dispatcher
        self.ui = Ui_LaunchTemplateSelectDialog()
        self.ui.setupUi(self)
        self.setFocusPolicy(Qt.TabFocus)
        self.ui.close_action.clicked.connect(
            self._close)

        self._display_templates()

    def _close(self):
        '''
        Close.

        Cancel the dialog, emit rejected so the callee can revert to original
        values.
        '''
        self.reject()

    def _commit(self):
        '''
        Commit.

        Send a signal with the dialog's selection.
        '''
        self.commit.emit(self._get_launch_item_template())
        self.accept()

    def _display_templates(self):
        '''
        Display Templates.

        Compile a list of template widgets and set them to the template_list
        scroll view.
        '''
        group_widgets = dict()
        for template in config_templates.TEMPLATES:
            if self._filtered_by_platform(template[FIELD_PLATFORM_FILTER]):
                continue
            for group in template['groups']:
                widget = self._display_template(template)
                list_widget_layout = None
                if group not in group_widgets.keys():
                    list_widget = QWidget(self)
                    list_widget_layout = QVBoxLayout(list_widget)
                    list_widget_layout.setContentsMargins(0, 0, 0, 0)
                    group_widgets[group] = {
                        'layout': list_widget_layout,
                        'widget': list_widget}
                else:
                    list_widget_layout = group_widgets[group]['layout']
                list_widget_layout.addWidget(widget)
        for group_name in config_templates.TEMPLATE_GROUPS:
            if group_name not in group_widgets.keys():
                continue
            group_widget = group_widgets[group_name]['widget']
            group_widget_layout = group_widgets[group_name]['layout']
            bottom_spacer = QSpacerItem(
                5, 5, QSizePolicy.Minimum, QSizePolicy.MinimumExpanding)
            group_widget_layout.addItem(bottom_spacer)
            group_scroll_widget = QScrollArea(self)
            group_scroll_widget.setFrameShape(QFrame.NoFrame)
            group_scroll_widget.setWidgetResizable(True)
            group_scroll_widget.setWidget(group_widget)
            self.ui.template_groups.addTab(
                group_scroll_widget, group_name)

    def _display_template(self, template):
        '''
        Display Template

        Compile a widget that displays the template's description and a
        select action.
        '''
        widget = QWidget(self)
        ui = Ui_LaunchTemplateSelectItem()
        ui.setupUi(widget)

        ui.select_action.clicked.connect(self._template_selected)
        description = str(self.tr(template[FIELD_DESCRIPTION]))
        if template[FIELD_ELEVATED]:
            description += str(
                self.tr('<p>This is an <b>elevated</b> rule.'))
        ui.text.setText(description)
        ui.select_action.template = template

        return widget

    def _filtered_by_platform(self, platform_filter):
        '''
        Filtered by Platform.

        Return True if sys.platform is in one of the platform filters.
        '''
        if platform_filter is None:
            return False
        filtered = False
        for item in platform_filter:
            item_filter = item
            if item.startswith('!'):
                item_filter = item[1:]
                if sys.platform.startswith(item_filter):
                    filtered = True
            else:
                if not sys.platform.startswith(item_filter):
                    filtered = True
        return filtered

    def _get_launch_item_template(self):
        '''
        Get Launch Item Template.

        Return a key_value_table template for a new item in the launch list.
        '''
        template = configuration_item_model(0)
        template.key.set(self._event_name)
        template.add_value(
            FIELD_ID, str(uuid.uuid4()))
        template.add_value(
            FIELD_ACTIVE, True)
        template.add_value(
            FIELD_ELEVATED, self._elevated)
        template.add_value(
            FIELD_EXECUTABLE, self._executable)
        template.add_value(
            FIELD_PARAMETERS, json.dumps(self._parameters))
        template.add_value(
            FIELD_WORKING_DIRECTORY, self._working_directory)
        return template

    def _template_selected(self):
        '''
        '''
        sender_action = self.sender()
        selected_template = sender_action.template

        self._event_name = selected_template[FIELD_EVENT_NAME]
        self._executable = selected_template[FIELD_EXECUTABLE]
        self._parameters = selected_template[FIELD_PARAMETERS]
        self._working_directory = selected_template[FIELD_WORKING_DIRECTORY]
        self._elevated = selected_template[FIELD_ELEVATED]
        self._commit()
