# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/plugins/process_launcher/gui/account_editor/views/launch_template_select_item.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_LaunchTemplateSelectItem(object):
    def setupUi(self, launch_template_select_item):
        launch_template_select_item.setObjectName("launch_template_select_item")
        launch_template_select_item.setGeometry(QtCore.QRect(0, 0, 300, 80))
        self.launch_template_select_item_layout = QtGui.QVBoxLayout(launch_template_select_item)
        self.launch_template_select_item_layout.setObjectName("launch_template_select_item_layout")
        self.text_action = QtGui.QWidget(launch_template_select_item)
        self.text_action.setObjectName("text_action")
        self.text_action_layout = QtGui.QHBoxLayout(self.text_action)
        self.text_action_layout.setContentsMargins(0, 0, 0, 0)
        self.text_action_layout.setObjectName("text_action_layout")
        self.text_spacer = QtGui.QWidget(self.text_action)
        self.text_spacer.setObjectName("text_spacer")
        self.vboxlayout = QtGui.QVBoxLayout(self.text_spacer)
        self.vboxlayout.setContentsMargins(0, 0, 0, 0)
        self.vboxlayout.setObjectName("vboxlayout")
        self.text = QtGui.QLabel(self.text_spacer)
        self.text.setWordWrap(True)
        self.text.setObjectName("text")
        self.vboxlayout.addWidget(self.text)
        spacerItem = QtGui.QSpacerItem(5, 1, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.vboxlayout.addItem(spacerItem)
        self.text_action_layout.addWidget(self.text_spacer)
        self.action = QtGui.QWidget(self.text_action)
        self.action.setObjectName("action")
        self.action_layout = QtGui.QVBoxLayout(self.action)
        self.action_layout.setContentsMargins(0, 0, 0, 0)
        self.action_layout.setObjectName("action_layout")
        spacerItem1 = QtGui.QSpacerItem(5, 5, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.action_layout.addItem(spacerItem1)
        self.select_action = QtGui.QPushButton(self.action)
        self.select_action.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.select_action.setObjectName("select_action")
        self.action_layout.addWidget(self.select_action)
        self.text_action_layout.addWidget(self.action)
        self.launch_template_select_item_layout.addWidget(self.text_action)
        self.horizontal_line = QtGui.QFrame(launch_template_select_item)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.horizontal_line.sizePolicy().hasHeightForWidth())
        self.horizontal_line.setSizePolicy(sizePolicy)
        self.horizontal_line.setFrameShape(QtGui.QFrame.HLine)
        self.horizontal_line.setFrameShadow(QtGui.QFrame.Raised)
        self.horizontal_line.setObjectName("horizontal_line")
        self.launch_template_select_item_layout.addWidget(self.horizontal_line)

        self.retranslateUi(launch_template_select_item)
        QtCore.QMetaObject.connectSlotsByName(launch_template_select_item)

    def retranslateUi(self, launch_template_select_item):
        self.select_action.setText(QtGui.QApplication.translate("LaunchTemplateSelectItem", "Select", None, QtGui.QApplication.UnicodeUTF8))

