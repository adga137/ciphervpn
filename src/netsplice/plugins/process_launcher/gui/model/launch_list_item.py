# -*- coding: utf-8 -*-
# launch_list_item.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for a single launch item.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.plugins.process_launcher.model.validator.directory import (
    directory as directory_validator
)
from netsplice.plugins.process_launcher.model.validator.event import (
    event as event_validator
)
from netsplice.plugins.process_launcher.model.validator.executable import (
    executable as executable_validator
)
from netsplice.plugins.process_launcher.model.validator.parameter_list import (
    parameter_list as parameter_list_validator
)
from netsplice.model.validator.boolean import boolean as boolean_validator
from netsplice.model.validator.uuid import uuid as uuid_validator


class launch_list_item(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.id = field(
            required=True,
            validators=[uuid_validator()])

        self.active = field(
            required=True,
            validators=[boolean_validator()])

        self.elevated = field(
            required=True,
            validators=[boolean_validator()])

        self.event = field(
            required=True,
            validators=[event_validator()])

        self.executable = field(
            required=True,
            validators=[executable_validator()])

        self.parameters = field(
            required=True,
            validators=[parameter_list_validator()])

        self.working_directory = field(
            required=True,
            validators=[directory_validator()])
