# -*- coding: utf-8 -*-
# process.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
import json
from netsplice.util.process.dispatcher import dispatcher as process_dispatcher
from netsplice.util import get_logger
from netsplice.util.model.errors import ValidationError

logger = get_logger()


class process(object):
    '''
    '''
    def __init__(self):
        pass

    def launch(self, executable, parameters, working_directory, environment):
        '''
        Launch.

        Launch the given commandline. Limit the executables by requiring
        relative executable path's and setting a prefix.
        '''
        dispatcher = process_dispatcher(
            executable, parameters, None)
        if executable.startswith('/') or executable.startswith('..'):
            raise ValidationError(
                'Privileged process must not execute commands'
                ' out of installation directory.')
        dispatcher.set_prefix('process_launcher')
        dispatcher.set_working_directory(working_directory)
        dispatcher.set_environment(environment)
        returncode = dispatcher.start_application_sync()
        commandline = dispatcher.get_commandline()
        new_pid = dispatcher.get_subprocess_pid()
        logger.info(
            'Started privileged command %s with %s in %s. Got PID: %s'
            ' Returncode: %d'
            % (str(executable), json.dumps(commandline), working_directory,
                str(new_pid), returncode))
        return returncode
