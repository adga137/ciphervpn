# -*- coding: utf-8 -*-
# process_returncode.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for a returncode of a executed script
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.validator.min import (
    min as min_validator
)
from netsplice.model.validator.max import (
    max as max_validator
)


class process_returncode(marshalable):

    def __init__(self):
        marshalable.__init__(self)
        self.returncode = field(
            required=True,
            validators=[min_validator(0), max_validator(255)])
