# -*- coding: utf-8 -*-
# process_controller.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.errors import (
    NotFoundError
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.util.process.errors import ProcessError
from netsplice.util.errors import NotInitializedError
from netsplice.util import get_logger
from .model.request.launch_process import (
    launch_process as launch_process_model
)
from .model.response.process_item import (
    process_item as process_item_model
)
from .process import process as process_dispatcher

logger = get_logger()


class process_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def post(self):
        '''
        The "post" action requests the application to launch a given
        application with the given commandline.
        '''
        request_model = launch_process_model()
        response_model = process_item_model()
        try:
            request_model.from_json(
                self.request.body.decode('utf-8'))
            process = process_dispatcher()
            (pid, commandline) = process.launch(
                request_model.executable.get(),
                request_model.parameters.get(),
                request_model.working_directory.get(),
                request_model.environment)
            response_model.pid.set(pid)
            response_model.commandline.set(commandline)
            self.write(response_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            logger.error(str(errors))
            self.set_status(400)
        except ProcessError as errors:
            logger.error(str(errors))
            self.set_status(500)
        except NotInitializedError as errors:
            logger.error(str(errors))
            self.set_status(500)
        except NotFoundError:
            self.set_status(404)
        except KeyError as errors:
            logger.error(str(errors))
            self.set_status(400)
        except Exception as errors:
            logger.error(
                'Exception during launch: %s (%s)'
                % (str(errors), type(errors),))
            import traceback
            traceback.print_exc()
            self.set_status(400)
        self.finish()
