# -*- coding: utf-8 -*-
# returncodes.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

import netsplice.backend.event.names as names

RETURNCODE_OK = 0
RETURNCODE_DISCONNECT = 32
RETURNCODE_FATAL = 64

PRIVILEGED_RETURNCODES = {
    RETURNCODE_OK: None,
    RETURNCODE_DISCONNECT: [names.ACCOUNT_REQUEST_DISCONNECT, names.ERROR],
    RETURNCODE_FATAL: [names.ERROR],
}
