# -*- coding: utf-8 -*-
# gui.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Config for About
'''

DESCRIPTION = '''
<p><b>Process Launcher</b> is an account plugin that is able to start
 a process when the connection state changes.</p>
'''

HINT_ELEVATED = '''
<p>In order to limit the damage that can be done by the user
 <b>Elevated Rules</b> are restricted to selected set of executions.</p>
<p>It is not possible to pass parameters to elevated scripts. The scripts
 get environment variables from the connection.</p>
'''

HINT_ELEVATED_UNIX = '''
<p>Place custom scripts in the <br/><b>{LIBEXEC}</b><br/> directory, make
 them executable (700) and owned by root.</p>
<p>It is not possible to execute scripts or binaries outside that directory.
 </p>
'''

HINT_ELEVATED_WIN32 = '''
<p>Place custom scripts in the <b>{LIBEXEC}</b> directory and make
 them executable by the administrative user.
<p><i>*.bat</i>, <i>*.cmd</i> and <i>*.ps1/ps2</i> scripts
 are executed with the shell, <i>*.exe</i>-files are executed without
 an additional shell.</p>
<p><i>*.ps1/ps2</i> scripts need an execution policy active.</p>
<p>It is not possible to execute scripts or binaries outside that directory.
 </p>
'''

MESSAGE_FAILED_TEST = '''
<p>Failed to execute the command.</p>
<p>Review the logs.</p>
<p>Check that the executable is absolute (/usr/bin/...) or relative to the
 libexec/process_launcher installation directory.</p>
<p><b>Elevated</b> commands are only executed from the later location.</p>
'''

MESSAGE_INACTIVE_CANNOT_TEST = '''
<p>This rule is <b>inactive</b>.</p>
<p>To test it, make it active.</p>
'''

MESSAGE_ELEVATED_CANNOT_TEST = '''
<p>This rule is <b>elevated</b>.</p>
<p>Elevated rules rely on environment variables that cannot be emulated with a
 test. Enter the <br/><b>{LIBEXEC}</b><br/> path with administrative privileges
 and read the Readme.txt for instructions how to debug and test that scripts.
</p>
'''

MESSAGE_SUCCESS_TEST = '''
<p>Command executed with success.</p>
'''

MESSAGE_ELEVATED_ONLY_IN_LIBEXEC = '''
<p>The selected file is not located in <b>{LIBEXEC}</b>. For security
 reasons it is not allowed to elevate those executables.</p>
'''
