# -*- coding: utf-8 -*-
# process.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
from tornado import gen
from netsplice.util import get_logger
from netsplice.model.named_value_list import (
    named_value_list as named_value_list_model
)

logger = get_logger()


class process(object):
    '''
    '''
    def __init__(self, application):
        '''
        '''
        self.application = application

    @gen.coroutine
    def test(self, launch_item):
        '''
        Test.

        Execute the given launch_item in the correct backend.
        This method is a security issue as it allows any plugin to
        execute processes.
        '''
        if launch_item.elevated.get():
            process_launcher = \
                self.application.get_privileged().process_launcher
        else:
            process_launcher = \
                self.application.get_unprivileged().process_launcher

        empty_environment = named_value_list_model()

        result = yield process_launcher.launch(
            launch_item.executable.get(),
            launch_item.parameters.get(),
            launch_item.working_directory.get(),
            empty_environment)
        raise gen.Return(result)
