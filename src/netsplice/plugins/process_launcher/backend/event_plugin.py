# -*- coding: utf-8 -*-
# event_plugin.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Event plugin for backend app.

'''

from tornado import gen
import netsplice.backend.event.types as types
from netsplice.model.named_value_list import (
    named_value_list as named_value_list_model
)
from netsplice.util import get_logger
from netsplice.util.errors import NotFoundError

from netsplice.plugins.process_launcher.config.returncodes import (
    PRIVILEGED_RETURNCODES
)
from netsplice.plugins.process_launcher.unprivileged.process_launcher.model.\
    response.process_item import (
        process_item as process_item_model
    )
from netsplice.plugins.process_launcher.privileged.process_launcher.model.\
    response.process_returncode import (
        process_returncode as process_returncode_model
    )


logger = get_logger()


class event_plugin(object):
    '''
    Event loop for the backend.

    Requires modules registered and waits for model-change on that modules
    to process events.
    '''

    def __init__(self, application):
        '''
        Initialize members.

        Requires the user to set application and add modules.
        '''
        self.application = application

    def _find_rules(self, event_name, connection_id):
        '''
        Find Rules.

        Find Rules for the given connection_id that match the event.
        '''
        rules = []
        try:
            broker = self.application.get_module('connection').broker
            connection = broker.find_connection_by_connection_id(
                connection_id)
            account = connection.account

            launch_list = account.process_launcher.launch_list
            for item in launch_list:
                if item.active.get() is False:
                    continue
                if item.event.get() != event_name:
                    continue
                rules.append(item)
        except AttributeError:
            # plugin data not available
            pass
        except NotFoundError:
            # account id not found
            pass
        return rules

    @gen.coroutine
    def _trigger(self, launch_item, connection_id):
        '''
        Trigger.

        Launch with the commandline configured to create a process.
        '''
        environment = named_value_list_model()
        try:
            gui_model = self.application.get_module('gui').model
            connection = gui_model.connections.find_by_id(connection_id)
            environment.from_json(connection.environment.to_json())
            for (index, route) in enumerate(connection.routes):
                environment.add_value(
                    'netsplice_connection_route_%d'
                    % (index,), route.to_env())
        except NotFoundError:
            pass

        if launch_item.elevated.get():
            yield self._trigger_privileged(
                connection_id, launch_item, environment)
        else:
            yield self._trigger_unprivileged(
                connection_id, launch_item, environment)

    @gen.coroutine
    def _trigger_from_privileged_returncode(self, connection_id, returncode):
        '''
        Trigger from privileged returncode.

        Trigger events in core based on the returncode of a privileged
        launcher script.
        '''
        if returncode not in PRIVILEGED_RETURNCODES.keys():
            # Do nothing, when the returncode is not registered.
            return
        if PRIVILEGED_RETURNCODES[returncode] is None:
            # Do nothing, when the returncode has no registered events.
            return
        logger.warn(
            'The privileged script returned %d that causes %s events'
            % (returncode, str(PRIVILEGED_RETURNCODES[returncode])))
        for event_name in PRIVILEGED_RETURNCODES[returncode]:
            event_module = self.application.get_module('event')
            event_module.notify(event_name, connection_id)

    @gen.coroutine
    def _trigger_privileged(self, connection_id, launch_item, environment):
        '''
        Trigger.

        Launch with the commandline configured to create a process.
        '''
        process_launcher = \
            self.application.get_privileged().process_launcher
        process_returncode_json = yield process_launcher.launch(
            launch_item.executable.get(),
            launch_item.parameters.get(),
            launch_item.working_directory.get(),
            environment)
        process_returncode = process_returncode_model()
        process_returncode.from_json(process_returncode_json)
        yield self._trigger_from_privileged_returncode(
            connection_id, process_returncode.returncode.get())

    @gen.coroutine
    def _trigger_unprivileged(self, connection_id, launch_item, environment):
        '''
        Trigger.

        Launch with the commandline configured to create a process.
        '''
        # check if the id has a pid and this pid is active, do nothing then
        process_launcher = \
            self.application.get_unprivileged().process_launcher
        process_item_json = yield process_launcher.launch(
            launch_item.executable.get(),
            launch_item.parameters.get(),
            launch_item.working_directory.get(),
            environment)
        process_item = process_item_model()
        process_item.from_json(process_item_json)
        process_launcher.add_launched(
            launch_item.id.get(),
            process_item.pid.get(),
            process_item.commandline.get())

    @gen.coroutine
    def process_event(self, event_model_instance):
        '''
        Process the event in the event_model_instance.

        Evaluate the event-type, name and origin and call handler functions.
        '''
        event_name = event_model_instance.name.get()
        event_type = event_model_instance.type.get()
        event_data = event_model_instance.data.get()

        if event_type == types.NOTIFY:
            for rule in self._find_rules(event_name, event_data):
                yield self._trigger(rule, event_data)
