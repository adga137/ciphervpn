import mock
import uuid
import tornado
from tornado.testing import AsyncTestCase

from .event_plugin import event_plugin
from preferences.model.plugin_collection import (
    plugin_collection as process_launcher_model
)

import netsplice.backend.log as log_module
import netsplice.backend.gui as gui_module
import netsplice.backend.connection as connection_module
import netsplice.backend.connection.connection as connection
import netsplice.backend.preferences as preferences_module
import netsplice.backend.event as event_module
import netsplice.backend.event.origins as origins
import netsplice.backend.event.types as types
from netsplice.backend.event_loop import event_loop as event_loop
from netsplice.model.named_value_list import (
    named_value_list as named_value_list_model
)
from netsplice.model.event import event as event_model
from netsplice.util.errors import NotFoundError

from netsplice.backend.preferences.model.account_item import (
    account_item as account_item_model
)
from netsplice.util.ipc.application import application as application

import netsplice.config.connection as config_connection
import netsplice.config.process as config
import netsplice.config.flags as flags

app = application()
app.add_module(log_module)
app.add_module(gui_module)
app.add_module(event_module)
app.add_module(preferences_module)
app.add_module(connection_module)
app.set_event_loop(event_loop())

ACCOUNT_ID = '31c5aa5a-d1df-4ee8-9ede-86898effe163'
OTHER_ACCOUNT_ID = '31c5aa5a-d1df-4ee8-9ede-86898effe164'
CONNECTION_ID = 'c1c5aa5a-d1df-4ee8-9ede-86898effe163'


def add_account():
    pmodel = app.get_module('preferences').model
    account = account_item_model(None)
    account.id.set(ACCOUNT_ID)
    account.name.set('test')
    account.type.set_value(None)
    account.configuration.set('remote host')
    account.import_configuration.set('remote host')
    account.enabled.set(True)
    account.default_route.set(False)
    account.autostart.set(True)
    account.process_launcher = process_launcher_model(None)

    account.process_launcher.launch_list.append(get_launch_item())

    pmodel.accounts.append(account)
    connection_instance = connection(connection_module.broker, account)
    connection_instance.id.set(CONNECTION_ID)
    connection_instance.account = account
    connection_module.broker.connections[ACCOUNT_ID] = (
        connection_instance, None)


def add_connection(account_id):
    gui_model = app.get_module('gui').model
    connection = gui_model.connections.item_model_class()
    connection.account_id.set(account_id)
    connection.id.set(CONNECTION_ID)
    connection.routes.add('127.0.0.1', '255.255.255.0', '127.0.0.1')
    del gui_model.connections[:]
    gui_model.connections.append(connection)


def get_event_plugin():
    e = event_plugin(app)
    pmodel = app.get_module('preferences').model
    del pmodel.accounts[:]
    return e


def get_launch_item(account_id=None):
    li = None
    pmodel = app.get_module('preferences').model
    try:
        account = pmodel.accounts.find_by_id(account_id)
        li = account.process_launcher.launch_list[0]
    except NotFoundError:
        li = process_launcher_model(None).launch_list.item_model_class()
    li.active.set(True)
    li.event.set(config_connection.CONNECTED)
    return li


def test__find_rules_returns_empty_when_no_rules_for_account():
    e = get_event_plugin()
    result = e._find_rules('name', 'invalid_account_id')
    assert(result == [])


def test__find_rules_returns_empty_when_no_process_launcher_plugin_values():
    e = get_event_plugin()
    add_account()
    module_model = app.get_module('preferences').model
    account = module_model.accounts.find_by_id(ACCOUNT_ID)
    del account.process_launcher.launch_list

    result = e._find_rules('connect', CONNECTION_ID)
    del app.get_module('preferences').model.accounts[:]
    assert(result == [])


def test__find_rules_returns_empty_when_no_active_launch_items():
    e = get_event_plugin()
    add_account()

    lli = get_launch_item(ACCOUNT_ID)
    lli.active.set(False)

    result = e._find_rules('undefined_event', CONNECTION_ID)
    del app.get_module('preferences').model.accounts[:]
    assert(result == [])


def test__find_rules_returns_empty_when_no_match_event_in_launch_items():
    e = get_event_plugin()
    add_account()

    result = e._find_rules(config_connection.DISCONNECTED, CONNECTION_ID)
    del app.get_module('preferences').model.accounts[:]
    assert(result == [])


def test__find_rules_returns_launch_items():
    e = get_event_plugin()
    add_account()

    result = e._find_rules(config_connection.CONNECTED, CONNECTION_ID)
    del app.get_module('preferences').model.accounts[:]
    assert(len(result) == 1)


class EventLoopTests(AsyncTestCase):
    '''
    Event Loop Tests.

    Checks the async event_loop methods work
    '''

    @mock.patch(
        'netsplice.plugins.process_launcher.backend.event_plugin.'
        '_trigger_privileged')
    @tornado.testing.gen_test
    def test__trigger_calls_privileged(self, mock__trigger_privileged):
        li = get_launch_item()
        li.elevated.set(True)
        el = get_event_plugin()
        el._trigger(li, CONNECTION_ID)
        mock__trigger_privileged.assert_called()

    @mock.patch(
        'netsplice.plugins.process_launcher.backend.event_plugin.'
        '_trigger_unprivileged')
    @tornado.testing.gen_test
    def test__trigger_calls_unprivileged(self, mock__trigger_unprivileged):
        li = get_launch_item()
        li.elevated.set(False)
        el = get_event_plugin()
        el._trigger(li, CONNECTION_ID)
        mock__trigger_unprivileged.assert_called()

    @mock.patch(
        'netsplice.plugins.process_launcher.backend.event_plugin.'
        '_trigger_unprivileged')
    @tornado.testing.gen_test
    def test__trigger_adds_environment(self, mock__trigger_unprivileged):
        li = get_launch_item()
        li.elevated.set(False)
        env = named_value_list_model()
        env.add_value('a', 'b')
        el = get_event_plugin()
        add_account()
        add_connection(ACCOUNT_ID)
        gui_model = app.get_module('gui').model
        connection = gui_model.connections.find_by_id(CONNECTION_ID)
        connection.environment.from_json(env.to_json())
        el._trigger(li, CONNECTION_ID)
        del app.get_module('preferences').model.accounts[:]
        mock__trigger_unprivileged.assert_called()
        (account_id, cli, cenv) = mock__trigger_unprivileged.call_args[0]
        assert(cli == li)
        assert(cenv.get_value('a') == 'b')

    @mock.patch(
        'netsplice.plugins.process_launcher.backend.event_plugin.'
        '_trigger_unprivileged')
    @tornado.testing.gen_test
    def test__trigger_ignores_empty_id(self, mock__trigger_unprivileged):
        li = get_launch_item()
        li.elevated.set(False)
        env = named_value_list_model()
        env.add_value('a', 'b')
        el = get_event_plugin()
        add_account()
        add_connection(ACCOUNT_ID)
        gui_model = app.get_module('gui').model
        connection = gui_model.connections.find_by_id(CONNECTION_ID)
        connection.id.value = None
        el._trigger(li, CONNECTION_ID)
        del app.get_module('preferences').model.accounts[:]
        mock__trigger_unprivileged.assert_called_with(
            CONNECTION_ID, li, named_value_list_model())

    @mock.patch(
        'netsplice.plugins.process_launcher.backend.event_plugin.'
        '_trigger_unprivileged')
    @tornado.testing.gen_test
    def test__trigger_ignores_not_found(self, mock__trigger_unprivileged):
        li = get_launch_item()
        li.elevated.set(False)
        env = named_value_list_model()
        env.add_value('a', 'b')
        el = get_event_plugin()
        add_account()
        gui_model = app.get_module('gui').model
        del gui_model.connections[:]
        el._trigger(li, CONNECTION_ID)
        del app.get_module('preferences').model.accounts[:]
        mock__trigger_unprivileged.assert_called_with(
            CONNECTION_ID, li, named_value_list_model())

    @mock.patch(
        'netsplice.util.ipc.application.application.get_event_loop')
    @tornado.testing.gen_test
    def test__trigger_from_privileged_returncode_some_code_returns(
            self, mock_get_event_loop):
        el = get_event_plugin()
        el._trigger_from_privileged_returncode(ACCOUNT_ID, 254)
        mock_get_event_loop.assert_not_called()

    @mock.patch(
        'netsplice.util.ipc.application.application.get_event_loop')
    @tornado.testing.gen_test
    def test__trigger_from_privileged_returncode_ok_returns(
            self, mock_get_event_loop):
        el = get_event_plugin()
        yield el._trigger_from_privileged_returncode(ACCOUNT_ID, 0)
        mock_get_event_loop.assert_not_called()

    @mock.patch(
        'netsplice.plugins.process_launcher.backend.event_plugin.'
        '_find_rules')
    @mock.patch(
        'netsplice.plugins.process_launcher.backend.event_plugin.'
        '_trigger')
    @tornado.testing.gen_test
    def test_process_event(self, mock__trigger, mock__find_rules):
        el = get_event_plugin()
        emi = event_model()
        emi.name.set(config_connection.CONNECTED)
        emi.origin.set(origins.BACKEND)
        emi.type.set(types.NOTIFY)
        emi.data.set(CONNECTION_ID)
        mock__find_rules.return_value = ['mock_rule']
        el.process_event(emi)
        mock__trigger.assert_called()
