# -*- coding: utf-8 -*-
# plugin_collection.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Account.

Add a launch-list that is associated with the account.
'''

from netsplice.model.plugin_collection_item import (
    plugin_collection_item as plugin_collection_item_model
)
from .launch_list import launch_list as launch_list_model


class plugin_collection(plugin_collection_item_model):
    def __init__(self):
        plugin_collection_item_model.__init__(self, None)

        self.launch_list = launch_list_model()
