# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from .model.plugin import plugin as plugin_model
from .model.plugin_collection import (
    plugin_collection as plugin_collection_model
)
from .model.request.plugin_collection import (
    plugin_collection as request_plugin_collection_model
)
from .model.response.plugin_collection import (
    plugin_collection as response_plugin_collection_model
)
from .model.response.plugin import (
    plugin as response_plugin_model
)
from netsplice.plugins.process_launcher.config import PLUGIN_NAME


def register(app):
    '''
    Register.

    Register the plugin_model for persistent preferences and define
    overrides that are required for credential requests.
    '''
    model = app.get_module('preferences').model.accounts

    model.plugins.register_collection_plugin(
        PLUGIN_NAME, plugin_collection_model(model))
    model.response.plugins.register_collection_plugin(
        PLUGIN_NAME, response_plugin_collection_model())
    model.request.plugins.register_collection_plugin(
        PLUGIN_NAME, request_plugin_collection_model())

    model = app.get_module('preferences').model
    model.plugins.register_plugin(
        PLUGIN_NAME, plugin_model(model))
    model.response.plugins.register_plugin(
        PLUGIN_NAME, response_plugin_model())
