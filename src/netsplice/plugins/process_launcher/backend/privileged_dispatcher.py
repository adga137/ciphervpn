# -*- coding: utf-8 -*-
# privileged_dispatcher.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Dispatcher for unprivileged actions.
'''

from socket import error as socket_error

from tornado import gen, httpclient

from . import dispatcher_endpoints as endpoints
from netsplice.plugins.process_launcher.privileged.process_launcher.model.\
    request.launch_process import (
        launch_process as launch_process_model
    )
from netsplice.plugins.process_launcher.privileged.process_launcher.model.\
    response.process_returncode import (
        process_returncode as process_returncode_model
    )
from netsplice.util import get_logger
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.model.errors import ValidationError

logger = get_logger()


class privileged_dispatcher(object):

    def __init__(self, service_instance):
        self.service = service_instance

    @gen.coroutine
    def launch(self, executable, parameters, working_directory, environment):
        '''
        Launch Proccess.

        Launch the given commandline with the given parameters in
        the given workingdirectory.
        More secure would be to send an UUID only and let the unpriv backend
        decide the executable.
        '''
        try:
            request_model = launch_process_model()
            response_model = process_returncode_model()
            request_model.executable.set(executable)
            request_model.parameters.set(parameters)
            request_model.working_directory.set(working_directory)
            request_model.environment.from_json(environment.to_json())

            response = yield self.service.post(
                endpoints.PROCESS_LAUNCHER,
                request_model.to_json()
            )
            response_model.from_json(response.body)

            raise gen.Return(response_model.to_json())
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2263, str(errors))
            logger.error(str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2264, str(errors))
            logger.error(str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2265, str(errors))
            logger.error(str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2266, str(errors))
            logger.error(str(errors))
            raise ServerConnectionError(str(errors))
