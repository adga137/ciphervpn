# -*- coding: utf-8 -*-
# account_editor.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Account editor.

Extend Account Editor widget.
'''
import os
import cqc
import datetime
import codecs
import time
import threading
from PySide.QtGui import QWidget, QIcon, QPixmap
from PySide.QtCore import Qt, QTimer

from .account_editor_ui import Ui_AccountEditor

from netsplice.plugins.config_quality_check.config import gui as config

from cqc.spec import spec as cqc_spec
from cqc.spec.report import report as cqc_report
from cqc.spec.grammar.openvpn import openvpn as openvpn_parser
from cqc.spec.grammar.openssh import openssh as openssh_parser

from netsplice.util import get_logger

logger = get_logger()

# ms how long a reschedule of config_changed should wait for the
# async load thread to finish
RESCHEDULE_CONFIG_CHANGED_TIME = 300


class async_load_thread(threading.Thread):
    '''
    Helper class to load specs async from dialog.
    '''
    def __init__(self, parent):
        threading.Thread.__init__(self)
        self._parent = parent
        self.is_running = False

    def run(self):
        '''
        Load Specs.

        Load the specs and documentation for the registered parsers.
        Logger is not available in the thread.
        '''
        cqc_dirname = os.path.dirname(cqc.__file__)
        # mapping for type_names and cqc spec names.
        cqc_type_spec = {
            'OpenVPN': 'openvpn',
            'ssh': 'openssh',
            'Tor': 'tor'
        }

        for type_name in cqc_type_spec.keys():
            spec_file_name = os.path.join(
                cqc_dirname, 'res', '{ltype}.spec'.format(
                    ltype=cqc_type_spec[type_name]))
            spec_doc_file_name = os.path.join(
                cqc_dirname, 'res', '{ltype}.spec.{language}'.format(
                    ltype=cqc_type_spec[type_name],
                    language='en'))
            spec_doc_en_file_name = os.path.join(
                cqc_dirname, 'res', '{ltype}.spec.{language}'.format(
                    ltype=cqc_type_spec[type_name],
                    language='en'))

            if not os.path.exists(spec_file_name):
                print('No spec for %s' % (type_name,))
                continue
            if not os.path.exists(spec_doc_file_name):
                print('No spec-doc for %s / %s' % (type_name, 'en'))
                if not os.path.exists(spec_doc_en_file_name):
                    print('No english spec-doc for %s' % (type_name,))
                    continue

            spec = cqc_spec()
            with open(spec_file_name, mode='r') as fh:
                spec.load(fh)

            if os.path.exists(spec_doc_file_name):
                with open(spec_doc_file_name, mode='r') \
                        as fh:
                    spec.load_doc(fh)
            else:
                with open(spec_doc_en_file_name, mode='r') \
                        as fh:
                    spec.load_doc(fh)
            self._parent.spec[type_name] = spec

        self._parent._config_parser['ssh'] = openssh_parser()
        self._parent._config_parser['OpenVPN'] = openvpn_parser()
        self._parent._spec_loaded = True
        self.is_running = False


class account_editor(QWidget):
    '''
    Account Editor.

    Custom Editor for Config Quality Check options.
    '''

    def __init__(self, parent, dispatcher):
        '''
        Initialize Module.

        Initialize widget and abstract base, Setup UI.
        '''
        QWidget.__init__(self, parent)
        self.backend = dispatcher
        self.parent = parent
        self.ui = Ui_AccountEditor()
        self.ui.setupUi(self)
        self.tab_index = 2  # After advanced
        self.tab_ui = None
        self.setFocusPolicy(Qt.TabFocus)
        # values for cqc
        self.type = None
        self.configuration = None
        self.checked_configuration = None
        self.report = None
        self.spec = dict()
        # results from cqc
        self.broken = 0
        self.negative = 0
        self.positive = 0
        # async load of spec
        self._load_thread = async_load_thread(self)
        self._spec_loaded = False
        self._config_parser = dict()

        self.ICON_BROKEN = QIcon()
        self.ICON_NEGATIVE = QIcon()
        self.ICON_OK = QIcon()
        self._load_icons()

        self.parent.ui.tab_general.type_changed.connect(
            self._change_account_type)
        self.parent.ui.tab_editor.config_changed.connect(
            self._config_changed)

    def _load_specs_async(self):
        '''
        Load Specs Async.

        Avoid blocking the application when the specs are loaded.
        Unfortunately loading takes > 1s. A thread is started for this
        and the methods using the spec are blocked by _spec_loaded bit.
        '''
        if self._load_thread.is_running:
            return
        if self._spec_loaded:
            return
        self._load_thread.is_running = True
        self._load_thread.start()

    def _change_account_type(self, account_type):
        '''
        Change Account Type

        Handler for changing the account type on the general tab.

        Arguments:
            account_type -- string with account type
        '''
        self.type = account_type
        self.report = None
        self.checked_configuration = None
        self._change_tab_label()
        self._config_changed()

    def _change_tab_label(self):
        '''
        Change tab Icon.

        Set the tab icon depending on the broken and negative values.
        '''
        if not self._spec_loaded:
            return
        current_tab_index = self.tab_ui.indexOf(self)

        if self.type not in self.spec.keys():
            self.tab_ui.setTabEnabled(current_tab_index, False)
        else:
            self.tab_ui.setTabEnabled(current_tab_index, True)

        if self.broken > 0:
            self.tab_ui.setTabIcon(
                current_tab_index, self.ICON_BROKEN)
        elif self.negative > 0:
            self.tab_ui.setTabIcon(
                current_tab_index, self.ICON_NEGATIVE)
        else:
            self.tab_ui.setTabIcon(
                current_tab_index, self.ICON_OK)

    def _config_changed(self):
        '''
        Config Changed.

        Check if the relevant values for the config have changed and generate
        the values for the tab-label.
        '''
        if not self._spec_loaded:
            QTimer.singleShot(0, self._load_specs_async)
            QTimer.singleShot(
                RESCHEDULE_CONFIG_CHANGED_TIME, self._config_changed)
            return
        try:
            editor_model = self.parent.ui.tab_editor.ui.editor_model
            if self.checked_configuration == editor_model.configuration.get():
                return
            self.configuration = editor_model.configuration.get()
        except AttributeError:
            # no ui loaded for type yet
            return
        if self.configuration is None:
            return
        if self.type is None:
            return
        if self.type != self.parent.ui.tab_general.get_type():
            # type is changing, config_changed will emit again
            return

        config_parser = self._get_config_parser(self.type)
        if config_parser is None:
            return
        try:
            config_parser.parse_string(self.configuration)
            rate = self.spec[self.type].execute(config_parser.options)
            general_editor_model = self.parent.ui.tab_general.editor_model

            self.report = cqc_report(
                general_editor_model.name.get(),
                rate,
                datetime.datetime.now(),
                self.type)
            template_report = self.report.get_template_values()
            self.positive = template_report['rate'].sum_positive()
            self.negative = template_report['rate'].sum_negative()
            self.broken = template_report['rate'].sum_broken()
            self.checked_configuration = self.configuration
        except KeyError:
            self.positive = 0
            self.negative = 0
            self.broken = 0

        self._change_tab_label()

    def _get_config_parser(self, config_type):
        '''
        Get config parser.

        Return a instance of a cqc grammar parser.

        Arguments:
            config_type -- string
        '''
        try:
            return self._config_parser[config_type]
        except KeyError:
            return None

    def _load_icons(self):
        '''
        Load Icons.

        Load icons that are used for the tab. This loader is called
        once on initialize so the icons are reused.
        '''
        self.ICON_BROKEN.addPixmap(
            QPixmap(config.RES_ICON_BROKEN),
            QIcon.Normal, QIcon.Off)
        self.ICON_NEGATIVE.addPixmap(
            QPixmap(config.RES_ICON_NEGATIVE),
            QIcon.Normal, QIcon.Off)
        self.ICON_OK.addPixmap(
            QPixmap(config.RES_ICON_OK),
            QIcon.Normal, QIcon.Off)

    def _show_report(self):
        '''
        Report.

        Generate the HTML report and set it to the widget.
        '''
        if self.report is None:
            self._config_changed()
        if self.report is None:
            self.ui.report_html.setHtml(self.tr(config.NO_REPORT))
            return

        cqc_dirname = os.path.dirname(cqc.__file__)
        cqc_report_file = os.path.join(cqc_dirname, config.REPORT_FILE)
        with codecs.open(cqc_report_file, 'r', 'utf-8') as spec_report:
            self.report.set_template_file(spec_report)
            self.ui.report_html.setHtml(str(self.report))

    def config_values_changed(self):
        '''
        Config Values Changed.

        Evaluate if the config is different from the loaded values.
        '''
        changed = False
        return changed

    def showEvent(self, event):
        '''
        Show Event.

        Qt show event when the user selects the tab. Update the report.
        '''
        self._show_report()
