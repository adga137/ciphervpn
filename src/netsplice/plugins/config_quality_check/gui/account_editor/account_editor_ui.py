# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/plugins/config_quality_check/gui/account_editor/views/account_editor.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_AccountEditor(object):
    def setupUi(self, process_manager):
        process_manager.setObjectName("process_manager")
        process_manager.setMinimumSize(QtCore.QSize(100, 210))
        self.editor_layout = QtGui.QVBoxLayout(process_manager)
        self.editor_layout.setObjectName("editor_layout")
        self.report_html = QtGui.QTextBrowser(process_manager)
        self.report_html.setOpenExternalLinks(True)
        self.report_html.setObjectName("report_html")
        self.editor_layout.addWidget(self.report_html)

        self.retranslateUi(process_manager)
        QtCore.QMetaObject.connectSlotsByName(process_manager)

    def retranslateUi(self, process_manager):
        pass

