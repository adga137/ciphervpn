# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Config Quality Check Plugin.

Extends Netsplice with a check and report of the configuration properties.
'''
import sys

try:
    sys.modules['netsplice.gui']
    from . import gui
except KeyError:
    gui = None

from netsplice.plugins import openvpn as openvpn_plugin
from netsplice.plugins import ssh as ssh_plugin
from netsplice.plugins import tor as tor_plugin

this = sys.modules[__name__]

this.components = {
    'gui': gui,
}
this.dependencies = [
    openvpn_plugin,
    ssh_plugin,
    tor_plugin
]


def register(app, component):
    '''
    Register.

    Register the components of component with the given app instance.
    '''
    this.components[component].register(app)
