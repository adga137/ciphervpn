# -*- coding: utf-8 -*-
# gui.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Config for Config Quality Check
'''

DESCRIPTION = '''
<p><b>Config Quality Check</b> is an account plugin that is able to evaluate
the properties of the account configuration and produce a report with hints
how to enhance security, stability and performance.</p>
'''

TAB_LABEL = 'Config Quality'

REPORT_FILE = 'res/report.qthtml.jinja'

RES_ICON_BROKEN = (
    ':/plugins/config_quality_check/images/broken.png')
RES_ICON_NEGATIVE = (
    ':/plugins/config_quality_check/images/negative.png')
RES_ICON_OK = (
    ':/plugins/config_quality_check/images/ok.png')


NO_REPORT = '''

<body>
  <div class="content">
    <h1>Config Quality Report</h1>

    <p>There was an error processing the config. No report was generated.</p>
</body>
'''
