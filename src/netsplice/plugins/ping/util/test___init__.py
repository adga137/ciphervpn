# -*- coding: utf-8 -*-
# test___init__.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Test Ping utilities.
'''
import mock
from . import get_ping

REMOTE_IPv4 = '1.2.3.4'
REMOTE_IPv6 = '::2600'

@mock.patch.multiple('sys', platform='win32')
@mock.patch('os.getenv', return_value='c:\\windows;c:\\windows\\system32')
def test_get_ping_win32_ipv4_returns_correct_values(mock_os_getenv):
    result = get_ping(REMOTE_IPv4, ipv6=False, count=1, timeout=1)
    print(result)
    (executable_name, parameters, search_paths) = result
    assert(executable_name == 'ping')
    assert('-n' in parameters)  # check for count parameter
    assert('1' in parameters)  # check for count parameter
    assert('-w' in parameters)  # check for timeout parameter
    assert('1000' in parameters)  # check for timeout parameter
    assert('-4' in parameters)  # check for ipv6 parameter

    assert(REMOTE_IPv4 in parameters)  # check for remote parameter

    assert('c:\\windows' in search_paths)  # search_paths
    assert('c:\\windows\\system32' in search_paths)  # search_paths


@mock.patch.multiple('sys', platform='win32')
@mock.patch('os.getenv', return_value='c:\\windows;c:\\windows\\system32')
def test_get_ping_win32_ipv6_returns_correct_values(mock_os_getenv):
    result = get_ping(REMOTE_IPv6, ipv6=True, count=1, timeout=1)
    print(result)
    (executable_name, parameters, search_paths) = result
    assert(executable_name == 'ping')
    assert('-n' in parameters)  # check for count parameter
    assert('1' in parameters)  # check for count parameter
    assert('-w' in parameters)  # check for timeout parameter
    assert('1000' in parameters)  # check for timeout parameter
    assert('-6' in parameters)  # check for ipv6 parameter

    assert(REMOTE_IPv6 in parameters)  # check for remote parameter

    assert('c:\\windows' in search_paths)  # search_paths
    assert('c:\\windows\\system32' in search_paths)  # search_paths


@mock.patch.multiple('sys', platform='linux')
@mock.patch('os.getenv', return_value='/usr/bin:/usr/local/bin')
def test_get_ping_posix_ipv4_returns_correct_values(mock_os_getenv):
    result = get_ping(REMOTE_IPv4, ipv6=False, count=4, timeout=8)
    print(result)
    (executable_name, parameters, search_paths) = result
    assert(executable_name == 'ping')
    assert('-c' in parameters)  # check for count parameter
    assert('4' in parameters)  # check for count parameter
    assert('-w' in parameters)  # check for timeout parameter
    assert('8' in parameters)  # check for timeout parameter
    assert('-n' in parameters)  # check for noresolve parameter
    assert('-q' in parameters)  # check for quiet parameter
    assert('-4' in parameters)  # check for qipv6 parameter

    assert(REMOTE_IPv4 in parameters)  # check for remote parameter

    assert('/usr/bin' in search_paths)  # search_paths
    assert('/usr/local/bin' in search_paths)  # search_paths


@mock.patch.multiple('sys', platform='darwin')
@mock.patch('os.getenv', return_value='/usr/bin:/usr/local/bin')
def test_get_ping_win32_posix_returns_correct_values(mock_os_getenv):
    result = get_ping(REMOTE_IPv6, ipv6=True, count=4, timeout=8)
    print(result)
    (executable_name, parameters, search_paths) = result
    assert(executable_name == 'ping')
    assert('-c' in parameters)  # check for count parameter
    assert('4' in parameters)  # check for count parameter
    assert('-w' in parameters)  # check for timeout parameter
    assert('8' in parameters)  # check for timeout parameter
    assert('-n' in parameters)  # check for noresolve parameter
    assert('-q' in parameters)  # check for quiet parameter
    assert('-6' in parameters)  # check for ipv6 parameter

    assert(REMOTE_IPv6 in parameters)  # check for remote

    assert('/usr/bin' in search_paths)  # search_paths
    assert('/usr/local/bin' in search_paths)  # search_paths
