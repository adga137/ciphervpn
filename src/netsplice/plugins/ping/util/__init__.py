# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Ping utilities.
'''
import sys
import os


def get_ping(remote, ipv6=False, count=1, timeout=1):
    '''
    Get Ping Executable.

    Return the executable with the required parameters for ping on the current
    system.
    '''
    executable = 'ping'
    env_search_path = os.getenv('PATH', '')
    search_path = search_path = env_search_path.split(':')
    parameters = [remote]
    if sys.platform.startswith('win32'):
        parameters = [
            '-n', str(count),
            '-w', str(timeout * 1000)
        ]
        if ipv6:
            parameters.append('-6')
        else:
            parameters.append('-4')
        parameters.append(remote)
        search_path = env_search_path.split(';')
    elif sys.platform.startswith('darwin') or sys.platform.startswith('linux'):
        parameters = [
            '-c', str(count),
            '-n', '-q',
            '-w', str(timeout)
        ]
        if ipv6:
            parameters.append('-6')
        else:
            parameters.append('-4')
        parameters.append(remote)
    return (executable, parameters, search_path)
