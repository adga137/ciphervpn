# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
from netsplice.gui.account_edit.type_editor_factory import register_plugin
from .account_editor.account_editor import (
    account_editor as account_editor_widget
)

from netsplice.plugins.ping.config import DISPLAY_NAME

def register(app):
    '''
    Register.

    Register the account_types to the editor.
    '''

    register_plugin(DISPLAY_NAME, account_editor_widget)
