# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/plugins/ping/gui/account_edit/account_editor/views/account_editor.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_AccountEditor(object):
    def setupUi(self, ping):
        ping.setObjectName("ping")
        ping.setMinimumSize(QtCore.QSize(100, 200))
        self.editor_layout = QtGui.QVBoxLayout(ping)
        self.editor_layout.setObjectName("editor_layout")
        self.detail_stack = QtGui.QStackedWidget(ping)
        self.detail_stack.setObjectName("detail_stack")
        self.basic = QtGui.QWidget()
        self.basic.setObjectName("basic")
        self.basic_layout = QtGui.QGridLayout(self.basic)
        self.basic_layout.setObjectName("basic_layout")
        self.errors = QtGui.QLabel(self.basic)
        self.errors.setWordWrap(True)
        self.errors.setVisible(False)
        self.errors.setObjectName("errors")
        self.basic_layout.addWidget(self.errors, 0, 0, 1, 3)
        self.label_period = QtGui.QLabel(self.basic)
        self.label_period.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_period.setObjectName("label_period")
        self.basic_layout.addWidget(self.label_period, 1, 0, 1, 1)
        self.period = QtGui.QSpinBox(self.basic)
        self.period.setMinimum(0)
        self.period.setMaximum(999)
        self.period.setObjectName("period")
        self.basic_layout.addWidget(self.period, 1, 1, 1, 1)
        self.period_info = QtGui.QLabel(self.basic)
        self.period_info.setWordWrap(True)
        self.period_info.setObjectName("period_info")
        self.basic_layout.addWidget(self.period_info, 2, 1, 1, 2)
        self.label_remote = QtGui.QLabel(self.basic)
        self.label_remote.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_remote.setObjectName("label_remote")
        self.basic_layout.addWidget(self.label_remote, 3, 0, 1, 1)
        self.remote = QtGui.QComboBox(self.basic)
        self.remote.setEditable(True)
        self.remote.setObjectName("remote")
        self.basic_layout.addWidget(self.remote, 3, 1, 1, 2)
        self.remote_info = QtGui.QLabel(self.basic)
        self.remote_info.setWordWrap(True)
        self.remote_info.setObjectName("remote_info")
        self.basic_layout.addWidget(self.remote_info, 4, 1, 1, 2)
        self.option_list = key_value_editor(self.basic)
        self.option_list.setObjectName("option_list")
        self.basic_layout.addWidget(self.option_list, 5, 0, 1, 3)
        self.detail_stack.addWidget(self.basic)
        self.editor_layout.addWidget(self.detail_stack)

        self.retranslateUi(ping)
        self.detail_stack.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(ping)

    def retranslateUi(self, ping):
        self.label_period.setText(QtGui.QApplication.translate("AccountEditor", "<b>Period", None, QtGui.QApplication.UnicodeUTF8))
        self.label_remote.setText(QtGui.QApplication.translate("AccountEditor", "<b>Remote", None, QtGui.QApplication.UnicodeUTF8))

from netsplice.gui.key_value_editor.key_value_editor import key_value_editor
