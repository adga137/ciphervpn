# -*- coding: utf-8 -*-
# gui_dispatcher.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Dispatcher for unprivileged actions.
'''
import sys
from PySide import QtCore

from tornado import gen, httpclient

from netsplice.gui import backend_dispatcher_qt_replacement

from netsplice.util import get_logger
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.model.errors import ValidationError

logger = get_logger()

if sys.platform.startswith('darwin'):
    # Python only threads in gui.
    # Due to bugs in PySide osx may crash at random locations. Use this option
    # to use a threading implementation that mimics the emit/Signal code.
    Signal = backend_dispatcher_qt_replacement.Signal
    QObject = backend_dispatcher_qt_replacement.QObject
    QThread = backend_dispatcher_qt_replacement.QThread
    QMutex = backend_dispatcher_qt_replacement.QMutex
    QCoreApplication = backend_dispatcher_qt_replacement.QCoreApplication
else:
    Signal = QtCore.Signal
    QObject = QtCore.QObject
    QThread = QtCore.QThread
    QMutex = QtCore.QMutex
    QCoreApplication = QtCore.QCoreApplication


class gui_dispatcher(QObject):


    def __init__(self, owning_dispatcher):
        QObject.__init__(self)
        self.owner = owning_dispatcher
