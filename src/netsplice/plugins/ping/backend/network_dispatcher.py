# -*- coding: utf-8 -*-
# network_dispatcher.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Dispatcher for network actions.
'''
import os
from socket import error as socket_error

from tornado import gen, httpclient


from netsplice.plugins.ping.config import (
    backend as config
)

from netsplice.util import get_logger
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.model.errors import ValidationError

from . import dispatcher_endpoints as endpoints
from .net.model.request.setup import setup as setup_request_model
from .net.model.request.connection_id import (
    connection_id as connection_id_request_model
)

logger = get_logger()


class network_dispatcher(object):

    def __init__(self, service_instance):
        self.service = service_instance

    @gen.coroutine
    def start(self, connection_id, remote, period):
        '''
        Start.

        Start the ping for the given connection id.
        '''
        try:
            request_model = setup_request_model()
            request_model.remote.set(remote)
            request_model.period.set(period)
            request_model.connection_id.set(connection_id)
            response = yield self.service.post(
                endpoints.START, request_model.to_json()
            )

        except socket_error as errors:
            self.service.set_error_code(2319, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2320, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2322, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2323, str(errors))
            if errors.response is not None:
                headers = errors.response.headers.get_all()
                message = ''
                for (header_name, header_value) in sorted(headers):
                    if header_name != 'X-Errordetail':
                        continue
                    message += header_value
                message = message.replace('\\n', '\n')
                raise ServerConnectionError(message)

            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def stop(self, connection_id):
        '''
        Stop.

        Stop the ping for the given connection id.
        '''
        try:
            request_model = connection_id_request_model()
            request_model.id.set(connection_id)
            options = {
                'connection_id': request_model.id.get()
            }
            response = yield self.service.delete(
                endpoints.CONNECTION % options
            )

        except socket_error as errors:
            self.service.set_error_code(2324, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2325, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2326, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2327, str(errors))
            if errors.response is not None:
                headers = errors.response.headers.get_all()
                message = ''
                for (header_name, header_value) in sorted(headers):
                    if header_name != 'X-Errordetail':
                        continue
                    message += header_value
                message = message.replace('\\n', '\n')
                raise ServerConnectionError(message)

            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def status(self, connection_id):
        '''
        Status.

        Return the status for the ping loop for the connection id.
        '''
        try:
            request_model = connection_id_request_model()
            request_model.id.set(connection_id)
            options = {
                'connection_id': request_model.id.get()
            }
            response = yield self.service.get(
                endpoints.CONNECTION % options, None
            )

        except socket_error as errors:
            self.service.set_error_code(2328, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2329, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2330, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2331, str(errors))
            if errors.response is not None:
                headers = errors.response.headers.get_all()
                message = ''
                for (header_name, header_value) in sorted(headers):
                    if header_name != 'X-Errordetail':
                        continue
                    message += header_value
                message = message.replace('\\n', '\n')
                raise ServerConnectionError(message)

            raise ServerConnectionError(str(errors))

