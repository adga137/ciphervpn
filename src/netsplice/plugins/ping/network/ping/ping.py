# -*- coding: utf-8 -*-
# ping.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
OpenVPN Process Control. Uses a management connection that is setup() and
allows to connect, reconnect and disconnect the VPN connection using the
management socket.
'''

from tornado import ioloop
from netsplice.util import get_logger
from netsplice.config import flags
from netsplice.util.stream_to_log import FILTER_ALL
from netsplice.util.process.dispatcher import dispatcher as process_dispatcher
from netsplice.plugins.ping.config import backend as config_backend
from netsplice.plugins.ping.util import get_ping

logger = get_logger()


class ping(object):

    def __init__(self, model):
        '''
        Initialize members.
        '''
        self.model = model
        self._call_handle = None

    def stop(self):
        '''
        Stop Ping.

        Cancel any pending pings.
        '''
        ioloop.IOLoop.current().remove_timeout(self._call_handle)
        logger.info('Ping to %s stopped.' % (self.model.remote.get(),))

    def start(self):
        '''
        Start Ping.

        Schedule the the ping to the remote in the model.
        '''
        self.schedule(self.model.remote.get())
        logger.info(
            'Ping to %s every %d seconds started.'
            % (self.model.remote.get(), self.model.period.get()))

    def schedule(self, remote_ip):
        '''
        Schedule.

        Start a loop that that pings the given ip.
        '''
        self._call_handle = ioloop.IOLoop.current().call_later(
            self.model.period.get(),
            lambda: self.ping(remote_ip))

    def schedule_quit_process(self, process):
        '''
        Schedule Quit Process.

        Stop the process to avoid a zombie while the wait period.
        '''
        ioloop.IOLoop.current().call_later(
            (config_backend.COUNT * config_backend.TIMEOUT),
            lambda: process.stop())

    def ping(self, remote_ip):
        '''
        Ping Gateway.

        Ping the given gateway and reschedule the next ping.
        '''
        try:
            connection_id = self.model.id.get()
            ipv6 = False
            if ':' in remote_ip:
                ipv6 = True
            (ping_executable, ping_parameters, search_paths) = get_ping(
                remote_ip, ipv6=ipv6,
                count=config_backend.COUNT,
                timeout=config_backend.TIMEOUT)

            ping_process = process_dispatcher(
                ping_executable,
                ping_parameters,
                model=None)
            ping_process.set_custom_search_paths(search_paths)
            ping_process.set_logger_extra({
                'connection_id': connection_id
            })
            if flags.VERBOSE is 0:
                ping_process.set_logger_filter(FILTER_ALL)
            ping_process.start_subprocess()
            self.model.count.set(
                self.model.count.get() + 1)

            # schedule next interval
            self.schedule(remote_ip)
            # schedule kill of the process after timeout
            # otherwise ping zombies are in the os process list.
            self.schedule_quit_process(ping_process)
        except Exception as errors:
            logger.error(str(type(errors)))
            logger.warn(str(errors))
