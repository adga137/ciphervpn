# -*- coding: utf-8 -*-
# ping_status.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for connection id's used to validate requests.
This model is also implemented in the Backend.
'''
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable

from netsplice.model.validator.max import max as max_validator
from netsplice.model.validator.min import min as min_validator
from netsplice.model.validator.none import none as none_validator
from netsplice.model.validator.ip_address import (
    ip_address as ip_address_validator
)


class ping_status(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.period = field(
            required=True,
            validators=[min_validator(0), max_validator(999)])

        self.remote = field(
            required=True,
            validators=[none_validator(exp_or=[ip_address_validator()])])

        self.count = field(
            required=True,
            default=0,
            validators=[min_validator(0)])
