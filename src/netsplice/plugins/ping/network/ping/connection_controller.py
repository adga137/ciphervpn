# -*- coding: utf-8 -*-
# ping_controller.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.util.errors import NotFoundError
from .model.request.connection_id import (
    connection_id as connection_id_model
)
from .model.response.ping_status import (
    ping_status as ping_status_model
)

class connection_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def delete(self, connection_id):
        '''
        Stop connection ping.

        Stop the current active ping for the given connection_id.
        request model: {connection_id}
        '''
        request_model = connection_id_model()
        try:
            request_model.id.set(connection_id)
            model = self.application.get_module('ping').model
            connections = model.connections
            connections.delete_connection(request_model.id.get())
            self.set_status(201)
        except ValidationError:
            self.set_status(400)
        except NotFoundError:
            self.set_status(404)
        self.finish()

    def get(self, connection_id):
        '''
        Get information about the connection.

        Return the currently configured ping for the given connection_id
        response model: {period, remote, count}
        '''
        request_model = connection_id_model()
        response_model = ping_status_model()
        try:
            request_model.id.set(connection_id)
            model = self.application.get_module('ping').model
            connections = model.connections
            connection = connections.find_by_id(request_model.id.get())
            response_model.period.set(
                connection.period.get())
            response_model.remote.set(
                connection.remote.get())
            response_model.count.set(
                connection.count.get())
            self.write(response_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2339, errors)
            self.set_status(400)
        except NotFoundError as errors:
            self.set_error_code(2340, errors)
            self.set_status(404)
        self.finish()
