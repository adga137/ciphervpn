# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

import os
import sys

from netsplice.util.ipc.route import get_module_route
from .model import model as module_model

name = 'ping'

endpoints = get_module_route(
    'netsplice.plugins.ping.network',
    [
        (r'/module/plugin/ping/', 'module'),
        (r'/module/plugin/ping/connections/'
            '(?P<connection_id>[^\/]+)',
            'connection'),

    ])

model = module_model()
