# -*- coding: utf-8 -*-
# events.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
from netsplice.config import connection as config_connection

PING_MODEL = 'ping.model'

# Events emitted into the backend
OUTGOING = [
]

# Events from the backend
INCOMMING = [
    PING_MODEL,
    config_connection.UP,
    config_connection.DOWN,
]

# Events from the backend.net.event_controller
INCOMMING_NET = [
    PING_MODEL
]

# Events from the backend.privileged.event_controller
INCOMMING_PRIVILEGED = [
    PING_MODEL
]
