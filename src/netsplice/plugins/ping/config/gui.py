# -*- coding: utf-8 -*-
# gui.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Configuration for gui components.
'''

DESCRIPTION = '''
<p><b>Ping</b> is a plugin that sends a ICMP packet to a remote host with
configurable period length. The remote can be a ENV: from the connection
or a ipv4 and ipv6 address.</p>
<p>Pings are sent when the connection has entered a connected state using the
 operating system ping. This means the default route is used.
'''

PERIOD_INFO = '''
<p>Specify the number of seconds to wait between pings. A period of 0 disables
 the ping.
'''

SUGGESTED_REMOTES_INFO = '''
<p>The suggested remotes resemble a short list of the commonly used IPv6 and
 IPv4 IP's that are responding to pings. The ENV remotes may differ based on
 the current connection type and have to be selected carefully.
'''

SUGGESTED_REMOTES = [
    '2600::',
    '208.67.222.222',
    '208.67.220.220',
    '2620:0:ccc::2',
    '2620:0:ccd::2',
    '1.1.1.1',
    '4.2.2.1',
    '4.2.2.2',
    '8.8.8.8',
    '2001:4860:4860::8888',
    '8.8.4.4',
    '2001:4860:4860::8844',
    'ENV:connection_ifconfig_ipv6_remote',
    'ENV:connection_ifconfig_ipv6_netbits',
    'ENV:connection_ifconfig_ipv6_local',
    'ENV:connection_ifconfig_broadcast',
    'ENV:connection_ifconfig_netmask',
    'ENV:connection_ifconfig_local',
    'ENV:openvpn_redirect_gateway',
    'ENV:openvpn_route_ipv6_gateway_1',
    'ENV:openvpn_route_ipv6_network_1',
    'ENV:openvpn_route_gateway_1',
    'ENV:openvpn_route_netmask_1',
    'ENV:openvpn_route_network_1',
    'ENV:openvpn_route_vpn_gateway',
    'ENV:openvpn_route_net_gateway',
    'ENV:openvpn_ifconfig_local',
    'ENV:openvpn_ifconfig_ipv6_remote',
    'ENV:openvpn_ifconfig_ipv6_netbits',
    'ENV:openvpn_ifconfig_ipv6_local',
    'ENV:openvpn_ifconfig_broadcast',
    'ENV:openvpn_ifconfig_netmask',
    'ENV:openvpn_foreign_option_3',
    'ENV:openvpn_foreign_option_2',
    'ENV:openvpn_foreign_option_1',
    'ENV:openvpn_trusted_ip',
    'ENV:openvpn_untrusted_ip',
    'ENV:openvpn_remote_1',
    'ENV:openvpn_remote_2',
    'ENV:openvpn_remote_3',
]
