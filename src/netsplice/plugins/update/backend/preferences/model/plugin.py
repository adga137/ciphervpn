# -*- coding: utf-8 -*-
# plugin.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Preferences. Defines how the user input is persisted
'''
from netsplice import __version__ as VERSION

from netsplice.model.plugin_item import (
    plugin_item as plugin_item_model
)
from netsplice.model.validator.boolean import (
    boolean as boolean_validator
)
from netsplice.model.validator.min import (
    min as min_validator
)
from netsplice.model.validator.max import (
    max as max_validator
)
from netsplice.model.validator.none import (
    none as none_validator
)
from netsplice.model.validator.uri import (
    uri as uri_validator
)
from netsplice.model.validator.version import (
    version as version_validator
)
from netsplice.model.validator.sign_key import (
    sign_key as sign_key_validator
)
from netsplice.model.validator.plaintext import (
    plaintext as plaintext_validator
)
from .url_list import url_list
from .fingerprint_list import fingerprint_list

from netsplice.util import get_logger
from netsplice.util.model.field import field

from netsplice.plugins.update.config import backend as config

logger = get_logger()


class plugin(plugin_item_model):
    def __init__(self, owner):
        plugin_item_model.__init__(self, owner)

        self.active = field(
            required=True,
            default=config.UPDATE_ACTIVE,
            validators=[boolean_validator()])

        self.auto_check = field(
            required=True,
            default=config.UPDATE_AUTO_CHECK,
            validators=[boolean_validator()])

        # no config.backend value
        self.last = field(
            required=True,
            default=None,
            validators=[none_validator(exp_or=[min_validator(0)])])

        # no config.backend value
        self.latest_known_version = field(
            required=True,
            default=VERSION,
            validators=[version_validator()])

        self.maximum_age = field(
            required=True,
            default=config.UPDATE_MAXIMUM_AGE,
            validators=[min_validator(0), max_validator(config.MAXIMUM_AGE)])

        self.update_url = field(
            required=True,
            default=config.UPDATE_URL,
            validators=[uri_validator()])

        self.update_signature_url = field(
            required=True,
            default=config.UPDATE_SIGNATURE_URL,
            validators=[uri_validator()])

        self.fingerprints = fingerprint_list()
        self.fingerprints.set_list(config.UPDATE_URL_FINGERPRINTS)

        self.sign_key = field(
            required=True,
            validators=[none_validator(exp_or=[sign_key_validator()])])
        self.sign_key.set(config.UPDATE_URL_SIGN_KEY)

        # no config.backend value
        self.update_url_list = url_list()

        # no config.backend value
        self.changelog = field(
            required=True,
            default=None,
            validators=[none_validator(exp_or=[plaintext_validator()])])

    def apply_values(self):
        '''
        Apply Values.

        Apply values from the preferences to the config so other
        backend components can use the values.
        '''
        config.UPDATE_ACTIVE = \
            self.active.get()

        config.UPDATE_AUTO_CHECK = \
            self.auto_check.get()

        config.UPDATE_MAXIMUM_AGE = \
            self.maximum_age.get()

        config.UPDATE_URL = \
            self.update_url.get()
        config.UPDATE_SIGNATURE_URL = \
            self.update_signature_url.get()

        config.UPDATE_URL_FINGERPRINTS = []
        for fingerprint in self.fingerprints:
            config.UPDATE_URL_FINGERPRINTS.append(
                str(fingerprint.fingerprint.get()))

        config.UPDATE_URL_SIGN_KEY = \
            self.sign_key.get()
        if config.UPDATE_URL_SIGN_KEY == 'None' \
                or config.UPDATE_URL_SIGN_KEY == '':
            config.UPDATE_URL_SIGN_KEY = None
