# -*- coding: utf-8 -*-
# event_plugin.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Event plugin for backend app.

'''

import datetime
import timeago
from tornado import gen, ioloop
from netsplice import __version__ as VERSION

import netsplice.backend.event.names as event_names
import netsplice.backend.event.types as event_types
from netsplice.plugins.update.config import backend as config
from netsplice.plugins.update.config import events as update_event_names
from netsplice.util import get_logger
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.model.errors import ValidationError

logger = get_logger()


class event_plugin(object):
    '''
    Event loop for the backend.

    Requires modules registered and waits for model-change on that modules
    to process events.
    '''

    def __init__(self, application):
        '''
        Initialize members.

        Requires the user to set application and add modules.
        '''
        self.application = application

    @gen.coroutine
    def process_event(self, event_model_instance):
        '''
        Process the event in the event_model_instance.

        Evaluate the event-type, name and origin and call handler functions.
        '''
        event_name = event_model_instance.name.get()
        event_type = event_model_instance.type.get()

        if event_type == event_types.NOTIFY:
            if event_name == event_names.BACKEND_READY:
                yield self.backend_ready()
            if event_name == update_event_names.UPDATE_CHECK_NOW:
                yield self.check_now()

    @gen.coroutine
    def backend_ready(self):
        '''
        Backend Ready.

        The backend has completed loading. Run the  automatic checks and
        schedule a check when no notification was created.
        '''
        notified = False
        if not config.UPDATE_ACTIVE:
            logger.warn(
                'Update not active, no startup or runtime notifications will'
                ' be displayed.')
            return

        if config.UPDATE_AUTO_CHECK:
            notified = yield self.check_now()

        if not notified:
            notified = self.check_age()
            if not notified:
                notified = self.check_version()
            if not notified:
                self.schedule_check_age()

    def check_age(self):
        '''
        Check Age.

        Check the age of the last_check with the current time. Notify the GUI
        when the age exceeds maximum_age.
        '''
        preferences_model = self.application.get_module(
            'preferences').model
        model = preferences_model.plugins.find_by_name('update')
        maximum_age = model.maximum_age.get() * (3600 * 24)
        last_check = model.last.get()
        if last_check is None:
            last_check = ioloop.IOLoop.current().time()
        current_age = ioloop.IOLoop.current().time() - last_check
        logger.debug('Current application age: %ds' % (current_age,))
        if current_age > maximum_age:
            self.notify_gui_age(current_age)
            return True
        return False

    def check_version(self):
        '''
        Check Version.

        Check the current version with the latest known version. When the last
        known version is newer than the current version, notify the GUI.
        '''
        preferences_model = self.application.get_module(
            'preferences').model
        model = preferences_model.plugins.find_by_name('update')
        current_version = VERSION
        logger.debug('Current application version: %s' % (current_version,))
        if model.latest_known_version.get() > current_version:
            self.notify_gui_version_available(model.latest_known_version.get())
            return True
        return False

    @gen.coroutine
    def check_now(self):
        '''
        Check Now.

        Request the latest version info from the remote service and compare it
        to the current stored values. Update the current stored values.

        Decorators:
            gen.coroutine
        '''
        try:
            latest = yield self.application.get_network().update.latest()
            preferences_model = self.application.get_module(
                'preferences').model
            model = preferences_model.plugins.find_by_name('update')
            model.last.set(int(ioloop.IOLoop.current().time()))
            model.latest_known_version.set(latest.version.get())
            model.commit()

            current_version = VERSION
            if latest.version.get() > current_version:
                self.notify_gui_version_available(latest.version.get())
                raise gen.Return(True)
        except ServerConnectionError as errors:
            logger.warn(
                'Problem with update service: %s'
                % (str(errors),))
        except ValidationError as errors:
            logger.error(
                'Problem with update service: %s'
                % (str(errors),))
        raise gen.Return(False)

    def notify_gui_age(self, age):
        '''
        Notify the UI that it has reached its maximum age.

        This method displays a popup that reminds the user to run a check
        for updates.

        Arguments:
            age (integer): Age in seconds since the last check.
        '''
        now = datetime.datetime.utcnow()
        age_date = datetime.datetime.fromtimestamp(
            int(ioloop.IOLoop.current().time()) - age)
        str_age = timeago.format(age_date, now)

        logger.info(
            'A check for updates should run. Last run was %s.'
            % (str_age,))
        events = self.application.get_module('gui').model.events
        events.notify(
            self.application,
            update_event_names.UPDATE_NOTIFY_AGE,
            str_age)

    def notify_gui_version_available(self, version):
        '''
        Notify the UI that a known version is available.

        This method displays a popup that a previous check found that a newer
        version is available, but the application still runs as the older
        version.

        Arguments:
            version (string): Known new version string.
        '''
        logger.info(
            'A new version is available: %s'
            % (version,))
        events = self.application.get_module('gui').model.events
        events.notify(
            self.application,
            update_event_names.UPDATE_NOTIFY_VERSION,
            version)

    def schedule_check_age(self):
        '''
        Schedule Check Age.

        Schedule a age check during application run.
        '''
        preferences_model = self.application.get_module(
            'preferences').model
        model = preferences_model.plugins.find_by_name('update')
        maximum_age = model.maximum_age.get()
        last = model.last.get()
        if last is None:
            last = 0
        if maximum_age is None:
            maximum_age = config.MAXIMUM_AGE

        schedule_time = last + (maximum_age * (3600 * 24) + 1)
        ioloop.IOLoop.current().call_at(
            schedule_time, self.check_age)
