# -*- coding: utf-8 -*-
# network_dispatcher.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Dispatcher for network actions.
'''
import os
from socket import error as socket_error

from tornado import gen, httpclient
from urllib3.util.url import parse_url

from . import dispatcher_endpoints as endpoints

from netsplice.plugins.update.network.update.model.response.update import (
    update as update_response_model
)
from netsplice.plugins.update.network.update.model.request.update import (
    update as update_request_model
)
from netsplice.plugins.update.config import (
    backend as config
)

from netsplice.util import get_logger
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.model.errors import ValidationError

logger = get_logger()

# Errors that are handled by the GUI and do not need the default logviewer
# handling.
HANDLED_ERRORS = [
    # /check_now network plugin endpoint -> remote
    2302,  # HKPK failed, buffer or Signature Error in network->remote
    # /latest network plugin endpoint -> remote
    2317,  # HKPK failed, Buffer or Signature Error in network->remote
    2316,  # 404 for update url or update_url.sig

    # self.download_latest
    2290,  # HTTP:502 in backend->network: signature verification
    # self.latest
    2299,  # HTTP:502 in backend->network: signature verification
]


class network_dispatcher(object):

    def __init__(self, service_instance):
        self.service = service_instance
        gui_module = self.service.application.get_module('gui')
        for error_code in HANDLED_ERRORS:
            gui_module.model.events.handle_error(error_code)

    @gen.coroutine
    def download_latest(self, update_os, destination):
        '''
        Download Latest.

        Download the latest update file for the given update_os to
        the destination directory.
        '''
        try:
            latest = yield self.latest()
            update_item = latest.update_url_list.find_by_os(update_os)
            update_url = update_item.url.get()
            signature = update_item.signature.get()

            url = parse_url(update_url)
            update_filename = os.path.basename(url.path)
            destination_file = os.path.join(
                destination, update_filename)

            sign_key = config.UPDATE_URL_SIGN_KEY
            fingerprints = config.UPDATE_URL_FINGERPRINTS
            sha1sum = update_item.sha1sum.get()
            sha256sum = update_item.sha256sum.get()

            download = yield self.service.download(
                update_url,
                fingerprints,
                destination_file,
                signature=signature,
                sign_key=sign_key,
                sha1sum=sha1sum,
                sha256sum=sha256sum
            )
            raise gen.Return(download)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2287, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2288, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2289, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2290, str(errors))
            if errors.response is not None:
                headers = errors.response.headers.get_all()
                message = ''
                for (header_name, header_value) in sorted(headers):
                    if header_name != 'X-Errordetail':
                        continue
                    message += header_value
                message = message.replace('\\n', '\n')
                raise ServerConnectionError(message)

            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def latest(self):
        '''
        Latest.

        Request the remote for the latest update information, check
        the signature and handle errors.
        '''
        try:
            request_model = update_request_model()
            request_model.update_url.set(config.UPDATE_URL)
            request_model.update_signature_url.set(
                config.UPDATE_SIGNATURE_URL)
            request_model.fingerprints.set_list(
                config.UPDATE_URL_FINGERPRINTS)
            request_model.sign_key.set(config.UPDATE_URL_SIGN_KEY)
            response = yield self.service.post(
                endpoints.UPDATE_LATEST, request_model.to_json()
            )

            response_model = update_response_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2296, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2297, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2298, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2299, str(errors))
            if errors.response is not None:
                headers = errors.response.headers.get_all()
                message = ''
                for (header_name, header_value) in sorted(headers):
                    if header_name != 'X-Errordetail':
                        continue
                    message += header_value
                message = message.replace('\\n', '\n')
                raise ServerConnectionError(message)

            raise ServerConnectionError(str(errors))
