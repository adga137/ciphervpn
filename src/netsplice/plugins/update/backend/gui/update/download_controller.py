# -*- coding: utf-8 -*-
# download_controller.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from tornado import gen, httpclient

from netsplice.util.errors import NotFoundError
from netsplice.util.ipc.middleware import middleware
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.model.errors import ValidationError
from netsplice.backend.net.model.response.download_id import (
    download_id as download_id_response_model
)
from netsplice.backend.net.model.response.download_status import (
    download_status as download_status_response_model
)
from netsplice.backend.net.model.request.download_id import (
    download_id as download_id_request_model
)
from netsplice.util.hkpk.errors import TargetError
from .model.request.download import download as download_request_model


class download_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    @gen.coroutine
    def delete(self, download_id):
        '''
        Cancel current download.

        .. :code-block:: none

            request model: {
                "id": "id of a active download",
            }
            response_model: {
            }

        '''
        try:
            request_model = download_id_request_model()
            request_model.id.set(download_id)
            network = self.application.get_network()
            yield network.cancel_download(
                request_model.id.get())
            self.set_status(202)
        except ValidationError as errors:
            self.set_error_code(2308, str(errors))
            self.set_status(400)
        except NotFoundError as errors:
            self.set_error_code(2309, str(errors))
            self.set_status(404)
        except TargetError as errors:
            self.set_error_code(2310, str(errors))
            self.set_status(403)
        except ServerConnectionError as errors:
            self.set_error_code(2311, str(errors))
            self.set_status(502)
        self.finish()

    @gen.coroutine
    def post(self):
        '''
        Trigger event in backend

        .. code-block:: none

            request model: {
                "os": "os to download the update for",
                "destination": "directory to store the update in"
            }
            response_model: {
                "id": "id of download for monitoring progress"
            }
        '''
        try:
            request_model = download_request_model()
            request_model.from_json(self.request.body.decode('utf-8'))
            response_model = download_id_response_model()
            update = self.application.get_network().update
            download = yield update.download_latest(
                request_model.os.get(), request_model.destination.get())
            response_model.from_json(download.to_json())
            self.write(response_model.to_json())
        except ValidationError as errors:
            self.set_error_code(2304, str(errors))
            self.set_status(400)
        except NotFoundError as errors:
            self.set_error_code(2305, str(errors))
            self.set_status(404)
        except TargetError as errors:
            self.set_error_code(2306, str(errors))
            self.set_status(403)
        except ServerConnectionError as errors:
            self.set_error_code(2307, str(errors))
            self.set_status(502)
        except httpclient.HTTPError as errors:
            self.set_error_code(2318, str(errors))
            if errors.response is not None:
                headers = errors.response.headers.get_all()
                message = ''
                for (header_name, header_value) in sorted(headers):
                    if header_name != 'X-Errordetail':
                        continue
                    message += header_value
                message = message.replace('\\n', '\n')
                self.set_header('X-Errordetail', message)
            else:
                self.set_header('X-Errordetail', str(errors))
            self.set_status(502)
        self.finish()

    @gen.coroutine
    def get(self, download_id):
        '''
        Get current download status.

        .. code-block:: none

            request model: {
                "id": "id of a active download",
            }
            response_model: {
                "url": "url that is being downloaded",
                "start_date": "date when the download started",
                "complete": "integer how complete the download is",
                "bps": "integer how fast the download is progressing",
                "errors": "boolean indicating a error occurred",
                "error_message": "message of a possible error",
                "signature_ok": null/true/false,
                "checksum_ok": null/true/false

            }
        '''
        try:
            request_model = download_id_request_model()
            request_model.id.set(download_id)
            response_model = download_status_response_model()
            network = self.application.get_network()
            status = yield network.download_status(
                request_model.id.get())
            response_model.from_json(status.to_json())
            self.write(response_model.to_json())
        except ValidationError as errors:
            self.set_error_code(2332, str(errors))
            self.set_status(400)
        except NotFoundError as errors:
            self.set_error_code(2334, str(errors))
            self.set_status(404)
        except TargetError as errors:
            self.set_error_code(2335, str(errors))
            self.set_status(403)
        except ServerConnectionError as errors:
            self.set_error_code(2336, str(errors))
            self.set_status(502)
        self.finish()
