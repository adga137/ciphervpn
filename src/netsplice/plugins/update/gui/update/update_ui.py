# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/plugins/update/gui/update/views/update.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Update(object):
    def setupUi(self, update):
        update.setObjectName("update")
        update.setGeometry(QtCore.QRect(0, 0, 700, 680))
        self.update_layout = QtGui.QVBoxLayout(update)
        self.update_layout.setObjectName("update_layout")
        self.update_message_line = QtGui.QWidget(update)
        self.update_message_line.setObjectName("update_message_line")
        self.update_message_line_layout = QtGui.QHBoxLayout(self.update_message_line)
        self.update_message_line_layout.setContentsMargins(0, 0, 0, 0)
        self.update_message_line_layout.setObjectName("update_message_line_layout")
        self.application_icon_widget = QtGui.QWidget(self.update_message_line)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.application_icon_widget.sizePolicy().hasHeightForWidth())
        self.application_icon_widget.setSizePolicy(sizePolicy)
        self.application_icon_widget.setMinimumSize(QtCore.QSize(122, 0))
        self.application_icon_widget.setMaximumSize(QtCore.QSize(122, 122))
        self.application_icon_widget.setObjectName("application_icon_widget")
        self.vboxlayout = QtGui.QVBoxLayout(self.application_icon_widget)
        self.vboxlayout.setContentsMargins(0, 0, 0, 0)
        self.vboxlayout.setObjectName("vboxlayout")
        self.application_icon_center_widget = QtGui.QWidget(self.application_icon_widget)
        self.application_icon_center_widget.setObjectName("application_icon_center_widget")
        self.hboxlayout = QtGui.QHBoxLayout(self.application_icon_center_widget)
        self.hboxlayout.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout.setObjectName("hboxlayout")
        spacerItem = QtGui.QSpacerItem(8, 0, QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Minimum)
        self.hboxlayout.addItem(spacerItem)
        self.application_icon = QtGui.QLabel(self.application_icon_center_widget)
        self.application_icon.setProperty("maximumSize", QtCore.QSize(64, 64))
        self.application_icon.setObjectName("application_icon")
        self.hboxlayout.addWidget(self.application_icon)
        spacerItem1 = QtGui.QSpacerItem(8, 0, QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Minimum)
        self.hboxlayout.addItem(spacerItem1)
        self.vboxlayout.addWidget(self.application_icon_center_widget)
        spacerItem2 = QtGui.QSpacerItem(132, 0, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.vboxlayout.addItem(spacerItem2)
        self.update_message_line_layout.addWidget(self.application_icon_widget)
        self.message_label = QtGui.QLabel(self.update_message_line)
        self.message_label.setWordWrap(True)
        self.message_label.setOpenExternalLinks(True)
        self.message_label.setTextInteractionFlags(QtCore.Qt.TextBrowserInteraction)
        self.message_label.setObjectName("message_label")
        self.update_message_line_layout.addWidget(self.message_label)
        self.loading_icon = QtGui.QLabel(self.update_message_line)
        self.loading_icon.setProperty("maximumSize", QtCore.QSize(32, 32))
        self.loading_icon.setObjectName("loading_icon")
        self.update_message_line_layout.addWidget(self.loading_icon)
        self.update_layout.addWidget(self.update_message_line)
        self.message_details = QtGui.QLabel(update)
        self.message_details.setWordWrap(True)
        self.message_details.setOpenExternalLinks(True)
        self.message_details.setTextInteractionFlags(QtCore.Qt.TextBrowserInteraction)
        self.message_details.setObjectName("message_details")
        self.update_layout.addWidget(self.message_details)
        self.active = QtGui.QCheckBox(update)
        self.active.setChecked(True)
        self.active.setObjectName("active")
        self.update_layout.addWidget(self.active)
        self.active_info_widget = QtGui.QWidget(update)
        self.active_info_widget.setObjectName("active_info_widget")
        self.hboxlayout1 = QtGui.QHBoxLayout(self.active_info_widget)
        self.hboxlayout1.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout1.setObjectName("hboxlayout1")
        spacerItem3 = QtGui.QSpacerItem(132, 0, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.hboxlayout1.addItem(spacerItem3)
        self.active_info = QtGui.QLabel(self.active_info_widget)
        self.active_info.setWordWrap(True)
        self.active_info.setOpenExternalLinks(True)
        self.active_info.setTextInteractionFlags(QtCore.Qt.TextBrowserInteraction)
        self.active_info.setObjectName("active_info")
        self.hboxlayout1.addWidget(self.active_info)
        self.update_layout.addWidget(self.active_info_widget)
        self.auto_check = QtGui.QCheckBox(update)
        self.auto_check.setChecked(True)
        self.auto_check.setObjectName("auto_check")
        self.update_layout.addWidget(self.auto_check)
        self.auto_check_info_widget = QtGui.QWidget(update)
        self.auto_check_info_widget.setObjectName("auto_check_info_widget")
        self.hboxlayout2 = QtGui.QHBoxLayout(self.auto_check_info_widget)
        self.hboxlayout2.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout2.setObjectName("hboxlayout2")
        spacerItem4 = QtGui.QSpacerItem(132, 0, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.hboxlayout2.addItem(spacerItem4)
        self.auto_check_info = QtGui.QLabel(self.auto_check_info_widget)
        self.auto_check_info.setWordWrap(True)
        self.auto_check_info.setOpenExternalLinks(True)
        self.auto_check_info.setTextInteractionFlags(QtCore.Qt.TextBrowserInteraction)
        self.auto_check_info.setObjectName("auto_check_info")
        self.hboxlayout2.addWidget(self.auto_check_info)
        self.update_layout.addWidget(self.auto_check_info_widget)
        self.update_server_widget = QtGui.QWidget(update)
        self.update_server_widget.setObjectName("update_server_widget")
        self.hboxlayout3 = QtGui.QHBoxLayout(self.update_server_widget)
        self.hboxlayout3.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout3.setObjectName("hboxlayout3")
        self.update_url_label = QtGui.QLabel(self.update_server_widget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.update_url_label.sizePolicy().hasHeightForWidth())
        self.update_url_label.setSizePolicy(sizePolicy)
        self.update_url_label.setMinimumSize(QtCore.QSize(120, 0))
        self.update_url_label.setObjectName("update_url_label")
        self.hboxlayout3.addWidget(self.update_url_label)
        self.update_url = QtGui.QLineEdit(self.update_server_widget)
        self.update_url.setObjectName("update_url")
        self.hboxlayout3.addWidget(self.update_url)
        spacerItem5 = QtGui.QSpacerItem(145, 0, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.hboxlayout3.addItem(spacerItem5)
        self.update_layout.addWidget(self.update_server_widget)
        self.update_server_info_widget = QtGui.QWidget(update)
        self.update_server_info_widget.setObjectName("update_server_info_widget")
        self.hboxlayout4 = QtGui.QHBoxLayout(self.update_server_info_widget)
        self.hboxlayout4.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout4.setObjectName("hboxlayout4")
        spacerItem6 = QtGui.QSpacerItem(132, 0, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.hboxlayout4.addItem(spacerItem6)
        self.update_server_info = QtGui.QLabel(self.update_server_info_widget)
        self.update_server_info.setWordWrap(True)
        self.update_server_info.setOpenExternalLinks(True)
        self.update_server_info.setTextInteractionFlags(QtCore.Qt.TextBrowserInteraction)
        self.update_server_info.setObjectName("update_server_info")
        self.hboxlayout4.addWidget(self.update_server_info)
        self.update_layout.addWidget(self.update_server_info_widget)
        self.update_os_widget = QtGui.QWidget(update)
        self.update_os_widget.setObjectName("update_os_widget")
        self.hboxlayout5 = QtGui.QHBoxLayout(self.update_os_widget)
        self.hboxlayout5.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout5.setObjectName("hboxlayout5")
        self.update_os_label = QtGui.QLabel(self.update_os_widget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.update_os_label.sizePolicy().hasHeightForWidth())
        self.update_os_label.setSizePolicy(sizePolicy)
        self.update_os_label.setMinimumSize(QtCore.QSize(120, 0))
        self.update_os_label.setObjectName("update_os_label")
        self.hboxlayout5.addWidget(self.update_os_label)
        self.os_select = QtGui.QComboBox(self.update_os_widget)
        self.os_select.setObjectName("os_select")
        self.hboxlayout5.addWidget(self.os_select)
        spacerItem7 = QtGui.QSpacerItem(145, 0, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.hboxlayout5.addItem(spacerItem7)
        self.update_layout.addWidget(self.update_os_widget)
        self.download_destination_widget = QtGui.QWidget(update)
        self.download_destination_widget.setObjectName("download_destination_widget")
        self.hboxlayout6 = QtGui.QHBoxLayout(self.download_destination_widget)
        self.hboxlayout6.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout6.setObjectName("hboxlayout6")
        self.download_destination_label = QtGui.QLabel(self.download_destination_widget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.download_destination_label.sizePolicy().hasHeightForWidth())
        self.download_destination_label.setSizePolicy(sizePolicy)
        self.download_destination_label.setMinimumSize(QtCore.QSize(120, 0))
        self.download_destination_label.setObjectName("download_destination_label")
        self.hboxlayout6.addWidget(self.download_destination_label)
        self.download_destination = QtGui.QLineEdit(self.download_destination_widget)
        self.download_destination.setObjectName("download_destination")
        self.hboxlayout6.addWidget(self.download_destination)
        self.button_change_download_destination = QtGui.QPushButton(self.download_destination_widget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.button_change_download_destination.sizePolicy().hasHeightForWidth())
        self.button_change_download_destination.setSizePolicy(sizePolicy)
        self.button_change_download_destination.setMinimumSize(QtCore.QSize(120, 0))
        self.button_change_download_destination.setObjectName("button_change_download_destination")
        self.hboxlayout6.addWidget(self.button_change_download_destination)
        self.update_layout.addWidget(self.download_destination_widget)
        self.download_url_widget = QtGui.QWidget(update)
        self.download_url_widget.setObjectName("download_url_widget")
        self.vboxlayout1 = QtGui.QVBoxLayout(self.download_url_widget)
        self.vboxlayout1.setContentsMargins(0, 0, 0, 0)
        self.vboxlayout1.setObjectName("vboxlayout1")
        self.download_url_url_widget = QtGui.QWidget(self.download_url_widget)
        self.download_url_url_widget.setObjectName("download_url_url_widget")
        self.hboxlayout7 = QtGui.QHBoxLayout(self.download_url_url_widget)
        self.hboxlayout7.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout7.setObjectName("hboxlayout7")
        self.download_url_label = QtGui.QLabel(self.download_url_url_widget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.download_url_label.sizePolicy().hasHeightForWidth())
        self.download_url_label.setSizePolicy(sizePolicy)
        self.download_url_label.setMinimumSize(QtCore.QSize(120, 0))
        self.download_url_label.setObjectName("download_url_label")
        self.hboxlayout7.addWidget(self.download_url_label)
        self.download_url = QtGui.QLabel(self.download_url_url_widget)
        self.download_url.setOpenExternalLinks(True)
        self.download_url.setTextInteractionFlags(QtCore.Qt.TextBrowserInteraction)
        self.download_url.setObjectName("download_url")
        self.hboxlayout7.addWidget(self.download_url)
        self.vboxlayout1.addWidget(self.download_url_url_widget)
        self.download_url_sha1sum_widget = QtGui.QWidget(self.download_url_widget)
        self.download_url_sha1sum_widget.setObjectName("download_url_sha1sum_widget")
        self.hboxlayout8 = QtGui.QHBoxLayout(self.download_url_sha1sum_widget)
        self.hboxlayout8.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout8.setObjectName("hboxlayout8")
        self.download_url_sha1sum_label = QtGui.QLabel(self.download_url_sha1sum_widget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.download_url_sha1sum_label.sizePolicy().hasHeightForWidth())
        self.download_url_sha1sum_label.setSizePolicy(sizePolicy)
        self.download_url_sha1sum_label.setMinimumSize(QtCore.QSize(120, 0))
        self.download_url_sha1sum_label.setObjectName("download_url_sha1sum_label")
        self.hboxlayout8.addWidget(self.download_url_sha1sum_label)
        self.download_url_sha1sum = QtGui.QLabel(self.download_url_sha1sum_widget)
        self.download_url_sha1sum.setOpenExternalLinks(True)
        self.download_url_sha1sum.setTextInteractionFlags(QtCore.Qt.TextBrowserInteraction)
        self.download_url_sha1sum.setObjectName("download_url_sha1sum")
        self.hboxlayout8.addWidget(self.download_url_sha1sum)
        self.vboxlayout1.addWidget(self.download_url_sha1sum_widget)
        self.download_url_sha256sum_widget = QtGui.QWidget(self.download_url_widget)
        self.download_url_sha256sum_widget.setObjectName("download_url_sha256sum_widget")
        self.hboxlayout9 = QtGui.QHBoxLayout(self.download_url_sha256sum_widget)
        self.hboxlayout9.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout9.setObjectName("hboxlayout9")
        self.download_url_sha256sum_label = QtGui.QLabel(self.download_url_sha256sum_widget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.download_url_sha256sum_label.sizePolicy().hasHeightForWidth())
        self.download_url_sha256sum_label.setSizePolicy(sizePolicy)
        self.download_url_sha256sum_label.setMinimumSize(QtCore.QSize(120, 0))
        self.download_url_sha256sum_label.setObjectName("download_url_sha256sum_label")
        self.hboxlayout9.addWidget(self.download_url_sha256sum_label)
        self.download_url_sha256sum = QtGui.QLabel(self.download_url_sha256sum_widget)
        self.download_url_sha256sum.setOpenExternalLinks(True)
        self.download_url_sha256sum.setTextInteractionFlags(QtCore.Qt.TextBrowserInteraction)
        self.download_url_sha256sum.setObjectName("download_url_sha256sum")
        self.hboxlayout9.addWidget(self.download_url_sha256sum)
        self.vboxlayout1.addWidget(self.download_url_sha256sum_widget)
        self.update_layout.addWidget(self.download_url_widget)
        self.progress_widget = QtGui.QWidget(update)
        self.progress_widget.setVisible(False)
        self.progress_widget.setObjectName("progress_widget")
        self.vboxlayout2 = QtGui.QVBoxLayout(self.progress_widget)
        self.vboxlayout2.setContentsMargins(0, 0, 0, 0)
        self.vboxlayout2.setObjectName("vboxlayout2")
        self.progress_progress_widget = QtGui.QWidget(self.progress_widget)
        self.progress_progress_widget.setObjectName("progress_progress_widget")
        self.hboxlayout10 = QtGui.QHBoxLayout(self.progress_progress_widget)
        self.hboxlayout10.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout10.setObjectName("hboxlayout10")
        self.progress_bar = QtGui.QProgressBar(self.progress_progress_widget)
        self.progress_bar.setMinimum(0)
        self.progress_bar.setMaximum(100)
        self.progress_bar.setObjectName("progress_bar")
        self.hboxlayout10.addWidget(self.progress_bar)
        self.progress_bps_label = QtGui.QLabel(self.progress_progress_widget)
        self.progress_bps_label.setObjectName("progress_bps_label")
        self.hboxlayout10.addWidget(self.progress_bps_label)
        self.vboxlayout2.addWidget(self.progress_progress_widget)
        self.progress_cancel_widget = QtGui.QWidget(self.progress_widget)
        self.progress_cancel_widget.setObjectName("progress_cancel_widget")
        self.hboxlayout11 = QtGui.QHBoxLayout(self.progress_cancel_widget)
        self.hboxlayout11.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout11.setObjectName("hboxlayout11")
        spacerItem8 = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.hboxlayout11.addItem(spacerItem8)
        self.button_cancel_download = QtGui.QPushButton(self.progress_cancel_widget)
        self.button_cancel_download.setObjectName("button_cancel_download")
        self.hboxlayout11.addWidget(self.button_cancel_download)
        self.vboxlayout2.addWidget(self.progress_cancel_widget)
        self.update_layout.addWidget(self.progress_widget)
        self.download_complete = QtGui.QWidget(update)
        self.download_complete.setVisible(False)
        self.download_complete.setObjectName("download_complete")
        self.vboxlayout3 = QtGui.QVBoxLayout(self.download_complete)
        self.vboxlayout3.setContentsMargins(0, 0, 0, 0)
        self.vboxlayout3.setObjectName("vboxlayout3")
        self.download_complete_file = QtGui.QWidget(self.download_complete)
        self.download_complete_file.setObjectName("download_complete_file")
        self.hboxlayout12 = QtGui.QHBoxLayout(self.download_complete_file)
        self.hboxlayout12.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout12.setObjectName("hboxlayout12")
        self.success_icon_file = QtGui.QLabel(self.download_complete_file)
        self.success_icon_file.setProperty("maximumSize", QtCore.QSize(24, 24))
        self.success_icon_file.setObjectName("success_icon_file")
        self.hboxlayout12.addWidget(self.success_icon_file)
        self.download_complete_file_label = QtGui.QLabel(self.download_complete_file)
        self.download_complete_file_label.setObjectName("download_complete_file_label")
        self.hboxlayout12.addWidget(self.download_complete_file_label)
        self.vboxlayout3.addWidget(self.download_complete_file)
        self.download_complete_checksum = QtGui.QWidget(self.download_complete)
        self.download_complete_checksum.setObjectName("download_complete_checksum")
        self.hboxlayout13 = QtGui.QHBoxLayout(self.download_complete_checksum)
        self.hboxlayout13.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout13.setObjectName("hboxlayout13")
        self.success_icon_checksum = QtGui.QLabel(self.download_complete_checksum)
        self.success_icon_checksum.setProperty("maximumSize", QtCore.QSize(24, 24))
        self.success_icon_checksum.setObjectName("success_icon_checksum")
        self.hboxlayout13.addWidget(self.success_icon_checksum)
        self.download_complete_checksum_label = QtGui.QLabel(self.download_complete_checksum)
        self.download_complete_checksum_label.setObjectName("download_complete_checksum_label")
        self.hboxlayout13.addWidget(self.download_complete_checksum_label)
        self.vboxlayout3.addWidget(self.download_complete_checksum)
        self.download_complete_signature = QtGui.QWidget(self.download_complete)
        self.download_complete_signature.setObjectName("download_complete_signature")
        self.hboxlayout14 = QtGui.QHBoxLayout(self.download_complete_signature)
        self.hboxlayout14.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout14.setObjectName("hboxlayout14")
        self.success_icon_signature = QtGui.QLabel(self.download_complete_signature)
        self.success_icon_signature.setProperty("maximumSize", QtCore.QSize(24, 24))
        self.success_icon_signature.setObjectName("success_icon_signature")
        self.hboxlayout14.addWidget(self.success_icon_signature)
        self.download_complete_signature_label = QtGui.QLabel(self.download_complete_signature)
        self.download_complete_signature_label.setObjectName("download_complete_signature_label")
        self.hboxlayout14.addWidget(self.download_complete_signature_label)
        self.vboxlayout3.addWidget(self.download_complete_signature)
        self.download_complete_info_open = QtGui.QWidget(self.download_complete)
        self.download_complete_info_open.setObjectName("download_complete_info_open")
        self.hboxlayout15 = QtGui.QHBoxLayout(self.download_complete_info_open)
        self.hboxlayout15.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout15.setObjectName("hboxlayout15")
        self.download_complete_info = QtGui.QLabel(self.download_complete_info_open)
        self.download_complete_info.setWordWrap(True)
        self.download_complete_info.setOpenExternalLinks(True)
        self.download_complete_info.setTextInteractionFlags(QtCore.Qt.TextBrowserInteraction)
        self.download_complete_info.setObjectName("download_complete_info")
        self.hboxlayout15.addWidget(self.download_complete_info)
        self.button_download_complete_open = QtGui.QPushButton(self.download_complete_info_open)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.button_download_complete_open.sizePolicy().hasHeightForWidth())
        self.button_download_complete_open.setSizePolicy(sizePolicy)
        self.button_download_complete_open.setMinimumSize(QtCore.QSize(120, 0))
        self.button_download_complete_open.setObjectName("button_download_complete_open")
        self.hboxlayout15.addWidget(self.button_download_complete_open)
        self.vboxlayout3.addWidget(self.download_complete_info_open)
        self.update_layout.addWidget(self.download_complete)
        self.download_error_widget = QtGui.QWidget(update)
        self.download_error_widget.setVisible(False)
        self.download_error_widget.setObjectName("download_error_widget")
        self.hboxlayout16 = QtGui.QHBoxLayout(self.download_error_widget)
        self.hboxlayout16.setContentsMargins(0, 0, 0, 0)
        self.hboxlayout16.setObjectName("hboxlayout16")
        self.error_icon = QtGui.QLabel(self.download_error_widget)
        self.error_icon.setProperty("maximumSize", QtCore.QSize(24, 24))
        self.error_icon.setProperty("minimumSize", QtCore.QSize(24, 24))
        self.error_icon.setObjectName("error_icon")
        self.hboxlayout16.addWidget(self.error_icon)
        self.message_error = QtGui.QLabel(self.download_error_widget)
        self.message_error.setOpenExternalLinks(True)
        self.message_error.setTextInteractionFlags(QtCore.Qt.TextBrowserInteraction)
        self.message_error.setWordWrap(True)
        self.message_error.setObjectName("message_error")
        self.hboxlayout16.addWidget(self.message_error)
        self.update_layout.addWidget(self.download_error_widget)
        spacerItem9 = QtGui.QSpacerItem(5, 5, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.MinimumExpanding)
        self.update_layout.addItem(spacerItem9)
        self.action_layout = QtGui.QHBoxLayout()
        self.action_layout.setObjectName("action_layout")
        spacerItem10 = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.action_layout.addItem(spacerItem10)
        self.button_check_now = QtGui.QPushButton(update)
        self.button_check_now.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.button_check_now.setObjectName("button_check_now")
        self.action_layout.addWidget(self.button_check_now)
        self.button_download = QtGui.QPushButton(update)
        self.button_download.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.button_download.setObjectName("button_download")
        self.action_layout.addWidget(self.button_download)
        self.button_ok = QtGui.QPushButton(update)
        self.button_ok.setProperty("minimumSize", QtCore.QSize(120, 0))
        self.button_ok.setObjectName("button_ok")
        self.action_layout.addWidget(self.button_ok)
        self.update_layout.addLayout(self.action_layout)

        self.retranslateUi(update)
        QtCore.QMetaObject.connectSlotsByName(update)

    def retranslateUi(self, update):
        update.setWindowTitle(QtGui.QApplication.translate("Update", "Netsplice Update", None, QtGui.QApplication.UnicodeUTF8))
        self.active.setText(QtGui.QApplication.translate("Update", "Active", None, QtGui.QApplication.UnicodeUTF8))
        self.auto_check.setText(QtGui.QApplication.translate("Update", "Autocheck on start", None, QtGui.QApplication.UnicodeUTF8))
        self.update_url_label.setText(QtGui.QApplication.translate("Update", "<b>Update Url", None, QtGui.QApplication.UnicodeUTF8))
        self.update_os_label.setText(QtGui.QApplication.translate("Update", "<b>Platform", None, QtGui.QApplication.UnicodeUTF8))
        self.download_destination_label.setText(QtGui.QApplication.translate("Update", "<b>Download Path", None, QtGui.QApplication.UnicodeUTF8))
        self.button_change_download_destination.setText(QtGui.QApplication.translate("Update", "Browse", None, QtGui.QApplication.UnicodeUTF8))
        self.download_url_label.setText(QtGui.QApplication.translate("Update", "<b>Download Url", None, QtGui.QApplication.UnicodeUTF8))
        self.download_url_sha1sum_label.setText(QtGui.QApplication.translate("Update", "<b>SHA1 Hash", None, QtGui.QApplication.UnicodeUTF8))
        self.download_url_sha256sum_label.setText(QtGui.QApplication.translate("Update", "<b>SHA256 Hash", None, QtGui.QApplication.UnicodeUTF8))
        self.button_cancel_download.setText(QtGui.QApplication.translate("Update", "Cancel Download", None, QtGui.QApplication.UnicodeUTF8))
        self.download_complete_file_label.setText(QtGui.QApplication.translate("Update", "Download is complete.", None, QtGui.QApplication.UnicodeUTF8))
        self.download_complete_checksum_label.setText(QtGui.QApplication.translate("Update", "Checksums match.", None, QtGui.QApplication.UnicodeUTF8))
        self.download_complete_signature_label.setText(QtGui.QApplication.translate("Update", "Signature is ok.", None, QtGui.QApplication.UnicodeUTF8))
        self.download_complete_info.setText(QtGui.QApplication.translate("Update", "\n"
"                        ", None, QtGui.QApplication.UnicodeUTF8))
        self.button_download_complete_open.setText(QtGui.QApplication.translate("Update", "Open", None, QtGui.QApplication.UnicodeUTF8))
        self.message_error.setText(QtGui.QApplication.translate("Update", "Download failed.", None, QtGui.QApplication.UnicodeUTF8))
        self.button_check_now.setText(QtGui.QApplication.translate("Update", "Check Now", None, QtGui.QApplication.UnicodeUTF8))
        self.button_download.setText(QtGui.QApplication.translate("Update", "Download and Verify", None, QtGui.QApplication.UnicodeUTF8))
        self.button_ok.setText(QtGui.QApplication.translate("Update", "Ok", None, QtGui.QApplication.UnicodeUTF8))

