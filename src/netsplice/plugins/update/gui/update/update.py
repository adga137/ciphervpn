# -*- coding: utf-8 -*-
# update.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Update Dialog.

This dialog allows the user to control the update.
'''
import datetime
import timeago
import os

from PySide.QtGui import (
    QPixmap, QMovie, QDesktopServices, QFileDialog
)
from PySide.QtCore import (
    Signal, Qt, QTimer, QUrl
)
from .update_ui import Ui_Update
from netsplice import __version__ as VERSION
from netsplice.config.gui import (
    RES_ICON_APPLICATION, RES_ICON_LOADING, RES_ICON_STATE_FAILURE,
    RES_ICON_STATE_SUCCESS
)
from netsplice.config import about as config_about
from netsplice.backend.net.model.response.download_id import (
    download_id as download_id_response_model
)
from netsplice.backend.net.model.response.download_status import (
    download_status as download_status_response_model
)
from netsplice.gui.widgets.dialog import dialog
from netsplice.plugins.update.config import backend as config_backend
from netsplice.plugins.update.config import gui as config_gui
from netsplice.plugins.update.gui.model.response.version_detail \
    import (
        version_detail as response_version_detail_model
    )
from netsplice.plugins.update.config import PLUGIN_NAME
from netsplice.plugins.update.util import get_os
from netsplice.util import get_logger, basestring, escape_html

logger = get_logger()

STATE_CHECKING = 'checking'
STATE_DOWNLOADING = 'downloading'
STATE_UPDATING_PREFERENCES = 'updating_preferences'
STATE_IDLE = 'idle'

# Timeout when changes to the url will be saved automatically
AUTOSAVE_TIMEOUT = 2000

# Timeout when the dialog text is updated
UPDATE_MESSAGE_TIMEOUT = 10000


class update(dialog):
    '''
    About Widget.

    Custom Widget that gets embedded in the core about dialog.
    '''

    backend_done = Signal(basestring)
    backend_failed = Signal(basestring)
    check_now_done = Signal(basestring)
    version_info_loaded = Signal(basestring)
    download_started = Signal(basestring)
    download_status = Signal(basestring)
    preferences_plugin_url_done = Signal(basestring)
    preferences_plugin_signature_url_done = Signal(basestring)

    def __init__(self, parent=None, dispatcher=None):
        '''
        Initialize Module.

        Initialize widget and abstract base, Setup UI.
        '''
        dialog.__init__(self, parent)
        self.mainwindow = parent
        self.backend = dispatcher
        self.ui = Ui_Update()
        self.ui.setupUi(self)
        application_logo = QPixmap(RES_ICON_APPLICATION)
        self.loading_icon = QMovie(RES_ICON_LOADING)
        self._autosave_timer = QTimer()
        self._update_text_timer = QTimer()
        self._version_detail = None
        self._update_url = None
        self._state = STATE_IDLE

        self.ui.application_icon.setPixmap(application_logo)
        self.ui.application_icon.setScaledContents(True)

        self.ui.error_icon.setPixmap(QPixmap(RES_ICON_STATE_FAILURE))
        self.ui.error_icon.setScaledContents(True)
        self.ui.success_icon_file.setPixmap(QPixmap(RES_ICON_STATE_SUCCESS))
        self.ui.success_icon_file.setScaledContents(True)
        self.ui.success_icon_checksum.setPixmap(
            QPixmap(RES_ICON_STATE_SUCCESS))
        self.ui.success_icon_checksum.setScaledContents(True)
        self.ui.success_icon_signature.setPixmap(
            QPixmap(RES_ICON_STATE_SUCCESS))
        self.ui.success_icon_signature.setScaledContents(True)

        self.ui.loading_icon.setMovie(self.loading_icon)
        self.ui.loading_icon.setScaledContents(True)
        self.ui.loading_icon.hide()

        self.setWindowTitle(self.tr('Update Netsplice - %s') % (VERSION,))

        self.backend_failed.connect(self._backend_failed)
        self.version_info_loaded.connect(self._version_info_loaded)
        self.check_now_done.connect(self._check_now_done)
        self.download_started.connect(self._download_started)
        self.download_status.connect(self._download_status)
        self.preferences_plugin_signature_url_done.connect(
            self._preferences_plugin_signature_url_done)
        self.preferences_plugin_url_done.connect(
            self._preferences_plugin_url_done)

        self.ui.button_check_now.clicked.connect(self._check_now)
        self.ui.button_download.clicked.connect(self._download)
        self.ui.button_ok.clicked.connect(self._ok)
        self.ui.button_download_complete_open.clicked.connect(self._open)
        self.ui.button_cancel_download.clicked.connect(self._download_cancel)

        self.ui.active.stateChanged.connect(
            self._active_changed)
        self.ui.auto_check.stateChanged.connect(
            self._auto_check_changed)
        self.ui.update_url.textChanged.connect(
            self._update_url_changing)
        self.ui.os_select.currentIndexChanged.connect(
            self._update_os_changed)
        self.ui.download_destination.textChanged.connect(
            self._check_download_destination)
        self.ui.button_change_download_destination.clicked.connect(
            self._change_download_destination)
        self.ui.button_check_now.setEnabled(True)
        self.ui.button_download.setEnabled(False)

        event_loop = self.backend.application.event_loop
        event_loop.preferences_plugins_changed.connect(
            self._preferences_plugins_changed)
        self._preferences_plugins_changed(
            self.backend.application.get_model().preferences.plugins)

        self.ui.active_info.setText(self.tr(
            config_gui.ACTIVE_INFO))
        self.ui.auto_check_info.setText(self.tr(
            config_gui.AUTO_CHECK_INFO))
        self.ui.update_server_info.setText(self.tr(
            config_gui.UPDATE_SERVER_INFO))
        self.ui.download_complete_info.setText(self.tr(
            config_gui.DOWNLOAD_COMPLETE_INFO))

        self._autosave_timer.timeout.connect(self._update_url_changed)
        self._autosave_timer.setSingleShot(True)
        self._autosave_timer.setInterval(AUTOSAVE_TIMEOUT)
        self._update_text_timer.timeout.connect(self._update_message_text)
        self._update_text_timer.setSingleShot(False)
        self._update_text_timer.setInterval(UPDATE_MESSAGE_TIMEOUT)
        self._update_text_timer.start()

        self._set_default_download_destination()

    def _active_changed(self, new_state):
        '''
        Active Changed.

        The state of the active checkbox changed. Store the value to the
        preferences.

        Arguments:
            new_state (boolean): State the checkbox was set by the user.
        '''
        bstate = False
        if new_state == Qt.Checked:
            bstate = True
        self.ui.auto_check.setEnabled(bstate)
        self.backend.preferences_plugin_set.emit(
            PLUGIN_NAME, 'active', bstate,
            self.backend_done, self.backend_failed)

    def _auto_check_changed(self, new_state):
        '''
        Auto check changed.

        The state of the auto_check checkbox changed. Store the value to the
        preferences.

        Arguments:
            new_state (boolean): State of the checkbox was set by the user.
        '''
        bstate = False
        if new_state == Qt.Checked:
            bstate = True
        self.backend.preferences_plugin_set.emit(
            PLUGIN_NAME, 'auto_check', bstate,
            self.backend_done, self.backend_failed)

    def _backend_failed(self, message):
        '''
        Backend Failed.

        A error in the backend occured. Display it to the user.

        Arguments:
            message (string): error message
        '''
        self.ui.download_error_widget.show()
        message_error = self.tr(config_gui.MESSAGE_ERROR)
        self.ui.message_error.setText(message_error.format(
            URL_MANUAL=config_about.PRODUCT_URL,
            ERROR_MESSAGE=escape_html(message).replace('\n', '<br/>')))
        self.ui.button_check_now.setEnabled(True)
        self.ui.update_url.setEnabled(True)
        if self._state == STATE_DOWNLOADING:
            self.ui.button_download.setEnabled(True)
        else:
            self.ui.button_download.setEnabled(False)
        self.ui.download_complete.hide()
        self.ui.loading_icon.hide()
        self.loading_icon.stop()
        self._state = STATE_IDLE

    def _change_download_destination(self):
        '''
        Change Download Destination.

        Change the directory where the update download will be located.
        Display a Directory only dialog
        '''
        dialog = QFileDialog(self)
        dialog.setFileMode(QFileDialog.Directory)
        dialog.setViewMode(QFileDialog.List)
        dialog.setDirectory(
            self.ui.download_destination.text())

        if not dialog.exec_():
            return

        selected = dialog.selectedFiles()
        for directory in selected:
            self.ui.download_destination.setText(directory)
            self._check_download_destination()

    def _check_download_destination(self):
        '''
        Check Download Destination.

        Check that the selected destination directory exists.
        '''
        self.ui.button_download.setEnabled(False)
        if self._version_detail is None:
            return
        destination = str(self.ui.download_destination.text())

        if os.path.isdir(destination):
            self.ui.button_download.setEnabled(True)

    def _check_now(self):
        '''
        Check Now.

        Request the backend to request a remote service for the latest version
        infos.
        '''
        self._state = STATE_CHECKING
        self._version_detail = None
        self.backend.update.check_now.emit(
            self.check_now_done, self.backend_failed)

        self.ui.button_check_now.setEnabled(False)
        self.ui.button_download.setEnabled(False)
        self.ui.update_url.setEnabled(False)
        self.ui.os_select.setEnabled(False)
        self.ui.download_destination.setEnabled(False)
        self.ui.button_change_download_destination.setEnabled(False)

        self.ui.loading_icon.show()
        self.ui.message_error.setText('')
        self.ui.download_error_widget.hide()
        self.ui.download_complete.hide()
        self.loading_icon.start()

    def _check_now_done(self):
        '''
        Check Now Done.

        Request the backend to for the latest version infos.
        '''
        self.backend.update.version_info.emit(
            self.version_info_loaded, self.backend_failed)

        self.ui.button_check_now.setEnabled(True)
        self.ui.update_url.setEnabled(True)
        self.ui.os_select.setEnabled(True)
        self.ui.download_destination.setEnabled(True)
        self.ui.button_change_download_destination.setEnabled(True)

        self.ui.loading_icon.hide()
        self.loading_icon.stop()
        self._state = STATE_IDLE

    def _download(self):
        '''
        Download.

        Request the backend to download the latest version.
        '''
        destination = self.ui.download_destination.text()
        os = self.ui.os_select.itemData(
            self.ui.os_select.currentIndex())
        self._state = STATE_DOWNLOADING
        self.backend.update.download.emit(
            os,
            destination,
            self.download_started, self.backend_failed)

        self.ui.progress_widget.show()
        self.ui.progress_bar.setValue(0)

        self.ui.update_url.setEnabled(False)
        self.ui.button_download.setEnabled(False)
        self.ui.button_cancel_download.setEnabled(False)
        self.ui.button_check_now.setEnabled(False)
        self.ui.os_select.setEnabled(False)
        self.ui.download_destination.setEnabled(False)
        self.ui.button_change_download_destination.setEnabled(False)

        self.ui.download_complete.hide()
        self.ui.message_error.setText('')
        self.ui.download_error_widget.hide()
        self.loading_icon.start()
        self.ui.loading_icon.show()

    def _download_cancel(self):
        '''
        Cancel Download.

        Request the backend to stop the download and cleanup any files.
        '''
        self.backend.update.download_cancel.emit(
            self._download_id, self.backend_done, self.backend_failed)

    def _download_status(self, model_json):
        '''
        Download Status.

        Handle a request of the download status, display the progress and
        schedule the next request.
        '''
        download_status = download_status_response_model()
        download_status.from_json(model_json)
        self.ui.progress_bar.setValue(download_status.complete.get())
        self.ui.progress_bps_label.setText(
            self._readable_velocity(download_status.bps.get()))

        if download_status.errors.get():
            self.ui.progress_widget.hide()
            self.ui.download_error_widget.show()
            if download_status.error_message.get() is None:
                self.ui.message_error.setText(self.tr(
                    'During download an error occurred,'
                    ' please check the log.'))
            else:
                self.ui.message_error.setText(
                    download_status.error_message.get())
            self.loading_icon.stop()
            self.ui.loading_icon.hide()
            self.ui.button_download.setEnabled(True)
            self.ui.button_check_now.setEnabled(True)
            self.ui.button_change_download_destination.setEnabled(True)
            self.ui.update_url.setEnabled(True)
            self.ui.os_select.setEnabled(True)
            self.ui.download_destination.setEnabled(True)
            self._state = STATE_IDLE
            return
        signature_ok = download_status.signature_ok.get()
        checksum_ok = download_status.checksum_ok.get()
        if signature_ok is True and checksum_ok:
            self.ui.progress_widget.hide()

            self.ui.download_url_widget.hide()

            self.ui.update_os_widget.hide()
            self.ui.download_destination_widget.hide()

            self.ui.download_complete.show()
            self.loading_icon.stop()
            self.ui.loading_icon.hide()
            self.ui.button_download.setEnabled(True)
            self.ui.button_check_now.setEnabled(True)
            self.ui.button_change_download_destination.setEnabled(True)
            self.ui.os_select.setEnabled(True)
            self.ui.update_url.setEnabled(True)
            self.ui.download_destination.setEnabled(True)
            self._state = STATE_IDLE
            return

        self._schedule_download_progess()

    def _download_started(self, model_json):
        '''
        Download Started.

        The download has started and is now in progress. Start a loop to
        request the backend for the download progress.
        '''
        download = download_id_response_model()
        download.from_json(model_json)
        self.ui.button_cancel_download.setEnabled(True)
        self._download_id = download.id.get()
        self._schedule_download_progess()

    def _ok(self):
        '''
        Ok.

        Close the window.
        '''
        self.hide()
        self.ui.loading_icon.hide()
        self.ui.download_error_widget.hide()
        self.ui.download_complete.hide()
        self.loading_icon.stop()
        self._state = STATE_IDLE

    def _open(self):
        '''
        Open.

        Open the Downloaded file with the OS open method.
        '''
        destination = self.ui.download_destination.text()
        open_url = QUrl.fromLocalFile(destination)
        QDesktopServices.openUrl(open_url)

    def _preferences_plugins_changed(self, model):
        '''
        Preferences Plugins Changed.

        The preferences of the plugins have changed

        Arguments:
            model (marshalable): preferences of all plugins
        '''
        plugin_model = model.find_by_name(PLUGIN_NAME)

        self.ui.update_url.setText(plugin_model.update_url.get())
        self._update_url = plugin_model.update_url.get()

        active = False
        if plugin_model.active.get():
            self.ui.active.setCheckState(Qt.Checked)
            active = True
        else:
            self.ui.active.setCheckState(Qt.Unchecked)

        if plugin_model.auto_check.get():
            self.ui.auto_check.setCheckState(Qt.Checked)
        else:
            self.ui.auto_check.setCheckState(Qt.Unchecked)
        self.ui.auto_check.setEnabled(active)

        self.ui.button_check_now.setEnabled(True)

    def _preferences_plugin_url_done(self, response):
        '''
        Preferences Plugin Url stored.

        Set the signature url derived from the stored url.
        '''
        update_signature_url = '%s.sig' % (self._update_url,)
        self.backend.preferences_plugin_set.emit(
            PLUGIN_NAME, 'update_signature_url',
            update_signature_url,
            self.preferences_plugin_signature_url_done, self.backend_failed)

    def _preferences_plugin_signature_url_done(self, response):
        '''
        Preferences Plugin Signature Url stored.

        Enable the controls and return to idle state.
        '''
        self.ui.button_check_now.setEnabled(True)
        self.ui.update_url.setEnabled(True)
        self._state = STATE_IDLE

    def _readable_velocity(self, bytes_per_second):
        '''
        Readable Velocity

        Create a readable string for the given bytes_per_second.

        1 MB = 1000000 bytes (= 10002 B = 106 B) is the definition recommended
        by the International System of Units (SI) and the International
        Electrotechnical Commission IEC.[2] This definition is used in
        networking contexts and most storage media, particularly hard drives,
        flash-based storage,[3] and DVDs, and is also consistent with the other
        uses of the SI prefix in computing, such as CPU clock speeds or
        measures of performance.

        Arguments:
            bytes_per_second (int): bytes per second
        '''
        if bytes_per_second is None:
            return ''
        if bytes_per_second > 1000 ** 4:
            return '%.2f %s' % (
                float(bytes_per_second) / (1000 ** 4), self.tr('TB/s'))
        if bytes_per_second > 1000 ** 3:
            return '%.2f %s' % (
                float(bytes_per_second) / (1000 ** 3), self.tr('GB/s'))
        if bytes_per_second > 1000 ** 2:
            return '%.2f %s' % (
                float(bytes_per_second) / (1000 ** 2), self.tr('MB/s'))
        if bytes_per_second > 1000:
            return '%.2f %s' % (
                float(bytes_per_second) / 1000, self.tr('kB/s'))

        return '%d %s' % (bytes_per_second, self.tr('B/s'))

    def _schedule_download_progess(self):
        '''
        Schedule Download Progress.

        Create a request lambda and schedule it for execution
        '''
        request = lambda: self.backend.update.download_status.emit(
            self._download_id,
            self.download_status,
            self.backend_failed)
        QTimer.singleShot(
            config_gui.DOWNLOAD_PROGRESS_FREQUENCY,
            request)

    def _set_default_download_destination(self):
        '''
        Set Default Download Destination.

        Set the download destination to the Desktop of the OS
        using the Qt Desktop Services. Fallback to Documents and
        Home if required.
        Set the lineedit with the result.
        Qt5:
        QStandardPaths.writableLocation(QStandardPaths.DownloadLocation)
        '''
        download_destination = QDesktopServices.storageLocation(
            QDesktopServices.DesktopLocation
        )
        if download_destination == '':
            download_destination = QDesktopServices.storageLocation(
                QDesktopServices.DocumentsLocation
            )
        if download_destination == '':
            download_destination = QDesktopServices.storageLocation(
                QDesktopServices.HomeLocation
            )
        self.ui.download_destination.setText(download_destination)

    def _update_message_text(self):
        '''
        Update Message Text.

        Update the text for the message. This method is called periodically.
        '''
        version_detail = self._version_detail
        if version_detail is None:
            return

        message_label = self.tr(config_gui.MESSAGE_LABEL)
        message_details = self.tr(config_gui.MESSAGE_DETAILS)
        latest_known_version = self.tr('Not available.')
        changelog = self.tr('No changelog available')
        check_age = self.tr('Never checked.')

        if version_detail.changelog.get() is not None:
            changelog = version_detail.changelog.get()

        if version_detail.last.get() is not None:
            now = datetime.datetime.utcnow()
            check_date = datetime.datetime.utcfromtimestamp(int(
                version_detail.last.get()))
            check_age = timeago.format(check_date, now)
            max_age = datetime.timedelta(
                days=config_backend.UPDATE_MAXIMUM_AGE)
            if now - check_date > max_age:
                message_label = self.tr(config_gui.MESSAGE_LABEL_OLD)
        else:
            message_label = self.tr(config_gui.MESSAGE_LABEL_UNKNOWN)
        if version_detail.latest_known_version.get() is not None:
            latest_known_version = version_detail.latest_known_version.get()
        self.ui.message_label.setText(message_label.format(
            VERSION=VERSION,
            LATEST_KNOWN_VERSION=escape_html(latest_known_version),
            CHECK_AGE=check_age))
        self.ui.message_details.setText(message_details.format(
            CHANGELOG=changelog))

    def _update_os_changed(self):
        '''
        Update OS Changed.

        The OS combobox has changed and the url and download button
        need updating.
        '''
        if self._version_detail is None:
            return
        selected_os = self.ui.os_select.itemData(
            self.ui.os_select.currentIndex())
        for url in self._version_detail.update_url_list:
            if url.os.get() != selected_os:
                continue
            self.ui.download_error_widget.hide()
            self.ui.button_download.setEnabled(True)
            self.ui.button_download.setFocus(Qt.OtherFocusReason)
            self.ui.download_url_widget.show()
            self.ui.download_url.setText(
                self.tr(config_gui.DOWNLOAD_URL_INFO).format(
                    url=url.url.get()))
            self.ui.download_url_sha1sum.setText(
                url.sha1sum.get())
            self.ui.download_url_sha256sum.setText(
                url.sha256sum.get())
            break

    def _update_url_changed(self):
        '''
        Update URI changed.

        The URI of the update services was changed. Store the value to the
        preferences.
        '''
        update_url = self.ui.update_url.text()
        if self._update_url == update_url:
            return
        if self._state == STATE_UPDATING_PREFERENCES:
            return
        self.ui.update_url.setEnabled(False)
        self._state = STATE_UPDATING_PREFERENCES
        self._update_url = update_url
        self.backend.preferences_plugin_set.emit(
            PLUGIN_NAME, 'update_url', update_url,
            self.preferences_plugin_url_done, self.backend_failed)

    def _update_url_changing(self):
        '''
        Update URI changing.

        The URI of the update services is changing, avoid that the wrong
        uri is requested.
        '''
        self.ui.button_check_now.setEnabled(False)
        update_url = self.ui.update_url.text()
        if self._update_url == update_url and self._version_detail is not None:
            self.ui.download_url_widget.show()
            self.ui.update_os_widget.show()
            self.ui.download_destination_widget.show()
            self.ui.button_check_now.setEnabled(True)
            return
        self.ui.button_check_now.setEnabled(False)
        self.ui.download_url_widget.hide()
        self.ui.update_os_widget.hide()
        self.ui.download_destination_widget.hide()
        self.ui.download_error_widget.hide()
        self._autosave_timer.start()

    def _version_info_loaded(self, model_json):
        '''
        Version Details Loaded.
        '''
        version_detail = response_version_detail_model()
        version_detail.from_json(model_json)
        self._version_detail = version_detail
        self._update_message_text()

        self.ui.button_download.setEnabled(False)
        self.ui.update_os_widget.hide()
        self.ui.download_url_widget.hide()
        self.ui.download_destination_widget.hide()
        if len(version_detail.update_url_list) > 0:
            self.ui.update_os_widget.show()
            self.ui.download_destination_widget.show()
            self.ui.download_url_widget.show()
        while self.ui.os_select.count():
            self.ui.os_select.removeItem(0)
        os = get_os()
        for url in version_detail.update_url_list:
            os_name = url.os.get()
            if os_name == 'osx':
                os_name = 'MacOS'
            elif os_name == 'win32':
                os_name = 'Windows'
            else:
                os_name = os_name.capitalize()
            self.ui.os_select.addItem(
                os_name, url.os.get())
            if os in url.os.get():
                self.ui.os_select.setCurrentIndex(
                    self.ui.os_select.findData(
                        url.os.get()))

        self._update_os_changed()

        self.ui.button_check_now.setEnabled(True)
        self.ui.update_url.setEnabled(True)
        self.ui.loading_icon.hide()
        self.loading_icon.stop()
        self._state = STATE_IDLE

    def showEvent(self, event):
        '''
        Show Event.

        Request the latest known information from the backend
        without triggering a remote request.
        '''
        self.backend.update.version_info.emit(
            self.version_info_loaded, self.backend_failed)
        self.ui.loading_icon.hide()
        self.loading_icon.stop()
        self.ui.download_error_widget.hide()
        self.ui.download_complete.hide()
        self.ui.message_error.setText('')
        self.ui.button_check_now.setFocus(Qt.OtherFocusReason)
