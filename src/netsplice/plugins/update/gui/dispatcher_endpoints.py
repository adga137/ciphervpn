# -*- coding: utf-8 -*-
# dispatcher_endpoints.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Endpoints used by dispatcher class(es).
'''


#
# Endpoints used by this dispatcher
#
UPDATE_CHECK_NOW = (
    r'module/plugin/update/check_now')
UPDATE_VERSION_INFO = (
    r'module/plugin/update/version_info')
UPDATE_DOWNLOAD = (
    r'module/plugin/update/download')
UPDATE_DOWNLOAD_ID = (
    r'module/plugin/update/download/%(download_id)s')
