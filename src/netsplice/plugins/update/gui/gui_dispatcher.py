# -*- coding: utf-8 -*-
# gui_dispatcher.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Dispatcher for unprivileged actions.
'''
import sys
from PySide import QtCore

from tornado import gen, httpclient

from netsplice.backend.net.model.response.download_id import (
    download_id as download_id_response_model
)
from netsplice.backend.net.model.response.download_status import (
    download_status as download_status_response_model
)
from netsplice.backend.net.model.request.download_id import (
    download_id as download_id_request_model
)
from netsplice.gui import backend_dispatcher_qt_replacement
from netsplice.util import get_logger, basestring
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.model.errors import ValidationError

from . import dispatcher_endpoints as endpoints

from .model.response.version_detail import (
    version_detail as version_detail_model
)
from .model.request.download import (
    download as download_request_model
)

logger = get_logger()

if sys.platform.startswith('darwin'):
    # Python only threads in gui.
    # Due to bugs in PySide osx may crash at random locations. Use this option
    # to use a threading implementation that mimics the emit/Signal code.
    Signal = backend_dispatcher_qt_replacement.Signal
    QObject = backend_dispatcher_qt_replacement.QObject
    QThread = backend_dispatcher_qt_replacement.QThread
    QMutex = backend_dispatcher_qt_replacement.QMutex
    QCoreApplication = backend_dispatcher_qt_replacement.QCoreApplication
else:
    Signal = QtCore.Signal
    QObject = QtCore.QObject
    QThread = QtCore.QThread
    QMutex = QtCore.QMutex
    QCoreApplication = QtCore.QCoreApplication


class gui_dispatcher(QObject):

    check_now = Signal(object, object)
    version_info = Signal(object, object)
    download = Signal(basestring, basestring, object, object)
    download_status = Signal(basestring, object, object)
    download_cancel = Signal(basestring, object, object)

    def __init__(self, owning_dispatcher):
        QObject.__init__(self)
        self.owner = owning_dispatcher
        self.check_now.connect(self._check_now)
        self.version_info.connect(self._version_info)
        self.download.connect(self._download)
        self.download_status.connect(self._download_status)
        self.download_cancel.connect(self._download_cancel)

    @gen.coroutine
    def _check_now(self, done_signal, failed_signal):
        '''
        Check for new version now.

        Request the backend to check for new version.
        '''

        try:
            yield self.owner.post(
                endpoints.UPDATE_CHECK_NOW, '{}')

            done_signal.emit('{}')
        except ValidationError as errors:
            self.owner._emit_failure(failed_signal, errors)
        except httpclient.HTTPError as errors:
            self.owner._emit_failure(failed_signal, errors)
        except ServerConnectionError as errors:
            self.owner._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _version_info(self, done_signal, failed_signal):
        '''
        Get Version Info.

        Request the backend for a details for the latest version.
        '''

        try:
            response = yield self.owner.get(
                endpoints.UPDATE_VERSION_INFO, None)

            response_model = version_detail_model()
            response_model.from_json(response.body)

            done_signal.emit(response_model.to_json())
        except ValidationError as errors:
            self.owner._emit_failure(failed_signal, errors)
        except httpclient.HTTPError as errors:
            self.owner._emit_failure(failed_signal, errors)
        except ServerConnectionError as errors:
            self.owner._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _download(self, os, destination, done_signal, failed_signal):
        '''
        Download the latest Version.

        Request the backend to download the latest version and return a
        network/download id for monitoring the download.
        '''
        try:
            request_model = download_request_model()
            request_model.destination.set(destination)
            request_model.os.set(os)
            response_model = download_id_response_model()
            response = yield self.owner.post(
                endpoints.UPDATE_DOWNLOAD, request_model.to_json())
            response_model.from_json(response.body)

            done_signal.emit(response_model.to_json())
        except ValidationError as errors:
            self.owner._emit_failure(failed_signal, errors)
        except httpclient.HTTPError as errors:
            self.owner._emit_failure(failed_signal, errors)
        except ServerConnectionError as errors:
            self.owner._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _download_cancel(self, download_id, done_signal, failed_signal):
        '''
        Download Cancel.

        Request the backend to stop the download with the given download_id.
        '''
        try:
            request_model = download_id_request_model()
            request_model.id.set(download_id)
            response_model = download_status_response_model()
            options = dict(
                download_id=request_model.id.get())
            yield self.owner.delete(
                endpoints.UPDATE_DOWNLOAD_ID % options)

            done_signal.emit(response_model.to_json())
        except ValidationError as errors:
            self.owner._emit_failure(failed_signal, errors)
        except httpclient.HTTPError as errors:
            self.owner._emit_failure(failed_signal, errors)
        except ServerConnectionError as errors:
            self.owner._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _download_status(self, download_id, done_signal, failed_signal):
        '''
        Download Status.

        Request the backend about the status of the given download_id.
        '''
        try:
            request_model = download_id_request_model()
            request_model.id.set(download_id)
            response_model = download_status_response_model()
            options = dict(
                download_id=request_model.id.get())
            response = yield self.owner.get(
                endpoints.UPDATE_DOWNLOAD_ID % options, None)
            response_model.from_json(response.body)

            done_signal.emit(response_model.to_json())
        except ValidationError as errors:
            self.owner._emit_failure(failed_signal, errors)
        except httpclient.HTTPError as errors:
            self.owner._emit_failure(failed_signal, errors)
        except ServerConnectionError as errors:
            self.owner._emit_failure(failed_signal, errors)
