# -*- coding: utf-8 -*-
# version_detail.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for version details.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.validator.plaintext import (
    plaintext as plaintext_validator
)
from netsplice.model.validator.none import (
    none as none_validator
)
from netsplice.model.validator.version import version as version_validator
from netsplice.model.validator.min import min as min_validator
from .url_list import url_list


class version_detail(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.version = field(
            required=True,
            validators=[version_validator()])

        self.latest_known_version = field(
            required=True,
            validators=[none_validator(exp_or=[version_validator()])])

        self.changelog = field(
            required=True,
            validators=[none_validator(exp_or=[plaintext_validator()])])

        self.last = field(
            required=True,
            validators=[none_validator(exp_or=[min_validator(0)])])

        self.update_url_list = url_list()
