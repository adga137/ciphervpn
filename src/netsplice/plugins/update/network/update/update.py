# -*- coding: utf-8 -*-
# update.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Update Process Control.
'''
from tornado import gen
from .model.response.update import (
    update as update_model
)
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.hkpk.download_ioloop_memory import download_ioloop_memory
from netsplice.util.hkpk.errors import (
    InvalidError, MaxBufferError, NetworkError, SignatureError
)
from netsplice.util.errors import NotFoundError
from netsplice.util import get_logger

logger = get_logger()


class update(object):

    def __init__(self, application):
        self.application = application

    @gen.coroutine
    def latest(self, update_url, update_signature_url, fingerprints, sign_key):
        '''
        Latest.

        Check the update_url (and the update_signature_url when a sign_key is
        provided) and return the contents of the url.

        Decorators:
            gen.coroutine

        Arguments:
            update_url (string): url for the version file.
            update_signature_url (string): url for the signature file.
            fingerprints (list(string)): tls fingerprint
            sign_key (string): base64 encoded public sign key.
                '''
        try:
            if sign_key is not None:
                signature_download = download_ioloop_memory(
                    update_signature_url, fingerprints)
                signature = yield signature_download.start()
                version_download = download_ioloop_memory(
                    update_url, fingerprints,
                    signature=signature,
                    sign_key=bytes(sign_key))
            else:
                version_download = download_ioloop_memory(
                    update_url, fingerprints)

            response = yield version_download.start()
            response_model = update_model()
            response_model.from_json(response)
            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except NetworkError as errors:
            if errors.status == 404:
                raise NotFoundError(str(errors))
            raise ServerConnectionError(str(errors))
        except InvalidError as errors:
            raise ServerConnectionError(str(errors))
        except MaxBufferError as errors:
            raise ServerConnectionError(str(errors))
        except SignatureError as errors:
            raise ServerConnectionError(str(errors))
