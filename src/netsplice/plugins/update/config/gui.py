# -*- coding: utf-8 -*-
# gui.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Configuration for gui components.
'''

DESCRIPTION = '''
<p><b>Update</b> is a plugin that notifies the user when updates for the
software are available.</p>
'''

PREFERENCES_HELP = '''
<p>Update options.</p>
'''

MESSAGE_LABEL = '''
<p>Netsplice is currently installed in version <b>{VERSION}</b>.
<p>The latest known version is <b>{LATEST_KNOWN_VERSION}</b>. This information
  was requested {CHECK_AGE}.
'''

MESSAGE_LABEL_OLD = '''
<p>Netsplice is currently installed in version <b>{VERSION}</b>.
<p>The latest known version is <b>{LATEST_KNOWN_VERSION}</b>.
<p><img src=":/images/state/failure.png"> This information
  was requested {CHECK_AGE}. Check for updates now!
'''

MESSAGE_LABEL_UNKNOWN = '''
<p>Netsplice is currently installed in version <b>{VERSION}</b>.
<p><img src=":/images/state/failure.png"> No information about more recent
 versions was requested yet. Check for updates now!
'''

MESSAGE_DETAILS = '''
'''

MESSAGE_ERROR = '''
<p>An error occurred requesting the update server. Check the internet
 connection or visit <a href="{URL_MANUAL}">{URL_MANUAL}</a>.
<p>The error was: {ERROR_MESSAGE}.
'''

ACTIVE_INFO = '''
<p>The update periodically checks for new versions. With the <i>Autocheck on
 start</i> enabled, the request for new updates will be executed regardless
 of the currently active connection. When this option is disabled, no reminder
 will be displayed and the version check has to be triggered manually.
'''

AUTO_CHECK_INFO = '''
<p>With <i>Autocheck on start</i> enabled, the update-check will be done when
 Netsplice starts. <b>If this option is enabled information that Netsplice
 is installed on the computer might be leaked.</b>
'''

UPDATE_SERVER_INFO = '''
<p>The update server is queried for the latest version and provides signatures
 for the download in return.
 Ensure that the providers TLS public key fingerprint, signature URL, and the
 public signature key are configured in the update preferences.
'''

DOWNLOAD_COMPLETE_INFO = '''
<p>To install the update, close this instance of Netsplice and run the
 installer.
 Netsplice is alpha software, please make sure a backup of the configuration
 exists before starting the new version.
'''

DOWNLOAD_URL_INFO = '''
<a href="{url}">{url}</a>
<br/>Use this URL in the browser and verify the checksums manually.
'''

MESSAGE_NEW_VERSION = dict(
    title=r'New Version Available.',
    body=(
        r'%(NEW_VERSION)s is available.'
        ' Use the Update action in the main menu to download the latest'
        ' version.')
)

MESSAGE_AGE = dict(
    title=r'Check for Updates!',
    body=(
        r'The last check for updates was %(AGE)s.'
        ' Please use the Update action in the main menu to check for new'
        ' versions.')
)

LABEL_MENU = 'Update'
SHORTCUT_MENU = 'Shift+Ctrl+U'

# How often the progress during a download is updated
# in milliseconds.
DOWNLOAD_PROGRESS_FREQUENCY = 300
