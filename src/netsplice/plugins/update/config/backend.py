# -*- coding: utf-8 -*-
# backend.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Configuration for backend.
'''
# Default URI where the update info is provided.
UPDATE_URL = 'https://ipredator.se/static/downloads/netsplice/update.json'

# Default URI where the signature for the update URL is provided.
UPDATE_SIGNATURE_URL = (
    'https://ipredator.se/static/downloads/netsplice/update.json.sig'
)

# Fingerprints of the TLS public key for the UPDATE_URL and
# UPDATE_SIGNATURE_URL. The update will fail if the fingerprint does not match.
# The fingerprint is for https://ipredator.se @ 2018-01-24
UPDATE_URL_FINGERPRINTS = [
    "d8afe74f60df769880cdb797a2f03cb8519b2ba5b769b4aac64c814334a05198",
    "698322a5176d1df6ba2c93a24eb67e26a201d527db37ef1b23e0b45ca7ceb301"
]

# Public Key that is used to sign all updates and the contents of the
# UPDATE_URL and the files referenced by the update url.
# extracted from https://ipredator.se/static/downloads/key.txt
# gpg --import key.txt
# gpg: key DD009DB6759F27B2: public key
#  "Netsplice signing key <netsplice@ipredator.se>" imported
# gpg --export-ssh-key \
# "FCFB6801AF03F12B47F728E455B1F9D3C24B82DB!" > ipredator-public-sub.ssh-key
# gpg -o pub-ipredator-ssh.key --export-ssh-key
# "FCFB 6801 AF03 F12B 47F7  28E4 55B1 F9D3 C24B 82DB"
# ssh-keygen -f ipredator-public-sub.ssh-key -e -m pkcs8
UPDATE_URL_SIGN_KEY = ('''-----BEGIN PUBLIC KEY-----
MIIEIjANBgkqhkiG9w0BAQEFAAOCBA8AMIIECgKCBAEA6b09PeZ7PWkYusKnr3c4
Q8JJsmCUNk05gb81pt2qQe8/iw3AE3ApwlZO+m4nqAL4yRiYkSZBHKsd4x4FgvXc
zvhl3z4IHGX8EDSxvwyCvC4mHoRRjd6vdD07xq8FUY+eltCzaw0mLRa8GpN9MYRf
1NYuHNLpaTm6oey/TBvZ+ur8zEXkjBpAHvp3JyGnEQBNmKmAht/RFEXpxtVolQVb
2W2UeOd5wg8qUKA4MTFab++Hnrbw35ZYKOl9D/fdobvO0qr+d/Pwa7bkOK52cpEG
eTC72+PIDBG3DSeGlOLvsazVr/yAlTZGvFQ7mRR3h4OcJTe87WqoneP5EivGqyED
WlKkUwEeU3W878xln3ALfOo6mu96qiyW9KyuCO43ikXtstMnmIluVeRCKsyJZrRc
zc94Wr8esM1SEvG2R0JumJ6D35EiTprrKZ3if1N8uMgkaVd+E6ui7nnJT7BfEMKo
Z5VUqrF9e4iCcLnQGhY/JiaxkbV27QZ1PPMy/iupRK10gaSSJ5TRv+v46sBWC+af
eOe4AAdTV5zDQE3MABlz4ft2xNJUUj3vYjhAfWKyuDPnFwvpXJ5LjnqPyQzl3tXy
/TLz3Kgaz21ZFJzoCETvNhUHlPJACTIXwIdUHs1VF7rKba+HzU9gGY44Aqz97mw7
4UWpgjMs/fJFoxg9FtBbWm4cO6hNkWWfF/DNmGPxkenB/N2GCvO/uWX9tub6xoW7
1VowZElch5KBv7uagrauhGCD5tnwLV8kqMp3Cl/FQMv3Rx9bLGk9VEo0g9wyD6To
B01txX9Pex63ONebvjxmD69Cgcen9u8Kcvzeie2DYNa3G3/aCqF8Pf6w3PaQZu1r
tK/cl0iQqCiSIVcxr04LzhqbkqVKHGuY7w6IhRv/GCyiSPzylE+3xdvlBbYVvsrc
2xX/ey0TrcnshcHTz9qsgnu8ibPzGNBQiw7RfA0AXcO0uWKm00t+HNAbVrHfBtfC
2T2/Yd0fHy+yeGkIEKbAgMbGY54lvlqtxolhhKyXe51AI4+Tm1y+WfWx7Y5mVgr4
QJKpnqJhEh1a/U06dXJo+RQueqMOSAeY23wQXjnUodV62KYXX1569LHDuw6k4jIz
9f7Jwv556gG50jAkkJ34l3ieT7RLcR0sU8RggaD6ydgmbccQB07SPxSaQJXZVnp2
nFLV55KWEDS6J+HmG/7ssQIKaPf0qgOd5mkZKlGshXdlKOh2uXSVK4Umq/Ddo+iX
gZme+Oxxb5LJkymqhu9ztGZBYtjLXm7OsJ0b9z0M4Uu+Duy1ox2Lsa+kL8y17Yu4
MGnibUK5KuH5G/3+406NVhIHYI+DhGCmbmAR24fvyvGQNlulYCgNAwuwOcaD7mxb
qwIDAQAB
-----END PUBLIC KEY-----
''')

# Timeout in seconds to wait for the UPDATE_URL to respond
REQUEST_TIMEOUT = 10

# Notify the user when a later version is known or when the maximum age
# was reached.
UPDATE_ACTIVE = True

# Automatic request UPDATE_URL when the maximum age was reached.
UPDATE_AUTO_CHECK = False

# Maximum age (in days) until the user is notified
UPDATE_MAXIMUM_AGE = 10

# Maximum age (in days) that can be set in the GUI
MAXIMUM_AGE = 100
