# -*- coding: utf-8 -*-
# events.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
from netsplice.backend.event import names as backend_event_names

UPDATE_MODEL = 'update.model'
UPDATE_CHECK_NOW = 'update.check_now'
UPDATE_NOTIFY_AGE = 'update.notify_age'
UPDATE_NOTIFY_VERSION = 'update.notify_version'

# Events emitted into the backend
OUTGOING = [
    UPDATE_NOTIFY_AGE,
    UPDATE_NOTIFY_VERSION
]

# Events from the backend
INCOMMING = [
    backend_event_names.BACKEND_READY,
    UPDATE_CHECK_NOW
]

# Events from the backend.net.event_controller
INCOMMING_NET = [
    UPDATE_MODEL
]
