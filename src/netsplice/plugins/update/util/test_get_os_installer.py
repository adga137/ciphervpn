# -*- coding: utf-8 -*-
# test_get_os_installer.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Update Plugin.

Extends Netsplice with a update mechanism.
'''
import mock
from . import get_os_installer


@mock.patch.multiple('sys', platform='any')
def test_get_os_installer_any_returns_source():
    result = get_os_installer('0.1.2')
    print(result)
    assert(result == 'source/netsplice-0.1.2.tar.bz2')


@mock.patch.multiple('sys', platform='win32')
def test_get_os_installer_any_returns_win32():
    result = get_os_installer('0.1.2')
    print(result)
    assert(result == 'win32/netsplice-0.1.2-0.exe')


@mock.patch.multiple('sys', platform='darwin')
def test_get_os_installer_any_returns_osx():
    result = get_os_installer('0.1.2')
    print(result)
    assert(result == 'osx/netsplice-0.1.2.dmg')


@mock.patch.multiple('sys', platform='linux')
@mock.patch(
    'netsplice.plugins.update.util.get_os_release',
    return_value=('gentoo', None, None))
def test_get_os_installer_linux_any_returns_source(mock_get_os_release):
    result = get_os_installer('0.1.2')
    print(result)
    assert(result == 'source/netsplice-0.1.2.tar.bz2')


@mock.patch.multiple('sys', platform='linux')
@mock.patch(
    'netsplice.plugins.update.util.get_os_release',
    return_value=('Arch', None, None))
def test_get_os_installer_linux_arch_returns_pkg(mock_get_os_release):
    result = get_os_installer('0.1.2')
    print(result)
    assert(result == 'archlinux/netsplice_0.1.2-0-x86_64.pkg.tar.xz')


@mock.patch.multiple('sys', platform='linux')
@mock.patch(
    'netsplice.plugins.update.util.get_os_release',
    return_value=('Debian', 8, None))
def test_get_os_installer_linux_debian8_returns_deb(mock_get_os_release):
    result = get_os_installer('0.1.2')
    print(result)
    assert(result == 'debian8/netsplice_0.1.2_amd64.deb')


@mock.patch.multiple('sys', platform='linux')
@mock.patch(
    'netsplice.plugins.update.util.get_os_release',
    return_value=('Debian', 9, None))
def test_get_os_installer_linux_debian9_returns_deb(mock_get_os_release):
    result = get_os_installer('0.1.2')
    print(result)
    assert(result == 'debian9/netsplice_0.1.2_amd64.deb')


@mock.patch.multiple('sys', platform='linux')
@mock.patch(
    'netsplice.plugins.update.util.get_os_release',
    return_value=('Ubuntu', "16.04.0", "16.04"))
def test_get_os_installer_linux_ubuntu1604_returns_deb(mock_get_os_release):
    result = get_os_installer('0.1.2')
    print(result)
    assert(result == 'ubuntu16.04/netsplice_0.1.2_amd64.deb')


@mock.patch.multiple('sys', platform='linux')
@mock.patch(
    'netsplice.plugins.update.util.get_os_release',
    return_value=('Ubuntu', "17.04.0", "17.04"))
def test_get_os_installer_linux_ubuntu1704_returns_deb(mock_get_os_release):
    result = get_os_installer('0.1.2')
    print(result)
    assert(result == 'ubuntu17.04/netsplice_0.1.2_amd64.deb')


@mock.patch.multiple('sys', platform='linux')
@mock.patch(
    'netsplice.plugins.update.util.get_os_release',
    return_value=('Ubuntu', "17.10.0", "17.10"))
def test_get_os_installer_linux_ubuntu1710_returns_deb(mock_get_os_release):
    result = get_os_installer('0.1.2')
    print(result)
    assert(result == 'ubuntu17.10/netsplice_0.1.2_amd64.deb')


@mock.patch.multiple('sys', platform='linux')
@mock.patch(
    'netsplice.plugins.update.util.get_os_release',
    return_value=('Fedora', "17.10.0", "24"))
def test_get_os_installer_linux_fedora24_returns_deb(mock_get_os_release):
    result = get_os_installer('0.1.2')
    print(result)
    assert(result == 'fedora24/netsplice_0.1.2-0.fc24.x86_64.rpm')


@mock.patch.multiple('sys', platform='linux')
@mock.patch(
    'netsplice.plugins.update.util.get_os_release',
    return_value=('Fedora', "26 (Twentysix)", "26"))
def test_get_os_installer_linux_fedora26_returns_deb(mock_get_os_release):
    result = get_os_installer('0.1.2')
    print(result)
    assert(result == 'fedora26/netsplice_0.1.2-0.fc26.x86_64.rpm')
