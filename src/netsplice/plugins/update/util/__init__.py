# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Update Plugin.

Extends Netsplice with a update mechanism.
'''
import sys
import os
from netsplice import __version__ as VERSION
from netsplice.config.constants import NS_NAME


def get_os_release():
    '''
    Get OS Release.

    Parse the /etc/os-release file for information about the distribution and
    its  version.

    Returns a tuple with os_name, os_version and os_version_id. Any of them
    may be None when the /etc/os-release does not exist or does not contain
    the information.
    '''
    os_name = None
    os_version = None
    os_version_id = None
    if os.path.isfile('/etc/os-release'):
        lines = open('/etc/os-release').read().split('\n')
        for line in lines:
            if line.startswith('NAME='):
                line_value = line.split('=')[1]
                os_name = line_value.replace('"', '')
                continue
            if line.startswith('VERSION='):
                line_value = line.split('=')[1]
                os_version = line_value.replace('"', '')
                continue
            if line.startswith('VERSION_ID='):
                line_value = line.split('=')[1]
                os_version_id = line_value.replace('"', '')
                continue
    return (os_name, os_version, os_version_id)


def get_os():
    '''
    Get Current OS.

    Return the current os prefix on the download server
    '''
    if sys.platform.startswith('win32'):
        return 'win32'
    elif sys.platform.startswith('darwin'):
        return 'osx'
    elif sys.platform.startswith('linux'):
        debian_based_name = None
        (os_name, os_version, os_version_id) = get_os_release()

        if os_name is None:
            return 'source'
        elif os_name.startswith('Arch'):
            return 'archlinux'
        elif os_name.startswith('Fedora'):
            return 'fedora{os_version_id}'.format(
                os_version_id=os_version_id)
        elif os_name.startswith('Debian'):
            debian_based_name = 'debian{os_version_id}'.format(
                os_version_id=os_version_id)
        elif os_name.startswith('Ubuntu'):
            debian_based_name = 'ubuntu{os_version_id}'.format(
                os_version_id=os_version_id)
        if debian_based_name is not None:
            return '{debian_base}'.format(
                debian_base=debian_based_name)
    return 'source'


def get_os_installer(version=VERSION):
    '''
    Get Current OS Installer.

    Return the package name for the current os.
    The package name is tied to the pkg/debian-build slaves and maps release
    versions to a filename that may be downloaded. The fallback is always
    the source release.
    '''
    version_parts = version.split('.')
    build_parts = version_parts[2].split('+')
    major_number = int(version_parts[0])
    minor_number = int(version_parts[1])
    release_number = 0
    if len(build_parts) == 1:
        build_number = int(version_parts[2])
        release_number = 0
    else:
        build_number = int(build_parts[0])
        release_number = int(build_parts[1])
    version_123 = '%d.%d.%d' % (major_number, minor_number, build_number)
    lc_product = NS_NAME.lower()

    if sys.platform.startswith('win32'):
        return 'win32/{lc_product}-{version_123}-{release_number}.exe'.format(
            lc_product=lc_product,
            version_123=version_123,
            release_number=release_number)
    elif sys.platform.startswith('darwin'):
        return 'osx/{lc_product}-{version_123}.dmg'.format(
            lc_product=lc_product,
            version_123=version_123)
    elif sys.platform.startswith('linux'):
        debian_based_name = None
        (os_name, os_version, os_version_id) = get_os_release()

        if os_name is None:
            return 'source/{lc_product}-{version}.tar.bz2'.format(
                lc_product=lc_product,
                version_123=version_123,
                release_number=release_number)
        elif os_name.startswith('Arch'):
            return (
                'archlinux/{lc_product}_{version_123}-{release_number}'
                '-x86_64.pkg.tar.xz').format(
                    lc_product=lc_product,
                    version_123=version_123,
                    release_number=release_number)
        elif os_name.startswith('Fedora'):
            return (
                'fedora{version_id}/{lc_product}_{version_123}-0'
                '.fc{version_id}.x86_64.rpm').format(
                    lc_product=lc_product,
                    version_123=version_123,
                    release_number=release_number,
                    version_id=os_version_id)
        elif os_name.startswith('Debian'):
            debian_based_name = 'debian{os_version}'.format(
                os_version=os_version)
        elif os_name.startswith('Ubuntu'):
            debian_based_name = 'ubuntu{os_version_id}'.format(
                os_version_id=os_version_id)
        if debian_based_name is not None:
            return '{debian_base}/{lc_product}_{version_123}_amd64.deb'.format(
                debian_base=debian_based_name,
                lc_product=lc_product,
                version_123=version_123,
                release_number=release_number)
    return 'source/{lc_product}-{version_123}.tar.bz2'.format(
        lc_product=lc_product,
        version_123=version_123,
        release_number=release_number)
