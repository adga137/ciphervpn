# -*- coding: utf-8 -*-
# test_get_os_release.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Update Plugin.

Extends Netsplice with a update mechanism.
'''
import mock
from . import get_os_release


class mock_fh(object):
    def __init__(self, content):
        self.content = content

    def read(self):
        return self.content


@mock.patch('os.path.isfile')
def test_get_os_release_nofile_returns_none(mock_isfile):
    mock_isfile.return_value = False
    (name, version, version_id) = get_os_release()
    print(name, version, version_id)
    assert(name is None)
    assert(version is None)
    assert(version_id is None)


@mock.patch('netsplice.plugins.update.util.open')
@mock.patch('os.path.isfile')
def test_get_os_release_file_fedora_returns_values(
        mock_isfile, mock_open):
    mock_isfile.return_value = True
    mock_open.return_value = mock_fh('''
NAME=Fedora
VERSION="27 (Twenty Seven)"
VERSION_ID=27
''')
    (name, version, version_id) = get_os_release()
    print(name, version, version_id)
    assert(name == 'Fedora')
    assert(version == '27 (Twenty Seven)')
    assert(version_id == '27')


@mock.patch('netsplice.plugins.update.util.open')
@mock.patch('os.path.isfile')
def test_get_os_release_file_debian8_returns_values(
        mock_isfile, mock_open):
    mock_isfile.return_value = True
    mock_open.return_value = mock_fh('''
PRETTY_NAME="Debian GNU/Linux 8 (jessie)"
NAME="Debian GNU/Linux"
VERSION_ID="8"
VERSION="8 (jessie)"
ID=debian
HOME_URL="http://www.debian.org/"
SUPPORT_URL="http://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"''')
    (name, version, version_id) = get_os_release()
    print(name, version, version_id)
    assert(name == 'Debian GNU/Linux')
    assert(version == '8 (jessie)')
    assert(version_id == '8')


@mock.patch('netsplice.plugins.update.util.open')
@mock.patch('os.path.isfile')
def test_get_os_release_file_ubuntu1604_returns_values(
        mock_isfile, mock_open):
    mock_isfile.return_value = True
    mock_open.return_value = mock_fh('''
NAME="Ubuntu"
VERSION="16.04.2 LTS (Xenial Xerus)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 16.04.2 LTS"
VERSION_ID="16.04"
HOME_URL="http://www.ubuntu.com/"
SUPPORT_URL="http://help.ubuntu.com/"
BUG_REPORT_URL="http://bugs.launchpad.net/ubuntu/"
VERSION_CODENAME=xenial
UBUNTU_CODENAME=xenial
''')
    (name, version, version_id) = get_os_release()
    print(name, version, version_id)
    assert(name == 'Ubuntu')
    assert(version == '16.04.2 LTS (Xenial Xerus)')
    assert(version_id == '16.04')
