# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
import sys

from netsplice.config import plugins as config

from . import config_quality_check as config_quality_check_plugin
from . import debug_check as debug_check_plugin
from . import openvpn as openvpn_plugin
from . import ping as ping_plugin
from . import process_launcher as process_launcher_plugin
from . import process_manager as process_manager_plugin
from . import process_sniper as process_sniper_plugin
from . import (
    profile_openvpn_ipredator_ipv4 as profile_openvpn_ipredator_ipv4_plugin
)
from . import (
    profile_openvpn_ipredator_ipv6 as profile_openvpn_ipredator_ipv6_plugin
)
from . import (
    profile_openvpn_ipredator_nat as profile_openvpn_ipredator_nat_plugin
)
from . import profile_openvpn_riseup as profile_openvpn_riseup_plugin
from . import profile_ssh_port_forward as profile_ssh_port_forward_plugin
from . import profile_ssh_port_forward_reverse as \
    profile_ssh_port_forward_reverse_plugin
from . import profile_ssh_socks_proxy as profile_ssh_socks_proxy_plugin
from . import profile_ssh_vpn as profile_ssh_vpn_plugin

from . import profile_tor_exit_ru_only as profile_tor_exit_ru_only_plugin
from . import profile_tor_exit_us_only as profile_tor_exit_us_only_plugin
from . import profile_tor_friend_entry_and_exit as \
    profile_tor_friend_entry_and_exit_plugin
from . import openvpn_server as openvpn_server_plugin
from . import update as update_plugin
from . import ssh as ssh_plugin
from . import tor as tor_plugin


this = sys.modules[__name__]

this.available_plugins = [
    config_quality_check_plugin,
    debug_check_plugin,
    openvpn_plugin,
    ping_plugin,
    process_launcher_plugin,
    process_manager_plugin,
    process_sniper_plugin,
    profile_openvpn_ipredator_ipv4_plugin,
    profile_openvpn_ipredator_ipv6_plugin,
    profile_openvpn_ipredator_nat_plugin,
    profile_openvpn_riseup_plugin,
    profile_ssh_port_forward_plugin,
    profile_ssh_port_forward_reverse_plugin,
    profile_ssh_socks_proxy_plugin,
    profile_ssh_vpn_plugin,
    profile_tor_exit_ru_only_plugin,
    profile_tor_exit_us_only_plugin,
    profile_tor_friend_entry_and_exit_plugin,
    openvpn_server_plugin,
    ssh_plugin,
    tor_plugin,
    update_plugin,
]


def get_plugins():
    '''
    Get Plugins.

    Get the list of plugins that are configured active.
    '''
    active_plugins = []
    for plugin in this.available_plugins:
        if plugin.__name__ in config.ACTIVE_PLUGINS:
            active_plugins.append(plugin)
    return active_plugins
