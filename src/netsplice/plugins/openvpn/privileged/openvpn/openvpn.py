# -*- coding: utf-8 -*-
# openvpn.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
OpenVPN Process Control. Uses a management connection that is setup() and
allows to connect, reconnect and disconnect the VPN connection using the
management socket.
'''

import atexit
import os
import re
import sys
import threading
import time
from tempfile import mkstemp
from evasion.common import net
from tornado import gen, ioloop
from netsplice.util.management.server import server
from netsplice.util import get_logger
from netsplice.config import constants
from netsplice.config import flags
from netsplice.model.named_value_list import (
    named_value_list as named_value_list_model
)
from netsplice.plugins.openvpn.config import process as config_process
from netsplice.plugins.openvpn.config import ipc as config_ipc
from netsplice.plugins.openvpn.config import state
from netsplice.plugins.openvpn.config import ACCOUNT_TYPE
from netsplice.plugins.openvpn.config.overrides import (
    overrides as config_overrides
)
from netsplice.util.parser import factory as get_config_parser
from netsplice.util.process.dispatcher import dispatcher as process_dispatcher
from netsplice.util.process.executable_extension import (
    factory as executable_factory
)
from netsplice.util.process.errors import ProcessError
from netsplice.util.errors import NotFoundError
from .model.message_list_item import (
    message_list_item as message_list_item_model
)

logger = get_logger()


class openvpn(object):

    def __init__(self, model, connection_model_instance):
        '''
        Initialize members.
        '''
        self.model = model
        self.connection_model_instance = connection_model_instance
        self.management_socket = None
        self.deleting = False
        self.disconnecting = False
        self.config_file_name = None
        self.server = None
        self.threading_lock = threading.RLock()
        self.hold_released = False

    def apply_overrides(self, named_config_items):
        '''
        Apply overrides.

        Iterate all configured overrides and apply them on the given items.
        Overrides that have a None value will filter the given item with the
        same name from the item list.
        '''
        for override in config_overrides:
            if override.type.get() != ACCOUNT_TYPE:
                continue
            name = override.name.get()
            value = override.value.get()
            try:
                named_value = named_config_items.find_by_key(name)
                if override.value.get() is None:
                    logger.info(
                        'Override for %s caused to remove the line'
                        % (name,))
                    named_config_items.remove(named_value)
                    continue
                logger.info(
                    'Override for %s with %s'
                    % (name, value),
                    extra={
                        'connection_id': self.model.id.get()
                    })
                named_value.values.find_by_name('value').value.set(value)
            except NotFoundError:
                pass

    def connect(self):
        '''
        Connect.

        Setup created the process that has read the config-file and waits for
        a "hold release" message.
        '''
        self.lock()
        self.connection_model_instance.state.set(
            state.CONNECT_CONNECTION)
        self.model.commit()
        message = message_list_item_model()
        message.date.set(time.time())
        message.data.set('hold release')
        self.connection_model_instance.up.set(False)
        self.connection_model_instance.send_message_list = []
        self.connection_model_instance.send_message_list.append(message)
        if flags.VERBOSE > 3:
            message = message_list_item_model()
            message.date.set(time.time())
            message.data.set('log all on')
            self.connection_model_instance.send_message_list.append(message)
        self.connection_model_instance.active.set(True)
        self.model.commit()
        self.unlock()
        self.send_management_commands()
        self.start_status_loop(self.connection_model_instance)

    def create_temporary_config(self, configuration_content):
        '''
        Create Temporary config.

        Apply the hard overrides to the given configuration content and
        pass it through the openvpn config parser/to text translator one last
        time.
        Create a temporary file with mkstemp and register that the file is
        removed at exit. The file is usually removed before the application
        exit
        The better approach using NamedTemporaryFile does not work on windows.
        '''
        openvpn_config_parser = get_config_parser(ACCOUNT_TYPE)
        openvpn_config_parser.set_text(configuration_content)
        openvpn_config = openvpn_config_parser.get_config()

        self.apply_overrides(openvpn_config)
        openvpn_config_parser.set_config(openvpn_config)

        plain_config = openvpn_config_parser.get_text()

        (file_handle, file_path) = mkstemp()
        os.write(file_handle, plain_config)
        os.close(file_handle)
        atexit.register(self.remove_temporary_config, file_path)

        return file_path

    def delete(self):
        '''
        Delete.

        Delete the connection and all its associated resources.
        '''
        self.lock()
        self.deleting = True
        self.connection_model_instance.state.set(
            state.DELETE_CONNECTION)
        self.model.commit()
        if self.management_socket is not None:
            self.management_socket.close()
        if self.process is not None:
            self.process.stop()
        if self.config_file_name is not None:
            self.remove_temporary_config(self.config_file_name)
            self.config_file_name = None  # Garbage-collect will remove file
        self.connection_model_instance.state.set(
            state.INVALID_CONNECTION)
        self.model.commit()
        self.unlock()

    def disconnect(self):
        '''
        Disconnect.

        Causes OpenVPN to exit gracefully.
        Updates the state and sends a management command to shutdown.
        When the wait time is over, the model is forced to be disconnected.
        This ensures that the UPDOWN:DOWN message is processed.
        '''
        self.lock()
        self.disconnecting = True
        self.connection_model_instance.state.set(
            state.DISCONNECT_CONNECTION)
        self.model.commit()
        disconnect_message = message_list_item_model()
        disconnect_message.date.set(time.time())
        disconnect_message.data.set('signal SIGTERM')
        self.connection_model_instance.send_message_list.append(
            disconnect_message)
        self.unlock()
        self.send_management_commands()

        time.sleep(config_ipc.OPENVPN_DISCONNECT_WAIT)

        self.lock()
        self.connection_model_instance.disconnect()
        self.model.commit()
        self.unlock()

    def lock(self):
        '''
        Lock.

        Lock the process, only the current thread may modify its resources.
        '''
        self.threading_lock.acquire()

    def management_handler(self, connection):
        '''
        Management Handler.

        Handle the management connection and read and send data on that
        connection. The data that was read from the socket will be processed
        by process_messages. When the socket is closed the connection is re-
        setup unless the connection is currently deleted. This handler runs in
        a separate thread and therefore needs to lock the resources it
        accesses.
        '''
        self.lock()
        if self.config_file_name is not None:
            # Remove the config_file_name that contains sensitive information
            # (keys). OpenVPN has already read the contents and stores its
            # content in its process. This way a 'SIGHUP' message will not
            # work.
            self.remove_temporary_config(self.config_file_name)
            self.config_file_name = None

        self.management_socket = connection
        self.connection_model_instance.state.set(
            state.MANAGEMENT_CONNECTED)
        self.model.commit()
        self.data_buffer = ''
        self.unlock()

        # Start Thread with continuous posting 'status' command to OpenVPN
        # and update & commit model when status changes
        socket_error = False
        while self.management_socket is not None:
            try:
                data = self.management_socket.recv(config_ipc.MAX_RECV)
            except Exception as errors:
                socket_error = True
                socket_error_message = str(errors)
                data = ''
            if data == '':
                self.process_messages()
                break
            self.lock()
            self.data_buffer += data
            self.unlock()
            self.process_messages()
            self.send_management_commands()

        self.lock()
        self.connection_model_instance.state.set(
            state.MANAGEMENT_DISCONNECTED)
        self.connection_model_instance.disconnect()
        self.model.commit()
        self.unlock()

        # Process leftover messages
        if self.data_buffer != '':
            self.lock()
            self.data_buffer += '\n'
            self.unlock()
            self.process_messages()

        # Connection was closed - eg due to kill/crash of OpenVPN instance
        self.lock()
        self.connection_model_instance.disconnect()
        self.management_socket = None
        self.unlock()

        if socket_error:
            logger.warning(
                'Socket error: exception receiving from management: %s'
                % (socket_error_message,),
                extra={
                    'connection_id': self.connection_model_instance.id.get()
                })
            self.disconnect()
            self.delete()

    def process_messages(self):
        '''
        Process Messages.

        Read all messages from the data_buffer and store each message in the
        connection_model_instance. Only processes when the current buffer ends
        with newline. Otherwise the message is incomplete.
        '''
        if not self.data_buffer.endswith('\n'):
            return
        self.lock()
        data_lines = self.data_buffer.splitlines(False)
        self.unlock()
        connection_id = self.connection_model_instance.id.get()

        while len(data_lines):
            line = data_lines[0]
            if flags.VERBOSE > 2:
                logger.debug(
                    'OpenVPN wrote: [%s]' % (str(data_lines),),
                    extra={
                        'connection_id': connection_id
                    })
            if line.startswith('OpenVPN STATISTICS'):
                delete_offset = self.process_statistic_message(data_lines)
                if 0 > delete_offset:
                    # statistics not yet complete
                    break
                del data_lines[0:(delete_offset + 1)]
            elif line.startswith('ERROR:'):
                self.process_error_message(line)
                del data_lines[0]
            elif line.startswith('SUCCESS:'):
                self.process_success_message(line)
                del data_lines[0]
            elif line.startswith('>FATAL:'):
                self.process_error_message(line)
                del data_lines[0]
            elif line.startswith('>HOLD:'):
                self.process_hold(line)
                del data_lines[0]
            elif line.startswith('>INFO:'):
                self.process_info_message(line)
                del data_lines[0]
            elif line.startswith('>PASSWORD:'):
                if self.process_empty_password_request(line):
                    self.process_password_request(line)
                self.process_password_failed(line)
                del data_lines[0]
            elif line.startswith('>UPDOWN:'):
                delete_offset = self.process_updown_message(data_lines)
                if 0 > delete_offset:
                    # updown not yet complete
                    break
                del data_lines[0:(delete_offset + 1)]
            else:
                logger.warning(
                    'Unhandled: %s' % (line,),
                    extra={
                        'connection_id': connection_id
                    })
                message = message_list_item_model()
                message.date.set(time.time())
                message.data.set(line)
                self.lock()
                self.connection_model_instance.recv_message_list.append(
                    message)
                self.unlock()
                del data_lines[0]

        self.lock()
        self.data_buffer = '\n'.join(data_lines)
        self.unlock()

    def process_empty_password_request(self, line):
        '''
        Process Empty Password Request.

        Handle that openvpn wants a password but the setup function was not
        provided with one, neither password for private key nor user/pass.
        '''

        if ":Need '" in line:
            # XXX validator cannot do this yet because config is separated from
            # password, this will be a change in OpenVPN soonish - some
            # providers want this empty password/username -> error
            # Injecting a Fatal error and shutting down the OpenVPN
            # executable
            # XXX copy of sensitive data
            username = self.connection_model_instance.username.get()
            password = self.connection_model_instance.password.get()
            connection_id = self.connection_model_instance.id.get()
            if line.startswith('>PASSWORD:Need \'Private Key\''):
                if password == '':
                    message = (
                        'Empty password. (%s) %d'
                        % (line, self.process.get_subprocess_pid()))
                    logger.error(
                        message,
                        extra={
                            'connection_id': connection_id
                        })
                    self.disconnect()
                    self.delete()
                    return False
            elif username == '' or password == '':
                message = (
                    'Empty username or password. (%s) %d'
                    % (line, self.process.get_subprocess_pid()))
                logger.error(
                    message,
                    extra={
                        'connection_id': connection_id
                    })
                self.disconnect()
                self.delete()
                return False
            return True

    def process_hold(self, line):
        '''
        Process Hold.

        Process Management Hold message that occurs every time the connection
        is dropped (physical interface down, server kicked, ping timeout etc)
        The automatic 'hold release' is only done when the connection was
        already up.
        '''
        logger.debug(
            'OpenVPN process is waiting for "hold release" %d'
            % (self.process.get_subprocess_pid(),),
            extra={
                'connection_id': self.connection_model_instance.id.get()
            })
        if self.connection_model_instance.active.get():
            message = message_list_item_model()
            message.date.set(time.time())
            message.data.set('hold release')
            self.connection_model_instance.up.set(False)
            self.connection_model_instance.send_message_list = []
            self.connection_model_instance.send_message_list.append(message)
            time.sleep(config_ipc.OPENVPN_HOLD_SLEEP)

    def process_password_request(self, line):
        '''
        Process Password Request.

        Chat the username/password or private key password to the management
        interface with the requested type.
        '''
        message = message_list_item_model()
        message.date.set(time.time())
        connection_id = self.connection_model_instance.id.get()
        if ":Need '" in line and line.endswith('username/password'):
            # XXX copy of sensitive data
            username = self.connection_model_instance.username.get()
            password = self.connection_model_instance.password.get()

            match = re.match("^.*'([^']*)'.*$", line)
            password_type = match.group(1)
            options = {
                'type': password_type,
                'username': username,
                'password': password
            }
            message.data.set(
                'username "%(type)s" "%(username)s"'
                '\n'
                'password "%(type)s" "%(password)s"'
                '\n' % options)
            self.lock()
            self.connection_model_instance.send_message_list.append(message)
            self.unlock()
            self.send_management_commands()
        elif ":Need '" in line and line.endswith('password'):
            match = re.match("^.*'([^']*)'.*$", line)
            password_type = match.group(1)
            options = {
                'type': password_type,
                'password': self.connection_model_instance.password.get()
            }
            message.data.set(
                'password "%(type)s" "%(password)s"'
                '\n' % options)
            self.lock()
            self.connection_model_instance.send_message_list.append(message)
            self.unlock()
            self.send_management_commands()
        else:
            logger.info(
                line,
                extra={
                    'connection_id': connection_id
                })

    def process_password_failed(self, line):
        '''
        Process Password Failed.

        Shutdown the subprocess when a authentication failure occured
        '''
        message = message_list_item_model()
        message.date.set(time.time())
        connection_id = self.connection_model_instance.id.get()
        if ":Verification Failed: '" in line:
            self.disconnect()
            logger.error(
                line,
                extra={
                    'connection_id': connection_id
                })

    def process_success_message(self, line):
        '''
        Process Success Message.

        Log the line and note that the hold was released.
        '''
        self.process_info_message(line)
        self.lock()
        self.hold_released = True
        self.unlock()

    def process_info_message(self, line):
        '''
        Process Info Message.

        Log the given line for the current connection id as info.
        '''
        logger.info(
            line,
            extra={
                'connection_id': self.connection_model_instance.id.get()
            })

    def process_statistic_message(self, lines):
        '''
        Process Statistic Message.

        Read multiple lines that need a 'END' line and extract the numeric
        bytes value from each line, mapping the value to the connection model.
        '''
        complete_index = -1
        statistics = []
        for index, line in enumerate(lines):
            statistics.append(line)
            if line == 'END':
                complete_index = index
                break

        if complete_index is -1:
            return complete_index

        self.lock()
        model = self.connection_model_instance
        changed = False
        for line in statistics:
            bytes_match = re.match('.*bytes,([0-9]*)$', line)
            if bytes_match is None:
                continue
            bytes = int(bytes_match.group(1))
            property_name = ''
            if line.startswith('TUN/TAP read bytes,'):
                property_name = 'read_tap_bytes'
            elif line.startswith('TUN/TAP write bytes,'):
                property_name = 'write_tap_bytes'
            elif line.startswith('TCP/UDP read bytes,'):
                property_name = 'read_udp_bytes'
            elif line.startswith('TCP/UDP write bytes,'):
                property_name = 'write_udp_bytes'
            elif line.startswith('Auth read bytes,'):
                property_name = 'auth_read_bytes'
            elif line.startswith('pre-compress bytes,'):
                property_name = 'pre_compress_bytes'
            elif line.startswith('post-compress bytes,'):
                property_name = 'post_compress_bytes'
            elif line.startswith('pre-decompress bytes,'):
                property_name = 'pre_decompress_bytes'
            elif line.startswith('post-compress bytes,'):
                property_name = 'post_decompress_bytes'
            if property_name != '':
                if model.__dict__[property_name].get() != bytes:
                    model.__dict__[property_name].set(bytes)
                    changed = True
        if changed:
            model.commit()
        self.unlock()
        return complete_index

    def process_updown_message(self, lines):
        '''
        Process Updown Message.

        Handle UPDOWN management messages. When the connection goes down,
        record the up state.
        '''
        complete_index = -1
        message = ''
        for index, line in enumerate(lines):
            if not message == '':
                message += '\n'
            message += line
            if line == '>UPDOWN:ENV,END':
                complete_index = index
                break
            elif line == '>UPDOWN:UP':
                self.connection_model_instance.up.set(True)
                # continue until UPDOWN:ENV,END occurs
            elif line == '>UPDOWN:DOWN':
                self.connection_model_instance.up.set(False)
                # continue until UPDOWN:ENV,END occurs
        if complete_index >= 0:
            logger.info(
                message,
                extra={
                    'connection_id': self.connection_model_instance.id.get()
                })
            return complete_index
        return complete_index

    def process_error_message(self, line):
        '''
        Process Error Message.

        Log the given line with the error logger for the current connection.
        '''
        logger.error(
            line,
            extra={
                'connection_id': self.connection_model_instance.id.get()
            })

    def reconnect(self):
        '''
        Reconnect.

        Send using SIGUSR1 to the OpenVPN process.

        Like SIGHUP, except don't re-read configuration file, and possi-
        bly  don't  close  and reopen TUN/TAP device, re-read key files,
        preserve  local  IP  address/port,  or  preserve  most  recently
        authenticated  remote  IP  address/port  based on --persist-tun,
        --persist-key,   --persist-local-ip,   and   --persist-remote-ip
        options respectively (see above).

        This signal may also be internally generated by a timeout condi-
        tion, governed by the --ping-restart option.

        This signal, when combined with --persist-remote-ip, may be sent
        when  the  underlying parameters of the host's network interface
        change such as when the host is a DHCP client and is assigned  a
        new IP address.  See --ipchange above for more information.
        '''
        self.lock()
        self.connection_model_instance.state.set(
            state.RECONNECT_CONNECTION)
        self.model.commit()
        reconnect_message = message_list_item_model()
        reconnect_message.date.set(time.time())
        reconnect_message.data.set('signal SIGUSR1')
        self.connection_model_instance.send_message_list.append(
            reconnect_message)
        self.hold_released = False
        self.model.commit()
        self.unlock()
        self.send_management_commands()
        self.connect()

    def remove_temporary_config(self, file_path):
        '''
        Remove temporary Config.

        Remove the given file_path from the filesystem. This function is used
        to cleanup the temporary file that is required to pass the config
        to openvpn. The function is called at apllication exit and when
        the code decides that the file is no longer required.
        '''
        if os.path.exists(file_path):
            os.remove(file_path)

    def send_management_commands(self):
        '''
        Send management Commands.

        Send all queued management commands to the OpenVPN process.
        When the socket is not available, the queued commands are dropped
        When duplicate messages are in the queue, the message is only executed
        once.
        When a error occurs sending the message to the OpenVPN process, the
        connection is disconnected.
        '''
        self.lock()
        socket_error = False
        if self.management_socket is None:
            del self.connection_model_instance.send_message_list[:]
            self.unlock()
            return
        last_message = None
        connection_id = self.connection_model_instance.id.get()
        for message in self.connection_model_instance.send_message_list:
            if last_message == message:
                logger.debug(
                    'Skipping duplicate message: %s' % (message,),
                    extra={
                        'connection_id': connection_id
                    })
                continue
            if flags.VERBOSE > 2:
                logger.debug(
                    'Sending %s to OpenVPN process' % (message.data.get()),
                    extra={
                        'connection_id': connection_id
                    })
            try:
                self.management_socket.sendall(message.data.get() + '\n')
            except Exception as errors:
                logger.error(
                    'Failed to send message to openvpn process: %s'
                    % (str(errors),),
                    extra={
                        'connection_id': connection_id
                    })
                socket_error = True
            last_message = message
        del self.connection_model_instance.send_message_list[:]
        if flags.VERBOSE > 2:
            logger.debug(
                'Send queue cleared',
                extra={
                    'connection_id': connection_id
                })
        self.unlock()
        if socket_error:
            if not self.disconnecting:
                self.disconnect()
            self.delete()

    def setup(self):
        '''
        Setup.

        Setup connection from the connection_model_instance.
        Get free ports for the management interface and start the openvpn
        process with a minimum of commandline options containing the
        --config that contains the user-config from the backend.
        '''
        logger.info(
            'Setup OpenVPN process.',
            extra={
                'connection_id': self.connection_model_instance.id.get()
            })
        self.lock()

        # find a free port to expose for the management connection
        active_ports = self.model.connections.get_connected_management_ports()
        management_port = net.get_free_port(exclude_ports=active_ports)
        self.server = server(
            constants.LOCALHOST,
            management_port,
            self.management_handler)
        self.connection_model_instance.management_port.set(management_port)
        self.model.commit()
        connection_id = self.connection_model_instance.id.get()
        nice_value = self.connection_model_instance.nice.get()
        logger.debug(
            'Listening on %d for management client' % (management_port,),
            extra={
                'connection_id': connection_id
            })

        # Store the config file to a temporary file so OpenVPN can read it.
        # The file is removed as soon as OpenVPN has connected to the
        # management port and therefore had read and processed the config
        self.config_file_name = self.create_temporary_config(
            self.connection_model_instance.configuration.get())

        # pw-file, if specified, is a password file  (password  on first line)
        # or "stdin" to prompt from standard input
        try:
            binary_name = 'openvpn'
            binary_prefix = os.path.join('openvpn', 'sbin')
            if self.connection_model_instance.version.get() is not None:
                # openvpn/v1.2.3-patch/openvpn/sbin/openvpn
                binary_prefix = os.path.join(
                    'openvpn',
                    self.connection_model_instance.version.get(),
                    'openvpn',
                    'sbin')

            self.hold_released = False
            self.process = process_dispatcher(
                binary_name, {
                    '--management-client': None,
                    '--management-up-down': None,
                    '--management-log-cache':
                        config_ipc.MAX_MANAGEMENT_LOG_CACHE,
                    '--management-hold': None,
                    '--management-query-passwords': None,
                    '--management': [constants.LOCALHOST, management_port],
                    '--auth-nocache': None,
                    '--nice': nice_value,
                    '--config': self.config_file_name
                },
                model=self.model,
                model_instance=self.connection_model_instance)
            self.process.set_prefix(binary_prefix)
            self.process.set_threading_lock(self.threading_lock)
            self.process.set_logger_extra({
                'connection_id': connection_id
            })
            self.process.set_logger_filter([
                "MANAGEMENT: CMD 'status'"
            ])

            # Ensure that LD_LIBRARY_PATH is not exported to the
            # subprocess.
            environment_remove_ld_library = named_value_list_model()
            environment_remove_ld_library.add_value('LD_LIBRARY_PATH', None)
            self.process.set_environment(environment_remove_ld_library)

            self.process.start_subprocess()
            logger.info(
                'OpenVPN subprocess has started: %d'
                % (self.process.get_subprocess_pid(),),
                extra={
                    'connection_id': connection_id
                })
        except ProcessError as errors:
            message = ('ProcessError: privileged setup: %s'
                % (str(errors),))
            logger.error(
                message,
                extra={
                    'connection_id': connection_id
                })
            self.config_file_name = None
            self.unlock()
            self.disconnect()
            self.lock()
        self.unlock()

    def start_status_loop(self, connection_model_instance):
        '''
        Start Status Loop.

        Start a thread that continuously requests the status from the
        management socket.
        '''
        status = openvpn_status_loop(self, connection_model_instance)
        status.start()

    def status(self):
        '''
        Status.

        Request status from OpenVPN instance.
        '''
        message = message_list_item_model()
        message.date.set(time.time())
        message.data.set('status')
        self.lock()
        if self.connection_model_instance.up.get() is True:
            self.connection_model_instance.send_message_list.append(message)
        self.unlock()
        self.send_management_commands()

    def unlock(self):
        '''
        Unlock.

        Unlock the process, other threads may modify its resources.
        '''
        self.threading_lock.release()


class openvpn_status_loop(threading.Thread):
    '''
    OpenVPN Status Loop.

    Thread class that calls status in the openvpn instance with a configured
    frequency.
    '''
    def __init__(self, openvpn_instance, connection_model_instance):
        '''
        Initialize Base and members.
        '''
        threading.Thread.__init__(self)
        self.openvpn = openvpn_instance
        self.connection = connection_model_instance

    def run(self):
        '''
        Run.

        Run as long as the connection is active. Call 'status' in the
        openvpn instance and sleep the configured time.
        When the connection is no longer active, the openvpn process will
        get stopped.
        '''
        while self.connection.active.get():
            self.openvpn.status()
            time.sleep(config_ipc.OPENVPN_STATUS_SLEEP)
        logger.info(
            'Connection is no longer active.',
            extra={
                'connection_id': self.connection.id.get()
            })
        self.openvpn.lock()
        self.openvpn.process.stop()
        self.openvpn.unlock()
