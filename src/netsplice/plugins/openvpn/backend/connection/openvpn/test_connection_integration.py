# -*- coding: utf-8 -*-
# test_connection_integration.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Connection.

Checks OpenVPN log messages and their state changes.
'''
import sys
import tornado
from tornado import gen
import netsplice.backend.connection as connection_module
import netsplice.backend.event as event_module
import netsplice.backend.gui as gui_module
import netsplice.backend.log as log_module
import netsplice.backend.preferences as preferences_module

from netsplice.backend.event_loop import event_loop
from netsplice.backend.preferences.model.override_list import (
    override_list as override_list_model
)
from netsplice.config import connection as config_connection
from netsplice.plugins.openvpn.config import openvpn as config_openvpn
from netsplice.config import plugins as config_plugins
from netsplice.backend.connection.connection import (
    connection as backend_connection
)
from netsplice.backend.connection.connection_broker import (
    connection_broker as backend_connection_broker
)
from netsplice.backend.connection.chain import (
    chain as connection_chain
)
from connection import connection as plugin_connection
from netsplice.backend.preferences.model.account_item import (
    account_item as account
)

from netsplice.backend.gui.model.connection_list_item import (
    connection_list_item as connection_item_model
)
from netsplice.model.log_item import log_item as log_item_model
from netsplice.util.ipc.application import application as application
from netsplice.plugins import get_plugins

FIRST_ACCOUNT_ID = '11c5aa5a-d1df-4ee8-9ede-86898effe163'


class mock_privileged_openvpn(object):
    def __init__(self):
        self.application = None
        self.reset()

    def reset(self):
        self.connect_yielded = False
        self.disconnect_yielded = False
        self.reconnect_yielded = False
        self.remove_yielded = False
        self.setup_connection_yielded = False
        self.setup_connection_return_value = connection_item_model()

    @gen.coroutine
    def connect(self, id):
        self.connect_yielded = True
        raise gen.Return('')

    @gen.coroutine
    def disconnect(self, id):
        self.disconnect_yielded = True
        raise gen.Return('')

    @gen.coroutine
    def reconnect(self, id):
        self.reconnect_yielded = True
        raise gen.Return('')

    @gen.coroutine
    def remove(self, id):
        self.remove_yielded = True
        raise gen.Return('')

    @gen.coroutine
    def setup_connection(
            self, plain_config, username, password, nice, version):
        self.setup_connection_yielded = True
        raise gen.Return(self.setup_connection_return_value)


class mock_privileged(object):
    def __init__(self):
        self.application = None
        self.reset()
        self.openvpn = None  # has to be done after register plugins
        self.server_role = ''

    def reset(self):
        pass

    def set_error_code(self, code, message):
        pass


class mock_unprivileged_openvpn(object):
    def __init__(self):
        self.application = None
        self.reset()

    def reset(self):
        self.get_version_list_yielded = False

    @gen.coroutine
    def get_version_list(self):
        self.get_version_list_yielded = True
        version_list = version_list_model()
        version_item = version_list.item_model_class()
        version_item.version.set('v0.0.0-test')
        version_list.append(version_item)
        raise gen.Return(version_list)


class mock_unprivileged(object):
    def __init__(self):
        self.application = None
        self.reset()
        self.openvpn = mock_unprivileged_openvpn()
        self.server_role = ''

    def reset(self):
        pass


class mock_connection_overrides(object):
    def __init__(self):
        self.reset()

    def reset(self):
        self.get_credential_yielded = False
        self.get_credential_return_value = credential_item_model()

    @gen.coroutine
    def get_credential(self, account_id):
        self.get_credential_yielded = True
        raise gen.Return(self.get_credential_return_value)

this = sys.modules[__name__]
this.test_application = None


def get_test_application():
    if this.test_application is not None:
        return this.test_application
    app = application()
    app.add_module(connection_module)
    app.add_module(log_module)
    app.add_module(event_module)
    app.add_module(preferences_module)
    app.add_module(gui_module)
    app.set_privileged(mock_privileged())
    app.set_unprivileged(mock_unprivileged())
    preferences_module.model.overrides = override_list_model(None)
    connection_module.register_module_events(event_module)
    orig_active_plugins = config_plugins.ACTIVE_PLUGINS
    config_plugins.ACTIVE_PLUGINS = ['netsplice.plugins.openvpn']
    for plugin in get_plugins():
        app.add_plugin(plugin)
    app.set_event_loop(event_loop())
    app.register_plugin_components('util')
    app.register_plugin_components('backend')
    config_plugins.ACTIVE_PLUGINS = orig_active_plugins
    # restore what plugin overrides
    app.get_unprivileged().ssh = mock_unprivileged_openvpn()
    this.test_application = app
    return this.test_application


def get_test_object(mock_overrides=[]):
    app = get_test_application()
    app.set_privileged(mock_privileged())
    app.set_unprivileged(mock_unprivileged())
    del app.get_module('event').model[:]
    test_account = account(None)
    test_account.id.set(FIRST_ACCOUNT_ID)
    bcb = app.get_module('connection').broker
    bcb.application = app
    bc = backend_connection(bcb, test_account)
    ch = connection_chain([test_account])
    app.get_module('connection').broker.connections[FIRST_ACCOUNT_ID] = (
        bc, ch)
    ci = connection_item_model()
    ci.id.set(FIRST_ACCOUNT_ID)
    bc.model = ci
    c = plugin_connection(app, bc)
    return c


class ConnectionIntegrationTests(tornado.testing.AsyncTestCase):
    '''
    Connection Integration Tests.

    Checks real OpenVPN messages and their state changes.
    '''
    @tornado.testing.gen_test
    def test_update_connection_000(self):
        '''
        TEMPLATE, Please write a short description of the integration,
        configure the before-state and set the assertions.
        '''
        c = get_test_object()
        lm = log_item_model()
        c.connection.state = 'TEMPLATE'
        lm.message.set('TEMPLATE_TEST_TEMPLATE')
        lm.level.set('info')
        state_changed = c.check_log_item(lm)
        yield c.application.event_loop.process_events()
        assert(state_changed is False)
        assert(lm.level.get() == 'info')
        assert(c.connection.state == 'TEMPLATE')

    @tornado.testing.gen_test
    def test_update_connection_001(self):
        '''
        Option Errors indicate that something is wrong with the config.
        '''
        c = get_test_object()
        lm = log_item_model()
        c.connection.state = config_connection.CONNECTING
        lm.message.set('Options error:')
        lm.level.set('info')
        state_changed = c.check_log_item(lm)
        yield c.application.event_loop.process_events()
        assert(state_changed is True)
        assert(lm.level.get() == 'error')
        print c.connection.state
        assert(c.connection.state == config_connection.ABORTED)

    @tornado.testing.gen_test
    def test_update_connection_002(self):
        '''
        Option Errors pushed from server may be ignored. The
        Level is set to warning.
        '''
        c = get_test_object()
        lm = log_item_model()
        c.connection.state = config_connection.CONNECTING
        lm.message.set('Options error: XXX in [PUSH-OPTIONS]')
        lm.level.set('info')
        state_changed = c.check_log_item(lm)
        yield c.application.event_loop.process_events()
        assert(state_changed is False)
        assert(lm.level.get() == 'warning')
        assert(c.connection.state == config_connection.CONNECTING)

    @tornado.testing.gen_test
    def test_update_connection_003(self):
        '''
        Fatal errors cancel connecting and connected connections.
        '''
        c = get_test_object()
        lm = log_item_model()
        c.connection.state = config_connection.CONNECTING
        lm.message.set('Exiting due to fatal error')
        lm.level.set('info')
        state_changed = c.check_log_item(lm)
        yield c.application.event_loop.process_events()
        assert(state_changed is True)
        assert(lm.level.get() == 'error')
        print c.connection.state
        assert(c.connection.state == config_connection.DISCONNECTED_FAILURE)

    @tornado.testing.gen_test
    def test_update_connection_004(self):
        '''
        TEMPLATE, Please write a short description of the integration,
        configure the before-state and set the assertions.
        '''
        c = get_test_object()
        lm = log_item_model()
        c.connection.state = config_connection.CONNECTING

        lm.message.set('SUCCESS: signal SIGTERM thrown')
        lm.level.set('info')
        state_changed = c.check_log_item(lm)
        yield c.application.event_loop.process_events()
        assert(state_changed is True)
        assert(lm.level.get() == 'info')
        assert(c.connection.state == config_connection.DISCONNECTED)

    @tornado.testing.gen_test
    def test_update_connection_005(self):
        '''
        TEMPLATE, Please write a short description of the integration,
        configure the before-state and set the assertions.
        '''
        c = get_test_object()
        lm = log_item_model()
        c.connection.state = config_connection.RECONNECTING

        lm.message.set('Initialization Sequence Completed')
        lm.level.set('info')
        state_changed = c.check_log_item(lm)
        yield c.application.event_loop.process_events()
        assert(state_changed is True)
        assert(lm.level.get() == 'info')
        assert(c.connection.state == config_connection.RECONNECTED)

    @tornado.testing.gen_test
    def test_update_connection_006(self):
        '''
        TEMPLATE, Please write a short description of the integration,
        configure the before-state and set the assertions.
        '''
        c = get_test_object()
        lm = log_item_model()
        c.connection.state = config_connection.CONNECTED

        lm.message.set('Initialization Sequence Completed')
        lm.level.set('info')
        state_changed = c.check_log_item(lm)
        yield c.application.event_loop.process_events()
        assert(state_changed is False)
        assert(lm.level.get() == 'info')
        assert(c.connection.state == config_connection.CONNECTED)
