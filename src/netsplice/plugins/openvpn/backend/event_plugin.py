# -*- coding: utf-8 -*-
# event_plugin.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Event plugin for backend app.

'''

from tornado import gen, ioloop

from .event import names as names
import netsplice.backend.event.names as event_names
import netsplice.backend.event.origins as origins
import netsplice.backend.event.types as types
from netsplice.util import get_logger

logger = get_logger()

# Wait (ms) after the model-changed event was triggered
UPDATE_PRIV_CONNECTION_DEFER_TIME = 10

class event_plugin(object):
    '''
    Event loop for the backend.

    Requires modules registered and waits for model-change on that modules
    to process events.
    '''

    def __init__(self, application):
        '''
        Initialize members.

        Requires the user to set application and add modules.
        '''
        self.application = application

    @gen.coroutine
    def disconnect_account(self, connection_id):
        '''
        Disconnect Account.

        Handle that an event was triggered that requests the disconnect of
        the given account_id.
        '''
        connection_module = self.application.get_module('connection')
        broker = connection_module.broker

        connection = broker.find_connection_by_connection_id(connection_id)
        logger.warn(
            'Disconnecting account %s because a event requested so.'
            % (connection.account.name.get(),))
        broker.disconnect(connection.account, None)

    @gen.coroutine
    def process_event(self, event_model_instance):
        '''
        Process the event in the event_model_instance.

        Evaluate the event-type, name and origin and call handler functions.
        '''
        event_name = event_model_instance.name.get()
        event_type = event_model_instance.type.get()
        event_origin = event_model_instance.origin.get()
        event_data = event_model_instance.data.get()

        if event_type == types.MODEL_CHANGED:
            if event_name == names.OPENVPN_MODEL:
                yield self.priv_model_changed()
        if event_type == types.NOTIFY:
            if event_name == event_names.LOG_CHANGED:
                yield self.log_model_changed()
            if event_name == event_names.ACCOUNT_REQUEST_DISCONNECT:
                yield self.disconnect_account(event_data)

    @gen.coroutine
    def priv_model_changed(self):
        '''
        The Model in the privileged backend has changed.

        Request all connections and update the priv-model and then instruct the
        connections dispatcher to update any connections that are affected by
        this.
        '''
        priv_backend = self.application.get_privileged()
        if priv_backend.get_available():
            yield priv_backend.openvpn.list_connections()
        yield self.log_model_changed()

    @gen.coroutine
    def log_model_changed(self):
        '''
        The Model in the privileged backend has changed.

        Request all connections and update the priv-model and then instruct the
        connections dispatcher to update any connections that are affected by
        this.
        '''
        broker = self.application.get_module('connection').broker
        for (connection_instance, chain) in broker.connections.values():
            yield connection_instance.plugin.update()
