# -*- coding: utf-8 -*-
# unprivileged_dispatcher.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Dispatcher for unprivileged actions.
'''

from socket import error as socket_error

from tornado import gen, httpclient

from . import dispatcher_endpoints as endpoints
from netsplice.util import get_logger
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.model.errors import ValidationError
from .unprivileged.model.request.version import version as version_model
from .unprivileged.model.response.version_detail import (
    version_detail as version_detail_model
)
from .unprivileged.model.response.version_help import (
    version_help as version_help_model
)
from .unprivileged.model.response.version_list import (
    version_list as version_list_model
)

logger = get_logger()


class unprivileged_dispatcher(object):

    def __init__(self, service_instance):
        self.service = service_instance

    @gen.coroutine
    def get_version_list(self):
        try:
            response_model = version_list_model()
            response = yield self.service.get(
                endpoints.OPENVPN_UNPRIV_EXECUTABLES,
                None
            )
            response_model.from_json(response.body)
            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2247, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2248, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2249, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2250, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def get_version_detail(self, version):
        try:
            request_model = version_model()
            response_model = version_detail_model()
            request_model.version.set(version)
            options = {
                'version': request_model.version.get()
            }
            response = yield self.service.get(
                endpoints.OPENVPN_UNPRIV_VERSION % options,
                None
            )
            response_model.from_json(response.body)
            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2251, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2252, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2253, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2254, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def get_version_help(self, version):
        try:
            request_model = version_model()
            response_model = version_help_model()
            request_model.version.set(version)
            options = {
                'version': request_model.version.get()
            }
            response = yield self.service.get(
                endpoints.OPENVPN_UNPRIV_HELP % options,
                None
            )
            response_model.from_json(response.body)
            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2255, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2256, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2257, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2258, str(errors))
            raise ServerConnectionError(str(errors))
