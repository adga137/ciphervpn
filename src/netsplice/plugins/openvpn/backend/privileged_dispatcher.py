# -*- coding: utf-8 -*-
# privileged_dispatcher.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Dispatcher for privileged actions.
'''

from socket import error as socket_error

from tornado import gen, httpclient

from . import dispatcher_endpoints as endpoints
from netsplice.backend.privileged.model.request.connection_id import (
    connection_id as connection_id_model
)
from netsplice.backend.privileged.model.response.connection_list import (
    connection_list as connection_list_model
)
from netsplice.backend.privileged.model.response.connection_status import (
    connection_status as connection_status_model
)

from netsplice.plugins.openvpn.backend.privileged.model.request.setup import (
    setup as setup_model
)
from netsplice.model.process import (
    process as process_model
)
from netsplice.util import get_logger
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.ipc.service import service
from netsplice.util.path import get_path_prefix
from netsplice.util.process.dispatcher import dispatcher as process_dispatcher
from netsplice.util.process.errors import ProcessError
from netsplice.util.model.errors import ValidationError

logger = get_logger()


class privileged_dispatcher(object):

    def __init__(self, service_instance):
        self.service = service_instance

    @gen.coroutine
    def connect(self, connection_id):
        options = {
            'connection_id': connection_id
        }
        try:
            response = yield self.service.post(
                endpoints.OPENVPN_CONN_CONNECT % options,
                '{}'
            )

            response_model = connection_id_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2083, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2084, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2085, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2086, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def connection_status(self, connection_id):
        options = {
            'connection_id': connection_id
        }

        response_model = connection_status_model()
        try:
            response = yield self.service.get(
                endpoints.OPENVPN_CONN_STATUS % options,
                None
            )
            response_model.from_json(response.body)
            model = self.service.application.get_module('privileged').model
            model.set_connection_status(
                'openvpn', response_model.id.get(), response_model)
            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2079, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2080, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2081, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2082, str(errors))
            response_model.id.set(connection_id)
            response_model.active.set(False)
            raise gen.Return(response_model)

    @gen.coroutine
    def disconnect(self, connection_id):
        options = {
            'connection_id': connection_id
        }
        try:
            response = yield self.service.post(
                endpoints.OPENVPN_CONN_DISCONNECT % options,
                '{}'
            )

            response_model = connection_id_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2071, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2072, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2073, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2074, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def remove(self, connection_id):
        options = {
            'connection_id': connection_id
        }
        try:
            response = yield self.service.delete(
                endpoints.OPENVPN_CONN_REMOVE % options
            )

            response_model = connection_id_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2075, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2076, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2077, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2078, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def list_connections(self):
        response = yield self.service.get(
            endpoints.OPENVPN_CONN_LIST,
            None
        )
        try:
            response_model = connection_list_model()
            response_model.from_json(response.body)

            model = self.service.application.get_module('privileged').model
            model.set_connections('openvpn', response_model)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2063, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2064, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2065, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2066, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def reconnect(self, connection_id):
        options = {
            'connection_id': connection_id
        }
        try:
            response = yield self.service.post(
                endpoints.OPENVPN_CONN_RECONNECT % options,
                '{}'
            )

            response_model = connection_id_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2059, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2060, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2061, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2062, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def setup_connection(
            self, configuration, username, password,
            openvpn_nice, openvpn_version):
        request_model = setup_model()
        try:
            request_model.configuration.set(configuration)
            request_model.username.set(username)
            request_model.password.set(password)
            request_model.nice.set(openvpn_nice)
            request_model.version.set(openvpn_version)

            response = yield self.service.put(
                endpoints.OPENVPN_CONN_SETUP,
                request_model.to_json()
            )
            response_model = connection_id_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2049, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2050, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2051, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2052, str(errors))
            raise ServerConnectionError(str(errors))
