# -*- coding: utf-8 -*-
# config.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Config for Account Edit
'''


ACCOUNT_EXISTING = 0
ACCOUNT_CREATE = 1

# Maximum number of bytes to be loaded as configuration
MAX_CONFIG_SIZE = 32768

# Default text when a new account is created
# - hint
# - easy to remove (Xorg: middle-mouse-copy)
# - no sensitive information
DEFAULT_ACCOUNT = (
    'client\n'
    'dev tun0\n'
    'remote localhost 1194\n'
    'nobind\n'
    'verb 3\n'
    '<ca>\nPASTE_YOUR_CA_IN_PEM_FORMAT_HERE\n</ca>\n'
    '<cert>\nPASTE_YOUR_CERT_IN_PEM_FORMAT_HERE\n</cert>\n'
    '<key>\nPASTE_YOUR_KEY_IN_PEM_FORMAT_HERE\n</key>\n'
)

DESCRIPTION = '''
<p><b>OpenVPN</b> is an open-source software application that implements
 virtual private network techniques for creating secure point-to-point or
 site-to-site connections in routed or bridged configurations and remote
 access facilities.</p>
<p>Presets simplify the process of connecting to a provider. To import
 existing configurations use the advanced tab. To configure
 connection timeouts and executable versions use the OpenVPN tab.</p>
<p>OpenVPN executables are shipped with the application.</p>
'''
PREFERENCES_HELP = '''
<p>Global OpenVPN options. The options are used as default for new accounts.
</p>
'''
# Default type of new accounts.
DEFAULT_ACCOUNT_TYPE = 'OpenVPN'

# Available account types. The name is used to select the editor with
# type_editor_factory.
ACCOUNT_LABEL = 'OpenVPN'
