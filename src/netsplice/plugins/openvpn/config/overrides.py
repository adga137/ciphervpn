# -*- coding: utf-8 -*-
# overrides.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from . import openvpn as config
from . import ACCOUNT_TYPE

from netsplice.backend.preferences.model.override_list import (
    override_list as override_list_model
)

overrides = override_list_model(None)

for keyword in config.KEYWORDS.keys():
    if config.KEYWORDS[keyword]['blocked'] is False:
        continue
    override = overrides.create(keyword, ACCOUNT_TYPE)
    override.value.set(None)
    overrides.append(override)
