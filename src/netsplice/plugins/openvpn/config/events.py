# -*- coding: utf-8 -*-
# events.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
from netsplice.config import connection as config_connection
from netsplice.plugins.openvpn.backend.event import names as event_names
from netsplice.backend.event import names as backend_event_names

# Events emitted into the backend
OUTGOING = [
    config_connection.SETUP_FINISH,
    config_connection.SETUP_CANCEL,
    config_connection.CONNECT,
    config_connection.CONNECT_FINISH,
    config_connection.CONNECT_PROCESS,
    config_connection.DISCONNECT,
    config_connection.DISCONNECT_ABORT,
    config_connection.DISCONNECT_FAILURE,
    config_connection.DISCONNECT_FAILURE_FINISH,
    config_connection.DISCONNECT_FAILURE_PROCESS,
    config_connection.DISCONNECT_FINISH,
    config_connection.DISCONNECT_PROCESS,
    config_connection.RECONNECT,
    config_connection.RECONNECT_PROCESS,
    config_connection.RECONNECT_FINISH,
]

# Events from the backend
INCOMMING = [
    backend_event_names.LOG_CHANGED,
    backend_event_names.ACCOUNT_REQUEST_DISCONNECT,
    event_names.OPENVPN_MODEL
]

# Events from the backend.privileged.event_controller
INCOMMING_PRIVILEGED = [
    event_names.OPENVPN_MODEL
]

# Events from the backend.unprivileged.event_controller
INCOMMING_UNPRIVILEGED = [
    event_names.OPENVPN_MODEL
]
