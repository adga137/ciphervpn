# -*- coding: utf-8 -*-
# openvpn.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

TYPE_AUTH_ALGORITHM = 'auth_algorithm'
TYPE_AUTH_RETRY = 'auth_retry'
TYPE_CERT_TYPE = 'cert_type'
TYPE_CHIPER_ALGORITHM = 'chiper_algorithm'
TYPE_COMMENT = 'comment'
TYPE_DEV = 'dev'
TYPE_DEVNAME = 'dev_name'
TYPE_DIRNAME = 'dirname'
TYPE_FILENAME = 'filename'
TYPE_FILENAME_OPT_INT = 'filename_opt_int'
TYPE_FILENAME_SECONDS = 'filename_seconds'
TYPE_FILE = 'file'
TYPE_GATEWAY = 'gateway'
TYPE_HEX = 'hex'
TYPE_HOST = 'host'
TYPE_HOST_OPT_PORT = 'host_opt_port'
TYPE_HTTP_PROXY = 'http_proxy'
TYPE_HTTP_PROXY_OPTION = 'http_proxy_option'
TYPE_IFCONFIG = 'ifconfig'
TYPE_IFCONFIG6 = 'ifconfig6'
TYPE_INETD = 'inetd'
TYPE_INT = 'int'
TYPE_INT_INFINITE = 'int_infinite'
TYPE_INT_PORT = 'int_port'
TYPE_KEYWORD = 'keyword'
TYPE_LIST = 'list'
TYPE_MAC_ADDRESS = 'mac_address'
TYPE_NO_MAYBE_YES = 'no_maybe_yes'
TYPE_MODE = 'mode'
TYPE_OPTION = 'option'
TYPE_OPTION_FLAGS = 'option_flags'
TYPE_OPTION_NAME = 'option_name'
TYPE_PLUGIN = 'plugin'
TYPE_PRNG_ALGORITM_OPT_NSL = 'alg_opt_nsl'
TYPE_PROTO = 'proto'
TYPE_PROTO_FORCE = 'proto_force'
TYPE_ROUTE = 'route'
TYPE_ROUTE6 = 'route6'
TYPE_SECONDS = 'seconds'
TYPE_SECONDS_AND_SECONDS = 'seconds_and_seconds'
TYPE_SECONDS_OR_INFINITE = 'seconds_or_infinite'
TYPE_SECONDS_OPT_BYTES = 'seconds_opt_bytes'
TYPE_SECONDS_OPT_SECONDS = 'seconds_opt_seconds'
TYPE_SIZE_OPT_SECONDS = 'size_opt_seconds'
TYPE_SOCKS_PROXY = 'socks_proxy'
TYPE_SERVER_CLIENT = 'server_client'
TYPE_SIGNAL = 'signal'
TYPE_STRING = 'string'
TYPE_TLS_CHIPER_ALGORITHM = 'tls_chiper_algorithm'
TYPE_VERSION = 'version'
TYPE_TOPOLOGY = 'topology'

ENUM_VALUES = {
    TYPE_CERT_TYPE: ['client', 'server'],
    TYPE_DEV: ['tun', 'tap'],
    TYPE_AUTH_RETRY: ['none', 'interact', 'nointeract'],
    TYPE_MODE: ['p2p', 'server'],
    TYPE_NO_MAYBE_YES: ['no', 'maybe', 'yes'],
    TYPE_PROTO: ['udp', 'tcp-server', 'tcp-client'],
    TYPE_PROTO_FORCE: ['udp6', 'tcp6-server', 'tcp6-client'],
    TYPE_SIGNAL: ['SIGHUP', 'SIGTERM'],
    TYPE_GATEWAY: ['gw', 'dhcp'],
    TYPE_TOPOLOGY: ['net30', 'p2p', 'subnet'],
}
LOG_ABORT = 'abort'
LOG_ERROR = 'error'
LOG_ERROR_CONNECTED = 'error_connected'
LOG_RESET_CREDENTIAL = 'reset_credential'
LOG_CHANGE_STATE = 'change_state'
LOG_WARNING = 'warning'
LOG_INFO = 'info'
LOG_DEBUG = 'debug'
LOG_PROGRESS = 'progress'
LOG_CONNECTING = 'connecting'
LOG_CONNECTED = 'connected'
LOG_DISCONNECT = 'disconnect'
LOG_DISCONNECTED = 'disconnected'
LOG_RECONNECT = 'reconnect'
LOG_RECONNECTING = 'reconnecting'
LOG_ROUTE = 'route'

MESSAGE_INVALID_PERMISSIONS = (
    'Invalid permissions. It is not possible to configure network'
    ' interfaces without elevation. Ensure that the current user can'
    ' elevate. This can be done using pkexec (polkit) or by setting'
    ' chmod +s /usr/sbin/openvpn (not recommended).'
)

# LOG_ERROR: implicit LOG_PROGRESS, LOG_CHANGE_STATE:, LOG_DISCONNECTED
# Always add detailed messages before the general messages.

LOG_MESSAGES = {
    # Management Interface Messages
    ('>FATAL:ERROR: Cannot ioctl TUNSETIFF tun: Operation not permitted'): {
        'actions': [LOG_ABORT],
        'help': MESSAGE_INVALID_PERMISSIONS
    },
    ('>FATAL:'): {
        'actions': [LOG_ABORT],
        'help': (
            'Fix the error.'
        )
    },
    ('>INFO:OpenVPN Management Interface Version 1'): {
        'actions': [LOG_INFO],
        'help': (
            'This debug message indicates that OpenVPN has started.'
        )
    },
    ('>PASSWORD:Verification Failed'): {
        'actions': [LOG_ABORT, LOG_RESET_CREDENTIAL],
        'help': (
            'Enter correct password for server.'
        )
    },
    ('>UPDOWN:ENV'): {
        'actions': [LOG_DEBUG],
        'help': (
            'Post connect environment from the management interface.'
        )
    },
    ('>UPDOWN:DOWN'): {
        'actions': [LOG_INFO, LOG_CHANGE_STATE],
        'help': (
            'The OpenVPN connection has disconnected. Later messages decide'
            ' to reconnect or disconnect'
        )
    },
    ('>UPDOWN:UP'): {
        'actions': [LOG_INFO, LOG_CHANGE_STATE, LOG_CONNECTED],
        'help': (
            'The OpenVPN connection is considered connected.'
        )
    },
    # CAPITALIZED Prefixed messages.
    ('AUTH: Received control message: AUTH_FAILED'): {
        'actions': [LOG_ABORT, LOG_RESET_CREDENTIAL],
        'help': (
            'The server rejected the username or the password.'
        )
    },
    ('ERROR: Linux route add command failed'): {
        'actions': [LOG_WARNING],
        'help': (
            'This usually happens because the server has pushed'
            ' multiple routes.'
        )
    },
    ('ERROR: Windows route add command failed'): {
        'actions': [LOG_WARNING],
        'help': (
            'This usually happens because the server has pushed'
            ' multiple routes.'
        )
    },
    ('MANAGEMENT: CMD'): {
        'actions': [LOG_DEBUG],
        'help': (
            'Message from the management interface.'
        )
    },
    ('MANAGEMENT: CMD \'signal SIGTERM\''): {
        'actions': [LOG_WARNING, LOG_CHANGE_STATE, LOG_DISCONNECT],
        'help': (
            'Netsplice decided to disconnect.'
        )
    },
    ('OPTIONS IMPORT:'): {
        'actions': [LOG_DEBUG],
        'help': (
            'Config send by the remote host.'
        )
    },
    ('RESOLVE: Cannot resolve host address'): {
        'actions': [LOG_WARNING],
        'help': (
            'Check that you have a internet connection.'
        )
    },
    ('ROUTE: route addition failed'): {
        'actions': [LOG_WARNING],
        'help': (
            ''
        )
    },
    ('SUCCESS: \'Auth\''): {
        'actions': [LOG_DEBUG],
        'help': (
            'Message from the management interface that it has received'
            ' credentials.'
        )
    },
    ('SUCCESS: hold release succeeded'): {
        'actions': [LOG_INFO, LOG_CONNECTING],
        'help': (
            'This debug message indicates that the privileged backend'
            ' has started to execute the configuration.'
        )
    },
    ('VERIFY OK:'): {
        'actions': [LOG_INFO],
        'help': (
            'The server certificates have been verified with the'
            ' configuration.'
        )
    },
    ('WARNING:'): {
        'actions': [LOG_WARNING],
        'help': (
            'Warning from OpenVPN process.'
        )
    },
    ('WARNING: this cipher\'s block size is less than'): {
        'actions': [LOG_WARNING],
        'help': (
            'Consider updating the server configuration.'
        )
    },
    ('TCP: connect to'): {
        'actions': [LOG_INFO],
        'exceptions': {
            'failed: Connection refused': {
                'actions': [LOG_ABORT],
                'help': (
                    'Make sure the remote host is reachable. Check'
                    ' socks-proxy if configured.'
                )
            }
        },
        'help': (
            'Fix the error in the configuration.'
        )
    },
    # Normal prefixed Messages
    ('Error: private key password verification failed'): {
        'actions': [LOG_ERROR, LOG_RESET_CREDENTIAL],
        'help': (
            'Enter correct password for private key.'
        )
    },
    ('Options error:'): {
        'actions': [LOG_ABORT],
        'exceptions': {
            'in [PUSH-OPTIONS]': {
                'actions': [LOG_WARNING],
                'help': (
                    'Push options are not considered errors, continue'
                    ' connect.'
                )
            }
        },
        'help': (
            'Fix the error in the configuration.'
        )
    },
    ('ProcessError: privileged setup:'): {
        'actions': [LOG_ABORT],
        'help': (
            'Netsplice is not correcly configured. Check the'
            ' locations in the error message'
        )
    },
    ('TLS Error: TLS handshake failed'): {
        'actions': [LOG_ERROR],
        'help': (
            'The server identity could not be verified (tcp) or the server'
            ' is not up (udp).'
        )
    },
    (
        'TLS Error: TLS key negotiation failed to occur within 60'
        'seconds (check your network connectivity)'
    ): {
        'actions': [LOG_ERROR],
        'help': (
            'The server identity could not be verified (tcp) or the server'
            ' is not up (udp).'
        )
    },
    # Messages
    ('Inactivity timeout (--ping-restart), restarting'): {
        'actions': [LOG_WARNING, LOG_CHANGE_STATE, LOG_RECONNECT],
        'help': (
            'Inactivity Timeout.'
        )
    },
    ('inactivity timeout (--ping-restart)'): {
        'actions': [LOG_WARNING, LOG_CHANGE_STATE, LOG_RECONNECT],
        'help': (
            'Inactivity Timeout.'
        )
    },
    ('Initialization Sequence Completed'): {
        'actions': [LOG_INFO, LOG_PROGRESS, LOG_CHANGE_STATE, LOG_CONNECTED],
        'help': (
            'OpenVPN has completed the initialization sequence. Waiting for'
            ' UPDOWN:UP Message'
        )
    },

    ('UDPv4 link remote'): {
        'actions': [LOG_INFO, LOG_PROGRESS],
        'help': (
            'Connection to the remote host established using UDP.'
        )
    },
    ('UDPv4 link local'): {
        'actions': [LOG_INFO, LOG_PROGRESS],
        'help': (
            'Connection to the local host established using UDP.'
        )
    },
    ('write UDPv4: Network is unreachable (code=101)'): {
        'actions': [LOG_WARNING],
        'help': (
            'Unable to route traffic. Network connection is down'
        )
    },
    ('write UDP: Can\'t assign requested address (code=49)'): {
        'actions': [LOG_WARNING, LOG_CHANGE_STATE, LOG_RECONNECT],
        'help': (
            'Unable to send traffic. Network connection is down'
        )
    },
    ('/bin/route'): {
        'actions': [LOG_INFO, LOG_ROUTE],
        'help': (
            'A route has been requested from the server'
        )
    },
    ('SIGUSR1[soft,ping-restart] received, process restarting'): {
        'actions': [LOG_WARNING, LOG_CHANGE_STATE, LOG_RECONNECT],
        'help': (
            'Restart because a reconnect was triggered.'
        )
    },
    ('SIGUSR1[soft,connection-reset] received, process restarting'): {
        'actions': [LOG_WARNING, LOG_CHANGE_STATE, LOG_RECONNECT],
        'help': (
            'Restart because a reconnect was triggered.'
        )
    },
    ('SIGUSR1[soft,no-push-reply] received, process restarting'): {
        'actions': [LOG_RECONNECT],
        'help': (
            'OpenVPN process restarting.'
        )
    },
    ('SIGUSR1[soft,tls-error] received, process restarting'): {
        'actions': [LOG_RECONNECT],
        'help': (
            'OpenVPN process restarting.'
        )
    },
    ('SIGUSR1[hard,] received, process restarting'): {
        'actions': [LOG_RECONNECT],
        'help': (
            'OpenVPN process restarting.'
        )
    },
    ('SIGUSR1[connection failed(soft),init_instance]'): {
        'actions': [LOG_ERROR],
        'help': (
            'OpenVPN process stopped, usually because of network problems.'
        )
    },
    ('SIGUSR1'): {
        'actions': [LOG_RECONNECT],
        'help': (
            'OpenVPN process restarting.'
        )
    },
    ('SIGUSR1[soft,init_instance]'): {
        'actions': [LOG_ERROR],
        'help': (
            'OpenVPN process stopped, usually because of network problems.'
        )
    },
    ('SIGTERM[hard,] received, process exiting'): {
        'actions': [LOG_DISCONNECTED],
        'help': (
            'OpenVPN process ended.'
        )
    },
    ('SIGTERM[soft,exit-with-notification] received, process exiting'): {
        'actions': [LOG_DISCONNECTED],
        'help': (
            'OpenVPN process ended.'
        )
    },
    ('SIGTERM'): {
        'actions': [LOG_DISCONNECTED],
        'help': (
            'OpenVPN process ended.'
        )
    },
    ('SUCCESS: signal SIGTERM thrown'): {
        'actions': [LOG_DISCONNECTED],
        'help': (
            'OpenVPN process ended.'
        )
    },
    ('Exiting due to fatal error'): {
        'actions': [LOG_ERROR],
        'help': (
            'Fix the error.'
        )
    },
    ('OSError:[Errno 13] Permission denied'): {
        'actions': [LOG_ABORT],
        'help': MESSAGE_INVALID_PERMISSIONS
    },
    ('RESOLVE: Cannot resolve host address:'): {
        'actions': [LOG_ABORT],
        'help': (
            'It is not possible to resolve a address, check the network uplink'
        )
    },
    ('Failed to send message to openvpn process:'): {
        'actions': [LOG_ERROR],
        'help': (
            'Sometimes OpenVPN cannot recreate sockets and shuts down'
            ' internally. Check the firewall rules that the backend and'
            ' privileged-backend can communicate using localhost.'
        )
    },
    ('Connection is no longer active.'): {
        'actions': [LOG_ERROR_CONNECTED],
        'help': (
            'OpenVPN was shut down. This is a normal message unless'
            ' the connection is in a connected state.'
        )
    },
}

KEYWORDS = {
    '#': {
        'help': (
            'Comment in the config. This option is ignored'
        ),
        'type': TYPE_COMMENT,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    ';': {
        'help': (
            'Comment in the config. This option is ignored'
        ),
        'type': TYPE_COMMENT,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'allow-pull-fqdn': {
        'help': (
            'Allow client to pull DNS names from server for --ifconfig,'
            ' --route, and --route-gateway.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'askpass': {
        'help': (
            'askpass [file]: Get PEM password from controlling tty before we'
            ' daemonize.'
        ),
        'type': TYPE_FILENAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'auth': {
        'help': (
            'auth alg: Authenticate packets with HMAC using message digest'
            ' algorithm alg (default=SHA1). (usually adds 16 or 20 bytes per'
            ' packet) Set alg=none to disable authentication.'
        ),
        'type': TYPE_AUTH_ALGORITHM,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'auth-nocache': {
        'help': (
            'Do not cache --askpass or --auth-user-pass'
            ' passwords.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False
    },
    'auth-retry': {
        'help': (
            'auth-retry t: How to handle auth failures.  Set t to none'
            ' (default), interact, or nointeract.'
        ),
        'type': TYPE_AUTH_RETRY,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'auth-user-pass': {
        'help': (
            'auth-user-pass [up]: Authenticate with server using'
            ' username/password. up is a file containing the username on the'
            ' first line, and a password on the second. If either the password'
            ' or both the username and the password are omitted OpenVPN will'
            ' prompt for them from console.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': True,
        'blocked': False,
    },
    'bind': {
        'help': (
            'Bind to local address and port. (This is the default unless'
            ' --proto  tcp-client or --http-proxy or --socks-proxy is used).'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'ca': {
        'help': (
            'ca  file: Certificate authority file in .pem format containing'
            ' root certificate.'
        ),
        'type': TYPE_FILE,
        'block': True,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'capath': {
        'help': (
            'capath  dir: A directory of trusted certificates (CAs and CRLs).'
        ),
        'type': TYPE_DIRNAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'cd': {
        'help': (
            'cd  dir: Change to this directory before initialization.'
        ),
        'type': TYPE_DIRNAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'cert': {
        'help': (
            'cert file: Local certificate in .pem format -- must be signed by'
            ' a Certificate Authority in --ca  file.'
        ),
        'type': TYPE_FILE,
        'block': True,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'chroot': {
        'help': (
            'chroot  dir: Chroot to this directory after initialization.'
        ),
        'type': TYPE_DIRNAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': True,
    },
    'cipher': {
        'help': (
            'cipher  alg: Encrypt packets with cipher algorithm alg'
            ' (default=BF-CBC). Set alg=none to disable encryption.'
        ),
        'type': TYPE_CHIPER_ALGORITHM,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'client': {
        'help': (
            'client: Helper option to easily configure client mode.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'client-nat': {
        'help': (
            'client-nat  snat|dnat network netmask alias: on client add'
            ' 1-to-1 NAT rule.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'comp-lzo': {
        'help': (
            'comp-lzo: Use fast LZO compression -- may add up to 1 byte per'
            ' packet for uncompressible data.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'comp-noadapt': {
        'help': (
            'comp-noadapt: Do not use adaptive compression when --comp-lzo is'
            ' specified.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'connect-retry': {
        'help': (
            'connect-retry  n: For --proto tcp-client, number of seconds to'
            ' wait between connection retries (default=5).'
        ),
        'type': TYPE_SECONDS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'connect-retry-max': {
        'help': (
            'connect-retry-max  n: Maximum connection attempt retries, default'
            ' infinite.'
        ),
        'type': TYPE_SECONDS_OR_INFINITE,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'connect-timeout': {
        'help': (
            'connect-timeout  n: For --proto tcp-client, connection timeout'
            ' (in seconds).'
        ),
        'type': TYPE_SECONDS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'crl-verify': {
        'help': (
            'crl-verify  crl [dir]: Check peer certificate against a CRL.'
        ),
        'type': TYPE_DIRNAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'daemon': {
        'help': (
            'daemon  [name]: Become a daemon after initialization. The'
            ' optional name parameter will be passed as the program name to'
            ' the system logger.'
        ),
        'type': TYPE_STRING,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': True,
    },
    'dev': {
        'help': (
            'dev  tunX|tapX: tun/tap device (X can be omitted for dynamic'
            ' device.'
        ),
        'type': TYPE_DEVNAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'dev-node': {
        'help': (
            'dev-node node: Explicitly set the device node rather than using'
            ' /dev/net/tun, /dev/tun, /dev/tap, etc.'
        ),
        'type': TYPE_FILENAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'dev-type': {
        'help': (
            'dev-type  dt: Which device type are we using? (dt = tun or tap)'
            ' Use this option only if the tun/tap device used with dev does'
            ' not begin with tun or tap'
        ),
        'type': TYPE_DEV,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'dh': {
        'help': (
            'dh  file: File containing Diffie Hellman parameters in .pem'
            ' format (for --tls-server  only). Use openssl dhparam -out'
            ' dh1024.pem 1024 to generate.'
        ),
        'type': TYPE_FILE,
        'block': True,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'disable-occ': {
        'help': (
            'disable-occ: Disable options consistency check between peers.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'down': {
        'help': (
            'down  cmd: Run command cmd after tun device close. (post'
            ' --user/--group  UID/GID change and/or --chroot) (command'
            ' parameters are same as --up option)'
        ),
        'type': TYPE_FILENAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': True,
    },
    'down-pre': {
        'help': (
            'down-pre: Run --down command before TUN/TAP close.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'echo': {
        'help': (
            'echo  [parms ...]: Echo parameters to log output.'
        ),
        'type': TYPE_STRING,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'engine': {
        'help': (
            'engine  [name]: Enable OpenSSL hardware crypto engine'
            ' functionality.'
        ),
        'type': TYPE_STRING,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'explicit-exit-notify': {
        'help': (
            'explicit-exit-notify  [n]: On exit/restart, send exit signal to'
            ' server/remote. n = # of retries, default=1.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'extra-certs': {
        'help': (
            'extra-certs file: one or more PEM certs that complete the cert'
            ' chain.'
        ),
        'type': TYPE_FILE,
        'block': True,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'fast-io': {
        'help': (
            'fast-io: (experimental) Optimize TUN/TAP/UDP writes.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'float': {
        'help': (
            'float: Allow remote to change its IP address/port, such as'
            ' through DHCP (this is the default if --remote is not used).'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'fragment': {
        'help': (
            'fragment  max: Enable internal datagram fragmentation so that'
            ' no UDP datagrams are sent which are larger than max bytes. Adds'
            ' 4 bytes of overhead per datagram.'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'gremlin': {
        'help': (
            'gremlin  mask: Special stress testing mode (for debugging only).'
        ),
        'type': TYPE_STRING,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'group': {
        'help': (
            'group  group: Set GID to group after initialization.'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'hand-window': {
        'help': (
            'hand-window  n: Data channel key exchange must finalize within'
            ' n seconds of handshake initiation by any peer (default=60).'
        ),
        'type': TYPE_SECONDS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'http-proxy': {
        'help': (
            'http-proxy  s p auto[-nct]: Like the above directive, but'
            ' automatically determine auth method and query for'
            ' username/password if needed.  auto-nct disables weak proxy'
            ' auth methods.'
        ),
        'type': TYPE_HTTP_PROXY,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'http-proxy-option': {
        'help': (
            'http-proxy-option  type [parm]: Set extended HTTP proxy options.'
            ' Repeat to set multiple options. VERSION version (default=1.0)'
            ' AGENT user-agent'
        ),
        'type': TYPE_HTTP_PROXY_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'http-proxy-retry': {
        'help': (
            'http-proxy-retry: Retry indefinitely on HTTP proxy errors.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'http-proxy-timeout': {
        'help': (
            'http-proxy-timeout  n: Proxy timeout in seconds, default=5.'
        ),
        'type': TYPE_SECONDS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'ifconfig': {
        'help': (
            'ifconfig  l rn: TUN: configure device to use IP address l as a'
            ' local endpoint and rn as a remote endpoint.  l & rn should be'
            ' swapped on the other peer.  l & rn must be private addresses'
            ' outside of the subnets used by either peer. TAP: configure'
            ' device to use IP address l as a local endpoint and rn as a'
            ' subnet mask.'
        ),
        'type': TYPE_IFCONFIG,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'ifconfig-ipv6': {
        'help': (
            'ifconfig-ipv6  l r: configure device to use IPv6 address l as'
            ' local endpoint (as a /64) and r as remote endpoint'
        ),
        'type': TYPE_IFCONFIG6,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'ifconfig-noexec': {
        'help': (
            'ifconfig-noexec: Do not actually execute ifconfig/netsh command,'
            ' instead pass ifconfig parms by environment to scripts.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'ifconfig-nowarn': {
        'help': (
            'ifconfig-nowarn: Do not warn if the --ifconfig option on this'
            ' side of the connection doesnt match the remote side.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'ignore-unkown-option': {
        'help': (
            'ignore-unkown-option  opt1 opt2 ...: Relax config file syntax.'
            ' Allow these options to be ignored when unknown'
        ),
        'type': TYPE_LIST,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'inactive': {
        'help': (
            'inactive  n [bytes]: Exit after n seconds of activity on tun/tap'
            ' device produces a combined in/out byte count < bytes.'
        ),
        'type': TYPE_SECONDS_OPT_BYTES,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'inetd': {
        'help': (
            'inetd [name] [wait|nowait]: Run as an inetd or xinetd server.'
            ' See --daemon above for a description of the name parm.'
        ),
        'type': TYPE_INETD,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': True,
    },
    'ipchange': {
        'help': (
            'ipchange cmd: Run command cmd on remote ip address initial'
            ' setting. execute as: cmd ip-address port#'
        ),
        'type': TYPE_FILENAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': True,
    },
    'keepalive': {
        'help': (
            'keepalive  n m: Helper option for setting timeouts in server'
            ' mode.  Send ping once every n seconds, restart if ping not'
            ' received for m seconds.'
        ),
        'type': TYPE_SECONDS_AND_SECONDS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'key': {
        'help': (
            'key  file: Local private key in .pem format.'
        ),
        'type': TYPE_FILE,
        'block': True,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'key-method': {
        'help': (
            'key-method  m: Data channel key exchange method.  m should be'
            ' a method number, such as 1 (default), 2, etc.'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'keysize': {
        'help': (
            'keysize  n: Size of cipher key in bits (optional). If'
            ' unspecified, defaults to cipher-specific default.'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'link-mtu': {
        'help': (
            'link-mtu  n: Take the TCP/UDP device MTU to be n and derive the'
            ' tun MTU from it.'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'lladdr': {
        'help': (
            'lladdr  hw: Set the link layer address of the tap device.'
        ),
        'type': TYPE_MAC_ADDRESS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'local': {
        'help': (
            'local host: Local host name or ip address. Implies --bind.'
        ),
        'type': TYPE_HOST,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'log': {
        'help': (
            'log file: Output log to file which is created/truncated on open.'
        ),
        'type': TYPE_FILENAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'log-append': {
        'help': (
            'log-append  file: Append log to file, or create file if'
            ' nonexistent.'
        ),
        'type': TYPE_FILENAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'lport': {
        'help': (
            'lport  port: TCP/UDP port # for local (default=1194). Implies'
            ' --bind.'
        ),
        'type': TYPE_INT_PORT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'max-routes': {
        'help': (
            'max-routes n: Specify the maximum number of routes that may be'
            ' defined or pulled from a server.'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'mlock': {
        'help': (
            'mlock: Disable Paging -- ensures key material and tunnel data'
            ' will never be written to disk.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'mode': {
        'help': (
            'mode m: Major mode, m = p2p (default, point-to-point) or server.'
        ),
        'type': TYPE_MODE,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'mssfix': {
        'help': (
            'mssfix  [n]: Set upper bound on TCP MSS, default = tun-mtu size'
            ' or --fragment  max value, whichever is lower.'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'mtu-disc': {
        'help': (
            'mtu-disc  type: Should we do Path MTU discovery on TCP/UDP'
            ' channel? "no" -- Never send DF (Do not Fragment) frames "maybe"'
            ' -- Use per-route hints "yes" -- Always DF (Do not Fragment)'
        ),
        'type': TYPE_NO_MAYBE_YES,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'mtu-test': {
        'help': (
            'mtu-test: Empirically measure and report MTU.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'multihome': {
        'help': (
            'multihome: Configure a multi-homed UDP server.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'mute': {
        'help': (
            'mute  n: Log at most n consecutive messages in the same category.'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'mute-replay-warnings': {
        'help': (
            'mute-replay-warnings: Silence the output of replay warnings'
            ' to log file.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'nice': {
        'help': (
            'nice  n: Change process priority (>0 = lower, <0 = higher).'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'no-iv': {
        'help': (
            'no-iv: Disable cipher IV -- only allowed with CBC mode ciphers.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'no-replay': {
        'help': (
            'no-replay: Disable replay protection.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'nobind': {
        'help': (
            'nobind: Do not bind to local address and port.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'ns-cert-type': {
        'help': (
            'ns-cert-type  t: Require that peer certificate was signed with'
            ' an explicit nsCertType designation t = client | server.'
        ),
        'type': TYPE_CERT_TYPE,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'passtos': {
        'help': (
            'passtos: TOS passthrough (applies to IPv4 only).'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'persist-key': {
        'help': (
            'persist-key: Do not re-read key files across SIGUSR1 or'
            ' --ping-restart.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'persist-local-ip': {
        'help': (
            'persist-local-ip: Keep local IP address across SIGUSR1 or'
            ' --ping-restart.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'persist-remote-ip': {
        'help': (
            'persist-remote-ip: Keep remote IP address across SIGUSR1 or'
            ' --ping-restart.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'persist-tun': {
        'help': (
            'persist-tun: Keep tun/tap device open across SIGUSR1 or'
            ' --ping-restart.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'ping': {
        'help': (
            'ping n: Ping remote once every n seconds over TCP/UDP port.'
        ),
        'type': TYPE_SECONDS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'ping-exit': {
        'help': (
            'ping-exit  n: Exit if n seconds pass without reception of'
            ' remote ping.'
        ),
        'type': TYPE_SECONDS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'ping-restart': {
        'help': (
            'ping-restart  n: Restart if n seconds pass without reception'
            ' of remote ping.'
        ),
        'type': TYPE_SECONDS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'ping-timer-rem: ': {
        'help': (
            'ping-timer-rem: Run the --ping-exit/--ping-restart timer only'
            ' if we have a remote address.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'pkcs12': {
        'help': (
            'pkcs12  file: PKCS#12 file containing local private key,'
            ' local certificate and optionally the root CA certificate.'
        ),
        'type': TYPE_FILE,
        'block': True,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'plugin': {
        'help': (
            'plugin  m [str]: Load plug-in module m passing str as an'
            ' argument to its initialization function.'
        ),
        'type': TYPE_PLUGIN,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'port': {
        'help': (
            'port  port: TCP/UDP port # for both local and remote.'
        ),
        'type': TYPE_INT_PORT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'prng': {
        'help': (
            'prng  alg [nsl]: For PRNG, use digest algorithm alg, and'
            ' nonce_secret_len=nsl.  Set alg=none to disable PRNG.'
        ),
        'type': TYPE_PRNG_ALGORITM_OPT_NSL,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'proto': {
        'help': (
            'proto  p: Use protocol p for communicating with peer. p = udp'
            ' (default), tcp-server, or tcp-client'
        ),
        'type': TYPE_PROTO,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'proto-force': {
        'help': (
            'proto-force  p: only consider protocol p in list of connection'
            ' profiles. p = udp6, tcp6-server, or tcp6-client (ipv6)'
        ),
        'type': TYPE_PROTO_FORCE,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'pull': {
        'help': (
            'pull: Accept certain config file options from the peer as if'
            ' they were part of the local config file.  Must be specified'
            ' when connecting to a --mode  server remote host.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'push-peer-info': {
        'help': (
            'push-peer-info: (client only) push client info to server.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'rcvbuf': {
        'help': (
            'rcvbuf  size: Set the TCP/UDP receive buffer size.'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'redirect-gateway': {
        'help': (
            'redirect-gateway [flags]: Automatically execute routing commands'
            ' to redirect all outgoing IP traffic through the VPN.  Add local'
            ' flag if both OpenVPN servers are directly connected via a common'
            ' subnet, such as with WiFi. Add def1 flag to set default route'
            ' using using 0.0.0.0/1 and 128.0.0.0/1 rather than 0.0.0.0/0.'
            ' Add bypass-dhcp flag to add a direct route to DHCP server,'
            ' bypassing tunnel. Add bypass-dns flag to similarly bypass tunnel'
            ' for DNS.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'redirect-private': {
        'help': (
            'redirect-private  [flags]: Like --redirect-gateway, but omit'
            ' actually changing the default gateway.  Useful when pushing'
            ' private subnets.'
        ),
        'type': TYPE_OPTION_FLAGS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'remap-usr1': {
        'help': (
            'remap-usr1  s: On SIGUSR1 signals, remap signal (s=SIGHUP'
            ' or SIGTERM).'
        ),
        'type': TYPE_SIGNAL,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'remote': {
        'help': (
            'remote host [port]: Remote host name or ip address.,'
        ),
        'type': TYPE_HOST_OPT_PORT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'remote-cert-eku': {
        'help': (
            'remote-cert-eku  oid: Require that the peer certificate was'
            ' signed with explicit extended key usage. Extended key usage'
            ' can be encoded as an object identifier or OpenSSL string'
            ' representation.'
        ),
        'type': TYPE_STRING,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'remote-cert-ku': {
        'help': (
            'remote-cert-ku  v ...: Require that the peer certificate was'
            ' signed with explicit key usage, you can specify more than one'
            ' value. value should be given in hex format.'
        ),
        'type': TYPE_STRING,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'remote-cert-tls': {
        'help': (
            'remote-cert-tls  t: Require that peer certificate was signed with'
            ' explicit key usage and extended key usage based on RFC3280 TLS'
            ' rules. t = client | server.'
        ),
        'type': TYPE_CERT_TYPE,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'remote-random': {
        'help': (
            'remote-random: If multiple --remote options specified, choose'
            ' one randomly.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'remote-random-hostname': {
        'help': (
            'remote-random-hostname: Add a random string to remote DNS name.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'reneg-bytes': {
        'help': (
            'reneg-bytes  n: Renegotiate data chan. key after n bytes sent'
            ' and recvd.'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'reneg-pkts': {
        'help': (
            'reneg-pkts  n: Renegotiate data chan. key after n packets sent'
            ' and recvd.'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'reneg-sec': {
        'help': (
            'reneg-sec  n: Renegotiate data chan. key after n seconds'
            ' (default=3600).'
        ),
        'type': TYPE_SECONDS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'replay-persist': {
        'help': (
            'replay-persist  file: Persist replay-protection state across'
            ' sessions using file.'
        ),
        'type': TYPE_FILENAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'replay-window': {
        'help': (
            'replay-window  n [t]: Use a replay protection sliding window of'
            ' size n and a time window of t seconds. Default n=64 t=15'
        ),
        'type': TYPE_SIZE_OPT_SECONDS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'resolv-retry': {
        'help': (
            'resolv-retry  n: If hostname resolve fails for --remote, retry'
            ' resolve for n seconds before failing (disabled by default).'
            ' Set n=infinite to retry indefinitely.'
        ),
        'type': TYPE_SECONDS_OR_INFINITE,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'route': {
        'help': (
            'route  network [netmask] [gateway] [metric]: Add route to'
            ' routing table after connection is established.  Multiple'
            ' routes can be specified. netmask default: 255.255.255.255'
            ' gateway default: taken from route-gateway: route-gateway'
            '  or --ifconfig Specify default by leaving blank or setting to'
            ' nil.'
        ),
        'type': TYPE_ROUTE,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'route-delay': {
        'help': (
            'route-delay  n [w]: Delay n seconds after connection initiation'
            ' before adding routes (may be 0).  If not specified, routes will'
            ' be added immediately after tun/tap open.  On Windows, wait up to'
            ' w seconds for TUN/TAP adapter to come up.'
        ),
        'type': TYPE_SECONDS_OPT_SECONDS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'route-gateway': {
        'help': (
            'route-gateway  gw|dhcp: Specify a default gateway for use with'
            ' --route.'
        ),
        'type': TYPE_GATEWAY,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'route-ipv6': {
        'help': (
            'route-ipv6 network/bits [gateway] [metric]: Add IPv6 route to'
            ' routing table after connection is established.  Multiple routes'
            ' can be specified. gateway default: taken from route-ipv6-gateway'
            ' or --ifconfig'
        ),
        'type': TYPE_ROUTE6,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'route-metric': {
        'help': (
            'route-metric  m: Specify a default metric for use with --route.'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'route-noexec': {
        'help': (
            'route-noexec: Do not add routes automatically.  Instead pass'
            ' routes to route-up script using environmental variables.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'route-nopull': {
        'help': (
            'route-nopull: When used with --client or --pull, accept options'
            ' pushed by server EXCEPT for routes and dhcp options.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'route-pre-down': {
        'help': (
            'route-pre-down  cmd: Run command cmd before routes are removed.'
        ),
        'type': TYPE_FILENAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': True,
    },
    'route-up': {
        'help': (
            'route-up  cmd: Run command cmd after routes are added.'
        ),
        'type': TYPE_FILENAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': True,
    },
    'rport': {
        'help': (
            'rport  port: TCP/UDP port # for remote (default=1194).'
        ),
        'type': TYPE_INT_PORT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'script-security': {
        'help': (
            'script-security  level: Where level can be: 0 -- strictly no'
            ' calling of external programs 1 -- (default) only call built-ins'
            ' such as ifconfig 2 -- allow calling of built-ins and scripts 3'
            ' -- allow password to be passed to scripts via env'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'secret': {
        'help': (
            'secret  f [d]: Enable Static Key encryption mode (non-TLS). Use'
            ' shared secret file f, generate with --genkey. The optional d'
            ' parameter controls key directionality. If d is specified, use'
            ' separate keys for each direction, set d=0 on one side of the'
            ' connection, and d=1 on the other side.'
        ),
        'type': TYPE_FILENAME_OPT_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'server-poll-timeout': {
        'help': (
            'server-poll-timeout  n: when polling possible remote servers to'
            ' connect to in a round-robin fashion, spend no more than n'
            ' seconds waiting for a response before trying the next server.'
        ),
        'type': TYPE_SECONDS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'setenv': {
        'help': (
            'setenv  FORWARD_COMPATIBLE 1: Relax config file syntax checking'
            ' to allow directives for future OpenVPN versions to be ignored.'
        ),
        'type': TYPE_STRING,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'shaper': {
        'help': (
            'shaper n: Restrict output to peer to n bytes per second.'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'single-session: ': {
        'help': (
            'single-session: Allow only one session (reset state on restart).'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'sndbuf': {
        'help': (
            'sndbuf size: Set the TCP/UDP send buffer size.'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'socks-proxy': {
        'help': (
            'socks-proxy  s [p] [up]: Connect to remote host through a Socks5'
            ' proxy at address s and port p (default port = 1080). If proxy'
            ' authentication is required, up is a file containing'
            ' username/password on 2 lines, or stdin to prompt for console.'
        ),
        'type': TYPE_SOCKS_PROXY,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'socks-proxy-retry': {
        'help': (
            'socks-proxy-retry: Retry indefinitely on Socks proxy errors.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'static-challenge': {
        'help': (
            'static-challenge  t e: Enable static challenge/response protocol'
            ' using challenge text t, with e indicating echo flag (0|1)'
        ),
        'type': TYPE_STRING,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'status': {
        'help': (
            'status  file n: Write operational status to file every n seconds.'
        ),
        'type': TYPE_FILENAME_SECONDS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'status-version': {
        'help': (
            'status-version  [n]: Choose the status file format version'
            ' number. Currently, n can be 1, 2, or 3 (default=1).'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'suppress-timestamps': {
        'help': (
            'suppress-timestamps: Do not log timestamps to stdout/stderr.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'syslog': {
        'help': (
            'syslog  [name]: Output to syslog, but do not become a daemon.'
            ' See --daemonabove for a description of the name parm.'
        ),
        'type': TYPE_OPTION_NAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'tls-auth': {
        'help': (
            'tls-auth  f [d]: Add an additional layer of authentication on'
            ' top of the TLS control channel to protect against DoS attacks.'
            ' f (required) is a shared-secret passphrase file. The optional'
            ' d parameter controls key directionality, see --secret  option'
            ' for more info.'
        ),
        'type': TYPE_FILENAME_OPT_INT,
        'block': True,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'tls-cipher': {
        'help': (
            'tls-cipher  l: A list l of allowable TLS ciphers separated by:'
            ' (optional).: Use --show-tls  to see a list of supported TLS'
            ' ciphers.'
        ),
        'type': TYPE_TLS_CHIPER_ALGORITHM,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'tls-client': {
        'help': (
            'tls-client: Enable TLS and assume client role during TLS'
            ' handshake.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'tls-exit': {
        'help': (
            'tls-exit: Exit on TLS negotiation failure.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'tls-export-cert': {
        'help': (
            'tls-export-cert  [directory]: Get peer cert in PEM format and'
            ' store it in an openvpn temporary file in [directory]. Peer'
            ' cert is  stored before tls-verify script execution and'
            ' deleted after.'
        ),
        'type': TYPE_DIRNAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'tls-server': {
        'help': (
            'tls-server: Enable TLS and assume server role during TLS'
            ' handshake.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'tls-timeout': {
        'help': (
            'tls-timeout n: Packet retransmit timeout on TLS control channel'
            ' if no ACK from remote within n seconds (default=2).'
        ),
        'type': TYPE_SECONDS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'tls-verify': {
        'help': (
            'tls-verify  cmd: Run command cmd to verify the X509 name of a'
            ' pending TLS connection that has otherwise passed all other tests'
            ' of certification.  cmd should return 0 to allow TLS handshake'
            ' to proceed, or 1 to fail.  (cmd is executed as cmd'
            ' certificate_depth subject)'
        ),
        'type': TYPE_FILENAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': True,
    },
    'tls-version-max': {
        'help': (
            'tls-version-max  <version>: sets the maximum TLS version we will'
            ' use.'
        ),
        'type': TYPE_VERSION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'tls-version-min': {
        'help': (
            'tls-version-min  <version> [or-highest]: sets the minimum TLS'
            ' version we will accept from the peer.  If version is'
            ' unrecognized and or-highest is specified, require max TLS'
            ' version supported by SSL implementation.'
        ),
        'type': TYPE_VERSION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'topology': {
        'help': (
            'topology  t: Set --dev tun topology: net30, p2p, or subnet.'
        ),
        'type': TYPE_TOPOLOGY,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'tran-window': {
        'help': (
            'tran-window  n: Transition window -- old key can live this'
            ' many seconds after new key renegotiation begins (default=3600).'
        ),
        'type': TYPE_SECONDS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'tun-ipv6': {
        'help': (
            'tun-ipv6: Build tun link capable of forwarding IPv6 traffic.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'tun-mtu': {
        'help': (
            'tun-mtu  n: Take the tun/tap device MTU to be n and derive the'
            ' TCP/UDP MTU from it (default=1500).'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'tun-mtu-extra': {
        'help': (
            'tun-mtu-extra  n: Assume that tun/tap device might return as'
            ' many as n bytes more than the tun-mtu size on read (default'
            ' TUN=0 TAP=32).'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'txqueuelen': {
        'help': (
            'txqueuelen  n: Set the tun/tap TX queue length to n (Linux only).'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'up': {
        'help': (
            'up  cmd: Run command cmd after successful tun device open.'
            ' Execute as: cmd tun/tap-dev tun-mtu link-mtu ifconfig-local-ip'
            ' ifconfig-remote-ip (pre --user or --group UID/GID change)'
        ),
        'type': TYPE_FILENAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': True,
    },
    'up-delay': {
        'help': (
            'up-delay: Delay tun/tap open and possible --up script execution'
            ' until after TCP/UDP connection establishment with peer.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'up-restart': {
        'help': (
            'up-restart: Run up/down commands for all restarts including those'
            ' caused by --ping-restart or SIGUSR1'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'user': {
        'help': (
            'user: Set UID to user after initialization.'
        ),
        'type': TYPE_STRING,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'verb': {
        'help': (
            'verb  n: Set output verbosity to n (default=1): (Level 3 is'
            ' recommended if you want a good summary of whats happening'
            ' without being swamped by output).: 0  no output except fatal'
            ' errors: 1 -- startup info + connection initiated messages'
            ' + non-fatal encryption & net errors: 2,3 --  show TLS'
            ' negotiations & route info: 4 -- show parameters: 5 --  show'
            ' RrWw chars on console for each packet sent and received from'
            ' TCP/UDP (caps) or tun/tap (lc): 6 to 11 -- debug messages of'
            ' increasing verbosity'
        ),
        'type': TYPE_INT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'verify-hash': {
        'help': (
            'verify-hash: Specify SHA1 fingerprint for level-1 cert.'
        ),
        'type': TYPE_STRING,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'verify-x509-name': {
        'help': (
            'verify-x509-name  name: Accept connections only from a host with'
            ' X509 subject DN name. The remote host must also pass all other'
            ' tests of verification.'
        ),
        'type': TYPE_STRING,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'writepid': {
        'help': (
            'writepid  file: Write main process ID to file.'
        ),
        'type': TYPE_FILENAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'x509-track': {
        'help': (
            'x509-track  x: Save peer X509 attribute x in environment for use'
            ' by plugins and management interface.'
        ),
        'type': TYPE_STRING,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
}
