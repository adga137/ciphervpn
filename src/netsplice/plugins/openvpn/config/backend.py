# -*- coding: utf-8 -*-
# process.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Configuration for backend.
'''

# Default OpenVPN Version
# update pkg/debian-build/ Makefile.vars, build.sh.inc, slaves/sysosx/Makefile
DEFAULT_OPENVPN_VERSION = 'v2.4.6-vanilla-libressl'

# Default Nice (-20 to 19)
# Controls the niceness of the openvpn process.
DEFAULT_NICE = 0
