#!/bin/bash
#
# Test backend api
# execute curl commands on the backend api and evaluates its output. Used
# for quick integration test during development.
# set -x
echo 'this assumes that you disabled HTTPS and HMAC authentication'
echo 'and the backend is running on port 10000 on localhost'

function die(){
    echo "$@"
    section "FAILURE"
    exit 1
}
function section(){
    printf '\n'
    printf -v _hr "%*s" $(tput cols) && echo ${_hr// /-}
    printf '[ %s ]\n' "$@"
}
function tcurl(){
    code=$1
    shift
    content_filter=$1
    shift
    method=$1
    if [ ${method} == "GET" ]; then
        method=""
    else
        method="-X ${method}"
    fi
    shift
    url=$1
    section "curl: ${code} ${url}"
    shift
    if [ -z "$1" ]; then
        data=""
    else
        data="--data '"$@"'"
    fi
    export tcurl_result=/tmp/test.result
    export tcurl_script=/tmp/test.script
    echo "curl -sS -v ${url} ${method} ${data} > \$1 2> \$2" > ${tcurl_script}
    bash ${tcurl_script} ${tcurl_result} ${tcurl_result}.2

    rescode=$(cat ${tcurl_result}.2 | grep "HTTP/1.1 "| sed "s|< HTTP/1.1 \([0-9]*\).*|\1|")
    cat ${tcurl_result}.2 | grep "HTTP/1.1 ${code}" > /dev/null \
      && echo "[OK: code: ${code}]" \
      || die "bad response code ${code} vs ${rescode} for ${method} ${url}"

    result_output=$(cat ${tcurl_result})
    if [ "${result_output}" == "" ]; then
      if [ "${content_filter}" == "" ]; then
        echo "[OK: content: '${content_filter}']"
      else
        die "bad response, expecting '${content_filter}' in '${result_output}'"
      fi
    else
      cat ${tcurl_result} | grep "${content_filter}" > /dev/null \
        && echo "[OK: content: '${content_filter}']" \
        || die "bad response, expecting '${content_filter}' in '${result_output}'"
    fi

    export TCURL_RESULT=${result}
}

# preparation:
# * remove preferences

# tor integration
function test_connect_disconnect_delete(){
    section "module"
    tcurl 200 "" POST http://localhost:10000/module/preferences/accounts \
        "{ \
        \"name\": \"tor-account\", \
        \"configuration\":\"# VER: 0.25\nclient\ndev tunX\nproto udp\nremote nat.openvpn.ipredator.se 1194\nremote nat.openvpn.ipredator.me 1194\nremote nat.openvpn.ipredator.es 1194\nresolv-retry infinite\nnobind\nauth-user-pass\nauth-retry nointeract\nca [inline]\ntls-client\ntls-auth [inline]\nns-cert-type server\nremote-cert-tls server\nremote-cert-ku 0x00e0\nkeepalive 10 30\ncipher AES-256-CBC\npersist-key\ncomp-lzo\ntun-mtu 1500\nmssfix 1200\npasstos\nverb 3\nreplay-window 512 60\nmute-replay-warnings\nifconfig-nowarn\n# Disable this if your system does not support it!\ntls-version-min 1.2\n<ca>\n-----BEGIN CERTIFICATE-----\nMIIFJzCCBA+gAwIBAgIJAKee4ZMMpvhzMA0GCSqGSIb3DQEBBQUAMIG9MQswCQYD\nVQQGEwJTRTESMBAGA1UECBMJQnJ5Z2dsYW5kMQ8wDQYDVQQHEwZPZWxkYWwxJDAi\nBgNVBAoTG1JveWFsIFN3ZWRpc2ggQmVlciBTcXVhZHJvbjESMBAGA1UECxMJSW50\nZXJuZXR6MScwJQYDVQQDEx5Sb3lhbCBTd2VkaXNoIEJlZXIgU3F1YWRyb24gQ0Ex\nJjAkBgkqhkiG9w0BCQEWF2hvc3RtYXN0ZXJAaXByZWRhdG9yLnNlMB4XDTEyMDgw\nNDIxMTAyNVoXDTIyMDgwMjIxMTAyNVowgb0xCzAJBgNVBAYTAlNFMRIwEAYDVQQI\nEwlCcnlnZ2xhbmQxDzANBgNVBAcTBk9lbGRhbDEkMCIGA1UEChMbUm95YWwgU3dl\nZGlzaCBCZWVyIFNxdWFkcm9uMRIwEAYDVQQLEwlJbnRlcm5ldHoxJzAlBgNVBAMT\nHlJveWFsIFN3ZWRpc2ggQmVlciBTcXVhZHJvbiBDQTEmMCQGCSqGSIb3DQEJARYX\naG9zdG1hc3RlckBpcHJlZGF0b3Iuc2UwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw\nggEKAoIBAQCp5M22fZtwtIh6Mu9IwC3N2tEFqyNTEP1YyXasjf+7VNISqSpFy+tf\nDsHAkiE9Wbv8KFM9bOoVK1JjdDsetxArm/RNsUWm/SNyVbmY+5ezX/n95S7gQdMi\nbA74/ID2+KsCXUY+HNNUQqFpyK67S09A6r0ZwPNUDbLgGnmCZRMDBPCHCbiK6e68\nd75v6f/0nY4AyAAAyqwAELIAn6sy4rzoPbalxcO33eW0fUG/ir41qqo8BQrWKyEd\nQ9gy8tGEqbLQ+B30bhIvBh10YtWq6fgFZJzWP6K8bBJGRvioFOyQHCaVH98UjwOm\n/AqMTg7LwNrpRJGcKLHzUf3gNSHQGHfzAgMBAAGjggEmMIIBIjAdBgNVHQ4EFgQU\npRqJxaYdvv3XGEECUqj7DJJ8ptswgfIGA1UdIwSB6jCB54AUpRqJxaYdvv3XGEEC\nUqj7DJJ8ptuhgcOkgcAwgb0xCzAJBgNVBAYTAlNFMRIwEAYDVQQIEwlCcnlnZ2xh\nbmQxDzANBgNVBAcTBk9lbGRhbDEkMCIGA1UEChMbUm95YWwgU3dlZGlzaCBCZWVy\nIFNxdWFkcm9uMRIwEAYDVQQLEwlJbnRlcm5ldHoxJzAlBgNVBAMTHlJveWFsIFN3\nZWRpc2ggQmVlciBTcXVhZHJvbiBDQTEmMCQGCSqGSIb3DQEJARYXaG9zdG1hc3Rl\nckBpcHJlZGF0b3Iuc2WCCQCnnuGTDKb4czAMBgNVHRMEBTADAQH/MA0GCSqGSIb3\nDQEBBQUAA4IBAQB8nxZJaTvMMoSG47jD2w31zt9o6nSx8XJKop/0rMMHKBe1QBUw\n/n3clGwYxBW8mTnrXHhmJkwJzA0Vh525+dkF28E0I+DSigKUXEewIZtKjADYSxaG\nM+4272enbJ86JeXUhN8oF9TT+LKgMBgtt9yX5o63Ek6QOKwovH5kemDOVJmwae9p\ntXQEWfCPDFMc7VfSxS4BDBVinRWeMWZs+2AWeWu2CMsjcx7+B+kPbBCzfANanFDD\nCZEQON4pEpfK2XErhOudKEJGCl7psH+9Ex//pqsUS43nVN/4sqydiwbi+wQuUI3P\nBYtvqPnWdjIdf2ayAQQCWliAx9+P03vbef6y\n-----END CERTIFICATE-----\n</ca>\n<tls-auth>\n-----BEGIN OpenVPN Static key V1-----\n03f7b2056b9dc67aa79c59852cb6b35a\na3a15c0ca685ca76890bbb169e298837\n2bdc904116f5b66d8f7b3ea6a5ff05cb\nfc4f4889d702d394710e48164b28094f\na0e1c7888d471da39918d747ca4bbc2f\n285f676763b5b8bee9bc08e4b5a69315\nd2ff6b9f4b38e6e2e8bcd05c8ac33c5c\n56c4c44dbca35041b67e2374788f8977\n7ad4ab8e06cd59e7164200dfbadb942a\n351a4171ab212c23bee1920120f81205\nefabaa5e34619f13adbe58b6c83536d3\n0d34e6466feabdd0e63b39ad9bb1116b\n37fafb95759ab9a15572842f70e7cba9\n69700972a01b21229eba487745c091dd\n5cd6d77bdc7a54a756ffe440789fd39e\n97aa9abe2749732b7262f82e4097bee3\n-----END OpenVPN Static key V1-----\n</tls-auth>\", \
        \"import_configuration\": \"# VER: 0.25\nclient\ndev tunX\nproto udp\nremote nat.openvpn.ipredator.se 1194\nremote nat.openvpn.ipredator.me 1194\nremote nat.openvpn.ipredator.es 1194\nresolv-retry infinite\nnobind\nauth-user-pass\nauth-retry nointeract\nca [inline]\ntls-client\ntls-auth [inline]\nns-cert-type server\nremote-cert-tls server\nremote-cert-ku 0x00e0\nkeepalive 10 30\ncipher AES-256-CBC\npersist-key\ncomp-lzo\ntun-mtu 1500\nmssfix 1200\npasstos\nverb 3\nreplay-window 512 60\nmute-replay-warnings\nifconfig-nowarn\n# Disable this if your system does not support it!\ntls-version-min 1.2\n<ca>\n-----BEGIN CERTIFICATE-----\nMIIFJzCCBA+gAwIBAgIJAKee4ZMMpvhzMA0GCSqGSIb3DQEBBQUAMIG9MQswCQYD\nVQQGEwJTRTESMBAGA1UECBMJQnJ5Z2dsYW5kMQ8wDQYDVQQHEwZPZWxkYWwxJDAi\nBgNVBAoTG1JveWFsIFN3ZWRpc2ggQmVlciBTcXVhZHJvbjESMBAGA1UECxMJSW50\nZXJuZXR6MScwJQYDVQQDEx5Sb3lhbCBTd2VkaXNoIEJlZXIgU3F1YWRyb24gQ0Ex\nJjAkBgkqhkiG9w0BCQEWF2hvc3RtYXN0ZXJAaXByZWRhdG9yLnNlMB4XDTEyMDgw\nNDIxMTAyNVoXDTIyMDgwMjIxMTAyNVowgb0xCzAJBgNVBAYTAlNFMRIwEAYDVQQI\nEwlCcnlnZ2xhbmQxDzANBgNVBAcTBk9lbGRhbDEkMCIGA1UEChMbUm95YWwgU3dl\nZGlzaCBCZWVyIFNxdWFkcm9uMRIwEAYDVQQLEwlJbnRlcm5ldHoxJzAlBgNVBAMT\nHlJveWFsIFN3ZWRpc2ggQmVlciBTcXVhZHJvbiBDQTEmMCQGCSqGSIb3DQEJARYX\naG9zdG1hc3RlckBpcHJlZGF0b3Iuc2UwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw\nggEKAoIBAQCp5M22fZtwtIh6Mu9IwC3N2tEFqyNTEP1YyXasjf+7VNISqSpFy+tf\nDsHAkiE9Wbv8KFM9bOoVK1JjdDsetxArm/RNsUWm/SNyVbmY+5ezX/n95S7gQdMi\nbA74/ID2+KsCXUY+HNNUQqFpyK67S09A6r0ZwPNUDbLgGnmCZRMDBPCHCbiK6e68\nd75v6f/0nY4AyAAAyqwAELIAn6sy4rzoPbalxcO33eW0fUG/ir41qqo8BQrWKyEd\nQ9gy8tGEqbLQ+B30bhIvBh10YtWq6fgFZJzWP6K8bBJGRvioFOyQHCaVH98UjwOm\n/AqMTg7LwNrpRJGcKLHzUf3gNSHQGHfzAgMBAAGjggEmMIIBIjAdBgNVHQ4EFgQU\npRqJxaYdvv3XGEECUqj7DJJ8ptswgfIGA1UdIwSB6jCB54AUpRqJxaYdvv3XGEEC\nUqj7DJJ8ptuhgcOkgcAwgb0xCzAJBgNVBAYTAlNFMRIwEAYDVQQIEwlCcnlnZ2xh\nbmQxDzANBgNVBAcTBk9lbGRhbDEkMCIGA1UEChMbUm95YWwgU3dlZGlzaCBCZWVy\nIFNxdWFkcm9uMRIwEAYDVQQLEwlJbnRlcm5ldHoxJzAlBgNVBAMTHlJveWFsIFN3\nZWRpc2ggQmVlciBTcXVhZHJvbiBDQTEmMCQGCSqGSIb3DQEJARYXaG9zdG1hc3Rl\nckBpcHJlZGF0b3Iuc2WCCQCnnuGTDKb4czAMBgNVHRMEBTADAQH/MA0GCSqGSIb3\nDQEBBQUAA4IBAQB8nxZJaTvMMoSG47jD2w31zt9o6nSx8XJKop/0rMMHKBe1QBUw\n/n3clGwYxBW8mTnrXHhmJkwJzA0Vh525+dkF28E0I+DSigKUXEewIZtKjADYSxaG\nM+4272enbJ86JeXUhN8oF9TT+LKgMBgtt9yX5o63Ek6QOKwovH5kemDOVJmwae9p\ntXQEWfCPDFMc7VfSxS4BDBVinRWeMWZs+2AWeWu2CMsjcx7+B+kPbBCzfANanFDD\nCZEQON4pEpfK2XErhOudKEJGCl7psH+9Ex//pqsUS43nVN/4sqydiwbi+wQuUI3P\nBYtvqPnWdjIdf2ayAQQCWliAx9+P03vbef6y\n-----END CERTIFICATE-----\n</ca>\n<tls-auth>\n-----BEGIN OpenVPN Static key V1-----\n03f7b2056b9dc67aa79c59852cb6b35a\na3a15c0ca685ca76890bbb169e298837\n2bdc904116f5b66d8f7b3ea6a5ff05cb\nfc4f4889d702d394710e48164b28094f\na0e1c7888d471da39918d747ca4bbc2f\n285f676763b5b8bee9bc08e4b5a69315\nd2ff6b9f4b38e6e2e8bcd05c8ac33c5c\n56c4c44dbca35041b67e2374788f8977\n7ad4ab8e06cd59e7164200dfbadb942a\n351a4171ab212c23bee1920120f81205\nefabaa5e34619f13adbe58b6c83536d3\n0d34e6466feabdd0e63b39ad9bb1116b\n37fafb95759ab9a15572842f70e7cba9\n69700972a01b21229eba487745c091dd\n5cd6d77bdc7a54a756ffe440789fd39e\n97aa9abe2749732b7262f82e4097bee3\n-----END OpenVPN Static key V1-----\n</tls-auth>\", \
        \"type\": \"OpenVPN\", \
        \"enabled\": true, \
        \"default_route\": false, \
        \"autostart\": false \
        }"
    cat ${tcurl_result} | grep '^{"id": "' || die 'no account id'
    account_1_id=$(cat ${tcurl_result} | grep '^{"id": "' | sed 's|.*id": "\([^"]*\).*|\1|')
    printf "new account id: ${account_1_id}"
    tcurl 200 "" POST http://localhost:10000/module/gui/accounts/${account_1_id}/connect
    cat ${tcurl_result}
    sleep 5
    tcurl 200 "" POST  http://localhost:10000/module/gui/credential \
        "{\"username\":\"${VPN_USERNAME}\",\"password\":\"${VPN_PASSWORD}\",\"account_id\":\"${account_1_id}\",\"store_password\":\"never\"}"
    cat ${tcurl_result}
    sleep 15
    tcurl 200 "" POST http://localhost:10000/module/gui/accounts/${account_1_id}/disconnect
    cat ${tcurl_result}
    tcurl 200 "wait_for_connection - Connection state changed" GET http://localhost:10000/module/gui/logs
    cat ${tcurl_result}
    cat ${tcurl_result} | jq '.[-30:]' | jq '.[].formated_message'
    tcurl 200 "" DELETE http://localhost:10000/module/preferences/accounts/${account_1_id}
}


echo "Hallo developer,
This script creates and deletes accounts and therefore has a forced exit
built in. You have to edit the file to start and should be prepared to recreate
all settings.
VPN_USERNAME: ${VPN_USERNAME}
VPN_PASSWORD: ${VPN_PASSWORD}

Always leave the exit line in git.
"
exit
# ^ required, do not remove.
test_connect_disconnect_delete
