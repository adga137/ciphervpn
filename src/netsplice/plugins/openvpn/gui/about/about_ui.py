# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/plugins/openvpn/gui/about/views/about.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_About(object):
    def setupUi(self, about):
        about.setObjectName("about")
        about.setMinimumSize(QtCore.QSize(100, 200))
        self.about_layout = QtGui.QVBoxLayout(about)
        self.about_layout.setObjectName("about_layout")
        self.version_tabs = QtGui.QTabWidget(about)
        self.version_tabs.setObjectName("version_tabs")
        self.about_layout.addWidget(self.version_tabs)

        self.retranslateUi(about)
        QtCore.QMetaObject.connectSlotsByName(about)

    def retranslateUi(self, about):
        pass

