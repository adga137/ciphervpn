# -*- coding: utf-8 -*-
# about.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
About Widget.

This widget gets embedded in the account pane.
'''

from PySide.QtGui import (
    QFrame, QLabel, QScrollArea, QVBoxLayout, QWidget
)
from PySide.QtCore import Qt, Signal
from about_ui import Ui_About
from netsplice.plugins.openvpn.gui.model.response.version_list \
    import (
        version_list as response_version_list_model
    )
from netsplice.plugins.openvpn.gui.model.response.version_detail \
    import (
        version_detail as response_version_detail_model
    )


class about(QWidget):
    '''
    About Widget.

    Custom Widget that gets embedded in the core about dialog.
    '''

    backend_failed = Signal(basestring)
    executable_versions_loaded = Signal(basestring)
    version_details_loaded = Signal(basestring)

    def __init__(self, parent, dispatcher):
        '''
        Initialize Module.

        Initialize widget and abstract base, Setup UI.
        '''
        QWidget.__init__(self, parent)
        self.backend = dispatcher
        self.ui = Ui_About()
        self.ui.setupUi(self)
        self.setContentsMargins(0, 0, 0, 0)
        self.ui.about_layout.setSpacing(0)
        self.ui.about_layout.setContentsMargins(0, 0, 0, 0)
        self.version_tabs = []
        self.executable_versions_loaded.connect(
            self._executable_versions_loaded)
        self.version_details_loaded.connect(self._version_details_loaded)
        self.backend.openvpn.get_version_list.emit(
            self.executable_versions_loaded, self.backend_failed)

    def _executable_versions_loaded(self, model_json):
        '''
        Executable Versions loaded.
        '''
        version_list = response_version_list_model()
        version_list.from_json(model_json)
        for version_item in version_list.get_sorted_list():
            version = version_item.version.get()
            tab = QWidget(self)
            tab.version = version
            self.version_tabs.append(tab)
            self.ui.version_tabs.addTab(
                tab, version)
            self.backend.openvpn.get_version_detail.emit(
                version,
                self.version_details_loaded, self.backend_failed)

    def _version_details_loaded(self, model_json):
        '''
        Version Details Loaded.
        '''
        version_detail = response_version_detail_model()
        version_detail.from_json(model_json)
        for tab_widget in self.version_tabs:
            if tab_widget.version != version_detail.version.get():
                continue
            detail_text = version_detail.detail.get()
            tab_widget_layout = QVBoxLayout(tab_widget)
            scroll_area = QScrollArea(tab_widget)
            scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
            scroll_area.setWidgetResizable(True)
            scroll_area.setFrameShape(QFrame.NoFrame)

            tab_widget_description = QLabel(tab_widget)
            tab_widget_description.setText(
                detail_text)
            tab_widget_description.setWordWrap(True)
            tab_widget_description.setTextInteractionFlags(
                Qt.TextSelectableByMouse)
            scroll_area.setWidget(tab_widget_description)
            tab_widget_layout.addWidget(scroll_area)
