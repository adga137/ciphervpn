# -*- coding: utf-8 -*-
# version_list.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for listing versions. This is a response model that will be filled
temporary for JSON responses.
'''

from distutils.version import LooseVersion

from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.util.errors import NotFoundError
from version_list_item import version_list_item as version_list_item_model


class version_list(marshalable_list):
    '''
    Marshalable model that contains multiple items of
    version_list_item_model.
    '''
    def __init__(self):
        marshalable_list.__init__(self, version_list_item_model)

    def find_by_version(self, version):
        '''
        Find by Version.

        Return the version_list_item with the given version.
        '''
        for version_item in self:
            if version_item.version.get() == version:
                return version_item
        raise NotFoundError(
            'Version %s is not in the version list.' % (version,))

    def get_sorted_list(self):
        '''
        Sorted.

        Return a sorted list from the version items.
        Create a list with only the versions in them, sort this list using
        an algorithm that takes 0.1.10 > 0.1.9 and popuplate a new list in
        the order created by the sort algorithm.
        '''
        sortable_versions = []
        sorted_versions = []

        for version_item in self:
            sortable_versions.append(version_item.version.get())

        for version in sorted(sortable_versions, key=LooseVersion):
            try:
                sorted_versions.append(self.find_by_version(version))
            except NotFoundError:
                pass

        return sorted_versions
