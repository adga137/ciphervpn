# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/plugins/openvpn/gui/account_edit/account_type_editor/views/account_type_editor.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_AccountTypeEditor(object):
    def setupUi(self, openvpn_editor):
        openvpn_editor.setObjectName("openvpn_editor")
        openvpn_editor.setMinimumSize(QtCore.QSize(100, 400))
        self.editor_layout = QtGui.QVBoxLayout(openvpn_editor)
        self.editor_layout.setObjectName("editor_layout")
        self.detail_stack = QtGui.QStackedWidget(openvpn_editor)
        self.detail_stack.setObjectName("detail_stack")
        self.advanced = QtGui.QWidget()
        self.advanced.setObjectName("advanced")
        self.advanced_layout = QtGui.QVBoxLayout(self.advanced)
        self.advanced_layout.setObjectName("advanced_layout")
        self.widget = QtGui.QWidget(self.advanced)
        self.widget.setObjectName("widget")
        self.advanced_button_layout = QtGui.QHBoxLayout(self.widget)
        self.advanced_button_layout.setContentsMargins(0, 0, 0, 0)
        self.advanced_button_layout.setObjectName("advanced_button_layout")
        self.button_import_file = QtGui.QPushButton(self.widget)
        self.button_import_file.setObjectName("button_import_file")
        self.advanced_button_layout.addWidget(self.button_import_file)
        self.button_key_value_editor = QtGui.QPushButton(self.widget)
        self.button_key_value_editor.setObjectName("button_key_value_editor")
        self.advanced_button_layout.addWidget(self.button_key_value_editor)
        self.button_plaintext_editor = QtGui.QPushButton(self.widget)
        self.button_plaintext_editor.setObjectName("button_plaintext_editor")
        self.advanced_button_layout.addWidget(self.button_plaintext_editor)
        self.button_plaintext_import_editor = QtGui.QPushButton(self.widget)
        self.button_plaintext_import_editor.setObjectName("button_plaintext_import_editor")
        self.advanced_button_layout.addWidget(self.button_plaintext_import_editor)
        self.button_reset = QtGui.QPushButton(self.widget)
        self.button_reset.setObjectName("button_reset")
        self.advanced_button_layout.addWidget(self.button_reset)
        spacerItem = QtGui.QSpacerItem(10, 10, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.advanced_button_layout.addItem(spacerItem)
        self.advanced_layout.addWidget(self.widget)
        self.editor_stack = QtGui.QStackedWidget(self.advanced)
        self.editor_stack.setObjectName("editor_stack")
        self.widget1 = QtGui.QWidget()
        self.widget1.setObjectName("widget1")
        self.vboxlayout = QtGui.QVBoxLayout(self.widget1)
        self.vboxlayout.setObjectName("vboxlayout")
        self.key_value_user_config_edit = key_value_editor(self.widget1)
        self.key_value_user_config_edit.setObjectName("key_value_user_config_edit")
        self.vboxlayout.addWidget(self.key_value_user_config_edit)
        self.editor_stack.addWidget(self.widget1)
        self.widget2 = QtGui.QWidget()
        self.widget2.setObjectName("widget2")
        self.vboxlayout1 = QtGui.QVBoxLayout(self.widget2)
        self.vboxlayout1.setObjectName("vboxlayout1")
        self.label = QtGui.QLabel(self.widget2)
        self.label.setOpenExternalLinks(True)
        self.label.setObjectName("label")
        self.vboxlayout1.addWidget(self.label)
        self.plain_user_config_edit = text_editor(self.widget2)
        self.plain_user_config_edit.setObjectName("plain_user_config_edit")
        self.vboxlayout1.addWidget(self.plain_user_config_edit)
        self.editor_stack.addWidget(self.widget2)
        self.widget3 = QtGui.QWidget()
        self.widget3.setObjectName("widget3")
        self.vboxlayout2 = QtGui.QVBoxLayout(self.widget3)
        self.vboxlayout2.setObjectName("vboxlayout2")
        self.label1 = QtGui.QLabel(self.widget3)
        self.label1.setObjectName("label1")
        self.vboxlayout2.addWidget(self.label1)
        self.plain_user_config_import_edit = text_editor(self.widget3)
        self.plain_user_config_import_edit.setObjectName("plain_user_config_import_edit")
        self.vboxlayout2.addWidget(self.plain_user_config_import_edit)
        self.editor_stack.addWidget(self.widget3)
        self.advanced_layout.addWidget(self.editor_stack)
        self.detail_stack.addWidget(self.advanced)
        self.editor_layout.addWidget(self.detail_stack)

        self.retranslateUi(openvpn_editor)
        self.detail_stack.setCurrentIndex(0)
        self.editor_stack.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(openvpn_editor)

    def retranslateUi(self, openvpn_editor):
        self.button_import_file.setText(QtGui.QApplication.translate("AccountTypeEditor", "Import File", None, QtGui.QApplication.UnicodeUTF8))
        self.button_key_value_editor.setText(QtGui.QApplication.translate("AccountTypeEditor", "List Editor", None, QtGui.QApplication.UnicodeUTF8))
        self.button_plaintext_editor.setText(QtGui.QApplication.translate("AccountTypeEditor", "Plaintext Editor", None, QtGui.QApplication.UnicodeUTF8))
        self.button_plaintext_import_editor.setText(QtGui.QApplication.translate("AccountTypeEditor", "Import Editor", None, QtGui.QApplication.UnicodeUTF8))
        self.button_reset.setText(QtGui.QApplication.translate("AccountTypeEditor", "Reset Configuration", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("AccountTypeEditor", "Edit the configuration and read the <a href=\"https://openvpn.net/man.html\">Manpage</a>", None, QtGui.QApplication.UnicodeUTF8))
        self.label1.setText(QtGui.QApplication.translate("AccountTypeEditor", "Edit the imported configuration that is used to highlight changes in the Advanced Editor.", None, QtGui.QApplication.UnicodeUTF8))

from netsplice.gui.widgets.text_editor import text_editor
from netsplice.gui.key_value_editor.key_value_editor import key_value_editor
