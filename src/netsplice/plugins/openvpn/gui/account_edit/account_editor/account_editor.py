# -*- coding: utf-8 -*-
# account_editor.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Account editor.

Extend Account Editor widget.
'''

from PySide.QtGui import QWidget, QMessageBox
from PySide.QtCore import Qt, Signal
from account_editor_ui import Ui_AccountEditor

from netsplice.plugins.openvpn.config import PLUGIN_NAME
from netsplice.plugins.openvpn.config import ACCOUNT_TYPE
from netsplice.plugins.openvpn.config.backend import DEFAULT_OPENVPN_VERSION
from netsplice.plugins.openvpn.gui.model.response.plugin_collection \
    import (
        plugin_collection as response_plugin_collection_model
    )
from netsplice.plugins.openvpn.gui.model.response.version_list \
    import (
        version_list as response_version_list_model
    )
from netsplice.plugins.openvpn.gui.model.response.version_detail \
    import (
        version_detail as response_version_detail_model
    )

from netsplice.util.model.field import field
from netsplice.util.model.errors import ValidationError
from netsplice.util.parser import factory as get_config_parser
from netsplice.util import get_logger, basestring, escape_html

logger = get_logger()

PLUGIN_COLLECTION = 'accounts'


class account_editor(QWidget):
    '''
    Account Editor.

    Custom Editor for Process Sniper options.
    '''

    backend_failed = Signal(basestring)
    config_changed = Signal()
    plugin_collection_loaded = Signal(basestring)
    plugin_collection_stored = Signal(basestring)
    executable_versions_loaded = Signal(basestring)
    version_details_loaded = Signal(basestring)
    account_profile_changed = Signal(object)

    def __init__(self, parent, dispatcher):
        '''
        Initialize Module.

        Initialize widget and abstract base, Setup UI.
        '''
        QWidget.__init__(self, parent)
        self.backend = dispatcher
        self.model = response_plugin_collection_model()
        self.editor_model = response_plugin_collection_model()
        self.ui = Ui_AccountEditor()
        self.ui.setupUi(self)
        self.ui.editor_layout.setContentsMargins(0, 0, 0, 0)
        self.setFocusPolicy(Qt.TabFocus)
        self.ui.option_list.set_header([
            self.tr('Name'),
            self.tr('Type'),
            self.tr('Value')
        ])
        self.ui.option_list.set_value_column(2)
        self.ui.option_list.disable_editor_column(0)
        self.ui.option_list.values_changed.connect(
            self._values_changed)
        self.ui.action_version_details.clicked.connect(
            self._show_version_details)
        self.ui.executable_version.currentIndexChanged.connect(
            self._executable_version_changed)

        parent.model_loaded_signal.connect(self._load_plugin_values)
        parent.model_reset_signal.connect(self._reset_plugin_values)
        parent.model_stored_signal.connect(self._store_plugin_values)
        self.plugin_collection_loaded.connect(self._plugin_collection_loaded)
        self.plugin_collection_stored.connect(self._plugin_collection_stored)
        self.executable_versions_loaded.connect(
            self._executable_versions_loaded)
        self.version_details_loaded.connect(self._version_details_loaded)
        self.backend.openvpn.get_version_list.emit(
            self.executable_versions_loaded, self.backend_failed)

    def get_values(self):
        '''
        Get Values.

        Return the values suitable for storage.
        '''
        new_values = self.ui.option_list.get_values()
        plugin_collection = response_plugin_collection_model()
        for value in new_values:
            attribute_name = value.key.get()
            if attribute_name == 'name':
                continue
            ui_value = value.get_value('value')
            if attribute_name == 'openvpn_version':
                ui_value = self.ui.executable_version.itemData(
                    self.ui.executable_version.currentIndex())
            attribute = plugin_collection.__dict__[value.key.get()]
            current_value = attribute.get()
            typed_value = None
            if isinstance(current_value, (basestring,)):
                typed_value = str(ui_value)
            elif isinstance(current_value, (int,)):
                typed_value = int(ui_value)
            if typed_value is None:
                raise ValidationError(
                    'Unsupported Type %s'
                    % (str(type(ui_value))))
            attribute.set(typed_value)
        plugin_collection.name.set(PLUGIN_NAME)

        return plugin_collection

    def _executable_version_changed(self):
        '''
        Executable Version Changed.

        User changed the executable version in the combobox.
        '''
        # sync value to key-value list
        values = self.get_values()
        self.editor_model.from_json(values.to_json())
        self._update_ui()
        self.config_changed.emit()

    def _executable_versions_loaded(self, model_json):
        '''
        Executable Versions loaded.
        '''
        version_list = response_version_list_model()
        version_list.from_json(model_json)
        self.ui.executable_version.currentIndexChanged.disconnect(
            self._executable_version_changed)
        for version_item in version_list.get_sorted_list():
            self.ui.executable_version.addItem(
                version_item.version.get(), version_item.version.get())
        # Sync combobox so the create dialog displays correct defaults. The
        # Value will be overridden in plugin_collection_loaded for edit.
        selected_index = self.ui.executable_version.findData(
            str(DEFAULT_OPENVPN_VERSION))
        if selected_index >= 0:
            self.ui.executable_version.setCurrentIndex(selected_index)
        self.ui.executable_version.currentIndexChanged.connect(
            self._executable_version_changed)

    def _load_plugin_values(self, account_instance):
        '''
        Load Plugin Values.

        The account model was loaded, request the plugin value.
        '''
        if account_instance is None:
            plugin_collection = response_plugin_collection_model()
            plugin_collection.name.set(PLUGIN_NAME)
            self.plugin_collection_loaded.emit(plugin_collection.to_json())
            return
        account_id = account_instance.id.get()
        if account_id is None:
            return
        self.backend.preferences_plugin_collection_instance_get.emit(
            PLUGIN_NAME, PLUGIN_COLLECTION, account_id,
            self.plugin_collection_loaded, self.backend_failed)

    def _plugin_collection_loaded(self, model_json):
        '''
        Plugin Collection Loaded.

        The request for the plugin values completed with the given model_json.
        '''
        self.editor_model = response_plugin_collection_model()
        self.model = response_plugin_collection_model()
        self.editor_model.from_json(model_json)
        self.model.from_json(model_json)
        self.editor_model.name.set(PLUGIN_NAME)
        self.model.name.set(PLUGIN_NAME)
        self._update_ui()

    def _update_ui(self):
        '''
        Update UI.

        Sync the editor.model values with the combobox and tableview.
        '''
        parser = get_config_parser('Line')
        for name in self.editor_model.__dict__.keys():
            if not isinstance(self.editor_model.__dict__[name], (field,)):
                continue
            if name == 'name':
                continue
            if name == 'openvpn_version':
                # sync plugin model to combobox
                self.ui.executable_version.currentIndexChanged.disconnect(
                    self._executable_version_changed)
                selected_index = self.ui.executable_version.findData(
                    str(self.editor_model.__dict__[name].get()))
                if selected_index == -1:
                    selected_index = self.ui.executable_version.findData(
                        str(DEFAULT_OPENVPN_VERSION))
                if selected_index == -1:
                    # cannot find the default version, select the first
                    selected_index = 0
                    logger.warn(
                        self.tr('No default version for openvpn executable'))
                self.ui.executable_version.setCurrentIndex(selected_index)
                self.ui.executable_version.currentIndexChanged.connect(
                    self._executable_version_changed)
                # keep the value in the key-value-list
            line = parser.current_element
            line.key.set(name)
            line.add_value(
                'value_type',
                str(type(self.editor_model.__dict__[name].get())))
            line.add_value(
                'value',
                str(self.editor_model.__dict__[name].get()))
            parser.commit()

        self.ui.option_list.model_changed.emit(
            parser.get_config())

    def _plugin_collection_stored(self):
        '''
        Plugin Collection Stored.

        The plugin values have been stored.
        '''
        values = self.get_values()
        self.model.from_json(values.to_json())

    def _reset_plugin_values(self):
        '''
        Reset Plugin Values.

        The account model was reset, commit the plugin value.
        '''
        parser = get_config_parser('Line')
        self.editor_model = response_plugin_collection_model()
        self.model = response_plugin_collection_model()

        self.ui.option_list.model_changed.emit(
            parser.get_config())
        self.ui.errors.setText('')
        self.ui.errors.hide()

    def _show_version_details(self):
        '''
        Show Version Details.

        Display information about the selected version.
        '''
        if self.ui.executable_version.currentIndex() == -1:
            return
        version = self.ui.executable_version.itemData(
            self.ui.executable_version.currentIndex())
        self.backend.openvpn.get_version_detail.emit(
            version, self.version_details_loaded, self.backend_failed)

    def _store_plugin_values(self, account_id):
        '''
        Store Plugin Values.

        The account model was stored, commit the plugin value. There is no
        way to interrupt the storage, all values have been validated before.
        '''
        try:
            values = self.get_values()
            self.backend.preferences_plugin_collection_instance_set.emit(
                PLUGIN_NAME, PLUGIN_COLLECTION, account_id, values,
                self.plugin_collection_stored, self.backend_failed)
        except ValidationError as errors:
            pass

    def _values_changed(self, new_values):
        '''
        Values Changed.

        Key Value table values have changed, validate the values and output
        errors.
        '''
        self.editor_model = response_plugin_collection_model()
        self.editor_model.name.set(PLUGIN_NAME)
        error_list = []
        for value in new_values:
            ui_value = value.get_value('value')
            try:
                attribute = self.editor_model.__dict__[value.key.get()]
                current_value = attribute.get()
                typed_value = None
                try:
                    if isinstance(current_value, (basestring,)):
                        typed_value = str(ui_value)
                    elif isinstance(current_value, (int,)):
                        typed_value = int(ui_value)
                except ValueError as errors:
                    raise ValidationError(
                        self.tr(r'Wrong Value: %s for %s').encode('utf-8')
                        % (str(type(ui_value)), value.key.get()))
                if typed_value is None:
                    raise ValidationError(
                        self.tr(r'Unsupported Type %s').encode('utf-8')
                        % (str(type(ui_value))))

                self.editor_model.__dict__[value.key.get()].set(
                    typed_value)
            except ValidationError as errors:
                error_list.append(
                    self.tr(r'Validation error: %s').encode('utf-8')
                    % (str(errors)))

        if len(error_list) is 0:
            # set the validated values, most notably, remove the items
            # that have the KEY_REMOVED_PREFIX
            self._update_ui()
            self.ui.errors.hide()
        else:
            error_text = ''
            for error in error_list:
                error_text += '%s<br/>' % (escape_html(error),)
            self.ui.errors.setText(error_text)
            self.ui.errors.show()
        self.config_changed.emit()

    def _version_details_loaded(self, model_json):
        '''
        Version Details Loaded.
        '''
        version_detail = response_version_detail_model()
        version_detail.from_json(model_json)
        QMessageBox.about(
            self, self.tr('Version Details'), version_detail.detail.get())

    def config_values_changed(self):
        '''
        Config Values Changed.

        Evaluate if the config is different from the loaded values.
        '''
        changed = False
        values = self.ui.option_list.get_values()
        for value in values:
            attribute_name = value.key.get()
            if attribute_name == 'name':
                continue
            ui_value = value.get_value('value')
            if not isinstance(self.model.__dict__[attribute_name], (field,)):
                continue
            model_value = str(self.model.__dict__[attribute_name].get())
            changed |= model_value != ui_value

        return changed

    def visible_for_account_type(self, account_type):
        '''
        Visible for account type.

        Return True when this widget should be visible for the given account
        type.
        '''
        return account_type == ACCOUNT_TYPE

    def reset(self):
        '''
        Reset.

        Reset the widget to initial values.
        '''
        self.ui.errors.setText('')
        self.ui.errors.hide()
