# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/plugins/openvpn/gui/account_edit/account_editor/views/account_editor.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_AccountEditor(object):
    def setupUi(self, openvpn):
        openvpn.setObjectName("openvpn")
        openvpn.setMinimumSize(QtCore.QSize(100, 200))
        self.editor_layout = QtGui.QVBoxLayout(openvpn)
        self.editor_layout.setObjectName("editor_layout")
        self.detail_stack = QtGui.QStackedWidget(openvpn)
        self.detail_stack.setObjectName("detail_stack")
        self.basic = QtGui.QWidget()
        self.basic.setObjectName("basic")
        self.basic_layout = QtGui.QGridLayout(self.basic)
        self.basic_layout.setObjectName("basic_layout")
        self.errors = QtGui.QLabel(self.basic)
        self.errors.setWordWrap(True)
        self.errors.setVisible(False)
        self.errors.setObjectName("errors")
        self.basic_layout.addWidget(self.errors, 0, 0, 1, 3)
        self.label_executable_version = QtGui.QLabel(self.basic)
        self.label_executable_version.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_executable_version.setObjectName("label_executable_version")
        self.basic_layout.addWidget(self.label_executable_version, 1, 0, 1, 1)
        self.executable_version = QtGui.QComboBox(self.basic)
        self.executable_version.setObjectName("executable_version")
        self.basic_layout.addWidget(self.executable_version, 1, 1, 1, 1)
        self.action_version_details = QtGui.QPushButton(self.basic)
        self.action_version_details.setObjectName("action_version_details")
        self.basic_layout.addWidget(self.action_version_details, 1, 2, 1, 1)
        self.option_list = key_value_editor(self.basic)
        self.option_list.setObjectName("option_list")
        self.basic_layout.addWidget(self.option_list, 2, 0, 1, 3)
        self.detail_stack.addWidget(self.basic)
        self.editor_layout.addWidget(self.detail_stack)

        self.retranslateUi(openvpn)
        self.detail_stack.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(openvpn)

    def retranslateUi(self, openvpn):
        self.label_executable_version.setText(QtGui.QApplication.translate("AccountEditor", "<b>OpenVPN Version", None, QtGui.QApplication.UnicodeUTF8))
        self.action_version_details.setText(QtGui.QApplication.translate("AccountEditor", "Info", None, QtGui.QApplication.UnicodeUTF8))

from netsplice.gui.key_value_editor.key_value_editor import key_value_editor
