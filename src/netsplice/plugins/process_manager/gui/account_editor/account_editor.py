# -*- coding: utf-8 -*-
# account_editor.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Account editor.

Extend Account Editor widget.
'''

from PySide.QtGui import QWidget
from PySide.QtCore import Qt, Signal
from .account_editor_ui import Ui_AccountEditor

from netsplice.plugins.process_manager.config import gui as config
from netsplice.plugins.process_launcher.gui.account_editor.\
    account_editor import (
        account_editor as process_launcher_account_editor_widget
    )
from netsplice.plugins.process_sniper.gui.account_editor.\
    account_editor import (
        account_editor as process_sniper_account_editor_widget
    )

from netsplice.util import get_logger

logger = get_logger()

# This magic number was created by approximating the area taken up by
# a single subwidget minus the labels of this widget.
MAGIC_TABLE_PROPORTION = 2.7


class account_editor(QWidget):
    '''
    Account Editor.

    Custom Editor for Process Launcher options.
    '''
    config_changed = Signal()
    account_profile_changed = Signal(object)

    def __init__(self, parent, dispatcher):
        '''
        Initialize Module.

        Initialize widget and abstract base, Setup UI.
        '''
        QWidget.__init__(self, parent)
        self.backend = dispatcher
        self.ui = Ui_AccountEditor()
        self.ui.setupUi(self)
        self.setFocusPolicy(Qt.TabFocus)
        self.ui.process_launcher_label.setText(
            self.tr(config.LAUNCHER_LABEL))
        self.ui.process_sniper_label.setText(
            self.tr(config.SNIPER_LABEL))
        self.ui.basic_layout.setContentsMargins(0, 15, 0, 0)
        self.ui.editor_layout.setContentsMargins(0, 0, 0, 0)
        self.ui.process_sniper_label.setContentsMargins(10, 0, 0, 0)
        self.ui.process_launcher_label.setContentsMargins(10, 0, 0, 0)
        self.ui.process_launcher.set_dispatcher(dispatcher)
        self.ui.process_sniper.set_dispatcher(dispatcher)
        self.ui.process_launcher.set_manager(self)
        self.ui.process_sniper.set_manager(self)
        self.ui.process_launcher.set_account_editor(parent)
        self.ui.process_sniper.set_account_editor(parent)
        self.account_profile_changed.connect(
            self._account_profile_changed)
        self.ui.process_launcher.config_changed.connect(
            self.config_changed)
        self.ui.process_sniper.config_changed.connect(
            self.config_changed)

    def _account_profile_changed(self, account_profile):
        '''
        Account Profile Changed.

        The account profile has changed in the general tab. Check if there
        are user launchers/snipers and initialize the profile values if
        confirmed.

        Arguments:
            account_profile (dict): new account profile.
        '''
        try:
            templates = account_profile['process_launcher']
            self.ui.process_launcher.remove_profile_items()
            for template in templates:
                self.ui.process_launcher.add_from_template(
                    template=template,
                    profile=True)
        except KeyError:
            pass
        try:
            templates = account_profile['process_sniper']
            self.ui.process_sniper.remove_profile_items()
            for template in templates:
                self.ui.process_sniper.add_from_template(
                    template=template,
                    profile=True)
        except KeyError:
            pass

    def config_values_changed(self):
        '''
        Config Values Changed.

        Evaluate if the config is different from the loaded values.
        '''
        changed = False
        changed |= self.ui.process_launcher.config_values_changed()
        changed |= self.ui.process_sniper.config_values_changed()

        return changed

    def resizeEvent(self, event):
        '''
        Resize Event.

        Overloaded widget resize event.
        This is used to avoid that qt is doing strange things with the maximum
        of the sub-widgets and thus preventing to grow the window and shrink
        it later to the default size.
        '''
        self.ui.editor_layout.invalidate()
        self.ui.process_launcher.setMaximumHeight(
            event.size().height() / MAGIC_TABLE_PROPORTION)
        self.ui.process_sniper.setMaximumHeight(
            event.size().height() / MAGIC_TABLE_PROPORTION)
