# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/var/tmp/compile_copy/src/netsplice/plugins/process_manager/gui/account_editor/views/account_editor.ui'
#
# Created:
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_AccountEditor(object):
    def setupUi(self, process_manager):
        process_manager.setObjectName("process_manager")
        process_manager.setMinimumSize(QtCore.QSize(100, 210))
        self.editor_layout = QtGui.QVBoxLayout(process_manager)
        self.editor_layout.setObjectName("editor_layout")
        self.detail_stack = QtGui.QStackedWidget(process_manager)
        self.detail_stack.setObjectName("detail_stack")
        self.basic = QtGui.QWidget()
        self.basic.setObjectName("basic")
        self.basic_layout = QtGui.QGridLayout(self.basic)
        self.basic_layout.setObjectName("basic_layout")
        self.process_launcher_label = QtGui.QLabel(self.basic)
        self.process_launcher_label.setWordWrap(True)
        self.process_launcher_label.setObjectName("process_launcher_label")
        self.basic_layout.addWidget(self.process_launcher_label, 0, 0, 1, 3)
        self.process_launcher = process_launcher_account_editor(self.basic)
        self.process_launcher.setObjectName("process_launcher")
        self.basic_layout.addWidget(self.process_launcher, 1, 0, 1, 3)
        spacerItem = QtGui.QSpacerItem(5, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self.basic_layout.addItem(spacerItem, 2, 0, 1, 3)
        self.horizontal_line = QtGui.QFrame(self.basic)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.horizontal_line.sizePolicy().hasHeightForWidth())
        self.horizontal_line.setSizePolicy(sizePolicy)
        self.horizontal_line.setFrameShape(QtGui.QFrame.HLine)
        self.horizontal_line.setFrameShadow(QtGui.QFrame.Raised)
        self.horizontal_line.setObjectName("horizontal_line")
        self.basic_layout.addWidget(self.horizontal_line, 3, 0, 1, 3)
        self.process_sniper_label = QtGui.QLabel(self.basic)
        self.process_sniper_label.setWordWrap(True)
        self.process_sniper_label.setObjectName("process_sniper_label")
        self.basic_layout.addWidget(self.process_sniper_label, 4, 0, 1, 3)
        self.process_sniper = process_sniper_account_editor(self.basic)
        self.process_sniper.setObjectName("process_sniper")
        self.basic_layout.addWidget(self.process_sniper, 5, 0, 1, 3)
        self.detail_stack.addWidget(self.basic)
        self.editor_layout.addWidget(self.detail_stack)

        self.retranslateUi(process_manager)
        self.detail_stack.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(process_manager)

    def retranslateUi(self, process_manager):
        pass

from netsplice.plugins.process_manager.gui.account_editor.process_launcher_account_editor import process_launcher_account_editor
from netsplice.plugins.process_manager.gui.account_editor.process_sniper_account_editor import process_sniper_account_editor
