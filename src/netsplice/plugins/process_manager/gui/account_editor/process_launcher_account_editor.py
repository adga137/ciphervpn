# -*- coding: utf-8 -*-
# process_launcher_account_editor.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Account editor for process launcher.

Wrapper for ui file
'''
from netsplice.plugins.process_launcher.gui.account_editor.\
    account_editor import account_editor


class process_launcher_account_editor(account_editor):
    '''
    Account Editor.

    Custom Editor for Process Launcher options.
    '''

    def __init__(self, parent):
        '''
        Initialize Module.

        Initialize widget.
        '''
        account_editor.__init__(self, parent)
        self.manager = None

    def set_manager(self, manager):
        '''
        Set Manager.

        Set the process_manager instance to the widget.
        '''
        self.manager = manager
