# -*- coding: utf-8 -*-
# gui.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Configuration for gui components.
'''

DESCRIPTION = '''
<p><b>OpenSSH (SSH client)</b> is a program for logging into a remote machine
 and for executing commands on a remote machine.  It is intended to provide
 secure encrypted communications between two untrusted hosts over an insecure
 network.  X11 connections, arbitrary TCP ports and UNIX-domain sockets
 can also be forwarded over the secure channel.</p>
<p>The plugin does support username/password or public-key with
 a ssh-askpass helper. The plugin does not support host key verification.</p>
<p>ssh executables are not shipped with the application. A ssh client
 executable needs to be available in your PATH.</p>
'''

# Default text when a new account is created
# - hint
# - easy to remove (Xorg: middle-mouse-copy)
# - no sensitive information
DEFAULT_ACCOUNT = '''
-D PORT
-C
-N
-l USERNAME
host REMOTE_HOST
'''

PREFERENCES_HELP = '''
<p>Global SSH options. The options are used as default for new accounts..</p>
'''

# Available account types. The name is used to select the editor with
# type_editor_factory.
ACCOUNT_LABEL = 'OpenSSH'
