# -*- coding: utf-8 -*-
# ssh.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

TYPE_ADDRESS = 'address'
TYPE_CIPHER_SPEC = 'cipher_spec'
TYPE_COMMENT = 'comment'
TYPE_FILENAME = 'filename'
TYPE_HOST_PORT = 'host_port'
TYPE_LOCAL_REMOTE_SOCKET = 'local_remote_socket'
TYPE_LOGIN_NAME = 'login_name'
TYPE_OPTION = 'option'
TYPE_PORT = 'port'
TYPE_PORT_OPT_ADDRESS = 'port_opt_address'
TYPE_QUERY_OPTION = 'query_option'
TYPE_STRING = 'string'
TYPE_TUN_OPT_TUN = 'tun_opt_tun'

LOG_ABORT = 'abort'
LOG_ERROR = 'error'
LOG_RESET_CREDENTIAL = 'reset_credential'
LOG_CHANGE_STATE = 'change_state'
LOG_WARNING = 'warning'
LOG_INFO = 'info'
LOG_DEBUG = 'debug'
LOG_PROGRESS = 'progress'
LOG_CONNECTING = 'connecting'
LOG_CONNECTED = 'connected'
LOG_DISCONNECTED = 'disconnected'

# LOG_ERROR: implicit LOG_PROGRESS, LOG_CHANGE_STATE:, LOG_DISCONNECTED
# Always add detailed messages before the general messages.

LOG_MESSAGES = {
    # Management Interface Messages
    ('usage: ssh'): {
        'actions': [LOG_ABORT],
        'help': 'Invalid configuration'
    },
    ('ssh: Could not resolve hostname'): {
        'actions': [LOG_ERROR],
        'help': (
            'SSH cannot resolve hostname.'
        )
    },
    ('ssh: connect to host'): {
        'actions': [LOG_ERROR],
        'help': (
            'SSH cannot connect to host.'
        )
    },
    ('Host key verification failed.'): {
        'actions': [LOG_ABORT],
        'help': (
            'SSH host identity not known, you have to connect in the'
            ' terminal once and confirm the identity.'
        )
    },
    ('Password:'): {
        'actions': [LOG_ABORT],
        'help': (
            'SSH keys not loaded, run Netsplice in context of a ssh-agent.'
        )
    },
    ('Enter passphrase for key'): {
        'actions': [LOG_ABORT],
        'help': (
            'SSH keys not loaded, run Netsplice in context of a ssh-agent and'
            ' load the key for the server with ssh-add.'
        )
    },
    ('Permission denied'): {
        'actions': [LOG_ABORT, LOG_RESET_CREDENTIAL],
        'help': (
            'Enter correct password for server.'
        )
    },
    ('Bad dynamic forwarding specification'): {
        'actions': [LOG_ABORT],
        'help': (
            'Review the configuration.'
        )
    },
    ('Privileged ports can only be forwarded by root'): {
        'actions': [LOG_ABORT],
        'help': (
            'Review the configuration.'
        )
    },
    ('Authenticated to'): {
        'actions': [LOG_INFO, LOG_CHANGE_STATE, LOG_CONNECTED],
        'help': (
            'The SSH connection is considered connected.'
        )
    },
    ('debug1: Connecting to '): {
        'actions': [LOG_CONNECTING],
        'help': ''
    },
    ('debug1: Exit status '): {
        'actions': [LOG_DISCONNECTED],
        'help': ''
    },
    ('debug1:'): {
        'actions': [LOG_INFO],
        'help': ''
    },
    ('ssh_exchange_identification: Connection closed by remote host'): {
        'actions': [LOG_ERROR],
        'help': ''
    }
}

KEYWORDS = {
    '#': {
        'help': (
            'Comment in the config. This option is ignored'
        ),
        'type': TYPE_COMMENT,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    ';': {
        'help': (
            'Comment in the config. This option is ignored'
        ),
        'type': TYPE_COMMENT,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'host': {
        'help': (
            'host to connect to. May also contain username'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    'command': {
        'help': (
            'Command to be executed on remote machine.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-1': {
        'help': (
            'Forces ssh to try protocol version 1 only.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-2': {
        'help': (
            'Forces ssh to try protocol version 2 only.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-4': {
        'help': (
            'Forces ssh to use IPv4 addresses only.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-6': {
        'help': (
            'Forces ssh to use IPv6 addresses only.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-A': {
        'help': (
            'Enables forwarding of the authentication agent connection.  This'
            ' can also be specified on a per-host basis in a configuration'
            ' file.'
            ' Agent forwarding should be enabled with caution.  Users with the'
            ' ability to bypass file permissions on the remote host (for the'
            ' agents UNIX-domain socket) can access the local agent through'
            ' the forwarded connection.  An attacker cannot obtain key'
            ' material from the agent, however they can perform operations on'
            ' the keys that enable them to authenticate using the identities'
            ' loaded into the agent.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-a': {
        'help': (
            'Disables forwarding of the authentication agent connection.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-b': {
        'help': (
            'b bind_address'
            ' Use bind_address on the local machine as the source address of'
            ' the connection.  Only useful on systems with more than one'
            ' address.'
        ),
        'type': TYPE_ADDRESS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-C': {
        'help': (
            'Requests compression of all data (including stdin, stdout,'
            ' stderr, and data for forwarded X11, TCP and UNIX-domain'
            ' connections). The compression algorithm is the same used by'
            ' gzip(1), and the level can be controlled by the CompressionLevel'
            ' option for protocol version 1. Compression is desirable on modem'
            ' lines and other slow connections, but will only slow down things'
            ' on fast networks. The default value can be set on a host-by-host'
            ' basis in the configuration files; see the Compression option.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-c': {
        'help': (
            'cipher_spec'
            ' Selects the cipher specification for encrypting the session.'
            ' Protocol version 1 allows specification of a single cipher.  The'
            ' supported values are "3des", "blowfish", and "des".  For'
            ' protocol version 2, cipher_spec is a comma-separated list of'
            ' ciphers listed in order of preference.  See the Ciphers keyword'
            ' in ssh_config(5) for more information.'
        ),
        'type': TYPE_CIPHER_SPEC,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-D': {
        'help': (
            '[bind_address:]port'
            ' Specifies a local "dynamic" application-level port forwarding.'
            ' This works by allocating a socket to listen to port on the local'
            ' side, optionally bound to the specified bind_address.  Whenever'
            ' a connection is made to this port, the connection is forwarded'
            ' over the secure channel, and the application protocol is then'
            ' used to determine where to connect to from the remote machine.'
            ' Currently the SOCKS4 and SOCKS5 protocols are supported, and ssh'
            ' will act as a SOCKS server.  Only root can forward privileged'
            ' ports. Dynamic port forwardings can also be specified in the'
            ' configuration file.'
            ' IPv6 addresses can be specified by enclosing the address in'
            ' square brackets.  Only the superuser can forward privileged'
            ' ports.  By default, the local port is bound in accordance with'
            ' the GatewayPorts setting.  However, an explicit bind_address may'
            ' be used to bind the connection to a specific address.  The'
            ' bind_address of "localhost" indicates that the listening port be'
            ' bound for local use only, while an empty address or "*"'
            ' indicates that the port should be available from all interfaces.'
        ),
        'type': TYPE_PORT_OPT_ADDRESS,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-J': {
        'help': (
            'Connect to the target host by first making a ssh connection to'
            ' the jump host and then establishing a TCP forwarding to the'
            ' ultimate destination from there.  Multiple jump hops may be'
            ' specified separated by comma characters.  This is a shortcut to'
            ' specify a ProxyJump configuration directive'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-L': {
        'help': (
            'local_socket:remote_socket'
            ' Specifies that connections to the given TCP port or Unix socket'
            ' on the local (client) host are to be forwarded to the given host'
            ' and port, or Unix socket, on the remote side.  This works by'
            ' allocating a socket to listen to either a TCP port on the local'
            ' side, optionally bound to the specified bind_address, or to a'
            ' Unix socket.  Whenever a connection is made to the local port or'
            ' socket, the connection is forwarded over the secure channel, and'
            ' a connection is made to either host port hostport, or the Unix'
            ' socket remote_socket, from the remote machine.'
            ' Port forwardings can also be specified in the configuration'
            ' file. Only the superuser can forward privileged ports.  IPv6'
            ' addresses can be specified by enclosing the address in square'
            ' brackets. By default, the local port is bound in accordance with'
            ' the GatewayPorts setting.  However, an explicit bind_address may'
            ' be used to bind the connection to a specific address.  The'
            ' bind_address of "localhost" indicates that the listening port be'
            ' bound for local use only, while an empty address or "*'
        ),
        'type': TYPE_LOCAL_REMOTE_SOCKET,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-l': {
        'help': (
            'login_name'
            ' Specifies the user to log in as on the remote machine. This also'
            ' may be specified on a per-host basis in the configuration file.'
        ),
        'type': TYPE_LOGIN_NAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-N': {
        'help': (
            'Do not execute a remote command.  This is useful for just'
            ' forwarding ports.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-p': {
        'help': (
            'port'
            ' Port to connect to on the remote host.  This can be specified on'
            ' a per-host basis in the configuration file.'
        ),
        'type': TYPE_PORT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-Q': {
        'help': (
            'query_option'
            ' Queries ssh for the algorithms supported for the specified ver-'
            ' sion 2.  The available features are: cipher (supported symmetric'
            ' ciphers), cipher-auth (supported symmetric ciphers that support'
            ' authenticated encryption), mac (supported message integrity'
            ' codes), kex (key exchange algorithms), key (key types), key-cert'
            ' (certificate key types), key-plain (non-certificate key types),'
            ' and protocol-version (supported SSH protocol versions).'
        ),
        'type': TYPE_QUERY_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': True,
    },
    '-q': {
        'help': (
            'Quiet mode.  Causes most warning and diagnostic messages to be'
            ' suppressed.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': True,
    },
    '-R': {
        'help': (
            '[bind_address:]port:host:hostport'
            ' [bind_address:]port:local_socket'
            ' remote_socket:host:hostport'
            ' remote_socket:local_socket'
            ' Specifies that connections to the given TCP port or Unix socket'
            ' on the remote (server) host are to be forwarded to the given'
            ' host and port, or Unix socket, on the local side.  This works by'
            ' allocating a socket to listen to either a TCP port or to a Unix'
            ' socket on the remote side. Whenever a connection is made to this'
            ' port or Unix socket, the connection is forwarded over the secure'
            ' channel, and a connection is made to either host port hostport,'
            ' or local_socket, from the local machine.'
            ' Port forwardings can also be specified in the configuration'
            ' file. Privileged ports can be forwarded only when logging in as'
            ' root on the remote machine.  IPv6 addresses can be specified by'
            ' enclosing the address in square brackets.'
            ' By default, TCP listening sockets on the server will be bound to'
            ' the loopback interface only.  This may be overridden by specify-'
            ' ing a bind_address.  An empty bind_address, or the address "*",'
            ' indicates that the remote socket should listen on all'
            ' interfaces. Specifying a remote bind_address will only succeed'
            ' if the servers GatewayPorts option is enabled'
            ' (see sshd_config(5)).'
            ' If the port argument is "0", the listen port will be dynamically'
            ' allocated on the server and reported to the client at run time.'
            ' When used together with -O forward the allocated port will be'
            ' printed to the standard output.'
        ),
        'type': TYPE_STRING,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-S': {
        'help': (
            'ctl_path'
            ' Specifies the location of a control socket for connection shar-'
            ' ing, or the string "none" to disable connection sharing.  Refer'
            ' to the description of ControlPath and ControlMaster in'
            ' ssh_config(5) for details.'
        ),
        'type': TYPE_FILENAME,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-s': {
        'help': (
            'May be used to request invocation of a subsystem on the remote'
            ' system.  Subsystems facilitate the use of SSH as a secure trans-'
            ' port for other applications (e.g. sftp(1)).  The subsystem is'
            ' specified as the remote command.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-T': {
        'help': (
            'Disable pseudo-terminal allocation.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-t': {
        'help': (
            'Force pseudo-terminal allocation.  This can be used to execute'
            ' arbitrary screen-based programs on a remote machine, which can'
            ' be very useful, e.g. when implementing menu services.  Multiple'
            ' -t options force tty allocation, even if ssh has no local tty.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-V': {
        'help': (
            'Display the version number and exit.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },

    '-v': {
        'help': (
            'Verbose mode.  Causes ssh to print debugging messages about its'
            ' progress.  This is helpful in debugging connection, authentica-'
            ' tion, and configuration problems.  Multiple -v options increase'
            ' the verbosity.  The maximum is 3.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    '-W': {
        'help': (
            'host:port'
            ' Requests that standard input and output on the client be for-'
            ' warded to host on port over the secure channel.  Implies -N, -T,'
            ' ExitOnForwardFailure and ClearAllForwardings, though these can'
            ' be overridden in the configuration file or using -o command line'
            ' options.'
        ),
        'type': TYPE_HOST_PORT,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-w': {
        'help': (
            'local_tun[:remote_tun]'
            ' Requests tunnel device forwarding with the specified tun(4)'
            ' devices between the client (local_tun) and the server'
            ' (remote_tun).'
            ' The devices may be specified by numerical ID or the keyword'
            ' "any", which uses the next available tunnel device.  If'
            ' remote_tun is not specified, it defaults to "any".  See also the'
            ' Tunnel and TunnelDevice directives in ssh_config(5).  If the'
            ' Tunnel directive is unset, it is set to the default tunnel mode,'
            ' which is "point-to-point".'
        ),
        'type': TYPE_TUN_OPT_TUN,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-X': {
        'help': (
            'Enables X11 forwarding.  This can also be specified on a per-host'
            ' basis in a configuration file.'
            ' X11 forwarding should be enabled with caution.  Users with the'
            ' ability to bypass file permissions on the remote host (for the'
            ' users X authorization database) can access the local X11 display'
            ' through the forwarded connection.  An attacker may then be able'
            ' to perform activities such as keystroke monitoring.'
            ' For this reason, X11 forwarding is subjected to X11 SECURITY'
            ' extension restrictions by default.  Please refer to the ssh -Y'
            ' option and the ForwardX11Trusted directive in ssh_config(5) for'
            ' more information.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-x': {
        'help': (
            'Disables X11 forwarding.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-Y': {
        'help': (
            'Enables trusted X11 forwarding.  Trusted X11 forwardings are not'
            ' subjected to the X11 SECURITY extension controls.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-y': {
        'help': (
            'Send log information using the'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    },
    '-z': {
        'help': (
            'Use the SCTP protocol for connection instead of TCP which is the'
            ' default.  syslog(3) system module.  By default this information'
            ' is sent to stderr.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': False,
        'implicit': False,
        'blocked': False,
    }
}
