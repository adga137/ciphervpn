# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
from netsplice.plugins.ssh.config import events
from netsplice.backend.event import (
    register_destination as register_event_destination
)
from netsplice.backend.event import (
    register_origin as register_event_origin
)
from netsplice.backend.unprivileged.event_controller import (
    event_controller as backend_unprivileged_event_controller
)
from netsplice.plugins.ssh.config import PLUGIN_NAME
from .connection import register as register_connection
from .preferences import register as register_preferences
from .unprivileged_dispatcher import unprivileged_dispatcher
from .event_plugin import event_plugin


def register(app):
    '''
    Register.

    Register the components of subapp with the given app instance.
    '''
    register_connection(app)

    app.add_dispatcher_plugin(
        app.get_unprivileged(), PLUGIN_NAME, unprivileged_dispatcher)
    app.add_event_plugin(PLUGIN_NAME, event_plugin)
    register_event_destination(event_plugin, events.INCOMMING)

    register_event_origin(
        backend_unprivileged_event_controller, events.INCOMMING_UNPRIVILEGED)

    register_preferences(app)
