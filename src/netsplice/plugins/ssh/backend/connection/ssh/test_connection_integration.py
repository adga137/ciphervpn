# -*- coding: utf-8 -*-
# test_connection_integration.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Connection.

Checks SSH log messages and their state changes.
'''

from tornado.testing import AsyncTestCase
import sys
import netsplice.backend.connection as connection_module
import netsplice.backend.event as event_module
import netsplice.backend.gui as gui_module
import netsplice.backend.log as log_module
import netsplice.backend.preferences as preferences_module

from .connection import connection

from netsplice.backend.gui.model.connection_list_item import (
    connection_list_item as connection_item_model
)
from netsplice.backend.event_loop import event_loop
from netsplice.backend.gui.model import model as module_model
from netsplice.config import connection as config_connection
from netsplice.config import plugins as config_plugins
from netsplice.model.log_item import log_item as log_item_model
from netsplice.plugins import get_plugins
from netsplice.util.ipc.application import application as application


class mock_unprivileged_ssh(object):
    def __init__(self):
        self.application = None

    def reset(self):
        pass


class mock_unprivileged(object):
    def __init__(self):
        self.application = None
        self.reset()
        self.ssh = mock_unprivileged_ssh()
        self.server_role = ''

    def reset(self):
        pass

this = sys.modules[__name__]
this.test_application = None


def get_test_application():
    if this.test_application is not None:
        return this.test_application
    app = application()
    app.add_module(connection_module)
    app.add_module(log_module)
    app.add_module(event_module)
    app.add_module(preferences_module)
    app.add_module(gui_module)
    app.set_unprivileged(mock_unprivileged())
    connection_module.register_module_events(event_module)
    orig_active_plugins = config_plugins.ACTIVE_PLUGINS
    config_plugins.ACTIVE_PLUGINS = ['netsplice.plugins.ssh']
    for plugin in get_plugins():
        app.add_plugin(plugin)
    app.set_event_loop(event_loop())
    app.register_plugin_components('util')
    app.register_plugin_components('backend')
    config_plugins.ACTIVE_PLUGINS = orig_active_plugins
    # restore what plugin overrides
    this.test_application = app
    return this.test_application


def get_test_object(mock_overrides=[]):
    app = get_test_application()
    c = connection(app, module_model())
    c.model = connection_item_model()
    return c


class ConnectionIntegrationTests(AsyncTestCase):
    '''
    Connection Integration Tests.

    Checks real ssh messages and their state changes.
    '''
    def test_update_connection_000(self):
        '''
        TEMPLATE, Please write a short description of the integration,
        configure the before-state and set the assertions.
        '''
        c = get_test_object()
        lm = log_item_model()
        c.connection.state = config_connection.CONNECTING
        lm.message.set('TEMPLATE_TEST_TEMPLATE')
        lm.level.set('info')
        state_changed = c.check_log_item(lm)
        assert(state_changed is False)
        assert(lm.level.get() == 'info')
        assert(c.connection.state == config_connection.CONNECTING)
