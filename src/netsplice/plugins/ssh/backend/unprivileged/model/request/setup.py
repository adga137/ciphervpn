# -*- coding: utf-8 -*-
# setup_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for setting up connections.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.plugins.ssh.backend.unprivileged.model.validator.\
    ssh_configuration import (
        ssh_configuration as ssh_configuration_validator
    )
from netsplice.plugins.ssh.backend.unprivileged.model.validator.\
    ssh_version import (
        ssh_version as ssh_version_validator
    )
from netsplice.plugins.ssh.backend.unprivileged.model.validator.\
    username_value import (
        username_value as username_value_validator
    )
from netsplice.plugins.ssh.backend.unprivileged.model.validator.\
    password_value import (
        password_value as password_value_validator
    )


class setup(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.configuration = field(
            default='',
            required=True,
            validators=[ssh_configuration_validator()])

        self.username = field(
            required=True,
            validators=[username_value_validator()])

        self.password = field(
            required=True,
            validators=[password_value_validator()])

        self.version = field(
            required=True,
            validators=[ssh_version_validator()])
