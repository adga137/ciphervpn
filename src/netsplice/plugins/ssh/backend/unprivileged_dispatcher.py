# -*- coding: utf-8 -*-
# unprivileged_dispatcher.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Dispatcher for unprivileged actions.
'''

from socket import error as socket_error

from tornado import gen, httpclient

from . import dispatcher_endpoints as endpoints
from netsplice.backend.unprivileged.model.request.connection_id import (
    connection_id as connection_id_model
)
from netsplice.backend.unprivileged.model.response.connection_list import (
    connection_list as connection_list_model
)
from netsplice.backend.unprivileged.model.response.connection_status import (
    connection_status as connection_status_model
)
from netsplice.plugins.ssh.backend.unprivileged.model.request.setup import (
    setup as setup_model
)
from netsplice.model.process import (
    process as process_model
)
from netsplice.util import get_logger
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.ipc.service import service
from netsplice.util.path import get_path_prefix
from netsplice.util.process.dispatcher import dispatcher as process_dispatcher
from netsplice.util.process.errors import ProcessError
from netsplice.util.model.errors import ValidationError

logger = get_logger()


class unprivileged_dispatcher(object):

    def __init__(self, service_instance):
        self.service = service_instance

    @gen.coroutine
    def connect(self, connection_id):
        options = {
            'connection_id': connection_id
        }
        try:
            response = yield self.service.post(
                endpoints.SSH_CONN_CONNECT % options,
                '{}'
            )

            response_model = connection_id_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2166, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2167, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2168, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2169, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def connection_status(self, connection_id):
        options = {
            'connection_id': connection_id
        }

        response_model = connection_status_model()
        try:
            response = yield self.service.get(
                endpoints.SSH_CONN_STATUS % options,
                None
            )
            response_model.from_json(response.body)
            model = self.service.application.get_module('unprivileged').model
            model.set_connection_status(
                'ssh', response_model.id.get(), response_model)
            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2170, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2171, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2172, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2173, str(errors))
            response_model.id.set(connection_id)
            response_model.active.set(False)
            raise gen.Return(response_model)

    @gen.coroutine
    def disconnect(self, connection_id):
        options = {
            'connection_id': connection_id
        }
        try:
            response = yield self.service.post(
                endpoints.SSH_CONN_DISCONNECT % options,
                '{}'
            )

            response_model = connection_id_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2174, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2175, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2176, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2177, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def remove(self, connection_id):
        options = {
            'connection_id': connection_id
        }
        try:
            response = yield self.service.delete(
                endpoints.SSH_CONN_REMOVE % options
            )

            response_model = connection_id_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2178, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2179, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2180, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2181, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def list_connections(self):
        response = yield self.service.get(
            endpoints.SSH_CONN_LIST,
            None
        )
        try:
            response_model = connection_list_model()
            response_model.from_json(response.body)

            model = self.service.application.get_module('unprivileged').model
            model.set_connections('ssh', response_model)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2182, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2183, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2184, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2185, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def reconnect(self, connection_id):
        options = {
            'connection_id': connection_id
        }
        try:
            response = yield self.service.post(
                endpoints.SSH_CONN_RECONNECT % options,
                '{}'
            )

            response_model = connection_id_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2186, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2187, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2188, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2189, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def setup_connection(
            self, configuration, username, password, ssh_version):
        request_model = setup_model()
        try:
            request_model.configuration.set(configuration)
            request_model.username.set(username)
            request_model.password.set(password)
            request_model.version.set(ssh_version)

            response = yield self.service.put(
                endpoints.SSH_CONN_SETUP,
                request_model.to_json()
            )
            response_model = connection_id_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2190, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2191, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2192, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2193, str(errors))
            raise ServerConnectionError(str(errors))
