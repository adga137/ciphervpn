# -*- coding: utf-8 -*-
# connection_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for a single connection with all the state. Internal to the privileged
application.
'''
from netsplice.util import get_logger
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.validator.boolean import (
    boolean as boolean_validator
)
from .validator.connection_id import (
    connection_id as connection_id_validator
)
from .validator.connection_type import (
    connection_type as connection_type_validator
)
from .validator.ssh_configuration \
    import (
        ssh_configuration as ssh_configuration_validator
    )
from .validator.ssh_version import (
    ssh_version as ssh_version_validator
)
from .validator.username_value import (
    username_value as username_value_validator
)
from .validator.password_value import (
    password_value as password_value_validator
)
from .message_list import (
    message_list as message_list_model
)

logger = get_logger()


class connection(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.process = None

        self.id = field(
            required=True,
            validators=[connection_id_validator()])

        self.active = field(
            default=False,
            required=True,
            validators=[boolean_validator()])

        self.up = field(
            default=False,
            required=True,
            validators=[boolean_validator()])

        self.type = field(
            required=True,
            validators=[connection_type_validator()])

        self.configuration = field(
            default='',
            required=True,
            validators=[ssh_configuration_validator()])

        self.username = field(
            required=True,
            validators=[username_value_validator()])

        self.password = field(
            required=True,
            validators=[password_value_validator()])

        self.version = field(
            required=True,
            validators=[ssh_version_validator()])

        self.state = field(
            default='initialized',
            required=True)

        self.ip = field(
            default='0.0.0.0',
            required=True)

        self.peer_ip = field(
            default='0.0.0.0',
            required=True)

        self.management_port = field(
            required=False)

        self.recv_message_list = message_list_model()
        self.send_message_list = message_list_model()

        self.read_tap_bytes = field(required=False)
        self.write_tap_bytes = field(required=False)
        self.read_udp_bytes = field(required=False)
        self.write_udp_bytes = field(required=False)
        self.auth_read_bytes = field(required=False)
        self.pre_compress_bytes = field(required=False)
        self.post_compress_bytes = field(required=False)
        self.pre_decompress_bytes = field(required=False)
        self.post_decompress_bytes = field(required=False)

    def disconnect(self):
        self.active.set(False)
        self.up.set(False)
        self.management_port.set(0)
        self.commit()
