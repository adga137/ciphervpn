# -*- coding: utf-8 -*-
# connection_list_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for connections used internally.
'''

from netsplice.util.model.marshalable_list import marshalable_list
from .connection import (
    connection as connection_model
)
from netsplice.util.errors import (
    NotFoundError, ActiveError
)


class connection_list(marshalable_list):
    '''
    Marshalable model that contains multiple items of
    connection_model.
    '''
    def __init__(self):
        marshalable_list.__init__(self, connection_model)

    def delete_connection(self, connection_id):
        '''
        Delete the connection with the given id from the connection_list.
        '''
        found = False
        for index, connection in enumerate(self):
            if connection.id.get() == connection_id:
                found = True
                if connection.active.get() is True:
                    raise ActiveError()
                if connection.process is not None:
                    connection.process.delete()
                del self[index]
                break
        if not found:
            raise NotFoundError(connection_id)
        self.commit()

    def get_connected_management_ports(self):
        '''
        Get a list of currently connected management ports.
        '''
        active_ports = []
        for connection in self:
            if connection.active.get() is True:
                active_ports.append(connection.management_port.get())
        return active_ports

    def find_by_id(self, connection_id):
        '''
        Return the connection with the given id or raise an Exception.
        '''
        for connection in self:
            if connection.id.get() == connection_id:
                return connection
        raise NotFoundError(connection_id)
