# -*- coding: utf-8 -*-
# ssh_version.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Validator for ssh config files
'''
import re

from netsplice.util.model.validator import validator


class ssh_version(validator):
    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        '''
        Ssh Versions may be 'ssh-7.3p1-hpn14v11' or
        None for using the default / system ssh installation.
        '''
        if value is None:
            return True
        release_version_regex = re.compile(
            r'ssh-[0-9]*\.[0-9]\.[0-9]*')
        if release_version_regex.search(value):
            return True
        git_version_regex = re.compile(
            r'ssh-[0-9]*\.[0-9]\.[0-9]*-[a-f0-9]*')
        if git_version_regex.search(value):
            return True
        return True
