# -*- coding: utf-8 -*-
# connection_id_validator.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Validator for privileged connection id's
'''

from netsplice.util import get_logger
from netsplice.util.model.validator import validator
from netsplice.util import basestring

logger = get_logger()


class connection_id(validator):
    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        '''
        Id's are uuid4's (36 bytes) never None
        '''
        if value is None:
            logger.error('Value is None but should not.')
            return False
        if not isinstance(value, (basestring)):
            logger.error('Value is not a string')
            return False
        if len(value) != 36:
            logger.error('Value is not a uuid string with 36 bytes')
            return False
        if value[8] != '-':
            logger.error('Value does not contain a dash on byte 8')
            return False
        if value[13] != '-':
            logger.error('Value does not contain a dash on byte 13')
            return False
        if value[18] != '-':
            logger.error('Value does not contain a dash on byte 18')
            return False
        if value[23] != '-':
            logger.error('Value does not contain a dash on byte 23')
            return False
        return True
