# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

# Verbosity for Management Connections. Useful during development. The messages
# are passed to the Backend in production anyway.
# 0: No verbosity
# 3: Display messages sent and received to the management_process
VERBOSE = 0

# Buffer Size for receiving management messages
# Small buffer will cause the messages to be split into multiple
# recv calls and require the process_message to combine them correctly
# 1024: Default
# 16: Debug the process_message code
MAX_RECV = 1024

# Path to ssh binary
# Linux: /usr/bin/ssh
# OSX/Windows: CWD/ssh/ssh
# This config may be overridden on application-startup
SSH_BINARY_PATH = '/usr/bin/ssh'

# Relative Path, will be used when it is available relative to the
# executable loading this module
SSH_BINARY_PKG_PATH = 'ssh/ssh'

# Relative Path, will be used when it is available relative to the
# executable loading this module
SSH_BINARY_EXE_PATH = 'ssh/ssh.exe'

# Parameter to ssh
# Cache the most recent n lines of log file history for usage by the management
# channel.
# 0: Default of ssh may fill up the clients memory
# 100: should be enough for production.
MAX_MANAGEMENT_LOG_CACHE = 100

# Number of manageable connections. Should not be limited but based on the OS
# there are limits on the number of TCP-connections and or TUN devices.
MAX_PRIVILEGED_CONNECTIONS = 16

# Frequency of ssh status polling in seconds.
SSH_STATUS_SLEEP = 0.5

# Frequency of syncing process model (stderr/stdout) with the actual model
SSH_MODEL_SYNC = 0.1
