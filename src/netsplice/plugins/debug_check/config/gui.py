# -*- coding: utf-8 -*-
# gui.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Config for About
'''

DESCRIPTION = '''
<p><b>Check</b> is simple plugin that adds a menu entry that links to the
 <a href="https://check.ipredator.se">https://check.ipredator.se</a> website.
<p>With this check it is possible to evaluate the state of a VPN connection
 and get hints about possible DNS or IPv6 leaks</p>
'''
