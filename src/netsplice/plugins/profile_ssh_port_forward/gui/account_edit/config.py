# -*- coding: utf-8 -*-
# config.py
# Copyright (C) 2013 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Config for a local port forward.

'''
ACCOUNT_LABEL = 'Port Forward'

DESCRIPTION = '''
<p><b>Port forward</b> Pass requests from a local port to a remote system.</p>

<p>Specifies that connections to the given TCP port on the local (client) host
are to be forwarded to the given host and port on the remote side. This works
by allocating a socket to listen to a TCP port on the local side, optionally
bound to the specified bind_address. Whenever a connection is made to the local
port, the connection is forwarded over the secure channel, and a connection is
made to <i>host</i> port <i>hostport</i> from the remote machine. IPv6
addresses can be specified by enclosing the address in square brackets.</p>

<p>Point your browser to localhost:PORT to access HOST:HOSTPORT through
REMOTE_HOST<p>
'''

DEFAULT_CONFIG = '''
-L localhost:PORT:HOST:HOSTPORT
-C
-N
-T
-l USERNAME
host REMOTE_HOST
'''
