# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

SIGNAL_AVAILABLE = [
    'SIGHUP',
    'SIGINT',
    'SIGKILL',
    'SIGTERM',
    'SIGSTOP',
    'SIGCONT',
    'SIGUSR1',
    'SIGUSR2'
]

SIGNAL_NAMES = [
    'SIGHUP',
    'SIGINT',
    'SIGQUIT',
    'SIGILL',
    'SIGTRAP',
    'SIGABRT',
    'SIGBUS',
    'SIGFPE',
    'SIGKILL',
    'SIGUSR1',
    'SIGSEGV',
    'SIGUSR2',
    'SIGPIPE',
    'SIGALRM',
    'SIGTERM',
    'SIGSTKFLT',
    'SIGCHLD',
    'SIGCONT',
    'SIGSTOP',
    'SIGTSTP',
    'SIGTTIN',
    'SIGTTOU',
    'SIGURG',
    'SIGXCPU',
    'SIGXFSZ',
    'SIGVTALRM',
    'SIGPROF',
    'SIGWINCH',
    'SIGIO',
    'SIGPWR',
    'SIGSYS',
    'SIGRTMIN',
    'SIGRTMIN+1',
    'SIGRTMIN+2',
    'SIGRTMIN+3',
    'SIGRTMIN+4',
    'SIGRTMIN+5',
    'SIGRTMIN+6',
    'SIGRTMIN+7',
    'SIGRTMIN+8',
    'SIGRTMIN+9',
    'SIGRTMIN+10',
    'SIGRTMIN+11',
    'SIGRTMIN+12',
    'SIGRTMIN+13',
    'SIGRTMIN+14',
    'SIGRTMIN+15',
    'SIGRTMAX-14',
    'SIGRTMAX-13',
    'SIGRTMAX-12',
    'SIGRTMAX-11',
    'SIGRTMAX-10',
    'SIGRTMAX-9',
    'SIGRTMAX-8',
    'SIGRTMAX-7',
    'SIGRTMAX-6',
    'SIGRTMAX-5',
    'SIGRTMAX-4',
    'SIGRTMAX-3',
    'SIGRTMAX-2',
    'SIGRTMAX-1',
    'SIGRTMAX'
]
