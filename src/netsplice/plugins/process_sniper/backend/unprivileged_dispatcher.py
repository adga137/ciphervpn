# -*- coding: utf-8 -*-
# unprivileged_dispatcher.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Dispatcher for unprivileged actions.
'''

from socket import error as socket_error

from tornado import gen, httpclient

from . import dispatcher_endpoints as endpoints
from netsplice.plugins.process_sniper.unprivileged.process_sniper.model.\
    request.kill_process import (
        kill_process as kill_process_model
    )
from netsplice.plugins.process_sniper.unprivileged.process_sniper.model.\
    response.process_list import (
        process_list as process_list_model
    )
from netsplice.util import get_logger
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.model.errors import ValidationError

logger = get_logger()


class unprivileged_dispatcher(object):

    def __init__(self, service_instance):
        self.service = service_instance

    @gen.coroutine
    def kill_process(self, signal_name, pid):
        '''
        Kill Proccess.

        Kill the process with the given signal.
        '''
        options = {
            'signal': signal_name,
            'pid': pid
        }
        try:
            request_model = kill_process_model()
            request_model.pid.set(pid)
            request_model.signal.set(signal_name)

            response = yield self.service.post(
                endpoints.PROCESS_SNIPER_KILL_PROCESS % options,
                request_model.to_json()
            )

            raise gen.Return('')
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2229, str(errors))
            logger.error(str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2230, str(errors))
            logger.error(str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2231, str(errors))
            logger.error(str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2232, str(errors))
            logger.error(str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def get_process_list(self):
        '''
        Kill Proccess.

        Kill the process with the given signal.
        '''
        try:
            response = yield self.service.get(
                endpoints.PROCESS_SNIPER_PROCESS_LIST, None)

            response_model = process_list_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2233, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2234, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2235, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2236, str(errors))
            raise ServerConnectionError(str(errors))
