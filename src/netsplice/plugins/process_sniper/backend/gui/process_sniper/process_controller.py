# -*- coding: utf-8 -*-
# process_controller.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from tornado import gen

from netsplice.util.errors import (
    NotFoundError
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.util import get_logger
from .model.response.process_list import (
    process_list as process_list_model
)
from .model.request.kill_process import (
    kill_process as kill_process_model
)
from .process import process as process_dispatcher

logger = get_logger()


class process_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    @gen.coroutine
    def get(self):
        '''
        The "get" action requests the application to list all active processes
        and return the list.
        '''
        try:
            process = process_dispatcher(self.application)
            response_model = process_list_model()
            process_list = yield process.list()
            response_model.from_json(process_list.to_json())
            self.write(response_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            logger.error(str(errors))
            self.set_status(400)
        except Exception as errors:
            logger.error(str(errors))
            self.set_status(400)
        self.finish()
