# -*- coding: utf-8 -*-
# kill_process.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
from tornado import gen

from netsplice.plugins.process_sniper.model.process_list import (
    process_list as process_list_model
)


class process(object):
    '''
    '''
    def __init__(self, application):
        '''
        '''
        self.application = application

    @gen.coroutine
    def list(self):
        '''
        List.

        List processes as a process_list model.
        '''
        process_sniper = self.application.get_unprivileged().process_sniper
        processes = yield process_sniper.get_process_list()
        raise gen.Return(processes)
