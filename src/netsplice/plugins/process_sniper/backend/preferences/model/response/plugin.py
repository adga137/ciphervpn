# -*- coding: utf-8 -*-
# plugin.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Preferences. Defines how the user input is persisted
'''

from netsplice.model.plugin_item import (
    plugin_item as plugin_item_model
)
from netsplice.util.model.field import field

from .signals import signals

from netsplice.plugins.process_sniper.config import backend as config
from netsplice.plugins.process_sniper.model.validator.signal import (
    signal as signal_validator
)


class plugin(plugin_item_model):
    def __init__(self):
        plugin_item_model.__init__(self, None)

        self.default_signal = field(
            required=False,
            default=config.DEFAULT_SIGNAL,
            validators=[signal_validator()])

        self.extra_signals = signals()
