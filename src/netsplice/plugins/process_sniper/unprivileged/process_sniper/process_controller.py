# -*- coding: utf-8 -*-
# process_controller.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.errors import (
    NotFoundError
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.util import get_logger
from .model.response.process_list import (
    process_list as process_list_model
)
from .model.request.kill_process import (
    kill_process as kill_process_model
)
from .process import process as process_dispatcher

logger = get_logger()


class process_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def get(self):
        '''
        The "get" action requests the application to list all active processes
        and return the list.
        '''
        try:
            process = process_dispatcher()
            response_model = process_list_model()
            response_model.from_json(process.list().to_json())
            self.write(response_model.to_json())
            self.set_status(200)
        except ValidationError:
            self.set_status(400)
        except NotFoundError:
            self.set_status(404)
        except Exception as errors:
            logger.error(str(errors))
            self.set_status(400)
        self.finish()

    def post(self, pid, signal):
        '''
        The "post" action requests the application to kill the given pid
        with the given signal. This is a shoot without response action.
        '''
        request_model = kill_process_model()
        try:
            request_model.pid.set(int(pid))
            request_model.signal.set(signal)
            kill_process = process_dispatcher()
            int_signal = kill_process.int_signal(request_model.signal.get())
            kill_process.kill(
                request_model.pid.get(),
                int_signal)
            self.set_status(204)
        except ValidationError as errors:
            logger.error(str(errors))
            self.set_status(400)
        except NotFoundError:
            self.set_status(404)
        except KeyError as errors:
            logger.error('Invalid signal name: %s' % (str(errors),))
            self.set_status(400)
        except Exception as errors:
            logger.error(
                'Exception during kill: %s (%s)'
                % (str(errors), type(errors),))
            self.set_status(400)
        self.finish()
