# -*- coding: utf-8 -*-
# kill_process.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
import psutil
import sys
import signal as signal_module

from netsplice.config import flags
from netsplice.util.model.errors import ValidationError
from netsplice.util.process.platform import factory as platform_factory
from netsplice.plugins.process_sniper.model.process_list import (
    process_list as process_list_model
)
from netsplice.util import get_logger

logger = get_logger()


class process(object):
    '''
    '''
    def __init__(self):
        self._username = platform_factory(sys.platform).username()

    def int_signal(self, str_signal):
        '''
        Integer Signal.

        Return the signal_module.SIG* constant for the signal-name.
        '''
        if not str_signal.startswith('SIG'):
            raise KeyError()
        return signal_module.__dict__[str_signal]

    def kill(self, pid, signal):
        '''
        Kill.

        Kill a process with the given pid and signal.
        '''
        try:
            has_stop = hasattr(signal_module, 'SIGSTOP')
            has_cont = hasattr(signal_module, 'SIGCONT')
            proc = psutil.Process(pid)
            if signal == signal_module.SIGTERM:
                proc.terminate()
            elif signal == signal_module.SIGKILL:
                proc.kill()
            elif has_stop and signal == signal_module.SIGSTOP:
                    proc.suspend()
            elif has_cont and signal == signal_module.SIGCONT:
                proc.resume()
            else:
                proc.send_signal(signal)
        except psutil.Error as errors:
            logger.critical(
                'Failed to kill %d with signal %d: %s'
                % (pid, signal, str(errors)))

    def list(self):
        '''
        List.

        List processes as a process_list model.
        '''
        platform = platform_factory(sys.platform)
        process_list = process_list_model()
        for proc in platform.process_iter():
            # check whether the process name matches
            try:
                pid = proc.pid
                commandline = proc.cmdline()
                username = proc.username()
            except psutil.ZombieProcess:
                continue
            except psutil.AccessDenied:
                commandline = [proc.name()]
                username = 'maybe %s' % (self._username,)
            if len(commandline) < 1:
                continue
            if flags.DEBUG:
                logger.debug(
                    'Process: %d %s %s'
                    % (pid, commandline, username))
            if self._username not in username:
                continue
            process_item = process_list.item_model_class()
            try:
                process_item.pid.set(pid)
                process_item.commandline.set(str(commandline))
                process_list.append(process_item)
            except ValidationError as errors:
                logger.warn('Invalid process information: %s' % str(errors))

        if flags.DEBUG:
            logger.debug(process_list.to_json())
        return process_list
