# -*- coding: utf-8 -*-
# commands.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for commands.
'''

from .command_item import command_item as command_item_model
from netsplice.util.model.marshalable_list import marshalable_list


class commands(marshalable_list):
    def __init__(self):
        marshalable_list.__init__(self, command_item_model)

    def add_command(self, commandline, signal_name='SIGTERM'):
        '''
        Add Command.

        Add the given commandline that should receive the given signal on
        close.
        '''
        command = command_item_model()
        command.commandline.set(commandline)
        command.signal.set(signal_name)
        self.append(command)
