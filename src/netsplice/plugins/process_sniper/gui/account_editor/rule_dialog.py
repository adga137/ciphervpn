# -*- coding: utf-8 -*-
# rule_dialog.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Create Rule Dialog.

Dialog to create or edit rules for kill_list.
'''

import re

from PySide.QtCore import Qt, Signal
from .rule_dialog_ui import Ui_RuleDialog

from netsplice.gui.widgets.dialog import dialog
from netsplice.model.configuration_item import (
    configuration_item as configuration_item_model
)

from netsplice.plugins.process_sniper.config import gui as config
from netsplice.plugins.process_sniper.config import backend as config_backend
from netsplice.plugins.process_sniper.config import events as config_events
from netsplice.plugins.process_sniper.config import signals as config_signals
from netsplice.plugins.process_sniper.gui.model.response.process_list import (
    process_list as response_process_list_model
)
from netsplice.util import basestring

SOURCE_PROCESS_LAUNCHER = 'source_process_launcher'
SOURCE_PROCESS_LIST = 'source_process_list'


class rule_dialog(dialog):
    '''
    Rule Dialog.

    Dialog that allows the creation or modification of a rule.
    '''

    backend_failed = Signal(basestring)
    commit = Signal(basestring, bool, basestring, basestring)
    process_list_done = Signal(basestring)
    test_process_list_done = Signal(basestring)

    def __init__(self, parent, dispatcher):
        '''
        Initialize Module.

        Initialize widget and abstract base, Setup UI.
        '''
        dialog.__init__(self, parent)
        self.set_minimal()
        self.backend = dispatcher
        self.process_list = response_process_list_model()
        self.ui = Ui_RuleDialog()
        self.ui.setupUi(self)
        self.setFocusPolicy(Qt.TabFocus)
        self.ui.ok_action.setFocus()

        self.ui.cancel_action.clicked.connect(
            self._cancel)
        self.ui.commandline.textChanged.connect(
            self._commandline_changed)
        self.ui.ok_action.clicked.connect(
            self._commit)
        self.ui.update_process_action.clicked.connect(
            self._update_process_list)
        self.ui.test_action.clicked.connect(
            self._test)
        self.ui.process_list.currentItemChanged.connect(
            self._process_list_item_changed)
        self.ui.process_list.sortItems()
        self.ui.process_list_filter.textEdited.connect(
            self._process_list_filter)

        self.ui.event_select.currentIndexChanged.connect(
            self._event_select_changed)
        self.ui.process_launcher_select.currentIndexChanged.connect(
            self._process_launcher_select_item_changed)
        self.ui.source_select.currentIndexChanged.connect(
            self._source_changed)
        self.ui.source_select.addItem(
            self.tr('Process Launcher'), SOURCE_PROCESS_LAUNCHER)
        self.ui.source_select.addItem(
            self.tr('Process List'), SOURCE_PROCESS_LIST)
        self.ui.label_process_launcher_select_empty_hint.setText(
            self.tr(config.RULE_DIALOG_LAUNCHER_SELECT_EMPTY))
        self.ui.label_process_launcher_select_hint.setText(
            self.tr(config.RULE_DIALOG_LAUNCHER_SELECT))

        self._commandline_changed()
        self.process_list_done.connect(self._process_list_done)
        self.test_process_list_done.connect(self._test_process_list_done)

        for name in config_events.EVENT_NAMES:
            self.ui.event_select.addItem(
                self.tr(name), name)
        for name in config_signals.SIGNAL_AVAILABLE:
            self.ui.signal_select.addItem(
                self.tr(name), name)
        for name in config_backend.EXTRA_SIGNALS:
            self.ui.signal_select.addItem(
                self.tr(name), name)
        self._update_process_list()

    def _cancel(self):
        '''
        '''
        self.reject()

    def _commandline_changed(self):
        '''
        Commandline Changed.

        Enable or disable the ok-action based on the LineEdit value.
        '''
        commandline = self.ui.commandline.text()
        if commandline == '':
            self.ui.ok_action.setEnabled(False)
        else:
            self.ui.ok_action.setEnabled(True)

    def _commit(self):
        '''
        Commit.

        Send a signal with the dialog's selection.
        '''
        event_name = self.ui.event_select.itemData(
            self.ui.event_select.currentIndex())
        state = self.ui.active.checkState()
        active = False
        if state == Qt.Checked:
            active = True
        signal_name = self.ui.signal_select.itemData(
            self.ui.signal_select.currentIndex())
        commandline = self.ui.commandline.text()
        self.commit.emit(event_name, active, signal_name, commandline)
        self.accept()

    def _event_select_changed(self):
        '''
        Event Select changed.

        The event to sniper on has changed. Repopulate the list of possible
        events.
        '''
        self._populate_launcher_items()

    def _is_preceding_event(self, event_name):
        '''
        Is Preceding Event.

        Return True when the given event_name can occur before the current
        selected sniper event.
        '''
        selected_event_name = self.ui.event_select.itemData(
            self.ui.event_select.currentIndex())
        if selected_event_name is None:
            return False
        predecessors = self._get_predecessors(selected_event_name)
        if event_name in predecessors:
            return True
        return False

    def _get_predecessors(self, event_name):
        '''
        Get Predecessors.

        Return a list of predecessors for the given event_name.

        Arguments:
            event_name (string): config.EVENT_NAME
        '''
        predecessors = config_events.EVENT_PREDECESSORS[event_name]
        return predecessors

    def _populate_launcher_items(self):
        '''
        Populate Launcher Items.

        Receive a list of items from the launcher widget that comes from the
        process manager.
        '''
        self.ui.process_launcher_select.clear()
        self.ui.label_process_launcher_id.hide()
        self.ui.process_launcher_select.hide()
        self.ui.label_process_launcher_select_hint.hide()
        self.ui.label_process_launcher_select_empty_hint.show()
        self.ui.label_process_launcher_select_empty_hint.setText(
            self.tr(config.RULE_DIALOG_LAUNCHER_SELECT_EMPTY))
        try:
            process_launcher = self.parent().manager.ui.process_launcher
        except AttributeError:
            # process launcher not available
            return
        self.ui.process_launcher_select.currentIndexChanged.disconnect(
            self._process_launcher_select_item_changed)

        launcher_values = process_launcher.get_values()
        launcher_item_available = False
        dropped_events = list()
        for item in launcher_values.launch_list:
            event_name = item.event.get()
            if not self._is_preceding_event(event_name):
                dropped_events.append(event_name)
                continue
            id = item.id.get()
            elevated = item.elevated.get()
            active = item.active.get()
            executable = item.executable.get()
            parameters = item.parameters.get()
            if elevated or not active:
                continue
            item_text = self.tr(
                '(Id: {id}) {event_name}: {executable}'
                ' with parameters {parameters}').format(
                    id=self.parent().get_launcher_user_id(id),
                    executable=executable,
                    event_name=event_name,
                    parameters=parameters)
            self.ui.process_launcher_select.addItem(item_text, id)
            launcher_item_available = True
        if launcher_item_available:
            self.ui.label_process_launcher_id.show()
            self.ui.process_launcher_select.show()
            self.ui.label_process_launcher_select_hint.show()
            self.ui.label_process_launcher_select_empty_hint.hide()
            selected = self.ui.process_launcher_select.findData(
                self.ui.commandline.text())
            if selected < 0:
                self.ui.ok_action.setEnabled(False)
            self.ui.process_launcher_select.setCurrentIndex(selected)
        else:
            try:
                event_name = self.ui.event_select.itemData(
                    self.ui.event_select.currentIndex())
                dropped_event_names = ', '.join(dropped_events)
                allowed_event_names = ', '.join(
                    self._get_predecessors(event_name))

                message = self.tr(
                    config.RULE_DIALOG_LAUNCHER_SELECT_EMPTY_DROPPED).format(
                        dropped_event_names=dropped_event_names,
                        allowed_event_names=allowed_event_names,
                        event_name=event_name)
                self.ui.label_process_launcher_select_empty_hint.setText(
                    message)
            except KeyError:
                # event_name or dropped_events is None
                pass
        self.ui.process_launcher_select.currentIndexChanged.connect(
            self._process_launcher_select_item_changed)

    def _process_launcher_select_item_changed(self, new_index):
        '''
        Process Launcher List Item Changed.

        The selected item in the list has changed.
        '''
        if new_index < 0:
            return
        source = self.ui.source_select.itemData(
            self.ui.source_select.currentIndex())
        if source != SOURCE_PROCESS_LAUNCHER:
            return
        commandline = self.ui.process_launcher_select.itemData(
            self.ui.process_launcher_select.currentIndex())
        self.ui.commandline.setText(commandline)

    def _process_list_item_changed(self, selected_item, prev_selected_item):
        '''
        Process List Activated.

        Update the commandline with the current selected values.
        '''
        text = selected_item.data(Qt.DisplayRole)
        self.ui.commandline.setText(text)

    def _process_list_filter(self):
        '''
        Process List Filter.

        Rebuild the process list with items that are in the filter lineedit.
        Avoid duplicate entries and convert a python-list to a regexp that
        can be used for the sniper.
        '''
        pattern_list = []
        filter_text = self.ui.process_list_filter.text()
        self.ui.process_list.hide()
        self.ui.process_list.currentItemChanged.disconnect(
            self._process_list_item_changed)

        while self.ui.process_list.count():
            item = self.ui.process_list.takeItem(0)
            if item is 0:
                break
            del item

        for item in self.process_list:
            commandline_regexp = item.commandline.get()
            commandline_regexp = re.sub('\'\', ', '', commandline_regexp)
            commandline_regexp = re.sub('^\[\'', '^.*', commandline_regexp)
            commandline_regexp = re.sub('\']$', '.*$', commandline_regexp)
            commandline_regexp = re.sub(
                '.*device.*harddiskvolume[0-9]*', '',
                commandline_regexp)
            commandline_regexp = commandline_regexp.replace('\', \'', '.*')
            commandline_regexp = commandline_regexp.replace('(', '.*')
            commandline_regexp = commandline_regexp.replace(')', '.*')
            commandline_regexp = commandline_regexp.replace('\\\\', '.*')
            commandline_regexp = commandline_regexp.replace('.*.*', '.*')
            if commandline_regexp in pattern_list:
                continue
            if filter_text not in commandline_regexp:
                continue
            pattern_list.append(commandline_regexp)
            self.ui.process_list.insertItem(0, commandline_regexp)
            self.ui.process_list.item(0).setData(
                Qt.ToolTipRole, item.commandline.get())

        self.ui.process_list.currentItemChanged.connect(
            self._process_list_item_changed)
        self.ui.process_list.show()

    def _process_list_done(self, process_list_json):
        '''
        Process List Done.

        Process List received from backend.
        Build a simple list with regexp magic from the items.
        '''

        self.process_list.from_json(process_list_json)
        self.ui.label_process_list_hint.show()
        self.ui.label_process_list_hint.setText(
            self.tr(config.RULE_DIALOG_HELP))
        self.ui.label_process_list_filter.show()
        self.ui.process_list_filter.show()
        self.ui.process_list_filter.setText('')
        self._process_list_filter()

    def _source_changed(self, new_index):
        '''
        Source changed.

        The Combobox that allows to select the process source has been
        changed.
        '''
        if new_index < 0:
            # Combobox reset
            return
        source = self.ui.source_select.itemData(new_index)
        self.ui.source_process_list.hide()
        self.ui.test_action.hide()

        self.ui.label_process_launcher_id.hide()
        self.ui.process_launcher_select.hide()
        self.ui.label_process_launcher_select_empty_hint.hide()
        self.ui.label_process_launcher_select_hint.hide()

        if source == SOURCE_PROCESS_LIST:
            self.ui.source_process_list.show()
            self.ui.process_list.clearSelection()
            self.ui.test_action.show()
        if source == SOURCE_PROCESS_LAUNCHER:
            self.ui.label_process_launcher_id.show()
            self.ui.process_launcher_select.show()
            self.ui.label_process_launcher_select_hint.show()
            self._populate_launcher_items()

    def _test(self):
        '''
        Test.

        Test the given configuration.
        '''
        self.backend.process_sniper.process_list_get.emit(
            self.test_process_list_done, self.backend_failed)

    def _test_process_list_done(self, process_list_json):
        '''
        Test process list done.

        The process list for the test.
        Create a configuration item for the account_editor and use its
        kill_list_message method to display a result.
        '''
        process_list = response_process_list_model()
        process_list.from_json(process_list_json)
        event_name = self.ui.event_select.itemData(
            self.ui.event_select.currentIndex())
        signal_name = self.ui.signal_select.itemData(
            self.ui.signal_select.currentIndex())
        state = self.ui.active.checkState()
        active = False
        if state == Qt.Checked:
            active = True
        commandline = self.ui.commandline.text()
        configuration_item = configuration_item_model(0)
        configuration_item.key.set(event_name)
        configuration_item.add_value('active', active)
        configuration_item.add_value('signal', signal_name)
        configuration_item.add_value('commandline', commandline)
        self.parent().kill_list_message(configuration_item, process_list)

    def _update_process_list(self):
        '''
        Update Process List.

        Update the list of processes.
        '''
        self.backend.process_sniper.process_list_get.emit(
            self.process_list_done, self.backend_failed)

    def create_mode(self):
        '''
        Create Mode.

        Rename the Ok action to display "Create".
        '''
        self.ui.ok_action.setText(self.tr('Create'))

    def set_values(self, event_name, active, signal_name, commandline):
        '''
        Set Values.

        Set the controls to the given values.
        '''
        state = Qt.Unchecked
        if active:
            state = Qt.Checked
        self.ui.active.setCheckState(state)
        self.ui.event_select.currentIndexChanged.disconnect(
            self._event_select_changed)
        self.ui.event_select.setCurrentIndex(
            self.ui.event_select.findData(event_name))
        self.ui.event_select.currentIndexChanged.connect(
            self._event_select_changed)
        self.ui.signal_select.setCurrentIndex(
            self.ui.signal_select.findData(signal_name))
        self.ui.commandline.setText(commandline)
        try:
            # commandline is a specific launcher id
            self.parent().get_launcher_user_id(commandline)
            self.ui.source_select.setCurrentIndex(
                self.ui.source_select.findData(SOURCE_PROCESS_LAUNCHER))
            self._populate_launcher_items()
        except KeyError as errors:
            try:
                process_launcher = self.parent().manager.ui.process_launcher
                commandline_initial = commandline == \
                    config_backend.DEFAULT_COMMANDLINE
                launcher_values = process_launcher.get_values()
                launcher_item_available = False
                dropped_events = list()
                for item in launcher_values.launch_list:
                    event_name = item.event.get()
                    if not self._is_preceding_event(event_name):
                        dropped_events.append(event_name)
                        continue
                    elevated = item.elevated.get()
                    active = item.active.get()
                    if elevated or not active:
                        continue
                    launcher_item_available = True
                if launcher_item_available and commandline_initial:
                    self.ui.source_select.setCurrentIndex(
                        self.ui.source_select.findData(
                            SOURCE_PROCESS_LAUNCHER))
                    self._populate_launcher_items()
                else:
                    self.ui.source_select.setCurrentIndex(
                        self.ui.source_select.findData(SOURCE_PROCESS_LIST))
            except AttributeError:
                # process launcher not available
                self.ui.source_select.setCurrentIndex(
                    self.ui.source_select.findData(SOURCE_PROCESS_LIST))

    def update_mode(self):
        '''
        Update Mode.

        Rename the Ok action to display "Update".
        '''
        self.ui.ok_action.setText(self.tr('Update'))
