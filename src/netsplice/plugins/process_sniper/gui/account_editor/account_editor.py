# -*- coding: utf-8 -*-
# account_editor.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Account editor.

Extend Account Editor widget.
'''

import re
from PySide.QtGui import QWidget, QMessageBox
from PySide.QtCore import Qt, Signal
from .account_editor_ui import Ui_AccountEditor
from .rule_dialog import rule_dialog

from netsplice.config import constants
from netsplice.gui.key_value_editor import key_value_editor as key_value_editor
from netsplice.model.configuration_item import (
    configuration_item as configuration_item_model
)
from netsplice.plugins.process_sniper.config import backend as config_backend
from netsplice.plugins.process_sniper.config import gui as config
from netsplice.plugins.process_sniper.config import (
    PLUGIN_NAME, PLUGIN_COLLECTION, FIELD_ID, FIELD_ACTIVE, FIELD_EVENT_NAME,
    FIELD_SIGNAL, FIELD_COMMANDLINE, FIELD_PROFILE, KEY_REMOVED_PREFIX
)

from netsplice.plugins.process_sniper.gui.model.response.plugin_collection \
    import (
        plugin_collection as response_plugin_collection_model
    )
from netsplice.plugins.process_sniper.gui.model.response.process_list import (
    process_list as response_process_list_model
)
from netsplice.util.model.errors import ValidationError
from netsplice.util.parser import factory as get_config_parser
from netsplice.util import basestring, escape_html, get_logger

logger = get_logger()


class account_editor(QWidget):
    '''
    Account Editor.

    Custom Editor for Process Sniper options.
    '''

    backend_failed = Signal(basestring)
    config_changed = Signal()
    plugin_collection_loaded = Signal(basestring)
    plugin_collection_stored = Signal(basestring)
    test_process_list_done = Signal(basestring)
    account_profile_changed = Signal(object)

    def __init__(self, parent):
        '''
        Initialize Module.

        Initialize widget and abstract base, Setup UI.
        This widget has to be embedded in an UI-file.
        '''
        QWidget.__init__(self, parent)
        self.backend = None
        self.account_model = None
        self.editor_model = response_plugin_collection_model()
        self.model = response_plugin_collection_model()
        self.parent_account_editor = None
        self._commandline_uuid_map = dict()
        self._uuid_commandline_map = dict()
        self.ui = Ui_AccountEditor()
        self.ui.setupUi(self)
        self.ui.editor_layout.setContentsMargins(0, 0, 0, 0)
        self.setFocusPolicy(Qt.TabFocus)
        self.ui.command_signal_list.set_header([
            self.tr('Event'),
            self.tr('Id'),
            self.tr('Active'),
            self.tr('Signal'),
            self.tr('Profile'),
            self.tr('Command line')
        ])
        self.ui.command_signal_list.enable_toolbar(
            key_value_editor.INSERT |
            key_value_editor.EDIT |
            key_value_editor.REMOVE)
        self.ui.command_signal_list.add_toolbar_action(
            key_value_editor.SELECTED,
            self.tr('Test Match'), self._test)
        self.ui.command_signal_list.set_insert_template(
            self._get_kill_item_template())
        self.ui.command_signal_list.set_row_edit_callback(
            self._edit_row_callback)
        self.ui.command_signal_list.set_row_insert_callback(
            self._insert_row_callback)

    def _add_rule(
            self, event_name, active, signal_name, commandline, profile=False):
        '''
        Add Rule.

        Add rule to kill List.
        '''
        plugin_collection = self.get_values()
        kill_item = plugin_collection.kill_list.item_model_class()
        kill_item.active.set(active)
        kill_item.event.set(event_name)
        kill_item.signal.set(signal_name)
        kill_item.commandline.set(commandline)
        kill_item.profile.set(profile)
        plugin_collection.kill_list.append(kill_item)
        self.editor_model.from_json(plugin_collection.to_json())
        self._update_ui()
        self.config_changed.emit()

    def _edit_row_callback(self, table_widget_item=None):
        '''
        Row Editor

        Display a dialog that loads the values of the current selected row
        and updates this row on commit.
        '''
        if table_widget_item is not None:
            self.ui.command_signal_list.table.closePersistentEditor(
                table_widget_item)
        dialog = rule_dialog(self, self.backend)
        selected_values = self.ui.command_signal_list.get_selected_values()
        for value in selected_values:
            event_name = value.key.get()
            active = value.get_value(FIELD_ACTIVE)
            profile = value.get_value(FIELD_PROFILE)
            signal_name = value.get_value(FIELD_SIGNAL)
            commandline = value.get_value(FIELD_COMMANDLINE)
            try:
                commandline = self._commandline_uuid_map[commandline]
            except KeyError:
                # commandline is a pattern
                pass
            dialog.set_values(event_name, active, signal_name, commandline)
            break
        dialog.update_mode()
        dialog.commit.connect(self._update_rule)
        dialog.setModal(True)
        dialog.show()

    def _get_kill_item_template(self):
        '''
        Get Kill Item Template.

        Return a key_value_table template for a new item in the kill list.
        '''
        template = configuration_item_model(0)
        template.key.set(config_backend.DEFAULT_EVENT)
        template.add_value(
            FIELD_ACTIVE, True)
        template.add_value(
            FIELD_SIGNAL, config_backend.DEFAULT_SIGNAL)
        template.add_value(
            FIELD_PROFILE, False)
        template.add_value(
            FIELD_COMMANDLINE, config_backend.DEFAULT_COMMANDLINE)
        return template

    def get_launcher_user_id(self, id):
        '''
        Get Launcher Id.

        Return the user id for the given launcher uuid.
        '''
        try:
            process_launcher = self.manager.ui.process_launcher
        except AttributeError:
            # process launcher not available
            return '-1'
        return process_launcher.get_user_id(id)

    def _insert_row_callback(self):
        '''
        Insert Rule.

        Display a dialog that inserts a new rule when commited.
        '''
        new_kill_item = self._get_kill_item_template()
        dialog = rule_dialog(self, self.backend)
        dialog.set_values(
            new_kill_item.key.get(),
            new_kill_item.get_value(FIELD_ACTIVE),
            new_kill_item.get_value(FIELD_SIGNAL),
            new_kill_item.get_value(FIELD_COMMANDLINE))
        dialog.create_mode()
        dialog.commit.connect(self._add_rule)
        dialog.setModal(True)
        dialog.show()

    def _is_launcher_commandline(self, commandline):
        '''
        Is Launcher Commandline.

        The given commandline is a uuid and therefore considered a launcher
        id.
        '''
        try:
            self.get_launcher_user_id(commandline)
            return True
        except KeyError:
            return False

    def _load_plugin_values(self, account_instance):
        '''
        Load Plugin Values.

        The account model was loaded, request the plugin value.
        '''
        if account_instance is None:
            return
        account_id = account_instance.id.get()
        if account_id is None:
            return
        self.account_model = account_instance
        self.backend.preferences_plugin_collection_instance_get.emit(
            PLUGIN_NAME, PLUGIN_COLLECTION, account_id,
            self.plugin_collection_loaded, self.backend_failed)

    def _plugin_collection_loaded(self, model_json):
        '''
        Plugin Collection Loaded.

        The request for the plugin values completed with the given model_json.
        '''
        self.model = response_plugin_collection_model()
        self.model.from_json(model_json)
        self.editor_model = response_plugin_collection_model()
        self.editor_model.from_json(model_json)
        self._update_ui()
        self.config_changed.emit()

    def _update_ui(self):
        '''
        Update UI.

        Update the view from the editor model
        '''
        parser = get_config_parser('Line')
        for (index, item) in enumerate(self.editor_model.kill_list):
            line = parser.current_element
            line.key.set(item.event.get())
            line.add_value(FIELD_ID, str(index))
            line.add_value(FIELD_ACTIVE, item.active.get())
            line.add_value(FIELD_SIGNAL, item.signal.get())
            line.add_value(FIELD_PROFILE, item.profile.get())
            commandline = item.commandline.get()
            if self._is_launcher_commandline(commandline):
                user_commandline = self.tr('Launcher Id: {id}').format(
                    id=self.get_launcher_user_id(commandline))
                self._uuid_commandline_map[commandline] = user_commandline
                self._commandline_uuid_map[user_commandline] = commandline
                commandline = user_commandline
            line.add_value(FIELD_COMMANDLINE, commandline)
            parser.commit()

        self.ui.command_signal_list.model_changed.emit(
            parser.get_config())

    def _plugin_collection_stored(self):
        '''
        Plugin Collection Stored.

        The plugin values have been stored.
        '''
        values = self.get_values()
        self.model.from_json(values.to_json())

    def _process_launcher_changed(self):
        '''
        Process Launcher Changed.

        The values of the process launcher have changed.
        Trigger reload of rules so the id-maps are up-to date.
        '''
        plugin_collection = self.get_values()
        self.editor_model.from_json(plugin_collection.to_json())
        self._update_ui()
        self.config_changed.emit()

    def _reset_plugin_values(self):
        '''
        Reset Plugin Values.

        The account model was reset, commit the plugin value.
        '''
        parser = get_config_parser('Line')

        self.ui.command_signal_list.model_changed.emit(
            parser.get_config())
        self.ui.errors.setText('')
        self.ui.errors.hide()

    def _setup_connections(self):
        '''
        Setup Connections.

        Setup the connections for the widget.
        '''
        self.ui.command_signal_list.values_changed.connect(
            self._values_changed)
        self.ui.command_signal_list.value_checked.connect(
            self._value_checked)
        if self.parent_account_editor is not None:
            self.parent_account_editor.model_loaded_signal.connect(
                self._load_plugin_values)
            self.parent_account_editor.model_reset_signal.connect(
                self._reset_plugin_values)
            self.parent_account_editor.model_stored_signal.connect(
                self._store_plugin_values)
        self.plugin_collection_loaded.connect(self._plugin_collection_loaded)
        self.plugin_collection_stored.connect(self._plugin_collection_stored)
        self.test_process_list_done.connect(self._test_process_list_done)

    def _store_plugin_values(self, account_id):
        '''
        Store Plugin Values.

        The account model was stored, commit the plugin value. There is no
        way to interrupt the storage, all values have been validated before.
        '''
        try:
            values = self.get_values()
            self.backend.preferences_plugin_collection_instance_set.emit(
                PLUGIN_NAME, PLUGIN_COLLECTION, account_id, values,
                self.plugin_collection_stored, self.backend_failed)
        except ValidationError:
            pass

    def _test(self):
        '''
        Test.

        Test the current values with a current process-list.
        '''
        self.backend.process_sniper.process_list_get.emit(
            self.test_process_list_done, self.backend_failed)

    def _test_process_list_done(self, process_list_json):
        '''
        Test Process List Done.

        Process List received from backend.
        '''
        process_list = response_process_list_model()
        process_list.from_json(process_list_json)
        selected_values = self.ui.command_signal_list.get_selected_values()
        self.kill_list_message(selected_values[0], process_list)

    def _update_rule(self, event_name, active, signal_name, commandline):
        '''
        Update Rule.

        Update the current selected rule with the values from the dialog.
        '''
        selected_values = self.ui.command_signal_list.get_selected_values()
        for value in selected_values:
            value.key.set(event_name)
            value.values.find_by_name(FIELD_SIGNAL).value.set(signal_name)
            value.values.find_by_name(FIELD_ACTIVE).value.set(active)
            if self._is_launcher_commandline(commandline):
                user_commandline = self.tr('Launcher Id: {id}').format(
                    id=self.get_launcher_user_id(commandline))
                self._uuid_commandline_map[commandline] = user_commandline
                self._commandline_uuid_map[user_commandline] = commandline
                commandline = user_commandline
            value.values.find_by_name(FIELD_COMMANDLINE).value.set(commandline)
            value.values.find_by_name(FIELD_PROFILE).value.set(False)
            break
        self.ui.command_signal_list.update_selected_values(selected_values)

        plugin_collection = self.get_values()
        self.editor_model.from_json(plugin_collection.to_json())
        self._update_ui()
        self.config_changed.emit()

    def _values_changed(self, new_values):
        '''
        Values Changed.

        Key Value table values have changed, validate the values and output
        errors.
        '''
        plugin_collection = response_plugin_collection_model()
        plugin_collection.name.set(PLUGIN_NAME)
        errors = []
        for value in new_values:
            event_name = value.key.get()
            if event_name.startswith(KEY_REMOVED_PREFIX):
                continue
            active = value.get_value(FIELD_ACTIVE)
            signal_name = value.get_value(FIELD_SIGNAL)
            commandline = value.get_value(FIELD_COMMANDLINE)
            kill_item = plugin_collection.kill_list.item_model_class()
            kill_item.active.set(active)
            kill_item.profile.set(False)
            try:
                kill_item.event.set(event_name)
            except ValidationError:
                errors.append(
                    self.tr(r'%s is not a valid event.')
                    % (event_name,))
            try:
                kill_item.signal.set(signal_name)
            except ValidationError:
                errors.append(
                    self.tr(r'%s is not a valid signal.')
                    % (signal_name,))
            try:
                kill_item.commandline.set(commandline)
            except ValidationError:
                errors.append(
                    self.tr(r'%s is not a valid commandline.')
                    % (commandline,))
            plugin_collection.kill_list.append(kill_item)

        if len(errors) is 0:
            # set the validated values, most notably, remove the items
            # that have the KEY_REMOVED_PREFIX
            self.editor_model.from_json(plugin_collection.to_json())
            self._update_ui()
            self.config_changed.emit()
            self.ui.errors.hide()
        else:
            error_text = ''
            for error in errors:
                error_text += '%s<br/>' % (escape_html(error),)
            self.ui.errors.setText(error_text)
            self.ui.errors.show()

    def _value_checked(self, new_row_values):
        '''
        Value Checked.

        A value has been toggled notify the manager.
        '''

        plugin_collection = self.get_values()
        self.editor_model.from_json(plugin_collection.to_json())
        self._update_ui()
        self.config_changed.emit()

    def add_from_template(self, template, profile):
        '''
        Add from Template.

        Add sniper from template, find the template by the command.

        Arguments:
            template_name (string): command of the template.
        '''
        self._add_rule(
            template[FIELD_EVENT_NAME],
            True,
            template[FIELD_SIGNAL],
            template[FIELD_COMMANDLINE],
            profile=profile)

    def config_values_changed(self):
        '''
        Config Values Changed.

        Evaluate if the config is different from the loaded values.
        '''
        changed = False
        kill_list = self.ui.command_signal_list.get_values()
        changed = len(self.model.kill_list) != len(kill_list)
        for launch_item in kill_list:
            if changed:
                break
            model_launch_item = None
            for model_item in self.model.kill_list:
                found = True
                found &= (
                    model_item.signal.get() ==
                    launch_item.get_value(FIELD_SIGNAL))
                found &= (
                    model_item.event.get() ==
                    launch_item.key.get())
                found &= (
                    str(model_item.commandline.get()) ==
                    str(launch_item.get_value(FIELD_COMMANDLINE)))
                found &= (
                    model_item.active.get() ==
                    launch_item.get_value(FIELD_ACTIVE))
                if found:
                    model_launch_item = model_item
                    break
            if model_launch_item is None:
                changed = True
                break
        return changed

    def get_values(self):
        '''
        Get Values.

        Return the values suitable for storage.
        '''
        new_values = self.ui.command_signal_list.get_values()
        plugin_collection = response_plugin_collection_model()
        plugin_collection.name.set(PLUGIN_NAME)
        for value in new_values:
            event_name = value.key.get()
            if event_name.startswith(KEY_REMOVED_PREFIX):
                continue
            active = value.get_value(FIELD_ACTIVE)
            signal_name = value.get_value(FIELD_SIGNAL)
            commandline = value.get_value(FIELD_COMMANDLINE)
            profile = value.get_value(FIELD_PROFILE)
            kill_item = plugin_collection.kill_list.item_model_class()
            kill_item.active.set(active)
            kill_item.event.set(event_name)
            kill_item.signal.set(signal_name)
            kill_item.profile.set(profile)
            try:
                commandline = self._commandline_uuid_map[commandline]
            except KeyError:
                # The generated commandline is not in the map, its a pattern
                pass
            kill_item.commandline.set(commandline)
            plugin_collection.kill_list.append(kill_item)

        return plugin_collection

    def kill_list_message(self, configuration_item, process_list):
        '''
        Kill List Message.

        Create a message with all processes that match the configuration_items
        commandline and display a information message box.
        '''

        message = self.tr(
            'The following processes will receive a'
            ' <b>{signal_name}</b>\n'
            'when <b>{event_name}</b> occurs for <b>{account_name}</b>\n'
            '<p>PID (commandline)</p>\n{kill_list}\n')
        kill_list = ''
        account_name = self.tr('Unnamed Account')
        if self.account_model is not None:
            account_name = escape_html(self.account_model.name.get())
        selected_commandline = escape_html(
            configuration_item.get_value('commandline'))
        selected_event_name = configuration_item.get_key()
        selected_signal_name = configuration_item.get_value('signal')
        selected_active = configuration_item.get_value('active')
        try:
            self._commandline_uuid_map[selected_commandline]
            message = self.tr(
                'The process launched by <b>{commandline}</b> will receive a'
                ' <b>{signal_name}</b>\n'
                'when <b>{event_name}</b> occurs for <b>{account_name}</b>'
                '\n')
            kill_list = 'uncertain'
        except KeyError:
            pass
        more_processes = 0
        num_matches = 0

        for item in process_list:
            commandline = item.commandline.get()
            if not re.match(selected_commandline, commandline):
                continue
            kill_item = (
                '<p>%d (%s)\n'
                % (item.pid.get(), escape_html(item.commandline.get()),))
            if num_matches < config.MAX_KILL_MESSAGE_ITEMS:
                kill_list += kill_item
            else:
                more_processes += 1
            num_matches += 1

        if more_processes > 0:
            kill_list += '<p>%s' % (self.tr('And {more_processes} more.'))
            kill_list = kill_list.format(more_processes=more_processes)

        if kill_list == '':
            kill_list = '<p>%s' % (self.tr('No process matches'),)

        if not selected_active:
            message += self.tr('<p><b>This rule is not active</b></p>')

        QMessageBox.information(
            None, constants.NS_NAME,
            message.format(
                account_name=account_name,
                commandline=selected_commandline,
                event_name=selected_event_name,
                signal_name=selected_signal_name,
                kill_list=kill_list),
            QMessageBox.Ok, QMessageBox.Ok)

    def remove_profile_items(self):
        '''
        Remove Profile Items.

        Remove all items that have the profile flag set.
        '''
        plugin_collection = self.get_values()

        parser = get_config_parser('Line')
        for (index, item) in enumerate(plugin_collection.kill_list):
            if item.profile.get():
                continue
            line = parser.current_element
            line.key.set(item.event.get())
            line.add_value(FIELD_ID, str(index))
            line.add_value(FIELD_ACTIVE, item.active.get())
            line.add_value(FIELD_SIGNAL, item.signal.get())
            line.add_value(FIELD_PROFILE, item.profile.get())
            commandline = item.commandline.get()
            if self._is_launcher_commandline(commandline):
                user_commandline = self.tr('Launcher Id: {id}').format(
                    id=self.get_launcher_user_id(commandline))
                self._uuid_commandline_map[commandline] = user_commandline
                self._commandline_uuid_map[user_commandline] = commandline
                commandline = user_commandline
            line.add_value(FIELD_COMMANDLINE, commandline)
            parser.commit()

        self.ui.command_signal_list.model_changed.emit(
            parser.get_config())

        plugin_collection = self.get_values()
        self.editor_model.from_json(plugin_collection.to_json())
        self._update_ui()

    def set_account_editor(self, account_editor_widget):
        '''
        Set Account Editor.

        Set the parent account_editor_widget.
        '''
        self.parent_account_editor = account_editor_widget
        self._setup_connections()

    def set_dispatcher(self, dispatcher):
        '''
        Set dispatcher.

        Set the backend dispatcher for the widget.
        '''
        self.backend = dispatcher

    def set_manager(self, manager):
        '''
        Set manager.

        '''
        try:
            process_launcher = manager.ui.process_launcher
            process_launcher.command_list_changed.connect(
                self._process_launcher_changed)
        except AttributeError:
            pass
