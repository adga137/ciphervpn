# -*- coding: utf-8 -*-
# config.py
# Copyright (C) 2013 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Config for a local 'dynamic' application-level port forwarding.

https://www.ipredator.se/guide/openvpn/settings
'''
ACCOUNT_LABEL = 'Socks Proxy'

DESCRIPTION = '''
<p><b>Socks Proxy</b> Access a network through a remote ssh service.</p>

<p>Specifies a local 'dynamic' application-level port forwarding. This works by
allocating a socket to listen to port on the local side, optionally bound to
the specified bind_address.  Whenever a connection is made to this port, the
connection is forwarded over the secure channel, and the application protocol
is then used to determine where to connect to from the remote machine.
Currently the SOCKS4 and SOCKS5 protocols are supported, and ssh will act as a
SOCKS server. Only root can forward privileged ports.

<p>Configure your browser proxy settings to the configured localhost:PORT with
Socks5. Choose Proxy DNS when the browser supports that setting.</p>
'''

DEFAULT_CONFIG = '''
-D localhost:PORT
-C
-N
-T
-l USERNAME
host REMOTE_HOST
'''
