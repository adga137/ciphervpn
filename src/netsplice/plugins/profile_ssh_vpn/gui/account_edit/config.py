# -*- coding: utf-8 -*-
# config.py
# Copyright (C) 2013 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Config for a ssh vpn.

'''
ACCOUNT_LABEL = 'Tunnel Forwarding'

DESCRIPTION = '''
<p><b>VPN</b> Pass requests to a local port to a remote system.</p>

<p>OpenSSH has built-in TUN/TAP support using -w
LOCAL_TUN_NUMBER:REMOTE_TUN_NUMBER. Here, a layer 3/point-to-point/ TUN tunnel
is described. It is also possible to create a layer 2/ethernet/TAP tunnel.</p>

<p>Requests tunnel device forwarding with the specified tun(4) devices between
the client (local_tun) and the server (remote_tun). Ssh should have access
rights to tun interface or permissions to create it. Check owner of tun
interface and/or /dev/net/tun.</p>

<p>If you want to access a network rather than a single machine you should
properly set up IP packet forwarding, routing and maybe a netfilter on both
sides.</p>
'''

DEFAULT_CONFIG = '''
-w LOCAL_TUN_NUMBER:REMOTE_TUN_NUMBER
-C
-N
-T
-l USERNAME
host REMOTE_HOST
'''
