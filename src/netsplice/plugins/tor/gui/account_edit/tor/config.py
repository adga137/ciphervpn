# -*- coding: utf-8 -*-
# config.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Config for Account Edit
'''


ACCOUNT_EXISTING = 0
ACCOUNT_CREATE = 1

# Maximum number of bytes to be loaded as configuration
MAX_CONFIG_SIZE = 32768

DEFAULT_ACCOUNT = ('''
SOCKSPort localhost:9055
''')


# Available account types. The name is used to select the editor with
# type_editor_factory.
ACCOUNT_LABEL = 'Tor'
