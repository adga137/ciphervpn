#!/bin/bash
#
# Test backend api
# execute curl commands on the backend api and evaluates its output. Used
# for quick integration test during development.
# set -x
echo 'this assumes that you disabled HTTPS and HMAC authentication'
echo 'and the backend is running on port 10000 on localhost'

function die(){
    echo "$@"
    section "FAILURE"
    exit 1
}
function section(){
    printf '\n'
    printf -v _hr "%*s" $(tput cols) && echo ${_hr// /-}
    printf '[ %s ]\n' "$@"
}
function tcurl(){
    code=$1
    shift
    content_filter=$1
    shift
    method=$1
    if [ ${method} == "GET" ]; then
        method=""
    else
        method="-X ${method}"
    fi
    shift
    url=$1
    section "curl: ${code} ${url}"
    shift
    if [ -z "$1" ]; then
        data=""
    else
        data="--data '"$@"'"
    fi
    export tcurl_result=/tmp/test.result
    export tcurl_script=/tmp/test.script
    echo "curl -sS -v ${url} ${method} ${data} 2>&1" > ${tcurl_script}
    bash ${tcurl_script} > ${tcurl_result}
    rescode=$(cat ${tcurl_result} | grep "HTTP/1.1 "| sed "s|< HTTP/1.1 \([0-9]*\).*|\1|")
    cat ${tcurl_result}| grep "HTTP/1.1 ${code}" > /dev/null \
      && echo "[OK: code: ${code}]" \
      || die "bad response code ${code} vs ${rescode} for ${method} ${url}"

    result_output=$(cat ${tcurl_result} | grep -v '^>' | grep -v '^<' | grep -v '^\*')
    cat ${tcurl_result} | grep "${content_filter}" > /dev/null \
      && echo "[OK: content: '${content_filter}']" \
      || die "bad response, expecting '${content_filter}' in '${result_output}'"

    export TCURL_RESULT=${result}
}

function test_versions(){
    section "module"
    tcurl 200 "v0.0.0-system-provided" GET http://localhost:10000/module/plugin/tor/executables
    tcurl 200 "/usr/bin/tor" GET http://localhost:10000/module/plugin/tor/versions/v0.0.0-system-provided
}

# preparation:
# * remove preferences

# tor integration
function test_connect_disconnect_delete(){
    section "module"
    tcurl 200 "" POST http://localhost:10000/module/preferences/accounts \
        "{ \
        \"name\": \"tor-account\", \
        \"configuration\":\"SOCKSPort localhost:9055\", \
        \"import_configuration\": \"SOCKSPort localhost:9055\", \
        \"type\": \"Tor\", \
        \"enabled\": true, \
        \"default_route\": false, \
        \"autostart\": false \
        }"
    cat ${tcurl_result} | grep '^{"id": "' || die 'no account id'
    account_1_id=$(cat ${tcurl_result} | grep '^{"id": "' | sed 's|.*id": "\([^"]*\).*|\1|')
    printf "new account id: ${account_1_id}"
    tcurl 200 "" POST http://localhost:10000/module/gui/accounts/${account_1_id}/connect
    cat ${tcurl_result}
    tcurl 200 "" POST http://localhost:10000/module/gui/accounts/${account_1_id}/disconnect
    cat ${tcurl_result}
    tcurl 200 "Bootstrapped 100%: Done" GET http://localhost:10000/module/gui/logs
    cat ${tcurl_result}
    tcurl 200 "" DELETE http://localhost:10000/module/preferences/accounts/${account_1_id}
}


echo "Hallo developer,
This script creates and deletes accounts and therefore has a forced exit
built in. You have to edit the file to start and should be prepared to recreate
all settings.

Always leave the exit line in git.
"
exit
# ^ required, do not remove.
test_versions
test_connect_disconnect_delete
