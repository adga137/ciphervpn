# -*- coding: utf-8 -*-
# tor.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Variables defined here are local to the backend component of the client.
'''
TYPE_STRING = 'string'
TYPE_OPTION = 'option'
TYPE_BYTESIZE = 'byte_size'
TYPE_COMMENT = 'comment'

LOG_ABORT = 'abort'
LOG_ERROR = 'error'
LOG_RESET_CREDENTIAL = 'reset_credential'
LOG_CHANGE_STATE = 'change_state'
LOG_WARNING = 'warning'
LOG_INFO = 'info'
LOG_DEBUG = 'debug'
LOG_PROGRESS = 'progress'
LOG_CONNECTING = 'connecting'
LOG_CONNECTED = 'connected'
LOG_DISCONNECTED = 'disconnected'

LOG_MESSAGES = {
    ('[err]'): {
        'actions': [LOG_ERROR],
        'help': (
            'Fix the error.'
        )
    },
    ('[warn]'): {
        'actions': [LOG_WARNING],
        'help': (
            ''
        )
    },
    ('[notice] Opening Control listener on'): {
        'actions': [LOG_CONNECTING],
        'help': (
            'Tor process has started. Please wait.'
        )
    },
    ('[notice] Bootstrapped 100%: Done'): {
        'actions': [LOG_CONNECTED, LOG_CHANGE_STATE],
        'help': (
            'Tor is connected.'
        )
    },
    ('[notice]'): {
        'actions': [LOG_INFO],
        'help': (
            ''
        )
    },
    ('[notice] Catching signal TERM, exiting cleanly.'): {
        'actions': [LOG_DISCONNECTED],
        'help': (
            'Tor is disconnected.'
        )
    },
    ('Tor connection is no longer active.'): {
        'actions': [LOG_DISCONNECTED],
        'help': (
            'Tor is disconnected.'
        )
    },
    ('[warn] Failed to parse/validate config:'): {
        'actions': [LOG_ABORT],
        'help': (
            'Tor configuration in broken. Fix the configuration.'
        )
    }
}

KEYWORDS = {
    '#': {
        'help': (
            'Comment in the config. This option is ignored'
        ),
        'type': TYPE_COMMENT,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    ';': {
        'help': (
            'Comment in the config. This option is ignored'
        ),
        'type': TYPE_COMMENT,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'BandwidthRate': {
        'help': (
            ' N bytes|KBytes|MBytes|GBytes|TBytes|KBits|MBits|GBits|TBits'
            ' A token bucket limits the average incoming bandwidth usage on'
            ' this node to the specified number of bytes per second, and the'
            ' average outgoing bandwidth usage to that same value. If you want'
            ' to run a relay in the public network, this needs to be at the'
            ' very least 75 KBytes for a relay (that is, 600 kbits) or 50'
            ' KBytes for a bridge (400 kbits) — but of course, more is better;'
            ' we recommend at least 250 KBytes (2 mbits) if possible.'
            ' (Default: 1 GByte)'
            '\n'
            ' Note that this option, and other bandwidth-limiting options,'
            ' apply to TCP data only: They do not count TCP headers or DNS'
            ' traffic.'
            '\n'
            ' With this option, and in other options that take arguments in'
            ' bytes, KBytes, and so on, other formats are also supported.'
            ' Notably, "KBytes" can also be written as "kilobytes" or "kb";'
            ' "MBytes" can be written as "megabytes" or "MB"; "kbits" can be'
            ' written as "kilobits"; and so forth. Tor also accepts "byte" and'
            ' "bit" in the singular. The prefixes "tera" and "T" are also'
            ' recognized. If no units are given, we default to bytes. To avoid'
            ' confusion, we recommend writing "bytes" or "bits" explicitly,'
            ' since it\'s easy to forget that "B" means bytes, not bits.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'BandwidthBurst': {
        'help': (
            ' N bytes|KBytes|MBytes|GBytes|TBytes|KBits|MBits|GBits|TBits'
            ' Limit the maximum token bucket size (also known as the burst) to'
            ' the given number of bytes in each direction. (Default: 1 GByte)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'MaxAdvertisedBandwidth': {
        'help': (
            ' N bytes|KBytes|MBytes|GBytes|TBytes|KBits|MBits|GBits|TBits'
            ' If set, we will not advertise more than this amount of bandwidth'
            ' for our BandwidthRate. Server operators who want to reduce the'
            ' number of clients who ask to build circuits through them (since'
            ' this is proportional to advertised bandwidth rate) can thus'
            ' reduce the CPU demands on their server without impacting network'
            ' performance.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'RelayBandwidthRate': {
        'help': (
            ' N bytes|KBytes|MBytes|GBytes|TBytes|KBits|MBits|GBits|TBits'
            ' If not 0, a separate token bucket limits the average incoming'
            ' bandwidth usage for _relayed traffic_ on this node to the'
            ' specified number of bytes per second, and the average outgoing'
            ' bandwidth usage to that same value. Relayed traffic currently is'
            ' calculated to include answers to directory requests, but that'
            ' may change in future versions. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'RelayBandwidthBurst': {
        'help': (
            ' N bytes|KBytes|MBytes|GBytes|TBytes|KBits|MBits|GBits|TBits'
            ' If not 0, limit the maximum token bucket size (also known as the'
            ' burst) for _relayed traffic_ to the given number of bytes in'
            ' each direction. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PerConnBWRate': {
        'help': (
            ' N bytes|KBytes|MBytes|GBytes|TBytes|KBits|MBits|GBits|TBits'
            ' If set, do separate rate limiting for each connection from a'
            ' non-relay. You should never need to change this value, since a'
            ' network-wide value is published in the consensus and your relay'
            ' will use that value. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PerConnBWBurst': {
        'help': (
            ' N bytes|KBytes|MBytes|GBytes|TBytes|KBits|MBits|GBits|TBits'
            ' If set, do separate rate limiting for each connection from a'
            ' non-relay. You should never need to change this value, since a'
            ' network-wide value is published in the consensus and your relay'
            ' will use that value. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ClientTransportPlugin': {
        'help': (
            ' transport socks4|socks5 IP:PORT'
            ' ClientTransportPlugin transport exec path-to-binary [options]'
            ' In its first form, when set along with a corresponding Bridge'
            ' line, the Tor client forwards its traffic to a SOCKS-speaking'
            ' proxy on "IP:PORT". It\'s the duty of that proxy to properly'
            ' forward the traffic to the bridge.'
            '\n'
            ' In its second form, when set along with a corresponding Bridge'
            ' line, the Tor client launches the pluggable transport proxy'
            ' executable in path-to-binary using options as its command-line'
            ' options, and forwards its traffic to it. It\'s the duty of that'
            ' proxy to properly forward the traffic to the bridge.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ServerTransportPlugin': {
        'help': (
            ' transport exec path-to-binary [options]'
            ' The Tor relay launches the pluggable transport proxy in path-to-'
            ' binary using options as its command-line options, and expects to'
            ' receive proxied client traffic from it.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ServerTransportListenAddr': {
        'help': (
            ' transport IP:PORT'
            ' When this option is set, Tor will suggest IP:PORT as the'
            ' listening address of any pluggable transport proxy that tries to'
            ' launch transport.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ServerTransportOptions': {
        'help': (
            ' transport k=v k=v ...'
            ' When this option is set, Tor will pass the k=v parameters to any'
            ' pluggable transport proxy that tries to launch transport.'
            '\n'
            ' (Example: ServerTransportOptions obfs45 shared-'
            ' secret=bridgepasswd cache=/var/lib/tor/cache)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ExtORPort': {
        'help': (
            ' [address:]port|auto'
            ' Open this port to listen for Extended ORPort connections from'
            ' your pluggable transports.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ExtORPortCookieAuthFile': {
        'help': (
            ' Path'
            ' If set, this option overrides the default location and file name'
            ' for the Extended ORPort\'s cookie file — the cookie file is'
            ' needed for pluggable transports to communicate through the'
            ' Extended ORPort.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ExtORPortCookieAuthFileGroupReadable': {
        'help': (
            ' 0|1'
            ' If this option is set to 0, don\'t allow the filesystem group to'
            ' read the Extended OR Port cookie file. If the option is set to'
            ' 1, make the cookie file readable by the default GID. [Making the'
            ' file readable by other groups is not yet implemented; let us'
            ' know if you need this for some reason.] (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ConnLimit': {
        'help': (
            ' NUM'
            ' The minimum number of file descriptors that must be available to'
            ' the Tor process before it will start. Tor will ask the OS for as'
            ' many file descriptors as the OS will allow (you can find this by'
            ' "ulimit -H -n"). If this number is less than ConnLimit, then Tor'
            ' will refuse to start.'
            '\n'
            ' You probably don\'t need to adjust this. It has no effect on'
            ' Windows since that platform lacks getrlimit(). (Default: 1000)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'DisableNetwork': {
        'help': (
            ' 0|1'
            ' When this option is set, we don\'t listen for or accept any'
            ' connections other than controller connections, and we close (and'
            ' don\'t reattempt) any outbound connections. Controllers sometimes'
            ' use this option to avoid using the network until Tor is fully'
            ' configured. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ConstrainedSockets': {
        'help': (
            ' 0|1'
            ' If set, Tor will tell the kernel to attempt to shrink the'
            ' buffers for all sockets to the size specified in'
            ' ConstrainedSockSize. This is useful for virtual servers and'
            ' other environments where system level TCP buffers may be'
            ' limited. If you\'re on a virtual server, and you encounter the'
            ' "Error creating network socket: No buffer space available"'
            ' message, you are likely experiencing this problem.'
            '\n'
            ' The preferred solution is to have the admin increase the buffer'
            ' pool for the host itself via /proc/sys/net/ipv4/tcp_mem or'
            ' equivalent facility; this configuration option is a second-'
            ' resort.'
            '\n'
            ' The DirPort option should also not be used if TCP buffers are'
            ' scarce. The cached directory requests consume additional sockets'
            ' which exacerbates the problem.'
            '\n'
            ' You should not enable this feature unless you encounter the "no'
            ' buffer space available" issue. Reducing the TCP buffers affects'
            ' window size for the TCP stream and will reduce throughput in'
            ' proportion to round trip time on long paths. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ConstrainedSockSize': {
        'help': (
            ' N bytes|KBytes'
            ' When ConstrainedSockets is enabled the receive and transmit'
            ' buffers for all sockets will be set to this limit. Must be a'
            ' value between 2048 and 262144, in 1024 byte increments. Default'
            ' of 8192 is recommended.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ControlPort': {
        'help': (
            ' PORT|unix:path|auto [flags]'
            ' If set, Tor will accept connections on this port and allow those'
            ' connections to control the Tor process using the Tor Control'
            ' Protocol (described in control-spec.txt in torspec). Note:'
            ' unless you also specify one or more of HashedControlPassword or'
            ' CookieAuthentication, setting this option will cause Tor to'
            ' allow any process on the local host to control it. (Setting both'
            ' authentication methods means either method is sufficient to'
            ' authenticate to Tor.) This option is required for many Tor'
            ' controllers; most use the value of 9051. If a unix domain socket'
            ' is used, you may quote the path using standard C escape'
            ' sequences. Set it to "auto" to have Tor pick a port for you.'
            ' (Default: 0)'
            '\n'
            ' Recognized flags are...'
            '\n'
            ' GroupWritable Unix domain sockets only: makes the socket get'
            ' created as group-writable.'
            '\n'
            ' WorldWritable Unix domain sockets only: makes the socket get'
            ' created as world-writable.'
            '\n'
            ' RelaxDirModeCheck Unix domain sockets only: Do not insist that'
            ' the directory that holds the socket be read-restricted.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ControlSocket': {
        'help': (
            ' Path'
            ' Like ControlPort, but listens on a Unix domain socket, rather'
            ' than a TCP socket.  0 disables ControlSocket (Unix and Unix-like'
            ' systems only.)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ControlSocketsGroupWritable': {
        'help': (
            ' 0|1'
            ' If this option is set to 0, don\'t allow the filesystem group to'
            ' read and write unix sockets (e.g. ControlSocket). If the option'
            ' is set to 1, make the control socket readable and writable by'
            ' the default GID. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HashedControlPassword': {
        'help': (
            ' hashed_password'
            ' Allow connections on the control port if they present the'
            ' password whose one-way hash is hashed_password. You can compute'
            ' the hash of a password by running "tor --hash-password'
            ' password". You can provide several acceptable passwords by using'
            ' more than one HashedControlPassword line.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'CookieAuthentication': {
        'help': (
            ' 0|1'
            ' If this option is set to 1, allow connections on the control'
            ' port when the connecting process knows the contents of a file'
            ' named "control_auth_cookie", which Tor will create in its data'
            ' directory. This authentication method should only be used on'
            ' systems with good filesystem security. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'CookieAuthFile': {
        'help': (
            ' Path'
            ' If set, this option overrides the default location and file name'
            ' for Tor\'s cookie file. (See CookieAuthentication above.)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'CookieAuthFileGroupReadable': {
        'help': (
            ' 0|1'
            ' If this option is set to 0, don\'t allow the filesystem group to'
            ' read the cookie file. If the option is set to 1, make the cookie'
            ' file readable by the default GID. [Making the file readable by'
            ' other groups is not yet implemented; let us know if you need'
            ' this for some reason.] (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ControlPortWriteToFile': {
        'help': (
            ' Path'
            ' If set, Tor writes the address and port of any control port it'
            ' opens to this address. Usable by controllers to learn the actual'
            ' control port when ControlPort is set to "auto".'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ControlPortFileGroupReadable': {
        'help': (
            ' 0|1'
            ' If this option is set to 0, don\'t allow the filesystem group to'
            ' read the control port file. If the option is set to 1, make the'
            ' control port file readable by the default GID. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'DataDirectory': {
        'help': (
            ' DIR'
            ' Store working data in DIR. Can not be changed while tor is'
            ' running. (Default: ~/.tor if your home directory is not /;'
            ' otherwise, /var/lib/tor. On Windows, the default is your'
            ' ApplicationData folder.)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'DataDirectoryGroupReadable': {
        'help': (
            ' 0|1'
            ' If this option is set to 0, don\'t allow the filesystem group to'
            ' read the DataDirectory. If the option is set to 1, make the'
            ' DataDirectory readable by the default GID. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'FallbackDir': {
        'help': (
            ' address:port orport=port id=fingerprint [weight=num]'
            ' [ipv6=address:orport] When we\'re unable to connect to any'
            ' directory cache for directory info (usually because we don\'t'
            ' know about any yet) we try a directory authority. Clients also'
            ' simultaneously try a FallbackDir, to avoid hangs on client'
            ' startup if a directory authority is down. Clients retry'
            ' FallbackDirs more often than directory authorities, to reduce'
            ' the load on the directory authorities. By default, the directory'
            ' authorities are also FallbackDirs. Specifying a FallbackDir'
            ' replaces Tor\'s default hard-coded FallbackDirs (if any). (See'
            ' the DirAuthority entry for an explanation of each flag.)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'UseDefaultFallbackDirs': {
        'help': (
            ' 0|1'
            ' Use Tor\'s default hard-coded FallbackDirs (if any). (When a'
            ' FallbackDir line is present, it replaces the hard-coded'
            ' FallbackDirs, regardless of the value of'
            ' UseDefaultFallbackDirs.) (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'DirAuthority': {
        'help': (
            ' [nickname] [flags] address:port fingerprint'
            ' Use a nonstandard authoritative directory server at the provided'
            ' address and port, with the specified key fingerprint. This'
            ' option can be repeated many times, for multiple authoritative'
            ' directory servers. Flags are separated by spaces, and determine'
            ' what kind of an authority this directory is. By default, an'
            ' authority is not authoritative for any directory style or'
            ' version unless an appropriate flag is given. Tor will use this'
            ' authority as a bridge authoritative directory if the "bridge"'
            ' flag is set. If a flag "orport=port" is given, Tor will use the'
            ' given port when opening encrypted tunnels to the dirserver. If a'
            ' flag "weight=num" is given, then the directory server is chosen'
            ' randomly with probability proportional to that weight (default'
            ' 1.0). If a flag "v3ident=fp" is given, the dirserver is a v3'
            ' directory authority whose v3 long-term signing key has the'
            ' fingerprint fp. Lastly, if an "ipv6=address:orport" flag is'
            ' present, then the directory authority is listening for IPv6'
            ' connections on the indicated IPv6 address and OR Port.'
            '\n'
            ' Tor will contact the authority at address:port (the DirPort) to'
            ' download directory documents. If an IPv6 address is supplied,'
            ' Tor will also download directory documents at the IPv6 address'
            ' on the DirPort.'
            '\n'
            ' If no DirAuthority line is given, Tor will use the default'
            ' directory authorities. NOTE: this option is intended for setting'
            ' up a private Tor network with its own directory authorities. If'
            ' you use it, you will be distinguishable from other users,'
            ' because you won\'t believe the same authorities they do.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'DirAuthorityFallbackRate': {
        'help': (
            ' NUM'
            ' When configured to use both directory authorities and fallback'
            ' directories, the directory authorities also work as fallbacks.'
            ' They are chosen with their regular weights, multiplied by this'
            ' number, which should be 1.0 or less. (Default: 1.0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'AlternateDirAuthority': {
        'help': (
            ' [nickname] [flags] address:port fingerprint'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'AlternateBridgeAuthority': {
        'help': (
            ' [nickname] [flags] address:port  fingerprint'
            ' These options behave as DirAuthority, but they replace fewer of'
            ' the default directory authorities. Using AlternateDirAuthority'
            ' replaces the default Tor directory authorities, but leaves the'
            ' default bridge authorities in place. Similarly,'
            ' AlternateBridgeAuthority replaces the default bridge authority,'
            ' but leaves the directory authorities alone.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'DisableAllSwap': {
        'help': (
            ' 0|1'
            ' If set to 1, Tor will attempt to lock all current and future'
            ' memory pages, so that memory cannot be paged out. Windows, OS X'
            ' and Solaris are currently not supported. We believe that this'
            ' feature works on modern Gnu/Linux distributions, and that it'
            ' should work on *BSD systems (untested). This option requires'
            ' that you start your Tor as root, and you should use the User'
            ' option to properly reduce Tor\'s privileges. Can not be changed'
            ' while tor is running. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'DisableDebuggerAttachment': {
        'help': (
            ' 0|1'
            ' If set to 1, Tor will attempt to prevent basic debugging'
            ' attachment attempts by other processes. This may also keep Tor'
            ' from generating core files if it crashes. It has no impact for'
            ' users who wish to attach if they have CAP_SYS_PTRACE or if they'
            ' are root. We believe that this feature works on modern Gnu/Linux'
            ' distributions, and that it may also work on *BSD systems'
            ' (untested). Some modern Gnu/Linux systems such as Ubuntu have'
            ' the kernel.yama.ptrace_scope sysctl and by default enable it as'
            ' an attempt to limit the PTRACE scope for all user processes by'
            ' default. This feature will attempt to limit the PTRACE scope for'
            ' Tor specifically - it will not attempt to alter the system wide'
            ' ptrace scope as it may not even exist. If you wish to attach to'
            ' Tor with a debugger such as gdb or strace you will want to set'
            ' this to 0 for the duration of your debugging. Normal users'
            ' should leave it on. Disabling this option while Tor is running'
            ' is prohibited. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'FetchDirInfoEarly': {
        'help': (
            ' 0|1'
            ' If set to 1, Tor will always fetch directory information like'
            ' other directory caches, even if you don\'t meet the normal'
            ' criteria for fetching early. Normal users should leave it off.'
            ' (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'FetchDirInfoExtraEarly': {
        'help': (
            ' 0|1'
            ' If set to 1, Tor will fetch directory information before other'
            ' directory caches. It will attempt to download directory'
            ' information closer to the start of the consensus period. Normal'
            ' users should leave it off. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'FetchHidServDescriptors': {
        'help': (
            ' 0|1'
            ' If set to 0, Tor will never fetch any hidden service descriptors'
            ' from the rendezvous directories. This option is only useful if'
            ' you\'re using a Tor controller that handles hidden service'
            ' fetches for you. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'FetchServerDescriptors': {
        'help': (
            ' 0|1'
            ' If set to 0, Tor will never fetch any network status summaries'
            ' or server descriptors from the directory servers. This option is'
            ' only useful if you\'re using a Tor controller that handles'
            ' directory fetches for you. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'FetchUselessDescriptors': {
        'help': (
            ' 0|1'
            ' If set to 1, Tor will fetch every consensus flavor, descriptor,'
            ' and certificate that it hears about. Otherwise, it will avoid'
            ' fetching useless descriptors: flavors that it is not using to'
            ' build circuits, and authority certificates it does not trust.'
            ' This option is useful if you\'re using a tor client with an'
            ' external parser that uses a full consensus. This option fetches'
            ' all documents, DirCache fetches and serves all documents.'
            ' (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HTTPProxy': {
        'help': (
            ' host[:port]'
            ' Tor will make all its directory requests through this host:port'
            ' (or host:80 if port is not specified), rather than connecting'
            ' directly to any directory servers.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HTTPProxyAuthenticator': {
        'help': (
            ' username:password'
            ' If defined, Tor will use this username:password for Basic HTTP'
            ' proxy authentication, as in RFC 2617. This is currently the only'
            ' form of HTTP proxy authentication that Tor supports; feel free'
            ' to submit a patch if you want it to support others.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HTTPSProxy': {
        'help': (
            ' host[:port]'
            ' Tor will make all its OR (SSL) connections through this'
            ' host:port (or host:443 if port is not specified), via HTTP'
            ' CONNECT rather than connecting directly to servers. You may want'
            ' to set FascistFirewall to restrict the set of ports you might'
            ' try to connect to, if your HTTPS proxy only allows connecting to'
            ' certain ports.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HTTPSProxyAuthenticator': {
        'help': (
            ' username:password'
            ' If defined, Tor will use this username:password for Basic HTTPS'
            ' proxy authentication, as in RFC 2617. This is currently the only'
            ' form of HTTPS proxy authentication that Tor supports; feel free'
            ' to submit a patch if you want it to support others.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'Sandbox': {
        'help': (
            ' 0|1'
            ' If set to 1, Tor will run securely through the use of a syscall'
            ' sandbox. Otherwise the sandbox will be disabled. The option is'
            ' currently an experimental feature. Can not be changed while tor'
            ' is running.'
            '\n'
            ' When the Sandbox is 1, the following options can not be changed'
            ' when tor is running: Address ConnLimit CookieAuthFile'
            ' DirPortFrontPage ExtORPortCookieAuthFile Logs'
            ' ServerDNSResolvConfFile Tor must remain in client or server mode'
            ' (some changes to ClientOnly and ORPort are not allowed).'
            ' (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'Socks4Proxy': {
        'help': (
            ' host[:port]'
            ' Tor will make all OR connections through the SOCKS 4 proxy at'
            ' host:port (or host:1080 if port is not specified).'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'Socks5Proxy': {
        'help': (
            ' host[:port]'
            ' Tor will make all OR connections through the SOCKS 5 proxy at'
            ' host:port (or host:1080 if port is not specified).'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'Socks5ProxyUsername': {
        'help': (
            ' username'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'Socks5ProxyPassword': {
        'help': (
            ' password'
            ' If defined, authenticate to the SOCKS 5 server using username'
            ' and password in accordance to RFC 1929. Both username and'
            ' password must be between 1 and 255 characters.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'SocksSocketsGroupWritable': {
        'help': (
            ' 0|1'
            ' If this option is set to 0, don\'t allow the filesystem group to'
            ' read and write unix sockets (e.g. SocksSocket). If the option is'
            ' set to 1, make the SocksSocket socket readable and writable by'
            ' the default GID. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'KeepalivePeriod': {
        'help': (
            ' NUM'
            ' To keep firewalls from expiring connections, send a padding'
            ' keepalive cell every NUM seconds on open connections that are in'
            ' use. If the connection has no open circuits, it will instead be'
            ' closed after NUM seconds of idleness. (Default: 5 minutes)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'Log': {
        'help': (
            ' minSeverity[-maxSeverity] stderr|stdout|syslog'
            ' Send all messages between minSeverity and maxSeverity to the'
            ' standard output stream, the standard error stream, or to the'
            ' system log. (The "syslog" value is only supported on Unix.)'
            ' Recognized severity levels are debug, info, notice, warn, and'
            ' err. We advise using "notice" in most cases, since anything more'
            ' verbose may provide sensitive information to an attacker who'
            ' obtains the logs. If only one severity level is given, all'
            ' messages of that level or higher will be sent to the listed'
            ' destination.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'Log': {
        'help': (
            ' minSeverity[-maxSeverity] file FILENAME'
            ' As above, but send log messages to the listed filename. The'
            ' "Log" option may appear more than once in a configuration file.'
            ' Messages are sent to all the logs that match their severity'
            ' level.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'Log': {
        'help': (
            ' [domain,...]minSeverity[-maxSeverity] ... file FILENAME'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'Log': {
        'help': (
            ' [domain,...]minSeverity[-maxSeverity] ... stderr|stdout|syslog'
            ' As above, but select messages by range of log severity and by a'
            ' set of "logging domains". Each logging domain corresponds to an'
            ' area of functionality inside Tor. You can specify any number of'
            ' severity ranges for a single log statement, each of them'
            ' prefixed by a comma-separated list of logging domains. You can'
            ' prefix a domain with ~ to indicate negation, and use * to'
            ' indicate "all domains". If you specify a severity range without'
            ' a list of domains, it matches all domains.'
            '\n'
            ' This is an advanced feature which is most useful for debugging'
            ' one or two of Tor\'s subsystems at a time.'
            '\n'
            ' The currently recognized domains are: general, crypto, net,'
            ' config, fs, protocol, mm, http, app, control, circ, rend, bug,'
            ' dir, dirserv, or, edge, acct, hist, and handshake. Domain names'
            ' are case-insensitive.'
            '\n'
            ' For example, "Log [handshake]debug [~net,~mm]info notice stdout"'
            ' sends to stdout: all handshake messages of any severity, all'
            ' info-and-higher messages from domains other than networking and'
            ' memory management, and all messages of severity notice or'
            ' higher.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'LogMessageDomains': {
        'help': (
            ' 0|1'
            ' If 1, Tor includes message domains with each log message. Every'
            ' log message currently has at least one domain; most currently'
            ' have exactly one. This doesn\'t affect controller log messages.'
            ' (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'MaxUnparseableDescSizeToLog': {
        'help': (
            ' N bytes|KBytes|MBytes|GBytes|TBytes'
            ' Unparseable descriptors (e.g. for votes, consensuses, routers)'
            ' are logged in separate files by hash, up to the specified size'
            ' in total. Note that only files logged during the lifetime of'
            ' this Tor process count toward the total; this is intended to be'
            ' used to debug problems without opening live servers to resource'
            ' exhaustion attacks. (Default: 10 MB)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'OutboundBindAddress': {
        'help': (
            ' IP'
            ' Make all outbound connections originate from the IP address'
            ' specified. This is only useful when you have multiple network'
            ' interfaces, and you want all of Tor\'s outgoing connections to'
            ' use a single one. This option may be used twice, once with an'
            ' IPv4 address and once with an IPv6 address. This setting will be'
            ' ignored for connections to the loopback addresses (127.0.0.0/8'
            ' and ::1).'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'OutboundBindAddressOR': {
        'help': (
            ' IP'
            ' Make all outbound non-exit (relay and other) connections'
            ' originate from the IP address specified. This option overrides'
            ' OutboundBindAddress for the same IP version. This option may be'
            ' used twice, once with an IPv4 address and once with an IPv6'
            ' address. This setting will be ignored for connections to the'
            ' loopback addresses (127.0.0.0/8 and ::1).'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'OutboundBindAddressExit': {
        'help': (
            ' IP'
            ' Make all outbound exit connections originate from the IP address'
            ' specified. This option overrides OutboundBindAddress for the'
            ' same IP version. This option may be used twice, once with an'
            ' IPv4 address and once with an IPv6 address. This setting will be'
            ' ignored for connections to the loopback addresses (127.0.0.0/8'
            ' and ::1).'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PidFile': {
        'help': (
            ' FILE'
            ' On startup, write our PID to FILE. On clean shutdown, remove'
            ' FILE. Can not be changed while tor is running.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ProtocolWarnings': {
        'help': (
            ' 0|1'
            ' If 1, Tor will log with severity \'warn\' various cases of other'
            ' parties not following the Tor specification. Otherwise, they are'
            ' logged with severity \'info\'. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'RunAsDaemon': {
        'help': (
            ' 0|1'
            ' If 1, Tor forks and daemonizes to the background. This option'
            ' has no effect on Windows; instead you should use the --service'
            ' command-line option. Can not be changed while tor is running.'
            ' (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'LogTimeGranularity': {
        'help': (
            ' NUM'
            ' Set the resolution of timestamps in Tor\'s logs to NUM'
            ' milliseconds. NUM must be positive and either a divisor or a'
            ' multiple of 1 second. Note that this option only controls the'
            ' granularity written by Tor to a file or console log. Tor does'
            ' not (for example) "batch up" log messages to affect times logged'
            ' by a controller, times attached to syslog messages, or the mtime'
            ' fields on log files. (Default: 1 second)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'TruncateLogFile': {
        'help': (
            ' 0|1'
            ' If 1, Tor will overwrite logs at startup and in response to a'
            ' HUP signal, instead of appending to them. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'SyslogIdentityTag': {
        'help': (
            ' tag'
            ' When logging to syslog, adds a tag to the syslog identity such'
            ' that log entries are marked with "Tor-tag". Can not be changed'
            ' while tor is running. (Default: none)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'SafeLogging': {
        'help': (
            ' 0|1|relay'
            ' Tor can scrub potentially sensitive strings from log messages'
            ' (e.g. addresses) by replacing them with the string [scrubbed].'
            ' This way logs can still be useful, but they don\'t leave behind'
            ' personally identifying information about what sites a user might'
            ' have visited.'
            '\n'
            ' If this option is set to 0, Tor will not perform any scrubbing,'
            ' if it is set to 1, all potentially sensitive strings are'
            ' replaced. If it is set to relay, all log messages generated when'
            ' acting as a relay are sanitized, but all messages generated when'
            ' acting as a client are not. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'User': {
        'help': (
            ' Username'
            ' On startup, setuid to this user and setgid to their primary'
            ' group. Can not be changed while tor is running.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'KeepBindCapabilities': {
        'help': (
            ' 0|1|auto'
            ' On Linux, when we are started as root and we switch our identity'
            ' using the User option, the KeepBindCapabilities option tells us'
            ' whether to try to retain our ability to bind to low ports. If'
            ' this value is 1, we try to keep the capability; if it is 0 we do'
            ' not; and if it is auto, we keep the capability only if we are'
            ' configured to listen on a low port. Can not be changed while tor'
            ' is running. (Default: auto.)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HardwareAccel': {
        'help': (
            ' 0|1'
            ' If non-zero, try to use built-in (static) crypto hardware'
            ' acceleration when available. Can not be changed while tor is'
            ' running. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'AccelName': {
        'help': (
            ' NAME'
            ' When using OpenSSL hardware crypto acceleration attempt to load'
            ' the dynamic engine of this name. This must be used for any'
            ' dynamic hardware engine. Names can be verified with the openssl'
            ' engine command. Can not be changed while tor is running.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'AccelDir': {
        'help': (
            ' DIR'
            ' Specify this option if using dynamic hardware acceleration and'
            ' the engine implementation library resides somewhere other than'
            ' the OpenSSL default. Can not be changed while tor is running.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'AvoidDiskWrites': {
        'help': (
            ' 0|1'
            ' If non-zero, try to write to disk less frequently than we would'
            ' otherwise. This is useful when running on flash memory or other'
            ' media that support only a limited number of writes. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'CircuitPriorityHalflife': {
        'help': (
            ' NUM1'
            ' If this value is set, we override the default algorithm for'
            ' choosing which circuit\'s cell to deliver or relay next. When the'
            ' value is 0, we round-robin between the active circuits on a'
            ' connection, delivering one cell from each in turn. When the'
            ' value is positive, we prefer delivering cells from whichever'
            ' connection has the lowest weighted cell count, where cells are'
            ' weighted exponentially according to the supplied'
            ' CircuitPriorityHalflife value (in seconds). If this option is'
            ' not set at all, we use the behavior recommended in the current'
            ' consensus networkstatus. This is an advanced option; you'
            ' generally shouldn\'t have to mess with it. (Default: not set)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'CountPrivateBandwidth': {
        'help': (
            ' 0|1'
            ' If this option is set, then Tor\'s rate-limiting applies not only'
            ' to remote connections, but also to connections to private'
            ' addresses like 127.0.0.1 or 10.0.0.1. This is mostly useful for'
            ' debugging rate-limiting. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ExtendByEd25519ID': {
        'help': (
            ' 0|1|auto'
            ' If this option is set to 1, we always try to include a relay\'s'
            ' Ed25519 ID when telling the proceeding relay in a circuit to'
            ' extend to it. If this option is set to 0, we never include'
            ' Ed25519 IDs when extending circuits. If the option is set to'
            ' "default", we obey a parameter in the consensus document.'
            ' (Default: auto)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'Bridge': {
        'help': (
            ' [transport] IP:ORPort [fingerprint]'
            ' When set along with UseBridges, instructs Tor to use the relay'
            ' at "IP:ORPort" as a "bridge" relaying into the Tor network. If'
            ' "fingerprint" is provided (using the same format as for'
            ' DirAuthority), we will verify that the relay running at that'
            ' location has the right fingerprint. We also use fingerprint to'
            ' look up the bridge descriptor at the bridge authority, if it\'s'
            ' provided and if UpdateBridgesFromAuthority is set too.'
            '\n'
            ' If "transport" is provided, it must match a'
            ' ClientTransportPlugin line. We then use that pluggable'
            ' transport\'s proxy to transfer data to the bridge, rather than'
            ' connecting to the bridge directly. Some transports use a'
            ' transport-specific method to work out the remote address to'
            ' connect to. These transports typically ignore the "IP:ORPort"'
            ' specified in the bridge line.'
            '\n'
            ' Tor passes any "key=val" settings to the pluggable transport'
            ' proxy as per-connection arguments when connecting to the bridge.'
            ' Consult the documentation of the pluggable transport for details'
            ' of what arguments it supports.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'LearnCircuitBuildTimeout': {
        'help': (
            ' 0|1'
            ' If 0, CircuitBuildTimeout adaptive learning is disabled.'
            ' (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'CircuitBuildTimeout': {
        'help': (
            ' NUM'
            ' Try for at most NUM seconds when building circuits. If the'
            ' circuit isn\'t open in that time, give up on it. If'
            ' LearnCircuitBuildTimeout is 1, this value serves as the initial'
            ' value to use before a timeout is learned. If'
            ' LearnCircuitBuildTimeout is 0, this value is the only value'
            ' used. (Default: 60 seconds)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'CircuitsAvailableTimeout': {
        'help': (
            ' NUM'
            ' Tor will attempt to keep at least one open, unused circuit'
            ' available for this amount of time. This option governs how long'
            ' idle circuits are kept open, as well as the amount of time Tor'
            ' will keep a circuit open to each of the recently used ports.'
            ' This way when the Tor client is entirely idle, it can expire all'
            ' of its circuits, and then expire its TLS connections. Note that'
            ' the actual timeout value is uniformly randomized from the'
            ' specified value to twice that amount. (Default: 30 minutes; Max:'
            ' 24 hours)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'CircuitStreamTimeout': {
        'help': (
            ' NUM'
            ' If non-zero, this option overrides our internal timeout schedule'
            ' for how many seconds until we detach a stream from a circuit and'
            ' try a new circuit. If your network is particularly slow, you'
            ' might want to set this to a number like 60. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ClientOnly': {
        'help': (
            ' 0|1'
            ' If set to 1, Tor will not run as a relay or serve directory'
            ' requests, even if the ORPort, ExtORPort, or DirPort options are'
            ' set. (This config option is mostly unnecessary: we added it back'
            ' when we were considering having Tor clients auto-promote'
            ' themselves to being relays if they were stable and fast enough.'
            ' The current behavior is simply that Tor is a client unless'
            ' ORPort, ExtORPort, or DirPort are configured.) (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ConnectionPadding': {
        'help': (
            ' 0|1|auto'
            ' This option governs Tor\'s use of padding to defend against some'
            ' forms of traffic analysis. If it is set to auto, Tor will send'
            ' padding only if both the client and the relay support it. If it'
            ' is set to 0, Tor will not send any padding cells. If it is set'
            ' to 1, Tor will still send padding for client connections'
            ' regardless of relay support. Only clients may set this option.'
            ' This option should be offered via the UI to mobile users for use'
            ' where bandwidth may be expensive. (Default: auto)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ReducedConnectionPadding': {
        'help': (
            ' 0|1'
            ' If set to 1, Tor will not not hold OR connections open for very'
            ' long, and will send less padding on these connections. Only'
            ' clients may set this option. This option should be offered via'
            ' the UI to mobile users for use where bandwidth may be expensive.'
            ' (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ExcludeNodes': {
        'help': (
            ' node,node,...'
            ' A list of identity fingerprints, country codes, and address'
            ' patterns of nodes to avoid when building a circuit. Country'
            ' codes are 2-letter ISO3166 codes, and must be wrapped in braces;'
            ' fingerprints may be preceded by a dollar sign. (Example:'
            ' ExcludeNodes ABCD1234CDEF5678ABCD1234CDEF5678ABCD1234, {cc},'
            ' 255.254.0.0/8)'
            '\n'
            ' By default, this option is treated as a preference that Tor is'
            ' allowed to override in order to keep working. For example, if'
            ' you try to connect to a hidden service, but you have excluded'
            ' all of the hidden service\'s introduction points, Tor will'
            ' connect to one of them anyway. If you do not want this behavior,'
            ' set the StrictNodes option (documented below).'
            '\n'
            ' Note also that if you are a relay, this (and the other node'
            ' selection options below) only affects your own circuits that Tor'
            ' builds for you. Clients can still build circuits through you to'
            ' any node. Controllers can tell Tor to build circuits through any'
            ' node.'
            '\n'
            ' Country codes are case-insensitive. The code "{??}" refers to'
            ' nodes whose country can\'t be identified. No country code,'
            ' including {??}, works if no GeoIPFile can be loaded. See also'
            ' the GeoIPExcludeUnknown option below.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ExcludeExitNodes': {
        'help': (
            ' node,node,...'
            ' A list of identity fingerprints, country codes, and address'
            ' patterns of nodes to never use when picking an exit node---that'
            ' is, a node that delivers traffic for you outside the Tor'
            ' network. Note that any node listed in ExcludeNodes is'
            ' automatically considered to be part of this list too. See the'
            ' ExcludeNodes option for more information on how to specify'
            ' nodes. See also the caveats on the "ExitNodes" option below.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'GeoIPExcludeUnknown': {
        'help': (
            ' 0|1|auto'
            ' If this option is set to auto, then whenever any country code is'
            ' set in ExcludeNodes or ExcludeExitNodes, all nodes with unknown'
            ' country ({??} and possibly {A1}) are treated as excluded as'
            ' well. If this option is set to 1, then all unknown countries are'
            ' treated as excluded in ExcludeNodes and ExcludeExitNodes. This'
            ' option has no effect when a GeoIP file isn\'t configured or can\'t'
            ' be found. (Default: auto)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ExitNodes': {
        'help': (
            ' node,node,...'
            ' A list of identity fingerprints, country codes, and address'
            ' patterns of nodes to use as exit node---that is, a node that'
            ' delivers traffic for you outside the Tor network. See the'
            ' ExcludeNodes option for more information on how to specify'
            ' nodes.'
            '\n'
            ' Note that if you list too few nodes here, or if you exclude too'
            ' many exit nodes with ExcludeExitNodes, you can degrade'
            ' functionality. For example, if none of the exits you list allows'
            ' traffic on port 80 or 443, you won\'t be able to browse the web.'
            '\n'
            ' Note also that not every circuit is used to deliver traffic'
            ' outside of the Tor network. It is normal to see non-exit'
            ' circuits (such as those used to connect to hidden services,'
            ' those that do directory fetches, those used for relay'
            ' reachability self-tests, and so on) that end at a non-exit node.'
            ' To keep a node from being used entirely, see ExcludeNodes and'
            ' StrictNodes.'
            '\n'
            ' The ExcludeNodes option overrides this option: any node listed'
            ' in both ExitNodes and ExcludeNodes is treated as excluded.'
            '\n'
            ' The .exit address notation, if enabled via AllowDotExit,'
            ' overrides this option.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'EntryNodes': {
        'help': (
            ' node,node,...'
            ' A list of identity fingerprints and country codes of nodes to'
            ' use for the first hop in your normal circuits. Normal circuits'
            ' include all circuits except for direct connections to directory'
            ' servers. The Bridge option overrides this option; if you have'
            ' configured bridges and UseBridges is 1, the Bridges are used as'
            ' your entry nodes.'
            '\n'
            ' The ExcludeNodes option overrides this option: any node listed'
            ' in both EntryNodes and ExcludeNodes is treated as excluded. See'
            ' the ExcludeNodes option for more information on how to specify'
            ' nodes.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'StrictNodes': {
        'help': (
            ' 0|1'
            ' If StrictNodes is set to 1, Tor will treat solely the'
            ' ExcludeNodes option as a requirement to follow for all the'
            ' circuits you generate, even if doing so will break functionality'
            ' for you (StrictNodes applies to neither ExcludeExitNodes nor to'
            ' ExitNodes). If StrictNodes is set to 0, Tor will still try to'
            ' avoid nodes in the ExcludeNodes list, but it will err on the'
            ' side of avoiding unexpected errors. Specifically, StrictNodes 0'
            ' tells Tor that it is okay to use an excluded node when it is'
            ' necessary to perform relay reachability self-tests, connect to a'
            ' hidden service, provide a hidden service to a client, fulfill a'
            ' .exit request, upload directory information, or download'
            ' directory information. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'FascistFirewall': {
        'help': (
            ' 0|1'
            ' If 1, Tor will only create outgoing connections to ORs running'
            ' on ports that your firewall allows (defaults to 80 and 443; see'
            ' FirewallPorts). This will allow you to run Tor as a client'
            ' behind a firewall with restrictive policies, but will not allow'
            ' you to run as a server behind such a firewall. If you prefer'
            ' more fine-grained control, use ReachableAddresses instead.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'FirewallPorts': {
        'help': (
            ' PORTS'
            ' A list of ports that your firewall allows you to connect to.'
            ' Only used when FascistFirewall is set. This option is'
            ' deprecated; use ReachableAddresses instead. (Default: 80, 443)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ReachableAddresses': {
        'help': (
            ' ADDR[/MASK][:PORT]...'
            ' A comma-separated list of IP addresses and ports that your'
            ' firewall allows you to connect to. The format is as for the'
            ' addresses in ExitPolicy, except that "accept" is understood'
            ' unless "reject" is explicitly provided. For example,'
            ' \'ReachableAddresses 99.0.0.0/8, reject 18.0.0.0/8:80, accept'
            ' *:80\' means that your firewall allows connections to everything'
            ' inside net 99, rejects port 80 connections to net 18, and'
            ' accepts connections to port 80 otherwise. (Default: \'accept'
            ' *:*\'.)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ReachableDirAddresses': {
        'help': (
            ' ADDR[/MASK][:PORT]...'
            ' Like ReachableAddresses, a list of addresses and ports. Tor will'
            ' obey these restrictions when fetching directory information,'
            ' using standard HTTP GET requests. If not set explicitly then the'
            ' value of ReachableAddresses is used. If HTTPProxy is set then'
            ' these connections will go through that proxy.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ReachableORAddresses': {
        'help': (
            ' ADDR[/MASK][:PORT]...'
            ' Like ReachableAddresses, a list of addresses and ports. Tor will'
            ' obey these restrictions when connecting to Onion Routers, using'
            ' TLS/SSL. If not set explicitly then the value of'
            ' ReachableAddresses is used. If HTTPSProxy is set then these'
            ' connections will go through that proxy.'
            '\n'
            ' The separation between ReachableORAddresses and'
            ' ReachableDirAddresses is only interesting when you are'
            ' connecting through proxies (see HTTPProxy and HTTPSProxy). Most'
            ' proxies limit TLS connections (which Tor uses to connect to'
            ' Onion Routers) to port 443, and some limit HTTP GET requests'
            ' (which Tor uses for fetching directory information) to port 80.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HidServAuth': {
        'help': (
            ' onion-address auth-cookie [service-name]'
            ' Client authorization for a hidden service. Valid onion addresses'
            ' contain 16 characters in a-z2-7 plus ".onion", and valid auth'
            ' cookies contain 22 characters in A-Za-z0-9+/. The service name'
            ' is only used for internal purposes, e.g., for Tor controllers.'
            ' This option may be used multiple times for different hidden'
            ' services. If a hidden service uses authorization and this option'
            ' is not set, the hidden service is not accessible. Hidden'
            ' services can be configured to require authorization using the'
            ' HiddenServiceAuthorizeClient option.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'LongLivedPorts': {
        'help': (
            ' PORTS'
            ' A list of ports for services that tend to have long-running connections (e.g. chat and interactive shells). Circuits for streams that use these ports will contain only high-uptime nodes, to'
            ' reduce the chance that a node will go down before the stream is finished. Note that the list is also honored for circuits (both client and service side) involving hidden services whose virtual'
            ' port is in this list. (Default: 21, 22, 706, 1863, 5050, 5190, 5222, 5223, 6523, 6667, 6697, 8300)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'MapAddress': {
        'help': (
            ' address newaddress'
            ' When a request for address arrives to Tor, it will transform to'
            ' newaddress before processing it. For example, if you always want'
            ' connections to www.example.com to exit via torserver (where'
            ' torserver is the fingerprint of the server), use "MapAddress'
            ' www.example.com www.example.com.torserver.exit". If the value is'
            ' prefixed with a "*.", matches an entire domain. For example, if'
            ' you always want connections to example.com and any if its'
            ' subdomains to exit via torserver (where torserver is the'
            ' fingerprint of the server), use "MapAddress *.example.com'
            ' *.example.com.torserver.exit". (Note the leading "*." in each'
            ' part of the directive.) You can also redirect all subdomains of'
            ' a domain to a single address. For example, "MapAddress'
            ' *.example.com www.example.com".'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'NewCircuitPeriod': {
        'help': (
            ' NUM'
            ' Every NUM seconds consider whether to build a new circuit.'
            ' (Default: 30 seconds)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'MaxCircuitDirtiness': {
        'help': (
            ' NUM'
            ' Feel free to reuse a circuit that was first used at most NUM'
            ' seconds ago, but never attach a new stream to a circuit that is'
            ' too old. For hidden services, this applies to the last time a'
            ' circuit was used, not the first. Circuits with streams'
            ' constructed with SOCKS authentication via SocksPorts that have'
            ' KeepAliveIsolateSOCKSAuth also remain alive for'
            ' MaxCircuitDirtiness seconds after carrying the last such stream.'
            ' (Default: 10 minutes)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'MaxClientCircuitsPending': {
        'help': (
            ' NUM'
            ' Do not allow more than NUM circuits to be pending at a time for'
            ' handling client streams. A circuit is pending if we have begun'
            ' constructing it, but it has not yet been completely constructed.'
            ' (Default: 32)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'NodeFamily': {
        'help': (
            ' node,node,...'
            ' The Tor servers, defined by their identity fingerprints,'
            ' constitute a "family" of similar or co-administered servers, so'
            ' never use any two of them in the same circuit. Defining a'
            ' NodeFamily is only needed when a server doesn\'t list the family'
            ' itself (with MyFamily). This option can be used multiple times;'
            ' each instance defines a separate family. In addition to nodes,'
            ' you can also list IP address and ranges and country codes in'
            ' {curly braces}. See the ExcludeNodes option for more information'
            ' on how to specify nodes.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'EnforceDistinctSubnets': {
        'help': (
            ' 0|1'
            ' If 1, Tor will not put two servers whose IP addresses are "too'
            ' close" on the same circuit. Currently, two addresses are "too'
            ' close" if they lie in the same /16 range. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'SOCKSPort': {
        'help': (
            ' [address:]port|unix:path|auto [flags] [isolation flags]'
            ' Open this port to listen for connections from SOCKS-speaking'
            ' applications. Set this to 0 if you don\'t want to allow'
            ' application connections via SOCKS. Set it to "auto" to have Tor'
            ' pick a port for you. This directive can be specified multiple'
            ' times to bind to multiple addresses/ports. If a unix domain'
            ' socket is used, you may quote the path using standard C escape'
            ' sequences. (Default: 9050)'
            '\n'
            ' NOTE: Although this option allows you to specify an IP address'
            ' other than localhost, you should do so only with extreme'
            ' caution. The SOCKS protocol is unencrypted and (as we use it)'
            ' unauthenticated, so exposing it in this way could leak your'
            ' information to anybody watching your network, and allow anybody'
            ' to use your computer as an open proxy.'
            '\n'
            ' The isolation flags arguments give Tor rules for which streams'
            ' received on this SocksPort are allowed to share circuits with'
            ' one another. Recognized isolation flags are:'
            '\n'
            ' IsolateClientAddr Don\'t share circuits with streams from a'
            ' different client address. (On by default and strongly'
            ' recommended when supported; you can disable it with'
            ' NoIsolateClientAddr. Unsupported and force-disabled when using'
            ' Unix domain sockets.)'
            '\n'
            ' IsolateSOCKSAuth Don\'t share circuits with streams for which'
            ' different SOCKS authentication was provided. (On by default; you'
            ' can disable it with NoIsolateSOCKSAuth.)'
            '\n'
            ' IsolateClientProtocol Don\'t share circuits with streams using a'
            ' different protocol. (SOCKS 4, SOCKS 5, TransPort connections,'
            ' NATDPort connections, and DNSPort requests are all considered to'
            ' be different protocols.)'
            '\n'
            ' IsolateDestPort Don\'t share circuits with streams targeting a'
            ' different destination port.'
            '\n'
            ' IsolateDestAddr Don\'t share circuits with streams targeting a'
            ' different destination address.'
            '\n'
            ' KeepAliveIsolateSOCKSAuth If IsolateSOCKSAuth is enabled, keep'
            ' alive circuits while they have at least one stream with SOCKS'
            ' authentication active. After such a circuit is idle for more'
            ' than MaxCircuitDirtiness seconds, it can be closed.'
            '\n'
            ' SessionGroup=INT If no other isolation rules would prevent it,'
            ' allow streams on this port to share circuits with streams from'
            ' every other port with the same session group. (By default,'
            ' streams received on different SocksPorts, TransPorts, etc are'
            ' always isolated from one another. This option overrides that'
            ' behavior.)'
            '\n'
            ' Other recognized flags for a SocksPort are:'
            '\n'
            ' NoIPv4Traffic Tell exits to not connect to IPv4 addresses in'
            ' response to SOCKS requests on this connection.'
            '\n'
            ' IPv6Traffic Tell exits to allow IPv6 addresses in response to'
            ' SOCKS requests on this connection, so long as SOCKS5 is in use.'
            ' (SOCKS4 can\'t handle IPv6.)'
            '\n'
            ' PreferIPv6 Tells exits that, if a host has both an IPv4 and an'
            ' IPv6 address, we would prefer to connect to it via IPv6. (IPv4'
            ' is the default.)'
            '\n'
            ' NoDNSRequest Do not ask exits to resolve DNS addresses in SOCKS5'
            ' requests. Tor will connect to IPv4 addresses, IPv6 addresses (if'
            ' IPv6Traffic is set) and .onion addresses.'
            '\n'
            ' NoOnionTraffic Do not connect to .onion addresses in SOCKS5'
            ' requests.'
            '\n'
            ' OnionTrafficOnly Tell the tor client to only connect to .onion'
            ' addresses in response to SOCKS5 requests on this connection.'
            ' This is equivalent to NoDNSRequest, NoIPv4Traffic,'
            ' NoIPv6Traffic. The corresponding NoOnionTrafficOnly flag is not'
            ' supported.'
            '\n'
            ' CacheIPv4DNS Tells the client to remember IPv4 DNS answers we'
            ' receive from exit nodes via this connection. (On by default.)'
            '\n'
            ' CacheIPv6DNS Tells the client to remember IPv6 DNS answers we'
            ' receive from exit nodes via this connection.'
            '\n'
            ' GroupWritable Unix domain sockets only: makes the socket get'
            ' created as group-writable.'
            '\n'
            ' WorldWritable Unix domain sockets only: makes the socket get'
            ' created as world-writable.'
            '\n'
            ' CacheDNS Tells the client to remember all DNS answers we receive'
            ' from exit nodes via this connection.'
            '\n'
            ' UseIPv4Cache Tells the client to use any cached IPv4 DNS answers'
            ' we have when making requests via this connection. (NOTE: This'
            ' option, along UseIPv6Cache and UseDNSCache, can harm your'
            ' anonymity, and probably won\'t help performance as much as you'
            ' might expect. Use with care!)'
            '\n'
            ' UseIPv6Cache Tells the client to use any cached IPv6 DNS answers'
            ' we have when making requests via this connection.'
            '\n'
            ' UseDNSCache Tells the client to use any cached DNS answers we'
            ' have when making requests via this connection.'
            '\n'
            ' PreferIPv6Automap When serving a hostname lookup request on this'
            ' port that should get automapped (according to'
            ' AutomapHostsOnResolve), if we could return either an IPv4 or an'
            ' IPv6 answer, prefer an IPv6 answer. (On by default.)'
            '\n'
            ' PreferSOCKSNoAuth Ordinarily, when an application offers both'
            ' "username/password authentication" and "no authentication" to'
            ' Tor via SOCKS5, Tor selects username/password authentication so'
            ' that IsolateSOCKSAuth can work. This can confuse some'
            ' applications, if they offer a username/password combination then'
            ' get confused when asked for one. You can disable this behavior,'
            ' so that Tor will select "No authentication" when'
            ' IsolateSOCKSAuth is disabled, or when this option is set.'
            '\n'
            ' Flags are processed left to right. If flags conflict, the last'
            ' flag on the line is used, and all earlier flags are ignored. No'
            ' error is issued for conflicting flags.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'SOCKSPolicy': {
        'help': (
            ' policy,policy,...'
            ' Set an entrance policy for this server, to limit who can connect'
            ' to the SocksPort and DNSPort ports. The policies have the same'
            ' form as exit policies below, except that port specifiers are'
            ' ignored. Any address not matched by some entry in the policy is'
            ' accepted.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'SOCKSTimeout': {
        'help': (
            ' NUM'
            ' Let a socks connection wait NUM seconds handshaking, and NUM'
            ' seconds unattached waiting for an appropriate circuit, before we'
            ' fail it. (Default: 2 minutes)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'TokenBucketRefillInterval': {
        'help': (
            ' NUM [msec|second]'
            ' Set the refill interval of Tor\'s token bucket to NUM'
            ' milliseconds. NUM must be between 1 and 1000, inclusive. Note'
            ' that the configured bandwidth limits are still expressed in'
            ' bytes per second: this option only affects the frequency with'
            ' which Tor checks to see whether previously exhausted connections'
            ' may read again. Can not be changed while tor is running.'
            ' (Default: 100 msec)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'TrackHostExits': {
        'help': (
            ' host,.domain,...'
            ' For each value in the comma separated list, Tor will track'
            ' recent connections to hosts that match this value and attempt to'
            ' reuse the same exit node for each. If the value is prepended'
            ' with a'
            ' \'.\', it is treated as matching an entire domain. If one of the'
            ' values is just a \'.\', it means match everything. This option'
            ' is useful if you frequently connect to sites that will expire'
            ' all your authentication cookies (i.e. log you out) if your IP'
            ' address changes. Note that this option does have the'
            ' disadvantage of making it more clear that a given history is'
            ' associated with a single user. However, most people who would'
            ' wish to observe this will observe it through cookies or other'
            ' protocol-specific means anyhow.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'TrackHostExitsExpire': {
        'help': (
            ' NUM'
            ' Since exit servers go up and down, it is desirable to expire the'
            ' association between host and exit server after NUM seconds. The'
            ' default is 1800 seconds (30 minutes).'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'UpdateBridgesFromAuthority': {
        'help': (
            ' 0|1'
            ' When set (along with UseBridges), Tor will try to fetch bridge'
            ' descriptors from the configured bridge authorities when'
            ' feasible. It will fall back to a direct request if the authority'
            ' responds with a 404. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'UseBridges': {
        'help': (
            ' 0|1'
            ' When set, Tor will fetch descriptors for each bridge listed in'
            ' the "Bridge" config lines, and use these relays as both entry'
            ' guards and directory guards. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'UseEntryGuards': {
        'help': (
            ' 0|1'
            ' If this option is set to 1, we pick a few long-term entry'
            ' servers, and try to stick with them. This is desirable because'
            ' constantly changing servers increases the odds that an adversary'
            ' who owns some servers will observe a fraction of your paths.'
            ' Entry Guards can not be used by Directory Authorities, Single'
            ' Onion Services, and Tor2web clients. In these cases, the this'
            ' option is ignored. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'GuardfractionFile': {
        'help': (
            ' FILENAME'
            ' V3 authoritative directories only. Configures the location of'
            ' the guardfraction file which contains information about how long'
            ' relays have been guards. (Default: unset)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'UseGuardFraction': {
        'help': (
            ' 0|1|auto'
            ' This torrc option specifies whether clients should use the'
            ' guardfraction information found in the consensus during path'
            ' selection. If it\'s set to auto, clients will do what the'
            ' UseGuardFraction consensus parameter tells them to do. (Default:'
            ' auto)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'NumEntryGuards': {
        'help': (
            ' NUM'
            ' If UseEntryGuards is set to 1, we will try to pick a total of'
            ' NUM routers as long-term entries for our circuits. If NUM is 0,'
            ' we try to learn the number from the guard-n-primary-guards-to-'
            ' use consensus parameter, and default to 1 if the consensus'
            ' parameter isn\'t set. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'NumDirectoryGuards': {
        'help': (
            ' NUM'
            ' If UseEntryGuardsAsDirectoryGuards is enabled, we try to make'
            ' sure we have at least NUM routers to use as directory guards. If'
            ' this option is set to 0, use the value from the guard-n-primary-'
            ' dir-guards-to-use consensus parameter, and default to 3 if the'
            ' consensus parameter isn\'t set. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'GuardLifetime': {
        'help': (
            ' N days|weeks|months'
            ' If nonzero, and UseEntryGuards is set, minimum time to keep a'
            ' guard before picking a new one. If zero, we use the'
            ' GuardLifetime parameter from the consensus directory. No value'
            ' here may be less than 1 month or greater than 5 years; out-of-'
            ' range values are clamped. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'SafeSocks': {
        'help': (
            ' 0|1'
            ' When this option is enabled, Tor will reject application'
            ' connections that use unsafe variants of the socks protocol —'
            ' ones that only provide an IP address, meaning the application is'
            ' doing a DNS resolve first. Specifically, these are socks4 and'
            ' socks5 when not doing remote DNS. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'TestSocks': {
        'help': (
            ' 0|1'
            ' When this option is enabled, Tor will make a notice-level log'
            ' entry for each connection to the Socks port indicating whether'
            ' the request used a safe socks protocol or an unsafe one (see'
            ' above entry on SafeSocks). This helps to determine whether an'
            ' application using Tor is possibly leaking DNS requests.'
            ' (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'VirtualAddrNetworkIPv4': {
        'help': (
            ' Address/bits'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'VirtualAddrNetworkIPv6': {
        'help': (
            ' [Address]/bits'
            ' When Tor needs to assign a virtual (unused) address because of a'
            ' MAPADDRESS command from the controller or the'
            ' AutomapHostsOnResolve feature, Tor picks an unassigned address'
            ' from this range. (Defaults: 127.192.0.0/10 and [FE80::]/10'
            ' respectively.)'
            '\n'
            ' When providing proxy server service to a network of computers'
            ' using a tool like dns-proxy-tor, change the IPv4 network to'
            ' "10.192.0.0/10" or "172.16.0.0/12" and change the IPv6 network'
            ' to "[FC00::]/7". The default VirtualAddrNetwork address ranges'
            ' on a properly configured machine will route to the loopback or'
            ' link-local interface. The maximum number of bits for the network'
            ' prefix is set to 104 for IPv6 and 16 for IPv4. However, a wider'
            ' network - smaller prefix length'
            '\n'
            ' ·   is preferable since it reduces the chances for an attacker'
            ' to guess the used IP. For local use, no change to the default'
            ' VirtualAddrNetwork setting is needed.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'AllowNonRFC953Hostnames': {
        'help': (
            ' 0|1'
            ' When this option is disabled, Tor blocks hostnames containing'
            ' illegal characters (like @ and :) rather than sending them to an'
            ' exit node to be resolved. This helps trap accidental attempts to'
            ' resolve URLs and so on. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'AllowDotExit': {
        'help': (
            ' 0|1'
            ' If enabled, we convert "www.google.com.foo.exit" addresses on'
            ' the SocksPort/TransPort/NATDPort into "www.google.com" addresses'
            ' that exit from the node "foo". Disabled by default since'
            ' attacking websites and exit relays can use it to manipulate your'
            ' path selection. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'TransPort': {
        'help': (
            ' [address:]port|auto [isolation flags]'
            ' Open this port to listen for transparent proxy connections. Set'
            ' this to 0 if you don\'t want to allow transparent proxy'
            ' connections. Set the port to "auto" to have Tor pick a port for'
            ' you. This directive can be specified multiple times to bind to'
            ' multiple addresses/ports. See SOCKSPort for an explanation of'
            ' isolation flags.'
            '\n'
            ' TransPort requires OS support for transparent proxies, such as'
            ' BSDs\' pf or Linux\'s IPTables. If you\'re planning to use Tor as'
            ' a transparent proxy for a network, you\'ll want to examine and'
            ' change VirtualAddrNetwork from the default setting. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'TransProxyType': {
        'help': (
            ' default|TPROXY|ipfw|pf-divert'
            ' TransProxyType may only be enabled when there is transparent'
            ' proxy listener enabled.'
            '\n'
            ' Set this to "TPROXY" if you wish to be able to use the TPROXY'
            ' Linux module to transparently proxy connections that are'
            ' configured using the TransPort option. Detailed information on'
            ' how to configure the TPROXY feature can be found in the Linux'
            ' kernel source tree in the file'
            ' Documentation/networking/tproxy.txt.'
            '\n'
            ' Set this option to "ipfw" to use the FreeBSD ipfw interface.'
            '\n'
            ' On *BSD operating systems when using pf, set this to "pf-divert"'
            ' to take advantage of divert-to rules, which do not modify the'
            ' packets like rdr-to rules do. Detailed information on how to'
            ' configure pf to use divert-to rules can be found in the'
            ' pf.conf(5) manual page. On OpenBSD, divert-to is available to'
            ' use on versions greater than or equal to OpenBSD 4.4.'
            '\n'
            ' Set this to "default", or leave it unconfigured, to use regular'
            ' IPTables on Linux, or to use pf rdr-to rules on *BSD systems.'
            '\n'
            ' (Default: "default".)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'NATDPort': {
        'help': (
            ' [address:]port|auto [isolation flags]'
            ' Open this port to listen for connections from old versions of'
            ' ipfw (as included in old versions of FreeBSD, etc) using the'
            ' NATD protocol. Use 0 if you don\'t want to allow NATD'
            ' connections. Set the port to "auto" to have Tor pick a port for'
            ' you. This directive can be specified multiple times to bind to'
            ' multiple addresses/ports. See SocksPort for an explanation of'
            ' isolation flags.'
            '\n'
            ' This option is only for people who cannot use TransPort.'
            ' (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'AutomapHostsOnResolve': {
        'help': (
            ' 0|1'
            ' When this option is enabled, and we get a request to resolve an'
            ' address that ends with one of the suffixes in'
            ' AutomapHostsSuffixes, we map an unused virtual address to that'
            ' address, and return the new virtual address. This is handy for'
            ' making ".onion" addresses work with applications that resolve an'
            ' address and then connect to it. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'AutomapHostsSuffixes': {
        'help': (
            ' SUFFIX,SUFFIX,...'
            ' A comma-separated list of suffixes to use with'
            ' AutomapHostsOnResolve. The "." suffix is equivalent to "all'
            ' addresses." (Default: .exit,.onion).'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'DNSPort': {
        'help': (
            ' [address:]port|auto [isolation flags]'
            ' If non-zero, open this port to listen for UDP DNS requests, and'
            ' resolve them anonymously. This port only handles A, AAAA, and'
            ' PTR requests---it doesn\'t handle arbitrary DNS request types.'
            ' Set the port to "auto" to have Tor pick a port for you. This'
            ' directive can be specified multiple times to bind to multiple'
            ' addresses/ports. See SocksPort for an explanation of isolation'
            ' flags. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ClientDNSRejectInternalAddresses': {
        'help': (
            ' 0|1'
            ' If true, Tor does not believe any anonymously retrieved DNS'
            ' answer that tells it that an address resolves to an internal'
            ' address (like 127.0.0.1 or 192.168.0.1). This option prevents'
            ' certain browser-based attacks; don\'t turn it off unless you know'
            ' what you\'re doing. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ClientRejectInternalAddresses': {
        'help': (
            ' 0|1'
            ' If true, Tor does not try to fulfill requests to connect to an'
            ' internal address (like 127.0.0.1 or 192.168.0.1) unless a exit'
            ' node is specifically requested (for example, via a .exit'
            ' hostname, or a controller request). If true, multicast DNS'
            ' hostnames for machines on the local network (of the form'
            ' *.local) are also rejected. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'DownloadExtraInfo': {
        'help': (
            ' 0|1'
            ' If true, Tor downloads and caches "extra-info" documents. These'
            ' documents contain information about servers other than the'
            ' information in their regular server descriptors. Tor does not'
            ' use this information for anything itself; to save bandwidth,'
            ' leave this option turned off. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'WarnPlaintextPorts': {
        'help': (
            ' port,port,...'
            ' Tells Tor to issue a warnings whenever the user tries to make an'
            ' anonymous connection to one of these ports. This option is'
            ' designed to alert users to services that risk sending passwords'
            ' in the clear. (Default: 23,109,110,143)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'RejectPlaintextPorts': {
        'help': (
            ' port,port,...'
            ' Like WarnPlaintextPorts, but instead of warning about risky port'
            ' uses, Tor will instead refuse to make the connection. (Default:'
            ' None)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'OptimisticData': {
        'help': (
            ' 0|1|auto'
            ' When this option is set, and Tor is using an exit node that'
            ' supports the feature, it will try optimistically to send data to'
            ' the exit node without waiting for the exit node to report'
            ' whether the connection succeeded. This can save a round-trip'
            ' time for protocols like HTTP where the client talks first. If'
            ' OptimisticData is set to auto, Tor will look at the'
            ' UseOptimisticData parameter in the networkstatus. (Default:'
            ' auto)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'Tor2webMode': {
        'help': (
            ' 0|1'
            ' When this option is set, Tor connects to hidden services non-'
            ' anonymously. This option also disables client connections to'
            ' non-hidden-service hostnames through Tor. It must only be used'
            ' when running a tor2web Hidden Service web proxy. To enable this'
            ' option the compile time flag --enable-tor2web-mode must be'
            ' specified. Since Tor2webMode is non-anonymous, you can not run'
            ' an anonymous Hidden Service on a tor version compiled with'
            ' Tor2webMode. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'Tor2webRendezvousPoints': {
        'help': (
            ' node,node,...'
            ' A list of identity fingerprints, nicknames, country codes and'
            ' address patterns of nodes that are allowed to be used as RPs in'
            ' HS circuits; any other nodes will not be used as RPs. (Example:'
            ' Tor2webRendezvousPoints Fastyfasty,'
            ' ABCD1234CDEF5678ABCD1234CDEF5678ABCD1234, {cc}, 255.254.0.0/8)'
            '\n'
            ' This feature can only be used if Tor2webMode is also enabled.'
            '\n'
            ' ExcludeNodes have higher priority than Tor2webRendezvousPoints,'
            ' which means that nodes specified in ExcludeNodes will not be'
            ' picked as RPs.'
            '\n'
            ' If no nodes in Tor2webRendezvousPoints are currently available'
            ' for use, Tor will choose a random node when building HS'
            ' circuits.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'UseMicrodescriptors': {
        'help': (
            ' 0|1|auto'
            ' Microdescriptors are a smaller version of the information that'
            ' Tor needs in order to build its circuits. Using microdescriptors'
            ' makes Tor clients download less directory information, thus'
            ' saving bandwidth. Directory caches need to fetch regular'
            ' descriptors and microdescriptors, so this option doesn\'t save'
            ' any bandwidth for them. If this option is set to "auto"'
            ' (recommended) then it is on for all clients that do not set'
            ' FetchUselessDescriptors. (Default: auto)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PathBiasCircThreshold': {
        'help': (
            ' NUM'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PathBiasNoticeRate': {
        'help': (
            ' NUM'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PathBiasWarnRate': {
        'help': (
            ' NUM'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PathBiasExtremeRate': {
        'help': (
            ' NUM'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PathBiasDropGuards': {
        'help': (
            ' NUM'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PathBiasScaleThreshold': {
        'help': (
            ' NUM'
            ' These options override the default behavior of Tor\'s (currently'
            ' experimental) path bias detection algorithm. To try to find'
            ' broken or misbehaving guard nodes, Tor looks for nodes where'
            ' more than a certain fraction of circuits through that guard fail'
            ' to get built.'
            '\n'
            ' The PathBiasCircThreshold option controls how many circuits we'
            ' need to build through a guard before we make these checks. The'
            ' PathBiasNoticeRate, PathBiasWarnRate and PathBiasExtremeRate'
            ' options control what fraction of circuits must succeed through a'
            ' guard so we won\'t write log messages. If less than'
            ' PathBiasExtremeRate circuits succeed and PathBiasDropGuards is'
            ' set to 1, we disable use of that guard.'
            '\n'
            ' When we have seen more than PathBiasScaleThreshold circuits'
            ' through a guard, we scale our observations by 0.5 (governed by'
            ' the consensus) so that new observations don\'t get swamped by old'
            ' ones.'
            '\n'
            ' By default, or if a negative value is provided for one of these'
            ' options, Tor uses reasonable defaults from the networkstatus'
            ' consensus document. If no defaults are available there, these'
            ' options default to 150, .70, .50, .30, 0, and 300 respectively.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PathBiasUseThreshold': {
        'help': (
            ' NUM'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PathBiasNoticeUseRate': {
        'help': (
            ' NUM'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PathBiasExtremeUseRate': {
        'help': (
            ' NUM'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PathBiasScaleUseThreshold': {
        'help': (
            ' NUM'
            ' Similar to the above options, these options override the default'
            ' behavior of Tor\'s (currently experimental) path use bias'
            ' detection algorithm.'
            '\n'
            ' Where as the path bias parameters govern thresholds for'
            ' successfully building circuits, these four path use bias'
            ' parameters govern thresholds only for circuit usage. Circuits'
            ' which receive no stream usage are not counted by this detection'
            ' algorithm. A used circuit is considered successful if it is'
            ' capable of carrying streams or otherwise receiving well-formed'
            ' responses to RELAY cells.'
            '\n'
            ' By default, or if a negative value is provided for one of these'
            ' options, Tor uses reasonable defaults from the networkstatus'
            ' consensus document. If no defaults are available there, these'
            ' options default to 20, .80, .60, and 100, respectively.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ClientUseIPv4': {
        'help': (
            ' 0|1'
            ' If this option is set to 0, Tor will avoid connecting to'
            ' directory servers and entry nodes over IPv4. Note that clients'
            ' with an IPv4 address in a Bridge, proxy, or pluggable transport'
            ' line will try connecting over IPv4 even if ClientUseIPv4 is set'
            ' to 0. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ClientUseIPv6': {
        'help': (
            ' 0|1'
            ' If this option is set to 1, Tor might connect to directory'
            ' servers or entry nodes over IPv6. Note that clients configured'
            ' with an IPv6 address in a Bridge, proxy, or pluggable transport'
            ' line will try connecting over IPv6 even if ClientUseIPv6 is set'
            ' to 0. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ClientPreferIPv6DirPort': {
        'help': (
            ' 0|1|auto'
            ' If this option is set to 1, Tor prefers a directory port with an'
            ' IPv6 address over one with IPv4, for direct connections, if a'
            ' given directory server has both. (Tor also prefers an IPv6'
            ' DirPort if IPv4Client is set to 0.) If this option is set to'
            ' auto, clients prefer IPv4. Other things may influence the'
            ' choice. This option breaks a tie to the favor of IPv6. (Default:'
            ' auto)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ClientPreferIPv6ORPort': {
        'help': (
            ' 0|1|auto'
            ' If this option is set to 1, Tor prefers an OR port with an IPv6'
            ' address over one with IPv4 if a given entry node has both. (Tor'
            ' also prefers an IPv6 ORPort if IPv4Client is set to 0.) If this'
            ' option is set to auto, Tor bridge clients prefer the configured'
            ' bridge address, and other clients prefer IPv4. Other things may'
            ' influence the choice. This option breaks a tie to the favor of'
            ' IPv6. (Default: auto)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PathsNeededToBuildCircuits': {
        'help': (
            ' NUM'
            ' Tor clients don\'t build circuits for user traffic until they'
            ' know about enough of the network so that they could potentially'
            ' construct enough of the possible paths through the network. If'
            ' this option is set to a fraction between 0.25 and 0.95, Tor'
            ' won\'t build circuits until it has enough descriptors or'
            ' microdescriptors to construct that fraction of possible paths.'
            ' Note that setting this option too low can make your Tor client'
            ' less anonymous, and setting it too high can prevent your Tor'
            ' client from bootstrapping. If this option is negative, Tor will'
            ' use a default value chosen by the directory authorities. If the'
            ' directory authorities do not choose a value, Tor will default to'
            ' 0.6. (Default: -1.)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ClientBootstrapConsensusAuthorityDownloadSchedule': {
        'help': (
            ' N,N,...'
            ' Schedule for when clients should download consensuses from'
            ' authorities if they are bootstrapping (that is, they don\'t have'
            ' a usable, reasonably live consensus). Only used by clients'
            ' fetching from a list of fallback directory mirrors. This'
            ' schedule is advanced by (potentially concurrent) connection'
            ' attempts, unlike other schedules, which are advanced by'
            ' connection failures. (Default: 10, 11, 3600, 10800, 25200,'
            ' 54000, 111600, 262800)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ClientBootstrapConsensusFallbackDownloadSchedule': {
        'help': (
            ' N,N,...'
            ' Schedule for when clients should download consensuses from'
            ' fallback directory mirrors if they are bootstrapping (that is,'
            ' they don\'t have a usable, reasonably live consensus). Only used'
            ' by clients fetching from a list of fallback directory mirrors.'
            ' This schedule is advanced by (potentially concurrent) connection'
            ' attempts, unlike other schedules, which are advanced by'
            ' connection failures. (Default: 0, 1, 4, 11, 3600, 10800, 25200,'
            ' 54000, 111600, 262800)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ClientBootstrapConsensusAuthorityOnlyDownloadSchedule': {
        'help': (
            ' N,N,...'
            ' Schedule for when clients should download consensuses from'
            ' authorities if they are bootstrapping (that is, they don\'t have'
            ' a usable, reasonably live consensus). Only used by clients which'
            ' don\'t have or won\'t fetch from a list of fallback directory'
            ' mirrors. This schedule is advanced by (potentially concurrent)'
            ' connection attempts, unlike other schedules, which are advanced'
            ' by connection failures. (Default: 0, 3, 7, 3600, 10800, 25200,'
            ' 54000, 111600, 262800)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ClientBootstrapConsensusMaxDownloadTries': {
        'help': (
            ' NUM'
            ' Try this many times to download a consensus while bootstrapping'
            ' using fallback directory mirrors before giving up. (Default: 7)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ClientBootstrapConsensusAuthorityOnlyMaxDownloadTries': {
        'help': (
            ' NUM'
            ' Try this many times to download a consensus while bootstrapping'
            ' using authorities before giving up. (Default: 4)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ClientBootstrapConsensusMaxInProgressTries': {
        'help': (
            ' NUM'
            ' Try this many simultaneous connections to download a consensus'
            ' before waiting for one to complete, timeout, or error out.'
            ' (Default: 4)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'Address': {
        'help': (
            ' address'
            ' The IP address or fully qualified domain name of this server'
            ' (e.g. moria.mit.edu). You can leave this unset, and Tor will'
            ' guess your IP address. This IP address is the one used to tell'
            ' clients and other servers where to find your Tor server; it'
            ' doesn\'t affect the IP that your Tor client binds to. To bind to'
            ' a different address, use the *ListenAddress and'
            ' OutboundBindAddress options.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'AssumeReachable': {
        'help': (
            ' 0|1'
            ' This option is used when bootstrapping a new Tor network. If set'
            ' to 1, don\'t do self-reachability testing; just upload your'
            ' server descriptor immediately. If AuthoritativeDirectory is also'
            ' set, this option instructs the dirserver to bypass remote'
            ' reachability testing too and list all connected servers as'
            ' running.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'BridgeRelay': {
        'help': (
            ' 0|1'
            ' Sets the relay to act as a "bridge" with respect to relaying'
            ' connections from bridge users to the Tor network. It mainly'
            ' causes Tor to publish a server descriptor to the bridge'
            ' database, rather than to the public directory authorities.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ContactInfo': {
        'help': (
            ' email_address'
            ' Administrative contact information for this relay or bridge.'
            ' This line can be used to contact you if your relay or bridge is'
            ' misconfigured or something else goes wrong. Note that we archive'
            ' and publish all descriptors containing these lines and that'
            ' Google indexes them, so spammers might also collect them. You'
            ' may want to obscure the fact that it\'s an email address and/or'
            ' generate a new address for this purpose.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ExitRelay': {
        'help': (
            ' 0|1|auto'
            ' Tells Tor whether to run as an exit relay. If Tor is running as'
            ' a non-bridge server, and ExitRelay is set to 1, then Tor allows'
            ' traffic to exit according to the ExitPolicy option (or the'
            ' default ExitPolicy if none is specified).'
            '\n'
            ' If ExitRelay is set to 0, no traffic is allowed to exit, and the'
            ' ExitPolicy option is ignored.'
            '\n'
            ' If ExitRelay is set to "auto", then Tor behaves as if it were'
            ' set to 1, but warns the user if this would cause traffic to'
            ' exit. In a future version, the default value will be 0.'
            ' (Default: auto)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ExitPolicy': {
        'help': (
            ' policy,policy,...'
            ' Set an exit policy for this server. Each policy is of the form'
            ' "accept[6]|reject[6] ADDR[/MASK][:PORT]". If /MASK is omitted'
            ' then this policy just applies to the host given. Instead of'
            ' giving a host or network you can also use "*" to denote the'
            ' universe (0.0.0.0/0 and ::/128), or *4 to denote all IPv4'
            ' addresses, and *6 to denote all IPv6 addresses.  PORT can be a'
            ' single port number, an interval of ports "FROM_PORT-TO_PORT", or'
            ' "*". If PORT is omitted, that means "*".'
            '\n'
            ' For example, "accept 18.7.22.69:*,reject 18.0.0.0/8:*,accept'
            ' *:*" would reject any IPv4 traffic destined for MIT except for'
            ' web.mit.edu, and accept any other IPv4 or IPv6 traffic.'
            '\n'
            ' Tor also allows IPv6 exit policy entries. For instance, "reject6'
            ' [FC00::]/7:*" rejects all destinations that share 7 most'
            ' significant bit prefix with address FC00::. Respectively,'
            ' "accept6 [C000::]/3:*" accepts all destinations that share 3'
            ' most significant bit prefix with address C000::.'
            '\n'
            ' accept6 and reject6 only produce IPv6 exit policy entries. Using'
            ' an IPv4 address with accept6 or reject6 is ignored and generates'
            ' a warning. accept/reject allows either IPv4 or IPv6 addresses.'
            ' Use *4 as an IPv4 wildcard address, and *6 as an IPv6 wildcard'
            ' address. accept/reject * expands to matching IPv4 and IPv6'
            ' wildcard address rules.'
            '\n'
            ' To specify all IPv4 and IPv6 internal and link-local networks'
            ' (including 0.0.0.0/8, 169.254.0.0/16, 127.0.0.0/8,'
            ' 192.168.0.0/16, 10.0.0.0/8, 172.16.0.0/12, [::]/8, [FC00::]/7,'
            ' [FE80::]/10, [FEC0::]/10, [FF00::]/8, and [::]/127), you can use'
            ' the "private" alias instead of an address. ("private" always'
            ' produces rules for IPv4 and IPv6 addresses, even when used with'
            ' accept6/reject6.)'
            '\n'
            ' Private addresses are rejected by default (at the beginning of'
            ' your exit policy), along with any configured primary public IPv4'
            ' and IPv6 addresses. These private addresses are rejected unless'
            ' you set the ExitPolicyRejectPrivate config option to 0. For'
            ' example, once you\'ve done that, you could allow HTTP to'
            ' 127.0.0.1 and block all other connections to internal networks'
            ' with "accept 127.0.0.1:80,reject private:*", though that may'
            ' also allow connections to your own computer that are addressed'
            ' to its public (external) IP address. See RFC 1918 and RFC 3330'
            ' for more details about internal and reserved IP address space.'
            ' See ExitPolicyRejectLocalInterfaces if you want to block every'
            ' address on the relay, even those that aren\'t advertised in the'
            ' descriptor.'
            '\n'
            ' This directive can be specified multiple times so you don\'t have'
            ' to put it all on one line.'
            '\n'
            ' Policies are considered first to last, and the first match wins.'
            ' If you want to allow the same ports on IPv4 and IPv6, write your'
            ' rules using accept/reject *. If you want to allow different'
            ' ports on IPv4 and IPv6, write your IPv6 rules using'
            ' accept6/reject6 *6, and your IPv4 rules using accept/reject *4.'
            ' If you want to _replace_ the default exit policy, end your exit'
            ' policy with either a reject *:* or an accept *:*. Otherwise,'
            ' you\'re _augmenting_ (prepending to) the default exit policy. The'
            ' default exit policy is:'
            '\n'
            ' reject *:25'
            ' reject *:119'
            ' reject *:135-139'
            ' reject *:445'
            ' reject *:563'
            ' reject *:1214'
            ' reject *:4661-4666'
            ' reject *:6346-6429'
            ' reject *:6699'
            ' reject *:6881-6999'
            ' accept *:*'
            '\n'
            ' Since the default exit policy uses accept/reject *, it applies'
            ' to both IPv4 and IPv6 addresses.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ExitPolicyRejectPrivate': {
        'help': (
            ' 0|1'
            ' Reject all private (local) networks, along with the relay\'s'
            ' advertised public IPv4 and IPv6 addresses, at the beginning of'
            ' your exit policy. See above entry on ExitPolicy. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ExitPolicyRejectLocalInterfaces': {
        'help': (
            ' 0|1'
            ' Reject all IPv4 and IPv6 addresses that the relay knows about,'
            ' at the beginning of your exit policy. This includes any'
            ' OutboundBindAddress, the bind addresses of any port options,'
            ' such as ControlPort or DNSPort, and any public IPv4 and IPv6'
            ' addresses on any interface on the relay. (If IPv6Exit is not'
            ' set, all IPv6 addresses will be rejected anyway.) See above'
            ' entry on ExitPolicy. This option is off by default, because it'
            ' lists all public relay IP addresses in the ExitPolicy, even'
            ' those relay operators might prefer not to disclose. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'IPv6Exit': {
        'help': (
            ' 0|1'
            ' If set, and we are an exit node, allow clients to use us for'
            ' IPv6 traffic. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'MaxOnionQueueDelay': {
        'help': (
            ' NUM [msec|second]'
            ' If we have more onionskins queued for processing than we can'
            ' process in this amount of time, reject new ones. (Default: 1750'
            ' msec)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'MyFamily': {
        'help': (
            ' fingerprint,fingerprint,...'
            ' Declare that this Tor relay is controlled or administered by a'
            ' group or organization identical or similar to that of the other'
            ' relays, defined by their (possibly $-prefixed) identity'
            ' fingerprints. This option can be repeated many times, for'
            ' convenience in defining large families: all fingerprints in all'
            ' MyFamily lines are merged into one list. When two relays both'
            ' declare that they are in the same \'family\', Tor clients will'
            ' not use them in the same circuit. (Each relay only needs to list'
            ' the other servers in its family; it doesn\'t need to list itself,'
            ' but it won\'t hurt if it does.) Do not list any bridge relay as'
            ' it would compromise its concealment.'
            '\n'
            ' When listing a node, it\'s better to list it by fingerprint than'
            ' by nickname: fingerprints are more reliable.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'Nickname': {
        'help': (
            ' name'
            ' Set the server\'s nickname to \'name\'. Nicknames must be between'
            ' 1 and 19 characters inclusive, and must contain only the'
            ' characters [a-zA-Z0-9].'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'NumCPUs': {
        'help': (
            ' num'
            ' How many processes to use at once for decrypting onionskins and'
            ' other parallelizable operations. If this is set to 0, Tor will'
            ' try to detect how many CPUs you have, defaulting to 1 if it'
            ' can\'t tell. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ORPort': {
        'help': (
            ' [address:]PORT|auto [flags]'
            ' Advertise this port to listen for connections from Tor clients'
            ' and servers. This option is required to be a Tor server. Set it'
            ' to "auto" to have Tor pick a port for you. Set it to 0 to not'
            ' run an ORPort at all. This option can occur more than once.'
            ' (Default: 0)'
            '\n'
            ' Tor recognizes these flags on each ORPort:'
            '\n'
            ' NoAdvertise By default, we bind to a port and tell our users'
            ' about it. If NoAdvertise is specified, we don\'t advertise, but'
            ' listen anyway. This can be useful if the port everybody will be'
            ' connecting to (for example, one that\'s opened on our firewall)'
            ' is somewhere else.'
            '\n'
            ' NoListen By default, we bind to a port and tell our users about'
            ' it. If NoListen is specified, we don\'t bind, but advertise'
            ' anyway. This can be useful if something else (for example, a'
            ' firewall\'s port forwarding configuration) is causing connections'
            ' to reach us.'
            '\n'
            ' IPv4Only If the address is absent, or resolves to both an IPv4'
            ' and an IPv6 address, only listen to the IPv4 address.'
            '\n'
            ' IPv6Only If the address is absent, or resolves to both an IPv4'
            ' and an IPv6 address, only listen to the IPv6 address.'
            '\n'
            ' For obvious reasons, NoAdvertise and NoListen are mutually'
            ' exclusive, and IPv4Only and IPv6Only are mutually exclusive.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PortForwarding': {
        'help': (
            ' 0|1'
            ' Attempt to automatically forward the DirPort and ORPort on a NAT'
            ' router connecting this Tor server to the Internet. If set, Tor'
            ' will try both NAT-PMP (common on Apple routers) and UPnP (common'
            ' on routers from other manufacturers). (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PortForwardingHelper': {
        'help': (
            ' filename|pathname'
            ' If PortForwarding is set, use this executable to configure the'
            ' forwarding. If set to a filename, the system path will be'
            ' searched for the executable. If set to a path, only the'
            ' specified path will be executed. (Default: tor-fw-helper)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PublishServerDescriptor': {
        'help': (
            ' 0|1|v3|bridge,...'
            ' This option specifies which descriptors Tor will publish when'
            ' acting as a relay. You can choose multiple arguments, separated'
            ' by commas.'
            '\n'
            ' If this option is set to 0, Tor will not publish its descriptors'
            ' to any directories. (This is useful if you\'re testing out your'
            ' server, or if you\'re using a Tor controller that handles'
            ' directory publishing for you.) Otherwise, Tor will publish its'
            ' descriptors of all type(s) specified. The default is "1", which'
            ' means "if running as a server, publish the appropriate'
            ' descriptors to the authorities".'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ShutdownWaitLength': {
        'help': (
            ' NUM'
            ' When we get a SIGINT and we\'re a server, we begin shutting down:'
            ' we close listeners and start refusing new circuits. After NUM'
            ' seconds, we exit. If we get a second SIGINT, we exit'
            ' immediately. (Default: 30 seconds)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'SSLKeyLifetime': {
        'help': (
            ' N minutes|hours|days|weeks'
            ' When creating a link certificate for our outermost SSL'
            ' handshake, set its lifetime to this amount of time. If set to 0,'
            ' Tor will choose some reasonable random defaults. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HeartbeatPeriod': {
        'help': (
            ' N minutes|hours|days|weeks'
            ' Log a heartbeat message every HeartbeatPeriod seconds. This is a'
            ' log level notice message, designed to let you know your Tor'
            ' server is still alive and doing useful things. Settings this to'
            ' 0 will disable the heartbeat. Otherwise, it must be at least 30'
            ' minutes. (Default: 6 hours)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'AccountingMax': {
        'help': (
            ' N bytes|KBytes|MBytes|GBytes|TBytes|KBits|MBits|GBits|TBits'
            ' Limits the max number of bytes sent and received within a set'
            ' time period using a given calculation rule (see:'
            ' AccountingStart, AccountingRule). Useful if you need to stay'
            ' under a specific bandwidth. By default, the number used for'
            ' calculation is the max of either the bytes sent or received. For'
            ' example, with AccountingMax set to 1 GByte, a server could send'
            ' 900 MBytes and receive 800 MBytes and continue running. It will'
            ' only hibernate once one of the two reaches 1 GByte. This can be'
            ' changed to use the sum of the both bytes received and sent by'
            ' setting the AccountingRule option to "sum" (total bandwidth'
            ' in/out). When the number of bytes remaining gets low, Tor will'
            ' stop accepting new connections and circuits. When the number of'
            ' bytes is exhausted, Tor will hibernate until some time in the'
            ' next accounting period. To prevent all servers from waking at'
            ' the same time, Tor will also wait until a random point in each'
            ' period before waking up. If you have bandwidth cost issues,'
            ' enabling hibernation is preferable to setting a low bandwidth,'
            ' since it provides users with a collection of fast servers that'
            ' are up some of the time, which is more useful than a set of slow'
            ' servers that are always "available".'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'AccountingRule': {
        'help': (
            ' sum|max|in|out'
            ' How we determine when our AccountingMax has been reached (when'
            ' we should hibernate) during a time interval. Set to "max" to'
            ' calculate using the higher of either the sent or received bytes'
            ' (this is the default functionality). Set to "sum" to calculate'
            ' using the sent plus received bytes. Set to "in" to calculate'
            ' using only the received bytes. Set to "out" to calculate using'
            ' only the sent bytes. (Default: max)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'AccountingStart': {
        'help': (
            ' day|week|month [day] HH:MM'
            ' Specify how long accounting periods last. If month is given,'
            ' each accounting period runs from the time HH:MM on the dayth day'
            ' of one month to the same day and time of the next. (The day must'
            ' be between 1 and 28.) If week is given, each accounting period'
            ' runs from the time HH:MM of the dayth day of one week to the'
            ' same day and time of the next week, with Monday as day 1 and'
            ' Sunday as day'
            ' 7. If day is given, each accounting period runs from the time'
            ' HH:MM each day to the same time on the next day. All times'
            ' are local, and given in 24-hour time. (Default: "month 1 0:00")'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'RefuseUnknownExits': {
        'help': (
            ' 0|1|auto'
            ' Prevent nodes that don\'t appear in the consensus from exiting'
            ' using this relay. If the option is 1, we always block exit'
            ' attempts from such nodes; if it\'s 0, we never do, and if the'
            ' option is "auto", then we do whatever the authorities suggest in'
            ' the consensus (and block if the consensus is quiet on the'
            ' issue). (Default: auto)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ServerDNSResolvConfFile': {
        'help': (
            ' filename'
            ' Overrides the default DNS configuration with the configuration'
            ' in filename. The file format is the same as the standard Unix'
            ' "resolv.conf" file (7). This option, like all other ServerDNS'
            ' options, only affects name lookups that your server does on'
            ' behalf of clients. (Defaults to use the system DNS'
            ' configuration.)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ServerDNSAllowBrokenConfig': {
        'help': (
            ' 0|1'
            ' If this option is false, Tor exits immediately if there are'
            ' problems parsing the system DNS configuration or connecting to'
            ' nameservers. Otherwise, Tor continues to periodically retry the'
            ' system nameservers until it eventually succeeds. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ServerDNSSearchDomains': {
        'help': (
            ' 0|1'
            ' If set to 1, then we will search for addresses in the local'
            ' search domain. For example, if this system is configured to'
            ' believe it is in "example.com", and a client tries to connect to'
            ' "www", the client will be connected to "www.example.com". This'
            ' option only affects name lookups that your server does on behalf'
            ' of clients. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ServerDNSDetectHijacking': {
        'help': (
            ' 0|1'
            ' When this option is set to 1, we will test periodically to'
            ' determine whether our local nameservers have been configured to'
            ' hijack failing DNS requests (usually to an advertising site). If'
            ' they are, we will attempt to correct this. This option only'
            ' affects name lookups that your server does on behalf of clients.'
            ' (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ServerDNSTestAddresses': {
        'help': (
            ' address,address,...'
            ' When we\'re detecting DNS hijacking, make sure that these valid'
            ' addresses aren\'t getting redirected. If they are, then our DNS'
            ' is completely useless, and we\'ll reset our exit policy to'
            ' "reject *:*". This option only affects name lookups that your'
            ' server does on behalf of clients. (Default: "www.google.com,'
            ' www.mit.edu, www.yahoo.com, www.slashdot.org")'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ServerDNSAllowNonRFC953Hostnames': {
        'help': (
            ' 0|1'
            ' When this option is disabled, Tor does not try to resolve'
            ' hostnames containing illegal characters (like @ and :) rather'
            ' than sending them to an exit node to be resolved. This helps'
            ' trap accidental attempts to resolve URLs and so on. This option'
            ' only affects name lookups that your server does on behalf of'
            ' clients. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'BridgeRecordUsageByCountry': {
        'help': (
            ' 0|1'
            ' When this option is enabled and BridgeRelay is also enabled, and'
            ' we have GeoIP data, Tor keeps a per-country count of how many'
            ' client addresses have contacted it so that it can help the'
            ' bridge authority guess which countries have blocked access to'
            ' it. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ServerDNSRandomizeCase': {
        'help': (
            ' 0|1'
            ' When this option is set, Tor sets the case of each character'
            ' randomly in outgoing DNS requests, and makes sure that the case'
            ' matches in DNS replies. This so-called "0x20 hack" helps resist'
            ' some types of DNS poisoning attack. For more information, see'
            ' "Increased DNS Forgery Resistance through 0x20-Bit Encoding".'
            ' This option only affects name lookups that your server does on'
            ' behalf of clients. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'GeoIPFile': {
        'help': (
            ' filename'
            ' A filename containing IPv4 GeoIP data, for use with by-country'
            ' statistics.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'GeoIPv6File': {
        'help': (
            ' filename'
            ' A filename containing IPv6 GeoIP data, for use with by-country'
            ' statistics.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'CellStatistics': {
        'help': (
            ' 0|1'
            ' Relays only. When this option is enabled, Tor collects'
            ' statistics about cell processing (i.e. mean time a cell is'
            ' spending in a queue, mean number of cells in a queue and mean'
            ' number of processed cells per circuit) and writes them into disk'
            ' every 24 hours. Onion router operators may use the statistics'
            ' for performance monitoring. If ExtraInfoStatistics is enabled,'
            ' it will published as part of extra-info document. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PaddingStatistics': {
        'help': (
            ' 0|1'
            ' Relays only. When this option is enabled, Tor collects'
            ' statistics for padding cells sent and received by this relay, in'
            ' addition to total cell counts. These statistics are rounded, and'
            ' omitted if traffic is low. This information is important for'
            ' load balancing decisions related to padding. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'DirReqStatistics': {
        'help': (
            ' 0|1'
            ' Relays and bridges only. When this option is enabled, a Tor'
            ' directory writes statistics on the number and response time of'
            ' network status requests to disk every 24 hours. Enables relay'
            ' and bridge operators to monitor how much their server is being'
            ' used by clients to learn about Tor network. If'
            ' ExtraInfoStatistics is enabled, it will published as part of'
            ' extra-info document. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'EntryStatistics': {
        'help': (
            ' 0|1'
            ' Relays only. When this option is enabled, Tor writes statistics'
            ' on the number of directly connecting clients to disk every 24'
            ' hours. Enables relay operators to monitor how much inbound'
            ' traffic that originates from Tor clients passes through their'
            ' server to go further down the Tor network. If'
            ' ExtraInfoStatistics is enabled, it will be published as part of'
            ' extra-info document. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ExitPortStatistics': {
        'help': (
            ' 0|1'
            ' Exit relays only. When this option is enabled, Tor writes'
            ' statistics on the number of relayed bytes and opened stream per'
            ' exit port to disk every 24 hours. Enables exit relay operators'
            ' to measure and monitor amounts of traffic that leaves Tor'
            ' network through their exit node. If ExtraInfoStatistics is'
            ' enabled, it will be published as part of extra-info document.'
            ' (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ConnDirectionStatistics': {
        'help': (
            ' 0|1'
            ' Relays only. When this option is enabled, Tor writes statistics'
            ' on the amounts of traffic it passes between itself and other'
            ' relays to disk every 24 hours. Enables relay operators to'
            ' monitor how much their relay is being used as middle node in the'
            ' circuit. If ExtraInfoStatistics is enabled, it will be published'
            ' as part of extra-info document. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HiddenServiceStatistics': {
        'help': (
            ' 0|1'
            ' Relays only. When this option is enabled, a Tor relay writes'
            ' obfuscated statistics on its role as hidden-service directory,'
            ' introduction point, or rendezvous point to disk every 24 hours.'
            ' If ExtraInfoStatistics is also enabled, these statistics are'
            ' further published to the directory authorities. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ExtraInfoStatistics': {
        'help': (
            ' 0|1'
            ' When this option is enabled, Tor includes previously gathered'
            ' statistics in its extra-info documents that it uploads to the'
            ' directory authorities. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'ExtendAllowPrivateAddresses': {
        'help': (
            ' 0|1'
            ' When this option is enabled, Tor will connect to relays on'
            ' localhost, RFC1918 addresses, and so on. In particular, Tor will'
            ' make direct OR connections, and Tor routers allow EXTEND'
            ' requests, to these private addresses. (Tor will always allow'
            ' connections to bridges, proxies, and pluggable transports'
            ' configured on private addresses.) Enabling this option can'
            ' create security issues; you should probably leave it off.'
            ' (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'MaxMemInQueues': {
        'help': (
            ' N bytes|KB|MB|GB'
            ' This option configures a threshold above which Tor will assume'
            ' that it needs to stop queueing or buffering data because it\'s'
            ' about to run out of memory. If it hits this threshold, it will'
            ' begin killing circuits until it has recovered at least 10% of'
            ' this memory. Do not set this option too low, or your relay may'
            ' be unreliable under load. This option only affects some queues,'
            ' so the actual process size will be larger than this. If this'
            ' option is set to 0, Tor will try to pick a reasonable default'
            ' based on your system\'s physical memory. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'DisableOOSCheck': {
        'help': (
            ' 0|1'
            ' This option disables the code that closes connections when Tor'
            ' notices that it is running low on sockets. Right now, it is on'
            ' by default, since the existing out-of-sockets mechanism tends to'
            ' kill OR connections more than it should. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'SigningKeyLifetime': {
        'help': (
            ' N days|weeks|months'
            ' For how long should each Ed25519 signing key be valid? Tor uses'
            ' a permanent master identity key that can be kept offline, and'
            ' periodically generates new "signing" keys that it uses online.'
            ' This option configures their lifetime. (Default: 30 days)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'OfflineMasterKey': {
        'help': (
            ' 0|1'
            ' If non-zero, the Tor relay will never generate or load its'
            ' master secret key. Instead, you\'ll have to use "tor --keygen" to'
            ' manage the permanent ed25519 master identity key, as well as the'
            ' corresponding temporary signing keys and certificates. (Default:'
            ' 0)'
# HIDDEN SERVICE OPTIONS
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HiddenServiceDir': {
        'help': (
            ' DIRECTORY'
            ' Store data files for a hidden service in DIRECTORY. Every hidden'
            ' service must have a separate directory. You may use this option'
            ' multiple times to specify multiple services. If DIRECTORY does'
            ' not exist, Tor will create it. (Note: in current versions of'
            ' Tor, if DIRECTORY is a relative path, it will be relative to the'
            ' current working directory of Tor instance, not to its'
            ' DataDirectory. Do not rely on this behavior; it is not'
            ' guaranteed to remain the same in future versions.)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HiddenServicePort': {
        'help': (
            ' VIRTPORT [TARGET]'
            ' Configure a virtual port VIRTPORT for a hidden service. You may'
            ' use this option multiple times; each time applies to the service'
            ' using the most recent HiddenServiceDir. By default, this option'
            ' maps the virtual port to the same port on 127.0.0.1 over TCP.'
            ' You may override the target port, address, or both by specifying'
            ' a target of addr, port, addr:port, or unix:path. (You can'
            ' specify an IPv6 target as [addr]:port. Unix paths may be quoted,'
            ' and may use standard C escapes.) You may also have multiple'
            ' lines with the same VIRTPORT: when a user connects to that'
            ' VIRTPORT, one of the TARGETs from those lines will be chosen at'
            ' random.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'PublishHidServDescriptors': {
        'help': (
            ' 0|1'
            ' If set to 0, Tor will run any hidden services you configure, but'
            ' it won\'t advertise them to the rendezvous directory. This option'
            ' is only useful if you\'re using a Tor controller that handles'
            ' hidserv publishing for you. (Default: 1)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HiddenServiceVersion': {
        'help': (
            ' version,version,...'
            ' A list of rendezvous service descriptor versions to publish for'
            ' the hidden service. Currently, only version 2 is supported.'
            ' (Default: 2)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HiddenServiceAuthorizeClient': {
        'help': (
            ' auth-type client-name,client-name,...'
            ' If configured, the hidden service is accessible for authorized'
            ' clients only. The auth-type can either be \'basic\' for a'
            ' general-purpose authorization protocol or \'stealth\' for a less'
            ' scalable protocol that also hides service activity from'
            ' unauthorized clients. Only clients that are listed here are'
            ' authorized to access the hidden service. Valid client names are'
            ' 1 to 16 characters long and only use characters in A-Za-z0-9+-_'
            ' (no spaces). If this option is set, the hidden service is not'
            ' accessible for clients without authorization any more. Generated'
            ' authorization data can be found in the hostname file. Clients'
            ' need to put this authorization data in their configuration file'
            ' using HidServAuth.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HiddenServiceAllowUnknownPorts': {
        'help': (
            ' 0|1'
            ' If set to 1, then connections to unrecognized ports do not cause'
            ' the current hidden service to close rendezvous circuits.'
            ' (Setting this to 0 is not an authorization mechanism; it is'
            ' instead meant to be a mild inconvenience to port-scanners.)'
            ' (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HiddenServiceMaxStreams': {
        'help': (
            ' N'
            ' The maximum number of simultaneous streams (connections) per'
            ' rendezvous circuit. The maximum value allowed is 65535. (Setting'
            ' this to 0 will allow an unlimited number of simultanous'
            ' streams.) (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HiddenServiceMaxStreamsCloseCircuit': {
        'help': (
            ' 0|1'
            ' If set to 1, then exceeding HiddenServiceMaxStreams will cause'
            ' the offending rendezvous circuit to be torn down, as opposed to'
            ' stream creation requests that exceed the limit being silently'
            ' ignored. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'RendPostPeriod': {
        'help': (
            ' N seconds|minutes|hours|days|weeks'
            ' Every time the specified period elapses, Tor uploads any'
            ' rendezvous service descriptors to the directory servers. This'
            ' information is also uploaded whenever it changes. Minimum value'
            ' allowed is 10 minutes and maximum is 3.5 days. (Default: 1 hour)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HiddenServiceDirGroupReadable': {
        'help': (
            ' 0|1'
            ' If this option is set to 1, allow the filesystem group to read'
            ' the hidden service directory and hostname file. If the option is'
            ' set to 0, only owner is able to read the hidden service'
            ' directory. (Default: 0) Has no effect on Windows.'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HiddenServiceNumIntroductionPoints': {
        'help': (
            ' NUM'
            ' Number of introduction points the hidden service will have. You'
            ' can\'t have more than 10. (Default: 3)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HiddenServiceSingleHopMode': {
        'help': (
            ' 0|1'
            ' Experimental - Non Anonymous Hidden Services on a tor instance'
            ' in HiddenServiceSingleHopMode make one-hop (direct) circuits'
            ' between the onion service server, and the introduction and'
            ' rendezvous points. (Onion service descriptors are still posted'
            ' using 3-hop paths, to avoid onion service directories blocking'
            ' the service.) This option makes every hidden service instance'
            ' hosted by a tor instance a Single Onion Service. One-hop'
            ' circuits make Single Onion servers easily locatable, but clients'
            ' remain location-anonymous. However, the fact that a client is'
            ' accessing a Single Onion rather than a Hidden Service may be'
            ' statistically distinguishable.'
            '\n'
            ' WARNING: Once a hidden service directory has been used by a tor'
            ' instance in HiddenServiceSingleHopMode, it can NEVER be used'
            ' again for a hidden service. It is best practice to create a new'
            ' hidden service directory, key, and address for each new Single'
            ' Onion Service and Hidden Service. It is not possible to run'
            ' Single Onion Services and Hidden Services from the same tor'
            ' instance: they should be run on different servers with different'
            ' IP addresses.'
            '\n'
            ' HiddenServiceSingleHopMode requires'
            ' HiddenServiceNonAnonymousMode to be set to 1. Since a Single'
            ' Onion service is non-anonymous, you can not configure a'
            ' SOCKSPort on a tor instance that is running in'
            ' HiddenServiceSingleHopMode. Can not be changed while tor is'
            ' running. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
    'HiddenServiceNonAnonymousMode': {
        'help': (
            ' 0|1'
            ' Makes hidden services non-anonymous on this tor instance. Allows'
            ' the non-anonymous HiddenServiceSingleHopMode. Enables direct'
            ' connections in the server-side hidden service protocol. If you'
            ' are using this option, you need to disable all client-side'
            ' services on your Tor instance, including setting SOCKSPort to'
            ' "0". Can not be changed while tor is running. (Default: 0)'
        ),
        'type': TYPE_OPTION,
        'block': False,
        'multiple': True,
        'implicit': False,
        'blocked': False,
    },
}
