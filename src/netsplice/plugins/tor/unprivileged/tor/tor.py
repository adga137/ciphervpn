# -*- coding: utf-8 -*-
# tor.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tor Process Control. Uses a management connection that is setup() and
allows to connect, reconnect and disconnect the VPN connection using the
management socket.
'''

import atexit
import os
import re
import sys
import threading
import time
import random
import base64

from tempfile import mkstemp
from evasion.common import net
from netsplice.model.named_value_list import (
    named_value_list as named_value_list_model
)
from netsplice.util.management.client import client
from netsplice.util import get_logger
from netsplice.config import constants
from netsplice.util.process.dispatcher import dispatcher as process_dispatcher
from netsplice.util.process.location import (
    factory as location_factory
)
from netsplice.util.process.errors import ProcessError
from . import config as config
from .model.message_list_item import (
    message_list_item as message_list_item_model
)
from . import state as state
from .model.version_list import version_list as version_list_model
from .model.version_detail import version_detail as version_detail_model
from .model.version_help import version_help as version_help_model

logger = get_logger()
VERSION_SYSTEM_PROVIDED = 'v0.0.0-system-provided'


class tor(object):
    BINARY_NAME = 'tor'

    def __init__(self, model, connection_model_instance):
        self.model = model
        self.process = None
        self.connection_model_instance = connection_model_instance
        self.deleting = False
        self.disconnecting = False
        self.config_file_name = None
        self.server = None
        self.threading_lock = threading.RLock()
        self.hold_released = False
        self.management_socket = None

    def _execute(self, version, parameter_name, expected_error_code=None):
        '''
        Execute.

        Execute the given parameter and return the execution result.
        '''
        result = ''
        try:
            binary_name = self.BINARY_NAME
            binary_prefix = os.path.join('tor', 'bin')
            if version is not None:
                # tor/v1.2.3-patch/tor/bin/tor
                binary_prefix = os.path.join(
                    'tor',
                    version,
                    'tor',
                    'bin')

            self.process = process_dispatcher(
                binary_name, [parameter_name], None)
            self.process.set_prefix(binary_prefix)
            self.process.set_expected_error_code(expected_error_code)

            # Ensure that LD_LIBRARY_PATH is not exported to the
            # subprocess.
            environment_remove_ld_library = named_value_list_model()
            environment_remove_ld_library.add_value('LD_LIBRARY_PATH', None)
            self.process.set_environment(environment_remove_ld_library)

            result = self.process.start_subprocess_sync()
        except ProcessError as errors:
            logger.error(str(errors))
            raise
        return result

    def _get_executable_path(self, version):
        '''
        Get Executable Path.

        This string function returns the path of the executable in the
        filesystem. This duplicates the implementation in the process
        dispatcher for the purpose of displaying the information to the user.
        '''
        binary_prefix = os.path.join('tor', 'bin')
        if version is not None:
            # openvpn/v1.2.3-patch/openvpn/sbin/openvpn
            binary_prefix = os.path.join(
                'tor',
                version,
                'tor',
                'bin')

        location = location_factory(sys.platform)
        location.set_name(self.BINARY_NAME)
        search_paths = location.get_binary_search_paths()
        for path in search_paths:
            versions_path = os.path.join(path, self.BINARY_NAME)
            executable_path = os.path.join(path, location.get_binary_name())
            if not os.path.exists(versions_path):
                continue
            if version is None and os.path.isfile(executable_path):
                return versions_path
            versions = os.listdir(versions_path)
            for dir_version in versions:
                if dir_version != version:
                    continue
                executable_path = os.path.join(
                    versions_path,
                    dir_version,
                    'tor', 'bin', location.get_binary_name())
                if not os.path.exists(executable_path):
                    continue
                return executable_path
        logger.warn('No executable path found for %s' % (str(version),))
        return ''

    def connect(self):
        '''
        Setup created the process that has read the config-file and waits for
        a "hold release" message.
        '''
        self.lock()
        self.connection_model_instance.state.set(
            state.CONNECT_CONNECTION)
        self.connection_model_instance.up.set(False)
        self.connection_model_instance.active.set(True)
        self.model.commit()
        self.unlock()
        self.start_subprocess()
        self.start_status_loop(self.connection_model_instance)

    def create_temporary_config(self, configuration_content):
        # magic: use mkstemp and register atexit
        # Better would be to use NamedTemporaryFile but this does not work
        # on windows
        (file_handle, file_path) = mkstemp()
        os.write(file_handle, configuration_content)
        os.close(file_handle)
        atexit.register(self.remove_temporary_config, file_path)

        return file_path

    def remove_temporary_config(self, file_path):
        if os.path.exists(file_path):
            os.remove(file_path)

    def delete(self):
        '''
        Delete the connection and all its associated resources.
        '''
        self.lock()
        self.deleting = True
        self.connection_model_instance.state.set(
            state.DELETE_CONNECTION)
        self.model.commit()
        if self.config_file_name is not None:
            self.remove_temporary_config(self.config_file_name)
            self.config_file_name = None  # Garbage-collect will remove file
        self.connection_model_instance.state.set(
            state.INVALID_CONNECTION)
        self.model.commit()
        self.unlock()

    def disconnect(self):
        '''
        Causes Tor to exit gracefully.
        '''
        self.lock()
        self.disconnecting = True
        self.connection_model_instance.state.set(
            state.DISCONNECT_CONNECTION)
        self.connection_model_instance.disconnect()
        self.model.commit()
        self.unlock()
        if self.process:
            self.process.stop()
        logger.info(
            'Tor process has stopped.',
            extra={
                'connection_id': self.connection_model_instance.id.get()
            })

    def get_hash_password(self, binary_name, binary_prefix):
        '''
        Get Hash Password.

        Request the tor process to hash a password.

        Arguments:
            binary_name -- name of the tor executable.
            binary_prefix -- binary prefix for tor executable
        '''
        random_password = bytearray(
            random.getrandbits(8) for i in range(16))
        self.management_password = base64.b64encode(random_password)

        control_password_process = process_dispatcher(
            binary_name, {
                '--hash-password': self.management_password
            },
            model=None)
        control_password_process.set_prefix(binary_prefix)
        password_hash_lines = control_password_process.start_subprocess_sync()
        password_hash = None
        for line in password_hash_lines.split('\n'):
            if '[warn]' in line:
                continue
            password_hash = line.replace('\r', '')
            break
        if password_hash is None:
            raise ProcessError('Failed to acquire password hash.')
        return password_hash

    def get_geoip_files(self, geoip_prefix):
        '''
        Get GeoIP files.

        The packaged binaries '[warn]' when they get relative paths to the
        geoip files - and they do not find them in the libexec path.
        Find the files and return a tupple (ipv4,ipv6) with the absolute paths.
        When the files are not found, the tupple is None

        Arguments:
            geoip_prefix -- prefix in libexec where to expect the files
        '''
        geoipfile = None
        geoipv6file = None
        geoiplocation = location_factory(sys.platform)
        geoiplocation.set_name('geoip')
        geoiplocation.set_prefix(geoip_prefix)
        geoipv6location = location_factory(sys.platform)
        geoipv6location.set_name('geoip6')
        geoipv6location.set_prefix(geoip_prefix)
        try:
            geoipfile = geoiplocation.get_resource_path()
        except ProcessError:
            pass
        try:
            geoipv6file = geoipv6location.get_resource_path()
        except ProcessError:
            pass
        return (geoipfile, geoipv6file)

    def get_version_list(self):
        '''
        Get Version List.

        Create a list of available tor executables in the binary search
        paths.
        Only versions that contain a tor/bin/tor file will be listed.
        '''
        version_list = version_list_model()
        location = location_factory(sys.platform)
        location.set_name(self.BINARY_NAME)
        search_paths = location.get_binary_search_paths()
        for path in search_paths:
            versions_path = os.path.join(path, self.BINARY_NAME)
            executable_path = os.path.join(path, location.get_binary_name())
            if not os.path.exists(versions_path):
                continue
            if os.path.isfile(executable_path):
                version_item = version_list.item_model_class()
                version_item.version.set(VERSION_SYSTEM_PROVIDED)
                version_list.append(version_item)
                continue
            versions = os.listdir(versions_path)
            for version in versions:
                executable_path = os.path.join(
                    versions_path,
                    version,
                    'tor', 'bin', location.get_binary_name())
                if not os.path.exists(executable_path):
                    continue
                version_item = version_list.item_model_class()
                version_item.version.set(version)
                version_list.append(version_item)
        return version_list

    def get_version_detail(self, version):
        '''
        Get Version Detail.

        Request given tor version about its --version text
        '''
        version_detail = version_detail_model()
        version_detail.version.set(version)
        if version == VERSION_SYSTEM_PROVIDED:
            version = None
        detail = self._get_executable_path(version)
        detail += '\n'
        detail += self._execute(version, '--version', expected_error_code=1)
        version_detail.detail.set(detail)
        return version_detail

    def get_version_help(self, version):
        '''
        Get Version Help.

        Request given tor version about its --help and convert the output
        to a version_help_model.
        '''
        version_help = version_help_model()
        help_text = self._execute(version, '--help', expected_error_code=1)
        help_item = None
        help_item_text = ''
        for line in help_text.split('\n'):
            if line.startswith('--'):
                # commit last help item
                if help_item is not None:
                    help_item.help.set(help_item_text)
                    version_help.append(help_item)
                # start new help_item
                item_parts = line.split(':')
                if len(item_parts) is 0:
                    continue
                option_parts = item_parts[0].split(' ')
                if len(option_parts) is 0:
                    continue
                help_item = version_help.item_model_class()
                help_item_text = ''
                option = option_parts[0].strip().replace('--', '')
                del option_parts[0]
                del item_parts[0]
                help_item_text = ':'.join(item_parts).strip()
                parameter = ' '.join(option_parts).strip()

                help_item.option.set(option)
                help_item.parameter.set(parameter)
            elif line == '' and help_item is not None:
                # 'section' in help separating client/server/tun etc
                # commit the last item and wait for the next ^--
                help_item.help.set(help_item_text)
                version_help.append(help_item)
                help_item = None
            elif help_item is not None:
                # continuation of a help_item, strip any blanks and append
                # to the help text
                help_item_text += ' %s' % (line.strip(),)

        if help_item is not None:
            # commit the last help_item
            help_item.help.set(help_item_text)
            version_help.append(help_item)

        return version_help

    def lock(self):
        '''
        Lock the process, only the current thread may modify its resources.
        '''
        self.threading_lock.acquire()

    def management_handler(self, connection):
        '''
        Handle the management connection and read and send data on that
        connection. The data that was read from the socket will be processed
        by process_messages. When the socket is closed the connection is re-
        setup unless the connection is currently deleted. This handler runs in
        a separate thread and therefore needs to lock the resources it
        accesses.
        '''

        # Remove the config_file_name that contains sensitive information
        # (keys). tor has already read the contents and stores its content
        # in its process.
        self.lock()
        if self.config_file_name is not None:
            self.remove_temporary_config(self.config_file_name)
            self.config_file_name = None

        self.management_socket = connection
        self.connection_model_instance.state.set(
            state.MANAGEMENT_CONNECTED)
        message = message_list_item_model()
        message.date.set(time.time())
        message.data.set('Authenticate "%s"' % (self.management_password,))
        self.connection_model_instance.up.set(True)
        self.connection_model_instance.send_message_list = []
        self.connection_model_instance.send_message_list.append(message)
        self.model.commit()
        self.data_buffer = ''
        self.unlock()

        socket_error = False
        while self.management_socket is not None:
            self.send_management_commands()
            try:
                data = self.management_socket.recv(config.MAX_RECV)
            except:
                socket_error = True
                data = ''
            if data == '':
                break
            self.lock()
            self.data_buffer += data
            self.unlock()
            self.process_messages()

        self.lock()
        self.connection_model_instance.state.set(
            state.MANAGEMENT_DISCONNECTED)
        self.connection_model_instance.disconnect()
        self.model.commit()
        self.unlock()

        # Process leftover messages
        if self.data_buffer != '':
            self.lock()
            self.data_buffer += '\n'
            self.unlock()
            self.process_messages()

        # Connection was closed - eg due to kill/crash of tor instance
        self.lock()
        self.connection_model_instance.disconnect()
        self.management_socket = None
        self.unlock()

        if socket_error:
            logger.warning(
                'Tor management socket error:'
                ' exception receiving from management',
                extra={
                    'connection_id': self.connection_model_instance.id.get()
                })
            if not self.disconnecting:
                self.disconnect()
                self.delete()

    def process_messages(self):
        '''
        Process Messages.

        Read all messages from the data_buffer and store each message in the
        connection_model_instance. Only processes when the current buffer ends
        with newline. Otherwise the message is incomplete.
        '''
        if not self.data_buffer.endswith('\n'):
            return
        self.lock()
        data_lines = self.data_buffer.splitlines(False)
        self.unlock()
        connection_id = self.connection_model_instance.id.get()

        while len(data_lines):
            line = data_lines[0]
            if config.VERBOSE > 2:
                logger.debug(
                    'Tor management wrote: [%s]' % (str(data_lines),),
                    extra={
                        'connection_id': connection_id
                    })
            if line.startswith('250 OK'):
                del data_lines[0]
            elif line.startswith('250-traffic'):
                delete_offset = self.process_statistic_message(data_lines)
                if 0 > delete_offset:
                    # statistics not yet complete
                    break
                del data_lines[0:(delete_offset + 1)]
            else:
                logger.warning(
                    'Tor management unhandled: %s' % (line,),
                    extra={
                        'connection_id': connection_id
                    })
                message = message_list_item_model()
                message.date.set(time.time())
                message.data.set(line)
                self.lock()
                self.connection_model_instance.recv_message_list.append(
                    message)
                self.unlock()
                del data_lines[0]

        self.lock()
        self.data_buffer = '\n'.join(data_lines)
        self.unlock()

    def process_statistic_message(self, lines):
        complete_index = -1
        statistics = []
        for index, line in enumerate(lines):
            statistics.append(line)
            complete_index = index

        self.lock()
        model = self.connection_model_instance
        changed = False
        for line in statistics:
            written_match = re.match('.*written=([0-9]*)$', line)
            read_match = re.match('.*read=([0-9]*)$', line)
            if written_match is not None:
                bytes = int(written_match.group(1))
                model.write_tap_bytes.set(bytes)
                changed = True
            if read_match is not None:
                bytes = int(read_match.group(1))
                model.read_tap_bytes.set(bytes)
                changed = True

        if changed:
            model.commit()
        self.unlock()
        return complete_index

    def reconnect(self):
        '''
        Reconnect.

        Disconnect and Connect the active connection
        '''
        logger.warn('Reconnecting')
        self.disconnect()
        time.sleep(1)
        self.connect()

    def send_management_commands(self):
        '''
        Send all queued management commands to the tor process.
        '''
        self.lock()
        socket_error = False
        if self.management_socket is None:
            del self.connection_model_instance.send_message_list[:]
            self.unlock()
            return

        last_message = None
        connection_id = self.connection_model_instance.id.get()
        for message in self.connection_model_instance.send_message_list:
            if last_message == message:
                logger.debug(
                    'Skipping duplicate message: %s' % (message,),
                    extra={
                        'connection_id': connection_id
                    })
                continue
            if config.VERBOSE > 2:
                logger.debug(
                    'Sending %s to Tor process' % (message.data.get()),
                    extra={
                        'connection_id': connection_id
                    })
            try:
                self.management_socket.sendall(message.data.get() + '\n')
            except:
                logger.error(
                    'Failed to send message to Tor process.',
                    extra={
                        'connection_id': connection_id
                    })
                socket_error = True
            last_message = message
        del self.connection_model_instance.send_message_list[:]
        if config.VERBOSE > 2:
            logger.debug(
                'Send queue cleared',
                extra={
                    'connection_id': connection_id
                })
        self.unlock()
        if socket_error:
            if not self.disconnecting:
                self.disconnect()
                self.delete()

    def setup(self):
        logger.info(
            'Setup Tor connection.',
            extra={
                'connection_id': self.connection_model_instance.id.get()
            })

    def start_subprocess(self):
        logger.info(
            'Start Tor subprocess.',
            extra={
                'connection_id': self.connection_model_instance.id.get()
            })
        self.lock()
        connection_id = self.connection_model_instance.id.get()
        active_ports = self.model.connections.get_connected_management_ports()
        management_port = net.get_free_port(exclude_ports=active_ports)
        # Store the config file to a temporary file so OpenVPN can read it.
        # The file is removed as soon as OpenVPN has connected to the
        # management port and therefore had read and processed the config
        self.config_file_name = self.create_temporary_config(
            self.connection_model_instance.configuration.get())

        try:
            binary_name = 'tor'
            binary_prefix = os.path.join('tor', 'bin')
            geoip_prefix = os.path.join('tor', 'share', 'tor')

            if self.connection_model_instance.version.get() is not None:
                # tor/tor-1.2.3/bin/tor
                binary_prefix = os.path.join(
                    'tor',
                    self.connection_model_instance.version.get(),
                    'tor', 'bin')
                geoip_prefix = os.path.join(
                    'tor',
                    self.connection_model_instance.version.get(),
                    'tor', 'share', 'tor')

            self.hold_released = False

            password_hash = self.get_hash_password(
                binary_name, binary_prefix)
            (geoipfile, geoipv6file) = self.get_geoip_files(
                geoip_prefix)
            options = {
                '--Log': 'notice',
                '-f': self.config_file_name,
                '--HashedControlPassword': password_hash,
                '--ControlPort': management_port
            }
            if geoipfile is not None and geoipv6file is not None:
                options['--geoipfile'] = geoipfile
                options['--geoipv6file'] = geoipv6file
            self.process = process_dispatcher(
                binary_name, options,
                model=self.model,
                model_instance=self.connection_model_instance)
            self.process.set_prefix(binary_prefix)
            self.process.set_threading_lock(self.threading_lock)
            self.process.set_logger_extra({
                'connection_id': connection_id
            })
            # Ensure that LD_LIBRARY_PATH is not exported to the
            # subprocess.
            environment_remove_ld_library = named_value_list_model()
            environment_remove_ld_library.add_value('LD_LIBRARY_PATH', None)
            self.process.set_environment(environment_remove_ld_library)

            self.process.start_subprocess()
            try:
                time.sleep(1)
                self.client = client(
                    constants.LOCALHOST,
                    management_port,
                    self.management_handler)
            except:
                logger.warn(
                    'No statistics because of unavailable management.',
                    extra={
                        'connection_id':
                            self.connection_model_instance.id.get()
                    })

            logger.info(
                'Tor subprocess has started %s'
                % (str(self.process._process),), extra={
                    'connection_id': connection_id
                })
        except ProcessError as errors:
            logger.error(
                str(errors),
                extra={
                    'connection_id': connection_id
                })
            self.config_file_name = None
            self.connection_model_instance.disconnect()
        self.unlock()

    def start_shutdown_loop(self, connection_model_instance):
        shutdown_loop = tor_shutdown_loop(self)
        shutdown_loop.start()

    def start_status_loop(self, connection_model_instance):
        status = tor_status_loop(self, connection_model_instance)
        status.start()

    def status(self):
        '''
        Request Status from Tor instance/
        '''

        message1 = message_list_item_model()
        message1.date.set(time.time())
        message1.data.set('GETINFO traffic/read')
        message2 = message_list_item_model()
        message2.date.set(time.time())
        message2.data.set('GETINFO traffic/written')
        self.lock()
        if self.connection_model_instance.up.get() is True:
            self.connection_model_instance.send_message_list.append(message1)
            self.connection_model_instance.send_message_list.append(message2)
        self.unlock()
        self.send_management_commands()

    def unlock(self):
        '''
        Unlock the process, other threads may modify its resources.
        '''
        self.threading_lock.release()


class tor_status_loop(threading.Thread):
    def __init__(self, tor_instance, connection_model_instance):
        threading.Thread.__init__(self)
        self.tor = tor_instance
        self.connection = connection_model_instance

    def run(self):
        while self.connection.active.get():
            time.sleep(config.TOR_STATUS_SLEEP)
            # when the process is not 'in the right state' it will stop working
            self.tor.status()
        logger.info(
            'Tor connection is no longer active.',
            extra={
                'connection_id': self.connection.id.get()
            })
        if self.tor.process is not None:
            self.tor.process.stop()


class tor_shutdown_loop(threading.Thread):
    def __init__(self, tor_instance):
        threading.Thread.__init__(self)
        self.tor = tor_instance

    def run(self):
        shutdown_tries = 0
        while self.tor.process.return_code is None:
            self.tor.lock()
            self.tor.process.stop()
            self.tor.process._process.poll()
            self.tor.unlock()
            time.sleep(config.TOR_SHUTDOWN_SLEEP)
            if shutdown_tries < config.TOR_SHUTDOWN_TRIES:
                shutdown_tries += 1
                logger.info(
                    'Waiting for tor process to stop: %d' % (shutdown_tries,))
                continue
            break
        self.tor.process.poll()
        if self.tor.process.return_code is None:
            logger.info('Killing tor process.')
            self.tor.lock()
            self.tor.process._process.kill()
            self.tor.process._process.poll()
            self.tor.unlock()

        if self.tor.process.return_code is None:
            logger.error('Failed to kill tor process.')
