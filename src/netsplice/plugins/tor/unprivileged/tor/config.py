# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

# Verbosity for Management Connections. Useful during development. The messages
# are passed to the Backend in production anyway.
# 0: No verbosity
# 3: Display messages sent and received to the management_process
VERBOSE = 0

# Buffer Size for receiving management messages
# Small buffer will cause the messages to be split into multiple
# recv calls and require the process_message to combine them correctly
# 1024: Default
# 16: Debug the process_message code
MAX_RECV = 1024

# Path to Tor binary
# Linux: /usr/bin/tor
# OSX/Windows: CWD/libexec/tor/VERSION/bin/tor
# This config may be overridden on application-startup
TOR_BINARY_PATH = '/usr/bin/tor'

# Relative Path, will be used when it is available relative to the
# executable loading this module
TOR_BINARY_PKG_PATH = 'tor/tor'

# Relative Path, will be used when it is available relative to the
# executable loading this module
TOR_BINARY_EXE_PATH = 'tor/tor.exe'

# Frequency of status polling in seconds.
TOR_STATUS_SLEEP = 1

# Frequency of shutdown messages
TOR_SHUTDOWN_SLEEP = 0.5

# Number of shutdown messages before SIGKILL
TOR_SHUTDOWN_TRIES = 20

# Frequency of syncing process model (stderr/stdout) with the actual model
TOR_MODEL_SYNC = 0.1
