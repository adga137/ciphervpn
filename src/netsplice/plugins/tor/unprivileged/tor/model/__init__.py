# -*- coding: utf-8 -*-
# model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Privileged OpenVPN
'''

import uuid
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.util.errors import (
    InvalidUsageError
)
from .connection_list import (
    connection_list as connection_list_model
)
from .connection import (
    connection as connection_model
)


class model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.connections = connection_list_model()

    def add_connection(self, connection_model_instance):
        '''
        Add the connection to the active connection list if it was setup.
        '''
        if connection_model_instance.process is None:
            raise InvalidUsageError(
                'dev: connections need setup before add')
        self.connections.append(connection_model_instance)
        self.commit()

    def create_connection(self, type, setup_model_instance):
        '''
        Create a connection that has to be setup before it is added to the
        connection list. Refuses to create the connection if the maximum number
        of connections has been reached.
        '''
        connection = connection_model()
        connection.id.set(str(uuid.uuid4()))
        connection.type.set(type)
        connection.configuration.set(
            setup_model_instance.configuration.get())
        connection.version.set(
            setup_model_instance.version.get())
        return connection
