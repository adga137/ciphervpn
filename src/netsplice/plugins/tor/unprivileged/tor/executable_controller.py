# -*- coding: utf-8 -*-
# executable_controller.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.ipc.middleware import middleware

from .model.response.version_list import (
    version_list as version_list_model
)
from .tor import tor


class executable_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def get(self):
        '''
        The "list-executables" action lists the available executable versions

        request model: {}
        response_model: [
            {"version": "v1.2.3-vanilla"}, {"version": "v2.3.4-abc"}]
        '''
        response_model = version_list_model()
        tor_dispatcher = tor(None, None)
        version_list = tor_dispatcher.get_version_list()
        response_model.from_json(version_list.to_json())
        self.write(response_model.to_json())
        self.finish()
