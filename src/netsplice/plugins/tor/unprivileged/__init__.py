# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from . import tor as tor_module
from netsplice.config import process as core_process_config
from netsplice.plugins.tor.config import process as plugin_process_config


def register(app):
    '''
    Register.

    Add the tor endpoints to the application and add a new binary system
    location.
    '''
    app.add_module(tor_module)

    plugin_locations = plugin_process_config.LIBEXEC_BINARY_SYSTEM_LOCATIONS
    for key, location in plugin_locations.items():
        core_process_config.LIBEXEC_BINARY_SYSTEM_LOCATIONS[key] = location
