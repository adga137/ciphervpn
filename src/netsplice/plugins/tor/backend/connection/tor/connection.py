# -*- coding: utf-8 -*-
# connections.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tor connection dispatcher. Defines dispatcher functions that are called by
controllers with the tor backend.
'''
from tornado import gen
from netsplice.backend.connection.connection_plugin import (
    connection_plugin as connection_base_dispatcher
)
from netsplice.backend.connection.model.status_list_item import (
    status_list_item as status_list_item_model
)
from netsplice.config import flags
from netsplice.config import connection as config_connnection
from netsplice.plugins.tor.config import backend as config
from netsplice.plugins.tor.config import tor as config_tor
from netsplice.util import get_logger
from netsplice.util.errors import NotFoundError
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.parser import factory as get_config_parser

logger = get_logger()


class connection(connection_base_dispatcher):
    '''
    '''
    def __init__(self, application, module_model):
        connection_base_dispatcher.__init__(
            self, 'Tor', application, module_model)

    @gen.coroutine
    def abort(self):
        '''
        Abort the process.

        Free all resources that were allocated.
        '''
        logger.warn('Aborting %s.' % (self.connection.account.name.get(),))
        try:
            status = yield self.get_status()
            if status:
                if status.active.get():
                    yield self.unprivileged.tor.disconnect(
                        self.connection.model.unprivileged_id.get())
                yield self.unprivileged.tor.remove(
                    self.connection.model.unprivileged_id.get())
            self.connection.model.removed = True
        except ServerConnectionError:
            pass

    @gen.coroutine
    def connect(self):
        '''
        Connect a setup connection.

        Instruct tor to initiate a previously setup connection.
        '''
        del self.connection.model.errors[:]
        self.connection.model.reset_environment()
        self.connection.model.reset_routes()
        self.connection.model.commit()

        plain_config = self.connection.account.configuration.get()
        tor_config_parser = get_config_parser(self.type)
        tor_config_parser.set_text(plain_config)
        for element in tor_config_parser.elements:
            key = element.key.get()
            value = element.get_value('value')
            self.connection.model.environment.add_value(
                'tor_%s' % (key,), value)
            if key == 'SOCKSPort':
                self.connection.model.environment.add_value(
                    'connection_socks', value)

        try:
            yield self.unprivileged.tor.connect(
                self.connection.model.unprivileged_id.get())
        except ServerConnectionError as errors:
            logger.critical(
                'Failed to connect %s in the unprivileged backend: %s'
                % (self.connection.account.name.get(), str(errors),),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
            self.disconnect_abort()
            raise gen.Return(False)
        self.schedule_get_status()
        raise gen.Return(True)

    @gen.coroutine
    def disconnect(self):
        '''
        Disconnect an active connection.

        Instruct tor to shutdown a previously setup connection only when
        the connection is currently active. Remove the connection from the
        unprivileged backend afterwards.
        '''
        # disconnect finish is triggered from check_log_item
        if self.connection.in_disconnecting_process():
            logger.warn(
                'Already disconnecting: %s.'
                % (self.connection.account.name.get(),),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
            raise gen.Return(False)
        if self.connection.is_disconnected():
            logger.warn(
                'Already disconnected: %s.'
                % (self.connection.account.name.get(),),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
            raise gen.Return(False)
        try:
            status = yield self.get_status()
            if self.connection.state == config_connnection.DISCONNECTING or \
                    self.connection.state == \
                    config_connnection.DISCONNECTING_FAILURE:
                if self.connection.state == \
                        config_connnection.DISCONNECTING_FAILURE:
                    self.disconnect_failure_process()
                else:
                    self.disconnect_process()
            if status:
                if status.active.get():
                    yield self.unprivileged.tor.disconnect(
                        self.connection.model.unprivileged_id.get())
                yield self.unprivileged.tor.remove(
                    self.connection.model.unprivileged_id.get())
            if self.connection.state == \
                    config_connnection.DISCONNECTING_FAILURE_PROCESS or \
                    self.connection.state == \
                    config_connnection.DISCONNECTING_PROCESS:
                if self.connection.state == \
                        config_connnection.DISCONNECTING_FAILURE_PROCESS:
                    self.disconnect_failure_finish()
                else:
                    self.disconnect_finish()
            self.connection.model.removed = True
        except ServerConnectionError as errors:
            if self.connection.model.failed.get():
                if self.connection.state == \
                        config_connnection.DISCONNECTING_FAILURE:
                    self.disconnect_failure_process()
                    self.disconnect_failure_finish()
                elif self.connection.state == \
                        config_connnection.DISCONNECTING_FAILURE_PROCESS:
                    self.disconnect_failure_finish()
                else:
                    self.disconnect_failure_init()
                    self.disconnect_failure_process()
                    self.disconnect_failure_finish()

            logger.warn(
                'Failed to disconnect %s in the unprivileged backend: %s'
                % (self.connection.account.name.get(), str(errors),),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
            raise gen.Return(False)
        raise gen.Return(True)

    @gen.coroutine
    def get_status(self):
        '''
        Get the latest status of the connection.

        Result connection contains connection statistics and an active bit.
        '''
        if self.connection.in_disconnecting_process():
            raise gen.Return(None)
        if self.connection.is_disconnected():
            raise gen.Return(None)

        try:
            result = yield self.unprivileged.tor.connection_status(
                self.connection.model.unprivileged_id.get())

            # gui.model.connection_list_item
            try:
                gui_model = self.application.get_module('gui').model
                gui_connection = gui_model.connections.find_by_id(
                    self.connection.id.get())
                if gui_connection.update_connection_statistic(result):
                    gui_model.commit()
            except NotFoundError:
                pass
            raise gen.Return(result)
        except ServerConnectionError as errors:
            logger.critical(
                'Failed to get status %s in the unprivileged backend: %s'
                % (self.connection.account.name.get(), str(errors),),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
            raise gen.Return(None)

    @gen.coroutine
    def reconnect(self):
        '''
        Reconnect a previously connected connection.

        Changes the model state and calls disconnect on the current connection.
        Then setup a new connection as backend.gui.account would do.
        This is required because the unprivileged tor process does not support
        reconnect.
        '''
        # reconnected is triggered in self.check_log_item
        del self.connection.model.errors[:]
        self.connection.model.commit()
        try:
            status = yield self.get_status()
            if status:
                if status.active.get():
                    yield self.unprivileged.tor.reconnect(
                        self.connection.model.unprivileged_id.get())
                    raise gen.Return(True)
            yield self.unprivileged.tor.connect(
                self.connection.model.unprivileged_id.get())
            raise gen.Return(True)
        except ServerConnectionError as errors:
            logger.critical(
                'Failed to reconnect %s in the unprivileged backend: %s'
                % (self.connection.account.name.get(), str(errors),),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
            self.disconnect_abort()
            raise gen.Return(False)
        except NotFoundError as errors:
            logger.critical(
                'Failed to find account to reconnect %s in the unprivileged'
                ' backend: %s'
                % (self.connection.account.name.get(), str(errors),),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
            self.disconnect_abort()
            raise gen.Return(False)

    @gen.coroutine
    def setup(self, connection_model_instance, account_model_instance):
        '''
        Setup a connection with the values of the account.

        Creates the connection_id and returns a connection model instance.
        '''
        plain_config = account_model_instance.configuration.get()
        tor_config_parser = get_config_parser(self.type)
        tor_config_parser.set_text(plain_config)

        plain_config = tor_config_parser.get_text()

        try:
            connection_id = yield self.unprivileged.tor.setup_connection(
                plain_config,
                config.DEFAULT_TOR_VERSION)  # XXX: account-> select versions

            self.log_connection_id = connection_id.id.get()
            connection_model_instance.unprivileged_id.set(
                connection_id.id.get())
        except ServerConnectionError as errors:
            logger.critical(
                'Failed to setup %s in the unprivileged backend: %s'
                % (self.connection.account.name.get(), str(errors),),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
            self.setup_cancel()
            raise gen.Return(False)
        self.setup_finish()
        raise gen.Return(True)

    def check_log_item(self, log_item):
        '''
        Update connection status from the given message.

        Modifies the connection model and adds messages to the status and error
        lists. Interprets certain messages to be a status-change that needs
        further processing.
        '''
        message = log_item.message.get()
        # Log message from Streamlog (stdout of unprivileged process)
        # without the date and origin infos.
        log_message = message[20:]
        # The message is a 'conclusion' of the process. It is usually the last
        # (fatal) or a success indicator.
        # state_changed
        status_changed = False
        status_initial = self.connection.state
        # The Message was not handled
        handled = False

        error_log_item = status_list_item_model()
        error_log_item.state.set('failure')
        error_log_item.message.set(log_message)
        status_item = status_list_item_model()
        status_item.message.set(message)
        status_item.state.set('success')

        for tor_log, details in config_tor.LOG_MESSAGES.items():
            if not (message.startswith(tor_log) or
                    log_message.startswith(tor_log)):
                continue
            handled = True
            if 'exceptions' in details.keys():
                exception_details = None
                for in_string, exception in details['exceptions'].items():
                    if not (in_string in message or
                            in_string in log_message):
                        continue
                    exception_details = exception
                    break
                if exception_details:
                    details = exception_details
            actions = details['actions']
            if config_tor.LOG_CHANGE_STATE in actions:
                status_changed = True

            if config_tor.LOG_CONNECTED in actions:
                if self.connection.state == \
                        config_connnection.RECONNECTING_PROCESS:
                    self.reconnect_finish()
                elif self.connection.state == \
                        config_connnection.RECONNECTING:
                    self.reconnect_process()
                    self.reconnect_finish()
                elif self.connection.state == \
                        config_connnection.CONNECTING_PROCESS:
                    self.connect_finish()
                elif self.connection.state == \
                        config_connnection.CONNECTING:
                    self.connect_process()
                    self.connect_finish()
                else:
                    logger.error(
                        'Invalid LOG_CONNECTED by %s for %s in state %s.'
                        % (message, self.connection.account.name.get(),
                            self.connection.state),
                        extra={
                            'connection_id': self.connection.id.get(),
                            'account_id': self.connection.account.id.get()
                        })
            if config_tor.LOG_DISCONNECTED in actions:
                if self.connection.state == \
                        config_connnection.DISCONNECTING:
                    self.disconnect_process()
                    self.disconnect_finish()
                elif self.connection.state == \
                        config_connnection.DISCONNECTING_PROCESS:
                    self.disconnect_finish()
                elif self.connection.state == \
                        config_connnection.DISCONNECTING_FAILURE:
                    self.disconnect_failure_process()
                    self.disconnect_failure_finish()
                elif self.connection.state == \
                        config_connnection.DISCONNECTING_FAILURE_PROCESS:
                    self.disconnect_failure_finish()
                elif self.connection.state == \
                        config_connnection.DISCONNECTED:
                    pass
                elif self.connection.state == \
                        config_connnection.DISCONNECTED_FAILURE:
                    pass
                elif self.connection.state == \
                        config_connnection.RECONNECTING_PROCESS:
                    pass
                elif self.connection.state == \
                        config_connnection.RECONNECTING:
                    pass
                else:
                    self.disconnect_init()
                    self.disconnect_process()
                    self.disconnect_finish()
            if config_tor.LOG_CONNECTING in actions:
                if self.connection.state == \
                        config_connnection.RECONNECTING:
                    self.reconnect_process()
                elif self.connection.state == \
                        config_connnection.CONNECTING:
                    self.connect_process()

            if config_tor.LOG_ERROR in actions:
                status_changed = True
                if self.connection.model.failed.get() is False:
                    self.connection.model.failed.set(True)
                    self.disconnect_failure_init()
                status_item.state.set('failure')
                self.connection.model.errors.append(status_item)
                log_item.level.set('error')
            if config_tor.LOG_ABORT in actions:
                status_changed = True
                self.connection.model.failed.set(True)
                self.disconnect_abort()
                status_item.state.set('failure')
                self.connection.model.errors.append(status_item)
                log_item.level.set('error')
            if config_tor.LOG_INFO in actions:
                log_item.level.set('info')
            if config_tor.LOG_DEBUG in actions:
                log_item.level.set('debug')
            if config_tor.LOG_WARNING in actions:
                log_item.level.set('warning')
                status_item.state.set('warning')
            if config_tor.LOG_PROGRESS in actions:
                self.connection.model.progress.append(status_item)
            log_item.message.set(
                '%s (%s)'
                % (details['help'], log_item.message.get()))

        if flags.VERBOSE > 1 and not handled:
            logger.info(
                message,
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
        if status_initial != self.connection.state:
            status_changed = True
        return status_changed
