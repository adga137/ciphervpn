# -*- coding: utf-8 -*-
# unprivileged_dispatcher.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Dispatcher for unprivileged actions.
'''

from socket import error as socket_error

from tornado import gen, httpclient

from . import dispatcher_endpoints as endpoints
from .unprivileged.model.request.connection_id import (
    connection_id as connection_id_model
)
from .unprivileged.model.request.setup import (
    setup as setup_model
)
from .unprivileged.model.response.connection_list import (
    connection_list as connection_list_model
)
from .unprivileged.model.response.connection_status import (
    connection_status as connection_status_model
)
from .unprivileged.model.request.version import version as version_model
from .unprivileged.model.response.version_detail import (
    version_detail as version_detail_model
)
from .unprivileged.model.response.version_help import (
    version_help as version_help_model
)
from .unprivileged.model.response.version_list import (
    version_list as version_list_model
)
from netsplice.model.process import (
    process as process_model
)
from netsplice.util import get_logger
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.ipc.service import service
from netsplice.util.path import get_path_prefix
from netsplice.util.process.dispatcher import dispatcher as process_dispatcher
from netsplice.util.process.errors import ProcessError
from netsplice.util.model.errors import ValidationError

logger = get_logger()


class unprivileged_dispatcher(object):

    def __init__(self, service_instance):
        self.service = service_instance

    @gen.coroutine
    def connect(self, connection_id):
        options = {
            'connection_id': connection_id
        }
        try:
            response = yield self.service.post(
                endpoints.TOR_CONN_CONNECT % options,
                '{}'
            )

            response_model = connection_id_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2138, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2139, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2140, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2141, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def connection_status(self, connection_id):
        options = {
            'connection_id': connection_id
        }

        response_model = connection_status_model()
        try:
            response = yield self.service.get(
                endpoints.TOR_CONN_STATUS % options,
                None
            )
            response_model.from_json(response.body)
            model = self.service.application.get_module('unprivileged').model
            model.set_connection_status(
                'tor', response_model.id.get(), response_model)
            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2142, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2143, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2144, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2145, str(errors))
            response_model.id.set(connection_id)
            response_model.active.set(False)
            raise gen.Return(response_model)

    @gen.coroutine
    def disconnect(self, connection_id):
        options = {
            'connection_id': connection_id
        }
        try:
            response = yield self.service.post(
                endpoints.TOR_CONN_DISCONNECT % options,
                '{}'
            )

            response_model = connection_id_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2146, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2147, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2148, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2149, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def get_version_list(self):
        try:
            response_model = version_list_model()
            response = yield self.service.get(
                endpoints.TOR_UNPRIV_EXECUTABLES,
                None
            )
            response_model.from_json(response.body)
            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2341, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2342, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2343, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2344, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def get_version_detail(self, version):
        try:
            request_model = version_model()
            response_model = version_detail_model()
            request_model.version.set(version)
            options = {
                'version': request_model.version.get()
            }
            response = yield self.service.get(
                endpoints.TOR_UNPRIV_VERSION % options,
                None
            )
            response_model.from_json(response.body)
            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2345, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2346, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2347, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2348, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def get_version_help(self, version):
        try:
            request_model = version_model()
            response_model = version_help_model()
            request_model.version.set(version)
            options = {
                'version': request_model.version.get()
            }
            response = yield self.service.get(
                endpoints.TOR_UNPRIV_HELP % options,
                None
            )
            response_model.from_json(response.body)
            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2349, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2350, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2351, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2352, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def reconnect(self, connection_id):
        options = {
            'connection_id': connection_id
        }
        try:
            response = yield self.service.post(
                endpoints.TOR_CONN_RECONNECT % options,
                '{}'
            )

            response_model = connection_id_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2270, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2271, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2272, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2273, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def remove(self, connection_id):
        options = {
            'connection_id': connection_id
        }
        try:
            response = yield self.service.delete(
                endpoints.TOR_CONN_REMOVE % options
            )

            response_model = connection_id_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2150, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2151, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2152, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2153, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def list_connections(self):
        response = yield self.service.get(
            endpoints.TOR_CONN_LIST,
            None
        )
        try:
            response_model = connection_list_model()
            response_model.from_json(response.body)

            model = self.service.application.get_module('unprivileged').model
            model.set_connections('tor', response_model)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2154, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2155, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2156, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2157, str(errors))
            raise ServerConnectionError(str(errors))

    @gen.coroutine
    def setup_connection(
            self, configuration, tor_version):
        request_model = setup_model()
        try:
            request_model.configuration.set(configuration)
            request_model.version.set(tor_version)

            response = yield self.service.put(
                endpoints.TOR_CONN_SETUP,
                request_model.to_json()
            )
            response_model = connection_id_model()
            response_model.from_json(response.body)

            raise gen.Return(response_model)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.service.set_error_code(2162, str(errors))
            raise ServerConnectionError(str(errors))
        except ValidationError as errors:
            self.service.set_error_code(2163, str(errors))
            raise errors
        except ValueError as errors:
            self.service.set_error_code(2164, str(errors))
            raise ValidationError(str(errors))
        except httpclient.HTTPError as errors:
            self.service.set_error_code(2165, str(errors))
            raise ServerConnectionError(str(errors))
