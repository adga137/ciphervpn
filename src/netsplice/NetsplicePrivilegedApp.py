#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Part of the application that is used to execute privileged operations.
'''

import atexit
import multiprocessing
import pwd
import os
import sys
import time

import netsplice.privileged.systeminfo as systeminfo_module

from netsplice import __version__ as VERSION
from netsplice.util import get_logger, take_logs

from netsplice.config import flags
from netsplice.config import constants
from netsplice.config import process as config_process
from netsplice.model.privileged import privileged as privileged_model
from netsplice.plugins import get_plugins
from netsplice.privileged.backend_dispatcher import backend_dispatcher
from netsplice.privileged.event_loop import event_loop
from netsplice.util import commandline
from netsplice.util.ipc.errors import (
    ServerStartFailedError,
    NoSharedSecretError
)
from netsplice.util.ipc.route import get_route
from netsplice.util.ipc.server import server
from netsplice.util.ipc.shared_secret import shared_secret
from netsplice.util.path import set_config_home
from netsplice.util.process.reaper import reaper

logger = get_logger()


@atexit.register
def stop_priv():
    '''
    Handle shutdown.

    Print unpublished log entries.
    '''
    for log_item in take_logs():
        message = ''
        level = 'INFO'
        if isinstance(log_item, (dict,)):
            message = log_item['message']
            level = log_item['level']
        else:
            message = log_item.message.get()
            level = log_item.level.get()
        if level in ['ERROR', 'CRITICAL']:
            sys.stderr.write(message + '\n')
        else:
            sys.stdout.write(message + '\n')
    sys.stdout.flush()


def init_priv_uids():
    '''
    Init Privileged Uids

    In darwin there is a issue that the application needs to use the setuid
    bit on the executable to prevent the password dialog everytime the
    application starts. Now Netsplice needs to run bash scripts (eg the
    process_launcher) that need to do privileged tasks (change dns for example)
    As bash uses the uid for execution the Privileged process needs to force
    its uid to root, otherwise the scripts cannot work as expected (and ask
    for a password for every privileged operation (about 10 times per
    connection)
    '''
    if not sys.platform.startswith('darwin'):
        return
    try:
        pwd_name_tuple = pwd.getpwnam('root')
        root_id = pwd_name_tuple[2]
        wheel_id = pwd_name_tuple[3]
        if os.geteuid() == root_id:
            os.setuid(root_id)
            os.setgid(wheel_id)
            logger.info(
                'The uid/gid is set to 0 to allow process_launcher'
                ' scripts to keep elevated state.')
        else:
            logger.info(
                'Not dropping the uid as the PrivilegedApp is not run in'
                ' setuid mode.')
    except KeyError:
        logger.error(
            'The uid/gid for root could not be found. Expect'
            ' failing process_launcher scripts!')


def run_priv():
    '''
    Run the privileged tasks for the application.

    :param options: A dict of options parsed from the command line.
    :type options: dict
    :param flags_dict: A dict containing the flag values set on app start.
    :type flags_dict: dict
    '''
    options = commandline.get_options()

    flags.DEBUG = options.debug
    logger.info('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    logger.info('Netsplice version %s' % VERSION)
    logger.info('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

    init_priv_uids()

    backend_host = constants.LOCALHOST
    backend_port = constants.PORT_BACKEND

    if options.port_owner is not None:
        backend_port = options.port_owner

    if options.host_owner is not None:
        backend_host = options.host_owner

    if options.host is None:
        options.host = constants.LOCALHOST
    if options.port is None:
        options.port = constants.PORT_PRIV
    if options.config_home is not None:
        set_config_home(options.config_home)

    reaper_instance = reaper()
    if options.kill is not None:
        if reaper_instance.kill_pid(options.kill):
            sys.exit(0)
        sys.exit(1)
    if reaper_instance.kill_other_subprocess(sys.argv):
        time.sleep(config_process.WAIT_AFTER_KILL_OTHER)

    # a shared secret has to be generated from the backend
    # before the privileged process may start
    try:
        shared_secret.load_from_file()
    except NoSharedSecretError:
        logger.error(
            'Shared secret %s from backend could not be loaded'
            % (shared_secret.get_filename(),))
        sys.exit(1)

    app = server(options.host, options.port, 'privileged')
    app.set_owner(backend_dispatcher(backend_host, backend_port))
    app.set_model(privileged_model())
    app.set_event_loop(event_loop())
    app.add_module(systeminfo_module)

    for plugin in get_plugins():
        app.add_plugin(plugin)
    app.register_plugin_components('util')
    app.register_plugin_components('privileged')

    try:
        logger.debug('Start Application')
        app.start(get_route('netsplice.privileged'))
        sys.exit(app.exec_())
    except ServerStartFailedError:
        logger.critical('Application server failed to start')
        sys.exit(1)


if __name__ == '__main__':
    multiprocessing.freeze_support()
    run_priv()
